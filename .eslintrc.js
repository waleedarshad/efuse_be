const RULE = {
  OFF: 0,
  ON: 1,
  ERROR: 2
};

module.exports = {
  env: { browser: false, node: true, es2021: true },
  plugins: ["unused-imports", "import"],
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:eslint-comments/recommended"
  ],
  parser: "@typescript-eslint/parser",
  parserOptions: {
    createDefaultProgram: false,
    debugLevel: false,
    ecmaVersion: 2021,
    sourceType: "module",
    project: ["./tsconfig.eslint.json"]
  },
  reportUnusedDisableDirectives: true,
  rules: {
    "@typescript-eslint/no-explicit-any": RULE.OFF,
    "@typescript-eslint/ban-ts-comment": RULE.OFF,
    "@typescript-eslint/explicit-module-boundary-types": RULE.OFF,
    "@typescript-eslint/no-non-null-assertion": RULE.OFF,
    "@typescript-eslint/ban-types": RULE.OFF,
    "@typescript-eslint/no-empty-function": RULE.OFF,
    "@typescript-eslint/no-unused-vars": RULE.OFF,
    "@typescript-eslint/no-floating-promises": RULE.ERROR,
    "no-prototype-builtins": RULE.OFF,
    "no-async-promise-executor": RULE.OFF,
    "unused-imports/no-unused-imports": RULE.ERROR,
    "import/no-relative-packages": RULE.ERROR
  },
  overrides: [{ files: ["*.js"], rules: { "@typescript-eslint/no-var-requires": RULE.OFF } }]
};
