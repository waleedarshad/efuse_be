/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  testEnvironment: "node",
  verbose: true,
  projects: ["<rootDir>/packages/api/integration-tests/jest.config.js"]
};
