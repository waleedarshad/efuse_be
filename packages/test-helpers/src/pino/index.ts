class MockPino {
  constructor(binding: any) {}

  public child(bindings: any): MockPino {
    return new MockPino(bindings);
  }

  public debug(mergingObject: unknown, message: string, ...interpolationValues: unknown[]): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        console.debug(message);
        return { m: message, o: mergingObject, v: interpolationValues };
      })
      .mockName("pino.debug");
  }

  public error(mergingObject: unknown, message: string, ...interpolationValues: unknown[]): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        console.error(message);

        return {
          m: message,
          o: mergingObject,
          v: interpolationValues
        };
      })
      .mockName("pino.error");
  }

  public fatal(mergingObject: unknown, message: string, ...interpolationValues: unknown[]): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        console.error(message);

        return { m: message, o: mergingObject, v: interpolationValues };
      })
      .mockName("pino.fatal");
  }

  public info(mergingObject: unknown, message: string, ...interpolationValues: unknown[]): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        console.info(message);

        return { m: message, o: mergingObject, v: interpolationValues };
      })
      .mockName("pino.info");
  }

  public silent(): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        return null;
      })
      .mockName("pino.silent");
  }

  public trace(mergingObject: unknown, message: string, ...interpolationValues: unknown[]): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        console.trace(message);

        return { m: message, o: mergingObject, v: interpolationValues };
      })
      .mockName("pino.trace");
  }

  public warn(mergingObject: unknown, message: string, ...interpolationValues: unknown[]): jest.Mock<unknown, any> {
    return jest
      .fn()
      .mockImplementation(() => {
        console.warn(message);

        return { m: message, o: mergingObject, v: interpolationValues };
      })
      .mockName("pino.warn");
  }
}

export const pino = (options: unknown): MockPino => new MockPino(options);
