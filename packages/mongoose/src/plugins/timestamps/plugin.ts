import { CallbackError, Document, Schema } from "mongoose";

import { TimestampOptions } from "./options";

interface Timestampable {
  createdAt?: Date;
  updatedAt?: Date;
  $setOnInsert?: Timestampable;
}

type TimestampedDocument = {
  _createdAt: Date;
  _update: Timestampable;
  $setOnInsert: Timestampable;
  createdAt?: Date;
  methods: any;
  updatedAt?: Date;
} & Document;

function save(this: TimestampedDocument, next: (error?: CallbackError) => void): void {
  if (this.isNew) {
    const newDate = new Date();

    this.createdAt = newDate;
    this.updatedAt = newDate;
  } else if (this.isModified()) {
    this.updatedAt = new Date();
  }

  next();
}

function touch(this: TimestampedDocument, next: (error?: CallbackError) => void): void {
  this.updatedAt = new Date();
  this.save(next);
}

function update(this: TimestampedDocument, next: (error?: CallbackError) => void): void {
  if (this.$op === "findOneAndUpdate") {
    const newDate = new Date();

    this._update = this._update || {};

    if (this._update.createdAt) {
      delete this._update.createdAt;
    }

    this._update.$setOnInsert = this._update.$setOnInsert || {};
    this._update.$setOnInsert.createdAt = newDate;
    this._update.updatedAt = newDate;
  }

  next();
}

function plugin(schema: Schema<Document>, options?: TimestampOptions): void {
  const dataObj: Record<string, any> = {};

  if (!schema.path("updatedAt")) {
    dataObj.updatedAt = Date;
  }

  if (schema.path("createdAt")) {
    if (!schema.path("updatedAt")) {
      schema.add(dataObj);
    }

    schema.virtual("createdAt").get(function (this: TimestampedDocument) {
      if (this && !this._createdAt) {
        this._createdAt = this._id.getTimestamp();
      }

      return this._createdAt;
    });

    schema.pre(<never>"save", save);
  } else {
    dataObj.createdAt = Date;

    if (dataObj.createdAt || dataObj.updatedAt) {
      schema.add(dataObj);
    }

    schema.pre(<never>"save", save);
  }

  schema.pre(<never>"findOneAndUpdate", update);
  schema.pre(<never>"update", update);

  if (!schema.methods.hasOwnProperty("touch")) {
    (<TimestampedDocument>(<unknown>schema)).methods.touch = touch;
  }
}

export const timestamps = plugin;
