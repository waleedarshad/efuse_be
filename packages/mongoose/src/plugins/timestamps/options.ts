export interface TimestampInitialOptions {
  createdAt: string;
  updatedAt: string;
}

export class TimestampOptions {
  public createdAt: string;
  public updatedAt: string;

  constructor(init: TimestampInitialOptions) {
    if (!init) {
      throw new Error("config is missing");
    }

    this.createdAt = "createdAt";
    this.updatedAt = "updatedAt";
  }
}
