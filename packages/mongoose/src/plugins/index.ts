export { timestamps } from "./timestamps";

export type { TimestampOptions } from "./timestamps";
