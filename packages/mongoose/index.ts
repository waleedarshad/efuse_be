export { timestamps } from "./src/plugins";

export type { TimestampOptions } from "./src/plugins";
