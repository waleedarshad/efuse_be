import { Namespace } from "cls-hooked";

import { ILogger } from "../interfaces/logger";
import { Logger } from "../logger";
import { StreamParser } from "../utils/stream-parser";
import { streamToGenerator } from "../utils/stream-to-generator";

describe("@efuse/logger", () => {
  it("should correctly initialize", () => {
    const logger = Logger.create();

    expect(logger).toBeDefined();
  });

  it("should correctly initialize with only options", () => {
    const logger = Logger.create({ name: "jest" });
    expect(logger).toBeDefined();
  });

  it("should properly log message with context", async () => {
    const transformer = StreamParser.transformer();
    const gen = streamToGenerator(transformer);
    const logger: ILogger = Logger.create({ name: "jest" }, transformer);

    const context = { placeholder: "value" };
    const message = "foo";

    logger.info(context, message);

    const entry: any = await gen.next().value;
    expect(entry.placeholder).toEqual(context.placeholder);
    expect(entry.msg).toEqual(message);
  });

  it("should expose a CLS namespace", () => {
    const logger = Logger.create();

    expect(logger.cls).toBeDefined();
  });

  it("should have distinct namespaces if provided", () => {
    const logger = Logger.create();
    const another = Logger.create();

    expect(logger).not.toBe(another);
  });

  it("should properly log message with cls", () => {
    const transformer = StreamParser.transformer();
    const gen = streamToGenerator(transformer);
    const logger: ILogger = Logger.create({ name: "jest" }, transformer);

    const message = "foo";
    const clsValues = { placeholder: "value", another: "another value" };
    const { cls }: { cls: Namespace } = logger;

    cls.run(() => {
      cls.set("placeholder", clsValues.placeholder);
      cls.set("another", clsValues.another);

      process.nextTick(async () => {
        try {
          logger.info(message);

          const entry: any = await gen.next().value;

          expect(entry.placeholder).toEqual(clsValues.placeholder);
          expect(entry.another).toEqual(clsValues.another);
        } catch (error) {
          console.error(error);
        }
      });
    });
  });

  it("should properly log message with both cls and local context", () => {
    const transformer = StreamParser.transformer();
    const gen = streamToGenerator(transformer);
    const logger = Logger.create({ name: "jest" }, transformer);

    const msg = "foo";
    const clsValues = { another: null, placeholder: "value", precedence: "will be overwritten" };

    const { cls }: { cls: Namespace } = <any>logger;

    cls.run(() => {
      cls.set("placeholder", clsValues.placeholder);
      cls.set("another", clsValues.another);

      process.nextTick(async () => {
        const localValues = { precedence: "local" };
        logger.info(localValues, msg);

        const entry: any = await gen.next().value;
        expect(entry.precedence).toEqual(localValues.precedence);
      });
    });
  });

  it("should properly propagate cls context to child logger", () => {
    const transformer = StreamParser.transformer();
    const gen = streamToGenerator(transformer);
    const logger = Logger.create({ name: "jest" }, transformer);

    const msg = "foo";
    const clsValues = { placeholder: "value", another: "another value" };

    const childContext = { foo: "bar" };
    const child = logger.child(childContext);
    const { cls }: { cls: Namespace } = <any>logger;

    cls.run(() => {
      cls.set("placeholder", clsValues.placeholder);
      cls.set("another", clsValues.another);

      process.nextTick(async () => {
        child.info(msg);

        const entry: any = await gen.next().value;

        expect(entry.placeholder).toEqual(clsValues.placeholder);
        expect(entry.another).toEqual(clsValues.another);
        expect(entry.foo).toEqual(childContext.foo);
      });
    });
  });

  it("should properly propagate cls context to grandchild logger", () => {
    const transformer = StreamParser.transformer();
    const gen = streamToGenerator(transformer);
    const logger = Logger.create({ name: "jest" }, transformer);

    const msg = "foo";
    const clsValues = { placeholder: "value", another: "another value" };

    const childContext = { foo: "bar" };
    const child = logger.child(childContext);

    const grandChildContext = { fizz: "buzz" };
    const grandChild = child.child(grandChildContext);

    const { cls }: { cls: Namespace } = <any>logger;

    cls.run(() => {
      cls.set("placeholder", clsValues.placeholder);
      cls.set("another", clsValues.another);

      process.nextTick(async () => {
        grandChild.info(msg);

        const entry: any = await gen.next().value;

        expect(entry.placeholder).toEqual(clsValues.placeholder);
        expect(entry.another).toEqual(clsValues.another);
        expect(entry.foo).toEqual(childContext.foo);
        expect(entry.fizz).toEqual(grandChildContext.fizz);
      });
    });
  });
});
