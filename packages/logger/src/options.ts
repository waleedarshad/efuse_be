import os from "os";
import stdSerializers from "pino-std-serializers";

import { epochTime } from "./time";
import { IBindings, ILogEvent, ILogFn, ILoggerOptions, IPrettyOptions, IRedactOptions } from "./interfaces";
import { Level, LevelWithSilent, MixinFn, TimeFn, SerializerFn, WriteFn } from "./types";

const defaultErrorSerializer = stdSerializers.err;

export class LoggerOptions implements ILoggerOptions {
  base?: { [key: string]: unknown } | null;
  customLevels?: { [key: string]: number } | null;
  enabled?: boolean;
  level?: LevelWithSilent | string;
  levelKey?: string;
  levelVal?: number;
  messageKey?: string;
  mixin?: MixinFn;
  name?: string;
  nestedKey?: string | null;
  onTerminated?(eventName: string, error: unknown): void;
  prettifier?: unknown;
  prettyPrint?: boolean | IPrettyOptions;
  redact?: string[] | IRedactOptions | null;
  safe?: boolean;
  serializers?: { [key: string]: SerializerFn };
  timestamp?: TimeFn | boolean;
  useLevelLabels?: boolean;
  useOnlyCustomLevels?: boolean;

  browser?: {
    asObject?: boolean;
    serialize?: boolean | string[];

    transmit?: {
      level?: Level | string;
      send: (level: Level, logEvent: ILogEvent) => void;
    };

    write?:
      | WriteFn
      | ({
          fatal?: WriteFn;
          error?: WriteFn;
          warn?: WriteFn;
          info?: WriteFn;
          debug?: WriteFn;
          trace?: WriteFn;
        } & { [logLevel: string]: WriteFn });
  };

  formatters?: {
    bindings?: (bindings: IBindings) => Record<string, unknown>;
    level?: (level: string, number: number) => Record<string, unknown>;
    log?: (object: Record<string, unknown>) => Record<string, unknown>;
  };

  hooks?: {
    logMethod?: (args: unknown[], method: ILogFn) => void;
  };

  private constructor() {
    // intentionally empty
  }

  public static default(): ILoggerOptions {
    const options = new LoggerOptions();

    const { pid } = process;
    const hostname = os.hostname();

    options.base = { pid, hostname };
    options.customLevels = null;
    options.enabled = true;
    options.level = "info";
    options.levelKey = undefined;
    options.messageKey = "msg";
    options.name = undefined;
    options.nestedKey = null;
    options.prettyPrint = false;
    options.redact = null;
    options.timestamp = epochTime;
    options.useOnlyCustomLevels = false;

    options.serializers = Object.assign(Object.create(null), { err: defaultErrorSerializer });

    options.formatters = Object.assign(Object.create(null), {
      bindings(bindings: IBindings) {
        return bindings;
      },
      level(label: string, num: number) {
        return { level: num };
      }
    });

    options.hooks = {
      logMethod: undefined
    };

    return options;
  }
}
