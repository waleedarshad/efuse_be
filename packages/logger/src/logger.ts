import pino from "pino";
import { createNamespace, Namespace } from "cls-hooked";
import { ILogger, ILoggerOptions } from "./interfaces";

export class Logger {
  private static counter = 0;

  public static create(opts?: ILoggerOptions, destination?: pino.DestinationStream): ILogger {
    const cls = createNamespace(`@efuse/logger-${this.counter}`);
    this.counter += 1;

    const pinoInstance = pino(
      <pino.LoggerOptions>opts || { level: "info" },
      <pino.DestinationStream>(destination || process.stdout)
    );

    return <ILogger>this.wrap({ cls }, pinoInstance);
  }

  private static createProxies(target: pino.Logger): {
    debug: Logger;
    error: Logger;
    fatal: Logger;
    info: Logger;
    trace: Logger;
    warn: Logger;
    child: Logger;
  } {
    return {
      debug: new Proxy(target.debug, this.logMethodHandler),
      error: new Proxy(target.error, this.logMethodHandler),
      fatal: new Proxy(target.fatal, this.logMethodHandler),
      info: new Proxy(target.info, this.logMethodHandler),
      trace: new Proxy(target.trace, this.logMethodHandler),
      warn: new Proxy(target.warn, this.logMethodHandler),
      child: new Proxy(target.child, this.childMethodHandler)
    };
  }

  private static childMethodHandler: ProxyHandler<Logger> = {
    apply(target, thisArg, argumentList) {
      const { cls } = thisArg;

      return Logger.wrap({ cls }, (<any>target).apply(thisArg, argumentList));
    }
  };

  private static logMethodHandler = {
    apply(target, thisArg, argumentList) {
      const { id, _ns_name: namespace, ...clsContext } = ((thisArg || {}).cls || {}).active || {};
      const [context, ...messages] = argumentList;

      const args =
        typeof context === "string"
          ? [clsContext, context, ...messages]
          : [Object.assign(context, clsContext), ...messages];

      return target.apply(thisArg, args);
    }
  };

  private static wrap({ cls }: { cls: Namespace }, pinoInstance: pino.Logger): Logger {
    const baseLogger = Object.create(pinoInstance, {
      cls: {
        value: cls
      }
    });

    const loggerWithProxies = Object.create(baseLogger, {
      proxies: {
        value: this.createProxies(baseLogger)
      }
    });

    return new Proxy(loggerWithProxies, {
      get(target: pino.Logger, prop: string) {
        if (target.proxies && target.proxies[prop]) {
          return target.proxies[prop];
        }

        return target[prop];
      }
    }) as Logger;
  }
}
