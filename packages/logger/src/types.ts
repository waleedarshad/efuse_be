export type Level = "fatal" | "error" | "warn" | "info" | "debug" | "trace";
export type LevelWithSilent = Level | "silent";
export type MixinFn = () => Record<string, unknown>;
export type SerializerFn = (value: unknown) => unknown;
export type TimeFn = () => string;
export type WriteFn = (o: Record<string, unknown>) => void;
