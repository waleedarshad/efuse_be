export interface ILogFn {
  <T extends unknown>(obj: T, msg?: string, ...args: unknown[]): void;
  (msg: string, ...args: unknown[]): void;
}
