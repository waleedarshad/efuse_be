import { Level, SerializerFn } from "../types";

export interface IBindings {
  level?: Level | string;
  serializers?: { [key: string]: SerializerFn };
  [key: string]: unknown;
}
