import { Namespace } from "cls-hooked";

import { IBindings } from "./bindings";
import { ILogFn } from "./log-fn";
import { LevelWithSilent } from "../types";

export interface ILogger {
  debug: ILogFn;
  error: ILogFn;
  fatal: ILogFn;
  info: ILogFn;
  silent: ILogFn;
  trace: ILogFn;
  warn: ILogFn;

  bindings(): IBindings;
  child(bindings: IBindings): ILogger;
  flush(): void;
  isLevelEnabled(level: LevelWithSilent | string): boolean;

  cls: Namespace;
}
