export interface IPrettyOptions {
  /**
   * If set to true, will add color information to the formatted output message.
   *
   * @default: `false`.
   */
  colorize?: boolean;

  /**
   * Appends carriage return and line feed, instead of just a line feed, to the formatted log line.
   */
  crlf?: boolean;

  /**
   * Define the log keys that are associated with error like objects.
   *
   * @default: ["err", "error"]
   */
  errorLikeObjectKeys?: string[];

  /**
   *  When formatting an error object, display this list of properties.
   *  The list should be a comma separated list of properties.
   *
   * @default: ''
   */
  errorProps?: string;

  /**
   * Ignore one or several keys.
   *
   * @example: "time,hostname"
   */
  ignore?: string;

  /**
   * If set to true, it will print the name of the log level as the first
   * field in the log line.
   *
   * @default: `false`.
   */
  levelFirst?: boolean;

  /**
   * Format output of message, e.g. {level} - {pid} will output message: INFO - 1123
   *
   * @default: `false`.
   */
  messageFormat?: false | string;

  /**
   * The key in the JSON object to use as the highlighted message.
   *
   * @default: "msg".
   */
  messageKey?: string;

  /**
   * Specify a search pattern according to {@link http://jmespath.org|jmespath}
   */
  search?: string;

  /**
   * Suppress warning on first synchronous flushing.
   */
  suppressFlushSyncWarning?: boolean;

  /**
   * The key in the JSON object to use for timestamp display.
   *
   * @default: "time".
   */
  timestampKey?: string;

  /**
   * Translate the epoch time value into a human readable date and time string.
   * This flag also can set the format string to apply when translating the date to
   * human readable format.
   *
   * The default format is yyyy-mm-dd HH:MM:ss.l o in UTC.
   * For a list of available pattern letters
   * @see the {@link https://www.npmjs.com/package/dateformat|dateformat documentation}.
   */
  translateTime?: boolean | string;
}
