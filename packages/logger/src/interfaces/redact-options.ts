export interface IRedactOptions {
  paths: string[];
  censor?: string | ((v: unknown) => unknown);
  remove?: boolean;
}
