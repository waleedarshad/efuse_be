import { IBindings } from "./bindings";
import { ILogEvent } from "./log-event";
import { ILogFn } from "./log-fn";
import { IPrettyOptions } from "./pretty-options";
import { IRedactOptions } from "./redact-options";
import { Level, LevelWithSilent, MixinFn, SerializerFn, TimeFn, WriteFn } from "../types";

export interface ILoggerOptions {
  /**
   * key-value object added as child logger to each log line.
   * If set to null the base child logger is not added
   */
  base?: { [key: string]: unknown } | null;

  /**
   * Use this option to define additional logging levels.
   *
   * The keys of the object correspond the namespace of the log level, and the
   * values should be the numerical value of the level.
   */
  customLevels?: { [key: string]: number } | null;

  /**
   * Enables logging.
   *
   * @default: `true`.
   */
  enabled?: boolean;

  /**
   * One of the supported levels or `silent` to disable logging.
   * Any other value defines a custom level and requires supplying a
   * level value via `levelVal`.
   *
   * @default: 'info'.
   */
  level?: LevelWithSilent | string;

  /**
   * Changes the property `level` to any string value you pass in.
   *
   * @default: 'level'
   */
  levelKey?: string;

  /**
   * When defining a custom log level via level, set to an integer value to define the new level.
   *
   * @default: `undefined`.
   */
  levelVal?: number;

  /**
   * The string key for the 'message' in the JSON object.
   *
   * @default: "msg".
   */
  messageKey?: string;

  /**
   * If provided, the `mixin` function is called each time one of the active logging methods
   * is called. The function must synchronously return an object. The properties of the
   * returned object will be added to the logged JSON.
   */
  mixin?: MixinFn;

  /**
   * The name of the logger. @default: `undefined`.
   */
  name?: string;

  /**
   * The string key to place any logged object under.
   */
  nestedKey?: string | null;

  /**
   * This function will be invoked during process shutdown when `extreme` is
   * set to `true`.
   *
   * If you do not specify a function, Pino will invoke `process.exit(0)` when
   * no error has occurred, and `process.exit(1)` otherwise. If you do specify
   * a function, it is up to you to terminate the process; you must perform only
   * synchronous operations at this point.
   *
   * @see http://getpino.io/#/docs/extreme for more detail.
   */
  onTerminated?(eventName: string, error: unknown): void;

  /**
   * Allows to optionally define which prettifier module to use.
   */
  prettifier?: unknown;

  /**
   * Enables pino.pretty.
   *
   * This is intended for non-production configurations. This may be set to a configuration
   * object as outlined in http://getpino.io/#/docs/API?id=pretty.
   *
   * @default: `false`.
   */
  prettyPrint?: boolean | IPrettyOptions;

  /**
   * As an array, the redact option specifies paths that should have their values redacted from any log output.
   *
   * Each path must be a string using a syntax which corresponds to JavaScript dot and bracket notation.
   *
   * If an object is supplied, three options can be specified:
   *
   *      paths (String[]): Required. An array of paths
   *      censor (String): Optional. A value to overwrite key which are to be redacted. @default: '[Redacted]'
   *      remove (Boolean): Optional. Instead of censoring the value, remove both the key and the value. @default: false
   */
  redact?: string[] | IRedactOptions | null;

  /**
   * Avoid error causes by circular references in the object tree.
   * @default: `true`.
   */
  safe?: boolean;

  /**
   * An object containing functions for custom serialization of objects.
   *
   * These functions should return an JSONifiable object and they should never throw. When logging an object,
   * each top-level property matching the exact key of a serializer will be serialized using the defined serializer.
   */
  serializers?: { [key: string]: SerializerFn };

  /**
   * Enables or disables the inclusion of a timestamp in the log message.
   *
   * If a function is supplied, it must synchronously return a JSON string representation of the time.
   * If set to `false`, no timestamp will be included in the output.
   *
   * @see stdTimeFunctions for a set of available functions for passing in as a value for this option.
   *
   * NOTE: any sort of formatted time will significantly slow down Pino's performance.
   */
  timestamp?: TimeFn | boolean;

  /**
   * Outputs the level as a string instead of integer.
   *
   * @default: `false`.
   */
  useLevelLabels?: boolean;

  /**
   * Use this option to only use defined `customLevels` and omit Pino's levels.
   *
   * Logger's default `level` must be changed to a value in `customLevels` in
   * order to use `useOnlyCustomLevels`
   *
   * NOTE: this option may not be supported by downstream transports.
   */
  useOnlyCustomLevels?: boolean;

  /**
   * Browser only, see http://getpino.io/#/docs/browser.
   */
  browser?: {
    /**
     * The `asObject` option will create a pino-like log object instead of
     * passing all arguments to a console method. When `write` is set, `asObject`
     * will always be true.
     *
     * @example
     * pino.info('hi') // creates and logs {msg: 'hi', level: 30, time: <ts>}
     */
    asObject?: boolean;

    /**
     * Instead of passing log messages to `console.log` they can be passed to a supplied function. If `write` is
     * set to a single function, all logging objects are passed to this function. If `write` is an object, it
     * can have methods that correspond to the levels. When a message is logged at a given level, the
     * corresponding method is called. If a method isn't present, the logging falls back to using the `console`.
     *
     * @example
     * const pino = require('pino')({
     *   browser: {
     *     write: (o) => {
     *       // do something with o
     *     }
     *   }
     * })
     *
     * @example
     * const pino = require('pino')({
     *   browser: {
     *     write: {
     *       info: function (o) {
     *         //process info log object
     *       },
     *       error: function (o) {
     *         //process error log object
     *       }
     *     }
     *   }
     * })
     */
    write?:
      | WriteFn
      | ({
          fatal?: WriteFn;
          error?: WriteFn;
          warn?: WriteFn;
          info?: WriteFn;
          debug?: WriteFn;
          trace?: WriteFn;
        } & { [logLevel: string]: WriteFn });

    /**
     * The serializers provided to `pino` are ignored by default in the browser, including the standard
     * serializers provided with Pino. Since the default destination for log messages is the console, values
     * such as `Error` objects are enhanced for inspection, which they otherwise wouldn't be if the Error
     * serializer was enabled. We can turn all serializers on or we can selectively enable them via an array.
     *
     * When `serialize` is `true` the standard error serializer is also enabled (see
     * {@link https://github.com/pinojs/pino/blob/master/docs/api.md#pino-stdserializers}). This is a global
     * serializer which will apply to any `Error` objects passed to the logger methods.
     *
     * If `serialize` is an array the standard error serializer is also automatically enabled, it can be
     * explicitly disabled by including a string in the serialize array: `!stdSerializers.err` (see example).
     *
     * The `serialize` array also applies to any child logger serializers (see
     * {@link https://github.com/pinojs/pino/blob/master/docs/api.md#bindingsserializers-object} for how to
     * set child-bound serializers).
     *
     * Unlike server pino the serializers apply to every object passed to the logger method, if the `asObject`
     * option is `true`, this results in the serializers applying to the first object (as in server pino).
     *
     * For more info on serializers see
     * {@link https://github.com/pinojs/pino/blob/master/docs/api.md#serializers-object}.
     *
     * @example
     * const pino = require('pino')({
     *   browser: {
     *     serialize: true
     *   }
     * })
     *
     * @example
     * const pino = require('pino')({
     *   serializers: {
     *     custom: myCustomSerializer,
     *     another: anotherSerializer
     *   },
     *   browser: {
     *     serialize: ['custom']
     *   }
     * })
     * // following will apply myCustomSerializer to the custom property,
     * // but will not apply anotherSerializer to another key
     * pino.info({custom: 'a', another: 'b'})
     *
     * @example
     * const pino = require('pino')({
     *   serializers: {
     *     custom: myCustomSerializer,
     *     another: anotherSerializer
     *   },
     *   browser: {
     *     serialize: ['!stdSerializers.err', 'custom'] //will not serialize Errors, will serialize `custom` keys
     *   }
     * })
     */
    serialize?: boolean | string[];

    /**
     * Options for transmission of logs.
     *
     * @example
     * const pino = require('pino')({
     *   browser: {
     *     transmit: {
     *       level: 'warn',
     *       send: function (level, logEvent) {
     *         if (level === 'warn') {
     *           // maybe send the logEvent to a separate endpoint
     *           // or maybe analyse the messages further before sending
     *         }
     *         // we could also use the `logEvent.level.value` property to determine
     *         // numerical value
     *         if (logEvent.level.value >= 50) { // covers error and fatal
     *
     *           // send the logEvent somewhere
     *         }
     *       }
     *     }
     *   }
     * })
     */
    transmit?: {
      /**
       * Specifies the minimum level (inclusive) of when the `send` function should be called, if not supplied
       * the `send` function will be called based on the main logging `level` (set via `options.level`,
       * defaulting to `info`).
       */
      level?: Level | string;

      /**
       * Remotely record log messages.
       *
       * @description Called after writing the log message.
       */
      send: (level: Level, logEvent: ILogEvent) => void;
    };
  };

  /**
   * An object containing functions for formatting the shape of the log lines.
   * These functions should return a JSONifiable object and should never throw.
   * These functions allow for full customization of the resulting log lines.
   * For example, they can be used to change the level key name or to enrich the default metadata.
   */
  formatters?: {
    /**
     * Changes the shape of the bindings.
     * The default shape is { pid, hostname }.
     * The function takes a single argument, the bindings object.
     * It will be called every time a child logger is created.
     */
    bindings?: (bindings: IBindings) => Record<string, unknown>;

    /**
     * Changes the shape of the log level.
     * The default shape is { level: number }.
     * The function takes two arguments, the label of the level (e.g. 'info') and the numeric value (e.g. 30).
     */
    level?: (level: string, number: number) => Record<string, unknown>;

    /**
     * Changes the shape of the log object.
     * This function will be called every time one of the log methods (such as .info) is called.
     * All arguments passed to the log method, except the message, will be pass to this function.
     * By default it does not change the shape of the log object.
     */
    log?: (object: Record<string, unknown>) => Record<string, unknown>;
  };

  /**
   * An object mapping to hook functions. Hook functions allow for customizing internal logger operations.
   * Hook functions must be synchronous functions.
   */
  hooks?: {
    /**
     * Allows for manipulating the parameters passed to logger methods. The signature for this hook is
     * logMethod (args, method) {}, where args is an array of the arguments that were passed to the
     * log method and method is the log method itself. This hook must invoke the method function by
     * using apply, like so: method.apply(this, newArgumentsArray).
     */
    logMethod?: (args: unknown[], method: ILogFn) => void;
  };
}
