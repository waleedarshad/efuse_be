import { Transform } from "stream";

export class StreamParser {
  public static transformer = (): Transform =>
    new Transform({
      objectMode: true,
      transform(chunk, encoding, cb) {
        try {
          const data = JSON.parse(chunk.toString("utf8"));
          return cb(null, data);
        } catch (error) {
          return cb(error);
        }
      }
    });
}
