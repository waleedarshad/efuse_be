import { Stream } from "stream";

export function* streamToGenerator(stream: Stream): Generator<Promise<unknown>, void, unknown> {
  while (true) {
    try {
      const res = yield Promise.race([
        new Promise((resolve) => stream.once("data", resolve)),
        new Promise((resolve) => stream.once("end", resolve)),
        new Promise((resolve, reject) => stream.once("error", reject))
      ]);

      if (!res) {
        break;
      }
    } catch (error) {
      yield new Promise((resolve, reject) => reject(error));
    }
  }
}
