export const epochTime = (): string => `,"time":${Date.now()}`;
export const isoTime = (): string => `,"time":"${new Date(Date.now()).toISOString()}"`; // using Date.now() for testability
export const nullTime = (): string => "";
export const unixTime = (): string => `,"time":${Math.round(Date.now() / 1000)}`;
