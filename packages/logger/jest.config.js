/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  name: "@efuse/logger",
  displayName: "@efuse/logger",
  collectCoverageFrom: ["./src/**/*.{j,t}s"]
};
