export { Logger } from "./src/logger";
export { LoggerOptions } from "./src/options";
export type { Level, LevelWithSilent, MixinFn, SerializerFn, TimeFn, WriteFn } from "./src/types";
export type {
  IBindings,
  ILogEvent,
  ILogger,
  ILogFn,
  ILoggerOptions,
  IPrettyOptions,
  IRedactOptions
} from "./src/interfaces";
