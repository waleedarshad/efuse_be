import axios, { AxiosInstance, AxiosResponse } from "axios";
import { BRAZE_API_KEY } from "@efuse/key-store";

import { ICampaignOptions, ICampaignResponse } from "./interfaces";

const BASE_URL = "https://rest.iad-06.braze.com";

export class Braze {
  private $http: AxiosInstance;

  constructor(baseURL?: string) {
    this.$http = axios.create({
      baseURL: baseURL || BASE_URL,
      headers: { Authorization: `Bearer ${BRAZE_API_KEY}`, "Content-Type": "application/json" }
    });
  }

  /**
   * @summary Sending Campaign Messages via API Triggered Delivery
   *
   * @param {ICampaignOptions} options
   *
   * @returns {Promise<AxiosResponse<ICampaignResponse>>}
   *
   * @link https://www.braze.com/docs/api/endpoints/messaging/send_messages/post_send_triggered_campaigns/
   */
  public async sendCampaignMessage(options: ICampaignOptions): Promise<AxiosResponse<ICampaignResponse>> {
    const response: AxiosResponse<ICampaignResponse> = await this.$http.post(
      "/campaigns/trigger/send",
      JSON.stringify({
        ...options,
        broadcast: options.broadcast || false
      })
    );
    return response;
  }
}
