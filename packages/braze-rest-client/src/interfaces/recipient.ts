import { IUserAlias } from "./user-alias";
import { IUserAttributes } from "./user-attributes";

export interface IRecipient {
  /**
   * @optional
   * Either "external_user_id" or "user_alias" is required. Requests must specify only one
   * User alias of user to receive message
   */
  user_alias?: IUserAlias;

  /**
   * @optional
   * Either "external_user_id" or "user_alias" is required. Requests must specify only one
   * External identifier of user to receive message
   */
  external_user_id?: string;

  /**
   * @optional
   * Personalization key value pairs that will apply to this user (these key value pairs will override any keys that conflict with trigger_properties in root object)
   */
  trigger_properties?: Record<string, unknown>;

  /**
   * @optional
   * If set to `false`, an attributes object must also be included
   *
   * @default true
   */
  send_to_existing_only?: boolean;

  /**
   * @optional
   * Fields in the attributes object will create or update an attribute of that name with the given value on the specified user profile before the message is sent and existing values will be overwritten
   */
  attributes?: IUserAttributes;
}
