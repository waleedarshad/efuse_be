export type { ICampaignOptions } from "./campaign-options";
export type { ICampaignResponse } from "./campaign-response";
export type { IRecipient } from "./recipient";
export type { IUserAttributes } from "./user-attributes";
