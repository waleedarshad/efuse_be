export interface IUserAlias {
  /**
   * @required
   * Name for the identifier itself
   */
  alias_name: string;

  /**
   * @required
   * Indicating the type of alias
   */
  alias_label: string;
}
