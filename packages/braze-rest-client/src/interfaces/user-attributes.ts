import { IUserAlias } from "./user-alias";

export interface IUserAttributes {
  /**
   * @optional
   * One of "external_id" or "user_alias" or "braze_id" is required
   */
  external_id?: string;

  /**
   * @optional
   * One of "external_id" or "user_alias" or "braze_id" is required
   */
  user_alias?: IUserAlias;

  /**
   * @optional
   * One of "external_id" or "user_alias" or "braze_id" is required
   */
  braze_id?: string;

  /**
   * @optional
   * Setting this flag to true will put the API in "Update Only" mode.
   */
  _update_existing_only?: boolean;

  /**
   * @optional
   */
  push_token_import?: boolean;
}
