import { IRecipient } from "./recipient";

export interface ICampaignOptions {
  /**
   * @required
   * Campaign Identifier
   */
  campaign_id: string;

  /**
   * @optional
   * Send Identifier
   */
  send_id?: string;

  /**
   * @optional
   * Personalization key value pairs that will apply to all users in this request
   */
  trigger_properties?: Record<string, unknown>;

  /**
   * @optional
   * Must be set to true if "recipients" is omitted
   *
   * @default false
   */
  broadcast?: boolean;

  /**
   * @optional
   * Including 'audience' will only send to users in the audience
   */
  audience?: Record<string, unknown>;

  /**
   * @optional
   * If not provided and broadcast is not set to `false`, message will send to the entire segment targeted by the campaign
   */
  recipients?: IRecipient[];
}
