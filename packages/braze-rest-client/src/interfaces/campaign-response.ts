export interface ICampaignResponse {
  /**
   * The id of the message dispatch (unique id for each ‘transmission’ sent from the Braze platform)
   */
  dispatch_id: string;
  success: boolean;
}
