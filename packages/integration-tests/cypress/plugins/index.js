/// <reference types="cypress" />
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)

const { MongoClient } = require("mongodb");
// or as an es module:
// import { MongoClient } from 'mongodb'

// Connection URL
// NOTE:  We have mongodb running on 27018 locally for integration testing in docker-compose
const url = "mongodb://localhost:27018";
const client = new MongoClient(url);

/**
 * @type {Cypress.PluginConfig}
 */
module.exports = (on, config) => {
  on("task", {
    async "db:seed"(data) {
      await client.connect();

      const database = client.db("integration");

      // Drop it like its hot
      await database.dropDatabase();

      for (const collection_name of Object.keys(data.collections)) {
        const collection = database.collection(collection_name);
        await collection.insertMany(data.collections[collection_name]);
      }
      return {};
    }
  });
  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
};
