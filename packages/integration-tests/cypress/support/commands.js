/* eslint-disable no-undef */

// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

Cypress.Commands.add("seed_mongodb", (seedName) => {
  cy.fixture(seedName).then(async function (data) {
    // NOTE:  All of the cy.commands are run in the context of the browser running the tests.
    // As such we can't do things like open mongodb / seed the database with the nodejs mongodb
    // drivers because they just don't work in browser.  Instead we have to create cypress tasks / plugins
    // which are run in the cypress nodejs proceses to do this type of work.  Took a long time for me (shuff)
    // to figure out.
    await cy.task("db:seed", data);
  });
});

/* eslint-enable no-undef */
