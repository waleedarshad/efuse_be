import tracer, { Scope, Span, SpanContext, SpanOptions, TraceOptions, Tracer, TracerOptions } from "dd-trace";

export class EFTracer {
  private $tracer: Tracer;

  static $instance: EFTracer;

  private constructor(options: TracerOptions) {
    this.$tracer = tracer.init(options);
  }

  /**
   * Initializes the tracer. This should be called before importing other libraries.
   */
  public static init(options: TracerOptions): EFTracer {
    if (!this.$instance) {
      this.$instance = new EFTracer(options);
    }

    return this.$instance;
  }

  /**
   * Returns a SpanContext instance extracted from `carrier` in the given
   * `format`.
   * @param  {string} format The format of the carrier.
   * @param  {any} carrier The carrier object.
   * @return {SpanContext}
   *         The extracted SpanContext, or null if no such SpanContext could
   *         be found in `carrier`
   */
  public extract(format: string, carrier: any): SpanContext | null {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.extract(format, carrier);
  }

  /**
   * Create and return a string that can be included in the <head> of a
   * document to enable RUM tracing to include it. The resulting string
   * should not be cached.
   */
  public getRumData(): string {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.getRumData();
  }

  /**
   * Injects the given SpanContext instance for cross-process propagation
   * within `carrier`
   * @param  {SpanContext} spanContext The SpanContext to inject into the
   *         carrier object. As a convenience, a Span instance may be passed
   *         in instead (in which case its .context() is used for the
   *         inject()).
   * @param  {string} format The format of the carrier.
   * @param  {any} carrier The carrier object.
   */
  public inject(spanContext: SpanContext | Span, format: string, carrier: any): void {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.inject(spanContext, format, carrier);
  }

  /**
   * Returns a reference to the current scope.
   */
  public scope(): Scope {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.scope();
  }

  /**
   * Sets the URL for the trace agent. This should only be called _after_
   * init() is called, only in cases where the URL needs to be set after
   * initialization.
   */
  public setUrl(url: string): this {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    this.$tracer.setUrl(url);

    return this;
  }

  /**
   * Starts and returns a new Span representing a logical unit of work.
   * @param {string} name The name of the operation.
   * @param {SpanOptions} [options] Options for the newly created span.
   * @returns {Span} A new Span object.
   */
  public startSpan(name: string, options?: SpanOptions): Span {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.startSpan(name, options);
  }

  /**
   * Instruments a function by automatically creating a span activated on its
   * scope.
   *
   * The span will automatically be finished when one of these conditions is
   * met:
   *
   * * The function returns a promise, in which case the span will finish when
   * the promise is resolved or rejected.
   * * The function takes a callback as its second parameter, in which case the
   * span will finish when that callback is called.
   * * The function doesn't accept a callback and doesn't return a promise, in
   * which case the span will finish at the end of the function execution.
   *
   * If the `orphanable` option is set to false, the function will not be traced
   * unless there is already an active span or `childOf` option.
   */
  public trace<T>(
    name: string,
    options: TraceOptions & SpanOptions,
    fn: (span?: Span, done?: (error?: Error) => string) => T
  ): T {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.trace(name, options, fn);
  }

  /**
   * Enable and optionally configure a plugin.
   * @param plugin The name of a built-in plugin.
   * @param config Configuration options. Can also be `false` to disable the plugin.
   */
  public use<P extends keyof any>(plugin: P, config?: any | boolean): this {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    this.$tracer.use(<never>plugin, config);

    return this;
  }

  /**
   * Wrap a function to automatically create a span activated on its
   * scope when it's called.
   *
   * The span will automatically be finished when one of these conditions is
   * met:
   *
   * * The function returns a promise, in which case the span will finish when
   * the promise is resolved or rejected.
   * * The function takes a callback as its last parameter, in which case the
   * span will finish when that callback is called.
   * * The function doesn't accept a callback and doesn't return a promise, in
   * which case the span will finish at the end of the function execution.
   */
  public wrap<T = (...args: any[]) => any>(name: string, fn: T, requiresParent?: boolean): T;
  public wrap<T = (...args: any[]) => any>(
    name: string,
    options: ((...args: any[]) => TraceOptions | TraceOptions) & SpanOptions,
    fn: T
  ): T {
    if (!this.$tracer) {
      throw new Error("EFTracer must be initialized before use");
    }

    return this.$tracer.wrap(name, options, fn);
  }
}
