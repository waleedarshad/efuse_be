import OAuth2Strategy from "passport-oauth2";

interface StateSpaceStrategyOptions {
  /**
   * @optional
   *
   * URL used to obtain an authorization grant from State Space
   *
   * @default 'https://auth.aimlab.gg/oauth/auth'
   */
  authorizationURL?: string;
  /**
   * @required
   * URL to which State Space will redirect the user after granting authorization
   */
  callbackURL: string;
  /**
   * @required
   * Application's client ID.
   */
  clientID: string;
  /**
   * @required
   * Application's client secret.
   */
  clientSecret: string;
  /**
   * @optional
   * Determines whether `req` is passed as the first argument to the verify callback
   * triggered when State Space redirects to the `callbackURL`.
   *
   * @default false
   */
  passReqToCallback?: boolean;

  /**
   * @optional
   *
   * An array of State Space OAuth2 scopes. May also be a string of scopes separated by `scopeSeparator`.
   * Valid scopes include:
   *  - `'openid'`
   *  - `'offline_access'`
   *
   * @default []
   */
  scope?: string | string[];
  /**
   * @optional
   *
   * String delimiter for scopes in `scopes`.
   *
   * @default ' '
   */
  scopeSeparator?: string;
  /**
   * @optional
   *
   * URL used to obtain an access token from State Space
   *
   * @default 'https://auth.aimlab.gg/oauth/token'
   */
  tokenURL?: string;
}

const ENDPOINT = "https://auth.aimlab.gg/oauth";

export class Strategy extends OAuth2Strategy {
  public name = "state-space";

  public _clientSecret: string | undefined;

  public _scope: string[];

  constructor(options: StateSpaceStrategyOptions, verify: OAuth2Strategy.VerifyFunction) {
    super(<any>buildOptions(options), verify);

    const { clientSecret, scope } = buildOptions(options);

    this._clientSecret = clientSecret;
    this._scope = scope as string[];
  }
}

function buildOptions(options: StateSpaceStrategyOptions): StateSpaceStrategyOptions {
  const scopesStringOrArray = options.scope || ["openid", "offline_access"];
  const scopeSeparator = options.scopeSeparator || " ";
  const scopes =
    typeof scopesStringOrArray === "string" ? scopesStringOrArray.split(scopeSeparator) : scopesStringOrArray;

  return {
    ...options,
    authorizationURL: options.authorizationURL || `${ENDPOINT}/auth`,
    scope: scopes,
    scopeSeparator: options.scopeSeparator || " ",
    tokenURL: options.tokenURL || `${ENDPOINT}/token`
  };
}
