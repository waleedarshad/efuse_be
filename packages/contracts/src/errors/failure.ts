import createError from "http-errors";

import { LogLevel } from "../enums/log-level";
import { IApiResponse } from "../interfaces/api-response";

/**
 * Models a HTTP Failure
 *
 * @see https://efuse.atlassian.net/l/c/91fjYveC
 * @class Failure
 * @extends {Error}
 * @implements {IApiResponse}
 */
export class Failure extends Error implements IApiResponse {
  public code: number;
  public data: unknown;
  public headers?: { [key: string]: string } | undefined;
  public level: LogLevel = LogLevel.info;
  public message: string;
  public stack?: string;
  public status: number;
  public statusCode: number;
  public success: boolean;

  private constructor(name?: string, code?: number, message?: string, data?: unknown) {
    super();

    this.code = code || 500;
    this.data = data || {};
    this.message = message || "ERROR";
    this.name = name || "Error";
    this.status = this.code;
    this.statusCode = this.code;
    this.success = false;
  }

  /**
   * 502 Bad Gateway
   *
   * Load Balancer could not find a suitable server to service request.
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static BadGateway(message?: string, data?: unknown): Failure {
    return this.merge("BadGatewayError", 502, message, data);
  }

  /**
   * 400 Bad Request
   *
   * Client sent a request that had missing or data that wasn’t valid for the
   * requested action
   *
   * e.g. tried to create a new opportunity and is missing a required field.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static BadRequest(message?: string, data?: unknown): Failure {
    return this.merge("BadRequestError", 400, message, data);
  }

  /**
   * 509 Bandwidth Exceeded
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static BandwidthLimitExceeded(message?: string, data?: unknown): Failure {
    return this.merge("BandwithLimitExceededError", 509, message, data);
  }

  /**
   * 409 Conflict
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static Conflict(message?: string, data?: unknown): Failure {
    return this.merge("ConflictError", 409, message, data);
  }

  /**
   * 417 Expectation Failed
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static ExpectationFailed(message?: string, data?: unknown): Failure {
    return this.merge("ExpectationFailedError", 417, message, data);
  }

  /**
   * 424 Failed Dependency
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static FailedDependency(message?: string, data?: unknown): Failure {
    return this.merge("FailedDependencyError", 424, message, data);
  }

  /**
   * 403 Forbidden
   *
   * Client is trying to make a request that it is not authorized to do.
   *
   * e.g. a user is trying to make a change to an organizations that they
   * don’t have access to modify.
   *
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static Forbidden(message?: string, data?: unknown): Failure {
    return this.merge("ForbiddenError", 403, message, data);
  }

  /**
   * 504 Gateway Timeout
   *
   * Server timed out servicing a request
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static GatewayTimeout(message?: string, data?: unknown): Failure {
    return this.merge("GatewayTimeoutError", 504, message, data);
  }

  /**
   * 410 Gone
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static Gone(message?: string, data?: unknown): Failure {
    return this.merge("GoneError", 410, message, data);
  }

  /**
   * 505 HTTP Version Not Supported
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static HTTPVersionNotSupported(message?: string, data?: unknown): Failure {
    return this.merge("HTTPVersionNotSupportedError", 505, message, data);
  }

  /**
   * 507 Insufficient Storage
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static InsufficientStorage(message?: string, data?: unknown): Failure {
    return this.merge("InsufficientStorageError", 507, message, data);
  }

  /**
   * 500 Internal Server Error
   *
   * The server hit an unhandled exception.
   *
   * This should never be explicitly sent to a client in a request handler
   * that didn’t hit an unhandled exception.
   *
   * Those should be in the 4XX range.
   *
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static InternalServerError(message?: string, data?: unknown): Failure {
    return this.merge("InternalServerErrorError", 500, message, data);
  }

  /**
   * 411 Length Required
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static LengthRequired(message?: string, data?: unknown): Failure {
    return this.merge("LengthRequiredError", 411, message, data);
  }

  /**
   * 423 Locked
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static Locked(message?: string, data?: unknown): Failure {
    return this.merge("LockedError", 423, message, data);
  }

  /**
   * 508 Loop Detected
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static LoopDetected(message?: string, data?: unknown): Failure {
    return this.merge("LoopDetectedError", 508, message, data);
  }

  /**
   * 405 Method Not Allowed
   *
   * Someone tried to send the wrong request type to endpoint.
   * e.g. tried to send POST to an endpoint that only support PUT.
   *
   * @note Generally never explicitly sent to client.
   *       Handled by express router.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static MethodNotAllowed(message?: string, data?: unknown): Failure {
    return this.merge("MethodNotAllowedError", 405, message, data);
  }

  /**
   * 421 Misdirected Request
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static MisdirectedRequest(message?: string, data?: unknown): Failure {
    return this.merge("MisdirectedRequestError", 421, message, data);
  }

  /**
   * 511 Network Authentication Required
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static NetworkAuthenticationRequired(message?: string, data?: unknown): Failure {
    return this.merge("NetworkAuthenticationRequiredError", 511, message, data);
  }

  /**
   * 405 Not Acceptable
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static NotAcceptable(message?: string, data?: unknown): Failure {
    return this.merge("NotAcceptableError", 406, message, data);
  }

  /**
   * 510 Not Extended
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static NotExtended(message?: string, data?: unknown): Failure {
    return this.merge("NotExtendedError", 510, message, data);
  }

  /**
   * 404 Not Found
   *
   * Resource not found.
   *
   * This should not be returned on a properly handled request.
   * If entity does not exist in the database, send a 422 instead.
   *
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static NotFound(message?: string, data?: unknown): Failure {
    return this.merge("NotFoundError", 404, message, data);
  }

  /**
   * 501 Not Implemented
   *
   * @note Never sent by eFuse applications in production.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static NotImplemented(message?: string, data?: unknown): Failure {
    return this.merge("NotImplementedError", 501, message, data);
  }

  /**
   * 413 Payload Too Large
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static PayloadTooLarge(message?: string, data?: unknown): Failure {
    return this.merge("PayloadTooLargeError", 413, message, data);
  }

  /**
   *
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static PaymentRequired(message?: string, data?: unknown): Failure {
    return this.merge("PaymentRequiredError", 402, message, data);
  }

  /**
   * 412 Precondition Failed
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static PreconditionFailed(message?: string, data?: unknown): Failure {
    return this.merge("PreconditionFailedError", 412, message, data);
  }

  /**
   * 428 Precondition Required
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static PreconditionRequired(message?: string, data?: unknown): Failure {
    return this.merge("PreconditionRequiredError", 428, message, data);
  }

  /**
   * 407 Proxy Authentication Required
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static ProxyAuthenticationRequired(message?: string, data?: unknown): Failure {
    return this.merge("ProxyAuthenticationRequiredError", 407, message, data);
  }

  /**
   * 416 Range Not Satisfiable
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static RangeNotSatisfiable(message?: string, data?: unknown): Failure {
    return this.merge("RangeNotSatisfiableError", 416, message, data);
  }

  /**
   * 431 Request Header Fields Too Large
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static RequestHeaderFieldsTooLarge(message?: string, data?: unknown): Failure {
    return this.merge("RequestHeaderFieldsTooLargeError", 431, message, data);
  }

  /**
   * 408 Request Timeout
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static RequestTimeout(message?: string, data?: unknown): Failure {
    return this.merge("RequestTimeoutError", 408, message, data);
  }

  /**
   * 503 Service Unavailable
   *
   * Load Balancer could not find a suitable server to service request.
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static ServiceUnavailable(message?: string, data?: unknown): Failure {
    return this.merge("ServiceUnavailableError", 503, message, data);
  }

  /**
   * 429 Too Many Requests
   *
   * Exceeded imposed rate limits.
   *
   * This should be handled exclusively by rate limiting middleware.
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static TooManyRequests(message?: string, data?: unknown): Failure {
    return this.merge("TooManyRequestsError", 429, message, data);
  }

  /**
   * 401 Unauthorized
   *
   * Used for endpoints where user must be logged in and has missing/invalid
   * Bearer Token, also used when user has password.
   *
   * Never send a 401 except in our middleware validating JWT tokens and on
   * password validation.
   *
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static Unauthorized(message?: string, data?: unknown): Failure {
    return this.merge("UnauthorizedError", 401, message, data);
  }

  /**
   * 451 Unavailable For Legal Reasons
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static UnavailableForLegalReasons(message?: string, data?: unknown): Failure {
    return this.merge("UnavailableForLegalReasonsError", 451, message, data);
  }

  /**
   * 425 Unordered Collection
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static UnorderedCollection(message?: string, data?: unknown): Failure {
    return this.merge("UnorderedCollectionError", 425, message, data);
  }

  /**
   * 422 Unprocessable Entity
   *
   * Request for an resource that does not exist.
   *
   * e.g. a request for a specific opportunity ID that doesn’t exist
   * in the database should send a 422 (not a 404)
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static UnprocessableEntity(message?: string, data?: unknown): Failure {
    return this.merge("UnprocessableEntityError", 422, message, data);
  }

  /**
   * 415 Unsupported Media Type Error
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static UnsupportedMediaType(message?: string, data?: unknown): Failure {
    return this.merge("UnsupportedMediaTypeError", 415, message, data);
  }

  /**
   * 426 Upgrade Required
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static UpgradeRequired(message?: string, data?: unknown): Failure {
    return this.merge("UpgradeRequiredError", 426, message, data);
  }

  /**
   * 414 Failure
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static URITooLong(message?: string, data?: unknown): Failure {
    return this.merge("URITooLongError", 414, message, data);
  }

  /**
   * 506 Variant Also Negotiates
   *
   * @note Never sent by eFuse applications.
   * @param {string} [message]
   * @param {unknown} [data]
   * @return {Failure}
   */
  public static VariantAlsoNegotiates(message?: string, data?: unknown): Failure {
    return this.merge("VariantAlsoNegotiatesError", 506, message, data);
  }

  private static merge(name: string, code: number, message?: string, data?: unknown): Failure {
    const failure = new Failure(name, code, message, data);
    const error = createError(code, failure);

    return Object.assign(error, failure);
  }
}
