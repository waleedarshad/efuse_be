export enum AimlabRankTier {
  BRONZE,
  DIAMOND,
  EMERALD,
  GOLD,
  GRANDMASTER,
  MASTER,
  PLATINUM,
  RUBY,
  SILVER
}
