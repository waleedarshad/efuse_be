export enum LogLevel {
  info = "info",
  error = "error",
  warn = "warn",
  fatal = "fatal"
}
