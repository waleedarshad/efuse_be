import { NextFunction, Request, Response } from "express";

export interface IRestController {
  delete(req: Request, res: Response, next: NextFunction): Promise<void>;
  get(req: Request, res: Response, next: NextFunction): Promise<void>;
  list(req: Request, res: Response, next: NextFunction): Promise<void>;
  patch(req: Request, res: Response, next: NextFunction): Promise<void>;
  post(req: Request, res: Response, next: NextFunction): Promise<void>;
  put(req: Request, res: Response, next: NextFunction): Promise<void>;
}
