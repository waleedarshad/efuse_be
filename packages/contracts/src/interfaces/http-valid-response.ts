export interface IHttpValidResponse<T> {
  count: number;
  items: T[];
}
