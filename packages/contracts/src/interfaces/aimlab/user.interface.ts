import { IAimlabPlay } from "./play.interface";
import { IAimlabProfile } from "./profile.interface";

export interface IAimlabUser {
  aimlabProfile: IAimlabProfile;
  id: string;
  playfabID: string;
  plays: IAimlabPlay[];
}
