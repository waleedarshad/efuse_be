export interface IAimlabSkillScore {
  name: string;
  score: number;
}
