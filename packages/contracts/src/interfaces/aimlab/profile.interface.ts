import { Types } from "mongoose";

import { IAimlabRanking } from "./ranking.interface";
import { IAimlabSkillScore } from "./skill-score.interface";
import { IAimlabTaskStats } from "./task-stats.interface";
import { IAimlabUser } from "./user.interface";

export interface IAimlabProfile {
  ranking: IAimlabRanking;
  skillScores: IAimlabSkillScore[];
  taskStats: IAimlabTaskStats[];
  owner: Types.ObjectId;
  user: IAimlabUser;
  username: string;
}
