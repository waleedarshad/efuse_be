import { IAimlabRank } from "./rank.interface";

export interface IAimlab {
  ranks: IAimlabRank[];
}
