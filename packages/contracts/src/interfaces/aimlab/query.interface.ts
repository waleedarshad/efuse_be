import { IAimlab } from "./aimlab.interface";
import { IAimlabProfile } from "./profile.interface";
import { IAimlabUser } from "./user.interface";

export interface IAimlabQuery {
  aimlab: IAimlab;
  aimlabProfile: IAimlabProfile;
  viewer: IAimlabUser;
}
