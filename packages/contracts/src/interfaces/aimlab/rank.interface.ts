import { AimlabRankTier } from "../../enums";

export interface IAimlabRank {
  displayName: string;
  level: number;
  maxSkill: number;
  minSkill: number;
  tier: AimlabRankTier;
}
