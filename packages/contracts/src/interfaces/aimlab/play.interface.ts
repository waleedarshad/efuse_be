import { IAimlabAppVersion } from "./app-version.interface";

export interface IAimlabPlay {
  createdAt: string;
  endedAt: string;
  mode: number;
  performanceScores: unknown;
  playId: string;
  rawDataUrl: string;
  score: number;
  startedAt: string;
  taskSlug: string;
  version: IAimlabAppVersion;
  weaponName: string;
  weaponType: string;
}
