import { IAimlabRank } from "./rank.interface";

export interface IAimlabRanking {
  badgeImage: string;
  rank: IAimlabRank;
  skill: number;
}
