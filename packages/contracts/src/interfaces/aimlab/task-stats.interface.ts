import { IAimlabTask } from "./task.interface";

export interface IAimlabTaskStats {
  averageScore: number;
  playCount: number;
  task: IAimlabTask;
  topScore: number;
}
