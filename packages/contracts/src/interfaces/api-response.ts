export interface IApiResponse {
  code: number;
  data?: unknown;
  message: string;
  success: boolean;
}
