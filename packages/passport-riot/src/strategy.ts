import OAuth2Strategy from "passport-oauth2";
import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";

import {
  IRiotStrategyOptions,
  IUserInfoResponse,
  IErrorResponse,
  IUserInfo,
  IValorantProfile,
  ILOLProfile,
  IError,
  IRiotProfile,
  IValorantProfileResponse,
  ILOLProfileResponse
} from "./interfaces";

const ENDPOINT = "https://auth.riotgames.com";
const VALORANT_REGION = "americas";
const VALORANT_PROFILE_ENDPOINT = "/riot/account/v1/accounts/me";
const LOL_PROFILE_ENDPOINT = "/lol/summoner/v4/summoners/me";

export class Strategy extends OAuth2Strategy {
  public name = "riot";

  public _clientSecret: string | undefined;

  public _scope: string[];

  private profileURL: string;

  private $http: AxiosInstance = axios;

  constructor(options: IRiotStrategyOptions, verify: OAuth2Strategy.VerifyFunction) {
    super(<any>buildOptions(options), verify);

    const { clientSecret, scope, profileURL } = buildOptions(options);

    this._clientSecret = clientSecret;
    this._scope = scope as string[];
    this.profileURL = profileURL;
  }

  public async userProfile(
    accessToken: string,
    done: (err: Error | null, profile: IRiotProfile) => void
  ): Promise<void> {
    const requestOptions: AxiosRequestConfig = {
      headers: { Authorization: `Bearer ${accessToken}`, "Content-Type": "application/json" }
    };

    // Fetch userInfo, This will provide us cpid
    let userInfo: IUserInfo;

    try {
      const userInfoResponse: AxiosResponse<IUserInfoResponse> = await this.$http.get(this.profileURL, requestOptions);
      const { data } = userInfoResponse;

      userInfo = { success: true, cpid: data.cpid, sub: data.sub, jti: data.jti };
    } catch (error: unknown) {
      const errorObject: AxiosError<""> = <AxiosError<"">>error;
      userInfo = {
        success: false,
        error: { code: errorObject.response?.status, message: errorObject.message || "Error fetching userInfo" }
      };
    }

    // Fetch valorant profile
    let valorantProfile: IValorantProfile;

    try {
      const valorantProfileResponse: AxiosResponse<IValorantProfileResponse> = await this.$http.get(
        `${this.riotAPIEndpoint(VALORANT_REGION)}${VALORANT_PROFILE_ENDPOINT}`,
        requestOptions
      );
      const { data } = valorantProfileResponse;

      valorantProfile = { success: true, ...data };
    } catch (error: unknown) {
      const errorObject: AxiosError<IErrorResponse> = <AxiosError<IErrorResponse>>error;

      valorantProfile = {
        success: false,
        error: this.buildError(errorObject.response?.data, "Error fetching valorant profile")
      };
    }

    // Fetch LOL profile
    let lolProfile: ILOLProfile;

    try {
      if (userInfo.cpid) {
        const lolProfileResponse: AxiosResponse<ILOLProfileResponse> = await this.$http.get(
          `${this.riotAPIEndpoint(userInfo.cpid)}${LOL_PROFILE_ENDPOINT}`,
          requestOptions
        );
        const { data } = lolProfileResponse;

        lolProfile = { success: true, ...data };
      } else {
        lolProfile = { success: false, error: { message: "CPID not found" } };
      }
    } catch (error: unknown) {
      const errorObject: AxiosError<IErrorResponse> = <AxiosError<IErrorResponse>>error;

      lolProfile = {
        success: false,
        error: this.buildError(errorObject.response?.data, "Error fetching LOL profile")
      };
    }

    const riotProfile = { userInfo, valorantProfile, lolProfile };
    return done(null, riotProfile);
  }

  private riotAPIEndpoint = (region: string): string => `https://${region.toLowerCase()}.api.riotgames.com`;

  private buildError = (errorObject: IErrorResponse | undefined, fallbackMessage: string): IError => {
    const receivedError = errorObject?.status;

    return {
      code: receivedError?.status_code,
      message: receivedError?.message || fallbackMessage
    };
  };
}

function buildOptions(options: IRiotStrategyOptions): IRiotStrategyOptions {
  const scopesStringOrArray = options.scope || ["openid", "cpid", "offline_access"];
  const scopeSeparator = options.scopeSeparator || " ";
  const scopes =
    typeof scopesStringOrArray === "string" ? scopesStringOrArray.split(scopeSeparator) : scopesStringOrArray;

  return {
    ...options,
    authorizationURL: options.authorizationURL || `${ENDPOINT}/authorize`,
    scope: scopes,
    scopeSeparator: options.scopeSeparator || " ",
    tokenURL: options.tokenURL || `${ENDPOINT}/token`,
    profileURL: options.profileURL || `${ENDPOINT}/userinfo`
  };
}
