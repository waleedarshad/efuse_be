import { IError } from "./error";
import { IValorantProfileResponse } from "./valorant-profile-response";

export interface IValorantProfile extends Partial<IValorantProfileResponse> {
  success: boolean;
  error?: IError;
}
