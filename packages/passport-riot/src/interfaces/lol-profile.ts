import { IError } from "./error";
import { ILOLProfileResponse } from "./lol-profile-response";

export interface ILOLProfile extends Partial<ILOLProfileResponse> {
  success: boolean;
  error?: IError;
}
