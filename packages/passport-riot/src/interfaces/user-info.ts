import { IError } from "./error";
import { IUserInfoResponse } from "./user-info-response";

export interface IUserInfo extends Partial<IUserInfoResponse> {
  success: boolean;
  error?: IError;
}
