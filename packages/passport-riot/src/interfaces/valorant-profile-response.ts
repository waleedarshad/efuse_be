export interface IValorantProfileResponse {
  puuid: string;
  gameName: string;
  tagLine: string;
  shard: string;
}
