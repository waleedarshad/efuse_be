import { ILOLProfile } from "./lol-profile";
import { IUserInfo } from "./user-info";
import { IValorantProfile } from "./valorant-profile";

export interface IRiotProfile {
  userInfo: IUserInfo;
  valorantProfile: IValorantProfile;
  lolProfile: ILOLProfile;
}
