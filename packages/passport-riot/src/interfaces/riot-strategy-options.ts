export interface IRiotStrategyOptions {
  /**
   * @optional
   *
   * URL used to obtain an authorization grant from Riot
   *
   * @default 'https://auth.riotgames.com/authorize'
   */
  authorizationURL?: string;

  /**
   * @required
   * URL to which Riot will redirect the user after granting authorization
   */
  callbackURL: string;

  /**
   * @required
   * Application's client ID.
   */
  clientID: string;

  /**
   * @required
   * Application's client secret.
   */
  clientSecret: string;

  /**
   * @optional
   * Determines whether `req` is passed as the first argument to the verify callback
   * triggered when Riot redirects to the `callbackURL`.
   *
   * @default false
   */
  passReqToCallback?: boolean;

  /**
   * @optional
   *
   * An array of Riot OAuth2 scopes. May also be a string of scopes separated by `scopeSeparator`.
   * Valid scopes include:
   *  - `'openid'`
   *  - `'cpid'`
   *  - `'offline_access'`
   *
   * @default []
   */
  scope?: string | string[];

  /**
   * @optional
   *
   * String delimiter for scopes in `scopes`.
   *
   * @default ' '
   */
  scopeSeparator?: string;

  /**
   * @optional
   *
   * URL used to obtain an access token from Riot
   *
   * @default 'https://auth.riotgames.com/token'
   */
  tokenURL?: string;

  /**
   * @optional
   *
   * URL used to obtain profile from Riot
   *
   * @default 'https://auth.riotgames.com/userinfo'
   */
  profileURL: string;
}
