export interface IErrorResponse {
  status: {
    status_code: number;
    message: string;
  };
}
