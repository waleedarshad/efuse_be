export interface IUserInfoResponse {
  sub: string;
  cpid: string | null;
  jti: string;
}
