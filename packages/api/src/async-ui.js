const { UI } = require("bull-board");

const { configure } = require("./backend/app");

const PORT = process.env.PORT || 5002;
const TAG = "async-ui";

configure()
  .then((payload) => {
    const { app, server } = payload;

    server.listen(PORT, () => {
      console.log(`server running on port ${PORT}`);
    });

    app.use("/", UI);

    return true;
  })
  .catch((error) => {
    console.error(error, `${TAG} an error occurred configuring async-ui`);
  });
