// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dd-trace").init({
  analytics: true,
  logInjection: true,
  env: process.env.NODE_ENV,
  service: "mongodb-watcher"
});

import { ILogger, Logger } from "@efuse/logger";

import { MongodbService } from "../lib/mongodb/mongodb.service";
import { Register } from "./register";

const logger: ILogger = Logger.create();

class MongodbWatcher {
  public initialize = () => {
    MongodbService.initialize()
      .then(() => Register.INIT())
      .catch((error) => logger.error(error, "an error occurred initializing mongodbWatcher"));
  };
}

export const mongodbWatcher: MongodbWatcher = new MongodbWatcher();

// if we are running directly
try {
  if (require.main === module) {
    mongodbWatcher.initialize();
  }
} catch (error) {
  logger.error(error, "a require error occurred initializing mongodbWatcher");
}
