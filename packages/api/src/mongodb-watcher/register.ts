import { ILogger, Logger } from "@efuse/logger";
import mongoose from "mongoose";

import { WATCHERS } from "./watchers";

const logger: ILogger = Logger.create();

export class Register {
  public static INIT(): void {
    for (const option of WATCHERS) {
      logger.info(`Registering watcher for collection: ${option.collectionName}`);

      try {
        mongoose.connection.db
          .collection(option.collectionName)
          .watch({ fullDocument: option.watch })
          .on(option.on, (data) => {
            logger.info({ data }, `Received data for collection: ${option.collectionName}`);

            return option.callback(data);
          });
      } catch (error) {
        logger.error(error, `Error registering watcher for collection" ${option.collectionName}`);
      }
    }
  }
}
