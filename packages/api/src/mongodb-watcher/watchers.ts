import { LeanDocument } from "mongoose";

import { ILearningArticle, IMongodbWatcherResponse, IOpportunity, IOrganization, IUser } from "../backend/interfaces";
import { OpportunityService } from "../lib/opportunities/opportunity.service";
import { OrganizationsService } from "../lib/organizations/organizations.service";
import { UserHelperService } from "../lib/users/user-helper.service";
import { LearningArticleService } from "../lib/learningArticles/learning-article.service";

interface IWatcher {
  collectionName: string;
  on: "change" | "close" | "end" | "error" | "resumeTokenChanged";
  watch?: "default" | "updateLookup";
  callback: (data: IMongodbWatcherResponse<LeanDocument<any>>) => void;
}

export const WATCHERS: Array<IWatcher> = [
  {
    collectionName: "opportunities",
    on: "change",
    watch: "updateLookup",
    callback: (data: IMongodbWatcherResponse<LeanDocument<IOpportunity>>) => OpportunityService.Watcher(data)
  },
  {
    collectionName: "organizations",
    on: "change",
    watch: "updateLookup",
    callback: (data: IMongodbWatcherResponse<LeanDocument<IOrganization>>) => OrganizationsService.Watcher(data)
  },
  {
    collectionName: "users",
    on: "change",
    watch: "updateLookup",
    callback: (data: IMongodbWatcherResponse<LeanDocument<IUser>>) => UserHelperService.Watcher(data)
  },
  {
    collectionName: "learningarticles",
    on: "change",
    watch: "updateLookup",
    callback: (data: IMongodbWatcherResponse<LeanDocument<ILearningArticle>>) => LearningArticleService.Watcher(data)
  }
];
