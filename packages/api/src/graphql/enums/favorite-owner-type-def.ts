export const enum FavoriteOwnerTypeDef {
  USER = "User",
  ORGANIZATION = "Organization"
}
