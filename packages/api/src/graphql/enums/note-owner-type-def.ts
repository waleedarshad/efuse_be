export const enum NoteOwnerTypeDef {
  USER = "User",
  ORGANIZATION = "Organization"
}
