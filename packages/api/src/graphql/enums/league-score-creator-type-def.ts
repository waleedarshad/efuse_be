export enum LeagueScoreCreatorTypeDef {
  ORGANIZATION = "Organization",
  TEAM = "Team",
  USER = "User"
}
