export const enum FavoriteRelatedDocTypeDef {
  APPLICANT = "Applicant",
  RECRUITMENT_PROFILE = "RecruitmentProfile"
}
