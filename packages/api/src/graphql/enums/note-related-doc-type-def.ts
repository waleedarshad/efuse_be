export const enum NoteRelatedDocTypeDef {
  APPLICANT = "Applicant",
  RECRUITMENT_PROFILE = "RecruitmentProfile"
}
