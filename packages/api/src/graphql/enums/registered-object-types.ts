export const enum RegisteredObjectTypes {
  USER = "User",
  OPPORTUNITY = "Opportunity",
  ERENA_TOURNAMENT = "ERenaTournament",
  ORGANIZATION = "Organization"
}
