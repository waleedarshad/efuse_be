export const enum LeaderboardStatType {
  AIM_LAB_STATS = "AimlabStats",
  LEAGUE_OF_LEGENDS_STATS = "LeagueOfLegendsStats",
  VALORANT_STATS = "ValorantUserStats"
}
