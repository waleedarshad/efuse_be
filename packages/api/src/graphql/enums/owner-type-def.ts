export const enum OwnerTypeDef {
  ORGANIZATION = "Organization",
  USER = "User"
}
