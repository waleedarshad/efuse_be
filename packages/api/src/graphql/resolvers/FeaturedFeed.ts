import { ILogger, Logger } from "@efuse/logger";
import { shuffle } from "lodash";

import { FeaturedLoungeService } from "../../lib/lounge/featured-lounge.service";
import { IRank } from "../../backend/interfaces/rank";
import { IGraphQLContext, PaginationArgs } from "../../backend/interfaces/graphql";
import { LoungeFeedService } from "../../lib/lounge/lounge-feed.service";
import { IFeed } from "../../backend/interfaces/feed";
import { ILoungeFeed } from "../../backend/interfaces/lounge-feed";

import ViewsLib from "../../lib/views";

const $logger: ILogger = Logger.create({ name: "FeaturedFeed" });

export const FeaturedFeedResolver = {
  Query: {
    FeaturedFeed: async (_, args) => {
      const featuredLoungeService = new FeaturedLoungeService();
      const { page, limit } = <PaginationArgs>args;
      return featuredLoungeService.getPaginatedFeaturedPosts(page, limit);
    }
  },
  FeaturedFeed: {
    docs: async ({ docs }: { docs: [IRank] }, args, context: IGraphQLContext) => {
      const userId = context.userId.toHexString();
      const feedIds = docs.map((rank) => rank.document);

      const $loungeFeedService = new LoungeFeedService();

      const feedDocs = await $loungeFeedService.find({ feed: { $in: feedIds } }, {}, { lean: true });
      const featuredFeed = await $loungeFeedService.filterFeedByConstraints(feedIds, userId);

      const results = $loungeFeedService.addRankToFeed(featuredFeed, docs);

      const extendFeaturedFeed = results
        .map((feed: IFeed) => {
          const doc: ILoungeFeed | undefined = feedDocs.find((f) => String(f.feed) === String(feed._id));

          // todo: investigate & fix
          // @ts-ignore
          feed.parentFeedId = doc?._id;
          // todo: investigate & fix
          // @ts-ignore
          feed.kind = doc?.timelineableType;
          feed.kindId = doc?.timelineable;
          return feed;
        })
        .sort((a, b) => {
          const order = feedIds.map((id) => String(id));
          return order.indexOf(String(a._id)) - order.indexOf(String(b._id));
        });

      // Increment views
      ViewsLib.incrementFeedViews(feedIds).catch((error) => {
        $logger?.warn(error, "Error while incrementing Feed view count");
      });

      return shuffle(extendFeaturedFeed);
    }
  }
};
