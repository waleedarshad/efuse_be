import { Failure } from "@efuse/contracts";
import { IResolvers } from "graphql-tools";
import { FilterQuery, PaginateResult, Types } from "mongoose";
import { FavoriteOwnerType, FavoriteRelatedModel } from "@efuse/entities";
import { IApplicant, IGraphQLContext, IOrganization, IUser } from "../../../backend/interfaces";
import { UserService } from "../../../lib/users/user.service";
import { IFavorite } from "../../../backend/interfaces/favorite";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { FavoriteOwnerTypeDef, FavoriteRelatedDocTypeDef } from "../../enums";
import { IRecruitmentProfile } from "../../../backend/interfaces/recruitment-profile";
import { ApplicantService } from "../../../lib/applicants/applicant.service";
import { RecruitmentProfileService } from "../../../lib/recruitment/recruitment-profile.service";
import { FavoritesService } from "../../../lib/favorites/favorites.service";
import { canRecruit } from "../../../lib/recruitment/utils";
import { OrganizationACLService } from "../../../lib/organizations/organization-acl.service";
import { OrganizationAccessTypes } from "../../../lib/organizations/organization-access-types";

type FavoriteOwner = IUser | IOrganization | null;
type FavoriteRelatedDoc = IApplicant | IRecruitmentProfile | null;
type ObjectId = Types.ObjectId | string;

interface CreateFavoriteArgs {
  favorite: IFavorite;
}

interface FavoriteIdArgs {
  favoriteId: ObjectId;
}

interface PaginatedFavoriteArgs {
  page: number;
  limit: number;
  owner: ObjectId;
}

interface GetFavoriteArgs {
  relatedDoc: ObjectId;
  owner: ObjectId;
}

interface GetFavoritesArgs {
  owner: ObjectId;
}

export class FavoriteResolver {
  public static favoriteService: FavoritesService = new FavoritesService();
  private static userService: UserService = new UserService();

  static get resolve(): IResolvers {
    return {
      Query: {
        getFavorite: async (_, args: GetFavoriteArgs): Promise<IFavorite | null> => {
          const { relatedDoc, owner } = args;
          const filter: FilterQuery<IFavorite> = {
            owner,
            relatedDoc
          };

          try {
            const favorite = await this.favoriteService.findOne(filter, {}, { lean: true });

            return favorite;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve favorite", error);
          }
        },
        getFavoriteById: async (_, args: FavoriteIdArgs): Promise<IFavorite | null> => {
          const { favoriteId } = args;

          try {
            const favorite = await this.favoriteService.findOne({ _id: favoriteId }, {}, { lean: true });

            return favorite;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve favorite", error);
          }
        },
        getFavoritesByOwner: async (_, args: GetFavoritesArgs): Promise<IFavorite[]> => {
          const { owner } = args;
          const filter: FilterQuery<IFavorite> = {
            owner
          };

          try {
            const favorite = await this.favoriteService.find(filter, {}, { lean: true });

            return favorite;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve favorites", error);
          }
        },
        getPaginatedFavoritesByOwner: async (_, args: PaginatedFavoriteArgs): Promise<PaginateResult<IFavorite>> => {
          const { page, limit, owner } = args;

          try {
            const favorite = await this.favoriteService.getPaginatedFavorites(owner, page, limit);

            return favorite;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve paginated favorites", error);
          }
        }
      },

      Mutation: {
        createFavorite: async (_, args: CreateFavoriteArgs, context: IGraphQLContext): Promise<IFavorite> => {
          const { favorite } = args;
          const { owner, ownerType, relatedDoc, relatedModel } = favorite;

          try {
            // will check for issues and appropriate permissions and throw error if there is an issue
            await this.CheckTypesAndCheckForIssues(context.userId, owner, relatedDoc, ownerType, relatedModel);

            const filter: FilterQuery<IFavorite> = {
              owner,
              relatedDoc
            };

            // check if there is already a favorite for this related doc and owner. if there is return it.
            const foundLeanFavorite = await this.favoriteService.findOne(filter, {}, { lean: true });

            if (foundLeanFavorite) {
              return foundLeanFavorite;
            }

            const savedFavorite = await this.favoriteService.create(favorite);

            return savedFavorite;
          } catch (error) {
            throw Failure.InternalServerError("Could not create favorite", error);
          }
        },

        deleteFavorite: async (_, args: FavoriteIdArgs, context: IGraphQLContext): Promise<boolean> => {
          const { favoriteId } = args;
          const adjustOutput = {
            _id: false,
            owner: true,
            relatedDoc: true,
            ownerType: true,
            relatedModel: true
          };

          try {
            const foundLeanFavorite = await this.favoriteService.findOne({ _id: favoriteId }, adjustOutput, {
              lean: true
            });

            if (!foundLeanFavorite) {
              throw Failure.NotFound("Favorite could not be deleted because it does not exist", favoriteId);
            }

            const { owner, relatedDoc, ownerType, relatedModel } = foundLeanFavorite;

            // will check for issues and appropriate permissions and throw error if issue
            await this.CheckTypesAndCheckForIssues(context.userId, owner, relatedDoc, ownerType, relatedModel);

            const partnerApplicant = await this.favoriteService.delete(favoriteId);

            return partnerApplicant;
          } catch (error) {
            throw Failure.InternalServerError("Could not delete favorite", error);
          }
        }
      },

      // Resolve Union
      FavoriteOwner: {
        __resolveType(parent: FavoriteOwner): FavoriteOwnerTypeDef {
          // should only exist on users
          if ((<IUser>parent)?.name) {
            return FavoriteOwnerTypeDef.USER;
          }

          // should only exist on organizations
          if ((<IOrganization>parent)?.organizationType) {
            return FavoriteOwnerTypeDef.ORGANIZATION;
          }

          throw Failure.ExpectationFailed("Could not determine owner type", parent);
        }
      },

      // Resolve Union
      FavoriteRelatedDoc: {
        __resolveType(parent: FavoriteRelatedDoc): FavoriteRelatedDocTypeDef {
          // should only exist on applicants
          if ((<IApplicant>parent)?.candidateQuestions) {
            return FavoriteRelatedDocTypeDef.APPLICANT;
          }

          // should only exist on recruiment profiles
          if ((<IRecruitmentProfile>parent)?.graduationClass) {
            return FavoriteRelatedDocTypeDef.RECRUITMENT_PROFILE;
          }

          throw Failure.ExpectationFailed("Could not determine favorite related model", parent);
        }
      },

      // Resolve Objects in Favorites
      Favorite: {
        creator: async (parent: IFavorite): Promise<IUser | null> => {
          const { creator }: { creator: ObjectId } = parent;

          try {
            const foundUser: IUser | null = await this.userService.findOne({ _id: creator }, {}, { lean: true });

            return foundUser;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve creator data for favorite", error);
          }
        },

        owner: async (parent: IFavorite): Promise<FavoriteOwner> => {
          const { owner }: { owner: ObjectId } = parent;

          const organizationService = new OrganizationsService();

          try {
            let foundDoc: FavoriteOwner;

            switch (parent.ownerType) {
              case FavoriteOwnerType.ORGANIZATIONS:
                foundDoc = await organizationService.findOne({ _id: owner }, {}, { lean: true });
                break;
              case FavoriteOwnerType.USERS:
                foundDoc = await this.userService.findOne({ _id: owner }, {}, { lean: true });
                break;
              default:
                throw Failure.ExpectationFailed(
                  "Could not determine which type of document to retrieve",
                  parent.ownerType
                );
            }

            return foundDoc;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve owner data for favorite", error);
          }
        },

        relatedDoc: async (parent: IFavorite): Promise<FavoriteRelatedDoc> => {
          const { relatedDoc }: { relatedDoc: ObjectId } = parent;

          const applicantService = new ApplicantService();
          const recruitmentProfileService = new RecruitmentProfileService();

          let foundDoc: FavoriteRelatedDoc;

          try {
            switch (parent.relatedModel) {
              case FavoriteRelatedModel.APPLICANTS:
                foundDoc = await applicantService.findOne({ _id: relatedDoc }, {}, { lean: true });
                break;
              case FavoriteRelatedModel.RECRUITMENT_PROFILES:
                foundDoc = await recruitmentProfileService.findOne({ _id: relatedDoc }, {}, { lean: true });
                break;
              default:
                throw Failure.ExpectationFailed(
                  "Could not determine which type of document to retrieve",
                  parent.relatedModel
                );
            }

            return foundDoc;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve related doc data for favorite", error);
          }
        }
      }
    };
  }

  private static async CheckTypesAndCheckForIssues(
    currentUserId: ObjectId,
    owner: ObjectId,
    relatedDoc: ObjectId,
    ownerType: FavoriteOwnerType,
    relatedModel: FavoriteRelatedModel
  ): Promise<void> {
    switch (relatedModel) {
      case FavoriteRelatedModel.RECRUITMENT_PROFILES:
        await this.ValidateUserAuthorizedInOrganization(currentUserId, owner, ownerType);
        // todo: investigate
        // @ts-ignore
        await canRecruit(currentUserId);
        break;
      case FavoriteRelatedModel.APPLICANTS:
        await this.ValidateUserAuthorizedInOrganization(currentUserId, owner, ownerType);
        break;
      default:
        Failure.ExpectationFailed("Could not determine type of model", {
          currentUserId,
          owner,
          relatedDoc,
          ownerType,
          relatedModel
        });
    }
  }

  private static async ValidateUserAuthorizedInOrganization(
    currentUserId: ObjectId,
    owner: ObjectId,
    ownerType: FavoriteOwnerType
  ): Promise<void> {
    if (ownerType === FavoriteOwnerType.ORGANIZATIONS) {
      const organizationACLService = new OrganizationACLService();
      await organizationACLService.authorizedToAccess(currentUserId, owner, OrganizationAccessTypes.ANY);
    }
  }
}
