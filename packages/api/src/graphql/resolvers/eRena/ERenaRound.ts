import { IERenaRound, IERenaRoundCreateParams } from "../../../backend/interfaces/erena/erena-round";
import { ERenaRoundService } from "../../../lib/erena/erena-round.service";
import { ERenaBracketService } from "../../../lib/erena/erena-bracket.service";
import { Context } from "../../schema/context";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { ERenaMatchService } from "../../../lib/erena/erena-match.service";
import { IERenaMatch, IERenaTournament } from "../../../backend/interfaces";
import { Failure } from "@efuse/contracts";

const erenaBracketService = new ERenaBracketService();
const eRenamatchService = new ERenaMatchService();
const eRenaroundService = new ERenaRoundService();
const erenaSharedService = new ERenaSharedService();

export const ERenaRoundResolver = {
  ERenaTournament: {
    rounds: async (parent: IERenaTournament): Promise<IERenaRound[]> => {
      const rounds = await eRenaroundService.find({ tournament: parent?._id }, {}, { lean: true });

      return rounds;
    }
  },

  Query: {
    getRound: async (_, args: { id: string }): Promise<IERenaRound | null> => {
      const { id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const erenaRound = await eRenaroundService.findOne({ _id: id }, {}, { lean: true });

      if (!erenaRound) {
        throw Failure.BadRequest("Unable to find bracket");
      }

      return erenaRound;
    }
  },

  Mutation: {
    createRound: async (
      _,
      args: { body: IERenaRoundCreateParams; tournamentIdOrSlug: string },
      context: Context
    ): Promise<IERenaRound> => {
      const userId = context.user._id;
      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = <IERenaTournament>await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const round = await erenaBracketService.createRound(tournament._id, body);

      if (!round) {
        throw Failure.InternalServerError("Failed to create round");
      }

      return round;
    },

    updateRound: async (
      _,
      args: { body: IERenaRoundCreateParams; id: string },
      context: Context
    ): Promise<IERenaRound> => {
      const userId = context.user._id;
      const { id, body } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const original = await eRenaroundService.findById(id);

      const tournament = <IERenaTournament>(
        await erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString())
      );
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const round = await eRenaroundService.updateRound(userId, id, body);

      if (!round) {
        throw Failure.NotFound("Failed to update round");
      }

      return round;
    }
  },

  ERenaRound: {
    matches: async (parent: IERenaRound): Promise<IERenaMatch[]> => {
      const matches = await eRenamatchService.find(
        { _id: { $in: parent.matches.map((match) => match.match) } },
        {},
        { lean: true }
      );

      return matches;
    },
    tournament: async (parent: IERenaRound): Promise<IERenaTournament | null> => {
      const erenaTournamentService = new ERenaTournamentService();

      const tournament = await erenaTournamentService.findOne({ _id: parent.tournament }, {}, { lean: true });

      return tournament;
    }
  }
};
