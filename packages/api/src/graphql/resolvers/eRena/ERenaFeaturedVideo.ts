import { ERenaFeaturedVideoService } from "../../../lib/erena/erena-featured-video.service";

const service = new ERenaFeaturedVideoService();

export const ERenaFeaturedVideoResolver = {
  Query: {
    getERenaFeaturedVideos: async () => {
      return service.getFeaturedVideos();
    }
  }
};
