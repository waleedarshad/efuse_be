import { FilterQuery, Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { IERenaPlayer, IERenaTeam, IERenaTournament, IUser } from "../../../backend/interfaces";
import { ERenaPlayerService } from "../../../lib/erena/erena-player.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTeamService } from "../../../lib/erena/erena-team.service";
import { Context } from "../../schema/context";
import { IERenaScore } from "../../../backend/interfaces/erena/erena-score";
import { ERenaScoreOwnerKinds } from "../../../backend/interfaces/erena/erena-enums";
import { ERenaScoreService } from "../../../lib/erena/erena-score.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { UserService } from "../../../lib/users/user.service";

const eRenaPlayerService = new ERenaPlayerService();
const erenaTeamService = new ERenaTeamService();
const erenaSharedService = new ERenaSharedService();

export const ERenaPlayerResolver = {
  ERenaTournament: {
    players: async (parent: IERenaTournament): Promise<IERenaPlayer[]> => {
      const players = await eRenaPlayerService.getByTournament(parent._id);
      return players;
    }
  },

  Query: {
    getPlayer: async (_, args): Promise<IERenaPlayer | null> => {
      const { id }: { id: string } = args;

      if (!id) {
        throw Failure.BadRequest("Missing id params");
      }

      const player: IERenaPlayer | null = await eRenaPlayerService.findOne({ _id: id }, {}, <never>{
        autopopulate: false
      });
      return player;
    }
  },
  Mutation: {
    /**
     * Attempts to create a new eRena player
     *
     * @param teamId Team ID
     * @param body the player data
     *
     * @memberof ERenaPlayer
     */
    createPlayer: async (_, args: { body: IERenaPlayer; teamId: string }, context: Context): Promise<IERenaPlayer> => {
      const userId = context.user._id;
      const { teamId, body } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!teamId) {
        throw Failure.BadRequest("Missing team id");
      }

      const team = await erenaTeamService.findById(teamId);

      if (!team) {
        throw Failure.BadRequest("Could not find team");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(team.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      body.team = <Types.ObjectId>team._id;
      body.tournament = <Types.ObjectId>tournament._id;

      const playerPartial: Partial<IERenaPlayer> = <Partial<IERenaPlayer>>{
        team: body.team,
        type: body.type,
        user: body.user,
        name: body.name,
        isActive: body.isActive,
        tournament: body.tournament
      };

      const player = await eRenaPlayerService.create(playerPartial);
      return player;
    },

    /**
     * Attempts to update the eRena player with matching `id`
     *
     * @param args.id The ID of the player to update
     * @param args.body the player data to update (only updates name, isActive)
     *
     * @memberof ERenaPlayer
     */
    updatePlayer: async (_, args: { body: IERenaPlayer; id: string }, context: Context): Promise<IERenaPlayer> => {
      const userId = context.user._id;
      const { id, body } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const original = await eRenaPlayerService.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());

      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      // only update name, isActive
      const playerUpdatePartial: Partial<IERenaPlayer> = <Partial<IERenaPlayer>>{
        name: body.name,
        isActive: body.isActive
      };

      const updatePlayer = await eRenaPlayerService.update(id, playerUpdatePartial);
      return updatePlayer;
    },
    /**
     * Attempts to delete the eRena Player with matching `id` also updates the team's score
     *
     * @param id The ID of the player to delete
     *
     * @memberof ERenaPlayer
     */
    deletePlayer: async (_, args: { id: string }, context: Context): Promise<IERenaPlayer> => {
      const userId = context.user._id;
      const { id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const player = await eRenaPlayerService.findById(id);

      if (!player) {
        throw Failure.UnprocessableEntity("Could not find player to delete");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(player.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const response = await eRenaPlayerService.delete(id);

      if (response) {
        return player;
      }

      throw Failure.InternalServerError("Failed to delete Player");
    }
  },

  ERenaPlayer: {
    score: async (parent: IERenaPlayer): Promise<number> => {
      const player = parent;

      const scoreQuery: FilterQuery<IERenaScore> = {
        tournament: player.tournament,
        ownerKind: ERenaScoreOwnerKinds.player,

        ownerId: { $in: player._id }
      };
      if (player.matchId) {
        scoreQuery.match = player.matchId;
      }

      const eRenaScoreService = new ERenaScoreService();
      const playerScore = await eRenaScoreService.find(scoreQuery, {}, { lean: true });

      const totalScore = playerScore.map((s) => s.score);

      const score = totalScore?.length > 0 ? totalScore.reduce((a, b) => a + b) : 0;
      return score;
    },
    team: async (parent: IERenaPlayer): Promise<IERenaTeam | null> => {
      const erenaTeam = await erenaTeamService.findOne({ _id: parent.team }, {}, { lean: true });

      return erenaTeam;
    },
    tournament: async (parent: IERenaPlayer): Promise<IERenaTournament | null> => {
      const erenaTournamentService = new ERenaTournamentService();

      const tournament = await erenaTournamentService.findOne({ _id: parent.tournament }, {}, { lean: true });

      return tournament;
    },
    user: async (parent: IERenaPlayer): Promise<IUser | null> => {
      const userService = new UserService();

      const tournament = await userService.findOne({ _id: parent.user }, {}, { lean: true });

      return tournament;
    }
  }
};
