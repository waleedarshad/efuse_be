import { Failure } from "@efuse/contracts";

import { IERenaScore } from "../../../backend/interfaces/erena/erena-score";
import { ERenaScoreService } from "../../../lib/erena/erena-score.service";
import { ERenaScoringKinds } from "../../../backend/interfaces/erena/erena-enums";
import { Context } from "../../schema/context";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { IERenaMatch, IERenaTournament } from "../../../backend/interfaces";
import { ERenaMatchService } from "../../../lib/erena/erena-match.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { FilterQuery } from "mongoose";

const eRenaScoreService = new ERenaScoreService();
const erenaSharedService = new ERenaSharedService();

export const ERenaScoreResolver = {
  ERenaTournament: {
    scores: async (parent: IERenaTournament): Promise<IERenaScore[]> => {
      const filter: FilterQuery<IERenaScore> = {
        tournament: parent?._id
      };

      const entities = await eRenaScoreService.find(filter, {}, { lean: true });
      return entities;
    }
  },

  Query: {
    getScore: async (_, args): Promise<IERenaScore | null> => {
      const { id }: { id: string } = args;

      if (!id) {
        throw Failure.BadRequest("Missing param id");
      }

      const score = await eRenaScoreService.findOne({ _id: id }, {}, { lean: 1 });
      return score;
    }
  },
  Mutation: {
    /**
     * Attempts to create a new eRena score
     *
     * @param tournamentIdOrSlug The ID of the tournament to post scores
     * @param body the score data
     *
     * @return {ERenaScore}
     */
    createScore: async (
      _,
      args: { tournamentIdOrSlug: string; body: IERenaScore },
      context: Context
    ): Promise<IERenaScore> => {
      const userId = context.user._id;
      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing tournament id");
      }

      if (body.score === undefined) {
        throw Failure.BadRequest("Missing 'score'");
      }

      body.score = Number(body.score);

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      if (tournament.bracketType === ERenaScoringKinds.bracket && !body.match) {
        throw Failure.BadRequest("Missing match id on score object.");
      }

      body.tournament = tournament._id;

      body.erenaKind =
        tournament.bracketType === ERenaScoringKinds.bracket ? ERenaScoringKinds.bracket : ERenaScoringKinds.pointRace;

      const validationErrors = eRenaScoreService.validateIncomingFields(body);
      if (validationErrors.length > 0) {
        throw Failure.BadRequest(validationErrors.join(" | "));
      }

      const entity = await eRenaScoreService.create(body);

      if (!entity) {
        throw Failure.InternalServerError("Failed to create score");
      }

      return entity;
    },
    /**
     * Attempts to update the eRena score with matching `id`
     * Note: only updates the score value
     *
     * @param id The ID of the score to update
     * @param body the score data to update
     *
     * @return {ERenaScore}
     */
    updateScore: async (_, args: { body: IERenaScore; id: string }, context: Context): Promise<IERenaScore> => {
      const userId = context.user._id;
      const { id, body } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (body.score === undefined) {
        throw Failure.BadRequest("Missing 'score'");
      }

      body.score = Number(body.score);

      const original = await eRenaScoreService.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      // only update the score
      original.score = body.score;

      const updated = await eRenaScoreService.update(id, original);

      if (!updated) {
        throw Failure.NotFound("Failed to update score");
      }

      return updated;
    },
    /**
     * Attempts to delete the eRena Score with matching `id`
     *
     * @param id The ID of the score to delete
     *
     * @return {ERenaScore}
     */
    deleteScore: async (_, args: { id: string }, context: Context): Promise<IERenaScore> => {
      const userId = context.user._id;
      const { id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const score = await eRenaScoreService.findById(id);

      if (!score) {
        throw Failure.NotFound("Could not find score to delete");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(score.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const entity = await eRenaScoreService.delete(id);

      if (!entity) {
        throw Failure.NotFound("Failed to delete Score");
      }

      return entity;
    },
    /**
     * Attempts to create new eRena scores in bulk
     *
     * @param tournamentIdOrSlug The ID of the tournament to post scores
     * @param body the score data
     *
     * @return {ERenaScore[]}
     */
    bulkCreateScores: async (
      _,
      args: { tournamentIdOrSlug: string; body: IERenaScore[] },
      context: Context
    ): Promise<Promise<IERenaScore>[]> => {
      const userId = context.user._id;
      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing tournament id");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const scores = await eRenaScoreService.bulkCreateScores(tournament._id, body);

      if (!scores) {
        throw Failure.InternalServerError("Failed to create score");
      }

      return scores;
    },
    /**
     * Attempts to update eRena scores in bulk
     *
     * @param tournamentIdOrSlug The ID of the tournament to post scores
     * @param body the score data
     *
     * @return {ERenaScore[]}
     */
    bulkUpdateScores: async (
      _,
      args: { tournamentIdOrSlug: string; body: IERenaScore[] },
      context: Context
    ): Promise<Promise<IERenaScore>[]> => {
      const userId = context.user._id;
      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing tournament id");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const scores = await eRenaScoreService.bulkUpdateScores(tournament._id, body);

      if (!scores) {
        throw Failure.InternalServerError("Failed to create score");
      }

      return scores;
    }
  },

  ERenaScore: {
    match: async (parent: IERenaScore): Promise<IERenaMatch | null> => {
      const matchService = new ERenaMatchService();
      const match = await matchService.findOne({ _id: parent.match }, {}, { lean: true });

      return match;
    },
    tournament: async (parent: IERenaScore): Promise<IERenaTournament | null> => {
      const tournamentService = new ERenaTournamentService();
      const tournament = await tournamentService.findOne({ _id: parent.tournament }, {}, { lean: true });

      return tournament;
    }
  }
};
