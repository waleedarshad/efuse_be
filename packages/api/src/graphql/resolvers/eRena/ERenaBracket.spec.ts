import { Types } from "mongoose";
import { ERenaBracketResolver } from "./ERenaBracket";
import { ERenaBracketService } from "../../../lib/erena/erena-bracket.service";
import { ERenaRoundService } from "../../../lib/erena/erena-round.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import {
  ERenaScoringKinds,
  ERenaTournamentOwnerKinds,
  ERenaTournamentStatus,
  ERenaType
} from "../../../backend/interfaces/erena/erena-enums";
import { IERenaBracket } from "../../../backend/interfaces";

jest.mock("../../../lib/erena/erena-bracket.service");
jest.mock("../../../lib/erena/erena-round.service");
jest.mock("../../../lib/erena/erena-tournament.service");

describe("ERenaBracket", () => {
  it("getBracket makes the correct service call if id is passed", async () => {
    ERenaBracketService.prototype.findOne = jest.fn().mockResolvedValue({ tournament: "123" });

    await ERenaBracketResolver.Query.getBracket(null, { id: "123" });

    expect(ERenaBracketService.prototype.findOne).toHaveBeenCalled();
  });

  it("getBracket throws the correct error if no id is passed", async () => {
    ERenaBracketService.prototype.findOne = jest.fn().mockResolvedValue(null);

    const promise = ERenaBracketResolver.Query.getBracket(null, {});

    await expect(promise).rejects.toThrowError("Missing id parameter");
  });

  it("throws the correct error if no bracket is returned", async () => {
    ERenaBracketService.prototype.findOne = jest.fn().mockReturnValue(undefined);

    const promise = ERenaBracketResolver.Query.getBracket(null, { id: "123" });

    await expect(promise).rejects.toThrowError("Unable to find bracket");
  });

  it("rounds makes the correct service call", async () => {
    const round1 = {
      _id: "11111",
      roundDate: new Date(),
      roundTitle: "round-title",
      roundNumber: 1,
      tournament: "33333",
      matches: []
    };

    const round2 = {
      _id: "22222",
      roundDate: new Date(),
      roundTitle: "round-title-2",
      roundNumber: 2,
      tournament: "44444",
      matches: []
    };

    const bracket = {
      totalTeams: 4,
      totalRounds: 2,
      totalMatches: 2,
      tournament: "1234",
      rounds: [
        { _id: "54321", round: "11111" },
        { _id: "9876", round: "22222" }
      ]
    };

    ERenaRoundService.prototype.find = jest.fn().mockResolvedValue([round1, round2]);

    await ERenaBracketResolver.ERenaBracket.rounds(bracket as unknown as IERenaBracket);

    expect(ERenaRoundService.prototype.find).toHaveBeenCalled();
    expect(ERenaRoundService.prototype.find).toHaveBeenCalledWith(
      { _id: { $in: ["11111", "22222"] } },
      {},
      { lean: true }
    );
  });

  it("tournament makes the correct service call", async () => {
    const tournament = {
      airtableBaseId: "",
      airtableId: "",
      backgroundImageUrl: "",
      bracketType: ERenaScoringKinds.bracket,
      brandLogoUrl: "",
      brandName: "",
      challongeLeaderboard: "",
      currentRound: 0,
      endDatetime: new Date(),
      extensionLinkoutUrl: "",
      extensionNamespace: "",
      extensionPointer: false,
      gameSelection: "",
      globalStats: { playersRemaining: 0, statName: "", teamsRemaining: 0, totalKills: 0, totalValue: 0 },
      hideLeaderboard: false,
      imageLink1: "",
      imageLink2: "",
      imageLink3: "",
      imageLink4: "",
      imageLink5: "",
      imageLink6: "",
      imageUrl1: "",
      imageUrl2: "",
      imageUrl3: "",
      imageUrl4: "",
      imageUrl5: "",
      imageUrl6: "",
      isFinal: false,
      landingPageUrl: "",
      lastAirtableModificationTime: "",
      logoURL: "",
      logoUrl: "",
      opportunity: undefined,
      organization: "",
      owner: undefined,
      ownerType: ERenaTournamentOwnerKinds.user,
      playersPerTeam: 0,
      primaryColor: "",
      requirements: [],
      rulesMarkdown: "",
      secondaryColor: "",
      slug: "",
      startDatetime: new Date(),
      statName: "",
      status: ERenaTournamentStatus.active,
      totalRounds: 0,
      totalTeams: 0,
      tournamentDescription: "",
      tournamentId: "",
      tournamentName: "",
      twitchChannel: "",
      type: ERenaType.efuse,
      websiteUrl: "",
      youtubeChannel: ""
    };

    ERenaTournamentService.prototype.findOne = jest.fn().mockResolvedValue(tournament);

    const tournamentId = Types.ObjectId();

    await ERenaBracketResolver.ERenaBracket.tournament({
      rounds: [],
      totalMatches: 0,
      totalRounds: 0,
      totalTeams: 0,
      tournament: tournamentId
    } as unknown as IERenaBracket);

    expect(ERenaTournamentService.prototype.findOne).toHaveBeenCalledWith({ _id: tournamentId }, {}, { lean: true });
  });
});
