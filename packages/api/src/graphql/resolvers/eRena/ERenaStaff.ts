import { Failure } from "@efuse/contracts";
import { Context } from "../../schema/context";
import { IERenaTournament, IUser } from "../../../backend/interfaces";
import { IERenaStaff } from "../../../backend/interfaces/erena/erena-staff";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaStaffService } from "../../../lib/erena/erena-staff.service";
import { UserService } from "../../../lib/users/user.service";
import { ERenaRoles } from "../../../backend/interfaces/erena/erena-enums";
import { sendTournamentStaffNotification } from "../../../backend/helpers/notifications.helper";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";

const eRenaStaffService = new ERenaStaffService();
const userService = new UserService();
const erenaSharedService = new ERenaSharedService();

export const ERenaStaffResolver = {
  ERenaTournament: {
    staff: async (parent: IERenaTournament): Promise<IERenaStaff[]> => {
      const tournament = parent;

      const queryValue = {
        tournament: tournament?._id
      };

      const staffs = await eRenaStaffService.find(queryValue, {}, { lean: true });
      return staffs;
    }
  },

  Query: {
    getStaff: async (_, args): Promise<IERenaStaff | null> => {
      const { id }: { id: string } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const staff = await eRenaStaffService.findOne({ _id: id }, {}, { lean: 1 });
      return staff;
    }
  },
  Mutation: {
    createStaff: async (
      _,
      args: { body: IERenaStaff; tournamentIdOrSlug: string },
      context: Context
    ): Promise<IERenaStaff> => {
      const userId = context.user._id;
      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      if (!body.user) {
        throw Failure.BadRequest("Missing user.");
      }

      const newStaff = await userService.findOne({ _id: body.user });
      if (!newStaff) {
        throw Failure.BadRequest("Unable to find user.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      body.tournament = tournament._id;

      const staffBody: Partial<IERenaStaff> = {
        user: body.user,

        tournament: tournament._id,
        role: body.role
      };

      const staff = await eRenaStaffService.create(staffBody);

      if (!staff) {
        throw Failure.InternalServerError("Failed to create staff");
      } else if (staffBody.role !== ERenaRoles.owner) {
        await sendTournamentStaffNotification(context.user, newStaff, tournament);
      }
      return staff;
    },
    updateStaff: async (_, args: { id: string; body: IERenaStaff }, context: Context): Promise<IERenaStaff> => {
      const userId = context.user._id;
      const { body, id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const original = await eRenaStaffService.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      // only update name, isActive
      const staffUpdatePartial: Partial<IERenaStaff> = <Partial<IERenaStaff>>{
        role: body.role
      };

      // verify permissions
      const tournament = await erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const updated = await eRenaStaffService.update(id, staffUpdatePartial);

      if (!updated) {
        throw Failure.NotFound("Failed to update Staff");
      }

      return updated;
    },
    deleteStaff: async (_, args: { id: string }, context: Context): Promise<IERenaStaff> => {
      const userId = context.user._id;
      const { id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const staff = await eRenaStaffService.findById(id);

      if (!staff) {
        throw Failure.UnprocessableEntity("Could not find Staff to delete");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(staff.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const response = await eRenaStaffService.delete(id);

      if (!response) {
        throw Failure.InternalServerError("Failed to delete Staff");
      }

      return staff;
    }
  },
  ERenaStaff: {
    user: async (parent: IERenaStaff): Promise<IUser | null> => {
      const user = await userService.findOne({ _id: parent.user }, {}, { lean: 1 });
      return user;
    },
    tournament: async (parent: IERenaStaff): Promise<IERenaTournament | null> => {
      const tournamentService = new ERenaTournamentService();
      const tournament = await tournamentService.findOne({ _id: parent.tournament }, {}, { lean: 1 });

      return tournament;
    }
  }
};
