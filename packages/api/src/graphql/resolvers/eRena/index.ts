import { ERenaFeaturedEventResolver } from "./ERenaFeaturedEvent";
import { ERenaFeaturedVideoResolver } from "./ERenaFeaturedVideo";
import { ERenaTournamentResolver } from "./ERenaTournament";
import { ERenaBracketResolver } from "./ERenaBracket";
import { ERenaMatchResolver } from "./ERenaMatch";
import { ERenaRoundResolver } from "./ERenaRound";
import { ERenaTeamResolver } from "./ERenaTeam";
import { ERenaPlayerResolver } from "./ERenaPlayer";
import { ERenaScoreResolver } from "./ERenaScore";
import { ERenaStaffResolver } from "./ERenaStaff";

export const ERenaResolvers = [
  ERenaTournamentResolver,
  ERenaFeaturedEventResolver,
  ERenaFeaturedVideoResolver,
  ERenaBracketResolver,
  ERenaMatchResolver,
  ERenaRoundResolver,
  ERenaTeamResolver,
  ERenaPlayerResolver,
  ERenaScoreResolver,
  ERenaStaffResolver
];
