import { ErenaFeaturedEventService } from "../../../lib/erena/erena-featured-event.service";

const service = new ErenaFeaturedEventService();

export const ERenaFeaturedEventResolver = {
  Query: {
    getFeaturedEvent: async () => {
      return service.getFeaturedEvent();
    }
  }
};
