import { Failure } from "@efuse/contracts";
import { Nullable } from "@efuse/types";
import { ERenaRoundService } from "../../../lib/erena/erena-round.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { IERenaRound, IERenaTournament } from "../../../backend/interfaces";
import { IERenaBracket } from "../../../backend/interfaces/erena/erena-bracket";
import { ERenaBracketService } from "../../../lib/erena/erena-bracket.service";

const eRenaBracketService = new ERenaBracketService();
const erenaRoundService = new ERenaRoundService();
const erenaTournamentService = new ERenaTournamentService();

export const ERenaBracketResolver = {
  ERenaTournament: {
    bracket: async (parent: IERenaTournament): Promise<IERenaBracket | null> => {
      const erenaBracket = await eRenaBracketService.findByTournament(parent._id);

      return erenaBracket;
    }
  },

  Query: {
    getBracket: async (_, args): Promise<Nullable<IERenaBracket>> => {
      const { id }: { id: string } = args;

      if (!id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const erenaBracket = await eRenaBracketService.findOne({ _id: id }, {}, <never>{ autopopulate: false, lean: 1 });

      if (!erenaBracket) {
        throw Failure.BadRequest("Unable to find bracket");
      }

      return erenaBracket;
    }
  },

  ERenaBracket: {
    rounds: async (parent: IERenaBracket): Promise<IERenaRound[]> => {
      const rounds = await erenaRoundService.find(
        { _id: { $in: parent.rounds.map((round) => round.round) } },
        {},
        { lean: true }
      );

      return rounds;
    },
    tournament: async (parent: IERenaBracket): Promise<IERenaTournament | null> => {
      const tournament = await erenaTournamentService.findOne({ _id: parent.tournament }, {}, { lean: true });

      return tournament;
    }
  }
};
