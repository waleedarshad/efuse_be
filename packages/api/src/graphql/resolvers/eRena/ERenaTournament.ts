import { PaginateResult } from "mongoose";
import { Failure } from "@efuse/contracts";
import { IOpportunity, IOrganization } from "../../../backend/interfaces";
import {
  IERenaTournament,
  IERenaTournamentFileUploader,
  IERenaTournamentPopulated
} from "../../../backend/interfaces/erena";
import {
  ERenaRoles,
  ERenaTournamentOwnerKinds,
  ERenaTournamentStatus
} from "../../../backend/interfaces/erena/erena-enums";
import { IUser } from "../../../backend/interfaces/user";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { UserService } from "../../../lib/users/user.service";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { ERenaGameService } from "../../../lib/erena/erena-game.service";
import { Context } from "../../schema/context";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { OpportunityService } from "../../../lib/opportunities/opportunity.service";

const erenaTournamentService = new ERenaTournamentService();
const erenaGameService = new ERenaGameService();
const erenaSharedService = new ERenaSharedService();
const organizationService = new OrganizationsService();
const userService = new UserService();
const opportunityService = new OpportunityService();

interface TournamentRequestArgs {
  page: number;
  limit: number;
  status: string[];
  sortBy: string;
  sortDirection: number;
  startDateMin: number;
  startDateMax: number;
  endDateMin: number;
  endDateMax: number;
}

export const ERenaTournamentResolver = {
  TournamentOwner: {
    __resolveType: async (parent: IUser | IOrganization | IERenaTournament) => {
      const userExists = await userService.exists({ _id: parent._id });
      if (userExists) {
        return "User";
      }

      const organizationExists = await organizationService.exists({ _id: parent._id });
      if (organizationExists) {
        return "Organization";
      }

      throw Failure.ExpectationFailed("Could not determine owner type", parent);
    }
  },

  Query: {
    getERenaTournament: async (_, args: { tournamentIdOrSlug: string }): Promise<IERenaTournament> => {
      const { tournamentIdOrSlug } = args;

      const tournament = await erenaTournamentService.getByIdOrSlug(tournamentIdOrSlug);

      return <IERenaTournament>(<unknown>tournament);
    },
    listERenaTournaments: async (_, args): Promise<PaginateResult<IERenaTournament>> => {
      const { page, limit, status, sortBy, sortDirection, startDateMin, startDateMax, endDateMin, endDateMax } =
        <TournamentRequestArgs>args || {};
      const results = await erenaTournamentService.getAll(
        Number(page),
        Number(limit),
        <ERenaTournamentStatus[]>status,
        String(sortBy),
        Number(sortDirection),
        Number(startDateMin),
        Number(startDateMax),
        Number(endDateMin),
        Number(endDateMax)
      );
      return results;
    }
  },
  Mutation: {
    createTournament: async (_, args: { body: IERenaTournament }, context: Context): Promise<IERenaTournament> => {
      const userId = context.user._id;
      const { body } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const tournamentParams = body;

      const { errors, isValid } = await erenaTournamentService.validateTournament(tournamentParams, context.user);

      if (!isValid) {
        throw Failure.BadRequest(errors.join(" "));
      }

      const tournament = await erenaTournamentService.create(tournamentParams, userId);

      if ("game" in tournamentParams) {
        await erenaGameService.associateGame(tournament._id, tournamentParams.game as string);
      }

      return tournament;
    },
    updateTournament: async (
      _,
      args: { tournamentIdOrSlug: string; files: IERenaTournamentFileUploader; body: IERenaTournament },
      context: Context
    ): Promise<IERenaTournamentPopulated> => {
      const { tournamentIdOrSlug, body, files } = args;
      const uploadedFiles: IERenaTournamentFileUploader = files;
      const tournament: IERenaTournament = args.body;
      const userId = context.user._id;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing param 'tournamentIdOrSlug'");
      }

      const original = <IERenaTournament>await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      await erenaSharedService.isAuthorizedUser(userId, original);

      const updatedTournament = await erenaTournamentService.update(tournamentIdOrSlug, tournament, uploadedFiles);

      if ("game" in tournament) {
        await erenaGameService.associateGame(updatedTournament._id, tournament.game as string);
      }

      return updatedTournament;
    },
    deleteTournament: async (
      _,
      args: { tournamentIdOrSlug: string },
      context: Context
    ): Promise<IERenaTournamentPopulated> => {
      const userId = context.user._id;
      const { tournamentIdOrSlug } = args;

      const original = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      await erenaSharedService.isAuthorizedUser(userId, original, ERenaRoles.owner);

      const patchValue = { status: ERenaTournamentStatus.deleted };

      const updatedTournament = await erenaTournamentService.update(original._id.toHexString(), patchValue);

      return updatedTournament;
    },
    updateTournamentAd: async (
      _,
      args: {
        tournamentIdOrSlug: string;
        link: string;
        index: number;
        imageURL: string;
      },
      context: Context
    ): Promise<IERenaTournamentPopulated> => {
      const userId = context.user._id;
      const { tournamentIdOrSlug, imageURL, index, link } = args;

      const original = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, original);

      const tournament: IERenaTournamentPopulated = await erenaTournamentService.addOrRemoveAd(
        original,
        userId,
        Number(index),
        true,
        imageURL,
        link
      );

      return tournament;
    },
    removeTournamentAd: async (
      _,
      args: { tournamentIdOrSlug: string; index: number },
      context: Context
    ): Promise<IERenaTournamentPopulated> => {
      const userId = context.user._id;
      const { tournamentIdOrSlug, index } = args;

      const original = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, original);

      const tournament: IERenaTournamentPopulated = await erenaTournamentService.addOrRemoveAd(
        original,
        userId,
        Number(index),
        false
      );

      return tournament;
    }
  },
  ERenaTournamentStatus: {
    scheduled: "Scheduled",
    active: "Active",
    finished: "Finished",
    deleted: "Deleted"
  },
  ERenaTournament: {
    ownerType: (parent: IERenaTournament): string | null => {
      return ERenaTournamentService.getOwnerType(parent);
    },
    owner: async (parent: IERenaTournament): Promise<IUser | IOrganization> => {
      const ownerId = parent?.owner || parent?.organization;
      const ownerType = ERenaTournamentService.getOwnerType(parent);

      const result = await (ownerType === ERenaTournamentOwnerKinds.org
        ? organizationService.findOne({ _id: ownerId }, {}, { lean: true })
        : userService.findOne({ _id: ownerId }, {}, { lean: true }));

      return <IUser | IOrganization>result;
    },
    opportunity: async (parent: IERenaTournament): Promise<IOpportunity | null> => {
      if (parent.opportunity) {
        const opportunity: IOpportunity | null = await opportunityService.findOne({ _id: parent.opportunity });
        return opportunity;
      }

      return null;
    }
  }
};
