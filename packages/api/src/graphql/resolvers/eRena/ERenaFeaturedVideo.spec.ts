import { ERenaFeaturedVideoResolver } from "./ERenaFeaturedVideo";
import { ERenaFeaturedVideoService } from "../../../lib/erena/erena-featured-video.service";

jest.mock("../../../lib/erena/erena-featured-video.service");

ERenaFeaturedVideoService.prototype.getFeaturedVideos = jest
  .fn()
  .mockResolvedValue([{ title: "Die Hard" }, { title: "Amelie" }]);
describe("ERenaFeaturedVideo", () => {
  it("getFeaturedVideos makes the correct service call", async () => {
    await ERenaFeaturedVideoResolver.Query.getERenaFeaturedVideos();

    expect(ERenaFeaturedVideoService.prototype.getFeaturedVideos).toHaveBeenCalled();
  });
});
