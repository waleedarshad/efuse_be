import { Failure } from "@efuse/contracts";
import { FilterQuery } from "mongoose";
import { ERenaScoreOwnerKinds } from "../../../backend/interfaces/erena/erena-enums";
import { IERenaScore } from "../../../backend/interfaces/erena/erena-score";
import { ERenaPlayerService } from "../../../lib/erena/erena-player.service";
import { ERenaScoreService } from "../../../lib/erena/erena-score.service";

import { IERenaLeaderboardPlayer, IERenaPlayer, IERenaTeam, IERenaTournament } from "../../../backend/interfaces";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTeamService } from "../../../lib/erena/erena-team.service";
import { Context } from "../../schema/context";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";

const eRenaTeamService = new ERenaTeamService();
const erenaSharedService = new ERenaSharedService();
const eRenaScoreService = new ERenaScoreService();

export const ERenaTeamResolver = {
  ERenaTournament: {
    teams: async (parent: IERenaTournament): Promise<IERenaTeam[]> => {
      const tournament = parent;

      const queryValue = {
        tournament: tournament?._id
      };

      /*
        -Will need to make a change later to sort by a different field other than _id.
        -Sorting by createdAt does not work, as most of these teams are essentially created simultaneously and have the same timestamp.
        -We may need to add a new field to teams to keep the original order, which may require some front end work as well.
      */
      const staffs = await eRenaTeamService.find(queryValue, {}, { lean: true, sort: { _id: 1 } });
      return staffs;
    }
  },

  Query: {
    getErenaTeam: async (_, args): Promise<IERenaTeam | null> => {
      const { id }: { id: string } = args;

      if (!id) {
        throw Failure.BadRequest("Missing param id");
      }
      const team = await eRenaTeamService.findOne({ _id: id }, {}, { lean: 1 });
      return team;
    }
  },
  Mutation: {
    createErenaTeam: async (
      _,
      args: { tournamentIdOrSlug: string; body: IERenaTeam },
      context: Context
    ): Promise<IERenaTeam> => {
      const userId = context.user._id;
      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      body.tournament = tournament._id;

      const team = await eRenaTeamService.create(body);

      if (!team) {
        throw Failure.InternalServerError("Failed to create team");
      }

      return team;
    },
    updateErenaTeam: async (_, args: { id: string; body: IERenaTeam }, context: Context): Promise<IERenaTeam> => {
      const userId = context.user._id;
      const { body, id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const original = await eRenaTeamService.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      // Only update the name and type
      const updateData = {
        name: body.name,
        type: body.type
      };

      const updated = await eRenaTeamService.update(id, updateData);

      if (!updated) {
        throw Failure.NotFound("Failed to update team");
      }

      return updated;
    },
    deleteErenaTeam: async (_, args: { id: string }, context: Context): Promise<IERenaTeam> => {
      const userId = context.user._id;
      const { id } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const team = await eRenaTeamService.findById(id);

      if (!team) {
        throw Failure.NotFound("Could not find team to delete");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(team.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const entity = await eRenaTeamService.deleteTeamAndPlayers(userId, id);

      if (!entity) {
        throw Failure.NotFound("Failed to delete Team");
      }

      return entity;
    }
  },

  ERenaTeam: {
    players: async (parent: IERenaTeam): Promise<IERenaPlayer[]> => {
      const eRenaPlayerService = new ERenaPlayerService();

      const players: IERenaPlayer[] = await eRenaPlayerService.getByTeam(parent._id);

      players.forEach((player) => (player.matchId = parent.matchId));

      return players;
    },
    teamScore: async (parent: IERenaTeam): Promise<number> => {
      const team = parent;
      const scoreQuery: FilterQuery<IERenaScore> = {
        ownerId: team._id,
        ownerKind: ERenaScoreOwnerKinds.team,
        match: team.matchId
      };

      const teamScore = <IERenaScore>await eRenaScoreService.findOne(scoreQuery, {}, { lean: true });

      // get the team's score
      return teamScore?.score ? teamScore.score : 0;
    },
    summedPlayerScore: async (parent: IERenaTeam): Promise<number> => {
      const team = parent;
      const eRenaPlayerService = new ERenaPlayerService();
      const players: IERenaPlayer[] = await eRenaPlayerService.getByTeam(team._id);

      const scoreQuery: FilterQuery<IERenaScore> = {
        tournament: team.tournament,
        ownerKind: ERenaScoreOwnerKinds.player,

        ownerId: { $in: players.map((player) => player._id) },
        match: parent.matchId
      };

      const teamPlayersScore = await eRenaScoreService.find(scoreQuery, {}, { lean: true });

      // map scores to player
      const teamPlayerScores = players.map((player: IERenaPlayer) => {
        const returnPlayer = <IERenaLeaderboardPlayer>player;

        // get all scores for the player
        const playerScores = teamPlayersScore
          .filter((s: IERenaScore) => s.ownerId.equals(player._id))
          .map((s) => s.score);

        returnPlayer.score = playerScores?.length > 0 ? playerScores.reduce((a, b) => a + b) : 0;

        return returnPlayer;
      });

      // sum up the player scores
      const rawScoreValues = teamPlayerScores.map((s) => s.score);

      return rawScoreValues?.length > 0 ? rawScoreValues.reduce((a, b) => a + b) : 0;
    },
    tournament: async (parent: IERenaTeam): Promise<IERenaTournament | null> => {
      const tournamentService = new ERenaTournamentService();
      const tournament = await tournamentService.findOne({ _id: parent.tournament }, {}, { lean: 1 });

      return tournament;
    }
  }
};
