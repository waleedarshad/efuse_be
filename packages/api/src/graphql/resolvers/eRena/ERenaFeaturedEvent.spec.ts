import { ERenaFeaturedEventResolver } from "./ERenaFeaturedEvent";
import { ErenaFeaturedEventService } from "../../../lib/erena/erena-featured-event.service";

jest.mock("../../../lib/erena/erena-featured-event.service");

describe("ErenaFeaturedEvent", () => {
  ErenaFeaturedEventService.prototype.getFeaturedEvent = jest
    .fn()
    .mockResolvedValue({ tournamentName: "cool-tournament" });

  it("makes the correct service call", async () => {
    await ERenaFeaturedEventResolver.Query.getFeaturedEvent();

    expect(ErenaFeaturedEventService.prototype.getFeaturedEvent).toHaveBeenCalled();
  });
});
