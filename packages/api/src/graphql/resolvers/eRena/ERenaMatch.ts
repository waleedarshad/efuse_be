import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { Context } from "../../schema/context";
import { IERenaMatch, IERenaTournament, IERenaMatchCreateParams, File, IERenaTeam } from "../../../backend/interfaces";
import { ERenaMatchService } from "../../../lib/erena/erena-match.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { ERenaTeamService } from "../../../lib/erena/erena-team.service";

const eRenamatchService = new ERenaMatchService();
const erenaSharedService = new ERenaSharedService();
const erenaTeamService = new ERenaTeamService();

export const ERenaMatchResolver = {
  ERenaTournament: {
    matches: async (parent: IERenaTournament, args: { roundId: string }): Promise<IERenaMatch[]> => {
      const matches = await eRenamatchService.findByTournament(parent.tournamentId, args.roundId);
      return matches;
    }
  },

  Query: {
    getMatch: async (_, args): Promise<IERenaMatch> => {
      const { id }: { id: string } = args;

      if (!id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const match = await eRenamatchService.findById(id);
      return match;
    }
  },

  Mutation: {
    createMatch: async (
      _,
      args: { tournamentIdOrSlug: string; body: IERenaMatchCreateParams },
      context: Context
    ): Promise<IERenaMatch> => {
      const userId = context.user._id;

      const { body, tournamentIdOrSlug } = args;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const match = await eRenamatchService.createMatch(userId, tournamentIdOrSlug, body);

      if (!match) {
        throw Failure.InternalServerError("Failed to create match");
      }

      return match;
    },
    updateMatch: async (
      _,
      args: { id: string; body: IERenaMatch },
      context: Context
    ): Promise<{ currentMatch: IERenaMatch; nextMatch?: IERenaMatch }> => {
      const userId = context.user._id;
      const { id, body } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const existingMatch = await eRenamatchService.findById(id);
      if (!existingMatch) {
        throw Failure.UnprocessableEntity("Unable to find match");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(
        (<Types.ObjectId>existingMatch.tournament).toHexString()
      );
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const match = await eRenamatchService.updateMatch(id, body);

      return match;
    },
    saveScreenShot: async (
      _,
      args: { id: string; body: { teamOneImage: File; teamTwoImage: File } },
      context: Context
    ): Promise<IERenaMatch> => {
      const userId = context.user._id;

      const { id, body } = args;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      const existingMatch = await eRenamatchService.findById(id);
      if (!existingMatch) {
        throw Failure.UnprocessableEntity("Unable to find match");
      }

      const tournament = await erenaSharedService.getTournamentByIdOrSlug(
        (<Types.ObjectId>existingMatch.tournament).toHexString()
      );
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      await erenaSharedService.isAuthorizedUser(userId, tournament);

      const match = await eRenamatchService.uploadScreenshot(userId, id, body.teamOneImage, body.teamTwoImage);

      return match;
    }
  },

  ERenaMatch: {
    teams: async (parent: IERenaMatch): Promise<IERenaTeam[]> => {
      if ("team" in parent.teams[0]) {
        // The order of the array set here determines the sorting order.
        const teamsOrder = parent.teams.map((team) => Types.ObjectId(team.team.toString()));

        const teams = await erenaTeamService
          .getAggregate()
          .match({ _id: { $in: teamsOrder } })
          .addFields({ __order: { $indexOfArray: [teamsOrder, "$_id"] } })
          .sort({ __order: 1 })
          // This removes our temporary field from the final object.
          .project({ __order: 0 });

        teams.forEach((team) => (team.matchId = parent._id));

        return teams;
      }
      return [];
    },
    tournament: async (parent: IERenaMatch): Promise<IERenaTournament | null> => {
      const erenaTournamentService = new ERenaTournamentService();

      const tournament = await erenaTournamentService.findOne({ _id: parent.tournament }, {}, { lean: true });

      return tournament;
    },
    winner: async (parent: IERenaMatch): Promise<IERenaTeam | null> => {
      const erenaTeam = await erenaTeamService.findOne({ _id: parent.winner }, {}, { lean: true });

      return erenaTeam;
    }
  }
};
