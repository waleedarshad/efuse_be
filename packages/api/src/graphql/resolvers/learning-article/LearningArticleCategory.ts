import { ILearningArticle } from "../../../backend/interfaces";
import { ILearningArticleCategory } from "../../../backend/interfaces/learning-article/learning-article-category";
import { LearningArticleCategory } from "../../../backend/models/LearningArticleCategory";

export const LearningArticleCategoryResolver = {
  Query: {
    getCategories: async (): Promise<ILearningArticleCategory[]> => {
      return <ILearningArticleCategory[]>await LearningArticleCategory.find({ isActive: true });
    }
  },
  LearningArticle: {
    category: async (parent: ILearningArticle): Promise<ILearningArticleCategory> => {
      const categoryObject = <ILearningArticleCategory>await LearningArticleCategory.findOne({
        _id: parent.category
      }).lean();

      return categoryObject;
    },
    url: async (parent: ILearningArticle): Promise<string> => {
      const category = <ILearningArticleCategory>await LearningArticleCategory.findOne({
        _id: parent.category
      }).lean();

      return `/learning/${category.slug}/${parent.slug}`;
    }
  }
};
