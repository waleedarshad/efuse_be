import { LearningArticleResolver } from "./LearningArticle";
import { LearningArticleCategoryResolver } from "./LearningArticleCategory";

export const LearningArticleResolvers = [LearningArticleResolver, LearningArticleCategoryResolver];
