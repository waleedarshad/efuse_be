import { UserService } from "../../../lib/users/user.service";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { ILearningArticle, IOrganization } from "../../../backend/interfaces";
import { IUser } from "../../../backend/interfaces/user";
import { LearningArticleAuthorTypeEnum } from "../../../lib/learningArticles/learning-article.enums";
import { RegisteredObjectTypes } from "../../enums";
import { LearningArticleGameService } from "../../../lib/learningArticles/learning-article-game.service";
import { Game } from "../../../backend/models/game.model";

export const LearningArticleResolver = {
  Author: {
    __resolveType(author: IUser | IOrganization | null) {
      return author?.kind || RegisteredObjectTypes.USER;
    }
  },

  LearningArticle: {
    author: async (parent: ILearningArticle): Promise<IUser | IOrganization | null> => {
      const organizationService = new OrganizationsService();
      const userService = new UserService();

      let result: IUser | IOrganization | null = null;

      switch (parent.authorType) {
        case LearningArticleAuthorTypeEnum.ORGANIZATION:
          result = await organizationService.findOne({ _id: parent.author });

          if (result) {
            result.kind = RegisteredObjectTypes.ORGANIZATION;
          }

          break;
        case LearningArticleAuthorTypeEnum.USER:
          result = await userService.findOne({ _id: parent.author });

          if (result) {
            result.kind = RegisteredObjectTypes.USER;
          }

          break;
        default:
          break;
      }

      return result;
    },
    associatedGames: async (parent: ILearningArticle): Promise<Game[] | null> => {
      const learningArticleGameService = new LearningArticleGameService();

      return await learningArticleGameService.getAssociatedGames(parent._id);
    }
  }
};
