import { DocumentType } from "@typegoose/typegoose";
import { GamePlatformsService } from "../../../lib/game_platforms/game-platforms.service";
import { GamePlatform } from "../../../backend/models/game-platform.model";
import { IUser } from "../../../../src/backend/interfaces";
import { IResolvers } from "graphql-tools";

export class GamePlatformResolver {
  private static $gamePlatformService = new GamePlatformsService();

  static get resolve(): IResolvers {
    return {
      User: {
        gamePlatforms: async (parent: IUser): Promise<DocumentType<GamePlatform>[]> => {
          const gamePlatforms = await this.$gamePlatformService.getGamePlatforms(parent._id);
          return gamePlatforms;
        }
      }
    };
  }
}
