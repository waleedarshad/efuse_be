import { IUser } from "../../../backend/interfaces/user";
import { IRiotValorantProfile } from "../../../backend/interfaces/riot/riot-valorant-profile";
import { RiotProfileService } from "../../../lib/riot/riot-profile/riot-profile.service";

const riotProfileService = new RiotProfileService();

export const ValorantProfileResolver = {
  User: {
    valorantAccountProfile: async (parent: IUser): Promise<IRiotValorantProfile | null> => {
      const profile = await riotProfileService.findOne({ owner: parent._id }, ["valorantProfile"], {
        sort: { createdAt: -1 }
      });

      return profile?.valorantProfile || null;
    }
  }
};
