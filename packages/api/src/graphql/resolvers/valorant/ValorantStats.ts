import { Failure } from "@efuse/contracts";
import { IUser } from "../../../backend/interfaces/user";
import { IValorantStats } from "../../../backend/interfaces/valorant/valorant-stats";
import { ValorantStatsService } from "../../../lib/valorant/valorant-stats.service";

const valorantStatsService = new ValorantStatsService();

export const ValorantStatsResolver = {
  User: {
    valorantStats: async (parent: IUser): Promise<IValorantStats | null> => {
      const stats = await valorantStatsService.findOne({ user: parent._id }, [], { sort: { createdAt: -1 } });

      return stats;
    }
  },
  Query: {
    getValorantStats: async (_, args): Promise<IValorantStats | null> => {
      const { userId }: { userId: string } = args;
      if (!userId) {
        throw Failure.BadRequest("Missing id parameter");
      }
      const query = { user: userId };

      const stats = await valorantStatsService.findOne(query);

      return stats;
    }
  }
};
