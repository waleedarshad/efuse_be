import { ValorantStatsResolver } from "./ValorantStats";
import { ValorantProfileResolver } from "./ValorantProfile";

export const ValorantResolvers = [ValorantStatsResolver, ValorantProfileResolver];
