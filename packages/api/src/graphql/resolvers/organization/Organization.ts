import { DOMAIN } from "@efuse/key-store";
import { IResolvers } from "graphql-tools";
import { InviteCodeType } from "@efuse/entities";
import { Logger } from "@efuse/logger";

import {
  IOrganization,
  IUser,
  IGraphQLContext,
  IOrganizationRequest,
  IOrganizationFollower
} from "../../../backend/interfaces";

import { IMember } from "../../../backend/interfaces/member";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { getOrganization } from "../../../backend/helpers/feed.helper";
import { IFeed } from "../../../backend/interfaces/feed";
import { IFeedOrganization } from "../../../backend/interfaces/feed/feed-organization";
import { UserService } from "../../../lib/users/user.service";
import { Failure } from "@efuse/contracts";
import { MemberService } from "../../../lib/member/member.service";
import { OrganizationRequestBaseService } from "../../../lib/organizations/organization-request-base.service";
import { OrganizationFollowerBaseService } from "../../../lib/organizations/organization-follower-base.service";
import { OrganizationGameService } from "../../../lib/organizations/organization-game.service";
import { InviteCodeService } from "../../../lib/invite-code/invite-code.service";
import { GameBaseService } from "../../../lib/game/game-base.service";
import { MemberBusinessRulesService } from "../../../lib/member/member-business-rules.service";

import { ObjectId } from "../../../backend/types";
import { Slack } from "../../../lib/slack";

const logger = Logger.create();

interface ICreateOrganizationExtended extends Omit<IOrganization, "associatedGames"> {
  associatedGames: ObjectId[];
}

interface ICreateOrganizationArgs {
  organization: ICreateOrganizationExtended;
}

export class OrganizationResolver {
  private static organizationService = new OrganizationsService();
  private static userService: UserService = new UserService();
  private static memberService: MemberService = new MemberService();
  private static organizationRequestBaseService: OrganizationRequestBaseService = new OrganizationRequestBaseService();
  private static organizationFollowerBaseService: OrganizationFollowerBaseService =
    new OrganizationFollowerBaseService();
  private static organizationGameService: OrganizationGameService = new OrganizationGameService();
  private static inviteCodeService: InviteCodeService = new InviteCodeService();
  private static memberBusinessRulesService: MemberBusinessRulesService = new MemberBusinessRulesService();
  private static gameBaseService: GameBaseService = new GameBaseService();

  public static get resolve(): IResolvers {
    return {
      Query: {
        getUserOrganizations: async (_, args) => this.getUserOrganizations(args),
        getOrganizationById: async (_, args) => this.getOrganizationById(_, args)
      },
      Mutation: {
        createOrganization: async (_, args, context) => this.createOrganization(_, args, context)
      },

      Organization: {
        user: async (parent) => this.resolveUser(parent),
        captains: async (parent) => this.resolveCaptains(parent),
        url: (parent): string => this.resolveUrl(parent),
        isCurrentUserAMember: async (parent, _, context) => this.resolveIsCurrentUserMember(parent, _, context),
        currentUserRequestToJoin: async (parent, _, context) =>
          this.resolveCurrentUserRequestToJoin(parent, _, context),
        membersCount: async (parent) => this.resolveMemberCount(parent),
        isCurrentUserAFollower: async (parent, _, context) => this.resolveIsCurrentUserFollower(parent, _, context)
      },
      // "Post" is an object we have and not the action.
      Post: {
        organization: async (parent) => this.resolveOrganizationForPost(parent)
      }
    };
  }

  private static async createOrganization(
    _,
    args: ICreateOrganizationArgs,
    context: IGraphQLContext
  ): Promise<IOrganization> {
    const { organization } = args;
    const { userId, user } = context;

    try {
      const games = organization.associatedGames;
      const newOrg: Partial<IOrganization> = {
        name: organization.name,
        shortName: organization.shortName,
        organizationType: organization.organizationType,
        status: organization.status,
        user: userId
      };

      const savedOrganization = await this.organizationService.createOrganization(user, newOrg, games);

      if (process.env.NODE_ENV === "production") {
        // send slack message to #organization-feed channel
        Slack.sendMessage(
          `New organization created!\n\n${savedOrganization.name} - https://efuse.gg/org/${savedOrganization.shortName}\nCreated by https://efuse.gg/users/portfolio?id=${userId}`,
          "#organization-feed"
        ).catch((error) => logger.error(error, "Error resolving sendMessage promise"));
      }

      const createInviteCodeForOrgMembers = this.inviteCodeService.create({
        type: InviteCodeType.ORGANIZATION_MEMBER_INVITE,
        organization: savedOrganization._id
      });

      const createInviteCodeForOrgFollowers = this.inviteCodeService.create({
        type: InviteCodeType.ORGANIZATION,
        organization: savedOrganization._id
      });

      const setOrganizationOwnerAsMemberOfOrg = this.memberBusinessRulesService.createMemberOnSignupAsync(
        savedOrganization._id,
        userId
      );

      await Promise.allSettled([
        createInviteCodeForOrgMembers,
        createInviteCodeForOrgFollowers,
        setOrganizationOwnerAsMemberOfOrg
      ]);

      return savedOrganization;
    } catch (error) {
      throw Failure.InternalServerError("Failed to create orgnization.", error);
    }
  }

  private static async getOrganizationById(_, args: { organizationId: ObjectId }): Promise<IOrganization | null> {
    const { organizationId } = args;

    const organization = await this.organizationService.findOne({ _id: organizationId }, {}, { lean: true });

    return organization;
  }

  private static async resolveCaptains(parent: IOrganization): Promise<IUser[]> {
    // TODO: [perms] Use permissions system
    const users = await this.userService.find({ _id: { $in: parent.captains } }, {}, { lean: true });

    return users;
  }

  private static async resolveCurrentUserRequestToJoin(
    parent: IOrganization,
    _,
    context: IGraphQLContext
  ): Promise<IOrganizationRequest | null> {
    if (context.userId) {
      const request = await this.organizationRequestBaseService.findOne({
        organization: parent._id,
        user: context.userId,
        deleted: { $ne: true }
      });

      return request;
    }

    return null;
  }

  private static async resolveIsCurrentUserFollower(
    parent: IOrganization,
    _,
    context: IGraphQLContext
  ): Promise<IOrganizationFollower | null> {
    if (context.userId) {
      const follower = await this.organizationFollowerBaseService.findOne(
        {
          followee: parent._id,
          follower: context.userId,
          deleted: { $ne: true }
        },
        {},
        { lean: true }
      );

      return follower;
    }

    return null;
  }

  private static async resolveIsCurrentUserMember(
    parent: IOrganization,
    _,
    context: IGraphQLContext
  ): Promise<IMember | null> {
    if (context.userId) {
      const member = await this.memberService.findOne(
        { organization: parent._id, user: context.userId, deleted: { $ne: true } },
        {},
        { lean: true }
      );

      return member;
    }

    return null;
  }

  private static async resolveMemberCount(parent: IOrganization): Promise<number> {
    const count = await this.memberService.countDocuments({ organization: parent._id });

    return count;
  }

  // "Post" is object not action
  private static async resolveOrganizationForPost(parent: IFeed): Promise<IFeedOrganization | null> {
    if (parent.kind === "organizations") {
      const organization = await this.organizationService.findOne(
        {
          _id: parent.kindId
        },
        {},
        { lean: true }
      );

      return getOrganization(organization);
    }

    return null;
  }

  private static async resolveUser(parent: IOrganization): Promise<IUser | null> {
    const user = await this.userService.findOne({ _id: parent.user }, {}, { lean: true });

    return user;
  }

  private static resolveUrl(parent: IOrganization): string {
    return `${DOMAIN}/org/${parent.shortName}`;
  }

  private static async getUserOrganizations(args: {
    userId: ObjectId;
    includeOrgsWhereMember: boolean;
  }): Promise<IOrganization[]> {
    const { includeOrgsWhereMember, userId } = args;

    const foundOrganizations = await this.organizationService.getOrganizationsByUserId(userId, includeOrgsWhereMember);

    return foundOrganizations;
  }

  private static async checkPermissionsAndThrowError(userId, owner): Promise<boolean | never> {
    // This will throw an error if the user does not have the correct permissions.
    if (userId === owner) {
      throw Failure.Forbidden("User does not have permission to perform this action.");
    }

    return true;
  }
}
