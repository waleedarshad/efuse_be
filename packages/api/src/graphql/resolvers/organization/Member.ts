import { IResolvers } from "graphql-tools";
import { ObjectId } from "../../../backend/types";
import { MemberService } from "../../../lib/member/member.service";

import { IOrganization, IUser } from "../../../backend/interfaces";

import { IMember } from "../../../backend/interfaces/member";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";

export class MemberResolver {
  private static memberService = new MemberService();
  private static organizationService = new OrganizationsService();
  private static userService: UserService = new UserService();

  static get resolve(): IResolvers {
    return {
      Query: {
        getOrganizationMemberById: async (_, args) => this.getOrganizationMemberById(args),
        getOrganizationMembers: async (_, args) => this.getOrganizationMembers(args)
      },
      Member: {
        organization: async (parent) => this.resolveOrganization(parent),
        user: async (parent) => this.resolveUser(parent)
      },
      Organization: {
        members: async (parent) => this.resolveMembersForOrganization(parent)
      }
    };
  }

  private static async getOrganizationMemberById(args: { memberId: ObjectId }): Promise<IMember | null> {
    const { memberId } = args;

    const member = await this.memberService.findOne({ _id: memberId }, {}, { lean: true });

    return member;
  }

  private static async getOrganizationMembers(args: { orgId: ObjectId }): Promise<IMember[]> {
    const { orgId } = args;

    const members = await this.memberService.find({ organization: orgId }, {}, { lean: true });

    return members;
  }

  private static async resolveMembersForOrganization(parent: IOrganization): Promise<IMember[]> {
    const members = await this.memberService.find({ organization: parent._id }, {}, { lean: true });

    return members;
  }

  private static async resolveOrganization(parent: IMember): Promise<IOrganization | null> {
    const organization = await this.organizationService.findOne({ _id: parent.organization }, {}, { lean: true });

    return organization;
  }

  private static async resolveUser(parent: IMember): Promise<IUser | null> {
    const user = await this.userService.findOne({ _id: parent.user }, {}, { lean: true });

    return user;
  }
}
