import { IResolvers } from "graphql-tools";

import { IOrganization, IOrganizationRequest, IUser } from "../../../backend/interfaces";

import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";

export class OrganizationFollowerResolver {
  private static organizationService = new OrganizationsService();
  private static userService: UserService = new UserService();

  public static get resolve(): IResolvers {
    return {
      OrganizationRequest: {
        organization: async (parent) => this.resolveOrganization(parent),
        user: async (parent) => this.resolveUser(parent)
      }
    };
  }

  private static async resolveOrganization(parent: IOrganizationRequest): Promise<IOrganization | null> {
    const organization = await this.organizationService.findOne({ _id: parent.organization }, {}, { lean: true });

    return organization;
  }

  private static async resolveUser(parent: IOrganizationRequest): Promise<IUser | null> {
    const user = await this.userService.findOne({ _id: parent.user }, {}, { lean: true });

    return user;
  }
}
