import { IResolvers } from "graphql-tools";

import { IOrganization, IOrganizationFollower, IUser } from "../../../backend/interfaces";

import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";

export class OrganizationRequestResolver {
  private static organizationService = new OrganizationsService();
  private static userService: UserService = new UserService();

  public static get resolve(): IResolvers {
    return {
      OrganizationFollower: {
        follower: async (parent) => this.resolveFollower(parent),
        followee: async (parent) => this.resolveFollowee(parent)
      }
    };
  }

  private static async resolveFollower(parent: IOrganizationFollower): Promise<IUser | null> {
    const user = await this.userService.findOne({ _id: parent.follower }, {}, { lean: true });

    return user;
  }

  private static async resolveFollowee(parent: IOrganizationFollower): Promise<IOrganization | null> {
    const organization = await this.organizationService.findOne({ _id: parent.followee }, {}, { lean: true });

    return organization;
  }
}
