import { MemberResolver } from "./Member";
import { OrganizationFollowerResolver } from "./OrganizationFollower";
import { OrganizationRequestResolver } from "./OrganizationRequest";
import { OrganizationResolver } from "./Organization";

export const OrganizationResolvers = [
  OrganizationResolver.resolve,
  MemberResolver.resolve,
  OrganizationFollowerResolver.resolve,
  OrganizationRequestResolver.resolve
];
