import { Failure } from "@efuse/contracts";
import { IResolvers } from "graphql-tools";
import { FilterQuery, PaginateResult, Types } from "mongoose";
import { NoteOwnerType, NoteRelatedModel } from "@efuse/entities";
import { IApplicant, IGraphQLContext, IOrganization, IUser } from "../../../backend/interfaces";
import { UserService } from "../../../lib/users/user.service";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { NoteOwnerTypeDef, NoteRelatedDocTypeDef } from "../../enums";
import { IRecruitmentProfile } from "../../../backend/interfaces/recruitment-profile";
import { ApplicantService } from "../../../lib/applicants/applicant.service";
import { RecruitmentProfileService } from "../../../lib/recruitment/recruitment-profile.service";
import { NotesService } from "../../../lib/notes/notes.service";
import { INote } from "../../../backend/interfaces/note";
import { OrganizationACLService } from "../../../lib/organizations/organization-acl.service";
import { OrganizationAccessTypes } from "../../../lib/organizations/organization-access-types";
import { canRecruit } from "../../../lib/recruitment/utils";

type NoteOwner = IUser | IOrganization | null;
type NoteRelatedDoc = IApplicant | IRecruitmentProfile | null;
type ObjectId = Types.ObjectId | string;

interface CreateNoteArgs {
  note: INote;
}

interface UpdateNoteArgs {
  noteId: ObjectId;
  content: string;
}

interface NoteIdArgs {
  noteId: ObjectId;
}

interface PaginatedNoteArgs {
  page: number;
  limit: number;
  relatedDoc: ObjectId;
  owner: ObjectId;
}

interface GetNoteArgs {
  relatedDoc: ObjectId;
  owner: ObjectId;
}

export class NoteResolver {
  public static noteService: NotesService = new NotesService();
  private static userService: UserService = new UserService();

  static get resolve(): IResolvers {
    return {
      Query: {
        getNote: async (_, args: NoteIdArgs): Promise<INote | null> => {
          const { noteId } = args;

          try {
            const note = await this.noteService.findOne({ _id: noteId }, {}, { lean: true });

            return note;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve Notes", error);
          }
        },
        getNotes: async (_, args: GetNoteArgs): Promise<INote[]> => {
          const { relatedDoc, owner } = args;
          const filter: FilterQuery<INote> = {
            owner,
            relatedDoc
          };

          try {
            const note = await this.noteService.find(filter, {}, { lean: true });

            return note;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve Notes", error);
          }
        },
        getPaginatedNotes: async (_, args: PaginatedNoteArgs): Promise<PaginateResult<INote>> => {
          const { page, limit, relatedDoc, owner } = args;

          try {
            const paginatedNotes = await this.noteService.getPaginatedNotes(relatedDoc, owner, page, limit);

            return paginatedNotes;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve paginated notes", error);
          }
        }
      },

      Mutation: {
        createNote: async (_, args: CreateNoteArgs, context: IGraphQLContext): Promise<INote> => {
          const { note } = args;
          const { owner, ownerType, relatedDoc, relatedModel } = note;

          try {
            // will check for issues and appropriate permissions and throw error if there is an issue
            await this.CheckTypesAndCheckForIssues(context.userId, owner, relatedDoc, ownerType, relatedModel);

            const savedNote = await this.noteService.create(note);

            return savedNote;
          } catch (error) {
            throw Failure.InternalServerError("Could not create note", error);
          }
        },

        deleteNote: async (_, args: NoteIdArgs, context: IGraphQLContext): Promise<boolean> => {
          const { noteId } = args;
          const adjustOutput = {
            _id: false,
            owner: true,
            relatedDoc: true,
            ownerType: true,
            relatedModel: true
          };

          try {
            const foundLeanNote = await this.noteService.findOne({ _id: noteId }, adjustOutput, {
              lean: true
            });

            if (!foundLeanNote) {
              throw Failure.NotFound("Note could not be deleted because it does not exist", noteId);
            }

            const { owner, relatedDoc, ownerType, relatedModel } = foundLeanNote;

            // will check for issues and appropriate permissions and throw error if there is an issue
            await this.CheckTypesAndCheckForIssues(context.userId, owner, relatedDoc, ownerType, relatedModel);

            const deleted = await this.noteService.delete(noteId);

            return deleted;
          } catch (error) {
            throw Failure.InternalServerError("Could not delete note", error);
          }
        },

        updateNote: async (_, args: UpdateNoteArgs, context: IGraphQLContext): Promise<INote> => {
          const { noteId, content } = args;
          const adjustOutput = {
            _id: false,
            owner: true,
            relatedDoc: true,
            ownerType: true,
            relatedModel: true
          };

          try {
            const foundLeanNote = await this.noteService.findOne({ _id: noteId }, adjustOutput, {
              lean: true
            });

            if (!foundLeanNote) {
              throw Failure.NotFound("Note could not be deleted because it does not exist", noteId);
            }

            const { owner, relatedDoc, ownerType, relatedModel } = foundLeanNote;
            // will check for issues and appropriate permissions and throw error if there is an issue
            await this.CheckTypesAndCheckForIssues(context.userId, owner, relatedDoc, ownerType, relatedModel);

            const savedNote = await this.noteService.updateOne({ _id: noteId }, { content });

            return savedNote;
          } catch (error) {
            throw Failure.InternalServerError("Could not update note", error);
          }
        }
      },

      // Resolve Union
      NoteOwner: {
        async __resolveType(parent: NoteOwner): Promise<NoteOwnerTypeDef> {
          if (!parent) {
            throw Failure.ExpectationFailed("Could not determine owner type", parent);
          }

          if (await NoteResolver.userService.exists(parent._id)) {
            return NoteOwnerTypeDef.USER;
          }

          return NoteOwnerTypeDef.ORGANIZATION;
        }
      },

      // Resolve Union
      NoteRelatedDoc: {
        __resolveType(parent: NoteRelatedDoc): NoteRelatedDocTypeDef {
          // should only exist on applicants
          if ((<IApplicant>parent)?.candidateQuestions) {
            return NoteRelatedDocTypeDef.APPLICANT;
          }

          // should only exist on recruiment profiles
          if ((<IRecruitmentProfile>parent)?.graduationClass) {
            return NoteRelatedDocTypeDef.RECRUITMENT_PROFILE;
          }

          throw Failure.ExpectationFailed("Could not determine note related model", parent);
        }
      },

      // Resolve Objects in notes
      Note: {
        author: async (parent: INote): Promise<IUser | null> => {
          const { author }: { author: ObjectId } = parent;

          try {
            const foundUser: IUser | null = await this.userService.findOne({ _id: author }, {}, { lean: true });

            return foundUser;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve author data for note", error);
          }
        },

        owner: async (parent: INote): Promise<NoteOwner> => {
          const { owner }: { owner: ObjectId } = parent;

          const organizationService = new OrganizationsService();

          try {
            let foundDoc: NoteOwner;

            switch (parent.ownerType) {
              case NoteOwnerType.ORGANIZATIONS:
                foundDoc = await organizationService.findOne({ _id: owner }, {}, { lean: true });
                break;
              case NoteOwnerType.USERS:
                foundDoc = await this.userService.findOne({ _id: owner }, {}, { lean: true });
                break;
              default:
                throw Failure.ExpectationFailed(
                  "Could not determine which type of document to retrieve",
                  parent.ownerType
                );
            }

            return foundDoc;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve owner data for note", error);
          }
        },

        relatedDoc: async (parent: INote): Promise<NoteRelatedDoc> => {
          const { relatedDoc }: { relatedDoc: ObjectId } = parent;

          const applicantService = new ApplicantService();
          const recruitmentProfileService = new RecruitmentProfileService();

          let foundDoc: NoteRelatedDoc;

          try {
            switch (parent.relatedModel) {
              case NoteRelatedModel.APPLICANTS:
                foundDoc = await applicantService.findOne({ _id: relatedDoc }, {}, { lean: true });
                break;
              case NoteRelatedModel.RECRUITMENT_PROFILES:
                foundDoc = await recruitmentProfileService.findOne({ _id: relatedDoc }, {}, { lean: true });
                break;
              default:
                throw Failure.ExpectationFailed(
                  "Could not determine which type of document to retrieve",
                  parent.relatedModel
                );
            }

            return foundDoc;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve related doc data for note", error);
          }
        }
      }
    };
  }

  private static async CheckTypesAndCheckForIssues(
    currentUserId: ObjectId,
    owner: ObjectId,
    relatedDoc: ObjectId,
    ownerType: NoteOwnerType,
    relatedModel: NoteRelatedModel
  ): Promise<void> {
    switch (relatedModel) {
      case NoteRelatedModel.RECRUITMENT_PROFILES:
        await this.ValidateUserAuthorizedInOrganization(currentUserId, owner, ownerType);

        // todo: investigate
        // @ts-ignore
        await canRecruit(currentUserId);
        break;
      case NoteRelatedModel.APPLICANTS:
        await this.ValidateUserAuthorizedInOrganization(currentUserId, owner, ownerType);
        break;
      default:
        Failure.ExpectationFailed("Could not determine type of model", {
          currentUserId,
          owner,
          relatedDoc,
          ownerType,
          relatedModel
        });
    }
  }

  private static async ValidateUserAuthorizedInOrganization(
    currentUserId: ObjectId,
    owner: ObjectId,
    ownerType: NoteOwnerType
  ): Promise<void> {
    if (ownerType === NoteOwnerType.ORGANIZATIONS) {
      const organizationACLService = new OrganizationACLService();
      await organizationACLService.authorizedToAccess(currentUserId, owner, OrganizationAccessTypes.ANY);
    }
  }
}
