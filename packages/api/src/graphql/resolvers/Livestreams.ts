import { UserService } from "../../lib/users/user.service";

import * as LiveStreamLib from "../../lib/livestream";
import { ILivestream } from "../../backend/interfaces/livestream/livestream";
import { ILivestreamData } from "../../backend/interfaces/livestream/livestream-data";
import { IUser } from "../../backend/interfaces";

const userService = new UserService();
export const LivestreamResolver = {
  Query: {
    getLivestreams: async (): Promise<ILivestream[]> => {
      const liveStreams = <ILivestream[]>await LiveStreamLib.getCarouselLiveStreams();

      return liveStreams;
    }
  },

  LivestreamData: {
    user: async (parent: ILivestreamData): Promise<IUser | null> => {
      const userId = parent.user;
      const user = await userService.findOne({ _id: userId });

      return user;
    }
  }
};
