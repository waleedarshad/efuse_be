import { IResolvers } from "graphql-tools";
import { IGraphQLContext, ILeagueBracket, ILeaguePool } from "../../../backend/interfaces";
import { LeagueBracketService, LeaguePoolService } from "../../../lib/leagues";
import { LeagueBracketType } from "@efuse/entities";
import { Failure } from "@efuse/contracts";
import { ObjectId } from "../../../backend/types";
import { OwnerPermissionService } from "../../../lib/shared";

interface CreateLeagueBracketBody {
  name: string;
  type: LeagueBracketType;
}

interface CreateLeagueBracketArgs {
  bracketBody: CreateLeagueBracketBody;
  leaguePoolId: ObjectId;
}

export class LeagueBracketResolver {
  private static bracketService = new LeagueBracketService();
  private static poolService = new LeaguePoolService();
  private static ownerPermissionsService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      LeaguePool: {
        bracket: async (parent: ILeaguePool) => this.resolveBracket(parent)
      },
      Query: {
        getLeagueBracketById: async (_, args) => this.getBracketById(args)
      },
      Mutation: {
        createLeagueBracket: async (_, args, context) => this.createLeagueBracket(args, context),
        deleteLeagueBracket: async (_, args, context) => this.deleteLeagueBracket(args, context),
        updateLeagueBracket: async (_, args, context) => this.updateLeagueBracket(args, context)
      },
      LeagueBracket: {
        pool: async (parent: ILeagueBracket) => this.resolvePool(parent)
      }
    };
  }

  private static async getBracketById(args: { bracketId: ObjectId }): Promise<ILeagueBracket | null> {
    const { bracketId } = args;

    const foundBracket = await this.bracketService.findOne({ _id: bracketId }, {}, { lean: true });

    if (!foundBracket) {
      throw Failure.UnprocessableEntity("League bracket is not found", foundBracket);
    }

    return foundBracket;
  }

  private static async createLeagueBracket(
    { bracketBody, leaguePoolId }: CreateLeagueBracketArgs,
    context: IGraphQLContext
  ): Promise<ILeagueBracket | null> {
    const owner = await this.poolService.findOwner(leaguePoolId);
    await this.ownerPermissionsService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const savedBracket = await this.bracketService.createBracketsForPools(
      [leaguePoolId],
      bracketBody.type,
      bracketBody.name
    );

    if (!savedBracket || savedBracket.length === 0) {
      throw Failure.ExpectationFailed("Failed to create League Bracket", { bracketBody, leaguePoolId });
    }

    return await this.bracketService.findOne({ _id: savedBracket[0] }, {}, { lean: true });
  }

  // TODO: Make async job to delete everything under such as rounds, matches, etc...
  private static async deleteLeagueBracket(args: { bracketId: ObjectId }, context: IGraphQLContext): Promise<boolean> {
    const { bracketId } = args;
    const owner = await this.bracketService.findOwner(bracketId);
    await this.ownerPermissionsService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return await this.bracketService.delete(bracketId);
  }

  private static async resolveBracket(parent: ILeaguePool): Promise<ILeagueBracket | null> {
    return this.bracketService.findOne({ pool: parent._id }, {}, { lean: true });
  }

  private static async resolvePool(parent: ILeagueBracket): Promise<ILeaguePool | null> {
    return this.poolService.findOne({ _id: parent.pool }, {}, { lean: true });
  }

  private static async updateLeagueBracket(
    args: {
      bracketId: ObjectId;
      name: string;
    },
    context: IGraphQLContext
  ): Promise<ILeagueBracket | null> {
    const { bracketId, name } = args;

    const owner = await this.bracketService.findOwner(bracketId);
    await this.ownerPermissionsService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return await this.bracketService.updateOne({ _id: bracketId }, { name }, { new: true, lean: true });
  }
}
