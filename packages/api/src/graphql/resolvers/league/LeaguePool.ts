import { IResolvers } from "graphql-tools";
import { TeamService } from "../../../lib/team";
import { IGraphQLContext, ILeagueEvent, ILeaguePool, ITeam } from "../../../backend/interfaces";
import { PaginateResult, Types } from "mongoose";
import { LeagueEventService, LeaguePoolService } from "../../../lib/leagues";
import { ObjectId } from "../../../backend/types";
import { Failure } from "@efuse/contracts";
import { OwnerPermissionService } from "../../../lib/shared";

interface CreateLeaguePoolArgs {
  eventId: ObjectId;
  name: string;
  teams: ObjectId[];
}

interface UpdateLeaguePoolBody {
  name: string;
  teams: Types.ObjectId[];
}

interface GetPaginatedLeaguePoolArgs {
  eventId: ObjectId;
  page: number;
  limit: number;
}

export class LeaguePoolResolver {
  private static eventService = new LeagueEventService();
  private static poolService = new LeaguePoolService();
  private static teamService = new TeamService();
  private static ownerPermissionService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      LeagueEvent: {
        pools: async (parent: ILeagueEvent) => this.resolvePools(parent)
      },
      Query: {
        getLeaguePoolById: async (_, args) => this.getPoolById(args),
        getPaginatedPools: async (_, args) => this.getPaginatedPools(args)
      },
      Mutation: {
        createLeaguePool: async (_, args, context) => this.createLeaguePool(args, context),
        deleteLeaguePool: async (_, args, context) => this.deleteLeaguePool(args, context),
        generateLeagueTeamPools: async (_, args, context) => this.generateLeagueTeamPools(args, context),
        updateLeaguePool: async (_, args, context) => this.updateLeaguePool(args, context),
        moveTeamsToLeaguePool: async (_, args, context) => this.moveTeamsToLeaguePool(args, context),
        clearAllTeamsInLeaguePools: async (_, args, context) => this.clearAllTeamsInLeaguePools(args, context)
      },
      LeaguePool: {
        event: async (parent: ILeaguePool) => this.resolveEvent(parent),
        teams: async (parent: ILeaguePool) => this.resolveTeams(parent)
      }
    };
  }

  private static async createLeaguePool(
    args: CreateLeaguePoolArgs,
    context: IGraphQLContext
  ): Promise<ILeaguePool | null> {
    const { eventId, name, teams } = args;
    const owner = await this.eventService.findOwner(eventId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const savedPool = await this.poolService.create(<ILeaguePool>{
      name: name || "New Pool",
      teams: teams,
      event: eventId
    });

    return savedPool;
  }

  private static async deleteLeaguePool(args: { poolId: ObjectId }, context: IGraphQLContext): Promise<boolean> {
    const { poolId } = args;

    const owner = await this.poolService.findOwner(poolId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const deletedPool = await this.poolService.delete(poolId);

    return deletedPool;
  }

  private static async getPoolById(args: { poolId: ObjectId }): Promise<ILeaguePool | null> {
    const { poolId } = args;

    const updatedPool = await this.poolService.findOne({ _id: poolId }, {}, { lean: true });

    return updatedPool;
  }

  private static async generateLeagueTeamPools(
    args: { eventId: ObjectId },
    context: IGraphQLContext
  ): Promise<ILeaguePool[]> {
    const { eventId } = args;

    const owner = await this.eventService.findOwner(eventId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const event = await this.eventService.findOne({ _id: eventId }, { teams: true }, { lean: true });

    if (!event) {
      throw Failure.UnprocessableEntity("Event not found", { eventId });
    }

    const pools = await this.poolService.find(
      { event: eventId },
      {},
      {
        sort: { _id: "asc" },
        lean: true
      }
    );

    if (!event.teams || event.teams.length === 0) {
      return pools;
    }

    const updatedPools = await this.poolService.generateLeagueTeamPools(
      event.teams,
      pools.map((pool) => pool._id)
    );

    return updatedPools;
  }

  private static async getPaginatedPools(args: GetPaginatedLeaguePoolArgs): Promise<PaginateResult<ILeaguePool>> {
    const { eventId, page = 1, limit = 1 } = args;

    const paginatedPool = await this.poolService.findPaginated(
      { event: eventId },
      {
        lean: true,
        page,
        limit,
        sort: { name: 1 }
      }
    );

    return paginatedPool;
  }

  private static async resolveEvent(parent: ILeaguePool): Promise<ILeagueEvent | null> {
    return await this.eventService.findOne({ _id: parent.event }, {}, { lean: true });
  }

  private static async resolvePools(parent: ILeagueEvent): Promise<ILeaguePool[]> {
    return await this.poolService.find({ event: parent._id }, {}, { lean: true, sort: { _id: "asc" } });
  }

  private static async resolveTeams(parent: ILeaguePool): Promise<ITeam[]> {
    return await this.teamService.find({ _id: { $in: parent.teams } }, {}, { lean: true });
  }

  private static async updateLeaguePool(
    args: {
      poolId: ObjectId;
      poolBody: UpdateLeaguePoolBody;
    },
    context: IGraphQLContext
  ): Promise<ILeaguePool> {
    const { poolId, poolBody } = args;

    const owner = await this.poolService.findOwner(poolId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return this.poolService.updateOne({ _id: poolId }, poolBody, { new: true, lean: true });
  }

  private static async moveTeamsToLeaguePool(
    args: {
      fromPoolId?: ObjectId;
      toPoolId?: ObjectId;
      teams: ObjectId[];
    },
    context: IGraphQLContext
  ): Promise<ILeaguePool[]> {
    const { fromPoolId, toPoolId, teams } = args;

    if (fromPoolId) {
      const owner = await this.poolService.findOwner(fromPoolId);
      await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);
    }

    if (toPoolId) {
      const owner = await this.poolService.findOwner(toPoolId);
      await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);
    }

    const updatedPools: ILeaguePool[] = [];
    if (fromPoolId) {
      const updatedFromPool = await this.poolService.updateOne(
        { _id: fromPoolId },
        { $pull: { teams: { $in: teams } } },
        { new: true }
      );
      updatedPools.push(updatedFromPool);
    }

    if (toPoolId) {
      const updatedToPool = await this.poolService.updateOne(
        { _id: toPoolId },
        { $push: { teams: { $each: teams } } },
        { new: true, lean: true }
      );
      updatedPools.push(updatedToPool);
    }

    return updatedPools;
  }

  private static async clearAllTeamsInLeaguePools(
    args: { eventId: ObjectId },
    context: IGraphQLContext
  ): Promise<ILeaguePool[]> {
    const { eventId } = args;

    const owner = await this.eventService.findOwner(eventId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return await this.poolService.updateMany({ event: eventId }, { teams: [] }, { new: true, lean: true });
  }
}
