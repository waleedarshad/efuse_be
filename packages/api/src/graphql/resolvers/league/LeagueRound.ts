import { IResolvers } from "graphql-tools";
import { ObjectId } from "../../../backend/types";
import { IGraphQLContext, ILeagueBracket, ILeagueRound } from "../../../backend/interfaces";
import { LeagueBracketService, LeagueRoundGenerationService, LeagueRoundService } from "../../../lib/leagues";
import { PaginateResult } from "mongoose";
import { Failure } from "@efuse/contracts";
import { LeagueRoundSort } from "../../enums";
import { SortDirection } from "mongodb";
import { OwnerPermissionService } from "../../../lib/shared";

interface UpdateLeagueRoundBody {
  name: string;
}

export class LeagueRoundResolver {
  private static bracketService = new LeagueBracketService();
  private static roundGenerationService = new LeagueRoundGenerationService();
  private static roundService = new LeagueRoundService();
  private static ownerPermissionService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      Query: {
        getLeagueRoundById: async (_, args) => this.getRoundById(args),
        getLeagueRoundsByBracket: async (_, args) => this.getRoundsByBracket(args),
        getPaginatedLeagueRounds: async (_, args) => this.getPaginatedRounds(args)
      },
      Mutation: {
        createLeagueRound: async (_, args, context) => this.createLeagueRound(args, context),
        deleteLeagueRound: async (_, args, context) => this.deleteLeagueRound(args, context),
        updateLeagueRound: async (_, args, context) => this.updateLeagueRound(args, context)
      },
      LeagueBracket: {
        rounds: async (parent) => this.resolveRounds(parent)
      },
      LeagueRound: {
        bracket: async (parent) => this.resolveBracket(parent)
      }
    };
  }

  private static async createLeagueRound(
    args: { round: ILeagueRound },
    context: IGraphQLContext
  ): Promise<ILeagueRound> {
    const { round } = args;
    const owner = await this.bracketService.findOwner(round.bracket);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const roundExists = await this.roundService.exists({ bracket: round.bracket, value: round.value });

    if (roundExists) {
      throw Failure.BadRequest(`Round with value "${round.value}" already exists on bracket`, round);
    }

    const savedRound = await this.roundService.create(round);

    return savedRound;
  }

  private static async deleteLeagueRound(args: { roundId: ObjectId }, context: IGraphQLContext): Promise<boolean> {
    const { roundId } = args;

    const owner = await this.roundService.findOwner(roundId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const deleted = await this.roundService.delete(roundId);

    return deleted;
  }

  private static async getPaginatedRounds(args: {
    bracketId: ObjectId;
    page: number;
    limit: number;
    sortOn: LeagueRoundSort;
    sortDirection: SortDirection;
  }): Promise<PaginateResult<ILeagueRound>> {
    const { bracketId, page, limit, sortOn, sortDirection } = args;

    const sorting = {};
    sorting[<LeagueRoundSort>sortOn] = sortDirection;

    const paginateQuery = { lean: true, page, limit };
    paginateQuery["sort"] = sorting;

    const rounds = await this.roundService.findPaginated({ bracket: bracketId }, paginateQuery);

    return rounds;
  }

  private static async getRoundsByBracket(args: { bracketId: ObjectId }): Promise<ILeagueRound[]> {
    const { bracketId } = args;

    const rounds = await this.roundService.find({ bracket: bracketId }, {}, { lean: true });

    return rounds;
  }

  private static async getRoundById(args: { roundId: ObjectId }): Promise<ILeagueRound | null> {
    const { roundId } = args;

    const round = await this.roundService.findOne({ _id: roundId }, {}, { lean: true });

    if (!round) {
      throw Failure.UnprocessableEntity("Round does not exist", { round });
    }

    return round;
  }

  private static async resolveBracket(parent: ILeagueRound): Promise<ILeagueBracket | null> {
    const bracket = this.bracketService.findOne({ _id: parent.bracket }, {}, { lean: true });

    return bracket;
  }

  private static async resolveRounds(parent: ILeagueBracket): Promise<ILeagueRound[]> {
    const rounds = await this.roundService.find({ bracket: parent._id }, {}, { lean: true, sort: { value: 1 } });

    return rounds;
  }

  private static async updateLeagueRound(
    args: {
      roundId: ObjectId;
      roundUpdateBody: UpdateLeagueRoundBody;
    },
    context: IGraphQLContext
  ): Promise<ILeagueRound> {
    const { roundUpdateBody, roundId } = args;

    const owner = await this.roundService.findOwner(roundId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const match = await this.roundService.updateOne({ _id: roundId }, roundUpdateBody, { new: true, lean: true });

    return match;
  }
}
