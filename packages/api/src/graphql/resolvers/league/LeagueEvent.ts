import { IResolvers } from "graphql-tools";
import { TeamService } from "../../../lib/team";
import {
  IGraphQLContext,
  ILeague,
  ILeagueBracket,
  ILeagueEvent,
  ILeaguePool,
  ITeam
} from "../../../backend/interfaces";
import { FilterQuery, PaginateResult, Types } from "mongoose";
import {
  LeagueBracketService,
  LeagueEventService,
  LeagueMatchMakingService,
  LeagueMatchService,
  LeaguePoolService,
  LeagueRoundGenerationService,
  LeagueRoundService,
  LeagueService
} from "../../../lib/leagues";
import { LeagueBracketType, LeagueEventCalendarStatus, LeagueEventState, LeagueEventTimingMode } from "@efuse/entities";
import { ObjectId } from "../../../backend/types";
import { Failure } from "@efuse/contracts";
import { OwnerPermissionService } from "../../../lib/shared";

interface LeagueEventCreateBody {
  bracketType: LeagueBracketType;
  leagueId: ObjectId;
  maxTeams: number;
  name: string;
  numberOfRounds: number;
  description: string;
  gamesPerMatch: number;
  numberOfPools: number;
  startDate: Date;
  state: LeagueEventState;
  timingMode: LeagueEventTimingMode;
  details: string;
  rules: string;
}

interface CreateEventArgs {
  leagueEvent: LeagueEventCreateBody;
}

interface UpdateEventArgs {
  leagueEvent: ILeagueEvent;
  eventId: ObjectId;
}

export class LeagueEventResolver {
  private static leagueBracketService: LeagueBracketService = new LeagueBracketService();
  private static leagueEventService: LeagueEventService = new LeagueEventService();
  private static leagueMatchService: LeagueMatchService = new LeagueMatchService();
  private static leaguePoolService: LeaguePoolService = new LeaguePoolService();
  private static leagueRoundService: LeagueRoundService = new LeagueRoundService();
  private static leagueService = new LeagueService();
  private static matchMakingService = new LeagueMatchMakingService();
  private static roundGenerationService = new LeagueRoundGenerationService();
  private static teamService: TeamService = new TeamService();
  private static ownerPermissionService: OwnerPermissionService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      Query: {
        getLeagueEventById: async (_, args) => this.getLeagueEventById(args),
        getLeagueEvents: async (_, args) => this.getLeagueEvents(args),
        getPaginatedLeagueEvents: async (_, args) => this.getPaginatedLeagueEvents(args)
      },
      Mutation: {
        createLeagueEvent: async (_, args, context) => this.createLeagueEvent(args, context),
        deleteLeagueEvent: async (_, args, context) => this.deleteLeagueEvent(args, context),
        generateRoundsAndMatches: async (_, args, context) => this.generateRoundsAndMatches(args, context),
        updateLeagueEvent: async (_, args, context) => this.updateLeagueEvent(args, context)
      },
      League: {
        events: async (parent: ILeague) => this.resolveEvents(parent)
      },
      LeagueEvent: {
        league: async (parent: ILeagueEvent) => this.resolveLeague(parent),
        teams: async (parent: ILeagueEvent) => this.resolveTeams(parent)
      }
    };
  }

  // TODO: Refactor args for easier maintenance in future. FE will also need updated
  private static async createLeagueEvent(args: CreateEventArgs, context: IGraphQLContext): Promise<ILeagueEvent> {
    const { leagueEvent } = args;
    const league = await this.leagueService.findOne({ _id: leagueEvent?.leagueId });

    //check to make sure league exists
    if (!league) {
      throw Failure.UnprocessableEntity("League does not exist");
    }

    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, league.owner, league.ownerType);

    //Set gamesPerMatch to 1 by default if 0 is provided
    if (leagueEvent.gamesPerMatch <= 0) {
      leagueEvent.gamesPerMatch = 1;
    }

    const eventToSave = <ILeagueEvent>{
      bracketType: leagueEvent.bracketType,
      name: leagueEvent.name,
      league: leagueEvent.leagueId,
      description: leagueEvent.description,
      gamesPerMatch: leagueEvent.gamesPerMatch,
      startDate: leagueEvent.startDate,
      state: leagueEvent.state,
      timingMode: leagueEvent.timingMode,
      maxTeams: leagueEvent.maxTeams,
      details: leagueEvent.details,
      rules: leagueEvent.rules
    };

    const savedEvent = await this.leagueEventService.create(eventToSave);

    const poolIds = await this.leaguePoolService.createNumberOfPoolsForEvent(leagueEvent.numberOfPools, savedEvent._id);
    await this.leagueBracketService.createBracketsForPools(poolIds, leagueEvent.bracketType);
    // TODO: when implementing other bracket types besides round robin we'll probably have to setup some round generation here.

    return savedEvent;
  }

  private static async deleteLeagueEvent(args: { eventId: ObjectId }, context: IGraphQLContext): Promise<boolean> {
    const { eventId } = args;
    const leagueEvent = await this.leagueEventService.findOne({ _id: eventId }, {}, { lean: true });

    if (!leagueEvent) {
      throw Failure.NotFound("League event not found.", args);
    }

    const owner = await this.leagueEventService.findOwner(eventId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);
    return await this.leagueEventService.delete(eventId);
  }

  private static async generateRoundsAndMatches(
    args: { eventId: ObjectId },
    context: IGraphQLContext
  ): Promise<ILeagueEvent | null> {
    const { eventId } = args;

    const owner = await this.leagueEventService.findOwner(eventId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    // needed for matchmaking
    const event = await this.leagueEventService.findOne(
      { _id: eventId },
      { startDate: true, timingMode: true },
      { lean: true }
    );

    if (!event) {
      throw Failure.UnprocessableEntity("Event does not exist", { event });
    }

    const pools = await this.leaguePoolService.find({ event: eventId }, { _id: true, teams: true }, { lean: true });

    const poolIds = pools.map((pool) => pool._id);
    const brackets = await this.leagueBracketService.find(
      { pool: { $in: poolIds } },
      { _id: true, type: true, pool: true },
      { lean: true }
    );

    await Promise.all(brackets.map((bracket) => this.generateRoundsAndMatchesForBracket(pools, bracket, event)));

    const updatedEvent = await this.leagueEventService.findOne({ _id: eventId }, {}, { lean: true });

    return updatedEvent;
  }

  private static async getLeagueEventById(args: { eventId: ObjectId }): Promise<ILeagueEvent | null> {
    const { eventId } = args;

    const event = await this.leagueEventService.findOne({ _id: eventId }, {}, { lean: true });

    return event;
  }

  private static async getLeagueEvents(args: {
    leagueId: ObjectId;
    bracketType?: LeagueBracketType;
    eventCalendarStatus?: LeagueEventCalendarStatus;
    eventState?: LeagueEventState;
  }): Promise<ILeagueEvent[]> {
    const { leagueId, bracketType, eventCalendarStatus, eventState } = args;

    const query: {}[] = this.constructQueryToGetLeagueEvents(leagueId, bracketType, eventCalendarStatus, eventState);

    const event = await this.leagueEventService.find({ $and: query }, {}, { lean: true });

    return event;
  }

  private static async getPaginatedLeagueEvents(args: {
    league: ObjectId;
    page?: number;
    limit?: number;
    bracketType?: LeagueBracketType;
    eventCalendarStatus?: LeagueEventCalendarStatus;
    eventState?: LeagueEventState;
  }): Promise<PaginateResult<ILeagueEvent>> {
    const { page, limit, league, bracketType, eventCalendarStatus, eventState } = args;

    const query: FilterQuery<ILeagueEvent>[] = this.constructQueryToGetLeagueEvents(
      league,
      bracketType,
      eventCalendarStatus,
      eventState
    );

    const paginatedEvent = await this.leagueEventService.findPaginated(
      { $and: query },
      {
        lean: true,
        page,
        limit,
        sort: { name: 1 }
      }
    );

    return paginatedEvent;
  }

  private static async resolveEvents(parent: ILeague): Promise<ILeagueEvent[]> {
    const events = await this.leagueEventService.find({ league: parent._id }, {}, { lean: true });

    return events;
  }

  private static async resolveLeague(parent: ILeagueEvent): Promise<ILeague | null> {
    const league = await this.leagueService.findOne({ _id: parent.league }, {}, { lean: true });

    return league;
  }

  private static async resolveTeams(parent: ILeagueEvent): Promise<ITeam[]> {
    const teams = await this.teamService.find({ _id: { $in: parent.teams } }, {}, { lean: true });

    return teams;
  }

  private static async updateLeagueEvent(args: UpdateEventArgs, context: IGraphQLContext): Promise<ILeagueEvent> {
    const { leagueEvent, eventId } = args;

    const owner = await this.leagueEventService.findOwner(eventId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    const numberOfTeams = leagueEvent?.teams?.length;

    const eventExists = await this.leagueEventService.exists({ _id: eventId });

    if (!eventExists) {
      throw Failure.UnprocessableEntity("Event does not exist", args);
    }

    if (numberOfTeams && numberOfTeams > 0) {
      const foundEvent = await this.leagueEventService.findOne({ _id: eventId }, { maxTeams: true }, { lean: true });
      const maxTeams = foundEvent?.maxTeams;

      if (maxTeams && maxTeams < numberOfTeams) {
        throw Failure.BadRequest("Number of teams must be less than or equal to max teams set for event", {
          maxTeams,
          numberOfTeams
        });
      }
    }

    const updatedEvent = await this.leagueEventService.updateOne({ _id: eventId }, leagueEvent, {
      new: true,
      lean: true
    });

    const pools = await this.leaguePoolService.find({ event: eventId }, { _id: true }, { lean: true });

    const teamsWereUpdated = leagueEvent.teams != null;
    await this.handleUpdatingPools(pools, updatedEvent.teams, teamsWereUpdated);

    if (leagueEvent.state != null) {
      await this.handleRoundsAndMatchesOnStateChange(eventId, leagueEvent.state, context);
    }

    return updatedEvent;
  }

  /**
   * Generate rounds and matches when pools are confirmed
   *
   * Delete rounds and matches if the event goes back to pending teams or pools
   */
  private static async handleRoundsAndMatchesOnStateChange(
    eventId: ObjectId,
    eventState: LeagueEventState,
    context: IGraphQLContext
  ): Promise<void> {
    if (eventState === LeagueEventState.PENDING) {
      await this.generateRoundsAndMatches({ eventId }, context);
    }

    if (eventState === LeagueEventState.PENDING_POOLS || eventState === LeagueEventState.PENDING_TEAMS) {
      await this.deleteRoundsAndMatches(eventId, context);
    }
  }

  private static async handleUpdatingPools(
    pools: ILeaguePool[],
    currentEventTeams: Types.ObjectId[],
    teamsWereUpdated: boolean
  ): Promise<void> {
    // if we have one pool and teams were updated then we will auto assign teams to pool
    if (teamsWereUpdated && pools?.length === 1) {
      await this.leaguePoolService.updateOne(
        { _id: pools[0]._id },
        { teams: currentEventTeams },
        { new: true, lean: true }
      );
    }

    // teams were updated for the event, so we will reset teams in pools
    if (teamsWereUpdated && pools?.length > 1) {
      await Promise.all(
        pools.map((pool) =>
          this.leaguePoolService.updateOne({ _id: pool._id }, { teams: [] }, { lean: true, new: true })
        )
      );
    }
  }

  private static async deleteRoundsAndMatches(eventId: ObjectId, context: IGraphQLContext): Promise<void> {
    const foundPools = await this.leaguePoolService.find({ event: eventId }, { _id: true }, { lean: true });

    if (!foundPools || foundPools.length === 0) {
      return;
    }

    const poolIds = foundPools.map((pool) => pool._id);
    const foundBrackets = await this.leagueBracketService.find(
      { pool: { $in: poolIds } },
      { _id: true },
      { lean: true }
    );

    if (!foundBrackets || foundBrackets.length === 0) {
      return;
    }

    const bracketIds = foundBrackets.map((bracket) => bracket._id);
    const foundRounds = await this.leagueRoundService.find(
      { bracket: { $in: bracketIds } },
      { _id: true },
      { lean: true }
    );

    if (!foundRounds || foundRounds.length === 0) {
      return;
    }

    const roundIds = foundRounds.map((round) => round._id);
    await this.leagueRoundService.deleteMany({ _id: { $in: roundIds } });

    const matchesExist = this.leagueMatchService.exists({ round: { $in: roundIds } });
    if (!matchesExist) {
      return;
    }

    await this.leagueMatchService.deleteMany({ round: { $in: roundIds } });
  }

  private static constructQueryToGetLeagueEvents(
    leagueId: ObjectId,
    bracketType?: LeagueBracketType,
    eventCalendarStatus?: LeagueEventCalendarStatus,
    eventState?: LeagueEventState
  ): FilterQuery<ILeagueEvent>[] {
    if (eventCalendarStatus && eventState) {
      throw Failure.BadRequest("Event Calendar Status and Event State cannot both be used in the same request", {
        leagueId,
        bracketType,
        eventCalendarStatus,
        eventState
      });
    }

    const query: FilterQuery<ILeagueEvent>[] = [{ league: leagueId }];

    if (bracketType) {
      query.push({ bracketType: bracketType });
    }

    if (eventCalendarStatus) {
      const currentDate = new Date();

      switch (eventCalendarStatus) {
        case LeagueEventCalendarStatus.CURRENT:
          query.push({ state: LeagueEventState.ACTIVE, startDate: { $lte: currentDate } });
          break;
        case LeagueEventCalendarStatus.PAST:
          query.push({ state: LeagueEventState.FINISHED });
          break;
        case LeagueEventCalendarStatus.UPCOMING:
          query.push({ state: LeagueEventState.ACTIVE, startDate: { $gt: currentDate } });
          break;
        default:
          break;
      }
    }

    if (eventState) {
      query.push({ state: eventState });
    }

    return query;
  }

  private static async generateRoundsAndMatchesForBracket(
    pools: ILeaguePool[],
    bracket: ILeagueBracket,
    event: ILeagueEvent
  ): Promise<void> {
    const foundPool = pools.find((pool) => bracket.pool.equals(pool._id));

    if (!foundPool) {
      return;
    }

    const rounds = await this.roundGenerationService.generateRoundsForEvent(
      bracket,
      foundPool?.teams.length || 0,
      event
    );

    await this.matchMakingService.determineBracketTypeAndCreateMatchesForRounds(
      rounds,
      foundPool.teams,
      bracket.type,
      event
    );
  }
}
