import { IResolvers } from "graphql-tools";
import { ILeagueCompetitorScore, ILeagueGameScore } from "../../../backend/interfaces";
import { Failure } from "@efuse/contracts";
import { ITeam, ITeamMember } from "../../../backend/interfaces/team";
import { LeagueScoreCompetitor } from "../../enums";
import { LeagueGameScoreType, LeagueScoreCompetitorType, LeagueScoreCreatorType } from "@efuse/entities";
import { TeamMemberService, TeamService } from "../../../lib/team";
import { LeagueCompetitorScoreService, LeagueGameScoreService } from "../../../lib/leagues";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";
import { LeagueScoreCreator, ObjectId } from "../../../backend/types";
import { OwnerPermissionService } from "../../../lib/shared";

type LeagueScoreCompetitorInterfaces = ITeam | ITeamMember | null;

interface UpdateScoreBody {
  value: number;
  image: string;
}

export class LeagueCompetitorScoreResolver {
  private static gameScoreService = new LeagueGameScoreService();
  private static organizationService = new OrganizationsService();
  private static teamService = new TeamService();
  private static teamMemberService = new TeamMemberService();
  private static scoreService = new LeagueCompetitorScoreService();
  private static userService = new UserService();
  private static ownerPermissionService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      Query: {
        getLeagueCompetitorScoreById: async (_, args) => this.getLeagueCompetitorScoreById(args),
        getLeagueCompetitorScores: async (_, args) => this.getLeagueCompetitorScores(args)
      },
      Mutation: {
        createLeagueCompetitorScore: async (_, args, context) => this.createLeagueCompetitorScore(args, context),
        deleteLeagueCompetitorScore: async (_, args, context) => this.deleteLeagueCompetitorScore(args, context),
        updateLeagueCompetitorScore: async (_, args, context) => this.updateLeagueCompetitorScore(args, context)
      },
      LeagueGameScore: {
        scores: async (parent) => this.resolveScores(parent)
      },
      LeagueScoreCompetitor: {
        __resolveType: this.resolveLeagueScoreCompetitorType
      },
      LeagueCompetitorScore: {
        competitor: async (parent) => this.resolveCompetitor(parent),
        gameScore: async (parent) => this.resolveGameScore(parent),
        creator: async (parent) => this.resolveCreator(parent)
      }
    };
  }

  private static async createLeagueCompetitorScore(
    args: {
      scoreBody: ILeagueCompetitorScore;
    },
    context
  ): Promise<ILeagueCompetitorScore> {
    const { scoreBody } = args;
    const { creator, creatorType, competitor, competitorType, gameScore } = scoreBody;
    const gameScoreType = (await this.gameScoreService.findOne({ _id: gameScore }))?.type;
    if (gameScoreType === LeagueGameScoreType.SUBMITTED) {
      const creatorOwner = await this.gameScoreService.findOwnerOfCreator(creator, creatorType);
      await this.ownerPermissionService.userAllowedToPerformActions(
        context.userId,
        creatorOwner.owner,
        creatorOwner.ownerType
      );
    } else {
      const owner = await this.gameScoreService.findOwner(gameScore);
      await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner?.owner, owner?.ownerType);
    }

    const scoreExists = await this.scoreService.exists({ competitor, gameScore });

    if (scoreExists) {
      throw Failure.MethodNotAllowed(
        `Competitor score already exists for game score. An update is recommended instead.`,
        scoreBody
      );
    }

    const creatorExists: boolean = await this.checkIfCreatorExists(creator, creatorType, scoreBody);

    if (!creatorExists) {
      throw Failure.BadRequest("Check that creator type matches with the creator", scoreBody);
    }

    const competitorExists: boolean = await this.checkIfCompetitorExists(competitor, competitorType, scoreBody);

    if (!competitorExists) {
      throw Failure.BadRequest("Check that competitor type matches with the competitor", scoreBody);
    }

    const gameScoreExists = await this.gameScoreService.exists({ _id: gameScore });

    if (!gameScoreExists) {
      throw Failure.UnprocessableEntity("Game score does not exist", scoreBody);
    }

    return await this.scoreService.create(scoreBody);
  }

  private static async deleteLeagueCompetitorScore(args: { scoreId: ObjectId }, context): Promise<boolean> {
    const { scoreId } = args;
    const score = await this.scoreService.findOne({ _id: scoreId });
    const leagueOwner = await this.scoreService.findOwner(scoreId);
    const creatorOwner = await this.gameScoreService.findOwnerOfCreator(score?.creator, score?.creatorType);
    await this.ownerPermissionService.userAllowedToPerformActionsForAny(context.userId, [leagueOwner, creatorOwner]);

    return await this.scoreService.delete(scoreId);
  }

  private static async getLeagueCompetitorScoreById(args: {
    scoreId: ObjectId;
  }): Promise<ILeagueCompetitorScore | null> {
    const { scoreId } = args;

    const score = await this.scoreService.findOne({ _id: scoreId }, {}, { lean: true });

    if (!score) {
      throw Failure.UnprocessableEntity("Score does not exist", score);
    }

    return score;
  }

  private static async getLeagueCompetitorScores(args: { gameScoreId: ObjectId }): Promise<ILeagueCompetitorScore[]> {
    const { gameScoreId } = args;

    return await this.scoreService.find({ gameScore: gameScoreId }, {}, { lean: true });
  }

  private static async resolveCompetitor(parent: ILeagueCompetitorScore): Promise<LeagueScoreCompetitorInterfaces> {
    switch (parent?.competitorType) {
      case LeagueScoreCompetitorType.TEAM:
        return this.teamService.findOne({ _id: parent.competitor }, {}, { lean: true });
      case LeagueScoreCompetitorType.TEAM_MEMBER:
        return this.teamMemberService.findOne({ _id: parent.competitor }, {}, { lean: true });
      default:
        throw Failure.ExpectationFailed("Could not determine competitor type", parent);
    }
  }

  private static async resolveCreator(parent: ILeagueCompetitorScore): Promise<LeagueScoreCreator | null> {
    const query = { _id: parent.creator };
    const leanQuery = { lean: true };

    switch (parent?.creatorType) {
      case LeagueScoreCreatorType.ORGANIZATIONS:
        return this.organizationService.findOne(query, {}, leanQuery);
      case LeagueScoreCreatorType.USERS:
        return this.userService.findOne(query, {}, leanQuery);
      case LeagueScoreCreatorType.TEAMS:
        return this.teamService.findOne(query, {}, leanQuery);
      default:
        throw Failure.ExpectationFailed("Could not determine creator type", parent);
    }
  }

  private static async resolveGameScore(parent: ILeagueCompetitorScore): Promise<ILeagueGameScore | null> {
    const gameScore = await this.gameScoreService.findOne({ _id: parent.gameScore }, {}, { lean: true });

    return gameScore;
  }

  private static resolveLeagueScoreCompetitorType(parent: LeagueScoreCompetitorInterfaces): LeagueScoreCompetitor {
    // should only exist on teams
    if ((<ITeam>parent)?.game && (<ITeam>parent)?.name && (<ITeam>parent)?.owner) {
      return LeagueScoreCompetitor.TEAM;
    }

    // should only exist on team members
    if ((<ITeamMember>parent)?.team && (<ITeamMember>parent)?.role) {
      return LeagueScoreCompetitor.TEAM_MEMBER;
    }

    throw Failure.ExpectationFailed("Could not determine competitor type", parent);
  }

  private static resolveScores(parent: ILeagueGameScore): Promise<ILeagueCompetitorScore[]> {
    const score = this.scoreService.find({ gameScore: parent._id }, {}, { lean: true });

    return score;
  }

  private static async updateLeagueCompetitorScore(
    args: {
      scoreId: ObjectId;
      scoreBody: UpdateScoreBody;
    },
    context
  ): Promise<ILeagueCompetitorScore> {
    const { scoreId, scoreBody } = args;
    const score = await this.scoreService.findOne({ _id: scoreId });
    const leagueOwner = await this.scoreService.findOwner(scoreId);
    const creatorOwner = await this.gameScoreService.findOwnerOfCreator(score?.creator, score?.creatorType);
    await this.ownerPermissionService.userAllowedToPerformActionsForAny(context.userId, [leagueOwner, creatorOwner]);

    return await this.scoreService.updateOne({ _id: scoreId }, scoreBody, {
      lean: true,
      new: true
    });
  }

  private static async checkIfCreatorExists(
    creator: ObjectId,
    creatorType: LeagueScoreCreatorType,
    scoreBody: ILeagueCompetitorScore
  ): Promise<boolean> {
    let creatorExists = false;

    switch (creatorType) {
      case LeagueScoreCreatorType.USERS:
        creatorExists = await this.userService.exists({ _id: creator });
        break;
      case LeagueScoreCreatorType.ORGANIZATIONS:
        creatorExists = await this.organizationService.exists({ _id: creator });
        break;
      case LeagueScoreCreatorType.TEAMS:
        creatorExists = await this.teamService.exists({ _id: creator });
        break;
      default:
        throw Failure.BadRequest("Could not determine creator type", scoreBody);
    }

    return creatorExists;
  }

  private static async checkIfCompetitorExists(
    competitor: ObjectId,
    competitorType: LeagueScoreCompetitorType,
    scoreBody: ILeagueCompetitorScore
  ): Promise<boolean> {
    let competitorExists = false;

    switch (competitorType) {
      case LeagueScoreCompetitorType.TEAM:
        competitorExists = await this.teamService.exists({ _id: competitor });
        break;
      case LeagueScoreCompetitorType.TEAM_MEMBER:
        competitorExists = await this.teamMemberService.exists({ _id: competitor });
        break;
      default:
        throw Failure.BadRequest("Could not determine competitor type", scoreBody);
    }

    return competitorExists;
  }
}
