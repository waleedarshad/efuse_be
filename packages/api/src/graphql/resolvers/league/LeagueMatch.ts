import { IResolvers } from "graphql-tools";
import { TeamService } from "../../../lib/team";
import { IGraphQLContext, ILeagueMatch, ILeagueRound, ITeam } from "../../../backend/interfaces";
import { PaginateResult, UpdateQuery } from "mongoose";
import { LeagueMatchService, LeagueRoundService } from "../../../lib/leagues";
import { LeagueMatchState } from "@efuse/entities";
import { ObjectId } from "../../../backend/types";
import { OwnerPermissionService } from "../../../lib/shared";

interface PaginatedMatches {
  roundId: ObjectId;
  page: number;
  limit: number;
}

interface LeagueMatchBody {
  round: ObjectId;
  teams: ObjectId[];
  winner: ObjectId;
  state: LeagueMatchState;
  startTime: Date;
  endTime: Date;
}

interface CreateLeagueMatchArgs {
  leagueMatchBody: LeagueMatchBody;
}

interface UpdateLeagueMatchArgs {
  matchId: ObjectId;
  leagueMatchBody: UpdateQuery<ILeagueMatch>;
}

export class LeagueMatchResolver {
  private static matchService = new LeagueMatchService();
  private static roundService = new LeagueRoundService();
  private static teamService = new TeamService();
  private static ownerPermissionService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      LeagueRound: {
        matches: async (parent: ILeagueRound) => this.resolveMatches(parent)
      },
      Query: {
        getLeagueMatchById: async (_, args) => this.getMatchById(args),
        getPaginatedLeagueMatches: async (_, args) => this.getPaginatedMatches(args)
      },
      Mutation: {
        createLeagueMatch: async (_, args, context) => this.createLeagueMatch(args, context),
        deleteLeagueMatch: async (_, args, context) => this.deleteLeagueMatch(args, context),
        updateLeagueMatch: async (_, args, context) => this.updateLeagueMatch(args, context)
      },
      LeagueMatch: {
        teams: async (parent: ILeagueMatch) => this.resolveTeams(parent),
        round: async (parent: ILeagueMatch) => this.resolveRound(parent),
        winner: async (parent: ILeagueMatch) => this.resolveWinner(parent)
      }
    };
  }

  private static async createLeagueMatch(args: CreateLeagueMatchArgs, context: IGraphQLContext): Promise<ILeagueMatch> {
    const { leagueMatchBody } = args;
    const owner = await this.roundService.findOwner(leagueMatchBody.round);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return await this.matchService.create(<ILeagueMatch>leagueMatchBody);
  }

  private static async deleteLeagueMatch(args: { matchId: ObjectId }, context: IGraphQLContext): Promise<boolean> {
    const { matchId } = args;

    const owner = await this.matchService.findOwner(matchId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return await this.matchService.delete(matchId);
  }

  private static async getMatchById(args: { matchId: ObjectId }): Promise<ILeagueMatch | null> {
    const { matchId } = args;

    return await this.matchService.findOne({ _id: matchId }, {}, { lean: true });
  }

  private static async getPaginatedMatches(args: PaginatedMatches): Promise<PaginateResult<ILeagueMatch>> {
    const { roundId, page, limit } = args;

    const paginatedMatches = await this.matchService.findPaginated(
      { round: roundId },
      {
        lean: true,
        page,
        limit,
        sort: { name: 1 }
      }
    );

    return paginatedMatches;
  }

  private static async resolveMatches(parent: ILeagueRound): Promise<ILeagueMatch[]> {
    return await this.matchService.find({ round: parent._id }, {}, { lean: true });
  }

  private static async resolveRound(parent: ILeagueMatch): Promise<ILeagueRound | null> {
    return await this.roundService.findOne({ _id: parent.round }, {}, { lean: true });
  }

  private static async resolveTeams(parent: ILeagueMatch): Promise<ITeam[]> {
    if (!parent.teams || parent.teams.length === 0) {
      return [];
    }

    return await this.teamService.find({ _id: { $in: parent.teams } }, {}, { lean: true });
  }

  private static async resolveWinner(parent: ILeagueMatch): Promise<ITeam | null> {
    return await this.teamService.findOne({ _id: parent.winner }, {}, { lean: true });
  }

  private static async updateLeagueMatch(args: UpdateLeagueMatchArgs, context: IGraphQLContext): Promise<ILeagueMatch> {
    const { matchId, leagueMatchBody } = args;

    const owner = await this.matchService.findOwner(matchId);
    await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);

    return await this.matchService.updateOne({ _id: matchId }, leagueMatchBody, { new: true, lean: true });
  }
}
