import { IResolvers } from "graphql-tools";
import { LeaguePoolService, LeagueScoreboardService } from "../../../lib/leagues";
import { ILeaguePool, ILeagueScoreboard, ILeagueScoreboardWithPool, ITeam } from "../../../backend/interfaces";
import { TeamService } from "../../../lib/team";
import { ObjectId } from "../../../backend/types";
import { Failure } from "@efuse/contracts";

export class LeagueScoreboardResolver {
  private static teamService = new TeamService();
  private static poolService = new LeaguePoolService();
  private static leagueScoreboardService = new LeagueScoreboardService();

  public static get resolve(): IResolvers {
    return {
      Query: {
        getScoreboardsForEvent: async (_, args) => this.getScoreboardsForEvent(args),
        getScoreboardForPool: async (_, args) => this.getScoreboardForPool(args)
      },
      LeagueScoreboard: {
        team: async (parent) => this.resolveTeam(parent)
      },
      LeagueScoreboardWithPool: {
        pool: async (parent) => this.resolvePool(parent)
      }
    };
  }

  private static async resolveTeam(parent: ILeagueScoreboard): Promise<ITeam | null> {
    return this.teamService.findOne({ _id: parent.team }, {}, { lean: true });
  }

  private static async resolvePool(parent: ILeagueScoreboardWithPool): Promise<ILeaguePool | null> {
    return this.poolService.findOne({ _id: parent.pool }, {}, { lean: true });
  }

  private static async getScoreboardsForEvent(args: { eventId: ObjectId }): Promise<ILeagueScoreboardWithPool[]> {
    const { eventId } = args;

    const generatedScoreboard = await this.leagueScoreboardService.getScoreboardsWithPoolForEvent(eventId);

    return generatedScoreboard;
  }

  private static async getScoreboardForPool(args: { poolId: ObjectId }): Promise<ILeagueScoreboard[]> {
    const { poolId } = args;

    const pool = await this.poolService.findOne({ _id: poolId }, { teams: true }, { lean: true });

    if (!pool) {
      throw Failure.UnprocessableEntity("Pool does not exist", { poolId });
    }

    const scoreboard = await this.leagueScoreboardService.getScoreboardForPool(pool._id, pool.teams);

    return scoreboard;
  }
}
