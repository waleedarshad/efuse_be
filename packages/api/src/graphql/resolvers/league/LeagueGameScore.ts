import { Logger } from "@efuse/logger";
import { IResolvers } from "graphql-tools";
import { SortDirection } from "mongodb";
import { FilterQuery, PaginateResult, Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { LeagueGameScoreType, LeagueMatchState, LeagueScoreCreatorType, OwnerType } from "@efuse/entities";

import {
  IGraphQLContext,
  ILeague,
  ILeagueCompetitorScore,
  ILeagueEvent,
  ILeagueGameScore,
  ILeagueMatch
} from "../../../backend/interfaces";
import {
  LeagueBracketService,
  LeagueCompetitorScoreService,
  LeagueEventService,
  LeagueGameScoreService,
  LeagueMatchService,
  LeaguePoolService,
  LeagueRoundService,
  LeagueService
} from "../../../lib/leagues";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";
import { LeagueScoreCreator, ObjectId } from "../../../backend/types";
import { TeamService } from "../../../lib/team";
import { OwnerPermissionService } from "../../../lib/shared";

const logger = Logger.create();

interface UpdateCompetitorScoreBody {
  _id: ObjectId;
  image: string;
  value: number;
}

interface UpdateGameScoreBody {
  image: string;
  type: LeagueGameScoreType;
}

export class LeagueGameScoreResolver {
  private static bracketService = new LeagueBracketService();
  private static eventService = new LeagueEventService();
  private static leagueService = new LeagueService();
  private static gameScoreService = new LeagueGameScoreService();
  private static matchService = new LeagueMatchService();
  private static organizationService = new OrganizationsService();
  private static poolService = new LeaguePoolService();
  private static roundService = new LeagueRoundService();
  private static scoreService = new LeagueCompetitorScoreService();
  private static teamService = new TeamService();
  private static userService = new UserService();
  private static ownerPermissionService = new OwnerPermissionService();

  public static get resolve(): IResolvers {
    return {
      Query: {
        getLeagueGameScoreById: async (_, args) => this.getLeagueGameScoreById(args),
        getLeagueGameScores: async (_, args) => this.getLeagueGameScores(args),
        getPaginatedLeagueGameScores: async (_, args) => this.getPaginatedLeagueGameScores(args)
      },
      Mutation: {
        submitLeagueGameScore: async (_, args, context) => this.submitLeagueGameScore(args, context),
        deleteLeagueGameScore: async (_, args, context) => this.deleteLeagueGameScore(args, context),
        updateLeagueGameScore: async (_, args, context) => this.updateLeagueGameScore(args, context)
      },
      LeagueMatch: {
        submittedScores: async (parent: ILeagueMatch) => this.resolveScores(parent, [LeagueGameScoreType.SUBMITTED]),
        finalScores: async (parent: ILeagueMatch) =>
          this.resolveScores(parent, [LeagueGameScoreType.FINALIZED, LeagueGameScoreType.CONFLICT])
      },
      LeagueGameScore: {
        match: async (parent) => this.resolveMatch(parent),
        creator: async (parent) => this.resolveCreator(parent)
      }
    };
  }

  private static async deleteLeagueGameScore(
    args: { gameScoreId: ObjectId },
    context: IGraphQLContext
  ): Promise<boolean> {
    const { gameScoreId } = args;
    const gameScore = await this.gameScoreService.findOne({ _id: gameScoreId });

    const leagueOwner = await this.gameScoreService.findOwner(gameScoreId);
    const creatorOwner = await this.gameScoreService.findOwnerOfCreator(gameScore?.creator, gameScore?.creatorType);
    await this.ownerPermissionService.userAllowedToPerformActionsForAny(context.userId, [leagueOwner, creatorOwner]);

    return await this.gameScoreService.delete(gameScoreId);
  }

  private static async getLeagueGameScoreById(args: { gameScoreId: ObjectId }): Promise<ILeagueGameScore | null> {
    const { gameScoreId } = args;
    const gameScore = await this.gameScoreService.findOne({ _id: gameScoreId }, {}, { lean: true });

    if (!gameScore) {
      throw Failure.UnprocessableEntity("League game score is not found", gameScore);
    }
    return gameScore;
  }

  private static async getLeagueGameScores(args: {
    matchId: ObjectId;
    type?: LeagueGameScoreType;
  }): Promise<ILeagueGameScore[]> {
    const { matchId, type } = args;

    const filterQuery: FilterQuery<ILeagueGameScore> = !type ? { match: matchId } : { match: matchId, type };

    const foundGameScores = await this.gameScoreService.find(filterQuery, {}, { lean: true });

    return foundGameScores;
  }

  private static async getPaginatedLeagueGameScores(args: {
    matchId: ObjectId;
    type: LeagueGameScoreType;
    page: number;
    limit: number;
    sortDirection: SortDirection;
  }): Promise<PaginateResult<ILeagueGameScore>> {
    const { matchId, type, page, limit, sortDirection } = args;

    const paginatedGameScores = await this.gameScoreService.findPaginated(
      { match: matchId, type },
      { page, limit, sort: { gameNumber: sortDirection } }
    );

    return paginatedGameScores;
  }

  private static async resolveCreator(parent: ILeagueGameScore): Promise<LeagueScoreCreator | null> {
    switch (parent?.creatorType) {
      case LeagueScoreCreatorType.ORGANIZATIONS:
        return this.organizationService.findOne({ _id: parent.creator }, {}, { lean: true });
      case LeagueScoreCreatorType.USERS:
        return this.userService.findOne({ _id: parent.creator }, {}, { lean: true });
      case LeagueScoreCreatorType.TEAMS:
        return this.teamService.findOne({ _id: parent.creator }, {}, { lean: true });
      default:
        throw Failure.ExpectationFailed("Could not determine creator type", parent);
    }
  }

  private static resolveMatch(parent: ILeagueGameScore): Promise<ILeagueMatch | null> {
    const match = this.matchService.findOne({ _id: parent.match }, {}, { lean: true });

    return match;
  }

  private static resolveScores(parent: ILeagueMatch, types: LeagueGameScoreType[]): Promise<ILeagueGameScore[]> {
    const score = this.gameScoreService.find({ match: parent._id, type: { $in: types } }, {}, { lean: true });

    return score;
  }

  private static async submitLeagueGameScore(
    args: {
      gameScoreBody: ILeagueGameScore;
      competitorScores: ILeagueCompetitorScore[];
    },
    context: IGraphQLContext
  ): Promise<ILeagueGameScore> {
    const { gameScoreBody, competitorScores } = args;

    const { gameNumber, match, type, creator, creatorType } = gameScoreBody;

    if (type === LeagueGameScoreType.SUBMITTED) {
      const creatorOwner = await this.gameScoreService.findOwnerOfCreator(creator, creatorType);
      await this.ownerPermissionService.userAllowedToPerformActions(
        context.userId,
        creatorOwner.owner,
        creatorOwner.ownerType
      );
    } else {
      const owner = await this.matchService.findOwner(match);
      await this.ownerPermissionService.userAllowedToPerformActions(context.userId, owner.owner, owner.ownerType);
    }

    const foundGameScore = await this.gameScoreService.findOne(
      {
        match,
        gameNumber,
        type,
        creator: { $ne: creator }
      },
      { _id: true },
      { lean: true }
    );

    const savedGameScore = await this.gameScoreService.create(gameScoreBody);

    const adjustedCompetitorScores = competitorScores.map((score) => {
      const adjustedScore = score;
      adjustedScore.creator = creator;
      adjustedScore.creatorType = creatorType;
      adjustedScore.gameScore = savedGameScore._id;

      return adjustedScore;
    });
    const savedCompetitorScores = await this.scoreService.createMany(adjustedCompetitorScores);

    if (foundGameScore) {
      const foundScores = await this.scoreService.find(
        { gameScore: foundGameScore._id },
        { competitor: true, value: true },
        { lean: true }
      );
      const foundLeague = await this.getLeagueFromMatchId(match, gameScoreBody, competitorScores);

      await this.createConflictedOrFinalizedGameScore(
        match,
        gameNumber,
        foundLeague.owner,
        foundLeague.ownerType,
        savedCompetitorScores,
        foundScores
      );
    }

    await this.checkAndDeclareMatchWinner(match);

    return savedGameScore;
  }

  private static async updateLeagueGameScore(
    args: {
      gameScoreId: ObjectId;
      gameScoreBody: UpdateGameScoreBody;
      competitorScores?: UpdateCompetitorScoreBody[];
    },
    context: IGraphQLContext
  ): Promise<ILeagueGameScore> {
    const { gameScoreId, gameScoreBody, competitorScores } = args;
    const gameScore = await this.gameScoreService.findOne({ _id: gameScoreId });

    const leagueOwner = await this.gameScoreService.findOwner(gameScoreId);
    const creatorOwner = await this.gameScoreService.findOwnerOfCreator(gameScore?.creator, gameScore?.creatorType);
    await this.ownerPermissionService.userAllowedToPerformActionsForAny(context.userId, [leagueOwner, creatorOwner]);

    const newGameScore = await this.gameScoreService.updateOne({ _id: gameScoreId }, gameScoreBody, {
      lean: true,
      new: true
    });

    if (competitorScores && competitorScores.length > 0) {
      Promise.all(
        competitorScores.map((score) => this.scoreService.updateOne({ _id: score._id }, score, { lean: true }))
      ).catch((error) => logger.error(error, "Error resolving promise"));
    }

    await this.checkAndDeclareMatchWinner(newGameScore.match);

    return newGameScore;
  }

  /**
   * This will be slow almost no matter what.
   * The goal of this is to get the league owner and there is no way (that I could think of) to do this faster.
   *
   * TODO: When we make the move to SQL this can be converted to a single query.
   */
  private static async getLeagueEventFromMatchId(
    matchId: ObjectId,
    gameScoreBody?: ILeagueGameScore,
    competitorScores?: ILeagueCompetitorScore[]
  ): Promise<ILeagueEvent> {
    const foundMatch = await this.matchService.findOne({ _id: matchId }, { round: true }, { lean: true });

    if (!foundMatch) {
      throw Failure.UnprocessableEntity("Could not find match for game score", { gameScoreBody, competitorScores });
    }

    const foundRound = await this.roundService.findOne({ _id: foundMatch.round }, { bracket: true }, { lean: true });

    if (!foundRound) {
      throw Failure.UnprocessableEntity("Could not find round for game score", { gameScoreBody, competitorScores });
    }

    const foundBracket = await this.bracketService.findOne({ _id: foundRound.bracket }, { pool: true }, { lean: true });

    if (!foundBracket) {
      throw Failure.UnprocessableEntity("Could not find bracket for game score", { gameScoreBody, competitorScores });
    }

    const foundPool = await this.poolService.findOne({ _id: foundBracket.pool }, { event: true }, { lean: true });

    if (!foundPool) {
      throw Failure.UnprocessableEntity("Could not find pool for game score", { gameScoreBody, competitorScores });
    }

    const foundEvent = await this.eventService.findOne(
      { _id: foundPool.event },
      { league: true, gamesPerMatch: true },
      { lean: true }
    );

    if (!foundEvent) {
      throw Failure.UnprocessableEntity("Could not find event for game score", { gameScoreBody, competitorScores });
    }

    return foundEvent;
  }

  /**
   * This will be slow almost no matter what.
   * The goal of this is to get the league owner and there is no way (that I could think of) to do this faster.
   *
   * TODO: When we make the move to SQL this can be converted to a single query.
   */
  private static async getLeagueFromMatchId(
    matchId: ObjectId,
    gameScoreBody: ILeagueGameScore,
    competitorScores: ILeagueCompetitorScore[]
  ): Promise<ILeague> {
    const foundEvent = await this.getLeagueEventFromMatchId(matchId, gameScoreBody, competitorScores);

    if (!foundEvent) {
      throw Failure.UnprocessableEntity("Could not find event for game score", { gameScoreBody, competitorScores });
    }

    const foundLeague = await this.leagueService.findOne(
      { _id: foundEvent.league },
      { owner: true, ownerType: true },
      { lean: true }
    );

    if (!foundLeague) {
      throw Failure.UnprocessableEntity("Could not find league for game score", { gameScoreBody, competitorScores });
    }

    return foundLeague;
  }

  /**
   * Once enough games are won in a match by a team then that team will be marked as the winner.
   */
  private static async checkAndDeclareMatchWinner(matchId: ObjectId): Promise<void> {
    const match = await this.matchService.findOne({ _id: matchId }, { teams: true, winner: true }, { lean: true });

    if (!match) {
      throw Failure.UnprocessableEntity("League match does not exist", { matchId });
    }

    const leagueEventPromise = this.getLeagueEventFromMatchId(matchId);

    const finalizedGameScoresPromise = this.gameScoreService.find(
      { match: matchId, type: LeagueGameScoreType.FINALIZED },
      {},
      { lean: true }
    );

    const finishedPromises: [ILeagueEvent, ILeagueGameScore[]] = await Promise.all([
      leagueEventPromise,
      finalizedGameScoresPromise
    ]);

    const leagueEvent: ILeagueEvent = finishedPromises[0];
    const finalizedGameScores: ILeagueGameScore[] = finishedPromises[1];

    const { gamesPerMatch } = leagueEvent;
    const isGamesPerMatchEven = gamesPerMatch % 2 === 0;
    const numberOfWinsRequired = isGamesPerMatchEven ? gamesPerMatch / 2 + 1 : Math.ceil(gamesPerMatch / 2);

    // only start checking if we have enough game scores to possibly declare a winner
    if (numberOfWinsRequired <= finalizedGameScores.length) {
      const foundWinner = await this.determineWinnerForMatch(match.teams, finalizedGameScores, numberOfWinsRequired);

      if (foundWinner) {
        await this.matchService.updateOne(
          { _id: matchId },
          { winner: foundWinner, state: LeagueMatchState.FINISHED },
          { lean: true }
        );
      }
    }
  }

  /**
   * returns the teamId for the team who won the match if there are enough required wins
   */
  private static async determineWinnerForMatch(
    teams: Types.ObjectId[],
    gameScores: ILeagueGameScore[],
    numberOfWinsRequired: number
  ): Promise<ObjectId | null> {
    // this determines which team won for a game and returns the winning team. We end up with an array of team ids. This can tell us how many wins each team has.
    const teamWinsForMatch = await Promise.all(
      gameScores.map((gameScore) => this.determineWinnerForGame(gameScore._id))
    );

    // we find the number wins per team here
    const winsPerTeam = teams.map((team) => {
      return { team, numberOfWins: teamWinsForMatch.filter((teamId) => teamId.equals(team)).length };
    });

    // we pick one team to be the winner and then check against the rest to determine the actual winner
    let winner = winsPerTeam[0];
    winsPerTeam.forEach((teamWin) => {
      if (!teamWin.team.equals(winner.team) && teamWin.numberOfWins > winner.numberOfWins) {
        winner = teamWin;
      }
    });

    return winner && winner.numberOfWins >= numberOfWinsRequired ? winner.team : null;
  }

  /**
   * returns the teamId for the team who won a game for match
   */
  private static async determineWinnerForGame(gameScoreId: ObjectId): Promise<Types.ObjectId> {
    const foundCompetitorScores = await this.scoreService.find(
      { gameScore: gameScoreId },
      { competitor: true, value: true },
      { lean: true }
    );

    let winningTeamCompetitorScore = foundCompetitorScores[0];
    foundCompetitorScores.forEach((score) => {
      if (
        !score.competitor.equals(winningTeamCompetitorScore.competitor) &&
        score.value > winningTeamCompetitorScore.value
      ) {
        winningTeamCompetitorScore = score;
      }
    });

    return winningTeamCompetitorScore.competitor;
  }

  /**
   * We are comparing the scores for a game here.
   * The teams will submit scores for both sides.
   * If the scores match then we will automatically create a finalized score.
   * If there is a difference then the score will be created and marked as conflict. An admin will need to intervene then.
   */
  private static async createConflictedOrFinalizedGameScore(
    match: ObjectId,
    gameNumber: number,
    creator: ObjectId,
    ownerType: OwnerType,
    submittedCompetitorScores: ILeagueCompetitorScore[], // scores that were just submitted to be saved
    foundCompetitorScores: ILeagueCompetitorScore[] // scores that already exist in database for the gameNumber match
  ): Promise<void> {
    let creatorType = LeagueScoreCreatorType.ORGANIZATIONS;
    if (ownerType !== OwnerType.ORGANIZATIONS) {
      creatorType = LeagueScoreCreatorType.USERS;
    }

    const finalizedGameScore = <ILeagueGameScore>{
      match,
      gameNumber,
      creator,
      creatorType
    };

    // comparing scores for teams to find mismatches
    const foundInconsistentScore = submittedCompetitorScores.some((submittedScore) => {
      const foundMatchScore = foundCompetitorScores.find(
        (foundScore) =>
          submittedScore &&
          foundScore &&
          foundScore.competitor.equals(submittedScore.competitor) &&
          foundScore.value === submittedScore.value
      );

      return foundMatchScore == null;
    });

    finalizedGameScore.type = !foundInconsistentScore ? LeagueGameScoreType.FINALIZED : LeagueGameScoreType.CONFLICT;
    const savedGameScore = await this.gameScoreService.create(finalizedGameScore);

    const adjustedCompetitorScores = submittedCompetitorScores.map(
      (score) =>
        <ILeagueCompetitorScore>{
          creator: savedGameScore.creator,
          creatorType: savedGameScore.creatorType,
          value: !foundInconsistentScore ? score.value : 0,
          gameScore: savedGameScore._id,
          competitor: score.competitor,
          competitorType: score.competitorType
        }
    );

    await this.scoreService.createMany(adjustedCompetitorScores);
  }
}
