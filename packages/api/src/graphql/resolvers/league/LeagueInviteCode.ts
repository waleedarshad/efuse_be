import { InviteCodeType } from "@efuse/entities";
import { IResolvers } from "graphql-tools";
import { OrganizationACLService } from "../../../lib/organizations/organization-acl.service";
import { IGraphQLContext, IOrganization } from "../../../backend/interfaces";
import { InviteCodeService } from "../../../lib/invite-code/invite-code.service";
import { OrganizationAccessTypes } from "../../../lib/organizations/organization-access-types";

export class LeagueInviteCodeResolver {
  private static inviteCodeService = new InviteCodeService();
  private static organizationAclService = new OrganizationACLService();

  public static get resolve(): IResolvers {
    return {
      Organization: {
        leaguesInviteCode: (parent: IOrganization, _, context: IGraphQLContext) =>
          this.resolveLeaguesInviteCode(parent, _, context)
      }
    };
  }

  private static async resolveLeaguesInviteCode(parent: IOrganization, _, context: IGraphQLContext): Promise<string> {
    let authorized = true;

    // We want to return an empty string if the user isn't authorized to get the invite instead of
    // throwing an error but the authorizedToAccess function is designed to throw errors. So we need
    // to swallow the error and flag the user as being unauthorized.
    try {
      await this.organizationAclService.authorizedToAccess(context.userId, parent._id, OrganizationAccessTypes.ANY);
    } catch (error) {
      authorized = false;
    }

    const inviteCode = await this.inviteCodeService.findOne(
      { organization: parent._id, type: InviteCodeType.ORGANIZATION_LEAGUES, active: true },
      {},
      { lean: true }
    );

    return authorized ? <string>inviteCode?.code : "";
  }
}
