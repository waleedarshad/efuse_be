import { PaginateResult, Types } from "mongoose";
import { IResolvers } from "graphql-tools";
import { Failure } from "@efuse/contracts";
import { DocumentType } from "@typegoose/typegoose";
import { BeAnObject } from "@typegoose/typegoose/lib/types";
import { ILogger, Logger } from "@efuse/logger";

import {
  IGraphQLContext,
  IInviteCode,
  ILeague,
  ILeagueWithJoinStatus,
  IOrganization,
  ITeam
} from "../../../backend/interfaces";
import { LeagueService } from "../../../lib/leagues/league.service";
import { InviteCodeType, LeagueEntryType, LeagueJoinStatus, LeagueState, OwnerType } from "@efuse/entities";
import { OwnerPermissionService } from "../../../lib/shared";
import { GameService } from "../../../lib/game/game.service";
import { ObjectId, Owner } from "../../../backend/types";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";
import { Game } from "../../../backend/models/game.model";
import { TeamService } from "../../../lib/team";
import { InviteCodeService } from "../../../lib/invite-code/invite-code.service";
import { BrazeService } from "../../../lib/braze/braze.service";
import { BrazeCampaignName } from "../../../lib/braze/braze.enum";

interface UpdateLeague {
  name?: string;
  imageUrl?: string;
  description?: string;
  entryType?: LeagueEntryType;
  game?: ObjectId;
  // Can't use a string here due to the Mongoose update function so we need to use the Mongoose object.
  teams?: Types.ObjectId[];
  rules?: string;
}

export class LeagueResolver {
  private static leagueService = new LeagueService();
  private static ownerPermissionService: OwnerPermissionService = new OwnerPermissionService();
  private static teamService = new TeamService();
  private static organizationsService = new OrganizationsService();
  private static inviteCodeService: InviteCodeService = new InviteCodeService();
  private static brazeService: BrazeService = new BrazeService();
  private static gameService = new GameService();
  private static logger: ILogger = Logger.create({ name: "LeagueResolver" });

  public static get resolveAll(): IResolvers[] {
    return [this.resolveQueries(), this.resolveMutations(), this.resolveObjects()];
  }

  private static resolveQueries(): IResolvers {
    return {
      Query: {
        getLeagueById: async (_, args: { leagueId: ObjectId }): Promise<ILeague | null> => {
          const { leagueId } = args;

          if (!leagueId) {
            throw Failure.BadRequest("A league ID is required but was not supplied.");
          }

          try {
            const league = await this.leagueService.findOne({ _id: leagueId });

            return league;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve the league.", error);
          }
        },

        getLeaguesByOrganization: async (_, args: { organizationId: ObjectId }): Promise<ILeague[] | null> => {
          const { organizationId } = args;

          if (!organizationId) {
            throw Failure.BadRequest("An organization ID is required but was not supplied.");
          }

          try {
            const leagues = await this.leagueService.find({
              ownerType: OwnerType.ORGANIZATIONS,
              owner: organizationId
            });

            return leagues;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve the leagues.", error);
          }
        },

        getPaginatedLeagues: async (_, args: { page: number; limit: number }): Promise<PaginateResult<ILeague>> => {
          const { page, limit } = args;

          try {
            const leagues = await this.leagueService.findPaginated(
              {},
              {
                page: page,
                limit: limit,
                sort: { name: 1 },
                lean: true
              }
            );

            return leagues;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve any leagues.", error);
          }
        },

        getJoinedOrPendingLeaguesByOrg: async (
          _,
          args: { organizationId: string; joinStatus: LeagueJoinStatus }
        ): Promise<ILeagueWithJoinStatus[] | null> => {
          const { organizationId, joinStatus = LeagueJoinStatus.JOINED } = args;

          if (!organizationId) {
            throw Failure.BadRequest("An organization ID is required but was not supplied.");
          }

          if (
            joinStatus !== LeagueJoinStatus.JOINED &&
            joinStatus !== LeagueJoinStatus.PENDING &&
            joinStatus !== LeagueJoinStatus.JOINED_AND_PENDING
          ) {
            throw Failure.BadRequest("Join status must be 'joined', 'pending', or 'joinedAndPending'");
          }

          try {
            const leagues = await this.leagueService.getJoinedOrPendingLeagues(organizationId, joinStatus);

            return leagues;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve the leagues.", error);
          }
        }
      }
    };
  }

  private static resolveMutations(): IResolvers {
    return {
      Mutation: {
        createLeague: async (_, args: { league: ILeague }, context: IGraphQLContext): Promise<ILeague> => {
          const { league } = args;
          const { userId } = context;

          try {
            if (!league.entryType) {
              league.entryType = LeagueEntryType.INVITATIONAL;
            }

            league.state = LeagueState.ACTIVE;

            await this.ownerPermissionService.userAllowedToPerformActions(userId, league.owner, league.ownerType);
            await this.ownerPermissionService.userAllowedToCreateLeagues(userId, league.owner, league.ownerType);

            const savedLeague = await this.leagueService.create(league);

            if (savedLeague.ownerType === OwnerType.ORGANIZATIONS) {
              // this currently only supports organizations
              this.sendBrazeCampaignMessageForLeagueCreation(
                userId,
                savedLeague.name,
                savedLeague.owner,
                savedLeague.game
              ).catch((error) => this.logger?.error(error));
            }

            return savedLeague;
          } catch (error) {
            throw Failure.InternalServerError("Failed to create league.", error);
          }
        },

        deleteLeague: async (_, args: { leagueId: ObjectId }, context: IGraphQLContext): Promise<boolean> => {
          const { leagueId } = args;
          const { userId } = context;
          const projection = { owner: 1, ownerType: 1 };

          try {
            const leanLeague = await this.leagueService.findOne({ _id: leagueId }, projection, { lean: true });

            if (!leanLeague) {
              throw Failure.NotFound("League not found.", args);
            }

            await this.ownerPermissionService.userAllowedToPerformActions(
              userId,
              leanLeague.owner,
              leanLeague.ownerType
            );

            const deleted = await this.leagueService.delete(leagueId);

            return deleted;
          } catch (error) {
            throw Failure.InternalServerError("Failed to delete league.", error);
          }
        },

        updateLeague: async (
          _,
          args: { leagueId: ObjectId; updates: UpdateLeague },
          context: IGraphQLContext
        ): Promise<ILeague> => {
          const { leagueId, updates } = args;
          const { userId } = context;
          const projection = { owner: 1, ownerType: 1 };

          try {
            const leanLeague = await this.leagueService.findOne({ _id: leagueId }, projection, { lean: true });

            if (!leanLeague) {
              throw Failure.NotFound("League not found.", args);
            }

            await this.ownerPermissionService.userAllowedToPerformActions(
              userId,
              leanLeague.owner,
              leanLeague.ownerType
            );

            const updatedLeague = await this.leagueService.updateOne({ _id: leagueId }, updates, {
              lean: true,
              new: true
            });

            return updatedLeague;
          } catch (error) {
            throw Failure.InternalServerError("Failed to update league.", error);
          }
        }
      }
    };
  }

  private static resolveObjects(): IResolvers {
    return {
      // TODO: Like the OwnerType, this could probably be genericized and shared.
      League: {
        game: async (parent: ILeague): Promise<DocumentType<Game> | null> => {
          const gameService = new GameService();
          const game = await gameService.findOne({ _id: parent.game }, {}, { lean: true });

          return game;
        },

        owner: async (parent: ILeague): Promise<Owner | null> => {
          const { owner }: { owner: ObjectId } = parent;

          try {
            let foundDoc: Owner | null;

            switch (parent.ownerType) {
              case OwnerType.ORGANIZATIONS: {
                const organizationService = new OrganizationsService();
                foundDoc = await organizationService.findOne({ _id: owner }, {}, { lean: true });
                break;
              }
              case OwnerType.USERS: {
                const userService = new UserService();
                foundDoc = await userService.findOne({ _id: owner }, {}, { lean: true });
                break;
              }
              default:
                throw Failure.ExpectationFailed(
                  "Could not determine which type of document to retrieve",
                  parent.ownerType
                );
            }

            return foundDoc;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve owner data for favorite", error);
          }
        },
        teams: async (parent: ILeague) => this.resolveTeams(parent)
      }
    };
  }

  private static async resolveTeams(parent: ILeague): Promise<ITeam[]> {
    const { teams } = parent;

    if (!teams || teams.length === 0) {
      return [];
    }

    const foundTeams = await this.teamService.find({ _id: { $in: teams } }, {}, { lean: true });

    return foundTeams;
  }

  private static async sendBrazeCampaignMessageForLeagueCreation(
    currentUserId: ObjectId,
    leagueName: string,
    orgId: ObjectId,
    gameId: ObjectId
  ): Promise<void> {
    const foundGamePromise = this.gameService.findOne({ _id: gameId }, { title: true }, { lean: true });
    const foundOrganizationPromise = this.organizationsService.findOne({ _id: orgId }, { name: true }, { lean: true });
    const foundInviteCodePromise = this.inviteCodeService.findOne(
      {
        type: InviteCodeType.ORGANIZATION_LEAGUES,
        organization: orgId,
        active: true
      },
      { code: true },
      { lean: true }
    );

    const completedPromises: [DocumentType<Game, BeAnObject> | null, IOrganization | null, IInviteCode | null] =
      await Promise.all([foundGamePromise, foundOrganizationPromise, foundInviteCodePromise]);

    // completedPromises will have [ Game, IOrganization, IInviteCode] in that order
    // It is based on the order the promises are placed in the array for Promise.all
    const foundGame = completedPromises[0];
    const foundOrg = completedPromises[1];
    const foundInviteCode = completedPromises[2];

    this.brazeService.sendCampaignMessageAsync(BrazeCampaignName.LEAGUE_CREATED, {
      trigger_properties: {
        league_name: leagueName,
        organization_name: foundOrg?.name,
        invite_url: `https://efuse.gg/i/${foundInviteCode?.code}`,
        game_title: foundGame?.title
      },
      recipients: [{ external_user_id: String(currentUserId) }]
    });
  }
}
