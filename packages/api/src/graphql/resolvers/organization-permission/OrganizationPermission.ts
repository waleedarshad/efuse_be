import { IResolvers } from "graphql-tools";
import { IGraphQLContext, IOrganization, IOrganizationUserRole, IUser } from "../../../backend/interfaces";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { UserService } from "../../../lib/users/user.service";
import { ObjectId } from "../../../backend/types";
import { OrganizationUserRoleEnum } from "../../../lib/permissions/enums";

/**
 * Once Organizations and Users are moved to type-graphql then the endpoints here can be moved to the type-graphql file
 */
export class OrganizationPermissionResolver {
  private static organizationService = new OrganizationsService();
  private static userService = new UserService();

  static get resolve(): IResolvers {
    return {
      Query: {
        getUserOrgRoles: async (_, args, context) => this.getUserOrgRoles(args, context),
        getUsersLeagueOrgsAndRoles: async (_, args, context) => this.getUsersLeagueOrgsAndRoles(args, context),
        canUserCreateLeagues: async (_, args, context) => this.canUserCreateLeagues(args, context),
        canUserCreateLeaguesForOrg: async (_, args, context) => this.canUserCreateLeaguesForOrg(args, context),
        getUserOrgRole: async (_, args, context) => this.getUserOrgRole(args, context)
      },
      OrganizationUserRole: {
        user: async (parent) => this.resolveUser(parent)
      },
      Organization: {
        currentUserRole: async (parent: IOrganization, _, context) => this.getUserRole(parent, _, context)
      }
    };
  }

  private static async getUsersLeagueOrgsAndRoles(
    args: { userId?: ObjectId },
    context: IGraphQLContext
  ): Promise<IOrganizationUserRole> {
    const { userId } = args;

    const chosenUserId = userId || context.userId;

    const finalizedOutput = await this.organizationService.getUserOrgRoles(chosenUserId, true);

    return finalizedOutput;
  }

  private static async getUserOrgRoles(
    args: { userId?: ObjectId },
    context: IGraphQLContext
  ): Promise<IOrganizationUserRole> {
    const { userId } = args;

    const chosenUserId = userId || context.userId;

    const finalizedOutput = await this.organizationService.getUserOrgRoles(chosenUserId);

    return finalizedOutput;
  }

  private static async canUserCreateLeagues(args: { userId?: ObjectId }, context: IGraphQLContext): Promise<boolean> {
    const userId = args.userId ?? context.userId;

    const result = await this.organizationService.canUserCreateLeagues(userId);

    return result;
  }

  private static async canUserCreateLeaguesForOrg(
    args: { orgId: ObjectId; userId?: ObjectId },
    context: IGraphQLContext
  ): Promise<boolean> {
    const { orgId } = args;
    const userId = args.userId ?? context.userId;

    const result = await this.organizationService.canUserCreateLeaguesForOrg(userId, orgId);

    return result;
  }

  private static async getUserOrgRole(
    args: { orgId: ObjectId; userId?: ObjectId },
    context: IGraphQLContext
  ): Promise<OrganizationUserRoleEnum> {
    const { orgId } = args;
    const userId = args.userId ?? context.userId;

    const org = await this.organizationService.getOrganizationById(orgId.toString());
    const role = await this.organizationService.getUserRole(org, userId);

    return role;
  }

  private static async resolveUser(parent: IOrganizationUserRole): Promise<IUser | null> {
    const foundUser = await this.userService.findOne({ _id: parent.user }, {}, { lean: true });

    return foundUser;
  }

  private static async getUserRole(
    parent: IOrganization,
    _,
    context: IGraphQLContext
  ): Promise<OrganizationUserRoleEnum> {
    const resolvedOrganization = await this.organizationService.getOrganizationById(parent._id);
    return this.organizationService.getUserRole(resolvedOrganization, context?.userId);
  }
}
