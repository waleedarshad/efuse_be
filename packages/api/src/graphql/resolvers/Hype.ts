import { IResolvers } from "graphql-tools";

import { Hype } from "../../backend/models/hype/hype.model";
import { IGraphQLContext } from "../../backend/interfaces";
import { UserService } from "../../lib/users/user.service";

export class HypeResolver {
  private static $userService = new UserService();
  static get resolve(): IResolvers {
    return {
      HypedUser: {
        docs: async ({ docs }: { docs }) => {
          const userIds = docs.map((doc) => doc.user);
          const users = await this.$userService.find({ _id: { $in: userIds } });
          return users;
        }
      },
      Hype: {
        user: async (parent: Hype, _: any, context: IGraphQLContext) => {
          const user = await this.$userService.findOne({ _id: parent.user });
          return user;
        }
      }
    };
  }
}
