import { IValidationResponse } from "../../backend/interfaces";
import { ValidationService } from "../../lib/validations/validation.service";

const validationService = new ValidationService();

export const ValidationsResolver = {
  Query: {
    ValidateSlug: async (__, args: { slug: string; slugType: string }): Promise<IValidationResponse> => {
      const response = await validationService.validateSlug(args.slug, args.slugType);

      return response;
    }
  }
};
