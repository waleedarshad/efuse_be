import { StreakName } from "@efuse/entities";
import { DOMAIN } from "@efuse/key-store";
import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";
import { sampleSize } from "lodash";

import { IGraphQLContext, IPopulatedUserBadge, ITwitchAccountInfo, IUser } from "../../backend/interfaces";
import { StreaksBaseService } from "../../lib/streaks/streaks-base.service";
import { TokenLedgerService } from "../../lib/token-ledger";
import { TwitchAccountInfoService } from "../../lib/twitch-account-info.service";
import { UserBadgeService } from "../../lib/badge/user-badge.service";
import { UserGraphqlService } from "../../lib/users/user-graphql.service";
import { UserService } from "../../lib/users/user.service";
import { AuthService } from "../../lib/auth/auth.service";
import { AuthHelper } from "../../backend/helpers/auth.helper";
import { FriendBaseService } from "../../lib/friend/friend-base.service";
import { TalentCardsEnum } from "../../lib/users/user.enum";
import { PathwayService } from "../../lib/pathway/pathway.service";
import { IPathway } from "../../backend/interfaces/pathway";
import { UserStatsService } from "../../lib/user-stats/user-stats.service";
import { IUserStats } from "../../backend/interfaces/user-stats";
import { Roles } from "../../lib/users/roles";
import { UserBanningService } from "../../lib/users/user-banning.service";

interface Args {
  id: string;
}

interface IUpdatePasswordArgs {
  currentPassword: string;
  newPassword: string;
}

const userService = new UserService();
const tokenLedgerService = new TokenLedgerService();
const userGraphqlService = new UserGraphqlService();
const userBadgeService = new UserBadgeService();
const $twitchAccountInfoService = new TwitchAccountInfoService();
const $streaksBaseService = new StreaksBaseService();
const $authService = new AuthService();
const $authHelper = new AuthHelper();
const $friendBaseService = new FriendBaseService();
const $pathwayService = new PathwayService();
const $userStats = new UserStatsService();
const $userBanningService = new UserBanningService();

const validateAuthorization = (user) => {
  if (!user.roles?.includes(Roles.ADMIN)) {
    throw Failure.BadRequest("You are not authorized to perform this action");
  }
};

export const UserResolver = {
  Query: {
    getUserById: async (_, args) => {
      const { id } = <Args>args;
      return userService.findOne({ _id: id });
    },

    getAuthenticatedUser: async (_, args, context: IGraphQLContext): Promise<IUser> => {
      const { user } = context;

      return user;
    },

    GetTalentCards: async (_, args: { limit: number }, context: IGraphQLContext): Promise<IUser[]> => {
      const { limit } = args;

      // Converting limit to number
      let recordsToReturn = Number(limit);

      // Make sure limit is present, valid & non-negative
      if (Number.isNaN(recordsToReturn) || !recordsToReturn || recordsToReturn < 0) {
        recordsToReturn = TalentCardsEnum.DEFAULT_SAMPLE_SIZE;
      }

      // Make sure limit is not greater than Max allowed
      if (recordsToReturn > TalentCardsEnum.MAX_SAMPLE_SIZE) {
        recordsToReturn = TalentCardsEnum.MAX_SAMPLE_SIZE;
      }

      // Fetch existing friends
      const existingFriends = await $friendBaseService.getFollowings(context.userId);

      // Get followee ids array
      const existingFollowees = existingFriends.map((friend) => friend.followee);

      // Make we don't return the user himself
      existingFollowees.push(context.userId);

      // Removing Invalid Ids & Converting Ids to ObjectIds
      const existingFolloweesObjectIds = existingFollowees
        .filter((id) => $friendBaseService.isValidObjectId(id))
        .map((id) => new Types.ObjectId(id));

      // Get 100 users
      const users = await userService.find(
        {
          _id: { $nin: existingFolloweesObjectIds },
          deleted: false,
          status: "Active",
          portfolioProgress: { $gt: 50 }
        },
        {},
        { lean: true, limit: 100 }
      );

      return sampleSize(users, recordsToReturn);
    }
  },

  Mutation: {
    updateUser: async (_, args: { body: Partial<IUser> }, context: IGraphQLContext) => {
      const updatedUser = await userGraphqlService.updateUser(context.userId, args.body);
      return updatedUser;
    },

    updatePassword: async (
      _,
      args: { body: IUpdatePasswordArgs },
      context: IGraphQLContext
    ): Promise<{ success: boolean }> => {
      const { currentPassword, newPassword } = args.body;

      // make sure password is valid
      const validationError = $authHelper.validatePassword(newPassword);

      if (validationError) {
        throw Failure.BadRequest(validationError.password, validationError);
      }

      // Make sure currentPassword is valid
      const isMatched = await $authService.matchPassword(context.userId, currentPassword);

      if (!isMatched) {
        throw Failure.BadRequest("Invalid Password");
      }

      // Make sure user always enters a new password
      const matchedPasswordHistory = await $authService.matchPasswordHistory(context.userId, newPassword);

      if (matchedPasswordHistory) {
        throw Failure.BadRequest("You have recently used this password. Please try a new password");
      }

      // Update password
      await $authService.updatePassword(context.userId, newPassword);

      return { success: true };
    },
    banUser: async (_, args: { idOrUsername: string }, context: IGraphQLContext): Promise<{ success: boolean }> => {
      validateAuthorization(context.user);

      await $userBanningService.ban(args.idOrUsername);

      return { success: true };
    },
    unbanUser: async (_, args: { idOrUsername: string }, context: IGraphQLContext): Promise<{ success: boolean }> => {
      validateAuthorization(context.user);

      await $userBanningService.unban(args.idOrUsername);

      return { success: true };
    }
  },

  User: {
    url: (parent: IUser): string => {
      return `${DOMAIN}/u/${parent.username}`;
    },
    tokenBalance: async (parent: IUser): Promise<number> => {
      const userBalanceEntry = await tokenLedgerService.getUserBalanceEntry(parent._id);

      return userBalanceEntry ? userBalanceEntry.balance : 0;
    },
    email: (parent: IUser, _, context: IGraphQLContext): string | null => {
      if (String(parent._id) === String(context.userId)) {
        return parent.email || context.user.email;
      }

      return null;
    },
    gender: (parent: IUser, _, context: IGraphQLContext): string | undefined | null => {
      if (String(parent._id) === String(context.userId)) {
        return parent.gender || context.user.gender;
      }

      return null;
    },
    dateOfBirth: (parent: IUser, _, context: IGraphQLContext): string | undefined | null => {
      if (String(parent._id) === String(context.userId)) {
        return parent.dateOfBirth || context.user.dateOfBirth;
      }

      return null;
    },
    profileImage: (parent: IUser): string | undefined | null => {
      return parent.profilePicture?.url || "";
    },

    streak: async (parent: IUser): Promise<number> => {
      const streak = await $streaksBaseService.findOne(
        {
          user: parent._id,
          streakName: StreakName.DAILY_USAGE,
          isActive: true
        },
        { streakDuration: 1 },
        { sort: { updateAt: -1 } }
      );

      return streak?.streakDuration || 0;
    },

    online: (parent: IUser): boolean => {
      return (parent.showOnline && parent?.online) || false;
    },
    badges: async (parent: IUser): Promise<IPopulatedUserBadge[]> => {
      const badges = await userBadgeService.findAllByUserId(parent._id);
      return badges;
    },
    twitch: async (parent: IUser): Promise<ITwitchAccountInfo | null> => {
      const twitchAccountInfo: ITwitchAccountInfo | null = await $twitchAccountInfoService.findOne({
        owner: String(parent._id)
      });

      if (!twitchAccountInfo) {
        return null;
      }

      return twitchAccountInfo;
    },

    pathway: async (parent: IUser): Promise<IPathway | null> => {
      const pathway = await $pathwayService.findOne({ _id: parent.pathway }, {}, { lean: true });

      return pathway;
    },

    stats: async (parent: IUser): Promise<IUserStats | null> => {
      const userStats = await $userStats.findOne({ user: parent._id }, {}, { lean: true });

      return userStats;
    }
  }
};
