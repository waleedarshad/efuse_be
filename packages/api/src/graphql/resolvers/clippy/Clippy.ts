import { IResolvers } from "graphql-tools";
import { DocumentType } from "@typegoose/typegoose";

import { UserService } from "../../../lib/users/user.service";
import { IUser } from "../../../backend/interfaces";
import { Clippy } from "../../../backend/models/clippy/clippy.model";

export class ClippyResolver {
  private static $userService = new UserService();

  static get resolve(): IResolvers {
    return {
      Clippy: {
        user: async (parent: DocumentType<Clippy>): Promise<IUser | null> => {
          const user = await this.$userService.findOne({ _id: parent.user }, {}, { lean: true });

          return user;
        }
      }
    };
  }
}
