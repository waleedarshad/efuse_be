import { PaginateResult, LeanDocument, Types } from "mongoose";
import { ILogger, Logger } from "@efuse/logger";
import { Failure } from "@efuse/contracts";
import { IGraphQLContext, PaginationArgs } from "../../backend/interfaces";
import { IFeed, IFeedInput } from "../../backend/interfaces/feed";
import { IFeedGame } from "../../backend/interfaces/feed/feed-game";
import { Comment } from "../../backend/models/comment";
import { FeedService } from "../../lib/feeds/feed.service";
import { MediaService } from "../../lib/media/media.service";
import { UserService } from "../../lib/users/user.service";
import { HypeService } from "../../lib/hype/hype.service";
import { UserBadgeService } from "../../lib/badge/user-badge.service";
import { FeedGameBaseService } from "../../lib/feeds/feed-game-base.service";
import { PaginationHelper } from "../helpers/pagination.helper";
import { GamePostsPaginationLimitEnum } from "../../lib/feeds/feed.enum";
import { GiphyService } from "../../lib/giphy/giphy.service";
import { LoungeFeedService } from "../../lib/lounge/lounge-feed.service";
import { getFeedKind, isFollowed } from "../../backend/helpers/feed.helper";
import { IUser, IScheduledFeed, IScheduledFeedItem } from "../../backend/interfaces";
import { IUserStats } from "../../backend/interfaces/user-stats";
import { OpengraphTagsService } from "../../lib/opengraph-tags/opengraph-tags.service";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { IHomeFeed } from "../../backend/interfaces/home-feed";
import { IPostFeedItem } from "../../backend/interfaces/feed/post-feed-item";
import { HomeFeedService } from "../../lib/home-feed/home-feed.service";
import ViewsLib from "../../lib/views";
import { ILoungeFeed } from "../../backend/interfaces/lounge-feed";
import { ScheduledFeedService } from "../../lib/creatortools/scheduled-feed.service";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { YoutubeVideo } from "../../backend/models/youtube-video/youtube-video.model";
import { ObjectId } from "../../backend/types";
import { Giphy } from "../../backend/models/giphy/giphy.model";

const $logger: ILogger = Logger.create({ name: "FeaturedFeed" });

const feedService = new FeedService();
const hypeService = new HypeService();
const mediaService = new MediaService();
const userBadgeService = new UserBadgeService();
const userService = new UserService();
const feedGameBaseService = new FeedGameBaseService();
const paginationHelper = new PaginationHelper();
const $giphyService = new GiphyService();
const $loungeFeedService = new LoungeFeedService();
const $openGraphTagsService = new OpengraphTagsService();
const $homeFeedService = new HomeFeedService();
const $scheduledFeedService = new ScheduledFeedService();

interface IPaginatedGamePostsArgs extends PaginationArgs {
  gameId: string;
}

interface IFeedUpdateBody extends Omit<IPostFeedItem, "associatedYoutubeVideo"> {
  youtubeVideo: YoutubeVideo;
}

const getParentFeed = async (feedId: Types.ObjectId | string): Promise<ILoungeFeed | IHomeFeed> => {
  const homeFeed: IHomeFeed | null = await $homeFeedService.findOne({ feed: feedId }, {}, { lean: true });

  if (homeFeed) {
    return homeFeed;
  }

  const loungeFeed: ILoungeFeed | null = await $loungeFeedService.findOne({ feed: feedId }, {}, { lean: true });

  if (loungeFeed) {
    return loungeFeed;
  }

  throw Failure.NotFound("Could not find feed.");
};

export const PostResolver = {
  Query: {
    getPostById: async (_, { id }) => {
      const post = await feedService.findOne({ _id: <string>id }, {}, { lean: true });

      if (!post) {
        throw Failure.NotFound("Could not find post");
      }

      const feed: IHomeFeed | ILoungeFeed = await getParentFeed(id);

      return {
        ...post,
        parentFeedId: feed._id,
        kind: feed.timelineableType,
        kindId: String(feed.timelineable)
      };
    },

    getPostsByGame: async (_, args: IPaginatedGamePostsArgs): Promise<PaginateResult<IFeedGame>> => {
      const { page, limit } = paginationHelper.validatePaginationParams(
        args.page,
        args.limit,
        GamePostsPaginationLimitEnum.DEFAULT,
        GamePostsPaginationLimitEnum.MAX
      );

      const paginatedGames = await feedGameBaseService.getPaginatedFeedGames(
        { game: args.gameId },
        { page, limit, sort: { createdAt: -1 } }
      );

      return paginatedGames;
    },
    gethomeFeedPost: async (_, args: { feedId: string }, context: IGraphQLContext): Promise<IFeed | null> => {
      const { userId } = context;
      const { feedId } = args;

      if (!feedId || !feedService.isValidObjectId(feedId)) {
        throw Failure.BadRequest("Invalid id");
      }

      const feedObjectId = GeneralHelper.getObjectId(feedId);

      if (!userId) {
        throw Failure.BadRequest("Missing user");
      }

      const homeFeed: IHomeFeed | null = await $homeFeedService.findOne({ _id: feedObjectId }, {}, { lean: true });

      if (!homeFeed) {
        throw Failure.NotFound("Could not find home feed.");
      }

      ViewsLib.incrementFeedViews([feedId]).catch((error) => {
        $logger?.warn(error, "Error while incrementing Feed view count");
      });
      const feed = await feedService.findOne({ _id: homeFeed.feed }, {}, { lean: true });

      return <IFeed>{
        ...feed,
        original: homeFeed.original,
        parentFeedId: homeFeed._id,
        kind: homeFeed.timelineableType,
        kindId: <string>(<any>homeFeed.timelineable)?._id
      };
    },
    getLoungeFeedPost: async (_, args: { feedId: string }, context: IGraphQLContext): Promise<IFeed | null> => {
      const { userId } = context;
      const { feedId } = args;

      if (!feedId || !$loungeFeedService.isValidObjectId(feedId)) {
        throw Failure.BadRequest("Invalid id");
      }

      const feedObjectId = GeneralHelper.getObjectId(feedId);

      if (!userId) {
        throw Failure.BadRequest("Missing user");
      }

      // todo: investigate & fix
      // @ts-ignore
      const loungFeed: ILoungeFeed | null = await $loungeFeedService.findOne({ _id: feedObjectId }, {}, { lean: true });

      if (!loungFeed) {
        throw Failure.NotFound("Could not find lounge feed.");
      }

      ViewsLib.incrementFeedViews([feedId]).catch((error) => {
        $logger?.warn(error, "Error while incrementing Feed view count");
      });
      const feed = await feedService.findOne({ _id: loungFeed.feed }, {}, { lean: true });

      return <IFeed>(<unknown>{
        ...feed,
        parentFeedId: loungFeed._id,
        kind: loungFeed.timelineableType,
        kindId: String(loungFeed.timelineable)
      });
    }
  },

  Mutation: {
    boostPost: async (_, args: { feedId: string }, context: IGraphQLContext): Promise<boolean> => {
      const { userId } = context;
      const post = await feedService.findOne({ _id: args.feedId });
      const user = await userService.findOne({ _id: userId });

      if (!post) {
        throw Failure.BadRequest("Could not find post to boost");
      }

      if (!user) {
        throw Failure.InternalServerError("Unable to find user.");
      }

      if (post.metadata?.boosted) {
        throw Failure.BadRequest("Post already boosted");
      }

      const success = await feedService.toggleFeedBoost(user, post._id, true, true);

      return success;
    },
    deletePost: async (_, { feedId }, context: IGraphQLContext): Promise<boolean> => {
      const { user } = context;
      const deletedPost = await feedService.deletePost(user, feedId);
      return deletedPost;
    },
    createPost: async (_, args: { body: IFeedInput }, context: IGraphQLContext): Promise<IFeed | null> => {
      const { user } = context;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }
      const { body } = args;

      // Validate feed, create giphy & media object
      const feedAttachments = await feedService.validateAndCreateAttachments(
        user._id,
        user.username,
        body.text,
        body.twitchClip,
        body.file,
        body.files,
        body.giphy as Giphy | undefined,
        body.youtubeVideo
      );

      const homeFeed: IHomeFeed = await feedService.createFeedAndPerformActions(
        user._id,
        body.kindId,
        body.kind,
        user.verified || false,
        body.platform,
        body.mentions,
        body.text,
        feedAttachments.media?._id,
        feedAttachments.mediaObjects?.map((m) => m._id as Types.ObjectId),
        body.twitchClip,
        undefined,
        body.shared,
        GeneralHelper.getObjectId(body.sharedFeed),
        body.sharedTimeline,
        body.sharedTimelineable,
        body.sharedTimelineableType,
        GeneralHelper.getObjectId(body.sharedPostUser),
        body.crossPosts,
        body.associatedGame,
        body.youtubeVideo
      );

      const post: IFeed | null = await feedService.findOne({ _id: <string>homeFeed.feed }, {}, { lean: true });

      return <IFeed>{
        ...post,
        original: homeFeed.original,
        parentFeedId: homeFeed._id,
        kind: homeFeed.timelineableType,
        kindId: <string>(<any>homeFeed.timelineable)?._id
      };
    },
    createSchedulePost: async (
      _,
      args: { body: IFeedInput },
      context: IGraphQLContext
    ): Promise<IScheduledFeedItem | null> => {
      const { user } = context;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }
      const { body } = args;

      // Validate feed, create giphy & media object
      const feedAttachments = await feedService.validateAndCreateAttachments(
        user._id,
        user.username,
        body.text,
        body.twitchClip,
        body.file,
        body.files,
        body.giphy as Giphy,
        body.youtubeVideo
      );

      try {
        EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.create.calls");

        // converting to unknown first as IFeedInput does not overlap with IScheduledFeed
        const scheduledFeedBody: IScheduledFeed = <IScheduledFeed>(<unknown>{
          user: user._id,
          content: body.text,
          mentions: body.mentions,
          shared: body.shared,
          sharedFeed: body.sharedFeed,
          sharedTimeline: body.sharedTimeline,
          timelineable: body.kindId,
          timelineableType: body.kind,
          sharedPostUser: body.sharedPostUser,
          sharedTimelineable: body.sharedTimelineable,
          sharedTimelineableType: body.sharedTimelineableType,
          verified: user.verified || false,
          crossPosts: body.crossPosts,
          media: feedAttachments.media?._id,
          mediaObjects: feedAttachments.mediaObjects?.map((m) => m._id),
          scheduledAt: body.scheduledAt,
          platform: body.platform,
          twitchClip: body.twitchClip,
          associatedGame: body.associatedGame,
          youtubeVideo: body.youtubeVideo
        });

        const scheduledFeed = await $scheduledFeedService.create(scheduledFeedBody);
        EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.create.success");
        return <IScheduledFeedItem>{
          ...scheduledFeed.toObject(),
          parentFeedId: scheduledFeed._id,
          kind: scheduledFeed.timelineableType,
          kindId: scheduledFeed.timelineable
        };
      } catch (error) {
        EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.create.failure");

        throw error;
      }
    },
    updatePost: async (
      _,
      args: { id: string; body: IFeedUpdateBody },
      context: IGraphQLContext
    ): Promise<IFeed | null> => {
      const { id, body } = args;
      const { user } = context;

      if (!id) {
        throw Failure.BadRequest("feedId is required");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const feed: IHomeFeed | ILoungeFeed | null = await getParentFeed(id);

      const updatedfeed = await feedService.updateFeedPost(user._id, GeneralHelper.getObjectId(id), {
        ...body,
        associatedYoutubeVideo: body.youtubeVideo
      });

      return <IFeed>{
        ...updatedfeed,
        original: feed.original,
        parentFeedId: feed._id,
        kind: feed.timelineableType,
        kindId: <string>(<any>feed.timelineable)?._id
      };
    }
  },
  Post: {
    parentFeedId: (parent: IFeed) => {
      return parent?.parentFeedId;
    },
    kind: (parent: IFeed) => {
      return getFeedKind(parent?.kind || "");
    },
    text: (parent: IFeed) => {
      return parent.content;
    },
    mentions: (parent: IFeed) => {
      return parent.mentions;
    },
    feedId: (parent: IFeed) => {
      return parent._id;
    },
    author(parent: IFeed) {
      const { user } = parent;
      return userService.findOne({ _id: user }, {}, { lean: true });
    },
    hypes(parent: IFeed) {
      return hypeService.getPostHypeCount(parent._id);
    },
    views(parent: IFeed) {
      return parent.metadata?.views;
    },
    comments(parent: IFeed) {
      return Comment.countDocuments({ commentable: parent._id });
    },
    currentUserHypes: async (parent: IFeed, _: any, context: IGraphQLContext) => {
      const { userId } = context;
      const hype = await hypeService.findOne({ user: userId, objId: parent._id });
      return hype ? hype.hypeCount : 0;
    },
    media: async (parent: IFeed) => {
      const results = await mediaService.getAssociatedMedia(parent.user, parent._id, "feeds");
      return results.medias;
    },
    currentUserFollows: async (parent: IFeed, _: any, context: IGraphQLContext) => {
      const { userId } = context;
      const loungFeed = await $loungeFeedService.findOne({ feed: parent._id });

      if (loungFeed) {
        const followed = await isFollowed(loungFeed, loungFeed.timelineable, userId);
        return followed;
      }

      return false;
    },
    feedRank: (parent: { [k: string]: any }, _: any, context: IGraphQLContext): number => {
      const { user } = context;
      if (user.roles.includes("admin")) {
        return <number>parent.feedRank;
      }
      return 0;
    },
    authorBadges: async (parent: IFeed) => {
      const badges = await userBadgeService.findAllByUserId(parent.user);
      return badges;
    },
    giphy: async (parent: IFeed) => {
      if (parent.giphy) {
        const giphy = await $giphyService.findOne({ _id: parent.giphy as ObjectId });
        return giphy;
      }

      return null;
    },
    rankScore: async (parent: IFeed) => {
      const { user } = parent;

      const associatedUser: LeanDocument<IUser> | null = await userService.findOne(
        { _id: user },
        { stats: 1 },
        { lean: true, populate: "stats" }
      );

      const stats = associatedUser?.stats as IUserStats;

      return stats?.hypeScoreRanking;
    },
    openGraph: async (parent: IFeed) => {
      if (parent.metadata) {
        const opengraphTags = await $openGraphTagsService.getCorrespondingOGTags(parent.metadata?.sharedLinks);
        return opengraphTags;
      }
      return null;
    },
    boosted: (parent: IFeed) => {
      return parent.metadata?.boosted;
    }
  },
  PaginatedPost: {
    docs: async (parent: PaginateResult<IFeedGame>): Promise<IFeed[]> => {
      const feed = await feedService.find({ _id: { $in: parent.docs.map((fg) => fg.feed) } }, {}, { lean: true });

      return feed;
    }
  }
};
