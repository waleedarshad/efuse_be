import { IAimlabLeaderboardDeprecated, IValorantLeaderboardDeprecated } from "../../../backend/interfaces";
import { PipelineLeaderboardService } from "../../../lib/leaderboards/pipeline-leaderboard.service";

const pipelineLeaderboardService = new PipelineLeaderboardService();

export const PipelineLeaderboardDeprecatedResolver = {
  Stats: {
    __resolveType(parent: { kind: string }) {
      return parent.kind;
    }
  },

  Query: {
    getValorantLeaderboard: async (): Promise<IValorantLeaderboardDeprecated[]> => {
      const leaderboard = await pipelineLeaderboardService.getValorantLeaderboardDeprecated();
      return leaderboard;
    },

    getAimlabLeaderboard: async (): Promise<IAimlabLeaderboardDeprecated[]> => {
      const leaderboard = await pipelineLeaderboardService.getAimlabLeaderboardDeprecated();
      return leaderboard;
    }
  }
};
