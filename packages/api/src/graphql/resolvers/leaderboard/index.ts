import { PipelineLeaderboardResolver } from "./PipelineLeaderboard";
import { PipelineLeaderboardDeprecatedResolver } from "./PipelineLeaderboardDeprecated";

export const LeaderboardResolvers = [PipelineLeaderboardResolver];
export const LeaderboardDeprecatedResolvers = [PipelineLeaderboardDeprecatedResolver];
