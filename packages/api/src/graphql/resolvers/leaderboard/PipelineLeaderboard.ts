import { Failure } from "@efuse/contracts";
import { PaginateResult, Types } from "mongoose";
import { IValorantStats } from "../../../backend/interfaces/valorant/valorant-stats";
import { IRecruitmentProfile } from "../../../backend/interfaces/recruitment-profile";
import {
  IAimlabLeaderboard,
  ILeaderboardGame,
  ILeagueOfLegendsLeaderboard,
  IUser,
  IValorantLeaderboard
} from "../../../backend/interfaces";
import { UserService } from "../../../lib/users/user.service";
import { RecruitmentProfileService } from "../../../lib/recruitment/recruitment-profile.service";
import { PipelineLeaderboardService } from "../../../lib/leaderboards/pipeline-leaderboard.service";
import { AimlabProfileDto } from "../../../backend/models/aimlab";
import { ILeagueOfLegendsStats } from "../../../backend/interfaces/leagueOfLegends";
import { AimlabStatsService } from "../../../lib/aimlab";
import { LeagueOfLegendsStatsService } from "../../../lib/leagueOfLegends/league-of-legends-stats.service";
import { ValorantStatsService } from "../../../lib/valorant/valorant-stats.service";
import { ILeaderboardBase } from "../../../backend/interfaces/leaderboard/leaderboard-base";
import { LeaderboardStatType } from "../../enums";
import { PipelineLeaderboardType } from "../../../lib/leaderboards/pipeline-leaderboard-type.enum";

const pipelineLeaderboardService = new PipelineLeaderboardService();

export interface PipelineArgs {
  page: number;
  limit: number;
}

export interface TopPlayersArgs extends PipelineArgs {
  games: PipelineLeaderboardType[];
}

export class LeaderboardRanks {
  aimlabLeaderboardRank?: IAimlabLeaderboard | null;
  leagueOfLegendsLeaderboardRank?: ILeagueOfLegendsLeaderboard | null;
  valorantLeaderboardRank?: IValorantLeaderboard | null;

  constructor(
    aimlabLeaderboardRank?: IAimlabLeaderboard | null,
    leagueOfLegendsLeaderboardRank?: ILeagueOfLegendsLeaderboard | null,
    valorantLeaderboardRank?: IValorantLeaderboard | null
  ) {
    this.aimlabLeaderboardRank = aimlabLeaderboardRank;
    this.leagueOfLegendsLeaderboardRank = leagueOfLegendsLeaderboardRank;
    this.valorantLeaderboardRank = valorantLeaderboardRank;
  }
}

export const PipelineLeaderboardResolver = {
  User: {
    // I'm not sure I'm a big fan of this. To have the leaderboards all under a property for user
    // it has to be done like this. At least, from my reading and testing. The only other option I've
    // found is to have the individual leaderboards directly in the User instead of under the pipelineLeaderboardRanks property
    pipelineLeaderboardRanks: async (parent: IUser): Promise<LeaderboardRanks> => {
      const aimLeaderboard = pipelineLeaderboardService.getAimlabLeaderboardByUser(parent._id);
      const leagueLeaderboard = pipelineLeaderboardService.getLOLLeaderboardByUser(parent._id);
      const valLeaderboard = pipelineLeaderboardService.getValorantLeaderboardByUser(parent._id);

      try {
        // This will make it so its as fast as the slowest promise.
        const resolvedPromiseArray = await Promise.all([aimLeaderboard, leagueLeaderboard, valLeaderboard]);

        // values in array will be based on promise input position
        return new LeaderboardRanks(
          resolvedPromiseArray[0], // aimlab
          resolvedPromiseArray[1], // league of legends
          resolvedPromiseArray[2] // valorant
        );
      } catch (error) {
        throw Failure.InternalServerError("Error occurred retrieving leaderboard info", error);
      }
    }
  },
  Query: {
    getPaginatedAimlabLeaderboard: async (_, args: PipelineArgs): Promise<PaginateResult<IAimlabLeaderboard>> => {
      const leaderboard = await pipelineLeaderboardService.getPaginatedAimlabLeaderboard(args.page, args.limit);

      return leaderboard;
    },
    getPaginatedLeagueOfLegendsLeaderboard: async (
      _,
      args: PipelineArgs
    ): Promise<PaginateResult<ILeagueOfLegendsLeaderboard>> => {
      const leaderboard = await pipelineLeaderboardService.getPaginatedLOLLeaderboard(args.page, args.limit);

      return leaderboard;
    },
    getPaginatedValorantLeaderboard: async (_, args: PipelineArgs): Promise<PaginateResult<IValorantLeaderboard>> => {
      const leaderboard = await pipelineLeaderboardService.getPaginatedValorantLeaderboard(args.page, args.limit);

      return leaderboard;
    },
    getTopPipelinePlayers: async (_, args: TopPlayersArgs): Promise<ILeaderboardGame[]> => {
      // TODO: Find a way to automatically call for each pipeline game.
      const promiseArray: Promise<ILeaderboardGame>[] = [];

      if (!args.games || args.games.includes(PipelineLeaderboardType.AIM_LAB) || args.games.length <= 0) {
        const aimlab = pipelineLeaderboardService.getLeaderboardGame(
          PipelineLeaderboardType.AIM_LAB,
          args.page,
          args.limit
        );
        promiseArray.push(aimlab);
      }

      if (!args.games || args.games.includes(PipelineLeaderboardType.LEAGUE_OF_LEGENDS) || args.games.length <= 0) {
        const leagueOfLegends = pipelineLeaderboardService.getLeaderboardGame(
          PipelineLeaderboardType.LEAGUE_OF_LEGENDS,
          args.page,
          args.limit
        );
        promiseArray.push(leagueOfLegends);
      }

      if (!args.games || args.games.includes(PipelineLeaderboardType.VALORANT) || args.games.length <= 0) {
        const valorant = pipelineLeaderboardService.getLeaderboardGame(
          PipelineLeaderboardType.VALORANT,
          args.page,
          args.limit
        );
        promiseArray.push(valorant);
      }

      const resolvedPromiseArray = await Promise.all(promiseArray);

      return resolvedPromiseArray;
    }
  },
  Stats: {
    __resolveType(parent) {
      // we will look at variables exclusive to each stat collection to determine type
      // Example: actId should only be in the valorant stats collection

      if (parent?.ranking) {
        return LeaderboardStatType.AIM_LAB_STATS;
      }

      if (parent?.leagueInfo) {
        return LeaderboardStatType.LEAGUE_OF_LEGENDS_STATS;
      }

      if (parent?.actId) {
        return LeaderboardStatType.VALORANT_STATS;
      }

      throw Failure.NotFound("Could not determine which game stats to retrieve");
    }
  },
  PipelineLeaderboard: {
    recruitmentProfile: async (parent: ILeaderboardBase): Promise<IRecruitmentProfile> => {
      const recruitmentProfileService = new RecruitmentProfileService();
      const profile = await recruitmentProfileService.findOne({ _id: parent.recruitmentProfileId });

      if (!profile) {
        throw Failure.NotFound("Bad Request: Recruitment Profile was not found");
      }

      return profile;
    },
    // parent could be IAimlabLeaderboard, ILeagueOfLegendsLeaderboard, IValorantLeaderboard
    stats: async (parent): Promise<AimlabProfileDto | ILeagueOfLegendsStats | IValorantStats> => {
      let statsService; // we will initialize with the appropriate stats service
      let statsObjectId; // this will just contain the object id for a stats collection

      if (parent?.aimlabStatsId) {
        statsService = new AimlabStatsService();

        statsObjectId = <Types.ObjectId>parent.aimlabStatsId;
      }

      if (parent?.leagueOfLegendsStatsId) {
        statsService = new LeagueOfLegendsStatsService();

        statsObjectId = <Types.ObjectId>parent.leagueOfLegendsStatsId;
      }

      if (parent?.valorantStatsId) {
        statsService = new ValorantStatsService();

        statsObjectId = <Types.ObjectId>parent.valorantStatsId;
      }

      let stats;
      if (statsService && statsObjectId) {
        stats = await statsService.findOne({ _id: <Types.ObjectId>statsObjectId });
      }

      if (!stats) {
        throw Failure.NotFound("Bad Request: Could not determine which stats to retrieve");
      }

      return <AimlabProfileDto | ILeagueOfLegendsStats | IValorantStats>stats;
    },
    user: async (parent: ILeaderboardBase): Promise<IUser> => {
      const userService = new UserService();
      const user = await userService.findOne({ _id: parent.userId });

      if (!user) {
        throw Failure.NotFound("Bad Request: User was not found");
      }

      return user;
    }
  }
};
