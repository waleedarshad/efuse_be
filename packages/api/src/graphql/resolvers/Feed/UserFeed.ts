import { ILogger, Logger } from "@efuse/logger";
import { Failure } from "@efuse/contracts";
import { IResolvers } from "graphql-tools";
import { PaginateResult } from "mongoose";
import { OwnerKind } from "@efuse/entities";

import { IGraphQLContext, IHomeFeed } from "../../../backend/interfaces";
import { IFeed } from "../../../backend/interfaces/feed";
import { HomeFeedService } from "../../../lib/home-feed/home-feed.service";
import { PaginationHelper } from "../../helpers/pagination.helper";
import ViewsLib from "../../../lib/views";
import { FeedService } from "../../../lib/feeds/feed.service";

const paginationHelper = new PaginationHelper();

interface Args {
  page: number;
  limit: number;
  userId: string;
}

interface IHomeFeedPopulatedFeed extends Omit<IHomeFeed, "feed"> {
  feed: IFeed;
}

export class UserFeedResolver {
  private static $feedService = new FeedService();
  private static $homeFeedService = new HomeFeedService();
  private static $logger: ILogger = Logger.create({ name: "UserFeed" });

  static get resolve(): IResolvers {
    return {
      Query: {
        UserFeed: async (_, args: Args, context: IGraphQLContext): Promise<PaginateResult<IHomeFeedPopulatedFeed>> => {
          const { page, limit } = paginationHelper.validatePaginationParams(args.page, args.limit);

          if (!args.userId) {
            throw Failure.BadRequest("Missing parameters 'userId'");
          }

          const userHomeFeed: PaginateResult<IHomeFeedPopulatedFeed> = <PaginateResult<IHomeFeedPopulatedFeed>>(
            (<unknown>(
              await this.$homeFeedService.getPaginatedHomeFeed(
                { user: args.userId, timelineableType: OwnerKind.USER, timelineable: args.userId, original: true },
                { lean: true, page, limit, sort: { createdAt: -1 }, populate: "feed" }
              )
            ))
          );

          return userHomeFeed;
        }
      },

      UserFeed: {
        docs: async (parent: PaginateResult<IHomeFeedPopulatedFeed>, _args, context: IGraphQLContext) => {
          // Neglect Reported Posts
          const filteredDocs = parent.docs.filter((hf) => {
            const isReported = hf.feed?.reports?.find(
              (report) => String(report?.reportedBy) === String(context.userId)
            );

            return hf.feed && !isReported;
          });

          const formattedFeed: IFeed[] = <IFeed[]>filteredDocs.map((doc) => ({
            ...doc.feed,
            parentFeedId: doc._id,
            kind: doc.timelineableType,
            kindId: doc.timelineable
          }));

          const feedIds = parent.docs.map((doc) => doc.feed?._id).filter((id) => id);

          ViewsLib.incrementFeedViews(feedIds).catch((error) => {
            this.$logger?.warn(error, "Error while incrementing Feed view count");
          });

          return formattedFeed;
        }
      }
    };
  }
}
