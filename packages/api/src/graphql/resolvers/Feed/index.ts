import { CommentResolver } from "./Comment";
import { MentionResolver } from "./Mention";
import { VerifiedFeedResolver } from "./VerifiedFeed";
import { FollowingFeedResolver } from "./FollowingFeed";
import { OrganizationFeedResolver } from "./OrganizationFeed";
import { UserFeedResolver } from "./UserFeed";

export const FeedResolvers = [
  MentionResolver.resolve,
  CommentResolver.resolve,
  VerifiedFeedResolver.resolve,
  FollowingFeedResolver.resolve,
  OrganizationFeedResolver.resolve,
  UserFeedResolver.resolve
];
