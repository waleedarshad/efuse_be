import { ILogger, Logger } from "@efuse/logger";
import { IResolvers } from "graphql-tools";
import { LoungeFeedService } from "../../../lib/lounge/lounge-feed.service";
import { ILoungeFeed } from "../../../backend/interfaces/lounge-feed";
import { IGraphQLContext, PaginationArgs } from "../../../backend/interfaces/graphql";
import { IFeed } from "../../../backend/interfaces/feed";
import ViewsLib from "../../../lib/views";

export class VerifiedFeedResolver {
  private static $loungeFeedService = new LoungeFeedService();
  private static $logger: ILogger = Logger.create({ name: "VerifiedFeed" });

  static get resolve(): IResolvers {
    return {
      Query: {
        VerifiedFeed: async (_, args) => {
          const { page, limit } = <PaginationArgs>args;
          return this.$loungeFeedService.getVerifiedFeed(page, limit);
        }
      },
      VerifiedFeed: {
        docs: async ({ docs }: { docs: [ILoungeFeed] }, _, context: IGraphQLContext) => {
          const userId = context.userId.toHexString();

          const feedIds = docs.map((feed) => feed.feed);
          const $loungeFeedService = new LoungeFeedService();
          const verifiedFeed: IFeed[] = await $loungeFeedService.filterFeedByConstraints(feedIds, userId);

          const extendVerifiedFeed = verifiedFeed
            .map((feed: IFeed) => {
              const doc: ILoungeFeed | undefined = docs.find((f) => String(f.feed) === String(feed._id));

              // todo: investigate & fix
              // @ts-ignore
              feed.parentFeedId = doc?._id;
              // todo: investigate & fix
              // @ts-ignore
              feed.kind = doc?.timelineableType;
              feed.kindId = doc?.timelineable;
              return feed;
            })
            .sort((a, b) => {
              const order = feedIds.map((id) => String(id));
              return order.indexOf(String(a._id)) - order.indexOf(String(b._id));
            });

          ViewsLib.incrementFeedViews(extendVerifiedFeed.map((doc) => doc && doc._id)).catch((error) => {
            this.$logger?.warn(error, "Error while incrementing Feed view count");
          });

          return extendVerifiedFeed;
        }
      }
    };
  }
}
