import { ILogger, Logger } from "@efuse/logger";
import { Failure } from "@efuse/contracts";
import { IResolvers } from "graphql-tools";
import { FeedService } from "../../../lib/feeds/feed.service";
import { IGraphQLContext } from "../../../backend/interfaces/graphql";
import { IFeed } from "../../../backend/interfaces/feed";
import { HomeFeedService } from "../../../lib/home-feed/home-feed.service";
import { PaginationHelper } from "../../helpers/pagination.helper";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { IOrganization } from "../../../backend/interfaces/organization";
import { IHomeFeed } from "../../../backend/interfaces/home-feed";
import ViewsLib from "../../../lib/views";

const paginationHelper = new PaginationHelper();

interface Args {
  page: number;
  limit: number;
  orgId: string;
}

export class OrganizationFeedResolver {
  private static $feedService = new FeedService();
  private static $homefeedService = new HomeFeedService();
  private static $organizationsService = new OrganizationsService();
  private static $logger: ILogger = Logger.create({ name: "OrganizationFeed" });

  static get resolve(): IResolvers {
    return {
      Query: {
        OrganizationFeed: async (_, args: Args, context: IGraphQLContext) => {
          const { userId } = context;

          const { page, limit } = paginationHelper.validatePaginationParams(args.page, args.limit);

          if (!args.orgId) {
            throw Failure.BadRequest("Missing parameter 'orgId'");
          }

          const organization: IOrganization = await this.$organizationsService.getOrganizationById(args.orgId);
          const orgCaptains = organization.captains || [];
          const userIds = [...orgCaptains];

          if (organization.user) {
            userIds.push(organization.user);
          }

          const homeFeed = await this.$homefeedService.aggregateOrganizationFeed(
            userId,
            userIds,
            organization._id,
            page,
            limit
          );

          return homeFeed;
        }
      },
      OrganizationFeed: {
        docs: async ({ docs }: { docs: [IHomeFeed] }, _, context: IGraphQLContext) => {
          const { userId } = context;
          const feedIds = docs.map((feed) => feed.feed);

          const orgFeed = await this.$feedService.find(
            { _id: { $in: feedIds }, "reports.reportedBy": { $ne: userId }, deleted: { $ne: true } },
            {},
            { lean: true }
          );

          const homeFeed = orgFeed
            .map((feed: IFeed) => {
              const doc: IHomeFeed | undefined = docs.find((f) => String(f.feed) === String(feed._id));

              // todo: investigate & fix
              // @ts-ignore
              feed.parentFeedId = doc?._id;
              // todo: investigate & fix
              // @ts-ignore
              feed.kind = doc?.timelineableType;
              feed.kindId = doc?.timelineable;
              return feed;
            })
            .sort((a, b) => {
              const order = feedIds.map((id) => String(id));
              return order.indexOf(String(a._id)) - order.indexOf(String(b._id));
            });

          ViewsLib.incrementFeedViews(homeFeed.map((doc) => doc && doc._id)).catch((error) => {
            this.$logger?.warn(error, "Error while incrementing Feed view count");
          });

          return homeFeed;
        }
      }
    };
  }
}
