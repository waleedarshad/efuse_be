import { ILogger, Logger } from "@efuse/logger";
import { IResolvers } from "graphql-tools";
import { IHomeFeed } from "../../../backend/interfaces/home-feed";
import { HomeFeedService } from "../../../lib/home-feed/home-feed.service";
import { IGraphQLContext, PaginationArgs } from "../../../backend/interfaces/graphql";
import { FeedService } from "../../../lib/feeds/feed.service";
import ViewsLib from "../../../lib/views";
import { IFeed } from "../../../backend/interfaces/feed";

export class FollowingFeedResolver {
  private static $homefeedService = new HomeFeedService();
  private static $feedService = new FeedService();
  private static $logger: ILogger = Logger.create({ name: "FollowingFeed" });

  static get resolve(): IResolvers {
    return {
      Query: {
        FollowingFeed: async (_, args, context: IGraphQLContext) => {
          const { userId } = context;
          const { page, limit } = <PaginationArgs>args;
          const homeFeed = await this.$homefeedService.paginatedHomeFeed(userId, page, limit);
          return homeFeed;
        }
      },
      FollowingFeed: {
        docs: async ({ docs }: { docs: [IHomeFeed] }, _, context: IGraphQLContext) => {
          const { userId } = context;
          const feedIds = docs.map((feed) => feed.feed);

          const homeFeed = await this.$feedService.find(
            { _id: { $in: feedIds }, "reports.reportedBy": { $ne: userId }, deleted: { $ne: true } },
            {},
            { lean: true }
          );

          const extendHomeFeed = homeFeed
            .map((feed: IFeed) => {
              const doc = docs.find((f) => String(f.feed) === String(feed._id));

              // todo: investigate & fix
              // @ts-ignore
              feed.parentFeedId = doc?._id;
              // todo: investigate & fix
              // @ts-ignore
              feed.kind = doc?.timelineableType;
              feed.kindId = doc?.timelineable;
              return feed;
            })
            .filter((feed) => feed && feed.user)
            .sort((a, b) => {
              const order = feedIds.map((id) => String(id));
              return order.indexOf(String(a._id)) - order.indexOf(String(b._id));
            });

          ViewsLib.incrementFeedViews(homeFeed.map((doc) => doc && doc._id)).catch((error) => {
            this.$logger?.warn(error, "Error while incrementing Feed view count");
          });

          return extendHomeFeed;
        }
      }
    };
  }
}
