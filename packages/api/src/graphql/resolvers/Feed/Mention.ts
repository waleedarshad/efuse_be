import { IResolvers } from "graphql-tools";

import { UserService } from "../../../lib/users/user.service";
import { IMention } from "../../../backend/interfaces/mention";
import { IUser } from "../../../backend/interfaces";

export class MentionResolver {
  private static $userService = new UserService();

  static get resolve(): IResolvers {
    return {
      Mention: {
        user: async (parent: IMention): Promise<IUser | null> => {
          const user = await this.$userService.findOne({ _id: parent.id });

          return user;
        }
      }
    };
  }
}
