import { IResolvers } from "graphql-tools";
import { PaginateResult, FilterQuery, LeanDocument } from "mongoose";
import { maxBy } from "lodash";

import { CommentBaseService } from "../../../lib/comments/comment-base.service";
import {
  CommentTypeEnum,
  ThreadPaginationLimitEnum,
  CommentPaginationLimitEnum
} from "../../../lib/comments/comment.enum";
import { IComment } from "../../../backend/interfaces/comment";
import { UserService } from "../../../lib/users/user.service";
import { IUser, ICommentBody, IGraphQLContext, PaginationArgs, File } from "../../../backend/interfaces";
import { GiphyService } from "../../../lib/giphy/giphy.service";
import { Giphy } from "../../../backend/models/giphy/giphy.model";
import { IMention } from "../../../backend/interfaces/mention";
import { PaginationHelper } from "../../helpers/pagination.helper";
import { CommentService } from "../../../lib/comments/comment.service";
import { IMedia } from "../../../backend/interfaces/media";
import { MediaService } from "../../../lib/media/media.service";
import { MediaTypeEnum } from "../../../lib/media/media.enum";
import { HypeActionsService } from "../../../lib/hype/hype-actions.service";
import { Hype } from "../../../backend/models/hype/hype.model";
import { HypeService } from "../../../lib/hype/hype.service";
import { HypeEnum } from "@efuse/entities";
import { FeedService } from "../../../lib/feeds/feed.service";
import { IFeed } from "packages/api/src/backend/interfaces/feed";
import { CommentModel, Comment } from "../../../backend/models/comment";

interface IPaginatedCommentArgs extends PaginationArgs {
  feedId: string;
}

interface IThreadPaginatedCommentArgs extends PaginationArgs {
  feedId: string;
  parentId: string;
}

export class CommentResolver {
  private static $commentBaseService = new CommentBaseService();
  private static $userService = new UserService();
  private static $giphyService = new GiphyService();
  private static $paginationHelper = new PaginationHelper();
  private static $commentService = new CommentService();
  private static $mediaService = new MediaService();
  private static $hypeActionsService = new HypeActionsService();
  private static $hypeService = new HypeService();
  private static $feedService = new FeedService();

  // todo: investigate & fix
  // @ts-ignore
  private static $comments: CommentModel<IComment> = Comment;

  private static async getPaginatedComments(
    pageParam: number,
    limitParam: number,
    query: FilterQuery<IComment>,
    defaultLimit: number,
    maxLimit: number
  ): Promise<PaginateResult<IComment>> {
    const { page, limit } = this.$paginationHelper.validatePaginationParams(
      pageParam,
      limitParam,
      defaultLimit,
      maxLimit
    );

    const paginatedComments = await this.$commentBaseService.getPaginatedComments(query, {
      limit,
      page,
      sort: { createdAt: 1 }
    });

    paginatedComments.totalDocs = await this.$comments.countDocuments(query);
    return paginatedComments;
  }

  static get resolve(): IResolvers {
    return {
      Query: {
        GetPaginatedComments: async (_, args: IPaginatedCommentArgs): Promise<PaginateResult<IComment>> => {
          const paginatedComments = await this.getPaginatedComments(
            args.page,
            args.limit,
            {
              commentable: args.feedId,
              commentableType: CommentTypeEnum.FEED,
              parent: { $eq: null },
              deleted: { $ne: true } // Making sure to return active comments only
            },
            CommentPaginationLimitEnum.DEFAULT,
            CommentPaginationLimitEnum.MAX
          );

          return paginatedComments;
        },

        GetPaginatedThreadComments: async (_, args: IThreadPaginatedCommentArgs): Promise<PaginateResult<IComment>> => {
          const paginatedComments = await this.getPaginatedComments(
            args.page,
            args.limit,
            {
              commentable: args.feedId,
              commentableType: CommentTypeEnum.FEED,
              parent: args.parentId,
              deleted: { $ne: true } // Making sure to return active comments only
            },
            ThreadPaginationLimitEnum.DEFAULT,
            ThreadPaginationLimitEnum.MAX
          );

          return paginatedComments;
        }
      },

      Mutation: {
        CreateFeedComment: async (_, args: { body: ICommentBody }, context: IGraphQLContext): Promise<IComment> => {
          const comment = await this.$commentService.createFeedComment(context.user, args.body);

          return comment;
        },

        UpdateFeedComment: async (
          _,
          args: { body: { commentId: string; content: string; giphy: Giphy; file: File } },
          context: IGraphQLContext
        ): Promise<LeanDocument<IComment>> => {
          const { body } = args;

          const comment = await this.$commentService.updateFeedComment(body.commentId, context.userId, body);

          return comment;
        },

        DeleteFeedComment: async (_, args: { commentId: string }, context: IGraphQLContext): Promise<IComment> => {
          await this.$commentService.validatePermission(context.user, args.commentId);

          const comment = await this.$commentService.deleteComment(context.user, args.commentId);

          return comment;
        }
      },

      Comment: {
        content: (parent: IComment): string | null | undefined => {
          return parent.deleted ? null : parent.content;
        },

        user: async (parent: IComment): Promise<IUser | null> => {
          const user = await this.$userService.findOne({ _id: parent.user }, {}, { lean: true });

          return user;
        },

        giphy: async (parent: IComment): Promise<Giphy | null> => {
          if (!parent.deleted && parent.giphy) {
            const giphy = await this.$giphyService.findOne({ _id: parent.giphy });

            return giphy;
          }

          return null;
        },

        mentions: (parent: IComment): IMention[] | undefined => {
          return parent.deleted ? [] : parent.mentions;
        },
        hype: async (parent: IComment, _, context: IGraphQLContext): Promise<Hype | null> => {
          if (!parent.deleted) {
            const hype = await this.$hypeService.findOne(
              {
                user: context.userId,
                objId: parent._id as string,
                objType: HypeEnum.COMMENT
              },
              {},
              { lean: true }
            );

            return hype;
          }

          return null;
        },
        commentable: async (parent: IComment): Promise<IFeed | null> => {
          const post: IFeed | null = await this.$feedService.findOne({ _id: parent.commentable }, {}, { lean: true });
          return post;
        },
        thread: async (parent: IComment): Promise<PaginateResult<IComment>> => {
          const paginatedComments = await this.getPaginatedComments(
            1,
            ThreadPaginationLimitEnum.DEFAULT,
            { commentable: parent.commentable, commentableType: parent.commentableType, parent: parent._id as string },
            ThreadPaginationLimitEnum.DEFAULT,
            ThreadPaginationLimitEnum.MAX
          );

          return paginatedComments;
        },

        media: async (parent: IComment, _, context: IGraphQLContext): Promise<IMedia | undefined> => {
          const associatedMedia = await this.$mediaService.getAssociatedMedia(
            context.userId,
            parent._id,
            MediaTypeEnum.COMMENT
          );

          return associatedMedia.media;
        }
      },
      Post: {
        topComment: async (parent: IFeed): Promise<IComment | null> => {
          // Get all comments
          const comments = await this.$commentService.find(
            { commentable: parent._id, deleted: false },
            {},
            { sort: { createdAt: -1 }, lean: true }
          );

          // No need to proceed when we don't have comments
          if (comments.length === 0) {
            return null;
          }

          // Exit If there is only one comment & engagement < 3
          if (comments.length === 1 && comments[0].likes < 3) {
            return null;
          }

          const parentComments = comments.filter((comment) => !comment.parent);
          const replies = comments.filter((comment) => comment.parent);

          // Calculating engagement points
          parentComments.forEach((comment) => {
            comment.replyCount = replies.filter((reply) => String(reply.parent) === String(comment._id)).length;

            comment.thread = { totalDocs: comment.replyCount };

            comment.totalEngagement = comment.replyCount + comment.likes;
          });

          // Get the comment with highest engagement
          const topComment = maxBy(parentComments, "totalEngagement");

          // Exit if there is no comment or engagement < 3
          if (!topComment || topComment.totalEngagement < 3) {
            return null;
          }

          return topComment;
        }
      }
    };
  }
}
