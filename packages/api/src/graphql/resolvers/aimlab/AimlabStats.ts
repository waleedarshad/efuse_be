import { Failure, IAimlabProfile } from "@efuse/contracts";
import { Nullable } from "@efuse/types";

import { AimlabStatsService } from "../../../lib/aimlab";
import { IUser } from "../../../backend/interfaces/user";

const aimlabStatsService = new AimlabStatsService();

export const AimlabStatsResolver = {
  User: {
    aimlabStats: async (parent: IUser): Promise<Nullable<IAimlabProfile>> => {
      const stats = await aimlabStatsService.findOne({ owner: parent._id }, [], { sort: { createdAt: -1 } });

      if (stats?.ranking) {
        stats.ranking.badgeImage = aimlabStatsService.getBadgeImage(stats.ranking.rank);
      }

      return stats;
    }
  },
  Query: {
    getAimlabStats: async (_, args): Promise<Nullable<IAimlabProfile>> => {
      const { userId }: { userId: string } = args;
      if (!userId) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const stats = await aimlabStatsService.findOne({ owner: userId }, [], { sort: { createdAt: -1 } });

      if (stats?.ranking) {
        stats.ranking.badgeImage = aimlabStatsService.getBadgeImage(stats.ranking.rank);
      }

      return stats;
    }
  }
};
