import { DOMAIN } from "@efuse/key-store";
import { Types } from "mongoose";

import { OrganizationsService } from "../../lib/organizations/organizations.service";
import { OpportunityService } from "../../lib/opportunities/opportunity.service";
import { IOpportunity, IOrganization } from "../../backend/interfaces";
import { IUser } from "../../backend/interfaces/user";
import { UserService } from "../../lib/users/user.service";

interface Args {
  idOrShortName: Types.ObjectId | string;
}

export const OpportunityResolver = {
  Query: {
    getOpportunityByIdOrShortName: async (_, args) => {
      const { idOrShortName } = <Args>args;
      const opportunityService = new OpportunityService();
      return opportunityService.getOpportunityByIdOrShortName(idOrShortName);
    }
  },
  Opportunity: {
    author: async (parent: IOpportunity): Promise<IUser | null> => {
      const userService = new UserService();
      const user = await userService.findOne({ _id: parent.user });
      return user;
    },
    category: (parent: IOpportunity): string => {
      return parent.opportunityType;
    },
    subCategory: (parent: IOpportunity): string => {
      return parent.subType;
    },
    organization: async (parent: IOpportunity): Promise<IOrganization | null> => {
      const organizationService = new OrganizationsService();
      const org = await organizationService.findOne({ _id: parent.organization });
      if (!org) return null;
      return org;
    },
    url: (parent: IOpportunity): string => {
      return `${DOMAIN}/o/${parent.shortName}`;
    }
  }
};
