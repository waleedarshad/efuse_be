import { ApplicantResolver } from "./Applicant";
import { RequirementResolver } from "./Requirement";

export const ApplicantsResolvers = [ApplicantResolver, RequirementResolver];
