import { Types } from "mongoose";

import { OpportunityRequirementBaseService } from "../../../lib/opportunities/opportunity-requirement-base.service";
import { IERenaTournament, IOpportunityRequirements, IOpportunity } from "../../../backend/interfaces";

const opportunityRequirementsBaseService = new OpportunityRequirementBaseService();

/**
 * @summary Resolve requirements given requirement ids
 *
 * @param {Array<Types.ObjectId | string>} requirementIds
 *
 * @returns {Promise<IOpportunityRequirements[]>}
 */
const getRequirements = async (requirementIds: Array<Types.ObjectId | string>): Promise<IOpportunityRequirements[]> => {
  if (!requirementIds || requirementIds.length === 0) {
    return [];
  }

  const requirements = await opportunityRequirementsBaseService.findByIds(requirementIds);

  return requirements;
};

export const RequirementResolver = {
  Query: {
    GetRequirements: async (): Promise<IOpportunityRequirements[]> => {
      const requirements = await opportunityRequirementsBaseService.find({ isActive: true });

      return requirements;
    }
  },

  ERenaTournament: {
    requirements: async (parent: IERenaTournament): Promise<IOpportunityRequirements[]> => {
      const requirements = await getRequirements(parent.requirements);

      return requirements;
    }
  },

  Opportunity: {
    requirements: async (parent: IOpportunity): Promise<IOpportunityRequirements[]> => {
      const requirements = await getRequirements(parent.requirements);

      return requirements;
    }
  }
};
