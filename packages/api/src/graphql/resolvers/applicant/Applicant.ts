import { LeanDocument, PaginateResult } from "mongoose";
import { Failure } from "@efuse/contracts";

import { File } from "../../../backend/interfaces/file";
import {
  IApplicant,
  IOpportunity,
  IERenaTournament,
  IUser,
  IGraphQLContext,
  IOpportunityRequirementsResponse,
  IOrganization
} from "../../../backend/interfaces";
import { ApplicantService } from "../../../lib/applicants/applicant.service";
import { RegisteredObjectTypes } from "../../enums";
import { ApplicantEntityTypeEnum, ApplicantLimitEnum } from "../../../lib/applicants/applicant.enum";
import { UserService } from "../../../lib/users/user.service";
import { PaginationHelper } from "../../helpers/pagination.helper";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { OpportunityACLService } from "../../../lib/opportunities/opportunity-acl.service";
import { ValidateRequirementsService } from "../../../lib/applicants/validate-requirements.service";

const applicantService = new ApplicantService();
const userService = new UserService();
const paginationHelper = new PaginationHelper();
const erenaSharedService = new ERenaSharedService();
const opportunityACLService = new OpportunityACLService();
const validateRequirementsService = new ValidateRequirementsService();

export interface IApplicantBody {
  body: {
    entity: string;
    entityType: string;
    candidateQuestions: Array<{ index: number; question: string; media: File }>;
  };
}

export interface IGetApplicantsArgs {
  status: string;
  page: number;
  limit: number;
}

export interface IUpdateApplicantArgs {
  applicantId: string;
  opportunityId: string;
  sendEmail: boolean;
  status: string;
}

/**
 * @summary Get paginated applicants
 *
 * @param {IGetApplicantsArgs} args
 * @param {string} entityId
 * @param {ApplicantEntityTypeEnum} entityType
 * @param {string} currentUserId
 *
 * @returns {Promise<PaginateResult<IApplicant>>}
 */
const getPaginatedApplicants = async (
  args: IGetApplicantsArgs,
  entityId: string,
  entityType: ApplicantEntityTypeEnum,
  currentUserId: string
): Promise<PaginateResult<IApplicant>> => {
  const { page, limit } = paginationHelper.validatePaginationParams(
    args.page,
    args.limit,
    ApplicantLimitEnum.DEFAULT,
    ApplicantLimitEnum.MAX
  );

  const applicants = await applicantService.getPaginatedApplicants(
    currentUserId,
    { entity: entityId, entityType, status: args.status },
    { limit, page, sort: { createdAt: -1 } }
  );

  return applicants;
};

export const ApplicantResolver = {
  ApplicantEntity: {
    __resolveType(entity: LeanDocument<IOpportunity> | LeanDocument<IERenaTournament>) {
      return entity.kind || RegisteredObjectTypes.OPPORTUNITY;
    }
  },

  Mutation: {
    ApplicantApply: async (_, args: IApplicantBody, context: IGraphQLContext): Promise<IApplicant> => {
      const applicant = await applicantService.create(context.userId, args.body);

      return applicant;
    },

    updateApplicantStatus: async (_, args: IUpdateApplicantArgs, context: IGraphQLContext) => {
      const { applicantId, opportunityId, sendEmail, status } = args;
      const updatedApplicant = await applicantService.updateApplicantStatus(
        context.userId,
        applicantId,
        opportunityId,
        sendEmail,
        status
      );
      return updatedApplicant;
    }
  },

  Query: {
    ValidateApplicantRequirements: async (
      _,
      args: { entity: string; entityType: string },
      context: IGraphQLContext
    ): Promise<IOpportunityRequirementsResponse> => {
      const entityObject = (await applicantService.getEntity(args.entity, args.entityType)).toObject();

      const { canApply, requirementsMet, requirementsNotMet } =
        await validateRequirementsService.validateByRequirementIds(context.userId, entityObject.requirements);

      return { canApply, requirementsMet, requirementsNotMet };
    }
  },

  Applicant: {
    entity: async (parent: IApplicant): Promise<LeanDocument<IOpportunity> | LeanDocument<IERenaTournament>> => {
      const entity = (await applicantService.getEntity(parent.entity, parent.entityType)).toObject();

      // Set the kind for resolveType for union type ApplicantEntity
      entity.kind =
        parent.entityType === ApplicantEntityTypeEnum.OPPORTUNITY
          ? RegisteredObjectTypes.OPPORTUNITY
          : RegisteredObjectTypes.ERENA_TOURNAMENT;

      return entity;
    },

    user: async (parent: IApplicant): Promise<IUser | null> => {
      const user = await userService.findOne({ _id: parent.user }, "+address", { lean: true });

      return user;
    }
  },

  ERenaTournament: {
    paginatedApplicants: async (
      parent: IERenaTournament,
      args: IGetApplicantsArgs,
      context: IGraphQLContext
    ): Promise<PaginateResult<IApplicant> | null> => {
      // Don't resolve applicants if user is not owner
      if (!(await erenaSharedService.verifyTournamentOwnership(context.userId, parent))) {
        return null;
      }

      const applicants = await getPaginatedApplicants(
        args,
        parent._id as string,
        ApplicantEntityTypeEnum.ERENA_TOURNAMENT,
        String(context.userId)
      );

      return applicants;
    },

    ableToApply: async (
      parent: IERenaTournament,
      __,
      context: IGraphQLContext
    ): Promise<IOpportunityRequirementsResponse> => {
      const { canApply, requirementsMet, requirementsNotMet } =
        await validateRequirementsService.validateByRequirementIds(context.userId, parent.requirements);

      return { canApply, requirementsMet, requirementsNotMet };
    }
  },

  Opportunity: {
    paginatedApplicants: async (
      parent: IOpportunity,
      args: IGetApplicantsArgs,
      context: IGraphQLContext
    ): Promise<PaginateResult<IApplicant> | null> => {
      // Don't resolve applicants if user is not owner

      // since we are using graphQL the organization is an object here not just an id
      const organizationId = <string>(<IOrganization>(<unknown>parent.organization))?._id;
      const hasPermission = await opportunityACLService.verifyOwnership(context.user, parent, organizationId);

      if (!hasPermission) {
        throw Failure.Forbidden("User is not allowed to perform this action.");
      }

      const applicants = await getPaginatedApplicants(
        args,
        parent._id as string,
        ApplicantEntityTypeEnum.OPPORTUNITY,
        String(context.userId)
      );

      return applicants;
    },

    ableToApply: async (
      parent: IERenaTournament,
      __,
      context: IGraphQLContext
    ): Promise<IOpportunityRequirementsResponse> => {
      const { canApply, requirementsMet, requirementsNotMet } =
        await validateRequirementsService.validateByRequirementIds(context.userId, parent.requirements);

      return { canApply, requirementsMet, requirementsNotMet };
    }
  }
};
