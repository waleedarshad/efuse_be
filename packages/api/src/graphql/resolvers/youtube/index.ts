import { YoutubeVideoResolver } from "./YoutubeVideo";

export const YoutubeResolvers = [YoutubeVideoResolver.resolve];
