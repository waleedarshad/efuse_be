import { IResolvers } from "graphql-tools";

import { YoutubeVideoBaseService } from "../../../lib/youtube/youtube-video-base.service";
import { IFeed } from "../../../backend/interfaces/feed";
import { YoutubeVideo } from "../../../backend/models/youtube-video/youtube-video.model";

export class YoutubeVideoResolver {
  private static $youtubeVideoBaseService = new YoutubeVideoBaseService();

  static get resolve(): IResolvers {
    return {
      YoutubeVideo: {
        videoUrl: (parent: YoutubeVideo): string | null => {
          if (parent.videoId) {
            return `https://www.youtube.com/watch?v=${parent.videoId}`;
          }

          return null;
        }
      },
      Post: {
        youtubeVideo: async (parent: IFeed): Promise<YoutubeVideo | null> => {
          if (parent.youtubeVideo) {
            const youtubeVideo = await this.$youtubeVideoBaseService.findOne(
              { _id: parent.youtubeVideo },
              { etag: 0, kind: 0 },
              { lean: 1 }
            );

            return youtubeVideo;
          }

          return null;
        }
      }
    };
  }
}
