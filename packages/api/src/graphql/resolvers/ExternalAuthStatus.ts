import { OAuthServiceKind } from "@efuse/entities";
import { IExternalAuthStatus } from "../../backend/interfaces/external-auth-status";
import { IUser } from "../../backend/interfaces/user";
import { ExternalAuthStatusService } from "../../lib/external-account-auth/external-auth-status.service";

const externalAuthStatusService = new ExternalAuthStatusService();

export const ExternalAuthStatusResolver = {
  User: {
    externalAuthStatus: async (parent: IUser): Promise<IExternalAuthStatus> => {
      const result = {};

      for (const service of Object.values(OAuthServiceKind)) {
        result[service] = await externalAuthStatusService.getOwnerConnectionStatus(service, parent.id);
      }

      return <IExternalAuthStatus>result;
    }
  }
};
