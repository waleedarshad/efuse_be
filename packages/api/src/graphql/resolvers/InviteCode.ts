import { Failure } from "@efuse/contracts";
import { InviteCodeType, InviteResourceType, OwnerType } from "@efuse/entities";
import { IResolvers } from "graphql-tools";
import { InviteCodeService } from "../../lib/invite-code/invite-code.service";
import { IInviteCode, ILeague, ILeagueEvent, IOrganization, ITeam, IUser } from "../../backend/interfaces";
import { LeagueEventService, LeagueService } from "../../lib/leagues";
import { OrganizationsService } from "../../lib/organizations/organizations.service";
import { TeamMemberService, TeamService } from "../../lib/team";
import { UserService } from "../../lib/users/user.service";
import { ObjectId } from "../../backend/types";
import { OwnerPermissionService } from "../../lib/shared";

type ResolveInviteResourceType = ITeam | ILeague | ILeagueEvent;

interface AcceptTeamInviteArgs {
  entityId: ObjectId;
  code: string;
}

export class InviteCodeResolver {
  private static inviteCodeService = new InviteCodeService();
  private static organizationService = new OrganizationsService();
  private static userService = new UserService();
  private static leagueService = new LeagueService();
  private static leagueEventService = new LeagueEventService();
  private static teamService = new TeamService();
  private static teamMemberService = new TeamMemberService();
  private static ownerPermissionService = new OwnerPermissionService();

  public static get resolveAll(): IResolvers[] {
    return [this.resolveQueries(), this.resolveMutations(), this.resolveObjects()];
  }

  private static resolveQueries(): IResolvers {
    return {
      Query: {
        getInviteCode: async (_, args: { code: string }): Promise<IInviteCode | null> => {
          const { code } = args;

          if (!code) {
            throw Failure.BadRequest("A code is required but was not supplied.");
          }

          return await this.inviteCodeService.findOne({ code }, {}, { lean: true });
        }
      }
    };
  }

  private static resolveMutations(): IResolvers {
    return {
      Mutation: {
        acceptInvite: async (_, args, context) => this.acceptInvite(args, context)
      }
    };
  }

  private static resolveObjects(): IResolvers {
    return {
      InviteResource: {
        __resolveType: (parent: ResolveInviteResourceType) => this.getInviteResourceType(parent)
      },
      InviteCode: {
        user: async (parent: IInviteCode): Promise<IUser | null> => {
          if (!parent.user) {
            return null;
          }

          const user = await this.userService.findOne({ _id: parent.user }, {}, { lean: true });

          return user;
        },
        organization: async (parent: IInviteCode): Promise<IOrganization | null> => {
          if (!parent.organization) {
            return null;
          }

          const organization = await this.organizationService.findOne({ _id: parent.organization }, {}, { lean: true });

          return organization;
        },
        resource: async (parent: IInviteCode): Promise<ILeague | ILeagueEvent | ITeam | null> => {
          if (parent.resourceType === InviteResourceType.LEAGUE) {
            return await this.leagueService.findOne({ _id: parent.resource }, {}, { lean: true });
          }

          if (parent.resourceType === InviteResourceType.LEAGUE_EVENT) {
            return await this.leagueEventService.findOne({ _id: parent.resource }, {}, { lean: true });
          }

          if (parent.resourceType === InviteResourceType.TEAM) {
            return await this.teamService.findOne({ _id: parent.resource }, {}, { lean: true });
          }

          return null;
        }
      }
    };
  }

  private static getInviteResourceType(parent: ResolveInviteResourceType): string | null {
    if ((<ILeague>parent).entryType) {
      return "League";
    }

    if ((<ILeagueEvent>parent).league) {
      return "LeagueEvent";
    }

    if ((<ITeam>parent).game && (<ITeam>parent).name) {
      return "Team";
    }

    return null;
  }

  private static async acceptInvite(args: AcceptTeamInviteArgs, context): Promise<boolean> {
    const { code, entityId } = args;

    const invite = await this.inviteCodeService.findOne({ code }, {}, { lean: true });

    if (!invite) {
      throw Failure.BadRequest("Invite code does not exist", {
        Invite_Code: code
      });
    }

    if (invite?.resourceType) {
      return this.acceptBasedOnResourceType(invite, entityId);
    }

    switch (invite.type) {
      case InviteCodeType.ORGANIZATION_LEAGUES: {
        const orgId: ObjectId = (<IOrganization>invite.organization)._id ?? invite.organization;
        await this.ownerPermissionService.userAllowedToPerformActions(
          context.userId,
          entityId,
          OwnerType.ORGANIZATIONS
        );

        return this.leagueService.giveOrgLeagueJoinPermissions(entityId, orgId);
      }
      default:
        throw Failure.ExpectationFailed("Could not determine type for invite", { code, entityId });
    }
  }

  private static async acceptTeamInvite(
    teamId: ObjectId,
    userId: ObjectId,
    organizationId: ObjectId,
    inviteCode: string
  ): Promise<boolean> {
    const userExists = await this.userService.exists({ _id: userId });

    if (!userExists) {
      throw Failure.UnprocessableEntity("User does not exist", {
        user: userId,
        Invite_Code: inviteCode
      });
    }

    const query = organizationId ? { _id: teamId, owner: organizationId } : { _id: teamId };
    const teamExists = await this.teamService.exists(query);

    if (!teamExists) {
      throw Failure.UnprocessableEntity("Team associated with invite code could not be found", {
        Invite_Code: inviteCode
      });
    }

    const mustBeCurrentOrgMember = false;
    const member = await this.teamMemberService.addUserToOrganizationTeam(teamId, userId, "", mustBeCurrentOrgMember);

    return member != null;
  }

  private static async acceptBasedOnResourceType(invite: IInviteCode, entityId: ObjectId): Promise<boolean> {
    if (invite.type === InviteCodeType.LEAGUE && invite?.resourceType === InviteResourceType.LEAGUE) {
      // TODO: Temporary, so other teams can keep working. Will be filled in with actual code shortly.
      return true;
    }

    if (invite.type === InviteCodeType.LEAGUE_EVENT && invite?.resourceType === InviteResourceType.LEAGUE_EVENT) {
      // TODO: Temporary, so other teams can keep working. Will be filled in with actual code shortly.
      return true;
    }

    if (invite.type === InviteCodeType.TEAM && invite?.resourceType === InviteResourceType.TEAM) {
      return this.acceptTeamInvite(invite.resource, entityId, <ObjectId>invite.organization, invite.code);
    }

    throw Failure.ExpectationFailed("A mismatch appears on this invite. Resource type does not matchup with the type", {
      invite
    });
  }
}
