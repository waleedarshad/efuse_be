import { Failure } from "@efuse/contracts";
import { ILeagueOfLegendsStats } from "../../../backend/interfaces/leagueOfLegends";
import { IUser } from "../../../backend/interfaces/user";
import { LeagueOfLegendsStatsService } from "../../../lib/leagueOfLegends/league-of-legends-stats.service";

const leagueOfLegendsStatsService = new LeagueOfLegendsStatsService();

export const LeagueOfLegendsStatsResolver = {
  Query: {
    getLeagueOfLegendsStats: async (_, args): Promise<ILeagueOfLegendsStats> => {
      const { id }: { id: string } = args;
      if (!id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const stats = await leagueOfLegendsStatsService.findOne({ _id: id }, null, { sort: { createdAt: -1 } });

      return <ILeagueOfLegendsStats>stats;
    }
  },
  User: {
    leagueOfLegendsStats: async (parent: IUser): Promise<ILeagueOfLegendsStats | null> => {
      const id = parent?.leagueOfLegends?.stats;
      if (!id) return null;

      // TODO: [spavel] update this to use the user id once we add that to the lol stats object
      const stats = await leagueOfLegendsStatsService.findOne({ _id: id }, null, { sort: { createdAt: -1 } });

      return <ILeagueOfLegendsStats>stats;
    }
  }
};
