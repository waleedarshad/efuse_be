import { DocumentType } from "@typegoose/typegoose";

import { ConstantsService } from "../../../lib/constants";
import { GameBaseService } from "../../../lib/game/game-base.service";
import { Game } from "../../../backend/models/game.model";

const gameService = new GameBaseService();
const constantsService = new ConstantsService();

export interface IGamePipelineInfo {
  pipelineEnabled: boolean;
  roles: string[];
}

export interface IRecruitmentVariables {
  requiredFieldsUSA: string[];
  requiredFieldsInternational: string[];
  games: {
    [key: string]: {
      roles: string[];
    };
  };
}

export const PipelineConstantsResolver = {
  Game: {
    pipelineInfo: async (parent: Game): Promise<IGamePipelineInfo> => {
      const variables = (await constantsService.getVariables("RECRUITMENT_PROFILE")) as IRecruitmentVariables;
      const gameInfo = variables.games[parent.slug];
      return {
        pipelineEnabled: !!gameInfo,
        roles: gameInfo?.roles
      };
    }
  },
  Query: {
    getPipelineGames: async (): Promise<DocumentType<Game>[]> => {
      const { games: pipelineGames } = (await constantsService.getVariables(
        "RECRUITMENT_PROFILE"
      )) as IRecruitmentVariables;
      const games = await gameService.find({}, null, { lean: true });

      return games.filter((game) => pipelineGames[game.slug]);
    }
  }
};
