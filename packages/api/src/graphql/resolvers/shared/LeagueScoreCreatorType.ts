import { IResolvers } from "graphql-tools";
import { Failure } from "@efuse/contracts";
import { IOrganization, ITeam, IUser } from "../../../backend/interfaces";
import { LeagueScoreCreator } from "../../../backend/types";
import { LeagueScoreCreatorTypeDef } from "../../enums";

// be careful when modifying this because many newer graphql types use this
export function resolveLeagueScoreCreatorType(): IResolvers {
  return {
    LeagueScoreCreator: {
      __resolveType(obj: LeagueScoreCreator | null, context: any, info: any): LeagueScoreCreatorTypeDef {
        // This should only exist on a user object.
        if ((<IUser>obj)?.username) {
          return LeagueScoreCreatorTypeDef.USER;
        }

        // This should only exist on an organization object.
        if ((<IOrganization>obj)?.organizationType) {
          return LeagueScoreCreatorTypeDef.ORGANIZATION;
        }

        // This should only exist on an team object.
        if ((<ITeam>obj)?.owner) {
          return LeagueScoreCreatorTypeDef.TEAM;
        }

        throw Failure.ExpectationFailed("Could not determine the owner type.", obj);
      }
    }
  };
}
