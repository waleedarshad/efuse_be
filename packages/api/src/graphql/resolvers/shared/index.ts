import { IResolvers } from "graphql-tools";
import { resolveLeagueScoreCreatorType } from "./LeagueScoreCreatorType";
import { resolveOwnerType } from "./OwnerType";

export class SharedResolver {
  public static get resolveAll(): IResolvers[] {
    return [resolveOwnerType(), resolveLeagueScoreCreatorType()];
  }
}
