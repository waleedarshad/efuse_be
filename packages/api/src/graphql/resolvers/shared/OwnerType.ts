import { IResolvers } from "graphql-tools";
import { Failure } from "@efuse/contracts";
import { IOrganization, IUser } from "../../../backend/interfaces";
import { Owner } from "../../../backend/types";
import { OwnerTypeDef } from "../../enums";

// be careful when modifying this because many newer graphql types use this
export function resolveOwnerType(): IResolvers {
  return {
    Owner: {
      __resolveType(obj: Owner | null, context: any, info: any): OwnerTypeDef {
        // This should only exist on a user object.
        if ((<IUser>obj)?.username) {
          return OwnerTypeDef.USER;
        }

        // This should only exist on an organization object.
        if ((<IOrganization>obj)?.organizationType) {
          return OwnerTypeDef.ORGANIZATION;
        }

        throw Failure.ExpectationFailed("Could not determine the owner type.", obj);
      }
    }
  };
}
