import { Failure } from "@efuse/contracts";
import { DocumentType } from "@typegoose/typegoose";

import { IOrganization, IGame } from "../../backend/interfaces";
import { IERenaTournament } from "../../backend/interfaces/erena";
import { IGraphQLContext } from "../../backend/interfaces/graphql";
import { IUser } from "../../backend/interfaces/user";
import { ERenaGameService } from "../../lib/erena/erena-game.service";
import { OrganizationGameService } from "../../lib/organizations/organization-game.service";
import { UserGameService } from "../../lib/users/user-game.service";
import { GameBaseService } from "../../lib/game/game-base.service";
import { FeedGameService } from "../../lib/feeds/feed-game.service";
import { IFeed } from "../../backend/interfaces/feed";
import { Game } from "../../backend/models/game.model";

const gameService = new GameBaseService();
const organizationGameService = new OrganizationGameService();
const userGameService = new UserGameService();
const feedGameService = new FeedGameService();

interface IOrganizationGameArgs {
  organizationId: string;
  gameIds: string[];
}

export const GameResolver = {
  Query: {
    getGames: async (): Promise<DocumentType<Game>[]> => {
      const games = await gameService.find({}, null, { lean: true });

      return games;
    },
    findGame: async (_, args: { id: string }): Promise<DocumentType<Game>> => {
      const { id } = args;
      if (!id) {
        throw Failure.BadRequest("Invalid id");
      }

      const game = await gameService.findById(id);

      return game;
    },
    findGameBySlug: async (_, args: { slug: string }): Promise<DocumentType<Game> | null> => {
      const { slug } = args;
      if (!slug) {
        throw Failure.BadRequest("Invalid slug");
      }

      const game = await gameService.findOne({ slug });

      return game;
    }
  },

  Mutation: {
    AddGamesToOrganization: async (
      _,
      args: IOrganizationGameArgs,
      context: IGraphQLContext
    ): Promise<IOrganization> => {
      const response = await organizationGameService.associateGames(context.userId, args.organizationId, args.gameIds);
      return response;
    },

    AddGamesToUser: async (_, args: { gameIds: string[] }, context: IGraphQLContext): Promise<IUser> => {
      const response = await userGameService.associateGames(context.userId, args.gameIds);
      return response;
    }
  },

  Organization: {
    games: async (parent: IOrganization): Promise<DocumentType<Game>[]> => {
      const games = await organizationGameService.getAssociatedGames(parent._id);
      return games;
    }
  },

  ERenaTournament: {
    game: async (parent: IERenaTournament): Promise<IGame | null> => {
      const erenaGameService = new ERenaGameService();

      const game = await erenaGameService.getAssociatedGame(parent._id);
      return game;
    }
  },

  User: {
    games: async (parent: IUser): Promise<DocumentType<Game>[]> => {
      const games = await userGameService.getAssociatedGames(parent._id);
      return games;
    }
  },

  Post: {
    game: async (parent: IFeed): Promise<IGame | null> => {
      const game = await feedGameService.getAssociatedGame(parent._id);
      return game;
    }
  }
};
