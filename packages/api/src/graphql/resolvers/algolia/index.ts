import { AlgoliaResolver } from "./Algolia";

export const AlgoliaResolvers = [AlgoliaResolver.resolve];
