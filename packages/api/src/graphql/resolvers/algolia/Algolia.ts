import { IResolvers } from "graphql-tools";
import { MultipleQueriesQuery, MultipleQueriesResponse } from "@algolia/client-search";

import { AlgoliaService } from "../../../lib/algolia/algolia.service";
import { IOrganization } from "../../../backend/interfaces";

interface ISearchArgs {
  requests: readonly MultipleQueriesQuery[];
}

export class AlgoliaResolver {
  private static $algoliaService = new AlgoliaService();

  private static async search<T>(queries: readonly MultipleQueriesQuery[]): Promise<MultipleQueriesResponse<T>> {
    const results = this.$algoliaService.nativeSearch<T>(queries);

    return results;
  }

  static get resolve(): IResolvers {
    return {
      Query: {
        OrganizationAlgoliaSearch: async (_, args: ISearchArgs): Promise<MultipleQueriesResponse<IOrganization>> => {
          const results = await this.search<IOrganization>(args.requests);

          return results;
        }
      }
    };
  }
}
