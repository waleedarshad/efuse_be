import { IResolvers } from "graphql-tools";
import { Failure } from "@efuse/contracts";
import { PaginateResult, Types } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { OwnerType, InviteResourceType } from "@efuse/entities";

import { IGraphQLContext, IInviteCode, IOrganization } from "../../../backend/interfaces";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { GameService } from "../../../lib/game/game.service";
import { ITeam } from "../../../backend/interfaces/team/team";
import { GameBaseService } from "../../../lib/game/game-base.service";
import { BrazeService } from "../../../lib/braze/braze.service";
import { InviteCodeService } from "../../../lib/invite-code/invite-code.service";
import { OrganizationBaseService } from "../../../lib/organizations/organization-base.service";
import { BrazeCampaignName } from "../../../lib/braze/braze.enum";
import { UserService } from "../../../lib/users/user.service";
import { TeamService, TeamPermissionService, TeamMemberService } from "../../../lib/team";
import { File } from "../../../backend/interfaces/file";
import { Owner } from "../../../backend/types";
import { LeagueService } from "../../../lib/leagues";
import { Game } from "../../../backend/models/game.model";
import { BeAnObject } from "@typegoose/typegoose/lib/types";
import { ILogger, Logger } from "@efuse/logger";

type TeamOwner = Owner | null;
type ObjectId = Types.ObjectId | string;

interface TeamIdArgs {
  teamId: ObjectId;
}

interface GetTeamsByOwnerArgs {
  owner: ObjectId;
}

interface GetPaginatedTeamsArgs {
  page: number;
  limit: number;
  owner: ObjectId;
}

interface CreateTeamArgs {
  team: ITeam;
  userIds?: ObjectId[];
}

interface CreateTeamForLeagueArgs {
  leagueId: ObjectId;
  team: ITeam;
}

interface UpdateTeam {
  description?: string;
  game?: ObjectId;
  image?: File;
  name?: string;
}

interface UpdateTeamArgs {
  teamId: ObjectId;
  teamUpdates: UpdateTeam;
}

export class TeamResolver {
  private static logger: ILogger = Logger.create({ name: "TeamResolver" });
  private static leagueService = new LeagueService();
  private static teamPermissionService: TeamPermissionService = new TeamPermissionService();
  private static teamService: TeamService = new TeamService();
  private static teamMemberService: TeamMemberService = new TeamMemberService();
  private static brazeService: BrazeService = new BrazeService();
  private static gameBaseService: GameBaseService = new GameBaseService();
  private static organizationBaseService: OrganizationBaseService = new OrganizationBaseService();
  private static inviteCodeService: InviteCodeService = new InviteCodeService();

  public static get resolveAll(): IResolvers[] {
    return [this.resolveQueries(), this.resolveMutations(), this.resolveObjects()];
  }

  private static resolveQueries(): IResolvers {
    return {
      Query: {
        getPaginatedTeams: async (_, args: GetPaginatedTeamsArgs): Promise<PaginateResult<ITeam>> => {
          const { owner, page, limit } = args;

          try {
            const foundTeams = await this.teamService.getPaginatedTeams(page, limit, owner);

            return foundTeams;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve teams", error);
          }
        },

        getTeamById: async (_, args: TeamIdArgs): Promise<ITeam | null> => {
          const { teamId } = args;

          try {
            const foundTeam = await this.teamService.findOne({ _id: teamId }, {}, { lean: true });

            return foundTeam;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve team", error);
          }
        },

        getTeamsByOwner: async (_, args: GetTeamsByOwnerArgs): Promise<ITeam[]> => {
          const { owner } = args;

          try {
            const foundTeams = await this.teamService.find({ owner }, {}, { lean: true });

            return foundTeams;
          } catch (error) {
            throw Failure.InternalServerError("Failed to retrieve teams", error);
          }
        }
      }
    };
  }

  private static resolveMutations(): IResolvers {
    return {
      Mutation: {
        createTeam: async (_, args: CreateTeamArgs, context: IGraphQLContext): Promise<ITeam> => {
          const { team, userIds } = args;
          const { owner, ownerType } = team;
          const currentUserId = context?.userId;

          try {
            // will throw error if user does not have permission for actions
            await this.teamPermissionService.userAllowedToPerformAction(currentUserId, owner, ownerType);

            // this is for when we support user teams fully
            if (currentUserId === owner && ownerType !== OwnerType.USERS) {
              throw Failure.ExpectationFailed("Check that you are making a user team", args);
            }

            const savedTeam = await this.teamService.create(team);

            if (userIds && userIds.length > 0 && ownerType === OwnerType.ORGANIZATIONS) {
              await Promise.allSettled(
                userIds.map((userId) => this.teamMemberService.addUserToOrganizationTeam(savedTeam._id, userId, ""))
              );
            }

            return savedTeam;
          } catch (error) {
            throw Failure.InternalServerError("Failed to create team", error);
          }
        },

        createTeamForLeague: async (_, args, context) => this.createTeamForLeague(_, args, context),

        deleteTeam: async (_, args: TeamIdArgs, context: IGraphQLContext): Promise<boolean> => {
          const { teamId } = args;
          const { userId } = context;

          const adjustedOutput = { owner: 1, ownerType: 1 };
          const leanOption = { lean: true };

          try {
            const foundLeanTeam = await this.teamService.findOne({ _id: teamId }, adjustedOutput, leanOption);

            if (!foundLeanTeam) {
              throw Failure.NotFound("Team not found", args);
            }

            const { owner, ownerType } = foundLeanTeam;

            // will throw error if user does not have permission for actions
            await this.teamPermissionService.userAllowedToPerformAction(userId, owner, ownerType);

            // delete members from team before deleting team
            await this.teamMemberService.deleteMembersFromTeam(teamId);

            const deleted = await this.teamService.delete(teamId);

            return deleted;
          } catch (error) {
            throw Failure.InternalServerError("Failed to delete teams", error);
          }
        },

        updateTeam: async (_, args: UpdateTeamArgs, context: IGraphQLContext): Promise<ITeam> => {
          const { teamId, teamUpdates } = args;
          const { userId } = context;

          const adjustedOutput = { owner: 1, ownerType: 1 };
          const leanOption = { lean: true };

          try {
            const foundLeanTeam = await this.teamService.findOne({ _id: teamId }, adjustedOutput, leanOption);

            if (!foundLeanTeam) {
              throw Failure.NotFound("Team not found", args);
            }

            const { owner, ownerType } = foundLeanTeam;

            // will throw error if user does not have permission for actions
            await this.teamPermissionService.userAllowedToPerformAction(userId, owner, ownerType);

            const updatedTeam = await this.teamService.updateOne({ _id: teamId }, teamUpdates, {
              lean: true,
              new: true
            });

            return updatedTeam;
          } catch (error) {
            throw Failure.InternalServerError("Failed to update team", error);
          }
        }
      }
    };
  }

  private static resolveObjects(): IResolvers {
    return {
      Organization: {
        teams: async (parent: IOrganization): Promise<ITeam[]> => {
          console.log(parent);
          const teams = await this.teamService.find({ owner: parent._id }, {}, { lean: true });

          return teams;
        }
      },
      Team: {
        game: async (parent: ITeam): Promise<DocumentType<Game> | null> => {
          const gameService = new GameService();
          const game = await gameService.findOne({ _id: parent.game }, {}, { lean: true });

          return game;
        },

        owner: async (parent: ITeam): Promise<TeamOwner> => {
          const { owner }: { owner: ObjectId } = parent;

          try {
            let foundDoc: TeamOwner;

            switch (parent.ownerType) {
              case OwnerType.ORGANIZATIONS: {
                const organizationService = new OrganizationsService();
                foundDoc = await organizationService.findOne({ _id: owner }, {}, { lean: true });
                break;
              }
              case OwnerType.USERS: {
                const userService = new UserService();
                foundDoc = await userService.findOne({ _id: owner }, {}, { lean: true });
                break;
              }
              default:
                throw Failure.ExpectationFailed(
                  "Could not determine which type of document to retrieve",
                  parent.ownerType
                );
            }

            return foundDoc;
          } catch (error) {
            throw Failure.InternalServerError("Could not retrieve owner data for favorite", error);
          }
        }
      }
    };
  }

  private static async createTeamForLeague(_, args: CreateTeamForLeagueArgs, context: IGraphQLContext): Promise<ITeam> {
    const { team, leagueId } = args;
    const currentUserId = context?.userId;

    // will throw error if user does not have permission for actions
    await this.teamPermissionService.userAllowedToPerformAction(currentUserId, team.owner, team.ownerType);

    const foundLeague = await this.leagueService.findOne({ _id: leagueId }, { game: true, name: true }, { lean: true });

    if (!foundLeague) {
      throw Failure.UnprocessableEntity("Could not find league", {
        leagueId
      });
    }

    team.game = foundLeague.game;

    const savedTeam = await this.teamService.create(team);

    if (savedTeam.ownerType === OwnerType.ORGANIZATIONS) {
      // this currently only supports organizations
      this.sendBrazeCampaignMessageForLeagueTeamCreation(
        currentUserId,
        savedTeam.name,
        foundLeague.name,
        savedTeam.game,
        savedTeam.owner,
        savedTeam._id
      ).catch((error) => this.logger?.error(error));
    }

    await this.leagueService.updateOne(
      { _id: leagueId },
      { $addToSet: { teams: savedTeam._id } },
      { lean: true, new: true }
    );

    return savedTeam;
  }

  /**
   * this currently only supports organizations
   *
   */
  private static async sendBrazeCampaignMessageForLeagueTeamCreation(
    currentUserId: ObjectId,
    teamName: string,
    leagueName: string,
    gameId: ObjectId,
    orgId: ObjectId,
    teamId: ObjectId
  ): Promise<void> {
    const foundGamePromise = this.gameBaseService.findOne({ _id: gameId }, { title: true }, { lean: true });
    const foundOrganizationPromise = this.organizationBaseService.findOne(
      { _id: orgId },
      { name: true },
      { lean: true }
    );
    const foundInviteCodePromise = this.inviteCodeService.findOne(
      {
        resourceType: InviteResourceType.TEAM,
        resource: teamId,
        active: true
      },
      { code: true },
      { lean: true }
    );

    const completedPromises: [DocumentType<Game, BeAnObject> | null, IOrganization | null, IInviteCode | null] =
      await Promise.all([foundGamePromise, foundOrganizationPromise, foundInviteCodePromise]);

    // completedPromises will have [ Game, IOrganization, IInviteCode ] in that order
    // It is based on the order the promises are placed in the array for Promise.all
    const foundGame = completedPromises[0];
    const foundOrg = completedPromises[1];
    const foundInviteCode = completedPromises[2];

    this.brazeService.sendCampaignMessageAsync(BrazeCampaignName.LEAGUE_TEAM_CREATED, {
      trigger_properties: {
        team_name: teamName,
        league_name: leagueName,
        game_title: foundGame?.title,
        organization_name: foundOrg?.name,
        invite_url: `https://efuse.gg/i/${foundInviteCode?.code}`
      },
      recipients: [{ external_user_id: String(currentUserId) }]
    });
  }
}
