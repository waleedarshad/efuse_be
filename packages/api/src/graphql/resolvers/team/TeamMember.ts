import { IResolvers } from "graphql-tools";
import { Failure } from "@efuse/contracts";
import { TeamMemberStatus, OwnerType } from "@efuse/entities";
import { PaginateResult, QueryOptions } from "mongoose";
import { IUser } from "../../../backend/interfaces/user";
import { UserService } from "../../../lib/users/user.service";
import { ITeamMember } from "../../../backend/interfaces/team/team-member";
import { TeamMemberService, TeamService, TeamPermissionService } from "../../../lib/team";
import { ITeam } from "../../../backend/interfaces/team/team";
import { IGraphQLContext } from "../../../backend/interfaces";
import { ObjectId } from "../../../backend/types";

interface AddUsersToTeamArgs {
  teamId: ObjectId;
  userIds: ObjectId[];
  role?: string;
}

interface GetAllTeamMembersArgs {
  teamId: ObjectId;
}

interface GetTeamMembersByStatusArgs {
  teamId: ObjectId;
  status: TeamMemberStatus;
}

interface GetPaginatedTeamMembersArgs {
  teamId: ObjectId;
  status: TeamMemberStatus;
  page: number;
  limit: number;
}

interface UpdateTeamMember {
  status: TeamMemberStatus;
  role: string;
}
interface UpdateTeamMemberStatusArgs {
  teamMemberId: ObjectId;
  teamMemberUpdate: UpdateTeamMember;
}

interface TeamMemberIdArgs {
  teamMemberId: ObjectId;
}

interface GetTeamMemberArgs {
  userId: ObjectId;
  teamId: ObjectId;
}

export class TeamMemberResolver {
  private static teamPermissionService = new TeamPermissionService();
  private static teamMemberService = new TeamMemberService();
  private static teamService = new TeamService();

  public static get resolve(): IResolvers {
    return {
      Mutation: {
        addUsersToTeam: async (_, args, context) => this.addUsersToTeam(_, args, context),
        deleteTeamMember: async (_, args, context) => this.deleteTeamMember(_, args, context),
        updateTeamMember: async (_, args, context) => this.updateTeamMember(_, args, context)
      },
      Query: {
        getAllTeamMembers: async (_, args) => this.getAllTeamMembers(_, args),
        getPaginatedTeamMembers: async (_, args) => this.getPaginatedTeamMembers(_, args),
        getTeamMember: async (_, args) => this.getTeamMember(_, args),
        getTeamMemberById: async (_, args) => this.getTeamMemberById(_, args),
        getTeamMembersByStatus: async (_, args) => this.getTeamMembersByStatus(_, args)
      },
      TeamMember: {
        team: async (parent: ITeamMember) => this.resolveTeam(parent),
        user: async (parent: ITeamMember) => this.resolveUser(parent)
      },
      Team: {
        members: async (parent: ITeam) => this.resolveMembers(parent)
      }
    };
  }

  private static addUserToTeam(
    ownerType: OwnerType,
    userId: ObjectId,
    teamId: ObjectId,
    role: string
  ): Promise<ITeamMember> {
    switch (ownerType) {
      case OwnerType.ORGANIZATIONS:
        return this.teamMemberService.addUserToOrganizationTeam(teamId, userId, role);
      default:
        throw Failure.NotImplemented("User Teams currently not implemented");
    }
  }

  private static async addUsersToTeam(_, args: AddUsersToTeamArgs, context: IGraphQLContext): Promise<ITeamMember[]> {
    const { teamId, userIds, role } = args;
    const currentUserId = context.userId;

    const foundLeanTeam = await this.teamService.findOne({ _id: teamId }, { owner: 1, ownerType: 1 }, { lean: true });

    if (!foundLeanTeam) {
      throw Failure.NotFound("Team does not exist", args);
    }

    // will throw error if user does not have permission for actions
    await this.teamPermissionService.userAllowedToPerformAction(
      currentUserId,
      foundLeanTeam.owner,
      foundLeanTeam.ownerType
    );

    // we should try to only add a role to the members if there is one member
    // if we try with 100 of them then they'll all have the same role
    const adjustedRole = userIds && userIds.length === 1 && role ? role : "";

    const finalizedTeamMembers = await this.checkIfUsersAreMembersOrAddToTeam(
      userIds,
      teamId,
      foundLeanTeam,
      adjustedRole
    );

    return finalizedTeamMembers;
  }

  private static async checkIfUsersAreMembersOrAddToTeam(
    userIds: ObjectId[],
    teamId: ObjectId,
    team: ITeam,
    adjustedRole: string
  ): Promise<ITeamMember[]> {
    const addedTeamMembers = await Promise.allSettled(
      userIds.map(async (userId: ObjectId) => {
        return this.addUserToTeam(team.ownerType, userId, teamId, adjustedRole);
      })
    );

    const fullfilledTeamMembers = addedTeamMembers.filter((member) => member.status === "fulfilled");

    return fullfilledTeamMembers.map((member) => (<PromiseFulfilledResult<ITeamMember>>member).value);
  }

  private static async deleteTeamMember(
    _,
    args: TeamMemberIdArgs,
    context: IGraphQLContext
  ): Promise<ITeamMember | null> {
    const { teamMemberId } = args;
    const { userId } = context;
    const leanOption: QueryOptions = { lean: true };

    const adjustedLeanTeam = await this.teamService.getTeamByTeamMember(
      teamMemberId,
      { owner: 1, ownerType: 1 },
      leanOption
    );

    if (!adjustedLeanTeam) {
      throw Failure.NotFound("Team for team member not found", args);
    }

    await this.teamPermissionService.userAllowedToPerformAction(
      userId,
      adjustedLeanTeam.owner,
      adjustedLeanTeam.ownerType
    );

    const foundMember = await this.teamMemberService.findOne({ _id: teamMemberId }, {}, leanOption);
    await this.teamMemberService.delete(teamMemberId);

    return foundMember;
  }

  private static async getAllTeamMembers(_, args: GetAllTeamMembersArgs): Promise<ITeamMember[]> {
    const { teamId } = args;

    const foundTeamMembers = await this.teamMemberService.find({ team: teamId }, {}, { lean: true });

    return foundTeamMembers;
  }

  private static async getPaginatedTeamMembers(
    _,
    args: GetPaginatedTeamMembersArgs
  ): Promise<PaginateResult<ITeamMember>> {
    const { teamId, status, page, limit } = args;

    const foundTeamMember = await this.teamMemberService.getPaginatedTeamMembers(teamId, status, page, limit);

    return foundTeamMember;
  }

  private static async getTeamMember(_, args: GetTeamMemberArgs): Promise<ITeamMember | null> {
    const { teamId, userId } = args;

    const foundTeamMember = await this.teamMemberService.findOne({ user: userId, team: teamId }, {}, { lean: true });

    return foundTeamMember;
  }

  private static async getTeamMemberById(_, args: TeamMemberIdArgs): Promise<ITeamMember | null> {
    const { teamMemberId } = args;

    const foundTeamMember = await this.teamMemberService.findOne({ _id: teamMemberId }, {}, { lean: true });

    return foundTeamMember;
  }

  private static async getTeamMembersByStatus(_, args: GetTeamMembersByStatusArgs): Promise<ITeamMember[]> {
    const { teamId, status } = args;

    const foundTeamMembers = await this.teamMemberService.find({ team: teamId, status }, {}, { lean: true });

    return foundTeamMembers;
  }

  private static async resolveMembers(parent: ITeam): Promise<ITeamMember[]> {
    const teamMembers = await this.teamMemberService.find({ team: parent._id }, {}, { lean: true });

    return teamMembers;
  }

  private static async resolveTeam(parent: ITeamMember): Promise<ITeam | null> {
    const teamService = new TeamService();
    const team = await teamService.findOne({ _id: parent.team }, {}, { lean: true });

    return team;
  }

  private static async resolveUser(parent: ITeamMember): Promise<IUser | null> {
    const userService = new UserService();

    const user = await userService.findOne({ _id: parent.user }, {}, { lean: true });

    return user;
  }

  private static async updateTeamMember(
    _,
    args: UpdateTeamMemberStatusArgs,
    context: IGraphQLContext
  ): Promise<ITeamMember> {
    const { teamMemberId, teamMemberUpdate } = args;
    const { userId } = context;
    const leanOption: QueryOptions = { lean: true };

    const team = await this.teamService.getTeamByTeamMember(teamMemberId, { owner: 1, ownerType: 1 }, leanOption);

    if (!team) {
      throw Failure.NotFound("Team for team member not found", args);
    }

    await this.teamPermissionService.userAllowedToPerformAction(userId, team.owner, team.ownerType);

    const updatedTeamMember = await this.teamMemberService.updateOne({ _id: teamMemberId }, teamMemberUpdate, {
      lean: true,
      new: true
    });

    return updatedTeamMember;
  }
}
