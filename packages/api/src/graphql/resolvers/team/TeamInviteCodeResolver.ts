import { InviteCodeType } from "@efuse/entities";
import { IResolvers } from "graphql-tools";
import { IGraphQLContext, ITeam } from "../../../backend/interfaces";
import { InviteCodeService } from "../../../lib/invite-code/invite-code.service";

export class TeamInviteCodeResolver {
  private static inviteCodeService = new InviteCodeService();

  public static get resolve(): IResolvers {
    return {
      Team: {
        inviteCode: (parent: ITeam, _, context: IGraphQLContext) => this.resolveTeamInviteCode(parent, _, context)
      }
    };
  }

  private static async resolveTeamInviteCode(parent: ITeam, _, context: IGraphQLContext): Promise<string> {
    const inviteCode = await this.inviteCodeService.findOne(
      { resource: parent._id, type: InviteCodeType.TEAM, active: true },
      {},
      { lean: true }
    );

    return <string>inviteCode?.code;
  }
}
