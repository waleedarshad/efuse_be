import { ITokenActionValues } from "../../../backend/interfaces/token-ledger";
import { TokenLedgerService } from "../../../lib/token-ledger/token-ledger.service";

const tokenLedgerService = new TokenLedgerService();

export const TokenActionValuesResolver = {
  Query: {
    getTokenActionValues: async (): Promise<ITokenActionValues> => {
      const tokenActionValues = await tokenLedgerService.getTokenActionValues();

      return tokenActionValues;
    }
  }
};
