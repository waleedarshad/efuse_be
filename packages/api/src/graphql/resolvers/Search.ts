import { SearchHelper } from "../../lib/search/search.helper";
import { IUser } from "../../backend/interfaces/user";
import { SearchService } from "../../lib/search/search.service";
import { IOrganization, IOpportunity, ILearningArticle } from "../../backend/interfaces";
import { IGraphQLContext } from "../../backend/interfaces/graphql";

interface ISearchArgs {
  params: { term: string; limit: number };
}

enum EntityTypeEnum {
  USER = "users",
  ORGANIZATION = "organizations",
  OPPORTUNITY = "opportunities",
  LEARNING_ARTICLE = "learningArticles"
}

const executeSearch = async (args: ISearchArgs, type: string, context: IGraphQLContext): Promise<any[]> => {
  const searchHelper = new SearchHelper();
  const searchService = new SearchService();

  const searchParams = args.params;
  const { limit } = searchParams;

  if (!searchParams.term) {
    return [];
  }

  const terms: string[] = searchHelper.sanitizeToArray(searchParams.term);

  if (terms.length === 0) {
    return [];
  }

  let sortedEntities: any[] = [];

  switch (type) {
    case EntityTypeEnum.USER: {
      const users = await searchService.searchUsersGraphQL(terms, limit, context?.userId);
      sortedEntities = searchHelper.sortEntities(users, limit);
      break;
    }
    case EntityTypeEnum.ORGANIZATION: {
      const organizations = await searchService.searchOrganizationsGraphQL(terms, limit);
      sortedEntities = searchHelper.sortEntitiesWhenVerifiedIsNested(organizations, limit);
      break;
    }
    case EntityTypeEnum.OPPORTUNITY: {
      sortedEntities = await searchService.searchOpportunitiesGraphQL(terms, limit);
      break;
    }
    case EntityTypeEnum.LEARNING_ARTICLE: {
      sortedEntities = await searchService.searchLearningArticlesGraphQL(
        terms.join(" "),
        limit,
        context?.userId ? String(context.userId) : undefined
      );
      break;
    }
    default:
      sortedEntities = [];
      break;
  }

  return <never>sortedEntities;
};

export const SearchResolver = {
  Query: {
    UserSearch: async (_, args: ISearchArgs, context: IGraphQLContext): Promise<IUser[]> => {
      const users: IUser[] = (await executeSearch(args, EntityTypeEnum.USER, context)) as IUser[];
      return users;
    },
    OrganizationSearch: async (_, args: ISearchArgs, context: IGraphQLContext): Promise<IOrganization[]> => {
      const organizations: IOrganization[] = (await executeSearch(
        args,
        EntityTypeEnum.ORGANIZATION,
        context
      )) as IOrganization[];
      return organizations;
    },
    OpportunitySearch: async (_, args: ISearchArgs, context: IGraphQLContext): Promise<IOpportunity[]> => {
      const opportunities: IOpportunity[] = (await executeSearch(
        args,
        EntityTypeEnum.OPPORTUNITY,
        context
      )) as IOpportunity[];
      return opportunities;
    },
    LearningArticleSearch: async (_, args: ISearchArgs, context: IGraphQLContext): Promise<ILearningArticle[]> => {
      const learningArticles: ILearningArticle[] = (await executeSearch(
        args,
        EntityTypeEnum.LEARNING_ARTICLE,
        context
      )) as ILearningArticle[];
      return learningArticles;
    }
  }
};
