import { Failure } from "@efuse/contracts";

import { IGraphQLContext } from "../../backend/interfaces";
import { IMedia } from "../../backend/interfaces/media";
import { MediaService } from "../../lib/media/media.service";

const mediaService = new MediaService();

export const MediaResolver = {
  Mutation: {
    DeleteMedia: async (_, args: { mediaId: string }, context: IGraphQLContext): Promise<IMedia> => {
      // Make sure mediaId is passed
      if (!args.mediaId) {
        throw Failure.BadRequest("mediaId is required");
      }

      // Make sure it is valid objectId
      if (!mediaService.isValidObjectId(args.mediaId)) {
        throw Failure.BadRequest("Invalid mediaId");
      }

      // Get the media object
      const media = await mediaService.findOne({ _id: args.mediaId }, {}, { lean: true });

      // Make sure media exists
      if (!media) {
        throw Failure.UnprocessableEntity("Media not found");
      }

      // Make sure user is owner of media
      if (String(media.user) !== String(context.userId)) {
        throw Failure.Forbidden("You are not allowed to perform this action");
      }

      const query = { mediaable: media.mediaable, mediaableType: media.mediaableType, user: context.userId };

      // Find all the transcoded medias
      const mediaObjects = await mediaService.find(query, {}, { lean: true });

      // Build S3 fileKeys
      const fileKeys = mediaService.buildFileKeys(mediaObjects);

      // Delete all associated media
      await mediaService.delete(query, fileKeys);

      return media;
    }
  }
};
