import { Failure } from "@efuse/contracts";
import { IMyntAsset } from "../../../backend/interfaces/mynt";
import { MyntAssetService } from "../../../lib/mynt/mynt-asset.service";

const myntAssetsService = new MyntAssetService();

export const MyntAssetResolver = {
  Query: {
    getMyntAssets: async (): Promise<IMyntAsset[]> => {
      const myntAssets = await myntAssetsService.find({});

      return myntAssets;
    },
    findMyntAsset: async (_, args): Promise<IMyntAsset | null> => {
      const { tokenId }: { tokenId: string } = args;
      if (!tokenId) {
        throw Failure.BadRequest("Missing id parameter");
      }
      const query = { tokenId };

      const myntAsset = await myntAssetsService.findOne(query);

      return myntAsset;
    }
  }
};
