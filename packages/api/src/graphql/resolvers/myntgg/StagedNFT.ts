import { Failure } from "@efuse/contracts";
import { Context } from "../../schema/context";
import { StagedNFTService } from "../../../lib/mynt/staged-nft.service";
import { IStagedNFT } from "../../../backend/interfaces/mynt";
import { StagedNFTTransactionService } from "../../../lib/mynt/staged-nft-transaction.service";

const stagedNFTService = new StagedNFTService();
const stagedNFTTransactionService = new StagedNFTTransactionService();

export const StagedNFTResolver = {
  Query: {
    getStagedNFTs: async (): Promise<IStagedNFT[]> => {
      const nftAssets = await stagedNFTService.find({});

      return nftAssets;
    },
    findStagedNFT: async (_, args): Promise<IStagedNFT | null> => {
      const { _id }: { _id: string } = args;

      if (!_id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const query = { _id };

      const nftAsset = await stagedNFTService.findOne(query);

      return nftAsset;
    },
    getNFTCollection: async (_, args, context: Context): Promise<IStagedNFT[]> => {
      const eFuseUserId = context.user._id.toHexString();
      return stagedNFTService.find({ eFuseUserId });
    }
  },
  Mutation: {
    async createNFT(_, args: any, context: Context): Promise<IStagedNFT> {
      const nftArgs = <IStagedNFT>args.args;
      const traits = { ...nftArgs.traits, owner: context.user.username, creator: context.user.username };

      const newNFT = <IStagedNFT>{
        assetFile: nftArgs.assetFile,
        name: nftArgs.name,
        externalLink: nftArgs.externalLink,
        description: nftArgs.description,
        traits,
        unlockableContent: nftArgs.unlockableContent,
        eFuseUserId: context.user._id.toHexString(),
        sellPrice: "0.00",
        isListed: false
      };

      const createdAsset = await stagedNFTService.create(newNFT);
      return createdAsset;
    },
    updateNFTAfterTransaction: async (
      _,
      args: { nftId: string; transactionId: string },
      context: Context
    ): Promise<IStagedNFT | null> => {
      const newOwnerId = context.user._id;

      if (!args.nftId) {
        throw Failure.BadRequest("Missing nftId parameter");
      }

      if (!args.transactionId) {
        throw Failure.BadRequest("Missing transaction id");
      }

      const nftTransaction = await stagedNFTTransactionService.findOne({ _id: args.transactionId });

      if (!nftTransaction) {
        throw Failure.BadRequest("No transaction available");
      }

      const nft = await stagedNFTService.findOneAndUpdate(
        { _id: args.nftId },
        {
          $set: { sellPrice: "0.00", eFuseUserId: newOwnerId, isListed: false },
          $push: { transactionHistory: nftTransaction }
        }
      );

      return nft;
    },
    listNFTForSale: async (_, args: { nftId: string; sellPrice: string }): Promise<IStagedNFT | null> => {
      if (!args.nftId) {
        throw Failure.BadRequest("Missing nftId parameter");
      }

      if (!args.sellPrice) {
        throw Failure.BadRequest("Missing sellPrice parameter");
      }

      const updatedNFT = await stagedNFTService.findOneAndUpdate(
        { _id: args.nftId },
        {
          $set: { sellPrice: args.sellPrice, isListed: true }
        }
      );

      if (!updatedNFT) {
        throw Failure.BadRequest("No NFT available to update");
      }

      return updatedNFT;
    }
  }
};
