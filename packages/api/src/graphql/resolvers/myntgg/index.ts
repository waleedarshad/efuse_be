import { MyntAssetResolver } from "./MyntAsset";
import { StagedNFTResolver } from "./StagedNFT";
import { StagedNFTTransactionResolver } from "./StagedNFTTransaction";

export const MyntResolvers = [MyntAssetResolver, StagedNFTResolver, StagedNFTTransactionResolver];
