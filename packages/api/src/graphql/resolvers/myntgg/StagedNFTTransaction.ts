import { Failure } from "@efuse/contracts";
import { Context } from "../../schema/context";
import { IStagedNFTTransaction } from "../../../backend/interfaces/mynt/staged/staged-nft-transaction";
import { StagedNFTTransactionService } from "../../../lib/mynt/staged-nft-transaction.service";
import { StagedNFTService } from "../../../lib/mynt/staged-nft.service";

const stagedNFTTransactionService = new StagedNFTTransactionService();
const stagedNFTService = new StagedNFTService();

export interface INFTTransactionArgs {
  orderId: string;
  payerId: string;
  purchaseAmount: string;
  facilitatorAccessToken: string;
  nftId: string;
  sellerId: string;
}

export const StagedNFTTransactionResolver = {
  Query: {
    getStagedNFTTransactions: async (): Promise<IStagedNFTTransaction[]> => {
      const nftTransactions = await stagedNFTTransactionService.find({});

      return nftTransactions;
    },
    findStagedNFTTransaction: async (_, args): Promise<IStagedNFTTransaction | null> => {
      const { _id }: { _id: string } = args;

      if (!_id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      const query = { _id };

      const nftTransaction = await stagedNFTTransactionService.findOne(query);

      return nftTransaction;
    }
  },
  Mutation: {
    async createNFTTransaction(
      _,
      args: { args: INFTTransactionArgs },
      context: Context
    ): Promise<IStagedNFTTransaction> {
      const nftTransactionArgs: INFTTransactionArgs = args.args;

      const newNFTTransaction = <IStagedNFTTransaction>{
        purchaserId: context.user._id.toHexString(),
        orderId: nftTransactionArgs.orderId,
        payerId: nftTransactionArgs.payerId,
        purchasePrice: nftTransactionArgs.purchaseAmount,
        nftId: nftTransactionArgs.nftId,
        sellerId: nftTransactionArgs.sellerId
      };

      const createdNFTTransaction = await stagedNFTTransactionService.create(newNFTTransaction);

      const updatedNFT = await stagedNFTService.findOneAndUpdate(
        { _id: newNFTTransaction.nftId },
        {
          $set: { sellPrice: "0.00", eFuseUserId: newNFTTransaction.purchaserId, isListed: false },
          $push: { transactionHistory: createdNFTTransaction }
        }
      );

      if (!updatedNFT) {
        throw Failure.BadRequest("Unable to update staged nft");
      }

      return createdNFTTransaction;
    }
  }
};
