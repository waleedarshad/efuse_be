import { IPartnerApplicant, IPartnerApplyResponse } from "../../backend/interfaces";
import { IGraphQLContext } from "../../backend/interfaces/graphql";
import { IUser } from "../../backend/interfaces/user";
import { PartnerApplicantService } from "../../lib/partner-applicant/partner-applicant.service";
import { UserService } from "../../lib/users/user.service";

const partnerApplicantService = new PartnerApplicantService();

export const PartnerApplicantResolver = {
  Query: {
    CanApplyForPartnerBadge: async (_, __, context: IGraphQLContext): Promise<IPartnerApplyResponse> => {
      const ableToApply = await partnerApplicantService.canApply(context.userId);
      return ableToApply;
    }
  },

  Mutation: {
    ApplyForPartnerBadge: async (_, __, context: IGraphQLContext): Promise<IPartnerApplicant> => {
      const partnerApplicant = await partnerApplicantService.apply(context.userId);
      return partnerApplicant;
    }
  },

  PartnerApplicant: {
    user: async (parent: IPartnerApplicant): Promise<IUser | null> => {
      const userService = new UserService();
      const user = await userService.findOne({ _id: parent.user });
      return user;
    }
  }
};
