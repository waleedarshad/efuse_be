import { defaultFieldResolver, GraphQLField, GraphQLObjectType } from "graphql";
import { Failure } from "@efuse/contracts";
import { SchemaDirectiveVisitor } from "graphql-tools";
import { IUser } from "../../backend/interfaces/user";
import { UserRoles } from "../enums";

export class AuthDirective extends SchemaDirectiveVisitor {
  /**
   * This method is for types ("Query", "Mutation", or "User")
   */
  public visitObject(type: GraphQLObjectType): GraphQLObjectType | void | null {
    this.applyAuthCheckToFields(type);

    // indicate that this object requires auth for the resolver defined below
    type["_requiredAuthRole"] = this.args["requires"];
  }

  /**
   * This method is for nested types such individual queries ("CanApplyForPartnerBadge"),
   * mutators("ApplyForPartnerBadge") or type props ("username" on user)
   */
  public visitFieldDefinition(
    field: GraphQLField<any, any>,
    details: {
      objectType: GraphQLObjectType;
    }
  ): GraphQLField<any, any> | void | null {
    this.applyAuthCheckToFields(details.objectType);
    // indicate that this field requires auth for the resolver defined below
    field["_requiredAuthRole"] = this.args["requires"];
  }

  /**
   * This method monkey patches the "resolve" method for fields/objects.
   * If the field has the "_requiredAuthRol property set to anything other than "NONE"
   * it verifies a user is logged in by checking the context for the authed user.
   */
  private applyAuthCheckToFields(objectType: GraphQLObjectType): void {
    // Mark the GraphQLObjectType object to avoid re-wrapping:
    if (objectType["_authFieldsWrapped"]) return;
    objectType["_authFieldsWrapped"] = true;

    const fields = objectType.getFields();

    // wrap all the fields
    Object.keys(fields).forEach((fieldName) => {
      const field = fields[fieldName];
      const { resolve = defaultFieldResolver } = field;

      field.resolve = async function (...args) {
        const context = args[2]; // 2 is always the index of context within the args array

        // check if the field requires auth, fallback to object if field doesn't have a role
        const requiredRole = field["_requiredAuthRole"] || objectType["_requiredAuthRole"];

        // this specific object/field does not require auth at all
        if (requiredRole === UserRoles.NONE) {
          return resolve.apply(this, args);
        }

        if (requiredRole === UserRoles.USER) {
          // at this point we can assume at least the user role is required
          // get the user from context (set during initialization of apollo in app.js)
          const { user }: { user: IUser } = context;
          if (!user) {
            throw Failure.Unauthorized("User is not authorized.");
          }
        }

        return resolve.apply(this, args);
      };
    });
  }
}
