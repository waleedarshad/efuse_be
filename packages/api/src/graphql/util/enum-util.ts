export class EnumUtil {
  /**
   * When you pass in an enum this will convert it to a string with the enum values seperated by space
   *
   * Useful for GraphQL Typedefs and you want to have an enum in it that is already in use.
   */
  public static convertEnumValuesToString<T>(enumValue: T): string {
    const objectValues = Object.values(enumValue);

    let objectValuesAsString = ``;
    objectValues.forEach((value) => {
      objectValuesAsString += `${value} `;
    });

    return objectValuesAsString;
  }

  /**
   * Used to make unions for GraphQL
   *
   * Examples:
   *    Owner = User | Organization
   */
  public static convertEnumToUnionString<T>(enumValue: T): string {
    const objectValues = Object.values(enumValue);

    let objectValuesAsUnionString = ``;
    objectValues.forEach((value) => {
      objectValuesAsUnionString += ` ${value} |`;
    });
    objectValuesAsUnionString = objectValuesAsUnionString.substring(0, objectValuesAsUnionString.length - 1);

    return objectValuesAsUnionString;
  }
}
