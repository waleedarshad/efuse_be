import { gql } from "apollo-server-express";

export const GamePlatformTypeDef = gql`
  extend type User {
    gamePlatforms: [GamePlatform]
  }
`;
