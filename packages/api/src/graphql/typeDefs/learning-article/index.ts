import { LearningArticleModelTypeDef } from "./LearningArticle";
import { LearningArticleCategoryTypeDef } from "./LearningArticleCategory";

export const LearningArticleTypeDefs = [LearningArticleModelTypeDef, LearningArticleCategoryTypeDef];
