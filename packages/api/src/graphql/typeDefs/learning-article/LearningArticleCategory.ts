import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const LearningArticleCategoryTypeDef = gql`
  extend type Query {
    getCategories: [LearningArticleCategory] @auth(requires: "${UserRoles.NONE}")
  }

  type LearningArticleCategory {
    _id: ID
    name: String
    isActive: Boolean
    image: File
    slug: String
  }
`;
