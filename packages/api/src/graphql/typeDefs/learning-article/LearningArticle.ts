import { gql } from "apollo-server-express";

export const LearningArticleModelTypeDef = gql`
  union Author = User | Organization

  type LearningArticle {
    _id: ID!
    slug: String
    title: String
    body: String
    status: String
    author: Author
    authorType: String
    category: LearningArticleCategory
    summary: String
    image: File
    video: File
    isActive: Boolean
    promoted: Boolean
    traits: [String]
    motivations: [String]
    publishDate: Date
    createdAt: Date
    updatedAt: Date
    url: String
    associatedGames: [Game]
  }
`;
