import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const MyntAssetTypeDef = gql`
  extend type Query {
    getMyntAssets: [MyntAsset] @auth(requires: "${UserRoles.NONE}")
    findMyntAsset(tokenId: String): MyntAsset @auth(requires: "${UserRoles.NONE}")
  }

  type MyntAsset {
    _id: String
    thirdPartyId: String
    tokenId: String
    tokenAddress: String
    schemaName: WyvernSchemaName
    version: TokenStandardVersion
    name: String
    decimals: Int
    assetContract: IMyntAssetContract
    assetCollection: IExternalCollection
    description: String
    owner: IExternalAccount
    isPresale: Boolean
    imageUrl: String
    imagePreviewUrl: String
    imageUrlOriginal: String
    imageUrlThumbnail: String
    animationUrl: String
    animationOriginalUrl: String
    openSeaLink: String
    externalLink: String
    traits: IMyntTraits
    numSales: Int
    lastSale: IMyntAssetEvent
    backgroundColor: String
    transferFee: String
    creator: IExternalAccount
  }

  type IMyntTraits {
    rarity: String
    game: String
    owner: String
    mediaType: String
  }

  type IMyntAssetEvent {
    eventType: AssetEventType
    eventTimestamp: Date
    auctionType: AuctionType
    totalPrice: String
    transaction: IMyntTransaction
    paymentToken: JSON
  }

  type IMyntTransaction {
    fromAccount: IExternalAccount
    toAccount: IExternalAccount
    createdDate: Date
    modifiedDate: Date
    transactionHash: String
    transactionIndex: String
    blockNumber: String
    blockHash: String
    timestamp: Date
  }

  type IExternalAccount {
    address: String
    config: String
    profileImgUrl: String
    user: IMyntUser
  }

  type IMyntUser {
    username: String
  }

  type IMyntAssetContract {
    name: String
    address: String
    type: AssetContractType
    schemaName: WyvernSchemaName
    sellerFeeBasisPoints: Int
    buyerFeeBasisPoints: Int
    description: String
    tokenSymbol: String
    imageUrl: String
    stats: JSON
    traitStats: [JSON]
    externalLink: String
    wikiLink: String
  }

  type IExternalCollection {
    name: String
    slug: String
    editors: [String]
    hidden: Boolean
    featured: Boolean
    createdDate: Date
    description: String
    imageUrl: String
    largeImageUrl: String
    featuredImageUrl: String
    stats: JSON
    displayData: JSON
    payoutAddress: String
    traitStats: JSON
    externalLink: String
    wikiLink: String
  }

  enum AssetContractType {
    Fungible
    SemiFungible
    NonFungible
    Unknown
  }

  enum WyvernSchemaName {
    ERC20
    ERC721
    ERC1155
    LegacyEnjin
    ENSShortNameAuction
  }

  enum TokenStandardVersion {
    Unsupported
    Locked
    Enjin
    ERC721v1
    ERC721v2
    ERC721v3
  }

  enum AssetEventType {
    AuctionCreated
    AuctionSuccessful
    AuctionCancelled
    OfferEntered
    BidEntered
    BidWithdraw
    AssetTransfer
    AssetApprove
    CompositionCreated
    Custom
    Payout
  }

  enum AuctionType {
    Dutch
    English
    MinPrice
  }
`;
