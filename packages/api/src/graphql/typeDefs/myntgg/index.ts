import { MyntAssetTypeDef } from "./MyntAsset";
import { StagedNFTTypeDef } from "./StagedNFT";
import { StagedNFTTransactionTypeDef } from "./StagedNFTTransaction";

export const MyntTypeDefs = [MyntAssetTypeDef, StagedNFTTypeDef, StagedNFTTransactionTypeDef];
