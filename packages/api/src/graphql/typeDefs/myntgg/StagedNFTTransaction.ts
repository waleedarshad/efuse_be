import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const StagedNFTTransactionTypeDef = gql`
  extend type Query {
    getStagedNFTTransactions: [StagedNFTTransaction] @auth(requires: "${UserRoles.NONE}")
    findStagedNFTTransaction(_id: String): StagedNFTTransaction @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createNFTTransaction(args: NFTTransactionArgs!): StagedNFTTransaction @auth(requires: "${UserRoles.USER}")
  }

  type StagedNFTTransaction {
    _id: ID
    purchaserId: String
    sellerId: String
    nftId: String
    orderId: String
    payerId: String
    purchasePrice: String
  }

  input NFTTransactionArgs {
    orderId: String
    payerId: String
    purchaseAmount: String
    nftId: String
    sellerId: String
  }
`;
