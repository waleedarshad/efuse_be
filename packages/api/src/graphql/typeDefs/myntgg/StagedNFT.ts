import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const StagedNFTTypeDef = gql`
  extend type Query {
    getStagedNFTs: [StagedNFT] @auth(requires: "${UserRoles.NONE}")
    findStagedNFT(_id: String): StagedNFT @auth(requires: "${UserRoles.NONE}")
    getNFTCollection: [StagedNFT] @auth(requires: "${UserRoles.USER}")
  }

  extend type Mutation {
    createNFT(args: NFTArgs!): StagedNFT @auth(requires: "${UserRoles.USER}")
    updateNFTAfterTransaction(nftId: String!, transactionId: String!): StagedNFT @auth(requires: "${UserRoles.USER}")
    listNFTForSale(nftId: String!, sellPrice: String!): StagedNFT @auth(requires: "${UserRoles.USER}")
  }

  type StagedNFT {
    _id: ID
    assetFile: String
    name: String
    externalLink: String
    description: String
    traits: NFTTraits
    unlockableContent: String
    eFuseUserId: String
    isListed: Boolean
    sellPrice: String
    transactionHistory: [StagedNFTTransaction]
  }

  type NFTTraits {
    rarity: String
    game: String
    owner: String
    creator: String
    mediaType: String
  }

  input NFTArgs {
    assetFile: String
    name: String
    externalLink: String
    description: String
    traits: NFTTraitsArgs
    unlockableContent: String
  }

  input NFTTraitsArgs {
    rarity: String
    game: String
    owner: String
    mediaType: String
  }
`;
