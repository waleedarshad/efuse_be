import { gql } from "apollo-server-express";

import { UserRoles } from "../enums";

export const ValidationsTypeDef = gql`
  extend type Query {
    ValidateSlug(slug: String!, slugType: String!): ValidationResponse @auth(requires: "${UserRoles.USER}")
  }

  type ValidationResponse {
    valid: Boolean
    error: String
  }
`;
