import { gql } from "apollo-server-express";

import { UserRoles } from "../enums";

export const MediaTypeDef = gql`
  """
  A type that describes a Media that is attached to any object
  """
  type Media {
    "The database id for the given media object"
    _id: String

    "The file that is associated to the given Media"
    file: File

    "The quality profile of the given choice of media"
    profile: String
  }

  extend type Mutation {
    DeleteMedia(mediaId: String!): Media @auth(requires: "${UserRoles.USER}")
  }
`;
