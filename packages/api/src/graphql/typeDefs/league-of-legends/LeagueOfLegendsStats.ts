import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const LeagueOfLegendsStatsTypeDef = gql`
  extend type Query {
    getLeagueOfLegendsStats(id: ID!): LeagueOfLegendsStats @auth(requires: "${UserRoles.NONE}")
  }

  extend type User {
    leagueOfLegendsStats: LeagueOfLegendsStats @auth(requires: "${UserRoles.NONE}")
  }

  type ChampionMastery {
    championPointsUntilNextLevel: Float
    chestGranted: Boolean
    championId: Float
    lastPlayTime: Float
    championLevel: Float
    summonerId: String
    championPoints: Float
    championPointsSinceLastLevel: Float
    tokensEarned: Int
    iconImage: String
    splashImage: String
    loadingImage: String
    championName: String
  }

  type MiniSeries {
    losses: Int
    progress: Float
    target: Float
    wins: Int
  }

  type LeagueInfo {
    id: ID!
    leagueId: String
    summonerId: String
    queueType: String
    tier: String
    tierImage: String
    rank: String
    summonerName: String
    leaguePoints: Int
    wins: Int
    losses: Int
    veteran: Boolean
    inactive: Boolean
    hotStreak: Boolean
    freshBlood: Boolean
    miniSeries: [MiniSeries]
  }

  type LeagueOfLegendsStats {
    id: ID!
    createdAt: Date
    leagueInfo: [LeagueInfo]
    championInfo: [ChampionMastery]
    leaderboardRankTierScore: Int
    leaderboardRankTier: String
  }
`;
