import { gql } from "apollo-server-express";

export const ClippyTypeDef = gql`
  extend type Clippy {
    user: User
  }
`;
