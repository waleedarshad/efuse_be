import { CommentTypeDef } from "./Comment";
import { GiphyTypeDef } from "./Giphy";
import { MentionTypeDef } from "./Mention";
import { VerifiedFeedTypeDef } from "./VerifiedFeed";
import { FollowingFeedTypeDef } from "./FollowingFeed";
import { OrganizationFeedTypeDef } from "./OrganizationFeed";
import { UserFeedTypeDef } from "./UserFeed";
import { OpenGraphTypeDef } from "./OpenGraph";

export const FeedTypeDefs = [
  MentionTypeDef,
  GiphyTypeDef,
  CommentTypeDef,
  VerifiedFeedTypeDef,
  FollowingFeedTypeDef,
  OrganizationFeedTypeDef,
  UserFeedTypeDef,
  OpenGraphTypeDef
];
