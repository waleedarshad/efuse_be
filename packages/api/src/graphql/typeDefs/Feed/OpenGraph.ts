import { gql } from "apollo-server-express";

export const OpenGraphTypeDef = gql`
  type OpenGraph {
    shortName: String
    url: String
    opengraph: OpenGraphDetail
    createdAt: Date
    updatedAt: Date
  }

  type OpenGraphDetail {
    title: String
    description: String
    domain: String
    img: String
  }
`;
