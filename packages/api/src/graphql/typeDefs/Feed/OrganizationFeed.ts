import { gql } from "apollo-server-express";

import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

export const OrganizationFeedTypeDef = gql`
  extend type Query {
    OrganizationFeed(orgId: String, page: Int, limit: Int): OrganizationFeed @auth(requires: "${UserRoles.USER}")
  }

  type OrganizationFeed {
    docs: [Post]
    ${PaginationFields}
  }
`;
