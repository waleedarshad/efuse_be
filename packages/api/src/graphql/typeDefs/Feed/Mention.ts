import { gql } from "apollo-server-express";

const mentionFields = `
  childIndex: Int
  display: String
  id: String
  index: Int
  plainTextIndex: Int
  type: String
`;

export const MentionTypeDef = gql`
  type Mention {
    user: User
    ${mentionFields}
  }

  input MentionInput {
    user: String
    ${mentionFields}
  }
`;
