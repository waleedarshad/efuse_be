import { gql } from "apollo-server-express";

import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

export const VerifiedFeedTypeDef = gql`
  extend type Query {
    VerifiedFeed(page: Int, limit: Int): VerifiedFeed @auth(requires: "${UserRoles.USER}")
  }

  type VerifiedFeed {
    docs: [Post]
    ${PaginationFields}
  }
`;
