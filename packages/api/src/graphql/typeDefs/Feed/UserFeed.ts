import { gql } from "apollo-server-express";

import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

export const UserFeedTypeDef = gql`
  extend type Query {
    UserFeed(userId:String, page: Int, limit: Int): UserFeed @auth(requires: "${UserRoles.USER}")
  }

  type UserFeed {
    docs: [Post]
    ${PaginationFields}
  }
`;
