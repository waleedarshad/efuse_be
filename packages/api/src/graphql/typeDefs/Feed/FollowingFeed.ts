import { gql } from "apollo-server-express";

import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

export const FollowingFeedTypeDef = gql`
  extend type Query {
    FollowingFeed(page: Int, limit: Int): FollowingFeed @auth(requires: "${UserRoles.USER}")
  }

  type FollowingFeed {
    docs: [Post]
    ${PaginationFields}
  }
`;
