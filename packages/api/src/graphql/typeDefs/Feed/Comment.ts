import { gql } from "apollo-server-express";

import { CommentPaginationLimitEnum, ThreadPaginationLimitEnum } from "../../../lib/comments/comment.enum";
import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

const commonInputFields = `
  content: String
  file: FileInput
  giphy: GiphyInput
`;

export const CommentTypeDef = gql`
  type Comment {
    _id: ID!
    content: String
    user: User
    commentable: Post
    commentableType: String
    image: File
    mentions: [Mention]
    likes: Int
    giphy: Giphy
    parent: String
    hype: Hype
    thread: PaginatedComment
    platform: String
    media: Media
    createdAt: Date
    updatedAt: Date
    deleted: Boolean
  }

  type PaginatedComment {
    docs: [Comment]
    ${PaginationFields}
  }

  extend type Query {
    GetPaginatedComments(feedId: String!, page: Int = 1, limit: Int = ${CommentPaginationLimitEnum.DEFAULT}): PaginatedComment @auth(requires: "${UserRoles.USER}")
    GetPaginatedThreadComments(feedId: String!, parentId: String!, page: Int = 1, limit: Int = ${ThreadPaginationLimitEnum.DEFAULT}): PaginatedComment @auth(requires: "${UserRoles.USER}")
    
  }

  input CommentInput {
    feedId: String!
    mentions: [MentionInput]
    parent: String
    platform: String!
    ${commonInputFields}
  }

  input CommentUpdateInput {
    commentId: String!
    ${commonInputFields}
  }

  extend type Mutation {
    CreateFeedComment(body: CommentInput): Comment @auth(requires: "${UserRoles.USER}")
    UpdateFeedComment(body: CommentUpdateInput): Comment @auth(requires: "${UserRoles.USER}")
    DeleteFeedComment(commentId: String!): Comment @auth(requires: "${UserRoles.USER}")
    
  }

  extend type Post {
    "Top Comment"
    topComment: Comment
  }
`;
