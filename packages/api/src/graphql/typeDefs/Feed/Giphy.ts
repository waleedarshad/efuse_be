import { gql } from "apollo-server-express";

const DIMENSION_FIELDS = `
  height: String
  size: String
  url: String
  width: String
`;

const MP4_FIELDS = `
  mp4: String
  mp4_size: String
`;

const WEBP_FIELDS = `
  webp: String
  webp_size: String
`;

const GIPHY_FIELDS = `
  type: String
  id: String
  url: String
  slug: String
  bitly_gif_url: String
  bitly_url: String
  embed_url: String
  username: String
  source: String
  title: String
  rating: String
  content_url: String
  source_tld: String
  source_post_url: String
  is_sticker: Int
  import_datetime: String
  trending_datetime: String
`;

const GIPHY_USER_FIELDS = `
  avatar_url: String
  banner_image: String
  banner_url: String
  profile_url: String
  username: String
  display_name: String
  description: String
  instagram_url: String
  website_url: String
  is_verified: Boolean
`;

const GIPHY_ANALYTICS_URL = `
  url: String
`;

const generateFields = (fields: string, name: string): string => {
  return `
    type ${name} {
      ${fields}
    }

    input ${name}Input {
      ${fields}
    }
  `;
};

export const GiphyTypeDef = gql`
  ${generateFields(DIMENSION_FIELDS, "GiphyDimension")}

  ${generateFields(DIMENSION_FIELDS + WEBP_FIELDS, "GiphyFixedHeightDownSampled")}

  ${generateFields(
    ` height: String
      width: String
      ${MP4_FIELDS}`,
    "GiphyMP4Original"
  )}

  ${generateFields(DIMENSION_FIELDS + MP4_FIELDS + WEBP_FIELDS, "GiphyFixedHeight")}

  ${generateFields(
    ` frames: String
      hash: String
      ${DIMENSION_FIELDS}
      ${MP4_FIELDS}
      ${WEBP_FIELDS}`,
    "GiphyOriginal"
  )}

  ${generateFields(MP4_FIELDS, "GiphyLooping")}

  ${generateFields(` ${DIMENSION_FIELDS}`, "Giphy480Still")}


  ${generateFields(`${GIPHY_ANALYTICS_URL}`, "GiphyURL")}

  input GiphyImageSizeInput {
    downsized_large: GiphyDimensionInput
    fixed_height_small_still: GiphyDimensionInput
    original: GiphyOriginalInput
    fixed_height_downsampled: GiphyFixedHeightDownSampledInput
    downsized_still: GiphyDimensionInput
    fixed_height_still: GiphyDimensionInput
    downsized_medium: GiphyDimensionInput
    downsized: GiphyDimensionInput
    preview_webp: GiphyDimensionInput
    original_mp4: GiphyMP4OriginalInput
    fixed_height_small: GiphyFixedHeightInput
    fixed_height: GiphyFixedHeightInput
    downsized_small: GiphyMP4OriginalInput
    preview: GiphyMP4OriginalInput
    fixed_width_downsampled: GiphyFixedHeightDownSampledInput
    fixed_width_small_still: GiphyDimensionInput
    fixed_width_small: GiphyFixedHeightInput
    original_still: GiphyDimensionInput
    fixed_width_still: GiphyDimensionInput
    looping: GiphyLoopingInput
    fixed_width: GiphyFixedHeightInput
    preview_gif: GiphyDimensionInput
    hd: GiphyMP4OriginalInput
  }

  input GiphyUserInput {
    ${GIPHY_USER_FIELDS}

  }

  input GiphyInput {
    ${GIPHY_FIELDS}
    images: GiphyImageSizeInput
    user: GiphyUserInput
    analytics_response_payload: String
    analytics: GiphyAnalyticsInput
  }


  input GiphyAnalyticsInput {
    onload: GiphyURLInput
    onclick: GiphyURLInput
    onsent: GiphyURLInput
  }

`;
