import { gql } from "apollo-server-express";

export const HypeTypeDef = gql`
  extend type Hype {
    user: HypeUser
  }
`;
