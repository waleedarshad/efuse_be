import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const AimlabStatsTypeDef = gql`
  extend type User {
    aimlabStats: AimlabStats @auth(requires: "${UserRoles.NONE}")
  }

  extend type Query {
    getAimlabStats(userId: String): AimlabStats @auth(requires: "${UserRoles.NONE}")
  }

  type AimlabAppVersion {
    marketing: String
  }

  type AimlabPlay {
    createdAt: String
    endedAt: String
    mode: Int!
    performanceScores: JSONObject!
    playId: ID!
    rawDataUrl: String!
    score: Int!
    startedAt: String
    taskSlug: String!
    version: AimlabAppVersion!
    weaponName: String!
    weaponType: String!
  }

  type AimlabRank {
    displayName: String!
    level: Int!
    maxSkill: Float!
    minSkill: Float!
    tier: AimlabRankTier!
  }

  type AimlabRanking {
    badgeImage: String
    rank: AimlabRank
    skill: Float
  }

  enum AimlabRankTier {
    BRONZE
    DIAMOND
    EMERALD
    GOLD
    GRANDMASTER
    MASTER
    PLATINUM
    RUBY
    SILVER
  }

  type AimlabSkillScore {
    name: String!
    score: Float!
  }

  type AimlabTask {
    slug: String!
  }

  type AimlabTaskStats {
    averageScore: Float!
    playCount: Int!
    task: AimlabTask!
    topScore: Float!
  }

  type AimlabUser {
    id: ID!
    playfabID: String!
    plays: [AimlabPlay!]!
  }

  type AimlabStats {
    ranking: AimlabRanking
    skillScores: [AimlabSkillScore]
    taskStats: [AimlabTaskStats]
    owner: ID!
    user: AimlabUser
    username: String
  }
`;
