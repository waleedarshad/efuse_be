import { gql } from "apollo-server-express";
import { UserRoles } from "../enums";

export const ExternalAuthStatusTypeDef = gql`
  extend type User {
    externalAuthStatus: ExternalAuthStatus @auth(requires: "${UserRoles.NONE}")
  }

  type ExternalAuthStatus {
    google: Boolean
    twitch: Boolean
    twitter: Boolean
    statespace: Boolean
    riot: Boolean
    valorant: Boolean
  }
`;
