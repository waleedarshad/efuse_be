import { LeagueScoreCreatorTypeDef } from "../../../enums";
import { EnumUtil } from "../../../util";

export const LeagueScoreCreatorUnionTypeDef = `
  union LeagueScoreCreator = ${EnumUtil.convertEnumToUnionString(LeagueScoreCreatorTypeDef)}
`;
