import { OwnerTypeDef } from "../../../enums";

export const OwnerUnionTypeDef = `
  union Owner = ${OwnerTypeDef.ORGANIZATION} | ${OwnerTypeDef.USER}
`;
