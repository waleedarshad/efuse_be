import { LeagueScoreCreatorUnionTypeDef } from "./LeagueScoreCreatorUnion";
import { OwnerUnionTypeDef } from "./OwnerUnion";

export const SharedUnionTypeDefs = [OwnerUnionTypeDef, LeagueScoreCreatorUnionTypeDef];
