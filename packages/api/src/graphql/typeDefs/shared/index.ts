import { SharedEnumsTypeDefs } from "./enums";
import { SharedUnionTypeDefs } from "./unions";

export const SharedTypeDefs = [...SharedEnumsTypeDefs, ...SharedUnionTypeDefs];
