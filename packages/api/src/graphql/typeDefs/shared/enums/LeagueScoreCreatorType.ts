import { LeagueScoreCreatorType } from "@efuse/entities";
import { EnumUtil } from "../../../util";

export const LeagueScoreCreatorTypeEnumTypeDef = `
  enum LeagueScoreCreatorType {
    ${EnumUtil.convertEnumValuesToString(LeagueScoreCreatorType)}
  }
`;
