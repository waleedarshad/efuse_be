export const SortDirectionEnumTypeDef = `
  enum SortDirection {
    asc
    desc
  }
`;
