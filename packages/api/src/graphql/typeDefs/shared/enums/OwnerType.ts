import { OwnerType } from "@efuse/entities";

export const OwnerTypeEnumTypeDef = `
  enum OwnerType {
    ${OwnerType.USERS}
    ${OwnerType.ORGANIZATIONS}
  }
`;
