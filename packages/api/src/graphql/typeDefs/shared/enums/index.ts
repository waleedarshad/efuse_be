import { LeagueScoreCreatorTypeEnumTypeDef } from "./LeagueScoreCreatorType";
import { OwnerTypeEnumTypeDef } from "./OwnerType";
import { SortDirectionEnumTypeDef } from "./SortDirection";

export const SharedEnumsTypeDefs = [OwnerTypeEnumTypeDef, SortDirectionEnumTypeDef, LeagueScoreCreatorTypeEnumTypeDef];
