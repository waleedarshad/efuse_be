import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

// TODO: Remove when Frontend updated to use new queries
export const PipelineLeaderboardDeprecatedTypeDef = gql`
  type PipelineLeaderboardDeprecated {
    city: String
    graduationClass: String
    state: String
    country: String
    highSchool: String
    user: User
    stats: Stats
  }

  extend type Query {
    getValorantLeaderboard: [PipelineLeaderboardDeprecated] @auth(requires: "${UserRoles.NONE}")
    getAimlabLeaderboard: [PipelineLeaderboardDeprecated] @auth(requires: "${UserRoles.NONE}")
  }
`;
