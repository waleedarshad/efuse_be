import { PipelineLeaderboardTypeDef } from "./PipelineLeaderboard";
import { PipelineLeaderboardDeprecatedTypeDef } from "./PipelineLeaderboardDeprecated";

export const LeaderboardTypeDefs = [PipelineLeaderboardTypeDef];
export const LeaderboardDeprecatedTypeDefs = [PipelineLeaderboardDeprecatedTypeDef];
