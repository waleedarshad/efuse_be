import { gql } from "apollo-server-express";
import { LeaderboardStatType, UserRoles } from "../../enums";

export const PipelineLeaderboardTypeDef = gql`
  extend type User {
    pipelineLeaderboardRanks: LeaderboardRanks @auth(requires: "${UserRoles.NONE}")
  }

  union Stats = ${LeaderboardStatType.AIM_LAB_STATS} | ${LeaderboardStatType.LEAGUE_OF_LEGENDS_STATS} | ${LeaderboardStatType.VALORANT_STATS}

  # TODO: Redo this to take an argument so we can keep things more concise and make less work when adding additional games.
  extend type Query {
    getPaginatedAimlabLeaderboard(page: Int, limit: Int): PaginatedPipelineLeaderboard @auth(requires: "${UserRoles.NONE}")
    getPaginatedLeagueOfLegendsLeaderboard(page: Int, limit: Int): PaginatedPipelineLeaderboard @auth(requires: "${UserRoles.NONE}")
    getPaginatedValorantLeaderboard(page: Int, limit: Int): PaginatedPipelineLeaderboard @auth(requires: "${UserRoles.NONE}")
    getTopPipelinePlayers(games: [PipelineLeaderboardType], page: Int, limit: Int): [PipelineGame] @auth(requires: "${UserRoles.NONE}")
  }

  enum PipelineLeaderboardType {
    AIM_LAB
    LEAGUE_OF_LEGENDS
    VALORANT
  }

  type LeaderboardRanks {
    valorantLeaderboardRank: PipelineLeaderboard
    aimlabLeaderboardRank: PipelineLeaderboard
    leagueOfLegendsLeaderboardRank: PipelineLeaderboard
  }

  type PaginatedPipelineLeaderboard {
    docs: [PipelineLeaderboard],
    totalDocs: Int
    limit: Int
    page: Int
    totalPages: Int
    nextPage: Int
    prevPage:Int
    PagingCounter: Boolean
    hasPrevPage: Boolean
    hasNextPage: Boolean
  }

  type PipelineLeaderboard {
    id: ID!
    rank: Int!
    user: User!
    recruitmentProfile: RecruitmentProfile
    stats: Stats!
    createdAt: Date
  }

  type PipelineGame {
    game: Game!
    leaderboard: PaginatedPipelineLeaderboard!
  }
`;
