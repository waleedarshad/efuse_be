import { InviteCodeType, InviteResourceType } from "@efuse/entities";
import { gql } from "apollo-server-express";

import { UserRoles } from "../enums";
import { EnumUtil } from "../util";

export const InviteCodeTypeDef = gql`
  extend type Query {
    getInviteCode(code: String!): InviteCode @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    acceptInvite(code: String!, entityId: String!): Boolean @auth(requires: "${UserRoles.USER}")
  }

  type InviteCode {
    _id: String
    active: Boolean
    code: String
    createdAt: Date
    organization: Organization
    type: InviteCodeType
    updatedAt: Date
    user: User
    resource: InviteResource
    resourceType: InviteResourceType
  }

   enum InviteCodeType {
    ${EnumUtil.convertEnumValuesToString(InviteCodeType)}
  }

  enum InviteResourceType {
    ${EnumUtil.convertEnumValuesToString(InviteResourceType)}
  }

  union InviteResource = League | LeagueEvent | Team
`;
