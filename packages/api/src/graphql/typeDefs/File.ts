import { gql } from "apollo-server-express";

export const FileTypeDef = gql`
  input FileInput {
    "id of the media to update"
    id: String

    "The name of the given file"
    filename: String

    "The MIME content-type for the given file"
    contentType: String

    "The URL for which the file can be retrieved"
    url: String
  }
`;
