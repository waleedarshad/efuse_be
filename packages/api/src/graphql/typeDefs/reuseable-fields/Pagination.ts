import { Failure } from "@efuse/contracts";

// TODO: Once everything is converted over to using the paginateTypeDef function, remove this and
// include the values directly in the paginateTypeDef return statement.
export const PaginationFields = `
  totalDocs: Int
  limit: Int
  page: Int
  totalPages: Int
  nextPage: Int
  prevPage:Int
  pagingCounter: Boolean
  hasPrevPage: Boolean
  hasNextPage: Boolean
`;

export function paginateTypeDef(typeName: string): string {
  if (!typeName) {
    throw Failure.InternalServerError("Expected typeName but no typeName was supplied.");
  }

  return `
    docs: [${typeName}]
    ${PaginationFields}
  `;
}
