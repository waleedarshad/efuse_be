import { gql } from "apollo-server-express";

import { UserRoles } from "../enums";

export const GameTypeDef = gql`
  extend type Query {
    getGames: [Game] @auth(requires: "${UserRoles.NONE}")
    game(id: ID!): Game @auth(requires: "${UserRoles.NONE}")
    findGame(id: String): Game @auth(requires: "${UserRoles.NONE}")
    findGameBySlug(slug: String): Game @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    AddGamesToOrganization(organizationId: String!, gameIds: [String]!): Organization @auth(requires: "${UserRoles.USER}")
    AddGamesToUser(gameIds: [String]!): User @auth(requires: "${UserRoles.USER}")
  }

  input GameInput {
    _id: String
    description: String
    kind: String
    slug: String
    title: String
    createdAt: Date
    updatedAt: Date
    gameImage: FileInput
  }

  extend type Organization {
    games: [Game]
  }

  extend type ERenaTournament {
    game: Game
  }

  extend type User {
    games: [Game]
  }

  extend type Post {
    game: Game
  }
`;
