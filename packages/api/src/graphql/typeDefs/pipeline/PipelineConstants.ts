import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const PipelineConstants = gql`
  extend type Query {
    getPipelineGames: [Game] @auth(requires: "${UserRoles.NONE}")
  }

  extend type Game {
    pipelineInfo: GamePipelineInfo
  }

  type GamePipelineInfo {
    pipelineEnabled: Boolean
    roles: [String]
  }
`;
