import { gql } from "apollo-server-express";
import { NoteOwnerType, NoteRelatedModel } from "@efuse/entities";
import { NoteOwnerTypeDef, NoteRelatedDocTypeDef, UserRoles } from "../../enums";
import { PaginationFields } from "../reuseable-fields";

export const NoteTypeDef = gql`
  extend type Query {
    getNote(noteId: String!): Note @auth(requires: "${UserRoles.NONE}")
    getNotes(relatedDoc: String!, owner: String!): [Note] @auth(requires: "${UserRoles.NONE}")
    getPaginatedNotes(relatedDoc: String!, owner: String!, page: Int, limit: Int): PaginatedNote @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createNote(note: CreateNoteArgs!): Note @auth(requires: "${UserRoles.NONE}")
    deleteNote(noteId: String!): Boolean @auth(requires: "${UserRoles.NONE}")
    updateNote(noteId: String!, content: String!): Note @auth(requires: "${UserRoles.NONE}")
  }

  union NoteOwner = ${NoteOwnerTypeDef.ORGANIZATION} | ${NoteOwnerTypeDef.USER}

  enum NoteOwnerType {
    ${NoteOwnerType.USERS}
    ${NoteOwnerType.ORGANIZATIONS}
  }

  union NoteRelatedDoc = ${NoteRelatedDocTypeDef.APPLICANT} | ${NoteRelatedDocTypeDef.RECRUITMENT_PROFILE}

  enum NoteRelatedModel {
    ${NoteRelatedModel.APPLICANTS}
    ${NoteRelatedModel.RECRUITMENT_PROFILES}
  }

  type PaginatedNote {
    docs: [Note]
    ${PaginationFields}
  }

  input CreateNoteArgs {
    author: String!
    content: String!
    relatedDoc: String!
    relatedModel: NoteRelatedModel!
    owner: String!
    ownerType: NoteOwnerType!
  }

  type Note {
    _id: ID!
    createdAt: Date
    author: User
    content: String
    owner: NoteOwner
    ownerType: NoteOwnerType
    relatedDoc: NoteRelatedDoc
    relatedModel: NoteRelatedModel
    updatedAt: Date
  }
`;
