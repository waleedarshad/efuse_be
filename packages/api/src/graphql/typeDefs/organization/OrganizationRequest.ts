import { gql } from "apollo-server-express";

export const OrganizationRequestTypeDef = gql`
  type OrganizationRequest {
    _id: String
    organization: Organization
    user: User
    status: String
    createdAt: Date
    UpdatedAt: Date
  }
`;
