import { MemberTypeDef } from "./Member";
import { OrganizationTypeDef } from "./Organization";
import { OrganizationFollowerTypeDef } from "./OrganizationFollower";
import { OrganizationRequestTypeDef } from "./OrganizationRequest";

export const OrganizationTypeDefs = [
  MemberTypeDef,
  OrganizationTypeDef,
  OrganizationFollowerTypeDef,
  OrganizationRequestTypeDef
];
