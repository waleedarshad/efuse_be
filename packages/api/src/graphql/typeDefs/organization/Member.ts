import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const MemberTypeDef = gql`
  extend type Organization {
    members: [Member]
  }

  extend type Query {
    getOrganizationMemberById(memberId: String!): Member @auth(requires: "${UserRoles.NONE}")
    getOrganizationMembers(orgId: String!): [Member] @auth(requires: "${UserRoles.NONE}")
  }

  type Member {
    _id: String
    title: String
    organization: Organization
    user: User
    isBanned: Boolean
    createdAt: Date
    updatedAt: Date
    deleted: Boolean
    deletedAt: Date
  }
`;
