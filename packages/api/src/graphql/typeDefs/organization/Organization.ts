import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const OrganizationTypeDef = gql`
  extend type Post {
    organization: FeedOrganization
  }

  extend type Query {
    getOrganizationById(organizationId: String!): Organization @auth(requires: "${UserRoles.NONE}")
    getUserOrganizations(userId: String!, includeOrgsWhereMember: Boolean): [Organization] @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createOrganization(organization: CreateOrganizationArgs!): Organization @auth(requires: "${UserRoles.USER}")
  }

  # TODO: Add the optional fields
  input CreateOrganizationArgs {
    name: String!
    shortName: String!
    status: String!
    organizationType: String!
    associatedGames: [String]
  }

  type Organization @auth(requires: "${UserRoles.NONE}")  {
    _id: ID!
    about: String
    captains: [User]
    createdAt: Date
    description: String
    discordServer: String
    email: String
    events: [OrganizationEvents]
    followersCount: Int
    headerImage: File
    honors: [OrganizationHonors]
    location: String
    name: String
    organizationType: String
    publishStatus: String
    playerCards: [OrganizationPlayerCards]
    phoneNumber: String
    profileImage: File
    promoted: Boolean
    promoVideo: OrganizationPromoVideo
    scale: String
    status: String
    shortName: String
    slug: String
    toBeVerified: Boolean
    updatedAt: Date
    url: String
    user: User
    verified: OrganizationVerified
    videoCarousel: [OrganizationVideoCarousel]
    website: String
    isCurrentUserAFollower: OrganizationFollower
    isCurrentUserAMember: Member
    currentUserRequestToJoin: OrganizationRequest
    membersCount: Int
  }

  type FeedOrganization {
    id: String
    profileImage: String
    slug: String
    name: String
    verified: Boolean
  }

  type OrganizationEvents {
    title: String
    subTitle: String
    eventDate: Date
    description: String
    image: File
  }

  type OrganizationHonors {
    title: String
    subTitle: String
    description: String
    image: File
  }

  type OrganizationPlayerCards {
    image: File
    subTitle: String
    title: String
    url: String
  }

  type OrganizationPromoVideo {
    description: String
    embeddedUrl: String
    isVideo: Boolean
    title: String
    videoFile: File
  }

  type OrganizationVerified {
    status: Boolean
    verifiedAt: Date
    verifiedBy: User
  }

  type OrganizationVideoCarousel {
    title: String
    uploaded: Boolean
    url: String
    video: File
  }
`;
