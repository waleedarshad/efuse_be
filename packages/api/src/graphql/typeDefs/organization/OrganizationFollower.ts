import { gql } from "apollo-server-express";

export const OrganizationFollowerTypeDef = gql`
  type OrganizationFollower {
    _id: String
    followee: Organization
    follower: User
    createdAt: Date
    UpdatedAt: Date
  }
`;
