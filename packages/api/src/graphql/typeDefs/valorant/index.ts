import { ValorantStatsTypeDef } from "./ValorantStats";
import { ValorantProfileTypeDef } from "./ValorantProfile";

export const ValorantTypeDefs = [ValorantStatsTypeDef, ValorantProfileTypeDef];
