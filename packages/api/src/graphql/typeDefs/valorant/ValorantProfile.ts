import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ValorantProfileTypeDef = gql`
  extend type User {
    valorantAccountProfile: ValorantProfile @auth(requires: "${UserRoles.NONE}")
  }

  type ValorantProfile {
    puuid: String
    gameName: String
    tagLine: String
    shard: String
  }
`;
