import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ValorantStatsTypeDef = gql`
  extend type Query {
    getValorantStats(userId: String): ValorantUserStats @auth(requires: "${UserRoles.NONE}")
  }

  extend type User {
    valorantStats: ValorantUserStats @auth(requires: "${UserRoles.NONE}")
  }

  type ValorantUserStats {
    user: ID!
    puuid: String
    gameName: String
    tagLine: String
    region: String

    competitiveTier: Int
    competitiveTierName: String
    competitiveTierImage: String
    rankedRating: Int
    leaderboardRank: Int

    wins: Int
    losses: Int
    agentMatchStats: [AgentPlayerStats]
    lastMatchTime: Float
    damage: Float
    kills: Int
    deaths: Int
    assists: Int
    episodeId: String
    episodeName: String
    actId: String
    actName: String
  }

  type AgentPlayerStats {
    agent: String
    wins: Int
    losses: Int
    kills: Int
    deaths: Int
    assists: Int
    damage: Float
    timePlayed: Float
    agentData: ValorantAgent
  }

  type ValorantAgent {
    uuid: String
    displayName: String
    description: String
    developerName: String
    displayIcon: String
    displayIconSmall: String
    bustPortrait: String
    fullPortrait: String
    assetPath: String
  }
`;
