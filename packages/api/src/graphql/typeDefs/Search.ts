import { gql } from "apollo-server-express";

import { SearchLimit } from "../../lib/search/search.enum";
import { UserRoles } from "../enums";

export const SearchTypeDef = gql`
  extend type Query {
    UserSearch(params: SearchArgs!): [User] @auth(requires: "${UserRoles.USER}")
    OrganizationSearch(params: SearchArgs!): [Organization] @auth(requires: "${UserRoles.USER}")
    OpportunitySearch(params: SearchArgs!): [Opportunity] @auth(requires: "${UserRoles.USER}")
    LearningArticleSearch(params: SearchArgs!): [LearningArticle] @auth(requires: "${UserRoles.USER}")
  }

  input SearchArgs {
    term: String
    limit: Int = ${SearchLimit.DEFAULT_SEARCH_LIMIT} @constraint(min: 1, max: ${SearchLimit.MAX_SEARCH_LIMIT})
  }
`;
