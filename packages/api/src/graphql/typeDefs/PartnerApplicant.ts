import { gql } from "apollo-server-express";

import { UserRoles } from "../enums";

export const PartnerApplicantTypeDef = gql`
  extend type Query {
    CanApplyForPartnerBadge: PartnerApplicantApplyResponse @auth(requires: "${UserRoles.USER}")
  }

  extend type Mutation {
    ApplyForPartnerBadge: PartnerApplicant @auth(requires: "${UserRoles.USER}")
  }

  type PartnerApplicant {
    user: User
    status: String
  }

  type PartnerApplicantApplyResponse {
    canApply: Boolean
    reasons: [String]
    partnerApplicant: PartnerApplicant
  }
`;
