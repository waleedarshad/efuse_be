import { gql } from "apollo-server-express";

export const RecruitmentProfileTypeDef = gql`
  type RecruitmentProfile {
    name: String
    user: User
    graduationClass: String
    addressLine1: String
    addressLine2: String
    city: String
    state: String
    zipCode: String
    country: String
    desiredCollegeMajor: [String]
    desiredCollegeSize: String
    desiredCollegeAttributes: [String]
    desiredCollegeRegion: String
    gender: String
    esportsCareerField: String
    highSchoolGPA: String
    usaTestScores: String
    satTestScore: String
    actTestScore: String
    toeflTestScores: String
    extracurricularActivities: String
    gradingScale: String
    primaryGame: String
    primaryGameUsername: String
    primaryGameHoursPlayed: String
    secondaryGame: String
    secondaryGameUsername: String
    secondaryGameHoursPlayed: String
    highSchoolClubTeam: String
    coachName: String
    coachEmail: String
    coachPhone: String
    isHidden: Boolean
    profileProgress: Float
    profileProgressUpdatedAt: Date
    highSchool: String
    createdAt: Date
    updatedAt: Date
    starred: Boolean
    recruitAction: String
  }
`;
