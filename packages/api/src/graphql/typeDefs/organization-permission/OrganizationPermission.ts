import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";
import { EnumUtil } from "../../util";
import { OrganizationUserRoleEnum } from "../../../lib/permissions/enums";

export const OrganizationPermissionTypeDef = gql`
  extend type Query {
    "Gets all orgs a user is at least a member of. Will default to authenticated user except if userId is passed in."
    getUserOrgRoles(userId: String): OrganizationUserRole @auth(requires: "${UserRoles.USER}")
    "Gets all user orgs associated with leagues. Will default to authenticated user except if userId is passed in"
    getUsersLeagueOrgsAndRoles(userId: String): OrganizationUserRole @auth(requires: "${UserRoles.USER}")
    "Determines if the user can create leagues for at least one organization they are a member of."
    canUserCreateLeagues(userId: String): Boolean @auth(requires: "${UserRoles.USER}")
    "Determines if the user can create leagues for the organization."
    canUserCreateLeaguesForOrg(orgId: String!, userId: String): Boolean @auth(requires: "${UserRoles.USER}")
    "Gets the role the user has for the organization."
    getUserOrgRole(orgId: String!, userId: String): OrganizationUserRoles @auth(requires: "${UserRoles.USER}")
  }

  type OrganizationUserRole {
    user: User
    organizationRoles: [OrganizationRole]
  }

  type OrganizationRole {
    organization: Organization
    role: OrganizationUserRoles
  }

  enum OrganizationUserRoles {
    ${EnumUtil.convertEnumValuesToString(OrganizationUserRoleEnum)}
  }

  extend type Organization {
    currentUserRole: OrganizationUserRoles
  }
`;
