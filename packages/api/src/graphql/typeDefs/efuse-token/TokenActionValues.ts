import { gql } from "apollo-server-express";

import { UserRoles } from "../../enums";

export const TokenActionValuesTypeDef = gql`
  extend type Query {
    getTokenActionValues: TokenActionValues @auth(requires: "${UserRoles.USER}")
  }

  type TokenActionValues {
    newPost: Float
    comment: Float
    hype: Float
    boostPost: Float
  }
`;
