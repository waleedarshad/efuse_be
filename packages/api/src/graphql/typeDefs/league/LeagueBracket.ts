import { LeagueBracketType } from "@efuse/entities";
import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";
import { EnumUtil } from "../../util";

const bracketsForGQL = EnumUtil.convertEnumValuesToString(LeagueBracketType);

export const LeagueBracketTypeDef = gql`
  extend type LeaguePool {
    bracket: LeagueBracket
  }

  extend type Query {
    getLeagueBracketById(bracketId: String!): LeagueBracket @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createLeagueBracket(leaguePoolId: String!, bracketBody: CreateLeagueBracketBody!): LeagueBracket @auth(requires: "${UserRoles.USER}")
    deleteLeagueBracket(bracketId: String!): LeagueBracket @auth(requires: "${UserRoles.USER}")
    updateLeagueBracket(bracketId: String!, name: String!): LeagueBracket @auth(requires: "${UserRoles.USER}")
  }

  input CreateLeagueBracketBody {
    name: String!
    type: BracketType!
  }

  enum BracketType {
    ${bracketsForGQL}
  }

  type LeagueBracket {
    _id: ID
    name: String
    pool: LeaguePool
    type: BracketType
    createdAt: Date
    updatedAt: Date
  }
`;
