import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const LeagueScoreboardTypeDef = gql`
  extend type Query {
    getScoreboardsForEvent(eventId: String!): [LeagueScoreboardWithPool] @auth(requires: "${UserRoles.NONE}")
    getScoreboardForPool(poolId: String!): [LeagueScoreboard] @auth(requires: "${UserRoles.NONE}")
  }

  type LeagueScoreboard {
    team: Team
    wins: Int
    losses: Int
  }

  type LeagueScoreboardWithPool {
    pool: LeaguePool
    scoreboard: [LeagueScoreboard]
  }
`;
