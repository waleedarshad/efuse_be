import { LeagueMatchState } from "@efuse/entities";
import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";
import { EnumUtil } from "../../util";
import { paginateTypeDef } from "../reuseable-fields";

const statesForGQL = EnumUtil.convertEnumValuesToString(LeagueMatchState);

export const LeagueMatchTypeDef = gql`
  extend type LeagueRound {
    matches: [LeagueMatch]
  }

  extend type Query {
    getLeagueMatchById(matchId: String!): LeagueMatch @auth(requires: "${UserRoles.NONE}")
    getPaginatedLeagueMatches(roundId: String!, page: Int, limit: Int): PaginatedLeagueMatch @auth(requires: "${
      UserRoles.NONE
    }")
  }

  extend type Mutation {
    createLeagueMatch(leagueMatchBody: LeagueMatchBody!): LeagueMatch @auth(requires: "${UserRoles.USER}")
    deleteLeagueMatch(matchId: String!): LeagueMatch @auth(requires: "${UserRoles.USER}")
    updateLeagueMatch(matchId: String!, leagueMatchBody: LeagueMatchBody!): LeagueMatch @auth(requires: "${
      UserRoles.USER
    }")
  }

  input LeagueMatchBody {
    round: String
    teams: [String]
    winner: String
    state: LeagueMatchState
    startTime: Date
    endTime: Date
  }

  enum LeagueMatchState {
    ${statesForGQL}
  }

  type PaginatedLeagueMatch {
    ${paginateTypeDef("LeagueMatch")}
  }

  type LeagueMatch {
    _id: ID
    teams: [Team]
    winner: Team
    round: LeagueRound
    state: LeagueMatchState
    startTime: Date
    endTime: Date
    createdAt: Date
    updatedAt: Date
  }
`;
