import { LeagueScoreCompetitorType } from "@efuse/entities";
import { gql } from "apollo-server-express";
import { LeagueScoreCompetitor, UserRoles } from "../../enums";
import { EnumUtil } from "../../util";

export const LeagueCompetitorScoreTypeDef = gql`
  extend type LeagueGameScore {
    scores: [LeagueCompetitorScore]
  }

  extend type Query {
    getLeagueCompetitorScoreById(scoreId: String!): LeagueCompetitorScore @auth(requires: "${UserRoles.NONE}")
    getLeagueCompetitorScores(gameScoreId: String!): [LeagueCompetitorScore] @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createLeagueCompetitorScore(scoreBody: CreateLeagueCompetitorScoreBody): LeagueCompetitorScore @auth(requires: "${
      UserRoles.USER
    }")
    deleteLeagueCompetitorScore(scoreId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    updateLeagueCompetitorScore(scoreId: String!, scoreBody: UpdateLeagueCompetitorScoreBody!): LeagueCompetitorScore @auth(requires: "${
      UserRoles.USER
    }")
  }

  input UpdateLeagueCompetitorScoreBody {
    value: Int
    image: String
  }

  input CreateLeagueCompetitorScoreBody {
    creator: String!
    creatorType: LeagueScoreCreatorType!
    value: Int
    image: String
    competitor: String!
    competitorType: LeagueScoreCompetitorType!
    gameScore: String!
  }

  union LeagueScoreCompetitor = ${EnumUtil.convertEnumToUnionString(LeagueScoreCompetitor)}

  enum LeagueScoreCompetitorType {
    ${EnumUtil.convertEnumValuesToString(LeagueScoreCompetitorType)}
  }

  type LeagueCompetitorScore {
    _id: ID
    creator: LeagueScoreCreator
    creatorType: LeagueScoreCreatorType
    image: String
    value: Int
    competitor: LeagueScoreCompetitor
    competitorType: LeagueScoreCompetitorType
    gameScore: LeagueGameScore
    createdAt: Date
    updatedAt: Date
  }
`;
