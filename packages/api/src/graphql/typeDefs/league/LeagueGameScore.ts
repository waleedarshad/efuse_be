import { LeagueGameScoreType } from "@efuse/entities";
import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";
import { EnumUtil } from "../../util";
import { paginateTypeDef } from "../reuseable-fields";

export const LeagueGameScoreTypeDef = gql`
  extend type LeagueMatch {
    submittedScores: [LeagueGameScore]
    finalScores: [LeagueGameScore]
  }

  extend type Query {
    getLeagueGameScoreById(gameScoreId: String!): LeagueGameScore @auth(requires: "${UserRoles.NONE}")
    "Use type to mark which game score to get. Otherwise will get all types"
    getLeagueGameScores(matchId: String!, type: LeagueGameScoreType): [LeagueGameScore] @auth(requires: "${
      UserRoles.NONE
    }")
    getPaginatedLeagueGameScores(
      matchId: String!,
      type: LeagueGameScoreType!,
      page: Int = 1,
      limit: Int = 5,
      sortDirection: SortDirection = asc
    ): PaginatedLeagueGameScore @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    submitLeagueGameScore(gameScoreBody: CreateLeagueGameScoreBody!, competitorScores: [LeagueCompetitorScoreBody]! ): LeagueGameScore @auth(requires: "${
      UserRoles.USER
    }")
    deleteLeagueGameScore(gameScoreId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    updateLeagueGameScore(gameScoreId: String!, gameScoreBody: UpdateLeagueGameScoreBody!, competitorScores: [UpdateCompetitorScoreBody]): LeagueGameScore @auth(requires: "${
      UserRoles.USER
    }")
  }

  input LeagueCompetitorScoreBody {
    value: Int!
    image: String
    competitor: String!
    competitorType: LeagueScoreCompetitorType!
  }

  input UpdateCompetitorScoreBody {
    _id: String!
    image: String
    value: Int
  }

  input UpdateLeagueGameScoreBody {
    image: String
    type: LeagueGameScoreType
  }

  input CreateLeagueGameScoreBody {
    creator: String!
    creatorType: LeagueScoreCreatorType!
    image: String
    match: String!
    gameNumber: Int!
    type: LeagueGameScoreType = ${LeagueGameScoreType.SUBMITTED}
  }

  enum LeagueGameScoreType {
    ${EnumUtil.convertEnumValuesToString(LeagueGameScoreType)}
  }

  type PaginatedLeagueGameScore {
    ${paginateTypeDef("LeagueGameScore")}
  }

  type LeagueGameScore {
    _id: ID
    creator: LeagueScoreCreator
    creatorType: LeagueScoreCreatorType
    image: String
    match: LeagueMatch
    gameNumber: Int
    type: LeagueGameScoreType
    createdAt: Date
    updatedAt: Date
  }
`;
