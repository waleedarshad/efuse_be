import { LeagueEventCalendarStatus, LeagueEventState, LeagueEventTimingMode } from "@efuse/entities";
import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";
import { EnumUtil } from "../../util";
import { paginateTypeDef } from "../reuseable-fields";

export const LeagueEventTypeDef = gql`
  extend type League {
    events: [LeagueEvent]
  }

  extend type Query {
    getLeagueEventById(eventId: String!): LeagueEvent @auth(requires: "${UserRoles.NONE}")

  "eventCalendarStatus and eventState cannot be used in same request"
    getLeagueEvents(
      leagueId: String!,
      bracketType: BracketType,
      eventCalendarStatus: LeagueEventCalendarStatus,
      eventState: LeagueEventState
    ): [LeagueEvent] @auth(requires: "${UserRoles.NONE}")

  "eventCalendarStatus and eventState cannot be used in same request"
    getPaginatedLeagueEvents(
      league: String!,
      page: Int = 1,
      limit: Int = 5,
      bracketType: BracketType,
      eventCalendarStatus: LeagueEventCalendarStatus,
      eventState: LeagueEventState
    ): PaginatedLeagueEvent @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createLeagueEvent(leagueEvent: CreateLeagueEventArg!): LeagueEvent @auth(requires: "${UserRoles.USER}")
    deleteLeagueEvent(eventId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    generateRoundsAndMatches(eventId: String!): LeagueEvent @auth(requires: "${UserRoles.USER}")
    updateLeagueEvent(eventId: String!, leagueEvent: UpdateLeagueEventArg!): LeagueEvent @auth(requires: "${
      UserRoles.USER
    }")
  }

  enum LeagueEventCalendarStatus {
    ${EnumUtil.convertEnumValuesToString(LeagueEventCalendarStatus)}
  }

  enum LeagueEventTimingMode {
    ${EnumUtil.convertEnumValuesToString(LeagueEventTimingMode)}
  }

  enum LeagueEventState {
    ${EnumUtil.convertEnumValuesToString(LeagueEventState)}
  }

  type PaginatedLeagueEvent {
    ${paginateTypeDef("LeagueEvent")}
  }

  input CreateLeagueEventArg {
    bracketType: BracketType!
    description: String
    gamesPerMatch: Int!
    leagueId: String!
    name: String!
    maxTeams: Int
    numberOfPools: Int
    numberOfRounds: Int
    startDate: Date
    state: LeagueEventState = ${LeagueEventState.PENDING_TEAMS}
    timingMode: LeagueEventTimingMode # This will change in the future
    details: String
    rules: String
  }

  input UpdateLeagueEventArg {
    description: String
    gamesPerMatch: Int
    maxTeams: Int
    name: String
    startDate: Date
    state: LeagueEventState
    teams: [String]
    timingMode: LeagueEventTimingMode # This will change in the future
    details: String
    rules: String
  }

  type LeagueEvent {
    _id: ID
    bracketType: BracketType
    createdAt: Date
    description: String
    details: String
    gamesPerMatch: Int
    league: League
    maxTeams: Int
    name: String
    teams: [Team]
    rules: String
    startDate: Date
    state: LeagueEventState
    timingMode: LeagueEventTimingMode # This will change in the future
    updatedAt: Date
  }
`;
