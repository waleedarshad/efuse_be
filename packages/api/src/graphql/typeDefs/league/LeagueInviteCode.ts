import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const LeagueInviteCodeTypeDef = gql`
  extend type Organization {
    leaguesInviteCode: String @auth(requires: "${UserRoles.USER}")
  }
`;
