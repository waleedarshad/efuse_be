import { gql } from "apollo-server-express";
import { LeagueEntryType, LeagueJoinStatus, LeagueState } from "@efuse/entities";
import { UserRoles } from "../../enums";
import { paginateTypeDef } from "../reuseable-fields";
import { EnumUtil } from "../../util";

export const LeagueTypeDef = gql`
  enum LeagueState {
    ${EnumUtil.convertEnumValuesToString(LeagueState)}
  }

  enum LeagueEntryType {
    ${EnumUtil.convertEnumValuesToString(LeagueEntryType)}
  }

  enum LeagueJoinStatus {
    ${EnumUtil.convertEnumValuesToString(LeagueJoinStatus)}
  }

  extend type Query {
    getLeagueById(leagueId: String!): League @auth(requires: "${UserRoles.USER}")
    getLeaguesByOrganization(organizationId: String!): [League] @auth(requires: "${UserRoles.USER}")
    getPaginatedLeagues(page: Int, limit: Int): PaginatedLeagues @auth(requires: "${UserRoles.USER}")
    getJoinedOrPendingLeaguesByOrg(organizationId: String!, joinStatus: LeagueJoinStatus): [LeagueResultWithJoinStatus] @auth(requires: "${
      UserRoles.USER
    }")
  }

  extend type Mutation {
    createLeague(league: CreateLeagueArgs!): League @auth(requires: "${UserRoles.USER}")
    deleteLeague(leagueId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    updateLeague(leagueId: String!, updates: UpdateLeagueArgs!): League @auth(requires: "${UserRoles.USER}")
  }

  input CreateLeagueArgs {
    name: String!
    imageUrl: String
    description: String
    owner: String!
    ownerType: OwnerType!
    entryType: LeagueEntryType
    game: String!
    teams: [String]
    rules: String
  }

  # TODO: Currently we do not support the ability to transfer ownership of a league.
  input UpdateLeagueArgs {
    name: String
    imageUrl: String
    description: String
    entryType: LeagueEntryType
    game: String
    teams: [String]
    rules: String
  }

  type PaginatedLeagues {
    ${paginateTypeDef("League")}
  }

  type League {
    _id: ID
    name: String
    imageUrl: String
    description: String
    owner: Owner
    ownerType: OwnerType
    createdAt: Date
    updatedAt: Date
    entryType: LeagueEntryType
    game: Game
    teams: [Team]
    state: LeagueState
    rules: String
  }

  type LeagueResultWithJoinStatus {
    league: League
    joinStatus: LeagueJoinStatus
  }
`;
