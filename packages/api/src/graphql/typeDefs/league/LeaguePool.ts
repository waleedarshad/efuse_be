import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";
import { paginateTypeDef } from "../reuseable-fields";

export const LeaguePoolTypeDef = gql`
  extend type LeagueEvent {
    pools: [LeaguePool]
  }

  extend type Query {
    getLeaguePoolById(poolId: String!): LeaguePool @auth(requires: "${UserRoles.NONE}")
    getPaginatedPools(eventId: String!, page: Int, limit: Int ): PaginatedLeaguePool @auth(requires: "${
      UserRoles.NONE
    }")
  }

  extend type Mutation {
    createLeaguePool(eventId: String!, name: String, teams: [String]): LeaguePool @auth(requires: "${UserRoles.USER}")
    deleteLeaguePool(poolId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    generateLeagueTeamPools(eventId: String!): [LeaguePool] @auth(requires: "${UserRoles.USER}")
    updateLeaguePool(poolId: String!, poolBody: UpdateLeaguePoolBody!): LeaguePool @auth(requires: "${UserRoles.USER}")
    moveTeamsToLeaguePool(fromPoolId: String, toPoolId: String, teams: [String]): [LeaguePool] @auth(requires: "${
      UserRoles.USER
    }")
    clearAllTeamsInLeaguePools(eventId: String): [LeaguePool] @auth(requires: "${UserRoles.USER}")
  }

  input UpdateLeaguePoolBody {
    name: String
    teams: [String]
  }

  type PaginatedLeaguePool {
    ${paginateTypeDef("LeaguePool")}
  }

  type LeaguePool {
    _id: ID
    event: LeagueEvent
    name: String
    teams: [Team]
    createdAt: Date
    updatedAt: Date
  }

  input GetPaginatedLeaguePoolArgs {
    eventId: String!
    page: Int
    limit: Int
  }
`;
