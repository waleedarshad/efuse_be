import { gql } from "apollo-server-express";
import { LeagueRoundSort, UserRoles } from "../../enums";
import { EnumUtil } from "../../util";
import { paginateTypeDef } from "../reuseable-fields";

export const LeagueRoundTypeDef = gql`
  extend type LeagueBracket {
    rounds: [LeagueRound]
  }

  extend type Query {
    getLeagueRoundById(roundId: String!): LeagueRound @auth(requires: "${UserRoles.NONE}")
    getLeagueRoundsByBracket(bracketId: String!): [LeagueRound] @auth(requires: "${UserRoles.NONE}")
    getPaginatedLeagueRounds(
      bracketId: String!,
      page: Int = 1,
      limit: Int = 5,
      sortOn: LeagueRoundSort = ${LeagueRoundSort.VALUE},
      sortDirection: SortDirection = ${"asc"}
    ): PaginatedLeagueRound @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createLeagueRound(round: CreateLeagueRoundBody): LeagueRound @auth(requires: "${UserRoles.USER}")
    deleteLeagueRound(roundId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    updateLeagueRound(roundId: String!, roundUpdateBody: UpdateLeagueRoundBody): LeagueRound @auth(requires: "${
      UserRoles.USER
    }")
  }

  input CreateLeagueRoundBody {
    bracket: String!
    name: String!
    value: Int!
    startDate: Date
  }

  input UpdateLeagueRoundBody {
    name: String!
  }

  enum LeagueRoundSort {
    ${EnumUtil.convertEnumValuesToString(LeagueRoundSort)}
  }

  type PaginatedLeagueRound {
    ${paginateTypeDef("LeagueRound")}
  }

  type LeagueRound {
    _id: ID
    bracket: LeagueBracket
    name: String
    value: Int
    startDate: Date
    createdAt: Date
    updatedAt: Date
  }
`;
