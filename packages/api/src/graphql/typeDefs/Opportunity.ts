import { gql } from "apollo-server-express";
import { UserRoles } from "../enums";

export const OpportunityTypeDef = gql`
  type Opportunity {
    _id: ID!
    title: String
    description: String
    category: String
    subCategory: String
    url: String
    author: User
    organization: Organization
    location: String
    image: File
  }

  extend type Query {
    getOpportunityByIdOrShortName(idOrShortName: String): Opportunity @auth(requires: "${UserRoles.NONE}")
  }
`;
