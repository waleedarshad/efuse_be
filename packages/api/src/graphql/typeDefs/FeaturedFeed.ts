import { gql } from "apollo-server-express";

import { PaginationFields } from "./reuseable-fields";
import { UserRoles } from "../enums";

export const FeaturedFeedTypeDef = gql`
  extend type Query {
    FeaturedFeed(page: Int, limit: Int): FeaturedFeed @auth(requires: "${UserRoles.USER}")
  }

  type FeaturedFeed {
    docs: [Post]
    ${PaginationFields}
  }
`;
