import { gql } from "apollo-server-express";

import { GamePostsPaginationLimitEnum } from "../../lib/feeds/feed.enum";
import { PaginationFields } from "./reuseable-fields";
import { UserRoles } from "../enums";

export const PostTypeDef = gql`
  extend type Query {
    getPostById(id: ID!): Post @auth(requires: "${UserRoles.USER}")
    getPostsByGame(gameId: ID!, page: Int = 1, limit: Int = ${GamePostsPaginationLimitEnum.DEFAULT}): PaginatedPost @auth(requires: "${UserRoles.USER}")
    gethomeFeedPost(feedId: ID!): Post @auth(requires: "${UserRoles.USER}")
    getLoungeFeedPost(feedId: ID!): Post @auth(requires: "${UserRoles.USER}")
  }

  extend type Mutation {
    boostPost(feedId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    deletePost(feedId: ID!): Boolean @auth(requires: "${UserRoles.USER}")
    createPost(body: PostInput!): Post @auth(requires: "${UserRoles.USER}")
    updatePost(id: ID, body: UpdatePostInput!): Post @auth(requires: "${UserRoles.USER}")
    createSchedulePost(body: PostInput!): Post @auth(requires: "${UserRoles.USER}")
  }

  """
  A type that describes a Post object
  """
  type Post {

    "Parent Feed Id"
    parentFeedId: String

    "Post type"
    kind: String

    "Feed timelinable id"
    kindId: String

    "The text content of the post"
    text: String

    "A reference to the user who created the post"
    author: User

    "Mentions"
    mentions: [Mention]

    "The database id for the given post"
    feedId: String

    "The media associated with the Post"
    media: [Media]

    "A reference to the timestamp the post was created"
    createdAt: Date

    "The total views currently on a post"
    views: Int

    "The total number of hypes currently on a post"
    comments: Int

    "The total number of hypes currently on a post"
    hypes: Int

    "The total number of hypes the current viewer has on the post"
    currentUserHypes: Int

    "The badges assigned to the post author"
    authorBadges: [UserBadge]

    "The Giphy associated with Feed"
    giphy: Giphy

    "Is current user follow"
    currentUserFollows: Boolean

    "Rank of the feed"
    feedRank: Float

    "Rank score"
    rankScore: Float

    "Open graph"
    openGraph: [OpenGraph]

    original: Boolean

    deleted: Boolean

    updatedAt: String

    shared: Boolean

    reports: [ReportedBy]

    "Is Feed Boosted"
    boosted: Boolean
  }

  type PaginatedPost {
    docs: [Post]
    ${PaginationFields}
  }

  input PostInput {
    text: String
    twitchClip: String
    file: FileInput
    files: [FileInput]
    giphy: GiphyInput
    kindId: String
    kind: String
    verified: Boolean
    platform: String
    mentions: [MentionInput]
    scheduledFeedId: String
    shared: Boolean
    sharedFeed: String
    sharedTimeline: String
    sharedTimelineable: String
    sharedPostUser: String
    crossPosts: [String]
    associatedGame: String
    youtubeVideo: String
    scheduledAt: String
    media: [FileInput]
  }

  input UpdatePostInput {
    parentFeedId: String
    text: String
    mentions: [MentionInput]
    media: [FileInput]
    associatedGame: String
    youtubeVideo: YoutubeVideoInput
    giphy: GiphyInput
    twitchClip: String
  }

  type ReportedBy {
    reportedBy: String
    description: String
    createdAt: Date
  }
`;
