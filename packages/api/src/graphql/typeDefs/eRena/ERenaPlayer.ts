import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaPlayerTypeDef = gql`
  extend type ERenaTournament {
    players: [ERenaPlayer]
  }

  extend type Query {
    getPlayer(id: ID!): ERenaPlayer @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createPlayer(teamId: ID!, body: PlayerArgs!): ERenaPlayer @auth(requires: "${UserRoles.USER}")
    updatePlayer(id: ID!, body: PlayerUpdateArgs!): ERenaPlayer @auth(requires: "${UserRoles.USER}")
    deletePlayer(id: ID!): ERenaPlayer @auth(requires: "${UserRoles.USER}")
  }

  type ERenaPlayer {
    _id: ID
    name: String
    isActive: Boolean
    team: ERenaTeam
    tournament: ERenaTournament
    type: ERenaType
    user: User
    createdAt: Date
    updatedAt: Date
    stats: PlayerStats
    airtableId: String @deprecated(reason: "")
    airtableBaseId: String @deprecated(reason: "")
    rank: Int @deprecated(reason: "Not stored in DB. Used during leaderboard generation")
    score: Int
    lastAirtableModificationTime: String @deprecated(reason: "")
  }

  type PlayerStats {
    kills: Int
    value: Int
    name: String
  }

  input PlayerArgs {
    name: String
    isActive: Boolean
    team: String
    tournament: String
    type: String
    user: String
    createdAt: Date
    updatedAt: Date
    stats: PlayerStatsInput
    airtableId: String
    airtableBaseId: String
    rank: Int
    score: Int
    lastAirtableModificationTime: String
  }

  input PlayerStatsInput {
    kills: Int
    value: Int
    name: String
  }

  input PlayerUpdateArgs {
    name: String,
    isActive: Boolean
  }
`;
