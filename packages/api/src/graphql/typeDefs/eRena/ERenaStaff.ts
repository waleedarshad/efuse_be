import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaStaffTypeDef = gql`
  extend type ERenaTournament {
    staff: [ERenaStaff]
  }

  extend type Query {
    getStaff(id: ID!): ERenaStaff @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createStaff(body: ERenaStaffInput!,tournamentIdOrSlug: String!): ERenaStaff @auth(requires: "${UserRoles.USER}")
    updateStaff(id: ID!,body: ERenaStaffUpdateInput!): ERenaStaff @auth(requires: "${UserRoles.USER}")
    deleteStaff(id: ID!): ERenaStaff @auth(requires: "${UserRoles.USER}")
  }

  enum ERenaRoles {
    owner
    administrator
  }

  type ERenaStaff {
    _id: ID
    user: User
    tournament: ERenaTournament
    role: ERenaRoles
    createdAt: Date
    updatedAt: Date
  }

  input ERenaStaffInput {
    user: String
    tournament: String
    role: ERenaRoles
  }

  input ERenaStaffUpdateInput {
    _id: String
    user: String
    tournament: String
    role: ERenaRoles
  }
`;
