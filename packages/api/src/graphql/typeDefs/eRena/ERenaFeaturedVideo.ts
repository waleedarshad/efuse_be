import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaFeaturedVideosTypeDef = gql`
  extend type Query {
    getERenaFeaturedVideos: [ERenaFeaturedVideo] @auth(requires: "${UserRoles.NONE}")
  }

  type ERenaFeaturedVideo {
    _id: ID!
    title: String
    description: String
    youtubeLink: String
    order: Int
    createdAt: Date
    updatedAT: Date
  }
`;
