import { ERenaFeaturedVideosTypeDef } from "./ERenaFeaturedVideo";
import { ERenaTournamentTypeDef } from "./ERenaTournament";
import { ERenaBracketTypeDef } from "./ERenaBracket";
import { ERenaMatchTypeDef } from "./ERenaMatch";
import { ERenaPlayerTypeDef } from "./ERenaPlayer";
import { ERenaScoreTypeDef } from "./ERenaScore";
import { ERenaTeamTypeDef } from "./ERenaTeam";
import { ERenaRoundTypeDef } from "./ERenaRound";
import { ERenaStaffTypeDef } from "./ERenaStaff";
import { ERenaTypeDef } from "./ERenaType";

export const ERenaTypeDefs = [
  ERenaTournamentTypeDef,
  ERenaFeaturedVideosTypeDef,
  ERenaBracketTypeDef,
  ERenaMatchTypeDef,
  ERenaPlayerTypeDef,
  ERenaScoreTypeDef,
  ERenaTeamTypeDef,
  ERenaRoundTypeDef,
  ERenaStaffTypeDef,
  ERenaTypeDef
];
