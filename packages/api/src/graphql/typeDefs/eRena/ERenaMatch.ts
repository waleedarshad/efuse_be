import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaMatchTypeDef = gql`
  extend type ERenaTournament {
    matches(round: String): [ERenaMatch]
  }

  extend type Query {
    getMatch(id: ID!): ERenaMatch @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createMatch(tournamentIdOrSlug: String, body: ERenaMatchInput!): ERenaMatch @auth(requires: "${UserRoles.USER}")
    updateMatch(id: ID!,body: UpdateERenaMatchInput!): UpdateERenaMatch @auth(requires: "${UserRoles.USER}")
    saveScreenShot(id: ID!,body: screenshotsInputs): ERenaMatch @auth(requires: "${UserRoles.USER}")
  }

  type UpdateERenaMatch{
    currentMatch: ERenaMatch
    nextMatch: ERenaMatch
  }

  type ERenaMatch {
    _id: ID
    matchDate: Date
    winner: ERenaTeam
    matchNumber: Int
    tournament: ERenaTournament
    teams: [ERenaTeam]
    createdAt: Date
    updatedAt: Date
    teamOneImage: File
    teamTwoImage: File
    teamOneScore: Int @deprecated(reason: "")
    teamTwoScore: Int @deprecated(reason: "")
    addTeam: String @deprecated(reason: "Not in database")
    addTeamIndex: Int @deprecated(reason: "Not in database")
  }

  input ERenaMatchInput {
    matchDate: Date
    team1: String
    team2: String
  }

  input UpdateERenaMatchInput {
    matchDate: Date
    winner: String
    teams: [teamsInput]
  }

  input teamsInput {
    team: String
  }

  input screenshotsInputs {
    teamOneImage: teamImageFile
    teamTwoImage: teamImageFile
  }

  input teamImageFile {
    filename: String
    contentType: String
    url: String
  }
`;
