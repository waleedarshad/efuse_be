import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaBracketTypeDef = gql`
  extend type ERenaTournament {
    bracket: ERenaBracket
  }

  extend type Query {
    getBracket(id: ID!): ERenaBracket @auth(requires: "${UserRoles.NONE}")
  }

  type ERenaBracket {
    totalTeams: Int
    totalRounds: Int
    totalMatches: Int
    tournament: ERenaTournament
    rounds: [ERenaRound]
  }
`;
