import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaRoundTypeDef = gql`
  extend type ERenaTournament {
    rounds: [ERenaRound]
  }

  extend type Query {
    getRound(id: ID!): ERenaRound @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createRound(body: ERenaRoundInput!,tournamentIdOrSlug: String!): ERenaRound @auth(requires: "${UserRoles.USER}")
    updateRound(id: ID!,body: ERenaRoundInput!): ERenaRound @auth(requires: "${UserRoles.USER}")
  }

  type ERenaRound {
    _id: ID
    roundDate: Date
    roundTitle: String
    roundNumber: String
    tournament: ERenaTournament
    matches: [ERenaMatch]
    createdAt: Date
    updatedAt: Date
  }

  input ERenaRoundInput {
    roundDate: Date
    roundTitle: String
    roundNumber: String
    matches: [String]
  }

`;
