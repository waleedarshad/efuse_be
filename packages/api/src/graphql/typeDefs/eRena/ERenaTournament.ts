import { gql } from "apollo-server-express";

import { UserRoles } from "../../enums";
import { PaginationFields } from "../reuseable-fields";
import { EnumUtil } from "../../util";
import { ERenaTournamentOwnerKinds } from "../../../backend/interfaces/erena/erena-enums";

export const ERenaTournamentTypeDef = gql`
  extend type Query {
    getERenaTournament(tournamentIdOrSlug: String): ERenaTournament @auth(requires: "${UserRoles.NONE}")
    getFeaturedEvent: ERenaTournament @auth(requires: "${UserRoles.NONE}")
    listERenaTournaments(page: Int,
    limit: Int,
    status: [String],
    sortBy: String,
    sortDirection: Int,
    startDateMin: Float,
    startDateMax: Float,
    endDateMin: Float,
    endDateMax: Float): PaginatedErenaTournament @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createTournament(body: ERenaTournamentInputs): ERenaTournament @auth(requires: "${UserRoles.USER}")
    updateTournament(tournamentIdOrSlug: String, files: ERenaTournamentFileUploaderInputs, body: ERenaTournamentInputs): ERenaTournament @auth(requires: "${
      UserRoles.USER
    }")
    deleteTournament(tournamentIdOrSlug: String): ERenaTournament @auth(requires: "${UserRoles.USER}")
    updateTournamentAd(tournamentIdOrSlug: String, index:Int, link:String,imageURL: String): ERenaTournament @auth(requires: "${
      UserRoles.USER
    }")
    removeTournamentAd(tournamentIdOrSlug: String, index: Int): ERenaTournament @auth(requires: "${UserRoles.USER}")
  }

  union TournamentOwner = User | Organization

  enum ERenaTournamentStatus {
    scheduled
    active
    finished
    deleted
  }

  type PaginatedErenaTournament {
    docs: [ERenaTournament],
    ${PaginationFields}
  }

  type ERenaTournament {
    _id: ID!
    backgroundImageUrl: String
    bracketType: String
    brandLogoUrl: String
    brandName: String
    challongeLeaderboard: String
    createdAt: Date
    currentRound: Int
    endDatetime: Date
    extensionLinkoutUrl: String
    extensionNamespace: String
    extensionPointer: Boolean
    gameSelection: String
    hideLeaderboard: Boolean
    imageLink1: String
    imageLink2: String
    imageLink3: String
    imageLink4: String
    imageLink5: String
    imageLink6: String
    imageUrl1: String
    imageUrl2: String
    imageUrl3: String
    imageUrl4: String
    imageUrl5: String
    imageUrl6: String
    isFinal: Boolean
    landingPageUrl: String
    logoUrl: String
    logoURL: String
    owner: TournamentOwner
    ownerType: String
    playersPerTeam: Int
    primaryColor: String
    rulesMarkdown: String
    secondaryColor: String
    slug: String
    startDatetime: Date
    status: ERenaTournamentStatus
    totalRounds: Int
    totalTeams: Int
    tournamentDescription: String
    tournamentId: String
    tournamentName: String
    twitchChannel: String
    type: String
    updatedAt: Date
    websiteUrl: String
    youtubeChannel: String
    opportunity: Opportunity
  }

  type ERenaTeamPopulated {
    players: [ERenaPlayer]
    teamScores: [ERenaScore]
  }

  input ERenaTournamentFileUploaderInputs {
    image: [FileInput]
    brandLogoUrl: [FileInput]
    backgroundImageUrl: [FileInput]
  }

  input ERenaTournamentInputs {
    backgroundImageUrl: String
    bracketType: String
    brandLogoUrl: String
    brandName: String
    challongeLeaderboard: String
    createdAt: Date
    currentRound: Int
    endDatetime: Date
    extensionLinkoutUrl: String
    extensionNamespace: String
    extensionPointer: Boolean
    gameSelection: String
    hideLeaderboard: Boolean
    imageLink1: String
    imageLink2: String
    imageLink3: String
    imageLink4: String
    imageLink5: String
    imageLink6: String
    imageUrl1: String
    imageUrl2: String
    imageUrl3: String
    imageUrl4: String
    imageUrl5: String
    imageUrl6: String
    isFinal: Boolean
    landingPageUrl: String
    logoUrl: String
    logoURL: String
    owner: String
    ownerKind: String
    playersPerTeam: Int
    primaryColor: String
    requirements: [ID]
    rulesMarkdown: String
    secondaryColor: String
    slug: String
    startDatetime: Date
    status: String
    totalRounds: Int
    totalTeams: Int
    tournamentDescription: String
    tournamentId: String
    tournamentName: String
    twitchChannel: String
    type: String
    updatedAt: Date
    websiteUrl: String
    youtubeChannel: String
    ownerType: String
    opportunity: String
  }

  enum TournamentOwnerType {
    ${EnumUtil.convertEnumValuesToString(ERenaTournamentOwnerKinds)}
  }
`;

// TODO: add these fields
// opportunity: Types.ObjectId
// game?: Types.ObjectId | String
