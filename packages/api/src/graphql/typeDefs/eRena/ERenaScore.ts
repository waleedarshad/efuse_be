import { gql } from "apollo-server-express";
import { UserRoles } from "../../enums";

export const ERenaScoreTypeDef = gql`
  extend type ERenaTournament {
    scores: [ERenaScore]
  }

  extend type Query {
    getScore(id: ID!): ERenaScore @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createScore(tournamentIdOrSlug: String!, body: ERenaScoreInput!): ERenaScore @auth(requires: "${UserRoles.USER}")
    updateScore(id: ID!, body: ERenaScoreInputValue!): ERenaScore @auth(requires: "${UserRoles.USER}")
    deleteScore(id: ID!): ERenaScore @auth(requires: "${UserRoles.USER}")
    bulkCreateScores(tournamentIdOrSlug: String!, body: [ERenaScoreInput]!): [ERenaScore] @auth(requires: "${UserRoles.USER}")
    bulkUpdateScores(tournamentIdOrSlug: String!, body: [ERenaScoreInput]!): [ERenaScore] @auth(requires: "${UserRoles.USER}")
  }

  enum ERenaScoringKinds {
    bracket
    pointRace
  }

  enum ERenaScoreOwnerKinds {
    player
    team
  }

  union ERenaScoreOwner = ERenaPlayer | ERenaTeam

  type ERenaScore {
    _id: ID
    erenaKind: ERenaScoringKinds
    ownerKind: ERenaScoreOwnerKinds
    ownerId: String
    match: ERenaMatch
    tournament: ERenaTournament
    score: Float
  }

  input ERenaScoreInput {
    erenaKind: ERenaScoringKinds
    ownerKind: ERenaScoreOwnerKinds
    ownerId: String
    match: String
    tournament: String
    score: Float
  }

  input ERenaScoreInputValue {
    score: Float
  }
`;
