import { gql } from "apollo-server-express";

import { UserRoles } from "../../enums";

export const ERenaTeamTypeDef = gql`
  extend type ERenaTournament {
    teams: [ERenaTeam]
  }

  extend type Query {
    getErenaTeam(id: ID!): ERenaTeam @auth(requires: "${UserRoles.USER}")
  }

  extend type Mutation {
    createErenaTeam(tournamentIdOrSlug: String!, body: ERenaTeamInput!): ERenaTeam @auth(requires: "${UserRoles.USER}")
    updateErenaTeam(id: ID!, body: ERenaTeamInputAttributes!): ERenaTeam @auth(requires: "${UserRoles.USER}")
    deleteErenaTeam(id: ID!): ERenaTeam @auth(requires: "${UserRoles.USER}")
  }

  type ERenaTeam {
    _id: ID
    players: [ERenaPlayer]
    teamScore: Float
    summedPlayerScore: Float
    name: String
    isActive: Boolean
    tournament: ERenaTournament
    type: ERenaType
    createdAt: Date
    updatedAt: Date
    rank: Int @deprecated(reason: "not stored in database")
    teamName: String @deprecated(reason: "This is only used for Twitch Plugin and is not stored in the database")
    airtableBaseId: String @deprecated(reason: "")
    airtableId: String @deprecated(reason: "")
    lastAirtableModificationTime: String @deprecated(reason: "")
  }

  input ERenaTeamInput {
    name: String
    isActive: Boolean
    tournament: String
    type: String
    rank: Int
    teamName: String
    airtableBaseId: String
    airtableId: String
    lastAirtableModificationTime: String
  }

  input ERenaTeamInputAttributes {
    name: String
    type: String
  }
`;
