import { gql } from "apollo-server-express";

export const ERenaTypeDef = gql`
  enum ERenaType {
    WHITE_LABEL
    EFUSE
  }
`;
