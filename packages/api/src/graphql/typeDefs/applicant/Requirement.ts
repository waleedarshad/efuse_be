import { gql } from "apollo-server-express";

import { UserRoles } from "../../enums";

export const RequirementTypeDef = gql`
  type Requirement {
    name: String
    description: String
    isActive: Boolean
    target: String
    property: String
    custom: Boolean
  }

  extend type ERenaTournament {
    requirements: [Requirement]
  }

  extend type Opportunity {
    requirements: [Requirement]
  }

  extend type Query {
    GetRequirements: [Requirement] @auth(requires: "${UserRoles.USER}")
  }
`;
