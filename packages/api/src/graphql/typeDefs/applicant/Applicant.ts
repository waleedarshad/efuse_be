import { gql } from "apollo-server-express";

import { ApplicantLimitEnum, ApplicantStatusEnum } from "../../../lib/applicants/applicant.enum";
import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

const candidateFields = `
  index: Int
  question: String
`;

export const ApplicantTypeDef = gql`
  union ApplicantEntity = Opportunity | ERenaTournament

  type CandidateQuestion {
    ${candidateFields}
    media: File
  }

  input CandidateQuestionParams {
    ${candidateFields}
    media: FileInput
  }

  type Applicant {
    _id: ID!
    user: User
    entity: ApplicantEntity
    entityType: String
    status: String
    candidateQuestions: [CandidateQuestion]
    favorite: Boolean
  }

  input ApplicantApplyBody {
    entity: String!
    entityType: String!
    candidateQuestions: [CandidateQuestionParams]
  }

  type PaginatedApplicant {
    docs: [Applicant]
    ${PaginationFields}
  }

  type ApplicantRequirementsResponse {
    canApply: Boolean
    requirementsMet: [Requirement]
    requirementsNotMet: [Requirement]
  }

  extend type ERenaTournament {
    paginatedApplicants(page: Int = 1, limit: Int = ${ApplicantLimitEnum.DEFAULT}, status: String = ${ApplicantStatusEnum.NO_ACTION} ): PaginatedApplicant
    ableToApply: ApplicantRequirementsResponse
  }

  extend type Opportunity {
    paginatedApplicants(page: Int = 1, limit: Int = ${ApplicantLimitEnum.DEFAULT}, status: String = ${ApplicantStatusEnum.NO_ACTION}): PaginatedApplicant
    ableToApply: ApplicantRequirementsResponse
  }

  extend type Mutation {
    ApplicantApply(body: ApplicantApplyBody!): Applicant @auth(requires: "${UserRoles.USER}")
    updateApplicantStatus( applicantId:ID!, opportunityId:ID!, sendEmail: Boolean, status: String): Applicant @auth(requires: "${UserRoles.USER}")
  }

  extend type Query {
    ValidateApplicantRequirements(entity: String!, entityType: String!): ApplicantRequirementsResponse @auth(requires: "${UserRoles.USER}")
  }
`;
