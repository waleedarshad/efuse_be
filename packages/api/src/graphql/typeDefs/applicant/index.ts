import { ApplicantTypeDef } from "./Applicant";
import { RequirementTypeDef } from "./Requirement";

export const ApplicantsTypeDefs = [ApplicantTypeDef, RequirementTypeDef];
