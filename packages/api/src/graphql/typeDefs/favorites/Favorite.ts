import { gql } from "apollo-server-express";
import { FavoriteOwnerType, FavoriteRelatedModel } from "@efuse/entities";
import { FavoriteOwnerTypeDef, FavoriteRelatedDocTypeDef, UserRoles } from "../../enums";
import { PaginationFields } from "../reuseable-fields";

export const FavoriteTypeDef = gql`
  extend type Query {
    getFavorite(owner: String!, relatedDoc: String!): Favorite @auth(requires: "${UserRoles.NONE}")
    getFavoriteById(favoriteId: String!): Favorite @auth(requires: "${UserRoles.NONE}")
    getFavoritesByOwner(owner: String!): [Favorite] @auth(requires: "${UserRoles.NONE}")
    getPaginatedFavoritesByOwner(owner: String!, page: Int, limit: Int): PaginatedFavorite @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createFavorite(favorite: CreateFavorite!): Favorite @auth(requires: "${UserRoles.NONE}")
    deleteFavorite(favoriteId: String!): Boolean @auth(requires: "${UserRoles.NONE}")
  }

  union FavoriteOwner = ${FavoriteOwnerTypeDef.ORGANIZATION} | ${FavoriteOwnerTypeDef.USER}

  enum FavoriteOwnerType {
    ${FavoriteOwnerType.USERS}
    ${FavoriteOwnerType.ORGANIZATIONS}
  }

  union FavoriteRelatedDoc = ${FavoriteRelatedDocTypeDef.APPLICANT} | ${FavoriteRelatedDocTypeDef.RECRUITMENT_PROFILE}

  enum FavoriteRelatedModel {
    ${FavoriteRelatedModel.APPLICANTS}
    ${FavoriteRelatedModel.RECRUITMENT_PROFILES}
  }

  type PaginatedFavorite {
    docs: [Favorite]
    ${PaginationFields}
  }

  input CreateFavorite {
    creator: String!
    relatedDoc: String!
    relatedModel: FavoriteRelatedModel!
    owner: String!
    ownerType: FavoriteOwnerType!
  }

  type Favorite {
    _id: ID!
    createdAt: Date
    creator: User
    owner: FavoriteOwner
    ownerType: FavoriteOwnerType
    relatedDoc: FavoriteRelatedDoc,
    relatedModel: FavoriteRelatedModel
    updatedAt: Date
  }
`;
