import { gql } from "apollo-server-express";

import { UserRoles } from "../enums";
import { TalentCardsEnum } from "../../lib/users/user.enum";

export const UserTypeDef = gql`
  extend type Query {
    getUserById(id: ID!): User @auth(requires: "${UserRoles.NONE}")
    getAuthenticatedUser: User @auth(requires: "${UserRoles.USER}")
    GetTalentCards(limit: Int = ${TalentCardsEnum.DEFAULT_SAMPLE_SIZE}): [User] @auth(requires: "${UserRoles.USER}")
  }

  extend type Mutation {
    updateUser(body: UserUpdateArgs!): User @auth(requires: "${UserRoles.USER}")
    updatePassword(body: UserPasswordInput): SuccessResponse @auth(requires: "${UserRoles.USER}")
    banUser(idOrUsername: String): SuccessResponse @auth(requires: "${UserRoles.USER}")
    unbanUser(idOrUsername: String): SuccessResponse @auth(requires: "${UserRoles.USER}")
  }

  type SuccessResponse {
    success: Boolean
  }

  input UserUpdateArgs {
    email: String
    username: String
    name: String
    address: String
    dateOfBirth: Date
    bio: String
    gender: String
  }

  input UserPasswordInput {
    currentPassword: String!
    newPassword: String!
  }

  type UserEducationExperience {
    level: Int
    subTitle: String
    school: String
    degree: String
    fieldofstudy: String
    grade: String
    startDate: Date
    endDate: Date
    present: Boolean
    image: File
    description: String
  }

  type Pathway {
    _id: String!
    description: String
    active: Boolean
  }

  type UserStats {
    user: String
    hypeScore: Int
    hypeScoreRanking: Int
    hypeScorePercentile: Int
    postViews: Int
    totalEngagements: Int
    totalHypes: Int
  }

  """
  The User type
  """
  type User {
    "The globally unique database identifier"
    _id: ID! @auth(requires: "${UserRoles.NONE}")

    "The full name for the user"
    name: String @auth(requires: "${UserRoles.NONE}")

    "The user's bio"
    bio: String @auth(requires: "${UserRoles.NONE}")

    "Does the user have verified status"
    verified: Boolean @auth(requires: "${UserRoles.NONE}")

    "The User's profile picture"
    profilePicture: File @auth(requires: "${UserRoles.NONE}")

    "The User's profile's Header Image"
    headerImage: File @auth(requires: "${UserRoles.NONE}")

    "The User's username"
    username: String @auth(requires: "${UserRoles.NONE}")

    "When was the User account created"
    createdAt: Date @auth(requires: "${UserRoles.NONE}")

    "The User's portfolio URL"
    url: String @auth(requires: "${UserRoles.NONE}")

    "The Search score of the user"
    score: Float @auth(requires: "${UserRoles.NONE}")

    "The user's current eFuse Token balance"
    tokenBalance: Float

    "The user's email id"
    email: String

    "The user's gender"
    gender: String

    "The user's address"
    address: String

    "The user's dateOfBirth"
    dateOfBirth: Date

    "User Profile Imagr"
    profileImage: String @auth(requires: "${UserRoles.NONE}")

    "User Streak"
    streak: Int @auth(requires: "${UserRoles.NONE}")

    "The User Online status"
    online: Boolean @auth(requires: "${UserRoles.NONE}")

    "The user's badges"
    badges: [UserBadge]

    "The User's education experience"
    educationExperience: [UserEducationExperience]

    twitch: TwitchAccountInfo

    "User's selected pathway"
    pathway: Pathway

    "User's engagement stats"
    stats: UserStats

    "User's portfolio progress"
    portfolioProgress: Int
  }
`;
