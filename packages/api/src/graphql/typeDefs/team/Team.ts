import { gql } from "apollo-server-express";

import { UserRoles } from "../../enums";
import { paginateTypeDef } from "../reuseable-fields";

const CreateTeamCommonArgs = `
  description: String
  image: FileInput
  name: String!
  owner: String!
  ownerType: OwnerType!
`;

export const TeamTypeDef = gql`
  extend type Organization {
    teams: [Team]
  }

  extend type Query {
    getPaginatedTeams(page: Int, limit: Int, owner: String): PaginatedTeams @auth(requires: "${UserRoles.NONE}")
    getTeamById(teamId: String!): Team @auth(requires: "${UserRoles.NONE}")
    getTeamsByOwner(owner: String!): [Team] @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    createTeam(team: CreateTeamArgs!, userIds: [String!]): Team @auth(requires: "${UserRoles.USER}")
    createTeamForLeague(team: CreateTeamForLeagueArgs!, leagueId: String!): Team @auth(requires: "${UserRoles.USER}")
    deleteTeam(teamId: String!): Boolean @auth(requires: "${UserRoles.USER}")
    updateTeam(teamId: String!, teamUpdates: UpdateTeamArgs!): Team @auth(requires: "${UserRoles.USER}")
  }

  input CreateTeamForLeagueArgs {
    game: String
    ${CreateTeamCommonArgs}
  }

  input CreateTeamArgs {
    game: String!
    ${CreateTeamCommonArgs}
  }

  input UpdateTeamArgs {
    description: String
    game: String
    image: FileInput
    name: String
  }

  type PaginatedTeams {
    ${paginateTypeDef("Team")}
  }

  type Team {
    _id: ID!
    description: String
    game: Game
    image: File
    name: String
    owner: Owner
    ownerType: OwnerType
    createdAt: Date
    updatedAt: Date
  }
`;
