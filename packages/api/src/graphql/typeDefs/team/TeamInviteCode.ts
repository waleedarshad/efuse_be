import { gql } from "apollo-server-express";

export const TeamInviteCodeTypeDef = gql`
  extend type Team {
    inviteCode: String
  }
`;
