import { gql } from "apollo-server-express";
import { TeamMemberStatus } from "@efuse/entities";
import { PaginationFields } from "../reuseable-fields";
import { UserRoles } from "../../enums";

export const TeamMemberTypeDef = gql`
  extend type Team {
    members: [TeamMember]
  }

  extend type Query {
    getTeamMember(userId: String!, teamId: String!): TeamMember @auth(requires: "${UserRoles.NONE}")
    getTeamMemberById(teamMemberId: String!): TeamMember @auth(requires: "${UserRoles.NONE}")
    getAllTeamMembers(teamId: String!): [TeamMember] @auth(requires: "${UserRoles.NONE}")
    getTeamMembersByStatus(teamId: String!, status: TeamMemberStatus): [TeamMember] @auth(requires: "${UserRoles.NONE}")
    getPaginatedTeamMembers(teamId: String!, status: TeamMemberStatus, page: Int, limit: Int ): PaginatedTeamMembers @auth(requires: "${UserRoles.NONE}")
  }

  extend type Mutation {
    updateTeamMember(teamMemberId: String!, teamMemberUpdate: UpdateTeamMemberArgs!): TeamMember @auth(requires: "${UserRoles.USER}")
    addUsersToTeam(teamId: String!, userIds: [String]!, role: String): [TeamMember] @auth(requires: "${UserRoles.USER}")
    deleteTeamMember(teamMemberId: String!): TeamMember @auth(requires: "${UserRoles.USER}")
  }

  input UpdateTeamMemberArgs {
    status: TeamMemberStatus
    role: String
  }

  enum TeamMemberStatus {
    ${TeamMemberStatus.ACTIVE}
    ${TeamMemberStatus.INACTIVE}
    ${TeamMemberStatus.PENDING}
  }

  type PaginatedTeamMembers {
    docs: [TeamMember],
    ${PaginationFields}
  }

  type TeamMember {
    _id: ID!
    team: Team
    user: User
    role: String
    status: TeamMemberStatus
    createdAt: Date
    updatedAt: Date
  }
`;
