import { gql } from "apollo-server-express";

const YoutubeVideoFields = `
  _id: ID!
  externalId: String
  title: String
  description: String
  playlistId: String
  videoId: String
  videoPublishedAt: Date
  owner: ID
  ownerKind: String
  videoUrl: String
`;

const ThumbnailFields = `
  url: String
  width: Int
  height: Int
`;

export const YoutubeVideoTypeDef = gql`
  type YoutubeVideoThumbnailFields {
    ${ThumbnailFields}
  }

 

  input YoutubeVideoInput {
    ${YoutubeVideoFields}
    thumbnails: YoutubeVideoThumbnailsInput
  }

  input YoutubeVideoThumbnailFieldsInput {
    ${ThumbnailFields}
  }

  input YoutubeVideoThumbnailsInput {
    default: YoutubeVideoThumbnailFieldsInput
    medium: YoutubeVideoThumbnailFieldsInput
    high: YoutubeVideoThumbnailFieldsInput
    standard: YoutubeVideoThumbnailFieldsInput
    maxres: YoutubeVideoThumbnailFieldsInput
  }


  extend type Post {
    youtubeVideo: YoutubeVideo
  }
`;
