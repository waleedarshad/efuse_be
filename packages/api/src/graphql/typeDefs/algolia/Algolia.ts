import { gql } from "apollo-server-express";

import { UserRoles } from "../../enums";

const SearchResultFields = `
  exhaustiveFacetsCount: Boolean
  exhaustiveNbHits: Boolean
  facets: JSONObject
  facets_stats: JSONObject
  hitsPerPage: Int
  index: String
  nbHits: Int
  nbPages: Int
  page: Int
  params: String
  processingTimeMS: Int
  query: String
  renderingContent:JSONObject
`;

export const AlgoliaTypeDef = gql`
  interface SearchResultFields {
    ${SearchResultFields}
  }

  input AlgoliaParamsInput {
    analytics: Boolean
    distinct: Boolean
    enablePersonalization: Boolean
    facets: [String]
    facetFilters: [[String]]
    filters: String
    highlightPostTag: String
    highlightPreTag: String
    hitsPerPage: Int
    maxValuesPerFacet: Int
    query: String
    tagFilters: String
    userToken: String
    page: Int
    attributesToRetrieve: [String]
    attributesToHighlight: [String]
    attributesToSnippet: [String]
    clickAnalytics: Boolean
  }

  input AlgoliaSearchInput {
    indexName: String!
    params: AlgoliaParamsInput
  }

  type OrganizationAlgoliaSearchResponse implements SearchResultFields {
    hits: [Organization]
    ${SearchResultFields}
  }

  type OrganizationAlgoliaSearchResult {
    results: [OrganizationAlgoliaSearchResponse]
  }

  extend type Organization {
    _highlightResult: JSONObject
  }

  extend type Query {
    OrganizationAlgoliaSearch(requests: [AlgoliaSearchInput]!): OrganizationAlgoliaSearchResult @auth(requires: "${UserRoles.NONE}")
  }
`;
