import { gql } from "apollo-server-express";

export const UserBadgeTypeDef = gql`
  type UserBadge {
    user: User
    badge: Badge
  }
`;
