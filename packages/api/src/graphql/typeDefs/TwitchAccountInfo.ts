import { gql } from "apollo-server-express";

export const TwitchAccountInfoTypeDef = gql`
  type TwitchAccountInfo {
    owner: String

    ownerKind: String

    kind: String

    verified: Boolean

    displayName: String

    username: String

    accountId: String

    link: String

    description: String

    followers: Int

    profileImage: String

    accountEmail: String

    viewCount: Int

    accountType: String

    broadcasterType: String

    sendStartStreamMessage: Boolean

    webhookSubscription: TwitchWebhookSubscription

    stream: TwitchStream
  }

  type TwitchWebhookSubscription {
    isActive: Boolean

    callback: String

    expiresAt: String
  }

  type TwitchStream {
    isLive: Boolean

    title: String

    viewerCount: Int

    startedAt: Date

    endedAt: Date
  }
`;
