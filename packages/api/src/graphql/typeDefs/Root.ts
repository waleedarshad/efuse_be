import { gql } from "apollo-server-express";
import { UserRoles } from "../enums";

// This is the root TypeDef that all from here will extend
// https://stackoverflow.com/questions/55601091/apollo-server-graphql-error-there-can-be-only-one-type-named-query

// By default ALL queries and mutations will require user auth
// If you wish to override this apply the `auth` directive on the individual query or mutator
export const RootTypeDef = gql`
  directive @auth(requires: String = ${UserRoles.USER}) on OBJECT | FIELD_DEFINITION

  scalar Date
  scalar JSON
  scalar JSONObject
`;
