import { gql } from "apollo-server-express";
import { UserRoles } from "../enums";

export const LivestreamTypeDef = gql`
  extend type Query {
    getLivestreams: [Livestream] @auth(requires: "${UserRoles.NONE}")
  }

  type LivestreamGame {
    id: Int
    name: String
    box_art_url: String
  }

  type LivestreamData {
    _id: String
    user: User
    platform: String
    startedAt: Date
    endedAt: Date
    streamTitle: String
    viewers: Int
    views: Int
    thumbnailUrl: String
    twitchUserAvatar: String
    game: LivestreamGame
  }

  type Livestream {
    type: String
    data: LivestreamData
  }
`;
