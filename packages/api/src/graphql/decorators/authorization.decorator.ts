import { AuthChecker } from "type-graphql";

import { IGraphQLContext } from "../../backend/interfaces";

export const authorization: AuthChecker<IGraphQLContext> = ({ context: { user } }, roles): boolean => {
  // When user object is not there
  if (!user) {
    return false;
  }

  // When no roles are passed
  if (roles.length === 0) {
    // Make sure user object exists
    return user !== undefined;
  }

  // Make sure user has any of the passed roles
  return user.roles.some((role) => roles.includes(role));
};
