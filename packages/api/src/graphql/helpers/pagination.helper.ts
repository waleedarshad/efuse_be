export class PaginationHelper {
  /**
   * @summary Validate Pagination params such as limit & page
   *
   * @param {number} pageParam
   * @param {number} limitParam
   * @param {number} [defaultLimit=10]
   * @param {number} [maxLimit=25]
   *
   * @returns {page: number, limit: number}
   */
  validatePaginationParams = (
    pageParam: number,
    limitParam: number,
    defaultLimit = 10,
    maxLimit = 25
  ): { page: number; limit: number } => {
    // Validate Limit Param
    let limit = Number(limitParam);

    if (limitParam < 1 || Number.isNaN(limit)) {
      limit = defaultLimit;
    } else if (limitParam > maxLimit) {
      limit = maxLimit;
    }

    // Validate Page Param
    let page = Number(pageParam);

    if (pageParam < 1 || Number.isNaN(page)) {
      page = 1;
    }

    return { page, limit };
  };
}
