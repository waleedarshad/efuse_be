import { Resolver, Query, Authorized, Args, Mutation, Ctx, Arg } from "type-graphql";
import { Hype, HypePagination } from "../../../backend/models/hype/hype.model";
import { HypePaginationLimitEnum, HypeEnum } from "@efuse/entities";
import { PaginationHelper } from "../../../graphql/helpers/pagination.helper";
import { FeedService } from "../../../lib/feeds/feed.service";
import { HypeService } from "../../../lib/hype/hype.service";
import { HypedUser } from "../../../backend/models/hype/hype-user.model";
import { HypeParams, HypeFeedParams, HypeCommentParams, HypeCommentMutationParams } from "./hype-input";
import { HypeResponse } from "../../../backend/models/hype/hype-response.model";
import { IGraphQLContext } from "../../../backend/interfaces";
import { HypeActionsService } from "../../../lib/hype/hype-actions.service";
import { CommentService } from "../../../lib/comments/comment.service";

@Resolver((_of) => Hype)
export class HypedResolver {
  private $paginationHelper = new PaginationHelper();
  private $feedService = new FeedService();
  private $hypeService = new HypeService();
  private $commentService = new CommentService();
  private $hypeActionsService = new HypeActionsService();

  /**
   * @authenticated protected by JWT
   * @kind Query
   * @param feedId string
   * @param _page string
   * @param _limit string
   * @returns {PaginateResult<Hype>}
   */
  @Authorized()
  @Query((_returns) => HypedUser)
  async getFeedHypedUsers(
    @Args() { feedId, page, limit = HypePaginationLimitEnum.DEFAULT }: HypeParams
  ): Promise<HypedUser> {
    await this.$feedService.validateFeed(feedId);
    const hypeResult = await this.getHypes(feedId, page, limit, HypeEnum.FEED);

    return hypeResult;
  }

  /**
   * @authenticated protected by JWT
   * @kind Query
   * @param feedId string
   * @param _page string
   * @param _limit string
   * @returns {PaginateResult<HypedUser>}
   */
  @Authorized()
  @Query((_returns) => HypePagination)
  async getFeedHypes(
    @Args() { feedId, page, limit = HypePaginationLimitEnum.DEFAULT }: HypeParams
  ): Promise<HypePagination> {
    await this.$feedService.validateFeed(feedId);
    const hypeResult = await this.getHypes(feedId, page, limit, HypeEnum.FEED);

    return hypeResult;
  }

  @Authorized()
  @Mutation((_of) => HypeResponse)
  async hypeFeed(@Args() { feedId, hypeCount }: HypeFeedParams, @Ctx() ctx: IGraphQLContext): Promise<HypeResponse> {
    const { userId } = ctx;

    const hypeResult = await this.$hypeActionsService.create(feedId, userId, hypeCount);
    return hypeResult;
  }

  /**
   * @authenticated protected by JWT
   * @kind Query
   * @param commentId string
   * @param _page string
   * @param _limit string
   * @returns {PaginateResult<HypedUser>}
   */
  @Authorized()
  @Query((_returns) => HypedUser)
  async getCommentHypedUsers(
    @Args() { commentId, page, limit = HypePaginationLimitEnum.DEFAULT }: HypeCommentParams
  ): Promise<HypedUser> {
    await this.$commentService.validateComment(commentId);

    const hypeResult = await this.getHypes(commentId, page, limit, HypeEnum.COMMENT);

    return hypeResult;
  }

  /**
   * @authenticated protected by JWT
   * @kind Query
   * @param commentId string
   * @param _page string
   * @param _limit string
   * @returns {PaginateResult<HypedUser>}
   */
  @Authorized()
  @Query((_returns) => HypePagination)
  async getCommentHypes(
    @Args() { commentId, page, limit = HypePaginationLimitEnum.DEFAULT }: HypeCommentParams
  ): Promise<HypePagination> {
    await this.$commentService.validateComment(commentId);

    const hypeResult = await this.getHypes(commentId, page, limit, HypeEnum.COMMENT);

    return hypeResult;
  }

  @Authorized()
  @Mutation((_of) => HypeResponse)
  async HypeComment(
    @Ctx() ctx: IGraphQLContext,
    @Args() { commentId, hypeCount, platform }: HypeCommentMutationParams
  ): Promise<HypeResponse> {
    const hypeResult = await this.$hypeActionsService.createCommentHypeAndNotify(
      commentId,
      ctx.user,
      hypeCount,
      (platform = "")
    );
    return hypeResult;
  }

  @Authorized()
  @Mutation((_of) => Hype)
  async UnHypeComment(@Ctx() ctx: IGraphQLContext, @Arg("commentId") commentId: string): Promise<Hype | null> {
    const hypeResult = await this.$hypeActionsService.deleteCommentHype(commentId, ctx.user);
    return hypeResult;
  }

  private async getHypes(
    hypeItemId: string,
    page: number,
    limit: number,
    feedType = HypeEnum.FEED
  ): Promise<HypePagination> {
    const { page: pageSize, limit: limitSize } = this.$paginationHelper.validatePaginationParams(page, limit);
    const hypeResult = await this.$hypeService.getPaginatedHypes(hypeItemId, pageSize, limitSize, feedType);

    return hypeResult;
  }
}
