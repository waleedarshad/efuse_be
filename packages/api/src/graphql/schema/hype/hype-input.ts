import { ArgsType, Field, Int } from "type-graphql";
import { IsInt, Min, IsNotEmpty } from "class-validator";
@ArgsType()
export class HypeParams {
  @Field((_type) => String, { nullable: false })
  @IsNotEmpty()
  feedId!: string;

  @Field((_type) => Int, { nullable: true })
  @IsInt()
  @Min(1)
  page!: number;

  @IsInt()
  @Field((_type) => Int, { nullable: true })
  @Min(1)
  limit!: number;
}

@ArgsType()
export class HypeFeedParams {
  @Field()
  feedId!: string;

  @Field((_type) => Int)
  hypeCount!: number;
}

@ArgsType()
export class HypeCommentParams {
  @Field((_type) => String)
  @IsNotEmpty()
  commentId!: string;

  @Field((_type) => Int, { nullable: true })
  @IsInt()
  @Min(1)
  page!: number;

  @Field((_type) => Int, { nullable: true })
  @IsInt()
  @Min(1)
  limit!: number;
}

@ArgsType()
export class HypeCommentMutationParams {
  @Field((_type) => String)
  @IsNotEmpty()
  commentId!: string;

  @Field((_type) => Int)
  @IsNotEmpty()
  @IsInt()
  hypeCount!: number;

  @Field((_type) => String, { nullable: true })
  platform?: string;
}
