import { Failure } from "@efuse/contracts";
import { Resolver, FieldResolver, Root, Ctx } from "type-graphql";

import { HypeUser } from "../../../backend/models/hype/hype-user.model";
import { isUserFollowed } from "../../../backend/helpers/feed.helper";
import { IUser } from "../../../backend/interfaces";
import { IGraphQLContext } from "../../../backend/interfaces";

@Resolver((_of) => HypeUser)
export class HypeUserResolver {
  @FieldResolver((_of) => HypeUser)
  async currentUserFollows(@Root() hypeUser: IUser, @Ctx() ctx: IGraphQLContext): Promise<boolean> {
    const { userId } = ctx;

    if (!userId) {
      throw Failure.BadRequest("Missing user");
    }

    const isFollower = await isUserFollowed(hypeUser._id, userId);
    return isFollower;
  }

  @FieldResolver((_of) => HypeUser)
  async isFollowedByUser(@Root() hypeUser: IUser, @Ctx() ctx: IGraphQLContext): Promise<boolean> {
    const { userId } = ctx;

    if (!userId) {
      throw Failure.BadRequest("Missing user");
    }

    const isFollower = await isUserFollowed(userId, hypeUser._id);
    return isFollower;
  }
}
