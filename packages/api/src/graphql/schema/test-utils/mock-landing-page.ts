import { mock } from "jest-mock-extended";
import { LandingPage } from "../../../backend/models/landing-page.model";

export const landingPageMock = (): LandingPage => {
  const page = mock<LandingPage>();

  page.slug = "landing-page-test-1";
  page.title = "test landing page";
  page.subtitle = "test page subtitle";
  page.active = true;

  return page;
};
