import { ObjectType, Field } from "type-graphql";

@ObjectType("CompleteMultipartUploadResponse", { description: "Representing the response for completeMultipartUpload" })
export class CompleteMultipartUploadResponse {
  @Field((_type) => String)
  url!: string;

  @Field((_type) => String)
  fileKey!: string;
}
