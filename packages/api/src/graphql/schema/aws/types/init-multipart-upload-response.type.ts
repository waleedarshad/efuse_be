import { ObjectType, Field } from "type-graphql";

import { MultipartUploadSignedURL } from "./multipart-upload-signed-url.type";

@ObjectType("InitMultipartUploadResponse", { description: "Representing the response for initMultipartUpload" })
export class InitMultipartUploadResponse {
  @Field((_type) => String)
  fileKey!: string;

  @Field((_type) => String)
  uploadId!: string;

  @Field((_type) => [MultipartUploadSignedURL])
  signedURLs!: MultipartUploadSignedURL[];
}
