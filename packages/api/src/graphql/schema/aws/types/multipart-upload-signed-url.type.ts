import { ObjectType, Field, Int } from "type-graphql";

@ObjectType("MultipartUploadSignedURL", { description: "Representing the object for multipart Upload Signed urls" })
export class MultipartUploadSignedURL {
  @Field((_type) => Int)
  part!: number;

  @Field((_type) => String)
  url!: string;
}
