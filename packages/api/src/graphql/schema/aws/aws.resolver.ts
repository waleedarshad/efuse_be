import { Resolver, Authorized, Query, Args } from "type-graphql";
import { Failure } from "@efuse/contracts";
import { Logger, ILogger } from "@efuse/logger";
import { isEmpty } from "lodash";

import { InitMultipartArgument, CompleteMultipartArgument } from "./argument";
import { InitMultipartUploadResponse, MultipartUploadSignedURL, CompleteMultipartUploadResponse } from "./types";
import { S3Service } from "../../../lib/aws/s3.service";
import { MULTIPART_UPLOAD_RESOLVER } from "../../../lib/metrics";

@Resolver()
export class AwsResolver {
  private logger: ILogger = Logger.create({ name: "aws.resolver" });

  private $s3Service: S3Service = new S3Service();

  /**
   * @summary Begin multipart upload & get the signed URLs
   *
   * @authenticated Protected by JWT Token
   *
   * @kind Query
   *
   * @param {InitMultipartArgument} params
   *
   * @returns {Promise<InitMultipartUploadResponse>}
   */
  @Authorized()
  @Query((_returns) => InitMultipartUploadResponse)
  async initMultipartUpload(@Args() params: InitMultipartArgument): Promise<InitMultipartUploadResponse> {
    try {
      this.$s3Service.recordMetric(MULTIPART_UPLOAD_RESOLVER.INIT_MULTIPART_UPLOAD.START);

      // Get extension
      const extension = params.fileName.split(".")[1];

      // Make sure we have the extension
      if (!extension) {
        throw Failure.BadRequest("Invalid file");
      }

      // Generate a unique filename
      const fileKey = this.$s3Service.generateFileName(extension);

      // Begin upload
      const multipartUploadOutput = await this.$s3Service.createMultipartUpload(fileKey);

      // Make sure we get the uploadID
      if (!multipartUploadOutput.UploadId) {
        throw Failure.BadRequest("UploadID not found");
      }

      const signedURLPromises: Promise<string>[] = [];

      // Generate SignedURLs Promises
      for (let partNumber = 1; partNumber <= params.totalParts; partNumber++) {
        signedURLPromises.push(
          this.$s3Service.getSignedURLForUploadPart(fileKey, multipartUploadOutput.UploadId, partNumber)
        );
      }

      // Resolve signed URL promises
      const signedURLs = await Promise.all(signedURLPromises);

      // Build response
      const response: InitMultipartUploadResponse = {
        fileKey,
        uploadId: multipartUploadOutput.UploadId,
        signedURLs: signedURLs.map((url, index): MultipartUploadSignedURL => ({ part: index + 1, url }))
      };

      this.$s3Service.recordMetric(MULTIPART_UPLOAD_RESOLVER.INIT_MULTIPART_UPLOAD.FINISH);

      return response;
    } catch (error) {
      this.$s3Service.recordMetric(MULTIPART_UPLOAD_RESOLVER.INIT_MULTIPART_UPLOAD.ERROR);

      throw error;
    }
  }

  /**
   * @summary Complete a multipart upload
   *
   * @authenticated Protected by JWT Token
   *
   * @kind Query
   *
   * @param {CompleteMultipartArgument} params
   *
   * @returns {Promise<CompleteMultipartUploadResponse>}
   */
  @Authorized()
  @Query((_returns) => CompleteMultipartUploadResponse)
  async completeMultipartUpload(@Args() params: CompleteMultipartArgument): Promise<CompleteMultipartUploadResponse> {
    try {
      this.$s3Service.recordMetric(MULTIPART_UPLOAD_RESOLVER.COMPLETE_MULTIPART_UPLOAD.START);

      // Validate parts
      if (isEmpty(params.parts)) {
        throw Failure.BadRequest("parts is missing");
      }

      // Mark upload as complete
      const response = await this.$s3Service.completeMultipartUpload(params.fileKey, params.uploadId, params.parts);

      // Throw an error when we don't get location or key back
      if (!response.Location || !response.Key) {
        this.logger.info({ response }, "Location or Key not received in multipart Upload response");

        throw Failure.BadRequest("Something went wrong while uploading");
      }

      this.$s3Service.recordMetric(MULTIPART_UPLOAD_RESOLVER.COMPLETE_MULTIPART_UPLOAD.FINISH);

      // return the response
      return { url: response.Location, fileKey: response.Key };
    } catch (error) {
      this.$s3Service.recordMetric(MULTIPART_UPLOAD_RESOLVER.COMPLETE_MULTIPART_UPLOAD.ERROR);

      throw error;
    }
  }
}
