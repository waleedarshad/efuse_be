import { InputType, Field, Int } from "type-graphql";
import S3 from "aws-sdk/clients/s3";

@InputType("UploadedPartInput", { description: "Representing the object for uploaded parts" })
export class UploadedPartInput implements S3.CompletedPart {
  @Field((_type) => String)
  ETag!: string;

  @Field((_type) => Int)
  PartNumber!: number;
}
