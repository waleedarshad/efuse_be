import { ArgsType, Field } from "type-graphql";
import { IsNotEmpty } from "class-validator";

import { UploadedPartInput } from "../input";

@ArgsType()
export class CompleteMultipartArgument {
  @Field((_type) => String)
  @IsNotEmpty()
  fileKey!: string;

  @Field((_type) => String)
  @IsNotEmpty()
  uploadId!: string;

  @Field((_type) => [UploadedPartInput])
  parts!: UploadedPartInput[];
}
