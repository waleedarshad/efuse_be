import { ArgsType, Field, Int } from "type-graphql";
import { IsNotEmpty, Min } from "class-validator";

@ArgsType()
export class InitMultipartArgument {
  @Field((_type) => String)
  @IsNotEmpty()
  fileName!: string;

  @Field((_type) => Int)
  @Min(1)
  totalParts!: number;
}
