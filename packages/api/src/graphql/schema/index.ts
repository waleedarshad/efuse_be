import { mergeSchemas, makeExecutableSchema } from "graphql-tools";
import { constraintDirective, constraintDirectiveTypeDefs } from "graphql-constraint-directive";
import GraphQLJSON, { GraphQLJSONObject } from "graphql-type-json";
import { buildTypeDefsAndResolvers } from "type-graphql";

import { AlgoliaTypeDefs } from "../typeDefs/algolia";
import { AlgoliaResolvers } from "../resolvers/algolia";

import { AuthDirective } from "../directives/auth.directive";

import { RootTypeDef } from "../typeDefs/Root";

import { SharedTypeDefs } from "../typeDefs/shared";
import { SharedResolver } from "../resolvers/shared";

import { RecruitmentProfileTypeDef } from "../typeDefs/RecruitmentProfile";

import { PostTypeDef } from "../typeDefs/Post";
import { PostResolver } from "../resolvers/Post";

import { UserTypeDef } from "../typeDefs/User";
import { UserResolver } from "../resolvers/User";

import { FeaturedFeedTypeDef } from "../typeDefs/FeaturedFeed";
import { FeaturedFeedResolver } from "../resolvers/FeaturedFeed";

import { MediaTypeDef } from "../typeDefs/Media";
import { MediaResolver } from "../resolvers/Media";

import { FileTypeDef } from "../typeDefs/File";

import { HypeTypeDef } from "../typeDefs/Hype";
import { HypeResolver } from "../resolvers/Hype";

import { UserBadgeTypeDef } from "../typeDefs/UserBadge";

import { SearchTypeDef } from "../typeDefs/Search";
import { SearchResolver } from "../resolvers/Search";

import { OrganizationTypeDefs } from "../typeDefs/organization";
import { OrganizationResolvers } from "../resolvers/organization";

import { PartnerApplicantTypeDef } from "../typeDefs/PartnerApplicant";
import { PartnerApplicantResolver } from "../resolvers/PartnerApplicant";

import { ERenaTypeDefs } from "../typeDefs/eRena";
import { ERenaResolvers } from "../resolvers/eRena";

import { MyntTypeDefs } from "../typeDefs/myntgg";
import { MyntResolvers } from "../resolvers/myntgg";

import { TokenTypeDefs } from "../typeDefs/efuse-token";
import { TokenResolvers } from "../resolvers/efuse-token";

import { GameTypeDef } from "../typeDefs/Game";
import { GameResolver } from "../resolvers/Game";

import { OpportunityTypeDef } from "../typeDefs/Opportunity";
import { OpportunityResolver } from "../resolvers/Opportunity";

import { ValorantTypeDefs } from "../typeDefs/valorant";
import { ValorantResolvers } from "../resolvers/valorant";

import { AimlabTypeDef } from "../typeDefs/aimlabs";
import { AimlabResolvers } from "../resolvers/aimlab";

import { LeaderboardTypeDefs, LeaderboardDeprecatedTypeDefs } from "../typeDefs/leaderboard";
import { LeaderboardResolvers, LeaderboardDeprecatedResolvers } from "../resolvers/leaderboard";

import { LearningArticleTypeDefs } from "../typeDefs/learning-article";
import { LearningArticleResolvers } from "../resolvers/learning-article";

import {
  LeagueEventResolver,
  LeagueBracketResolver,
  LeagueGameScoreResolver,
  LeagueInviteCodeResolver,
  LeagueMatchResolver,
  LeaguePoolResolver,
  LeagueRoundResolver,
  LeagueCompetitorScoreResolver,
  LeagueResolver,
  LeagueScoreboardResolver
} from "../resolvers/league";
import {
  LeagueBracketTypeDef,
  LeagueEventTypeDef,
  LeagueGameScoreTypeDef,
  LeagueInviteCodeTypeDef,
  LeagueMatchTypeDef,
  LeaguePoolTypeDef,
  LeagueRoundTypeDef,
  LeagueCompetitorScoreTypeDef,
  LeagueTypeDef,
  LeagueScoreboardTypeDef
} from "../typeDefs/league";

import { LeagueOfLegendsStatsTypeDef } from "../typeDefs/league-of-legends";
import { LeagueOfLegendsStatsResolver } from "../resolvers/league-of-legends";

import { ExternalAuthStatusTypeDef } from "../typeDefs/ExternalAuthStatus";
import { ExternalAuthStatusResolver } from "../resolvers/ExternalAuthStatus";
import { LivestreamResolver } from "../resolvers/Livestreams";
import { LivestreamTypeDef } from "../typeDefs/Livestream";

import { PipelineTypeDefs } from "../typeDefs/pipeline";
import { PipelineResolvers } from "../resolvers/pipeline";

import { ApplicantsTypeDefs } from "../typeDefs/applicant";
import { ApplicantsResolvers } from "../resolvers/applicant";

import { ValidationsTypeDef } from "../typeDefs/Validations";
import { ValidationsResolver } from "../resolvers/Validations";

import { FeedTypeDefs } from "../typeDefs/Feed";
import { FeedResolvers } from "../resolvers/Feed";

import { ClippyTypeDef } from "../typeDefs/Clippy";
import { ClippyResolvers } from "../resolvers/clippy";

import { TwitchAccountInfoTypeDef } from "../typeDefs/TwitchAccountInfo";

import { FavoriteTypeDef } from "../typeDefs/favorites";
import { FavoriteResolver } from "../resolvers/favorites";

import { NoteResolver } from "../resolvers/note";
import { NoteTypeDef } from "../typeDefs/note";

import { TeamMemberResolver, TeamInviteCodeResolver, TeamResolver } from "../resolvers/team";
import { TeamTypeDef, TeamInviteCodeTypeDef, TeamMemberTypeDef } from "../typeDefs/team";

import { YoutubeTypeDefs } from "../typeDefs/youtube";
import { YoutubeResolvers } from "../resolvers/youtube";

import { InviteCodeTypeDef } from "../typeDefs/InviteCode";
import { InviteCodeResolver } from "../resolvers/InviteCode";

import { authorization } from "../decorators";

import { OrganizationPermissionTypeDef } from "../typeDefs/organization-permission";
import { OrganizationPermissionResolver } from "../resolvers/organization-permission";

import { GamePlatformTypeDef } from "../typeDefs/GamePlatform";
import { GamePlatformResolver } from "../resolvers/GamePlatform";

export const registerSchemas = async () => {
  const { typeDefs, resolvers } = await buildTypeDefsAndResolvers({
    resolvers: [__dirname + "/**/*.resolver.{ts,js}"],
    authChecker: authorization
  });

  const JsonResolvers = {
    JSON: GraphQLJSON,
    JSONObject: GraphQLJSONObject
  };

  const MainSchema = makeExecutableSchema({
    resolvers: [
      resolvers,
      ...AimlabResolvers,
      ...AlgoliaResolvers,
      ...ApplicantsResolvers,
      ...ClippyResolvers,
      ...ERenaResolvers,
      ...FeedResolvers,
      ...InviteCodeResolver.resolveAll,
      ...LeaderboardDeprecatedResolvers, // TODO: Remove once frontend updated
      ...LeaderboardResolvers,
      ...LeagueResolver.resolveAll,
      ...LearningArticleResolvers,
      ...MyntResolvers,
      ...OrganizationResolvers,
      ...PipelineResolvers,
      ...SharedResolver.resolveAll,
      ...TeamResolver.resolveAll,
      ...TokenResolvers,
      ...ValorantResolvers,
      ...YoutubeResolvers,
      ExternalAuthStatusResolver,
      FavoriteResolver.resolve,
      FeaturedFeedResolver,
      GameResolver,
      HypeResolver.resolve,
      JsonResolvers,
      LeagueBracketResolver.resolve,
      LeagueEventResolver.resolve,
      LeagueGameScoreResolver.resolve,
      LeagueInviteCodeResolver.resolve,
      LeagueMatchResolver.resolve,
      LeagueOfLegendsStatsResolver,
      LeaguePoolResolver.resolve,
      LeagueRoundResolver.resolve,
      LeagueCompetitorScoreResolver.resolve,
      LeagueScoreboardResolver.resolve,
      LivestreamResolver,
      MediaResolver,
      NoteResolver.resolve,
      OpportunityResolver,
      OrganizationPermissionResolver.resolve,
      PartnerApplicantResolver,
      PostResolver,
      SearchResolver,
      TeamInviteCodeResolver.resolve,
      TeamMemberResolver.resolve,
      UserResolver,
      ValidationsResolver,
      GamePlatformResolver.resolve
    ],
    typeDefs: [
      typeDefs,
      ...AimlabTypeDef,
      ...AlgoliaTypeDefs,
      ...ApplicantsTypeDefs,
      ...ERenaTypeDefs,
      ...FeedTypeDefs,
      ...LeaderboardDeprecatedTypeDefs, // TODO: Remove once frontend updated
      ...LeaderboardTypeDefs,
      ...LearningArticleTypeDefs,
      ...MyntTypeDefs,
      ...OrganizationTypeDefs,
      ...PipelineTypeDefs,
      ...SharedTypeDefs,
      ...TokenTypeDefs,
      ...ValorantTypeDefs,
      ...YoutubeTypeDefs,
      ClippyTypeDef,
      constraintDirectiveTypeDefs,
      ExternalAuthStatusTypeDef,
      FavoriteTypeDef,
      FeaturedFeedTypeDef,
      FileTypeDef,
      GameTypeDef,
      HypeTypeDef,
      InviteCodeTypeDef,
      LeagueBracketTypeDef,
      LeagueEventTypeDef,
      LeagueGameScoreTypeDef,
      LeagueInviteCodeTypeDef,
      LeagueMatchTypeDef,
      LeagueOfLegendsStatsTypeDef,
      LeaguePoolTypeDef,
      LeagueRoundTypeDef,
      LeagueCompetitorScoreTypeDef,
      LeagueScoreboardTypeDef,
      LeagueTypeDef,
      LivestreamTypeDef,
      MediaTypeDef,
      NoteTypeDef,
      OpportunityTypeDef,
      OrganizationPermissionTypeDef,
      PartnerApplicantTypeDef,
      PostTypeDef,
      RecruitmentProfileTypeDef,
      RootTypeDef,
      SearchTypeDef,
      TeamInviteCodeTypeDef,
      TeamMemberTypeDef,
      TeamTypeDef,
      TwitchAccountInfoTypeDef,
      UserBadgeTypeDef,
      UserTypeDef,
      ValidationsTypeDef,
      GamePlatformTypeDef
    ],
    schemaTransforms: [constraintDirective()],
    schemaDirectives: { auth: AuthDirective }
  });

  // https://www.apollographql.com/blog/modularizing-your-graphql-schema-code-d7f71d5ed5f2/
  const schema = mergeSchemas({
    schemas: [MainSchema]
  });

  return schema;
};
