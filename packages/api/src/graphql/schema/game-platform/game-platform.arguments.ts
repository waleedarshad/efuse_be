import { ArgsType, Field } from "type-graphql";
import { IsNotEmpty } from "class-validator";
import { PlatformEnum } from "@efuse/entities";

@ArgsType()
export class AddGamePlatformArguments {
  @Field((_type) => String)
  @IsNotEmpty()
  platform!: PlatformEnum;

  @Field((_type) => String)
  @IsNotEmpty()
  displayName!: string;
}

@ArgsType()
export class RemoveGamePlatformArguments {
  @Field((_type) => String)
  @IsNotEmpty()
  platform!: PlatformEnum;
}
