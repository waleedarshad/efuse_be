import { Resolver, Ctx, Args, Authorized, Mutation } from "type-graphql";
import { GamePlatform } from "../../../backend/models/game-platform.model";
import { GamePlatformsService } from "../../../lib/game_platforms/game-platforms.service";
import { IGraphQLContext } from "../../../backend/interfaces";
import { AddGamePlatformArguments, RemoveGamePlatformArguments } from "./game-platform.arguments";

@Resolver((_of) => GamePlatform)
export class GamePlatformResolver {
  private $gamePlatformService = new GamePlatformsService();

  /**
   * @summary Add Game platform
   *
   * @authenticated Protected by JWT token
   *
   * @kind Mutation
   *
   * @param {IGraphQLContext} ctx
   * @param {String} userId User's ID
   * @param {String} platformId Platform's ID
   *
   * @return {GamePlatform} GamePlatform Object
   */
  @Authorized()
  @Mutation((_return) => GamePlatform)
  async addPlatform(@Ctx() ctx: IGraphQLContext, @Args() params: AddGamePlatformArguments): Promise<GamePlatform> {
    const gamePlatform = await this.$gamePlatformService.addPlatform(ctx.userId, params.platform, params.displayName);
    return gamePlatform;
  }

  /**
   * @summary Remove Game platform
   *
   * @authenticated Protected by JWT token
   *
   * @kind Mutation
   *
   * @param {IGraphQLContext} ctx
   * @param {String} userId User's ID
   * @param {String} platformId Platform's ID
   *
   * @return {GamePlatform} GamePlatform Object
   */
  @Authorized()
  @Mutation((_return) => GamePlatform)
  async deleteGamePlatform(
    @Ctx() ctx: IGraphQLContext,
    @Args() params: RemoveGamePlatformArguments
  ): Promise<GamePlatform> {
    const gamePlatform = await this.$gamePlatformService.deleteGamePlatform(ctx.userId, params.platform);
    return gamePlatform;
  }
}
