import { Resolver } from "type-graphql";

import { Badge } from "../../../backend/models/badge.model";

@Resolver((_of) => Badge)
export class BadgeResolver {}
