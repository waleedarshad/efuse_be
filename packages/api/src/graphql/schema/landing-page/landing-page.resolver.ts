import { Resolver, Query, Arg } from "type-graphql";
import { DocumentType } from "@typegoose/typegoose";

import { LandingPageService } from "../../../lib/landing-page/landing-page.service";
import { LandingPage } from "../../../backend/models/landing-page.model";

@Resolver((_of) => LandingPage)
export class LandingPageResolver {
  private $landingPageService = new LandingPageService();

  /**
   * @summary Get landing page object by slug
   *
   * @kind Query
   *
   * @param {string} slug
   *
   * @returns {Promise<DocumentType<LandingPage>>}
   */
  @Query((_returns) => LandingPage)
  async getLandingPage(@Arg("slug") slug: string): Promise<DocumentType<LandingPage>> {
    const landingPage = await this.$landingPageService.findOneBySlug(slug);

    return landingPage;
  }
}
