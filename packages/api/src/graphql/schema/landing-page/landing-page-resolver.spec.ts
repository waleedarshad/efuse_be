import { landingPageMock } from "../test-utils/mock-landing-page";
import { LandingPageResolver } from "./landing-page.resolver";
import { LandingPageService } from "../../../lib/landing-page/landing-page.service";

describe("landing-page.resolver", () => {
  let resolver: LandingPageResolver;

  const page = landingPageMock();
  const slug = page.slug;

  beforeEach(() => {
    resolver = new LandingPageResolver();
  });

  it("should pass if findOneBySlug is called", async () => {
    LandingPageService.prototype.findOneBySlug = jest.fn().mockResolvedValue(true);

    await resolver.getLandingPage(slug);

    expect(LandingPageService.prototype.findOneBySlug).toHaveBeenCalledWith(slug);
  });
});
