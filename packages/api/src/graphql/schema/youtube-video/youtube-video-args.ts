import { ArgsType, Field, Int } from "type-graphql";

@ArgsType()
export class YoutubeVideoArgs {
  @Field((_type) => String, { nullable: false })
  ownerKind!: string;

  @Field((_type) => String, { nullable: true })
  owner?: string;

  @Field((_type) => Int, { nullable: true })
  limit!: number;

  @Field((_type) => Int, { nullable: true })
  page!: number;
}
