import { Resolver, Authorized, Ctx, Query, Root, FieldResolver, Args } from "type-graphql";
import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";

import { YoutubeVideoArgs } from "./youtube-video-args";
import { YoutubeVideoBaseService } from "../../../lib/youtube/youtube-video-base.service";
import { IGraphQLContext } from "../../../backend/interfaces";
import { PaginationHelper } from "../../helpers/pagination.helper";
import { YoutubeVideoPaginationLimit } from "../../../lib/youtube/youtube.enum";
import { OrganizationACLService } from "../../../lib/organizations/organization-acl.service";
import { OrganizationAccessTypes } from "../../../lib/organizations/organization-access-types";
import { YoutubeVideo, YoutubeVideoPagination } from "../../../backend/models/youtube-video/youtube-video.model";

@Resolver((_of) => YoutubeVideo)
export class YoutubeVideoResolver {
  private $youtubeVideoBaseService = new YoutubeVideoBaseService();
  private $paginationHelper = new PaginationHelper();
  private $organizationACLService = new OrganizationACLService();

  @Authorized()
  @Query((_return) => YoutubeVideoPagination)
  async GetPaginatedYoutubeVideos(
    @Ctx() ctx: IGraphQLContext,
    @Args() args: YoutubeVideoArgs
  ): Promise<YoutubeVideoPagination> {
    // Validate pagination params
    const { page, limit } = this.$paginationHelper.validatePaginationParams(
      args.page,
      args.limit,
      YoutubeVideoPaginationLimit.DEFAULT,
      YoutubeVideoPaginationLimit.MAX
    );

    // Make sure user is either captain or owner of organization
    if (args.ownerKind === OwnerKind.ORGANIZATION) {
      // Make sure owner is present
      if (!args.owner) {
        throw Failure.BadRequest("owner is required");
      }

      // Validate Organization access
      await this.$organizationACLService.authorizedToAccess(ctx.user, args.owner, OrganizationAccessTypes.ANY);
    }

    // Get paginated results
    const youtubeVideos = await this.$youtubeVideoBaseService.getPaginatedYoutubeVideos(
      {
        owner: args.ownerKind === OwnerKind.ORGANIZATION ? args.owner : ctx.userId,
        ownerKind: args.ownerKind
      },
      { sort: { videoPublishedAt: -1 }, page, limit }
    );

    return youtubeVideos;
  }

  // Need to see why this did not work but old resolver field resolve
  @FieldResolver((_of) => YoutubeVideo)
  videoUrl(@Root() youtubeVideo: YoutubeVideo): string | null {
    if (youtubeVideo.videoId) {
      return `https://www.youtube.com/watch?v=${youtubeVideo.videoId}`;
    }

    return null;
  }
}
