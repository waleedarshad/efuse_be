import { Resolver } from "type-graphql";

import { Game } from "../../../backend/models/game.model";

@Resolver((_of) => Game)
export class GameResolver {}
