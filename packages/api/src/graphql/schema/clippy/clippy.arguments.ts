import { ArgsType, Field } from "type-graphql";
import { IsUrl, IsNotEmpty, IsMongoId } from "class-validator";

@ArgsType()
export class GetSpecificClipArguments {
  @Field((_type) => String)
  @IsNotEmpty()
  token!: string;

  @Field((_type) => String)
  @IsNotEmpty()
  @IsUrl()
  url!: string;
}

@ArgsType()
export class GetShoutoutClipArguments {
  @Field((_type) => String)
  @IsNotEmpty()
  token!: string;

  @Field((_type) => String)
  @IsNotEmpty()
  channelName!: string;

  @Field((_type) => String)
  @IsNotEmpty()
  @IsMongoId()
  commandId!: string;
}
