import { Resolver, FieldResolver, Root } from "type-graphql";

import { ClippyTwitchService } from "../../../lib/clippy/clippy-twitch.service";
import { ClippyTwitchClip } from "../../../backend/models/clippy/clippy-twitch-clip.model";
import { TwitchGame } from "../../../backend/models/twitch/twitch-game.model";

@Resolver((_of) => ClippyTwitchClip)
export class ClippyTwitchClipResolver {
  private $clippyTwitchService = new ClippyTwitchService();

  /**
   * @summary Resolve Game object by hitting twitch API
   *
   * @kind FieldResolver
   *
   * @param {ClippyTwitchClip} clippyTwitchClip
   *
   * @returns {Promise<TwitchGame | null>}
   */
  @FieldResolver()
  async game(@Root() clippyTwitchClip: ClippyTwitchClip): Promise<TwitchGame | null> {
    if (clippyTwitchClip.game_id) {
      const { data } = await this.$clippyTwitchService.getGames(undefined, { id: `id=${clippyTwitchClip.game_id}` });

      if (data.length > 0) {
        return data[0];
      }
    }

    return null;
  }
}
