import { Resolver, Query, Ctx, FieldResolver, Root, Mutation, Authorized, Arg, Args } from "type-graphql";
import { DocumentType } from "@typegoose/typegoose";
import { Failure } from "@efuse/contracts";

import { Clippy } from "../../../backend/models/clippy/clippy.model";
import { ClippyService } from "../../../lib/clippy/clippy.service";
import { IGraphQLContext } from "../../../backend/interfaces";
import { UserHelperService } from "../../../lib/users/user-helper.service";
import { ClippyCommand } from "../../../backend/models/clippy/clippy-command.model";
import { ClippyCommandsService } from "../../../lib/clippy/clippy-commands.service";
import { ClippyBurnedTokenService } from "../../../lib/clippy/clippy-burned-token.service";
import { ClippyTwitchClip } from "../../../backend/models/clippy/clippy-twitch-clip.model";
import { ClippyTwitchService } from "../../../lib/clippy/clippy-twitch.service";
import { GetSpecificClipArguments, GetShoutoutClipArguments } from "./clippy.arguments";
import { ClippyPermissionService } from "../../../lib/clippy/clippy-permission.service";
import { ObjectId } from "../../../backend/types";
import { ClippyRoleValueEnum } from "../../../lib/permissions/enums";

@Resolver((_of) => Clippy)
export class ClippyResolver {
  private $clippyPermissionService = ClippyPermissionService.new();
  private $clippyService = new ClippyService();
  private $userHelperService = new UserHelperService();
  private $clippyCommandsService = new ClippyCommandsService();
  private $clippyBurnedTokenService = new ClippyBurnedTokenService();
  private $clippyTwitchService = new ClippyTwitchService();

  /**
   * @summary Get current user's clippy config & commands
   *
   * @authenticated Protected by JWT Token
   *
   * @kind Query
   *
   * @param {IGraphQLContext} ctx
   *
   * @returns {Promise<DocumentType<Clippy>>}
   */
  @Authorized()
  @Query((_returns) => Clippy)
  async getClippy(@Ctx() ctx: IGraphQLContext): Promise<DocumentType<Clippy>> {
    // Make sure user has a twitch account linked
    const hasTwitchAccount = await this.$userHelperService.hasTwitchAccount(ctx.userId);

    // Throw error when there is not twitch account
    if (!hasTwitchAccount) {
      throw Failure.BadRequest("Please link a twitch account to gain clippy access");
    }

    // Find or create a new clippy object for the user
    const clippy = await this.$clippyService.findOrCreate(ctx.userId);

    return clippy;
  }

  /**
   * @summary Get user's clippy config & commands by token
   *
   * @kind Query
   *
   * @param {string} token
   *
   * @returns {Promise<DocumentType<Clippy>>}
   */
  @Query((_returns) => Clippy)
  async getClippyByToken(@Arg("token") token: string): Promise<DocumentType<Clippy>> {
    const clippy = await this.validateAndGetClippyByToken(token);

    return clippy;
  }

  /**
   * @summary Get shoutout clip on the basis channel name
   *
   * @kind Query
   *
   * @param {GetShoutoutClipArguments} params
   *
   * @returns {Promise<ClippyTwitchClip | null>}
   */
  @Query((_return) => ClippyTwitchClip, { nullable: true })
  async getShoutoutClip(@Args() params: GetShoutoutClipArguments): Promise<ClippyTwitchClip | null> {
    // Validate & Get clippy object by token
    const clippy = await this.validateAndGetClippyByToken(params.token);

    await this.$clippyPermissionService.userHasAccess(
      <ObjectId>clippy.user,
      [ClippyRoleValueEnum.CLIPPY_FREE],
      "User does not have access for shoutout command"
    );

    // Get clippy command object
    const clippyCommand = await this.$clippyCommandsService.findOne(
      { _id: params.commandId, clippy: clippy._id },
      {},
      { lean: true }
    );

    // Make sure it exists
    if (!clippyCommand) {
      throw Failure.UnprocessableEntity("Clippy Command not found");
    }

    // Get Shoutout Clip
    const twitchClip = await this.$clippyTwitchService.getShoutoutClip(
      String(clippy.user),
      params.channelName,
      clippyCommand
    );

    return twitchClip;
  }

  /**
   * @summary Get a specific clip from twitch on the basis of a clip URL
   *
   * @kind Query
   *
   * @param {GetSpecificClipArguments} params
   *
   * @returns {Promise<ClippyTwitchClip | null>}
   */
  @Query((_return) => ClippyTwitchClip, { nullable: true })
  async getSpecificClip(@Args() params: GetSpecificClipArguments): Promise<ClippyTwitchClip | null> {
    // Validate & Get clippy object by token
    const clippy = await this.validateAndGetClippyByToken(params.token);

    await this.$clippyPermissionService.userHasAccess(
      <ObjectId>clippy.user,
      [ClippyRoleValueEnum.CLIPPY_FREE],
      "User does not have access to get specific clip"
    );

    // Extract id from url
    const url = params.url.split("?")[0];
    let clipId: string | undefined = "";

    if (url.includes("clips.twitch.tv")) {
      clipId = url.split("tv/").pop();
    } else if (url.includes("/clip/")) {
      clipId = url.split("clip/").pop();
    }

    // Return nothing when id is not found
    if (!clipId) {
      return null;
    }

    // Get Clip
    const twitchClip = await this.$clippyTwitchService.getSpecificClip(String(clippy.user), clipId);

    return twitchClip;
  }

  /**
   * @summary Regenerate Clippy token for current user
   *
   * @kind Mutation
   *
   * @authenticated Protected by JWT Token
   *
   * @param {IGraphQLContext} ctx
   *
   * @returns {Promise<DocumentType<Clippy>>}
   */
  @Authorized()
  @Mutation((_returns) => Clippy)
  async regenerateClippyToken(@Ctx() ctx: IGraphQLContext): Promise<DocumentType<Clippy>> {
    // Make sure clippy object is available before generating token
    const clippy = await this.$clippyService.findOne({ user: ctx.userId }, {}, { lean: true });

    if (!clippy) {
      throw Failure.UnprocessableEntity("clippy object is not found");
    }

    const token = await this.$clippyBurnedTokenService.generateToken();

    // Find or create a new clippy object for the user
    const updatedClippy = await this.$clippyService.updateOne(
      { user: ctx.userId },
      { token },
      { lean: true, new: true }
    );

    return updatedClippy;
  }

  /**
   * @summary Resolve commands for the user
   *
   * @kind FieldResolver
   *
   * @param {Clippy} clippy
   * @param {IGraphQLContext} ctx
   *
   * @returns {Promise<DocumentType<ClippyCommand>[]>}
   */
  @FieldResolver()
  async commands(@Root() clippy: Clippy, @Ctx() ctx: IGraphQLContext): Promise<DocumentType<ClippyCommand>[]> {
    if (ctx.userId && String(clippy.user) !== String(ctx.userId)) {
      throw Failure.Forbidden("You are not allowed to perform this action");
    }

    const commands = await this.$clippyCommandsService.findOrCreateCommands(clippy._id);

    return commands;
  }

  /**
   * @summary Validate token
   *
   * @access private
   *
   * @param token
   *
   * @returns {Promise<DocumentType<Clippy>>}
   */
  private async validateAndGetClippyByToken(token: string): Promise<DocumentType<Clippy>> {
    // Make sure token is present
    if (!token) {
      throw Failure.BadRequest("Token not found");
    }

    // Get clippy object by token
    const clippy = await this.$clippyService.findOne({ token }, {}, { lean: true });

    // Make sure it exists
    if (!clippy) {
      throw Failure.UnprocessableEntity("Clippy object not found against the token");
    }

    return clippy;
  }
}
