import { InputType, Field, ID, Int } from "type-graphql";
import { ClippySortOrderEnum, ClippyDateRangeEnum, ClippyAllowedRolesEnum } from "@efuse/entities";
import { IsMongoId } from "class-validator";

import { ClippyCommand } from "../../../backend/models/clippy/clippy-command.model";
import { ObjectId } from "../../../backend/types";

@InputType("ClippyCommandInput", { description: "Input to interact with Clippy Command Model" })
export class ClippyCommandInput implements Partial<ClippyCommand> {
  @IsMongoId()
  @Field((_type) => ID)
  _id!: ObjectId;

  @Field((_type) => String, { nullable: true })
  command?: string;

  @Field((_type) => ClippySortOrderEnum, { nullable: true })
  sortOrder?: ClippySortOrderEnum;

  @Field((_type) => ClippyDateRangeEnum, { nullable: true })
  dateRange?: ClippyDateRangeEnum;

  @Field((_type) => [ClippyAllowedRolesEnum], { nullable: true })
  allowedRoles?: ClippyAllowedRolesEnum[];

  @Field((_type) => Int, { nullable: true })
  minimumViews?: number;

  @Field((_type) => Boolean, { nullable: true })
  queueClips?: boolean;
}
