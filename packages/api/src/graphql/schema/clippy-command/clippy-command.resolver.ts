import { Resolver, Mutation, Arg, Ctx, FieldResolver, Root, Authorized } from "type-graphql";
import { Failure } from "@efuse/contracts";
import { DocumentType } from "@typegoose/typegoose";

import { ClippyCommand } from "../../../backend/models/clippy/clippy-command.model";
import { ClippyCommandsService } from "../../../lib/clippy/clippy-commands.service";
import { ClippyCommandInput } from "./clippy-command.input";
import { IGraphQLContext } from "../../../backend/interfaces";
import { ClippyService } from "../../../lib/clippy/clippy.service";
import { Clippy } from "../../../backend/models/clippy/clippy.model";
import { ClippyRoleValueEnum } from "../../../lib/permissions/enums";
import { ClippyPermissionService } from "../../../lib/clippy/clippy-permission.service";

@Resolver((_of) => ClippyCommand)
export class ClippyCommandResolver {
  private $clippyPermissionService = ClippyPermissionService.new();
  private $clippyCommandsService = new ClippyCommandsService();
  private $clippyService = new ClippyService();

  /**
   * @summary Update a particular clippy command
   *
   * @kind Mutation
   *
   * @authenticated Protect by JWT token
   *
   * @param {ClippyCommandInput} updateCommandBody
   * @param {IGraphQLContext} ctx
   *
   * @returns {Promise<DocumentType<ClippyCommand>>}
   */
  @Authorized()
  @Mutation((_returns) => ClippyCommand)
  async updateClippyCommand(
    @Arg("params") updateCommandBody: ClippyCommandInput,
    @Ctx() ctx: IGraphQLContext
  ): Promise<DocumentType<ClippyCommand>> {
    // Get the clippyCommand object
    const clippyCommand = await this.$clippyCommandsService.findOne({ _id: updateCommandBody._id }, {}, { lean: true });

    // Make sure it exists
    if (!clippyCommand) {
      throw Failure.UnprocessableEntity("Command not found");
    }

    await this.$clippyPermissionService.userHasAccess(
      ctx.userId,
      [ClippyRoleValueEnum.CLIPPY_FREE],
      "User does not have permission to update clippy command"
    );

    // Get the corresponding Clippy object
    const clippy = await this.$clippyService.findOne({ _id: clippyCommand.clippy as string }, {}, { lean: true });

    // Make sure it exists
    if (!clippy) {
      throw Failure.UnprocessableEntity("clippy object is not found");
    }

    // Make sure user owns the command
    if (String(clippy.user) !== String(ctx.userId)) {
      throw Failure.Forbidden("You are not allowed to perform this action");
    }

    // Update command
    const updatedCommand = await this.$clippyCommandsService.updateCommand(clippy._id, updateCommandBody);

    return updatedCommand;
  }

  /**
   * @summary Resolving Clippy field
   *
   * @kind FieldResolver
   *
   * @param {ClippyCommand} clippyCommand
   *
   * @returns {Promise<DocumentType<Clippy> | null>}
   */
  @FieldResolver()
  async clippy(@Root() clippyCommand: ClippyCommand): Promise<DocumentType<Clippy> | null> {
    const clippy = await this.$clippyService.findOne({ _id: clippyCommand.clippy as string }, {}, { lean: true });

    return clippy;
  }
}
