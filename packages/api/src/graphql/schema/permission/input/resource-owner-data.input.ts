import { ObjectId } from "mongoose";
import { ResourceOwnerKindEnum } from "../../../../lib/permissions/enums";
import { Field, InputType, registerEnumType } from "type-graphql";

registerEnumType(ResourceOwnerKindEnum, {
  name: "ResourceOwnerKindEnum"
});

@InputType("ResourceOwnerData")
export class ResourceOwnerData {
  @Field((_type) => String)
  resourceOwner!: ObjectId;

  @Field((_type) => ResourceOwnerKindEnum)
  resourceOwnerKind!: ResourceOwnerKindEnum;
}
