import { IsMongoId, IsNotEmpty } from "class-validator";
import { ObjectId } from "mongoose";
import { EntityRoleEntityKindEnum } from "../../../../lib/permissions/enums";
import { Field, ID, InputType, registerEnumType } from "type-graphql";

registerEnumType(EntityRoleEntityKindEnum, {
  name: "EntityRoleEntityKindEnum"
});

@InputType("EntityData")
export class EntityData {
  @IsMongoId()
  @IsNotEmpty()
  @Field((_type) => ID, {
    description: "When entity id and/or entity kind are not set then it defaults to the authorized user",
    nullable: false
  })
  entityId!: ObjectId;

  @Field((_type) => EntityRoleEntityKindEnum, {
    description: "When entity id and/or entity kind are not set then it defaults to the authorized user",
    nullable: false
  })
  @IsNotEmpty()
  entityKind!: EntityRoleEntityKindEnum;
}
