import { Field, ID, InputType } from "type-graphql";
import { IsMongoId, IsNotEmpty } from "class-validator";
import { ObjectId } from "../../../../backend/types";
import { RoleValue } from "../../../../lib/permissions/types/role-value.type";

@InputType()
export class HasRoleInput {
  @IsMongoId()
  @Field((_type) => ID, {
    description: "When entity id and/or entity kind are not set then it defaults to the authorized user",
    nullable: true
  })
  entityId?: ObjectId;

  @Field((_type) => [ID], {
    description: "If the role id is known then use this",
    nullable: true
  })
  roleIds?: ObjectId[];

  @Field((_type) => [String], {
    description: `When checking for an efuse created role then this is recommended. (Example: clippyFree)
    Note: This is a union param and graphql cannot show those properly`,
    nullable: true
  })
  @IsNotEmpty()
  roleValues?: RoleValue[];
}
