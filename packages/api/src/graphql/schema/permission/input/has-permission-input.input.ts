import { Field, InputType, registerEnumType } from "type-graphql";
import { IsNotEmpty } from "class-validator";
import { PermissionActionsEnum } from "../../../../lib/permissions/enums";
import { PermissionResourceValueType } from "../../../../lib/permissions/types/permission-resource-value.type";
import { EntityData } from "./entity-data.input";
import { ResourceOwnerData } from "./resource-owner-data.input";

registerEnumType(PermissionActionsEnum, {
  name: "PermissionActionsEnum"
});
@InputType()
export class HasPermissionInput {
  @Field((_type) => EntityData, {
    nullable: true
  })
  entityData?: EntityData;

  @Field((_type) => PermissionActionsEnum)
  @IsNotEmpty()
  action!: PermissionActionsEnum;

  @Field((_type) => String, {
    description:
      "This field can be an enum, ID or possibly just a string. Because it is a union we cannot properly display it in apollo graphql"
  })
  @IsNotEmpty()
  resource!: PermissionResourceValueType;

  @Field((_type) => ResourceOwnerData, { nullable: true })
  resourceOwnerData?: ResourceOwnerData;
}
