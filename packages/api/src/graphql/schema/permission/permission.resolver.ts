import { EFPermissionService } from "../../../lib/permissions/ef-permission.service";
import { Resolver, Arg, Query, Ctx, Authorized } from "type-graphql";

import { HasPermissionInput, HasRoleInput } from "./input";
import { IGraphQLContext } from "../../../backend/interfaces";
import { EntityRoleEntityKindEnum } from "../../../lib/permissions/enums";
import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { RoleValue } from "../../../lib/permissions/types/role-value.type";

@Resolver()
export class PermissionResolver {
  private permissionService = EFPermissionService.new();

  @Authorized()
  @Query((_returns) => Boolean)
  public async entityHasPermission(
    @Arg("params") hasPermissionParams: HasPermissionInput,
    @Ctx() ctx: IGraphQLContext
  ): Promise<boolean> {
    const { action, entityData, resource, resourceOwnerData } = hasPermissionParams;

    const id = entityData ? <Types.ObjectId>(<unknown>entityData.entityId) : ctx.userId;
    const kind = entityData ? entityData.entityKind : EntityRoleEntityKindEnum.USERS;

    return this.permissionService.entityHasPermission(
      id,
      kind,
      <string>resource,
      action,
      <Types.ObjectId>(<unknown>resourceOwnerData?.resourceOwner),
      resourceOwnerData?.resourceOwnerKind
    );
  }

  @Authorized()
  @Query((_returns) => Boolean)
  public async entityHasRole(
    @Arg("params") hasRoleParams: HasRoleInput,
    @Ctx() ctx: IGraphQLContext
  ): Promise<boolean> {
    const { roleIds, entityId, roleValues } = hasRoleParams;

    const adjustedId = entityId ?? ctx.userId;

    if ((!roleIds || roleIds.length === 0) && (!roleValues || roleValues.length === 0)) {
      throw Failure.FailedDependency("role id or role value is required. One must be set.", {
        adjustedId,
        roleIds,
        roleValues
      });
    }

    return roleIds && roleIds.length > 0
      ? this.permissionService.entityHasRole(adjustedId, roleIds)
      : this.permissionService.entityHasRoleByRoleValue(adjustedId, <RoleValue[]>roleValues);
  }
}
