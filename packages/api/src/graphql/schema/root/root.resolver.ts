import { Resolver, Query, Mutation } from "type-graphql";

@Resolver()
export class RootResolver {
  @Query((returns) => String, { nullable: true })
  _emptyQuery(): void {}

  @Mutation((returns) => String, { nullable: true })
  _emptyMutation(): void {}
}
