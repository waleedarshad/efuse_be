import { Field, Int, ObjectType, ClassType } from "type-graphql";

export const PaginatedResponse = function <TDocs>(TDocsClass: ClassType<TDocs>): any {
  // `isAbstract` decorator option is mandatory to prevent registering in schema
  @ObjectType({ isAbstract: true })
  abstract class PaginatedResponseClass {
    @Field((_type) => [TDocsClass])
    docs!: TDocs[];

    @Field((_type) => Int)
    totalDocs?: number;

    @Field((_type) => Int)
    limit?: number;

    @Field((_type) => Int)
    page?: number;

    @Field((_type) => Int)
    totalPages?: number;

    @Field((_type) => Int)
    nextPage?: number;

    @Field((_type) => Int)
    prevPage?: number;

    @Field((_type) => Boolean)
    pagingCounter?: boolean;

    @Field((_type) => Boolean)
    hasPrevPage?: boolean;

    @Field((_type) => Boolean)
    hasNextPage?: boolean;
  }

  return PaginatedResponseClass;
};
