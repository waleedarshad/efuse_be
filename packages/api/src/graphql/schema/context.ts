import { IUser } from "../../backend/interfaces/user";

export interface Context {
  user: IUser;
}
