import { Resolver, Query, Ctx, Authorized } from "type-graphql";

import { IGraphQLContext } from "../../../backend/interfaces";

@Resolver()
export class RetoolResolver {
  @Authorized()
  @Query((_returns) => Boolean)
  public async isUserAuthenticated(@Ctx() ctx: IGraphQLContext): Promise<boolean> {
    return ctx.userId != null;
  }
}
