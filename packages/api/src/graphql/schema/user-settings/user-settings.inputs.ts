import { InputType, Field } from "type-graphql";

import { EmailNotificationInput } from "./arguments/email-notification.input";
import { PushNotificationInput } from "./arguments/push-notification.input";
import { MediaSettingsInput } from "./arguments/media-settings.input";
import { UserSettings } from "../../../backend/models/user-settings/user-settings.model";

@InputType("UserNotificationInput", { description: "Input to interact with User setting Model" })
class UserNotificationInput {
  @Field((_type) => EmailNotificationInput, { nullable: true })
  email!: EmailNotificationInput;

  @Field((_type) => PushNotificationInput, { nullable: true })
  push!: PushNotificationInput;
}

@InputType("UserSettingInput", { description: "Input to interact with User setting Model" })
export class UserSettingInput implements Partial<UserSettings> {
  @Field((_type) => UserNotificationInput, { nullable: true })
  notifications?: UserNotificationInput;

  @Field((_type) => Boolean, { nullable: true })
  showStreaks?: Boolean;

  @Field((_type) => MediaSettingsInput, { nullable: true })
  media?: MediaSettingsInput;
}
