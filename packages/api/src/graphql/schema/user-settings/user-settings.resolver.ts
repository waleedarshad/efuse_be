import { Resolver, Query, Ctx, Authorized, Arg, Mutation } from "type-graphql";
import { LeanDocument } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { UserSettings } from "../../../backend/models/user-settings/user-settings.model";
import { UserSettingsService } from "../../../lib/user-settings/user-settings";
import { IGraphQLContext } from "../../../backend/interfaces";
import { UserSettingInput } from "./user-settings.inputs";
@Resolver((_of) => UserSettings)
export class UserSettingsResolver {
  private $userSettingService = new UserSettingsService();

  /**
   * @summary Get current user's settings
   *
   * @authenticated Protected by JWT Token
   *
   * @kind Query
   *
   * @param {IGraphQLContext} ctx
   *
   * @returns {Promise<DocumentType<UserSettings>>}
   */
  @Authorized()
  @Query((_return) => UserSettings)
  async getUserSettings(@Ctx() ctx: IGraphQLContext): Promise<DocumentType<UserSettings>> {
    const userSettings = await this.$userSettingService.get(ctx.userId, {
      notifications: 1,
      user: 1,
      showStreaks: 1,
      media: 1,
      createdAt: 1,
      updatedAt: 1
    });

    return userSettings;
  }

  /**
   * @summary Update user settings
   *
   * @authenticated Protected by JWT Token
   *
   * @kind Mutation
   *
   * @returns {Promise<DocumentType<UserSettings>>}
   */
  @Authorized()
  @Mutation((_return) => UserSettings)
  async updateUserSettings(
    @Ctx() ctx: IGraphQLContext,
    @Arg("data") userSettingsInput: UserSettingInput
  ): Promise<LeanDocument<DocumentType<UserSettings>>> {
    const _userSettingsInput = JSON.parse(JSON.stringify(userSettingsInput));

    const userSettings = await this.$userSettingService.update(ctx.userId, _userSettingsInput, {
      notifications: 1,
      user: 1,
      showStreaks: 1,
      media: 1,
      createdAt: 1,
      updatedAt: 1
    });

    return userSettings;
  }
}
