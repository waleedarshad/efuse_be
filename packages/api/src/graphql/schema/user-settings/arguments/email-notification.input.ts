import { InputType, Field } from "type-graphql";
import {
  ChatNotificationInput,
  PostNotificationInput,
  FollowNotificationInput,
  OpportunityNotificationInput,
  SummaryNotificationInput
} from "./category-inputs";

@InputType("EmailNotificationInput", { description: "Input to interact with User setting Model" })
export class EmailNotificationInput {
  @Field((_type) => ChatNotificationInput, { nullable: true })
  chatNotifications!: ChatNotificationInput;

  @Field((_type) => PostNotificationInput, { nullable: true })
  postNotifications!: PostNotificationInput;

  @Field((_type) => FollowNotificationInput, { nullable: true })
  followNotifications!: FollowNotificationInput;

  @Field((_type) => OpportunityNotificationInput, { nullable: true })
  opportunityNotifications!: OpportunityNotificationInput;

  @Field((_type) => SummaryNotificationInput, { nullable: true })
  summaryNotifications!: SummaryNotificationInput;
}
