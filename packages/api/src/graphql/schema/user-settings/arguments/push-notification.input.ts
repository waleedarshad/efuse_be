import { InputType, Field } from "type-graphql";

import {
  ChatNotificationInput,
  PostNotificationInput,
  FollowNotificationInput,
  OpportunityNotificationInput
} from "./category-inputs";

@InputType("PushNotificationInput", { description: "Input to interact with Clippy Command Model" })
export class PushNotificationInput {
  @Field((_type) => ChatNotificationInput)
  chatNotifications!: ChatNotificationInput;

  @Field((_type) => PostNotificationInput)
  postNotifications!: PostNotificationInput;

  @Field((_type) => FollowNotificationInput)
  followNotifications!: FollowNotificationInput;

  @Field((_type) => OpportunityNotificationInput)
  opportunityNotifications!: OpportunityNotificationInput;
}
