import { InputType, Field } from "type-graphql";
import { MediaProfileEnum } from "@efuse/entities";

@InputType("MediaSettingsInput", { description: "Input to interact with User Setting Model" })
export class MediaSettingsInput {
  @Field((_type) => MediaProfileEnum, { nullable: true })
  profile!: MediaProfileEnum;

  @Field((_type) => Boolean, { nullable: true })
  mute!: Boolean;

  @Field((_type) => Boolean, { nullable: true })
  autoplay!: Boolean;
}
