import { InputType, Field } from "type-graphql";

@InputType("OpportunityNotificationInput", { description: "Input to interact with User Setting Model" })
export class OpportunityNotificationInput {
  @Field((_type) => Boolean)
  newApplicant!: Boolean;
}
