import { InputType, Field } from "type-graphql";

@InputType("ChatNotificationInput", { description: "Input to interact with User Setting Model" })
export class ChatNotificationInput {
  @Field((_type) => Boolean)
  newMessage!: Boolean;
}
