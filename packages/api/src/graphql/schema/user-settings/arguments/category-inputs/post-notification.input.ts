import { InputType, Field } from "type-graphql";

@InputType("PostNotificationInput", { description: "Input to interact with User Setting Model" })
export class PostNotificationInput {
  @Field((_type) => Boolean)
  newHype!: Boolean;

  @Field((_type) => Boolean)
  newComment!: Boolean;

  @Field((_type) => Boolean)
  newShare!: Boolean;
}
