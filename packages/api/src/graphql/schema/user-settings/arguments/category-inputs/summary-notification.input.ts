import { InputType, Field } from "type-graphql";

@InputType("SummaryNotificationInput", { description: "Input to interact with User Setting Model" })
export class SummaryNotificationInput {
  @Field((_type) => Boolean)
  weeklySummary!: Boolean;
}
