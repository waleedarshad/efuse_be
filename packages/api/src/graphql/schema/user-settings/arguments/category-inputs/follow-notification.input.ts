import { InputType, Field } from "type-graphql";

@InputType("FollowNotificationInput", { description: "Input to interact with User Setting Model" })
export class FollowNotificationInput {
  @Field((_type) => Boolean)
  newUserFollow!: Boolean;
}
