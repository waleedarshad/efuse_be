import { Resolver, Ctx, Authorized, Mutation, Args } from "type-graphql";
import { ReportService } from "../../../lib/report/report.service";
import { IGraphQLContext } from "../../../backend/interfaces";
import { Report } from "../../../backend/models/report.model";
import { ReportInput } from "./report.input";

import { Slack } from "../../../lib/slack";
import { DOMAIN } from "@efuse/key-store";
import { ReportEnum } from "@efuse/entities";
import { CommentService } from "../../../lib/comments/comment.service";
import { FeedService } from "../../../lib/feeds/feed.service";
import { UserHelperService } from "../../../lib/users/user-helper.service";
import { OrganizationsService } from "../../../lib/organizations/organizations.service";
import { LoungeFeedService } from "../../../lib/lounge/lounge-feed.service";
import { ILoungeFeed } from "../../../backend/interfaces/lounge-feed";
import { Failure } from "@efuse/contracts";

@Resolver((_of) => Report)
export class ReportResolver {
  private $reportService = new ReportService();
  private $commentService = new CommentService();
  private $feedService = new FeedService();
  private $userHelperService = new UserHelperService();
  private $organizationService = new OrganizationsService();
  private $loungeFeedService = new LoungeFeedService();
  /**
   * @summary Add report
   *
   * @authenticated Protected by JWT token
   *
   * @kind Mutation
   *
   * @returns {Promise<DocumentType<Report>>}
   */
  @Authorized()
  @Mutation((_return) => Report)
  async createReport(@Ctx() ctx: IGraphQLContext, @Args() args: ReportInput): Promise<Report> {
    const user = ctx.user;
    const body = args;

    body.user = user._id;
    const doc = body;
    const report = await this.$reportService.createReport(doc);
    if (report.reportkind === ReportEnum.COMMENT) {
      const comment = await this.$commentService.findOne({ _id: report.reportItem });

      if (!comment) {
        throw Failure.BadRequest("Comment does not exist");
      }

      const post = await this.$feedService.findOne({ _id: comment?.commentable });

      if (!post) {
        throw Failure.BadRequest("Post does not exist");
      }

      const parentFeed: ILoungeFeed | null = await this.$loungeFeedService.findOne(
        { feed: post?._id },
        {},
        { lean: true }
      );

      if (!parentFeed) {
        throw Failure.BadRequest("Post does not exist");
      }

      let creatorLink = "";
      if (parentFeed.timelineableType === "users") {
        const commentUser = await this.$userHelperService.findOne({ _id: comment.user }, { username: 1 }, { lean: 1 });
        creatorLink = `<${DOMAIN}/u/${commentUser?.username}|@${commentUser?.username}>`;
      } else {
        const org = await this.$organizationService.findOne({ _id: parentFeed.timelineable });
        creatorLink = `<${DOMAIN}/org/${org?.shortName}|@${org?.name}>`;
      }
      await Slack.sendMessage(
        `*Reporting user:*  <${DOMAIN}/u/${user.username}|@${user.username}>\n*Comment Creator:*  ${creatorLink}\n*Post link:*  ${DOMAIN}/lounge/featured?feedId=${parentFeed._id}\n*Comment Content:*  ${comment.content}\n*Description:*  ${body.reportReason}`,
        "#community-carl"
      );
    }
    return report;
  }
}
