import { ArgsType, Field } from "type-graphql";
import { Ref } from "@typegoose/typegoose";

import { Report } from "../../../backend/models/report.model";
import { ReportEnum } from "@efuse/entities";

import { IUser } from "../../../backend/interfaces";

@ArgsType()
export class ReportInput implements Partial<Report> {
  user!: Ref<IUser>;

  @Field((_type) => String)
  reportItem!: string;

  @Field((_type) => String)
  reportkind!: ReportEnum;

  @Field((_type) => String, { nullable: true })
  reportReason?: string;
}
