import { Authorized, Ctx, Query, Mutation, Resolver, Arg } from "type-graphql";
import { DocumentType } from "@typegoose/typegoose";

import { BlockedUsersService } from "../../../lib/blocked-users/blocked-users.service";
import { BlockedUser } from "../../../backend/models/blocked-user/blocked-user.model";
import { IGraphQLContext } from "../../../backend/interfaces";
import { BlockedUserInfo } from "../../../backend/models/blocked-user/blocked-user-info.model";

@Resolver((_type) => BlockedUser)
export class BlockedUserResolver {
  private $blockedUserService = new BlockedUsersService();
  /**
   * @summary Get blockedUsers
   *
   * @authenticated Protected by JWT token
   *
   * @kind Query
   *
   * @return {Promise<DocumentType<BlockedUser>>}
   */
  @Authorized()
  @Query((_return) => [BlockedUserInfo])
  async blockedUsers(@Ctx() ctx: IGraphQLContext): Promise<BlockedUserInfo[]> {
    const blockedUsers = await this.$blockedUserService.blockedUserInfo(ctx.userId.toHexString());
    console.log("blockedUsers", blockedUsers);
    return <BlockedUserInfo[]>(<unknown>blockedUsers);
  }

  /**
   * @summary Block User by blockeeId
   *
   * @kind Mutation
   * @authenticated Protected by JWT token
   *
   * @return {Promise<DocumentType<BlockedUser>>}
   */
  @Authorized()
  @Mutation((_return) => BlockedUser)
  async blockUser(
    @Ctx() ctx: IGraphQLContext,
    @Arg("blockeeId") blockeeId: string
  ): Promise<DocumentType<BlockedUser>> {
    const newBlockedUser = {
      blockerId: ctx.userId.toHexString(),
      blockeeId
    } as DocumentType<BlockedUser>;

    const blockedUser = await this.$blockedUserService.create(newBlockedUser);
    await this.$blockedUserService.removeFromFollowers(newBlockedUser);
    return blockedUser;
  }

  /**
   * @summary UnBlock User by blockeeId
   *
   * @kind Mutation
   * @authenticated Protected by JWT token
   *
   * @return {Promise<DocumentType<BlockedUser>>}
   */
  @Authorized()
  @Mutation((_return) => Boolean)
  async unBlockUser(@Ctx() ctx: IGraphQLContext, @Arg("blockeeId") blockeeId: string): Promise<boolean> {
    const query = { blockerId: ctx.userId.toHexString(), blockeeId };
    return await this.$blockedUserService.deleteMany(query);
  }
}
