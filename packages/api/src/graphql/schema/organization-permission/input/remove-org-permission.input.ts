import { InputType } from "type-graphql";

import { BaseOrganizationPermissionInput } from "./base-organization-permission-input";

@InputType("RemoveOrganizationPermissionInput")
export class RemoveOrganizationPermissionInput extends BaseOrganizationPermissionInput {}
