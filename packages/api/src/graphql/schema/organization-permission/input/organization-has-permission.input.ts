import { InputType } from "type-graphql";

import { BaseOrganizationPermissionInput } from "./base-organization-permission-input";

@InputType("OrganizationHasPermissionInput")
export class OrganizationHasPermissionInput extends BaseOrganizationPermissionInput {}
