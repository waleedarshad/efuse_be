import { Field, ID, InputType, registerEnumType } from "type-graphql";
import { IsMongoId } from "class-validator";

import { ObjectId } from "../../../../backend/types";
import { OrganizationPermissionSubjects, PermissionActionsEnum } from "../../../../lib/permissions/enums";

registerEnumType(OrganizationPermissionSubjects, {
  name: "OrganizationPermissionSubjects"
});

registerEnumType(PermissionActionsEnum, {
  name: "PermissionActionsEnum"
});

@InputType()
export class BaseOrganizationPermissionInput {
  @IsMongoId()
  @Field((_type) => ID)
  orgId!: ObjectId;

  @Field((_type) => OrganizationPermissionSubjects)
  object!: OrganizationPermissionSubjects;

  @Field((_type) => PermissionActionsEnum)
  action!: PermissionActionsEnum;
}
