export { AddOrgPermissionInput } from "./add-org-permission.input";
export { OrganizationHasPermissionInput } from "./organization-has-permission.input";
export { RemoveOrganizationPermissionInput } from "./remove-org-permission.input";
