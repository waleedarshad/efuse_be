import { InputType, Field, registerEnumType } from "type-graphql";

import { BaseOrganizationPermissionInput } from "./base-organization-permission-input";
import { OrganizationUserRoleEnum } from "../../../../lib/permissions/enums";

registerEnumType(OrganizationUserRoleEnum, {
  name: "OrganizationUserRoleEnum"
});

@InputType("AddOrgPermissionInput")
export class AddOrgPermissionInput extends BaseOrganizationPermissionInput {
  // TODO: remove roles as we aren't using them until casbin is fixed
  @Field((_type) => [OrganizationUserRoleEnum], { nullable: true })
  roles?: OrganizationUserRoleEnum[];
}
