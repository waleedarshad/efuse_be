import { Resolver, Authorized, Mutation, Arg, Query } from "type-graphql";

import { OrganizationACLService } from "../../../lib/organizations/organization-acl.service";
import { AddOrgPermissionInput } from "./input/add-org-permission.input";
import { OrganizationHasPermissionInput, RemoveOrganizationPermissionInput } from "./input";

@Resolver()
export class OrganizationPermissionResolver {
  private orgAclService = new OrganizationACLService();

  @Authorized()
  @Mutation((_returns) => Boolean)
  public async addOrganizationPermission(@Arg("params") orgPermissionParams: AddOrgPermissionInput): Promise<boolean> {
    const { orgId, object, action } = orgPermissionParams;

    const success = await this.orgAclService.addOrgPermission(<string>orgId, object, action);

    return success;
  }

  @Authorized()
  @Query((_returns) => Boolean)
  public async organizationHasPermission(
    @Arg("params") orgPermissionParams: OrganizationHasPermissionInput
  ): Promise<boolean> {
    const { orgId, object, action } = orgPermissionParams;

    const hasPermission = await this.orgAclService.orgHasPermission(<string>orgId, object, action);

    return hasPermission;
  }

  @Authorized()
  @Mutation((_returns) => Boolean)
  public async removeOrganizationPermission(
    @Arg("params") orgPermissionParams: RemoveOrganizationPermissionInput
  ): Promise<boolean> {
    const { orgId, object, action } = orgPermissionParams;

    const success = await this.orgAclService.removeOrgPermission(<string>orgId, object, action);

    return success;
  }
}
