import { Failure } from "@efuse/contracts";
import { UserStatus } from "@efuse/entities";

import { BaseService } from "../base/base.service";
import { FeedService } from "../feeds/feed.service";
import { IUser } from "../../backend/interfaces/user";
import { Service, Trace } from "../decorators";
import { StreamChatService } from "../streamchat/streamchat.service";
import { UserService } from "./user.service";
import { CommentBaseService } from "../comments/comment-base.service";
import { ObjectId, DeleteResult } from "../../backend/types";

@Service("user-banning.service")
export class UserBanningService extends BaseService {
  private feedService: FeedService;
  private streamchatService: StreamChatService;
  private userService: UserService;
  private $commentBaseService: CommentBaseService;

  constructor() {
    super();

    this.feedService = new FeedService();
    this.streamchatService = new StreamChatService();
    this.userService = new UserService();
    this.$commentBaseService = new CommentBaseService();
  }

  /**
   * Bans the passed user.
   *
   * @param {string} username
   * @return {Promise<IUser>}
   *
   * @memberof UserBanningService
   */
  @Trace()
  public async ban(username: string): Promise<IUser> {
    if (!username) {
      throw Failure.BadRequest("Username is required");
    }

    const updatedUser = await this.changeUserStatus(username, UserStatus.BLOCKED);
    await this.deleteUsersPosts(username);
    await this.deleteUsersComments(username);
    await this.banUserInStreamchat(username);

    return updatedUser;
  }

  /**
   * Unbans the passed user.
   *
   * @param {string} username
   * @return {Promise<IUser>}
   *
   * @memberof UserBanningService
   */
  @Trace()
  public async unban(username: string): Promise<IUser> {
    if (!username) {
      throw Failure.BadRequest("Username is required");
    }

    const updatedUser = await this.changeUserStatus(username, UserStatus.ACTIVE);
    await this.undeleteUsersPosts(username);
    await this.undeleteUsersComments(username);
    await this.unbanUserInStreamchat(username);

    return updatedUser;
  }

  @Trace()
  private async changeUserStatus(username: string, status: UserStatus): Promise<IUser> {
    const userId = await this.getUserId(username);

    const updatedUser = await this.userService.update(userId, { status });

    return updatedUser;
  }

  @Trace()
  private async banUserInStreamchat(username: string): Promise<boolean> {
    const userId = await this.getUserId(username);

    const banned = await this.streamchatService.ban(userId.toString());
    const deactivated = await this.streamchatService.deactivate(userId.toString());

    return banned && deactivated;
  }

  @Trace()
  private async deleteUsersPosts(username: string): Promise<DeleteResult> {
    const userId = await this.getUserId(username);

    const result: DeleteResult = await this.feedService.performSoftDelete(userId);

    return result;
  }

  @Trace()
  private async deleteUsersComments(username: string): Promise<DeleteResult> {
    const userId = await this.getUserId(username);

    const result: DeleteResult = await this.$commentBaseService.performSoftDelete(userId);

    return result;
  }

  @Trace()
  private async undeleteUsersComments(username: string): Promise<DeleteResult> {
    const userId = await this.getUserId(username);

    const result: DeleteResult = await this.$commentBaseService.performSoftUndelete(userId);

    return result;
  }

  @Trace()
  private async unbanUserInStreamchat(username: string): Promise<boolean> {
    const userId = await this.getUserId(username);

    const reactivated = await this.streamchatService.reactivate(userId.toString());
    const unbanned = await this.streamchatService.unban(userId.toString());

    return reactivated && unbanned;
  }

  @Trace()
  private async undeleteUsersPosts(username: string): Promise<any> {
    const userId = await this.getUserId(username);

    const result = await this.feedService.performSoftUndelete(userId);

    return result;
  }

  @Trace()
  private async getUserId(idOrName): Promise<ObjectId> {
    const userId = await this.userService.getUserIdByIdOrUsername(idOrName);

    if (!userId) {
      throw Failure.BadRequest("User is required");
    }
    return userId;
  }
}
