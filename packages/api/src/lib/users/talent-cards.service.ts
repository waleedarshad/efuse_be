import { Model, Types } from "mongoose";

import { BaseService } from "../base/base.service";
import { IFriend } from "../../backend/interfaces/friend";
import { IUser } from "../../backend/interfaces/user";
import { Service, Trace } from "../decorators";
import * as UserLib from ".";
import * as WhitelistHelper from "../../backend/helpers/whitelist_helper";
import { Friend } from "../../backend/models/friend";
import { User } from "../../backend/models/User";

@Service("talent-cards.service")
export class TalentCardsService extends BaseService {
  private readonly users: Model<IUser> = User;

  /**
   * @deprecated In favor of graphQL query GetTalentCards
   */
  @Trace()
  public async randomUsers(currentUserId: Types.ObjectId, limit = 10): Promise<IUser[]> {
    // Converting limit to number
    let sampleSize = Number(limit);

    // Make sure limit is present, valid & non-negative
    if (Number.isNaN(sampleSize) || !sampleSize || sampleSize < 0) {
      sampleSize = 10;
    }

    // Make sure limit is not greater than 30
    if (sampleSize > 30) {
      sampleSize = 30;
    }

    // Fetch existing friends

    const existingFriends = (await Friend.find({ follower: currentUserId }).cache(60)) as IFriend[];

    // Get followee ids array
    const existingFollowees = existingFriends.map((friend) => friend.followee);

    // Make we don't return the user himself
    existingFollowees.push(currentUserId);

    // Get random users
    const users = await this.users.aggregate<IUser>([
      {
        $match: { _id: { $nin: existingFollowees }, deleted: false, status: "Active", portfolioProgress: { $gt: 50 } }
      },
      { $sample: { size: sampleSize } },
      { $lookup: { from: "twitches", localField: "twitch", foreignField: "_id", as: "twitch" } },
      { $lookup: { from: "userstats", localField: "stats", foreignField: "_id", as: "stats" } },
      { $lookup: { from: "pathways", localField: "pathway", foreignField: "_id", as: "pathway" } },
      {
        $project: {
          ...WhitelistHelper.socialAccountLinkFields,
          ...WhitelistHelper.associatedUserFields().$project
        }
      },
      { $unwind: { path: "$twitch", preserveNullAndEmptyArrays: true } },
      { $unwind: { path: "$stats", preserveNullAndEmptyArrays: true } },
      { $unwind: { path: "$pathway", preserveNullAndEmptyArrays: true } }
    ]);

    // Lookup for streaks

    for (const user of users) {
      user.streaks = await UserLib.lookupDailyStreaks(user);
    }

    // return users
    return users;
  }
}
