import { Failure } from "@efuse/contracts";
import { LeanDocument } from "mongoose";
import { Queue } from "bull";

import { UserStatus } from "@efuse/entities";
import { IUser } from "../../backend/interfaces";
import { AlgoliaIndicesEnum } from "../algolia/algolia.enum";
import { AlgoliaService } from "../algolia/algolia.service";
import { Service, Trace } from "../decorators";
import { UserService } from "./user.service";
import Config from "../../async/config";
import SchemaHelper from "../../backend/helpers/schema_helper";
import { ObjectId } from "../../backend/types";

const SERVICE = "user.service";

@Service(SERVICE)
export class UserSearchService extends UserService {
  public $index = `${process.env.NODE_ENV}_${AlgoliaIndicesEnum.USERS}`;

  private $syncUsersWithAlgoliaQueue: Queue;

  private $algoliaService: AlgoliaService;

  constructor() {
    super();

    this.$syncUsersWithAlgoliaQueue = Config.initQueue("syncUsersWithAlgolia");

    this.$algoliaService = new AlgoliaService();
  }

  /**
   * @summary Search User using AlgoliaSearch
   *
   * @param {string} query
   * @param {number} limit
   * @param {string} user
   *
   * @returns {Promise<IUser[]>}
   */
  @Trace()
  public async search(query: string, limit: number, user?: string): Promise<IUser[]> {
    const results = await this.$algoliaService.search<IUser>(this.$index, query, limit, user);

    return results;
  }

  /**
   * @summary Sync with Algolia
   *
   * @param {LeanDocument<IUser>} user
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncWithAlgolia(user: LeanDocument<IUser>): Promise<void> {
    try {
      // Make sure user exists
      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      // Remove if user is blocked or soft deleted
      if (user.status === UserStatus.BLOCKED || user.deleted) {
        this.logger?.info({ _id: user._id }, "Removing user from algolia");

        await this.$algoliaService.deleteObject(this.$index, String(user._id));

        // Short circuit
        return;
      }

      // Make sure user has a profile image uploaded
      if (user.profilePicture?.url === SchemaHelper.fileSchema.url.default) {
        this.logger?.debug("Profile image not uploaded");

        // Short circuit
        return;
      }

      // Sync with algolia
      await this.$algoliaService.syncObject(this.$index, this.$algoliaService.buildUserObject(user));

      this.logger?.info({ _id: user._id }, "Synced user with algolia");
    } catch (error) {
      this.logger?.error(error, "Error while syncing user with algolia");
    }
  }

  /**
   * @summary Sync with Algolia using bull queue
   *
   * @param {LeanDocument<IUser>} user
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncWithAlgoliaAsync(user: LeanDocument<IUser>): void {
    this.$syncUsersWithAlgoliaQueue
      .add({ user })
      .catch((error) => this.logger?.error(error, "Error while adding to queue syncUsersWithAlgoliaQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const userSearchService = new UserSearchService();

    // syncUsersWithAlgoliaQueue
    userSearchService.$syncUsersWithAlgoliaQueue
      .process((job: { data: { user: LeanDocument<IUser> } }) => {
        return userSearchService.syncWithAlgolia(job.data.user);
      })
      .catch((error) => userSearchService.logger?.error(error, "Error while processing syncUsersWithAlgoliaQueue"));
  }

  /**
   * @summary Remove from algolia
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async deleteFromAlgolia(userId: ObjectId): Promise<void> {
    this.logger?.info({ userId }, "Removing user from algolia");

    await this.$algoliaService.deleteObject(this.$index, String(userId));
  }

  /**
   * @summary Remove from algolia asynchronously using bull queue
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public deleteFromAlgoliaAsync(userId: ObjectId): void {
    this.logger?.info({ userId }, "Removing user from algolia");

    this.$algoliaService.deleteObjectAsync(this.$index, <string>userId);
  }
}
