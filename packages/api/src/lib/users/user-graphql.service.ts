import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { isEmpty } from "lodash";

import { Service, Trace } from "../decorators";
import { UserService } from "./user.service";
import { IUser } from "../../backend/interfaces";
import { UserValidationService } from "./user-validation.service";
import { UserFields } from "./user-fields";
import { StreamChatService } from "../../lib/streamchat/streamchat.service";

@Service("user-graphql.service")
export class UserGraphqlService extends UserService {
  private $userValidationService: UserValidationService;
  private $streamChatService: StreamChatService;

  constructor() {
    super();

    this.$userValidationService = new UserValidationService();
    this.$streamChatService = new StreamChatService();
  }

  /**
   * @summary Update user
   *
   * @param {Types.ObjectId | string} userId
   * @param {Partial<IUser>} userBody
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async updateUser(userId: Types.ObjectId | string, userBody: Partial<IUser>): Promise<IUser> {
    // Make sure params are passed
    if (isEmpty(userBody)) {
      throw Failure.BadRequest("body is missing");
    }

    // Get user object
    const user = await this.fetchUser(userId);

    const {
      email: emailParam,
      username: usernameParam,
      name: nameParam,
      dateOfBirth: dateOfBirthParam,
      ...bodyWithoutRequiredFields
    } = userBody;

    const params: Partial<IUser> = bodyWithoutRequiredFields;

    // Validate username
    if (!isEmpty(usernameParam)) {
      const usernameToBeUpdated = await this.$userValidationService.prepareUsernameForUpdate(
        userId,
        usernameParam as string,
        user.username,
        user.lastUsernameUpdatedAt
      );

      if (usernameToBeUpdated) {
        params.username = usernameToBeUpdated;
        params.lastUsernameUpdatedAt = new Date();
      }
    }

    // Validate email
    if (!isEmpty(emailParam)) {
      const emailToBeUpdated = await this.$userValidationService.prepareEmailForUpdate(
        userId,
        emailParam as string,
        user.email
      );

      if (emailToBeUpdated) {
        params.email = emailToBeUpdated;
      }
    }

    // Validate name
    if (!isEmpty(nameParam)) {
      const nameToBeUpdated = this.$userValidationService.validateName(nameParam as string, user.name);

      if (nameToBeUpdated) {
        params.name = nameToBeUpdated;
      }
    }

    // Validate dateOfBirth
    if (!isEmpty(dateOfBirthParam)) {
      this.$userValidationService.validateDateOfBirth(dateOfBirthParam as string);

      params.dateOfBirth = dateOfBirthParam;
    }

    // Update user
    const updatedUser: any = await this.users
      .findOneAndUpdate({ _id: user._id }, params, { new: true })
      .select(UserFields.JWT_USER_FIELDS);

    // Make sure user exists
    if (!updatedUser) {
      throw Failure.UnprocessableEntity("User not found");
    }

    await this.$streamChatService.updateStreamChatUser(
      updatedUser._id,
      updatedUser.name,
      updatedUser.profilePicture?.url
    );

    return updatedUser;
  }

  /**
   * @summary Fetch user to be updated
   *
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IUser>}
   */
  private async fetchUser(userId: Types.ObjectId | string): Promise<IUser> {
    const user: IUser | null = await this.users.findOne({ _id: userId }).select(UserFields.JWT_USER_FIELDS);

    if (!user) {
      throw Failure.UnprocessableEntity("User not found.");
    }

    return user;
  }
}
