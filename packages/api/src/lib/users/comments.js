const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();

const { io } = require("../../backend/config/get-io-object");
const { HomeFeed } = require("../../backend/models/HomeFeed");
const { GeneralHelper } = require("../../backend/helpers/general.helper");
const CommentAsyncQueue = require("../../async/core/comments");

/**
 * @summary Toggle comment like
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} commentId - Id of comment
 * @param {Boolean} isLiked - True or false
 * @param {Number} value - 1 for like and -1 for unlike
 * @return {Promise}
 */
const initToggleCommentLike = async (feedId, userId, commentId, isLiked, value) => {
  const query = {
    user: { $ne: GeneralHelper.getObjectId(userId) },
    feed: feedId
  };
  try {
    const homeFeeds = await HomeFeed.find(query, {}, { autopopulate: false }).lean();
    if (!homeFeeds || homeFeeds.length === 0) {
      logger.info(query, "HomeFeed not found!");
      return;
    }
    await Promise.all(
      homeFeeds.map(async (homeFeed) => {
        await toggleCommentLikeAsync(commentId, isLiked, value, homeFeed._id, homeFeed.user);
      })
    );

    return;
  } catch (error) {
    logger.error(error, "Error toggling comment like");
  }
};

/**
 * @summary Emit notification of toggle comment like
 *
 * @param {ObjectId} commentId - Id of comment
 * @param {Boolean} isLiked - True or false
 * @param {Number} value - 1 for like and -1 for unlike
 * @param {ObjectId} homeFeedId - Id of feed
 * @param {ObjectId} user - Id of user
 */
const toggleCommentLike = async (commentId, isLiked, value, homeFeedId, user) => {
  await io.to(user).emit("BUMP_COMMENTS_LIKE", { commentId, isLiked, value, homeFeedId });
};

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} commentId - Id of comment
 * @param {Boolean} isLiked - True or false
 * @param {Number} value - 1 for like and -1 for unlike
 */
const initToggleCommentLikeAsync = async (feedId, userId, commentId, isLiked, value) => {
  return CommentAsyncQueue.initToggleCommentLikeQueue.add({
    feedId,
    userId,
    commentId,
    isLiked,
    value
  });
};

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} commentId - Id of comment
 * @param {Boolean} isLiked - True or false
 * @param {Number} value - 1 for like and -1 for unlike
 * @param {ObjectId} homeFeedId - Id of feed
 * @param {ObjectId} user - Id of user
 */
const toggleCommentLikeAsync = async (commentId, isLiked, value, homeFeedId, user) => {
  return CommentAsyncQueue.toggleCommentLikeQueue.add({
    commentId,
    isLiked,
    value,
    homeFeedId,
    user
  });
};

/**
 * @summary Create a new comment on post
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {Object} comment - Comment data
 * @return {Promise}
 */
const addCommentToFeed = async (feedId, userId, comment) => {
  try {
    const homeFeeds = await HomeFeed.find(
      {
        user: { $ne: GeneralHelper.getObjectId(userId) },
        feed: feedId,
        deleted: { $ne: true }
      },
      {},
      { autopopulate: false }
    );
    if (!homeFeeds || homeFeeds.length === 0) {
      return;
    }
    await Promise.all(
      homeFeeds.map((homeFeed) => {
        return io.to(homeFeed.user).emit("COMMENT_ON_FEED", {
          feedId,
          ...comment,
          homeFeedId: homeFeed._id
        });
      })
    );

    return;
  } catch (error) {
    logger.error(error, "Error adding comment to Feed");
  }
};

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {Object} comment - Comment data
 */
const addCommentToFeedAsync = async (feedId, userId, comment) => {
  const result = await CommentAsyncQueue.addCommentToFeedQueue.add({
    feedId,
    userId,
    comment
  });

  return result;
};

/**
 * @summary Delete a comment on post
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} commentId - Id of comment
 * @return {Promise}
 */
const removeCommentFromFeed = async (feedId, userId, commentId) => {
  try {
    const homeFeeds = await HomeFeed.find(
      {
        user: { $ne: GeneralHelper.getObjectId(userId) },
        feed: feedId,
        deleted: { $ne: true }
      },
      {},
      { autopopulate: false }
    ).lean();
    if (!homeFeeds || homeFeeds.length === 0) {
      return;
    }
    await Promise.all(
      homeFeeds.map((homeFeed) => {
        return io.to(homeFeed.user).emit("DELETE_COMMENT", { feedId, commentId });
      })
    );

    return;
  } catch (error) {
    logger.error(error, "Error removing comment from Feed");
  }
};

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} commentId - Id of comment
 */
const removeCommentFromFeedAsync = async (feedId, userId, commentId) => {
  const result = await CommentAsyncQueue.removeCommentFromFeedQueue.add({
    feedId,
    userId,
    commentId
  });

  return result;
};

/**
 * @summary Delete a comment on post
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} commentId - Id of comment
 * @param {Object} commentData - Comment data
 * @return {Promise}
 */
const updateCommentOnFeed = async (feedId, userId, commentId, commentData) => {
  try {
    const homeFeeds = await HomeFeed.find(
      {
        user: { $ne: GeneralHelper.getObjectId(userId) },
        feed: feedId,
        deleted: false
      },
      {},
      { autopopulate: false }
    ).lean();
    if (!homeFeeds || homeFeeds.length === 0) {
      return;
    }
    await Promise.all(
      homeFeeds.map((homeFeed) => {
        return io.to(homeFeed.user).emit("EDIT_COMMENT", {
          feedId,
          commentId,
          commentData,
          updated: true
        });
      })
    );

    return;
  } catch (error) {
    logger.error(error, "Error updating comment on Feed");
  }
};

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} feedId - Id of feed
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} commentId - Id of comment
 * @param {Object} commentData - Comment data
 */
const updateCommentOnFeedAsync = async (feedId, userId, commentId, commentData) => {
  const result = await CommentAsyncQueue.updateCommentOnFeedQueue.add({
    feedId,
    userId,
    commentId,
    commentData
  });

  return result;
};

module.exports.addCommentToFeed = addCommentToFeed;
module.exports.addCommentToFeedAsync = addCommentToFeedAsync;
module.exports.initToggleCommentLike = initToggleCommentLike;
module.exports.initToggleCommentLikeAsync = initToggleCommentLikeAsync;
module.exports.removeCommentFromFeed = removeCommentFromFeed;
module.exports.removeCommentFromFeedAsync = removeCommentFromFeedAsync;
module.exports.toggleCommentLike = toggleCommentLike;
module.exports.toggleCommentLikeAsync = toggleCommentLikeAsync;
module.exports.updateCommentOnFeed = updateCommentOnFeed;
module.exports.updateCommentOnFeedAsync = updateCommentOnFeedAsync;
