import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { DocumentType } from "@typegoose/typegoose";

import { Service, Trace } from "../decorators";
import { GameBaseService } from "../game/game-base.service";
import { UserGameBaseService } from "./user-game-base.service";
import { UserService } from "./user.service";
import { IUser } from "../../backend/interfaces/user";
import { Game } from "../../backend/models/game.model";

@Service("user-game.service")
export class UserGameService extends UserGameBaseService {
  private $gameBaseService: GameBaseService;
  private $userService: UserService;

  constructor() {
    super();

    this.$gameBaseService = new GameBaseService();
    this.$userService = new UserService();
  }

  /**
   * @summary Associate games with the user
   *
   * @param {Types.ObjectId | string} userId
   * @param {Array<Types.ObjectId | string>} gameIds
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async associateGames(
    userId: Types.ObjectId | string,
    gameIds: Array<Types.ObjectId | string>
  ): Promise<IUser> {
    // Make sure gameIds are valid object ids
    gameIds.forEach((gameId) => {
      if (!this.isValidObjectId(gameId)) {
        throw Failure.BadRequest("Invalid game ID");
      }
    });

    // Fetch User
    const user = await this.$userService.findOne({ _id: userId });

    if (!user) {
      throw Failure.BadRequest("User not found");
    }

    // Make sure game exists in our DB
    const games = await this.$gameBaseService.find({ _id: { $in: gameIds } });

    // IDs that exist in our DB
    const idsToStore: string[] = games.map((game) => String(game._id));

    // Associate with user & Store in DB
    await this.bulkUpdate(userId, idsToStore);

    return user;
  }

  /**
   * @summary Get games associated to user
   *
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<Game[]>}
   */
  @Trace()
  public async getAssociatedGames(userId: Types.ObjectId | string): Promise<DocumentType<Game>[]> {
    const organizationGames = await this.find({ user: userId });

    const games = await this.$gameBaseService.find({ _id: { $in: organizationGames.map((og) => og.game) } });

    return games;
  }
}
