const bcrypt = require("bcryptjs");
const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();

const { Secure } = require("../../backend/models/Secure");
const { User } = require("../../backend/models/User");
const { Feed } = require("../../backend/models/Feed");
const { HomeFeed } = require("../../backend/models/HomeFeed");
const { LoungeFeed } = require("../../backend/models/LoungeFeed");
const { Comment } = require("../../backend/models/comment");
const { Streak } = require("../../backend/models/streak");

const { sendEmail } = require("../email");
const { GeneralHelper } = require("../../backend/helpers/general.helper");
const { allowedUserFields } = require("../../backend/helpers/whitelist_helper");
const { calculatePortfolioProgress } = require("./portfolio-progress");

const { initQueue } = require("../../async/config");

const { GamePlatformsService } = require("../game_platforms/game-platforms.service");
const LeaderboardLib = require("../leaderboards");
const { FeedRankService } = require("../lounge/feed-rank.service");
const { UserBadgeService } = require("../badge/user-badge.service");
const { UserGameService } = require("./user-game.service");

const { DOMAIN } = require("@efuse/key-store");
const feedRankService = new FeedRankService();
const gamePlatformsService = new GamePlatformsService();
const { HypeEnum } = require("@efuse/entities");
const { HypeService } = require("../hype/hype.service");

const $hypeService = new HypeService()

/**
 * @summary Get password of user
 *
 * @param {ObjectId} userId - Id of user
 * @return {Promise<string | undefined>}
 */
const getPassword = async (userId) => {
  const context = { user: userId };
  try {
    const secureObject = await Secure.findOne({ user: userId }).lean();
    const logMsg = `[user: ${userId}]:`;

    if (!secureObject) {
      logger.info(`${logMsg} Password not found!`);
      logger.info(`${logMsg} Migrating Password!`);

      // todo: investigate
      // @ts-ignore
      const user = await User.findOne({ _id: userId }).select("password");

      const newSecureObject = await new Secure({
        user: userId,
        // todo: investigate
        // @ts-ignore
        password: user.password
      }).save();
      // todo: investigate
      // @ts-ignore
      user.passwordMigrated = true;
      // todo: investigate
      // @ts-ignore
      user.passwordMigratedAt = Date.now();
      // todo: investigate
      // @ts-ignore
      await user.save({ validateBeforeSave: false });

      return newSecureObject.password;
    }

    return secureObject.password;
  } catch (error) {
    const message = error.message || error;
    logger.error("Error fetching password", message);
    context.message = message;

    // todo: investigate
    // @ts-ignore
    buildError(error, 500, context);
    return;
  }
};

/**
 * @summary Set password of user
 *
 * @param {ObjectId} userId - Id of user
 * @param {String} password - Password to set
 * @return {Promise}
 */
const setPassword = async (userId, password) => {
  const context = { user: userId };
  const errorMsg = `[user: ${userId}] Error setting password | bcrypt`;
  try {
    return new Promise(async (resolve, reject) => {
      return bcrypt.hash(password, 10, async (err, hash) => {
        if (err) {
          logger.error(errorMsg, err);
          return reject(err);
        }

        // Set the password in an existing secure object
        const secureObject = await Secure.findOne({ user: userId });
        if (secureObject) {
          secureObject.history.push({
            password: secureObject.password,
            createdAt: secureObject.updatedAt.toISOString(),
            updatedAt: Date.now()
          });
          secureObject.password = hash;
          try {
            await secureObject.save();
            return resolve(true);
          } catch (error) {
            const message = error.message || error;
            logger.error(errorMsg, message);
            return reject("Something went wrong!");
          }
        }
        try {
          // If secure object doesn't exist create a new one
          await new Secure({ password: hash, user: userId }).save();
          return resolve(true);
        } catch (error) {
          const message = error.message || error;
          logger.error(errorMsg, message);
          return reject("Something went wrong!");
        }
      });
    });
  } catch (error) {
    context.message = errorMsg;
    logger.error(errorMsg, error.message || error);

    buildError(error, error.status, context);
  }
};

/**
 * @summary Match password of user
 *
 * @param {ObjectId} userId - Id of user
 * @param {String} password - Password to match
 * @return {Promise}
 */
const matchPassword = async (userId, password) => {
  const context = { user: userId };
  const errorMsg = `[user: ${userId}] Error comparing password | bcrypt`;
  try {
    return new Promise(async (resolve, reject) => {
      const passwordHash = await getPassword(userId);
      if (!passwordHash) {
        logger.error(`${errorMsg} | Password Hash not found`);
        return resolve(false);
      }
      return bcrypt.compare(password, passwordHash, async (err, matched) => {
        if (err) {
          logger.error(errorMsg, err);
          return reject(err);
        }
        return resolve(matched);
      });
    });
  } catch (error) {
    context.message = errorMsg;
    logger.error(errorMsg, error);

    buildError(error, error.status, context);
  }
};

/**
 * @summary Create error object
 *
 * @param {Object} error - Error object
 * @param {Number} status - Request status
 * @param {Record<string, unknown>} context - Error info
 * @return {Object}
 */
const buildError = (error, status, context) => {
  const errorObject = { ...error, context, status };

  throw errorObject;
};

/**
 * @summary Get users profile by query
 *
 * @param {Object} query - Query to search user with
 * @param {Boolean} includeSensitiveInfo
 * @return {Promise<any>}
 */
const getFullUserProfileByQuery = async (query, includeSensitiveInfo = false) => {
  const user = await User.findOne(query).select("_id").lean();
  if (!user) {
    return user;
  }

  // todo: investigate
  // @ts-ignore
  return await getFullUserProfileById(user._id, includeSensitiveInfo);
};

/**
 * @summary Get users profile by username
 *
 * @param {String} username - username to search user with
 * @param {Boolean} includeSensitiveInfo
 * @return {Promise<any>}
 */
const getFullUserProfileByUsername = async (username, includeSensitiveInfo = false) => {
  return await getFullUserProfileByQuery({ username }, includeSensitiveInfo);
};

/**
 * @summary Get users profile by user id
 *
 * @param {ObjectId} userId - Id to search user with
 * @param {Boolean} includeSensitiveInfo
 * @return {Promise<any>}
 */
const getFullUserProfileById = async (userId, includeSensitiveInfo = false) => {
  let select = "+showOnline +showOnlineMobile +googleVerified";
  if (includeSensitiveInfo) {
    select =
      "+email +gender +dateOfBirth +zipCode +address +streamChatToken +chatNotification +followsNotification +likeNotification +repliesNotification +commentsNotification +sharesNotification +mentionsNotification +postOrganizationNotification +opportunityOrganizationNotification +applyNotification +organizationInvitationNotification +newsOrganizationNotification +hypedPostCommentNotification +showOnline +chatNotificationMobile +followsNotificationMobile +likeNotificationMobile +repliesNotificationMobile +commentsNotificationMobile +sharesNotificationMobile +mentionsNotificationMobile +postOrganizationNotificationMobile +opportunityOrganizationNotificationMobile +applyNotificationMobile +organizationInvitationNotificationMobile +newsOrganizationNotificationMobile +hypedPostCommentNotificationMobile +showOnlineMobile +googleVerified +google +google.id";
  }

  // todo: investigate
  // @ts-ignore
  const user = await User.findOne({ _id: userId, status: { $ne: "Blocked" } })
    .select(select)
    .populate({ path: "mainOrganization" })
    .populate({ path: "steam" })
    .populate({ path: "twitch" })
    .populate({ path: "xbox" })
    .populate({ path: "leagueOfLegends.stats" })
    .populate({ path: "overwatch.stats" })
    .populate({ path: "rocketLeague.stats" })
    .populate({ path: "pubg.stats" })
    .populate({ path: "stats" })
    .populate({ path: "pathway" })
    .populate({ path: "youtubeChannel" })
    .lean();
  // .cache(0, getUserCacheKey(userId, includeSensitiveInfo));

  if (!user) {
    return null;
  }

  // Tacking on user streaks
  user.streaks = await lookupDailyStreaks(user);

  // Get GamePlatforms
  user.gamePlatforms = [];

  // todo: investigate
  // @ts-ignore
  const gamePlatforms = await gamePlatformsService.getGamePlatforms(user._id);
  if (gamePlatforms) {
    user.gamePlatforms = gamePlatforms;
  }

  // Leaderboard Lookup
  await lookupLeadboard(user);

  // User Badges
  await lookupUserBadges(user);

  // User Games
  await lookupUserGames(user);

  return user;
};

/**
 * @summary Send verification email
 *
 * @param {String} email - Email address of user
 */
const sendEmailVerificationEmail = async (email) => {
  try {
    const user = await User.findOne({ email });

    if (!user) throw "User not found with the given email.";

    const token = GeneralHelper.generateToken();
    user.emailVerificationToken = token;

    const dt = new Date();

    // todo: investigate
    // @ts-ignore
    user.emailVerificationExpires = dt.setHours(dt.getHours() + 24); // expires after 24 hour

    await user.save({ validateBeforeSave: false });

    return await sendEmail(email, "d-21196a1d789a4d9d877d89311c232378", {
      resetPasswordURL: `https://efuse.gg/verify_email?token=${token}`
    });
  } catch (error) {
    logger.info("error in user library - sendEmailVerificationEmail(): ", error);
    throw error;
  }
};

/**
 * @summary Update user portfolio progress
 *
 * @param {Object} userId - Id of user
 */
const updateUserPortfolioProgress = async (userId) => {
  try {
    const user = await User.findOne({ _id: userId });
    if (user) {
      const calculatedProgress = await calculatePortfolioProgress(user);
      user.portfolioProgress = calculatedProgress;
      // todo: investigate
      // @ts-ignore
      user.portfolioProgressUpdatedAt = Date.now();
      await user.save({ validateBeforeSave: false });
      logger.info(`User portfolio progress successfully updated as ${calculatedProgress}`);
    } else {
      logger.info(`No user found: ${userId}`);
    }
  } catch (error) {
    logger.error(error, "Error while updating the portfolio progress of user");
  }
};

/**
 * @summary Remove feeds on blocking user
 *
 * @param {Object} userId - Id of user
 * @return {Promise}
 */
const removeFeedsOnUserBlock = async (userId) => {
  try {
    const feeds = await Feed.find({ user: userId });
    if (feeds.length === 0) {
      return;
    }
    for (const feed of feeds) {
      try {
        await Promise.all([
          // todo: investigate
          // @ts-ignore
          LoungeFeed.delete({ feed: feed.id }),
          // todo: investigate
          // @ts-ignore
          HomeFeed.delete({ feed: feed.id }),
          $hypeService.deleteOne({ objId: feed.id, objType: HypeEnum.FEED }),
          Comment.delete({
            commentable: feed.id,
            commentableType: "feeds"
          }),
          feed.delete(),
          feedRankService.removeObject(feed.id)
        ]);
      } catch (error) {
        logger.error(error, "Error deleting the feed for blocked user");
      }
    }
  } catch (error) {
    logger.error(error, "Error removing the blocked the user feeds");
  }
};

const portfolioProgressQueue = initQueue("updatePortfolioProgressQueue", {
  limiter: {
    max: 5,
    duration: 1000 // Let's only run 5 per second so as to not overload the database
  }
});

const removeFeedsOnUserBlockQueue = initQueue("removeFeedsOnUserBlock");

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} userId - Id of user
 */
const updateUserPortfolioProgressAsync = (userId) => {
  portfolioProgressQueue.add(userId);
};

/**
 * @summary Async method to add job to queue
 *
 * @param {ObjectId} userId - Id of user
 */
const removeFeedsOnUserBlockAsync = async (userId) => {
  removeFeedsOnUserBlockQueue.add({ userId });
};

const initAsyncProcessors = () => {
  portfolioProgressQueue.process(async (job) => {
    await updateUserPortfolioProgress(job.data);
  });
  removeFeedsOnUserBlockQueue.process(async (job) => {
    const { userId } = job.data;
    await removeFeedsOnUserBlock(userId);
  });
};

/**
 * The singular function that should be used to get a single user by their ID
 * @param {String} userId
 * @param {Object} opts
 */
const getUserById = async (userId, opts = {}) => {
  const {
    disableCache = false,
    populate = [],
    dailyStreak = false,
    projection = {},
    portfolioExperience = false,
    leaderboard = false,
    userBadges = false
  } = opts;

  let user = User.findById(userId, projection).select(allowedUserFields);
  // Add in all of the populates
  for (const collection of populate) {
    user = user.populate(collection);
  }

  // Making sure pathway is always populated
  user = user.populate("pathway");

  // By default cache for 30 seconds unless requestor explictly asked for a non-cached result
  if (!disableCache) {
    user = user.cache(60);
  }

  if (dailyStreak || portfolioExperience || leaderboard || userBadges) {
    // todo: investigate
    // @ts-ignore
    user = await user.lean();
    if (!user) {
      logger.info({ userId }, "User not found");
      return;
    }

    // Lookup streaks
    if (dailyStreak) {
      // todo: investigate
      // @ts-ignore
      user.streaks = await lookupDailyStreaks(user);
    }

    // Leaderboard lookup
    if (leaderboard) {
      await lookupLeadboard(user);
    }

    // User Badges
    if (userBadges) {
      await lookupUserBadges(user);
    }
  }

  return user;
};

/**
 * @summary Lookup daily streaks
 *
 * @param {Object} user - User info i.e id, name etc
 * @return {Promise<any>}
 */
const lookupDailyStreaks = async (user) => {
  const streaks = {};
  if (!user) {
    return streaks;
  }
  const dailyStreaks = [
    { key: "daily", value: "dailyUsage" },
    { key: "dailyComment", value: "dailyComment" }
  ];

  for (const dailyStreak of dailyStreaks) {
    const streak = await Streak.findOne({
      user: user._id,
      streakName: dailyStreak.value,
      isActive: true
    })
      .sort({ updatedAt: -1 })
      .select("streakDuration isActive -_id")
      .lean()
      // todo: investigate
      // @ts-ignore
      .cache(60);

    streaks[dailyStreak.key] = streak ? streak : { isActive: false };
  }

  return streaks;
};

/**
 * @summary Get user cache key
 *
 * @param {ObjectId} userId - Id of user
 * @param {Object} sensitiveInfo
 * @return {String}
 */
const getUserCacheKey = (userId, sensitiveInfo) =>
  `cachegoose:uid:${userId}:sensitiveInfo:${sensitiveInfo}:fullProfile`;

/**
 * @summary Remove third party auth token
 *
 * @param {ObjectId} userId - Id of user
 * @param {String} service
 * @return {Promise}
 */
const removeThirdPartyAuthToken = async (userId, service) => {
  return new Promise(async (resolve, reject) => {
    try {
      const secureObject = await Secure.findOne({ user: userId }).select(service);
      if (!secureObject) {
        return reject({ message: "Secure object not found!", status: 422 });
      }
      secureObject[service] = undefined;
      await secureObject.save({ validateBeforeSave: false });
      return resolve(true);
    } catch (error) {
      logger.error({ message: error.message }, `Error while removing ${service} tokens`);
      return reject(error);
    }
  });
};

/**
 * @summary Lookup user's Leaderboard position
 *
 * @param {Object} user - User info i.e id, name etc
 */
const lookupLeadboard = async (user) => {
  user.leaderboard = {};
  user.activeLeaderboard = {};
  try {
    user.leaderboard = await LeaderboardLib.deprecatedGetLeaderboardByUserId(user._id);
    user.activeLeaderboard = await LeaderboardLib.getLeaderboardByUserId(user._id);
  } catch (error) {
    logger.error(error, "Error in lookupLeaderboard");
    user.leaderboard = {};
  }
};

/**
 * @summary Send reset password instructions email
 *
 * @param {Object} recipient - User object
 * @param {String} token - Reset password token
 */
const sendResetPasswordEmail = async (recipient, token) => {
  const emailData = {
    subject: "Reset Password Instructions",
    preHeader: "Reset Password Instructions",
    body: `<p style="padding:10px 20px; font-size:16px; color:#a9b5c4; line-height:22px;">
      Please click on the link below to reset your password. <br>
      <a href='${DOMAIN}?token=${token}' style='color:#fff;'>
        Reset Your Password
      </a>
    </p>`
  };
  return await sendEmail(recipient.email, "d-d92d1cb614c943d4824d3e9f1995eaaa", emailData);
};

/**
 * @summary Lookup userBadges
 *
 * @param {Object} user - User info i.e id, name etc
 * @return {Promise<any>}
 */
const lookupUserBadges = async (user) => {
  user.badges = [];
  try {
    const userBadgeService = new UserBadgeService();

    user.badges = await userBadgeService.findAllByUserId(user._id);
  } catch (error) {
    logger.error(error, "Error in lookupUserBadges");
    user.badges = [];
  }
};

/**
 * @summary Lookup user games
 *
 * @param {Object} user - User info i.e id, name etc
 * @return {Promise<any>}
 */
const lookupUserGames = async (user) => {
  user.associatedGames = [];
  try {
    const userGameService = new UserGameService();

    user.associatedGames = await userGameService.getAssociatedGames(user._id);
  } catch (error) {
    logger.error(error, "Error in lookupUserGames");
    user.associatedGames = [];
  }
};

module.exports.getFullUserProfileById = getFullUserProfileById;
module.exports.getFullUserProfileByQuery = getFullUserProfileByQuery;
module.exports.getFullUserProfileByUsername = getFullUserProfileByUsername;
module.exports.getUserById = getUserById;
module.exports.getUserCacheKey = getUserCacheKey;
module.exports.initAsyncProcessors = initAsyncProcessors;
module.exports.lookupDailyStreaks = lookupDailyStreaks;
module.exports.lookupLeadboard = lookupLeadboard;
module.exports.lookupUserBadges = lookupUserBadges;
module.exports.lookupUserGames = lookupUserGames;
module.exports.matchPassword = matchPassword;
module.exports.removeFeedsOnUserBlock = removeFeedsOnUserBlock;
module.exports.removeFeedsOnUserBlockAsync = removeFeedsOnUserBlockAsync;
module.exports.removeThirdPartyAuthToken = removeThirdPartyAuthToken;
module.exports.sendEmailVerificationEmail = sendEmailVerificationEmail;
module.exports.sendResetPasswordEmail = sendResetPasswordEmail;
module.exports.setPassword = setPassword;
module.exports.updateUserPortfolioProgress = updateUserPortfolioProgress;
module.exports.updateUserPortfolioProgressAsync = updateUserPortfolioProgressAsync;
