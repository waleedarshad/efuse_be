import { Types } from "mongoose";
import { isEmpty } from "lodash";
import { Failure } from "@efuse/contracts";
import moment from "moment";
import Validator from "validator";

import { Service, Trace } from "../decorators";
import { UserService } from "./user.service";
import { BAD_WORDS } from "../../backend/data/bad-words";
import { TOP_FOLLOWERS } from "../../backend/data/top-followers";
import { IUser, IValidationResponse } from "../../backend/interfaces";
import { EmailEnum, UsernameEnum } from "./user.enum";

@Service("user-validation.service")
export class UserValidationService extends UserService {
  /**
   * @summary Validate username to make sure it is unique & matches eFuse standards
   *
   * @param {string} username
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IValidationResponse>}
   */
  @Trace()
  public async validateUsername(username: string, userId: Types.ObjectId | string = ""): Promise<IValidationResponse> {
    const incomingUsername = username.trim();
    const usernameRegex = /^[\w-]*$/;
    let error = "";
    let valid = true;

    if (isEmpty(incomingUsername)) {
      error = "Username is required";
      valid = false;
    } else if (!usernameRegex.test(incomingUsername)) {
      error = "Username has invalid characters. Allowed: a-Z, 0-9, _";
      valid = false;
    } else if (incomingUsername.length < 4) {
      error = "Username must be at least 4 characters";
      valid = false;
    } else if (TOP_FOLLOWERS.includes(incomingUsername)) {
      error = "Username has already been taken";
      valid = false;
    } else if (BAD_WORDS.includes(incomingUsername.toLowerCase())) {
      error = "Sexual or offensive words are not allowed";
      valid = false;
    } else {
      const user: IUser | null = await this.users
        .findOneWithDeleted({ username: incomingUsername }, { username: 1 })
        .lean();

      // Check whether a user with this username exists and that it isn't the current user
      // signupbot is the dedicated username for Cross Browser Testing automated tests
      if (user && String(user._id) !== String(userId) && username !== UsernameEnum.SIGNUP_BOT) {
        error = "Username has already been taken";
        valid = false;
      }
    }

    return { valid, error };
  }

  /**
   * @summary Validate & make sure user name can be updated
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} username
   * @param {string} existingUsername
   * @param {Date | undefined} lastUsernameUpdatedAt
   *
   * @returns {Promise<string | undefined>}
   */
  @Trace()
  public async prepareUsernameForUpdate(
    userId: Types.ObjectId | string,
    username: string,
    existingUsername: string,
    lastUsernameUpdatedAt: Date | undefined
  ): Promise<string | undefined> {
    // Make sure presence of username
    if (!username) {
      throw Failure.BadRequest("username is required");
    }

    // Trim it before saving
    const usernameToBeSaved = username.trim();

    // Only process when a new username is passed
    if (existingUsername === usernameToBeSaved) {
      return;
    }

    // Make sure it is a valid username
    const { valid, error } = await this.validateUsername(username, userId);

    if (!valid) {
      throw Failure.BadRequest(error);
    }

    // Build dates
    const updatedAt = moment(lastUsernameUpdatedAt);
    const daysDifference = moment().diff(updatedAt, "days") + 1;

    // Make sure user can only update username once a month
    if (typeof lastUsernameUpdatedAt === "object" && daysDifference <= 30) {
      throw Failure.BadRequest("You can only update your username once a month");
    }

    return usernameToBeSaved;
  }

  /**
   * @summary Validate email to make sure it unique & valid
   *
   * @param {string} email
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IValidationResponse>}
   */
  @Trace()
  public async validateEmail(email: string, userId: Types.ObjectId | string = ""): Promise<IValidationResponse> {
    const incomingEmail = email.toLowerCase().trim();

    let error = "";
    let valid = true;

    if (isEmpty(incomingEmail)) {
      error = "Email is required";
      valid = false;
    } else if (!Validator.isEmail(incomingEmail)) {
      error = "Please enter a valid email address";
      valid = false;
    } else {
      const user: IUser | null = await this.users.findOneWithDeleted({ email: incomingEmail }, { email: 1 }).lean();

      // signupbot@efuse.gg is the dedicated email for Cross Browser Testing automated tests
      if (user && user._id.toString() !== userId.toString() && incomingEmail !== EmailEnum.SIGNUP_BOT) {
        error = "Email has already been taken";
        valid = false;
      }
    }

    return { valid, error };
  }

  /**
   * @summary Validate & make sure email can be updated
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} email
   * @param {string} existingEmail
   *
   * @returns {Promise<string | undefined>}
   */
  @Trace()
  public async prepareEmailForUpdate(
    userId: Types.ObjectId | string,
    email: string,
    existingEmail?: string
  ): Promise<string | undefined> {
    // Make sure presence of username
    if (!email) {
      throw Failure.BadRequest("email is required");
    }

    // Trim it before saving
    const emailToBeSaved = email.toLowerCase().trim();

    // Only process when a new email is passed
    if (existingEmail === emailToBeSaved) {
      return;
    }

    // Make sure it is a valid email
    const { valid, error } = await this.validateEmail(email, userId);

    if (!valid) {
      throw Failure.BadRequest(error);
    }

    return emailToBeSaved;
  }

  /**
   * @summary Make sure date of birth is valid & user is older than 12
   *
   * @param {string} dateOfBirth
   *
   * @returns {void}
   */
  @Trace()
  public validateDateOfBirth(dateOfBirth: string): void {
    // Make sure presence of DOB
    if (!dateOfBirth) {
      throw Failure.BadRequest("Date of Birth is required");
    }

    // Calculate age
    const age = moment().diff(new Date(dateOfBirth), "y");

    // Make sure it is a valid date
    if (Number.isNaN(age)) {
      throw Failure.BadRequest("Invalid Date of Birth");
    }

    // Make sure user is older than 12
    if (age < 13) {
      throw Failure.BadRequest("Age must be greater than 12 years");
    }
  }

  /**
   * @summary Make sure name is valid & not more than 100 chars
   *
   * @param {string} name
   * @param {string} existingName
   *
   * @returns {string | undefined}
   */
  @Trace()
  public validateName(name: string, existingName?: string): string | undefined {
    // Make sure presence of name
    if (!name) {
      throw Failure.BadRequest("Name is required");
    }

    // Trim before saving
    const nameToBeStored = name.trim();

    // Make sure name doesn't exceed 100 chars
    if (nameToBeStored.length > 100) {
      throw Failure.BadRequest("Name can not exceed 100 characters");
    }

    if (nameToBeStored === existingName) {
      return;
    }

    if (BAD_WORDS.includes(nameToBeStored.toLowerCase())) {
      throw Failure.BadRequest("Sexual or offensive words are not allowed");
    }

    return nameToBeStored;
  }
}
