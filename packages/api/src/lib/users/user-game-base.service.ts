import { FilterQuery, QueryOptions, Types } from "mongoose";

import { IBulkWriteResponse, IUserGame } from "../../backend/interfaces";
import { UserGame, UserGameModel } from "../../backend/models/user-game";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("user-game-base.service")
export class UserGameBaseService extends BaseService {
  protected readonly $userGames: UserGameModel<IUserGame> = UserGame;

  public async find(condition: FilterQuery<IUserGame>): Promise<IUserGame[]>;
  public async find(condition: FilterQuery<IUserGame>, projection: unknown): Promise<IUserGame[]>;
  public async find(
    condition: FilterQuery<IUserGame>,
    projection: unknown,
    options: QueryOptions
  ): Promise<IUserGame[]>;
  @Trace()
  public async find(
    condition: FilterQuery<IUserGame>,
    projection?: unknown,
    options?: QueryOptions
  ): Promise<IUserGame[]> {
    const userGames = await this.$userGames.find(condition, projection, options);
    return userGames;
  }

  /**
   * @summary Bulk update user games
   *
   * @param {Types.ObjectId | string} userId
   * @param {Array<Types.ObjectId | string>} gameIds
   *
   * @returns {Promise<IBulkWriteResponse>}
   */
  @Trace()
  public async bulkUpdate(
    userId: Types.ObjectId | string,
    gameIds: Array<Types.ObjectId | string>
  ): Promise<IBulkWriteResponse> {
    const userQuery: Record<string, unknown> = { user: userId };

    // Create or update user games
    const bulkOperations: any[] = gameIds.map((gameId) => {
      const query: Record<string, unknown> = { ...userQuery, game: gameId };
      return {
        updateOne: {
          filter: query,
          update: { ...query, game: gameId, updatedAt: Date.now() },
          upsert: true, // create new object if it is not already there
          setDefaultsOnInsert: true, // this would set the schema defaults
          ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
        }
      };
    });

    // Delete games which are not passed
    bulkOperations.push({ deleteMany: { filter: { ...userQuery, game: { $nin: gameIds } } } });

    // Execute bulkWrite
    const response = await this.$userGames.bulkWrite(bulkOperations);

    return {
      upserted: response.upsertedCount || 0,
      updated: response.upsertedCount || 0,
      modified: response.modifiedCount || 0,
      deleted: response.deletedCount || 0,
      matched: response.matchedCount || 0
    };
  }

  /**
   * @summary Get a Count of number of documents
   *
   * @param {FilterQuery<IUserGame>} filter
   *
   * @returns {Promise<number>}
   */
  public async countDocuments(filter?: FilterQuery<IUserGame>): Promise<number> {
    let count = 0;

    count = await (filter ? this.$userGames.countDocuments(filter) : this.$userGames.countDocuments());

    return count;
  }
}
