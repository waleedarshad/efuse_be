export class UserFields {
  public static readonly JWT_USER_FIELDS = [
    "email",
    "gender",
    "status",
    "dateOfBirth",
    "zipCode",
    "address",
    "streamChatToken",
    "lastUsernameUpdatedAt"
  ]
    .map((key) => `+${key}`)
    .join(" ");

  public static readonly OTHER_USER_FIELDS = ["showOnline", "showOnlineMobile", "googleVerified"]
    .map((key) => `+${key}`)
    .join(" ");

  public static readonly SENSITIVE_FIELDS = `${[
    "chatNotification",
    "followsNotification",
    "likeNotification",
    "repliesNotification",
    "commentsNotification",
    "sharesNotification",
    "mentionsNotification",
    "postOrganizationNotification",
    "opportunityOrganizationNotification",
    "applyNotification",
    "organizationInvitationNotification",
    "newsOrganizationNotification",
    "hypedPostCommentNotification",
    "chatNotificationMobile",
    "followsNotificationMobile",
    "likeNotificationMobile",
    "repliesNotificationMobile",
    "commentsNotificationMobile",
    "sharesNotificationMobile",
    "mentionsNotificationMobile",
    "postOrganizationNotificationMobile",
    "opportunityOrganizationNotificationMobile",
    "applyNotificationMobile",
    "organizationInvitationNotificationMobile",
    "newsOrganizationNotificationMobile",
    "hypedPostCommentNotificationMobile",
    "google",
    "google.id"
  ]
    .map((key) => `+${key}`)
    .join(" ")} ${UserFields.JWT_USER_FIELDS} ${UserFields.OTHER_USER_FIELDS}`;

  public static readonly NOTIFICATION_REDACTED_FIELDS = [
    "dailyNotificationSummary",
    "applyNotification",
    "chatNotification",
    "commentsNotification",
    "followsNotification",
    "hypedPostCommentNotification",
    "likeNotification",
    "mentionsNotification",
    "newsOrganizationNotification",
    "opportunityOrganizationNotification",
    "organizationInvitationNotification",
    "postOrganizationNotification",
    "repliesNotification",
    "sharesNotification",
    "showOnline",
    "applyNotificationMobile",
    "chatNotificationMobile",
    "commentsNotificationMobile",
    "followsNotificationMobile",
    "hypedPostCommentNotificationMobile",
    "likeNotificationMobile",
    "mentionsNotificationMobile",
    "newsOrganizationNotificationMobile",
    "opportunityOrganizationNotificationMobile",
    "organizationInvitationNotificationMobile",
    "postOrganizationNotificationMobile",
    "repliesNotificationMobile",
    "sharesNotificationMobile",
    "showOnlineMobile"
  ].join(" ");

  public static readonly DISCORD_REDACTED_FIELDS = [
    "discordAccessToken",
    "discordRefreshToken",
    "discordServer",
    "discordTokenExpiresAt",
    "discordTokenScope",
    "discordTokenType",
    "discordUserId",
    "discordUserUniqueId",
    "discordVerified",
    "discordWebhooks"
  ].join(" ");

  public static readonly GENERAL_REDACTED_FIELDS = [
    "blockedUsers",
    "gamingSkills",
    "businessSkills",
    "resume",
    "steamVerified",
    "snapchatVerified",
    "goldSubscriberSince",
    "xboxVerified",
    "online",
    "migratedToCognito",
    "goldSubscriber",
    "embeddedLink",
    "componentLayout",
    "streamChatToken",
    "password",
    "affiliatedGames",
    "deleteAccountRequest",
    "deleteAccountRequestDate",
    "emailVerificationConfirmedAt",
    "emailVerificationExpires",
    "emailVerificationToken",
    "lastUsernameUpdatedAt",
    "passwordMigrated",
    "passwordMigratedAt",
    "resetPasswordExpires",
    "resetPasswordToken",
    "referral",
    "referralUrl",
    "signupSocial",
    "signupUrl",
    "userAgent",
    "verifyAccountRequest",
    "verifyAccountRequestDate",
    "weeklyNewsLetter"
  ].join(" ");

  public static readonly FACEBOOK_REDACTED_FIELDS = [
    "facebookAccessToken",
    "facebookAccessTokenExpiresAt",
    "facebookID",
    "facebookLink",
    "facebookPages",
    "facebookTokenType",
    "facebookVerified"
  ].join(" ");

  public static readonly GOOGLE_REDACTED_FIELDS = ["google", "googleVerified"].join(" ");

  public static readonly INSTAGRAM_REDACTED_FIELDS = ["instagramLink"].join(" ");

  public static readonly TWITCH_REDACTED_FIELDS = [
    "twitch",
    "twitchFollowersCount",
    "twitchID",
    "twitchLink",
    "twitchProfileImage",
    "twitchUserName",
    "twitchVerified",
    "twitchEmail",
    "twitchAccessToken",
    "twitchAccessTokenExpiresAt",
    "twitchRefreshToken",
    "twitchScope",
    "twitchTokenType"
  ].join(" ");

  public static readonly TWITTER_REDACTED_FIELDS = [
    "twitterfollowersCount",
    "twitterId",
    "twitterLink",
    "twitterVerified",
    "twitterOauthToken",
    "twitterOauthTokenSecret"
  ].join(" ");

  public static readonly LINKEDIN_REDACTED_FIELDS = ["linkedinLink", "linkedinVerified"].join(" ");

  public static readonly BATTLENET_REDACTED_FIELDS = ["battlenetId", "battlenetVerified"].join(" ");

  public static readonly REDACTED_ALGOLIA_FIELDS = `${UserFields.NOTIFICATION_REDACTED_FIELDS} ${UserFields.DISCORD_REDACTED_FIELDS} ${UserFields.GENERAL_REDACTED_FIELDS} ${UserFields.FACEBOOK_REDACTED_FIELDS} ${UserFields.GOOGLE_REDACTED_FIELDS} ${UserFields.INSTAGRAM_REDACTED_FIELDS} ${UserFields.TWITCH_REDACTED_FIELDS} ${UserFields.TWITTER_REDACTED_FIELDS} ${UserFields.LINKEDIN_REDACTED_FIELDS} ${UserFields.BATTLENET_REDACTED_FIELDS}`;

  public static readonly ALGOLIA_FIELDS_TO_BE_SELECTED = `${
    UserFields.JWT_USER_FIELDS
  } ${UserFields.REDACTED_ALGOLIA_FIELDS.split(" ")
    .map((field) => `-${field}`)
    .join(" ")}`;
}
