import { Types } from "mongoose";
import { JWT_MAX_AGE, JWT_KEY } from "@efuse/key-store";
import JWT from "jsonwebtoken";
import crypto from "crypto";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ITokenResponse, IUserAgent } from "../../backend/interfaces";
import { IIpInfo } from "../../backend/interfaces/ip-info";
import { SessionService } from "../session/session.service";
import { EfuseAuthEnum } from "../auth/auth.enum";

const TOKEN_USE = "access";

@Service("token.service")
export class TokenService extends BaseService {
  private $sessionService: SessionService;

  constructor() {
    super();

    this.$sessionService = new SessionService();
  }

  /**
   * @summary Generate Bearer token
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string | undefined} sessionId
   *
   * @returns {string}
   */
  @Trace()
  public generateBearerToken(userId: Types.ObjectId | string, sessionId: Types.ObjectId | string | undefined): string {
    // Build Options
    const options = {
      issuer: EfuseAuthEnum.ISS,
      subject: String(userId),
      jwtid: String(sessionId),
      expiresIn: JWT_MAX_AGE,
      audience: EfuseAuthEnum.AUD
    };

    // Build Payload
    const payload = {
      token_use: TOKEN_USE,
      id: String(userId),
      "https://hasura.io/jwt/claims": {
        "x-hasura-default-role": "user",
        "x-hasura-allowed-roles": ["user"],
        "x-hasura-user-id": String(userId)
      }
    };

    // Generate Token
    const token = JWT.sign(payload, JWT_KEY, options);

    return token;
  }

  /**
   * @summary Generate bearerToken, refreshToken & store session in DB
   *
   * @param {Types.ObjectId | string} userId
   * @param {IUserAgent | undefined} userAgent
   * @param {IIpInfo} location
   *
   * @returns {Promise<ITokenResponse>}
   */
  @Trace()
  public async createSession(
    userId: Types.ObjectId | string,
    userAgent: IUserAgent | undefined,
    location: IIpInfo
  ): Promise<ITokenResponse> {
    // Generate refreshToken
    const { refreshToken, expiry } = this.generateRefreshToken();

    // Create Session
    const session = await this.$sessionService.create({ refreshToken, expiry, user: userId, userAgent, location });

    // Generate Bearer token
    const token = this.generateBearerToken(userId, session._id);

    return { refreshToken, token: `Bearer ${token}` };
  }

  /**
   * @summary Generate refresh token
   *
   * @returns {refreshToken: string, expiry: Date}
   */
  private generateRefreshToken(): { refreshToken: string; expiry: Date } {
    return {
      refreshToken: crypto.randomBytes(64).toString("hex"),
      // 7776e6 | Magic Number for 90 days in milliseconds
      expiry: new Date(Date.now() + 7776e6)
    };
  }
}
