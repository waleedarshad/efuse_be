export enum UsernameEnum {
  SIGNUP_BOT = "signupbot"
}

export enum EmailEnum {
  SIGNUP_BOT = "signupbot@efuse.gg"
}

export enum TalentCardsEnum {
  DEFAULT_SAMPLE_SIZE = 10,
  MAX_SAMPLE_SIZE = 30
}
