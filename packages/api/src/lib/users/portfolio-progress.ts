import { IUser } from "../../backend/interfaces/user";

/**
 * @summary Calculate portfolio progress
 *
 * @param {Object} user - User info i.e id, name etc
 * @returns {number}
 */
export const calculatePortfolioProgress = (user: IUser): number => {
  const DEFAULT_PROFILE_PICTURE = "mindblown_white.png";
  const otherContributingKeys = [
    "educationExperience",
    "businessExperience",
    "businessSkills",
    "gamingSkills",
    "portfolioEvents",
    "portfolioHonors"
  ];
  const contributingFieldsWeight = 1;
  const contributingFieldsCount = 3;

  let percentage = 0;
  if (user.profilePicture?.filename !== DEFAULT_PROFILE_PICTURE)
    percentage += contributingFieldsWeight / contributingFieldsCount;

  if (user.bio) percentage += contributingFieldsWeight / contributingFieldsCount;

  const hasOtherContributingFields = otherContributingKeys.find((prop) => {
    const value = user[prop];
    if (Array.isArray(value)) return value.length > 0;
    return !!value;
  });
  if (hasOtherContributingFields) percentage += contributingFieldsWeight / contributingFieldsCount;

  return Math.round(percentage * 100);
};
