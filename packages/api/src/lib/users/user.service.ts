import { Failure } from "@efuse/contracts";
import { UserStatus } from "@efuse/entities";
import { Types, QueryOptions, FilterQuery, UpdateQuery } from "mongoose";

import { BaseModelService } from "../base";
import { IUser } from "../../backend/interfaces/user";

import { Service, Trace } from "../decorators";
import { User, UserModel } from "../../backend/models/User";

@Service("user.service")
export class UserService extends BaseModelService<IUser> {
  protected readonly users: UserModel<IUser> = User;

  constructor() {
    super(User);
  }

  public async find(filter: FilterQuery<IUser>): Promise<IUser[]>;
  public async find(filter: FilterQuery<IUser>, projection: any): Promise<IUser[]>;
  public async find(
    filter: FilterQuery<IUser>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IUser[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IUser>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IUser[]> {
    const users = await this.users.find(filter, projection, options);
    return users;
  }

  public async findOne(filter: FilterQuery<IUser>): Promise<IUser | null>;
  public async findOne(filter: FilterQuery<IUser>, projection: any): Promise<IUser | null>;
  public async findOne(
    filter: FilterQuery<IUser>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IUser | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IUser>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUser | null> {
    const user = await this.users.findOne(filter, projection, options);
    return user;
  }

  /**
   * @summary Update a particular user
   *
   * @param {Types.ObjectId | string} userId
   * @param {Partial<IUser>} userParams
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async update(userId: Types.ObjectId | string, userParams: UpdateQuery<IUser>): Promise<IUser> {
    const user: IUser | null = await this.users
      .findOneAndUpdate({ _id: userId }, { ...userParams }, { new: true })
      .lean();

    if (!user) {
      throw Failure.UnprocessableEntity("User not found");
    }

    return user;
  }

  /**
   * @summary Create a user Alias
   *
   * @param {Types.ObjectId | string} userId
   *
   * @return {IUser}
   */
  @Trace()
  public createUserAlias(): Partial<IUser> {
    const randInt = Math.ceil(Math.random() * 100000000);

    const name = `Anonymous#${randInt}`;

    return <Partial<IUser>>{ name };
  }

  /**
   * @summary Enable user Alias by masking their first and last names
   *
   * @param {Types.ObjectId | string} userId
   *
   * @return {Promise<IUser>}
   */
  @Trace()
  public async enableUserAlias(userId: Types.ObjectId | string): Promise<IUser> {
    const userAlias = this.createUserAlias();
    const user = await this.update(userId, <IUser>userAlias);
    return user;
  }

  /**
   * @summary Get user's ID by id or username
   *
   * @param {Types.ObjectId | string | undefined} idOrUserName
   *
   * @returns {Promise<Types.ObjectId | undefined>}
   */
  @Trace()
  public async getUserIdByIdOrUsername(
    idOrUserName: Types.ObjectId | string | undefined
  ): Promise<Types.ObjectId | undefined> {
    if (!idOrUserName) {
      return;
    }

    if (this.isValidObjectId(idOrUserName)) {
      return new Types.ObjectId(idOrUserName);
    }

    // Lookup for user by username
    const dbUser = await this.findOne({ username: idOrUserName as string }, { _id: 1 });

    // Set ID when object exists in DB
    return dbUser ? new Types.ObjectId(dbUser._id) : undefined;
  }

  /**
   * @summary Soft delete a user
   *
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async softDelete(userId: Types.ObjectId | string): Promise<IUser> {
    const deletedUser = (await this.users.delete({ _id: userId })) as IUser | null;

    if (!deletedUser) {
      throw Failure.BadRequest("user not found");
    }

    return deletedUser;
  }

  /**
   * @summary Set Locale
   *
   * @param {string} id
   * @param {string} locale
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async setLocale(id: string, locale: string): Promise<IUser> {
    const user = await this.findOne({ _id: id });
    if (!user) {
      throw Failure.BadRequest("user not found");
    }
    user.locale = locale;
    await user.save({ validateBeforeSave: false });
    return user;
  }

  @Trace()
  public async ban(username: string): Promise<IUser> {
    if (!username) {
      throw Failure.BadRequest("Username is required");
    }

    const updatedUser = await this.changeUserStatus(username, UserStatus.BLOCKED);

    return updatedUser;
  }

  @Trace()
  public async unban(username: string): Promise<IUser> {
    if (!username) {
      throw Failure.BadRequest("Username is required");
    }

    const updatedUser = await this.changeUserStatus(username, UserStatus.ACTIVE);

    return updatedUser;
  }

  @Trace()
  private async changeUserStatus(username: string, status: UserStatus): Promise<IUser> {
    const userId = await this.getUserIdByIdOrUsername(username);

    if (!userId) {
      throw Failure.BadRequest("User is required");
    }

    const updatedUser = await this.update(userId, { status });

    return updatedUser;
  }
}
