import { Failure } from "@efuse/contracts";
import { Types, LeanDocument, UpdateQuery } from "mongoose";
import { OwnerKind, OAuthServiceKind, UserStatus, ChangeStreamOperationTypeEnum } from "@efuse/entities";
import moment from "moment";
import bcrypt from "bcryptjs";
import crypto from "crypto";
import { BACKEND_DOMAIN, DOMAIN } from "@efuse/key-store";
import { isEmpty } from "lodash";
import { DocumentType } from "@typegoose/typegoose";

import { AimlabAsyncService, AimlabStatsService } from "../aimlab";
import { AuthService } from "../auth/auth.service";
import { BrazeCampaignName } from "../braze/braze.enum";
import { BrazeService } from "../braze/braze.service";
import { EFuseEngagementLeaderboardService } from "../leaderboards/efuse-engagement-leaderboard.service";
import { Friend } from "../../backend/models/friend";
import { GamePlatformsService } from "../game_platforms/game-platforms.service";
import { GoogleAccountInfoService } from "../google-account-info.service";
import { IFileRequestParam } from "../../backend/interfaces/file";
import { IGame, IMongodbWatcherResponse, ITokenResponse } from "../../backend/interfaces";
import { IPopulatedUserBadge } from "../../backend/interfaces/badge";
import { ISecure } from "../../backend/interfaces/secure";
import { IUser } from "../../backend/interfaces/user";
import { IUserLeaderboard } from "../../backend/interfaces/leaderboard";
import { IUserOverwatchStats } from "../../backend/interfaces/user-overwatch-stats";
import { OAuthCredentialService } from "../oauth-credential.service";
import { RiotProfileService } from "../riot/riot-profile/riot-profile.service";
import { Service, Trace } from "../decorators";
import { StreaksService } from "../streaks/streaks.service";
import { subscribeToStreamChanges } from "../twitch";
import { TwitchAccountInfoAsyncService } from "../twitch/twitch-account-info-async.service";
import { TwitchAccountInfoService } from "../twitch-account-info.service";
import { TwitchAsyncService } from "../twitch/twitch-async.service";
import { TwitchClipAsyncService } from "../twitch/twitch-clip-async.service";
import { TwitchFollowerAsyncService } from "../twitch/twitch-follower-async.service";
import { TwitchVideoAsyncService } from "../twitch/twitch-video-async.service";
import { TwitterAccountInfoService } from "../twitter-account-info.service";
import { User } from "../../backend/models/User";
import { UserBadgeService } from "../badge/user-badge.service";
import { UserFields } from "./user-fields";
import { UserGameService } from "./user-game.service";
import { UserSearchService } from "./user-search.service";
import { UserService } from "./user.service";
import { UserValidationService } from "./user-validation.service";
import { ValorantStatsService } from "../valorant/valorant-stats.service";
import { YoutubeService } from "../youtube/youtube.service";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { Secure } from "../../backend/models/Secure";
import TwitchHandlerLib from "../creatortools/twitchHandler";
import { XboxLiveAuthService } from "../xbox-live-auth/xbox-live-auth.service";
import SchemaHelper from "../../backend/helpers/schema_helper";
import { YoutubeVideoAsyncService } from "../youtube/youtube-video-async.service";
import { ALLOWED_USER_PATCH_FIELDS } from "../../backend/helpers/whitelist_helper";
import { Game } from "../../backend/models/game.model";
import { ObjectId } from "../../backend/types";
import { HomeFeedAsyncService } from "../home-feed/home-feed-async.service";

const SERVICE = "user-helper.service";

@Service(SERVICE)
export class UserHelperService extends UserService {
  private $aimlabAsyncService: AimlabAsyncService;
  private $aimlabStatsService: AimlabStatsService;
  private $authService: AuthService;
  private $brazeService: BrazeService;
  private $efuseEngagementLeaderboardService: EFuseEngagementLeaderboardService;
  private $gamePlatformService: GamePlatformsService;
  private $googleAccountInfoService: GoogleAccountInfoService;
  private $oauthCredentialService: OAuthCredentialService;
  private $riotProfileService: RiotProfileService;
  private $streaksService: StreaksService;
  private $twitchAccountInfoAsyncService: TwitchAccountInfoAsyncService;
  private $twitchAccountInfoService: TwitchAccountInfoService;
  private $twitchAsyncService: TwitchAsyncService;
  private $twitchClipAsyncService: TwitchClipAsyncService;
  private $twitchFollowersAsyncService: TwitchFollowerAsyncService;
  private $twitchVideoAsyncService: TwitchVideoAsyncService;
  private $twitterAccountInfoService: TwitterAccountInfoService;
  private $userBadgeService: UserBadgeService;
  private $userGameService: UserGameService;
  private $userSearchService: UserSearchService;
  private $userValidationService: UserValidationService;
  private $valorantStatsService: ValorantStatsService;
  private $youtubeService: YoutubeService;
  private $youtubeVideoAsyncService: YoutubeVideoAsyncService;
  private $xboxLiveService: XboxLiveAuthService;
  private $homeFeedAsyncService: HomeFeedAsyncService;

  constructor() {
    super();

    this.$aimlabAsyncService = new AimlabAsyncService();
    this.$aimlabStatsService = new AimlabStatsService();
    this.$authService = new AuthService();
    this.$brazeService = new BrazeService();
    this.$efuseEngagementLeaderboardService = new EFuseEngagementLeaderboardService();
    this.$gamePlatformService = new GamePlatformsService();
    this.$googleAccountInfoService = new GoogleAccountInfoService();
    this.$oauthCredentialService = new OAuthCredentialService();
    this.$riotProfileService = new RiotProfileService();
    this.$streaksService = new StreaksService();
    this.$twitchAccountInfoAsyncService = new TwitchAccountInfoAsyncService();
    this.$twitchAccountInfoService = new TwitchAccountInfoService();
    this.$twitchAsyncService = new TwitchAsyncService();
    this.$twitchClipAsyncService = new TwitchClipAsyncService();
    this.$twitchFollowersAsyncService = new TwitchFollowerAsyncService();
    this.$twitchVideoAsyncService = new TwitchVideoAsyncService();
    this.$twitterAccountInfoService = new TwitterAccountInfoService();
    this.$userBadgeService = new UserBadgeService();
    this.$userGameService = new UserGameService();
    this.$userSearchService = new UserSearchService();
    this.$userValidationService = new UserValidationService();
    this.$valorantStatsService = new ValorantStatsService();
    this.$youtubeService = new YoutubeService();
    this.$youtubeVideoAsyncService = new YoutubeVideoAsyncService();
    this.$xboxLiveService = new XboxLiveAuthService();
    this.$homeFeedAsyncService = new HomeFeedAsyncService();
  }

  /**
   * @summary get current user
   *
   * @param {Types.ObjectId} userId
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async getCurrentUser(userId: Types.ObjectId): Promise<LeanDocument<IUser> | null> {
    const user = await this.getFullUserProfileById(userId, true);
    return user;
  }

  /**
   * @summary Get user by Id
   *
   * @param {Types.Object | string} userId
   */
  @Trace()
  public async getUserById(userId, senstiveInfo = false): Promise<LeanDocument<IUser> | null> {
    const user = await this.getFullUserProfileById(userId, senstiveInfo);
    return user;
  }

  /**
   * @summary Get user by Id
   *
   * @param {Types.Object | string} userId
   */
  @Trace()
  public async getUserByUsername(username: string): Promise<LeanDocument<IUser> | null> {
    const user = await this.getFullUserProfileByUsername(username);
    return user;
  }

  /**
   * @summary Get users profile by username
   *
   * @param {String} username - username to search user with
   * @param {Boolean} includeSensitiveInfo
   * @return {Object}
   */
  @Trace()
  public async getFullUserProfileByUsername(
    username: string,
    includeSensitiveInfo = false
  ): Promise<LeanDocument<IUser> | null> {
    const user = await this.getFullUserProfileByQuery({ username }, includeSensitiveInfo);
    return user;
  }

  /**
   * @summary Get users profile by query
   *
   * @param {Object} query - Query to search user with
   * @param {Boolean} includeSensitiveInfo
   * @return {Object}
   */
  @Trace()
  public async getFullUserProfileByQuery(
    query: object,
    includeSensitiveInfo = false
  ): Promise<LeanDocument<IUser> | null> {
    const user = await User.findOne(query).select("_id").lean();
    if (!user) {
      return user;
    }
    const userProfile = await this.getFullUserProfileById(user._id, includeSensitiveInfo);
    return userProfile;
  }

  /**
   * @summary Get users profile by id or username
   *
   * @param {Object} idOrUsername - idOrUsername to search user with
   * @param {Boolean} includeSensitiveInfo
   * @return {Object}
   */
  @Trace()
  public async getProfileByIdOrUsername(
    user: IUser | undefined,
    idOrUsername: string | Types.ObjectId
  ): Promise<LeanDocument<IUser> | null> {
    const isValidId = this.isValidObjectId(idOrUsername);

    const userPortfolio = await (isValidId
      ? this.getFullUserProfileById(idOrUsername, String(user?._id) === String(idOrUsername))
      : this.getFullUserProfileByUsername(idOrUsername as string, user?.username === idOrUsername));

    if (!userPortfolio) {
      throw Failure.UnprocessableEntity("User not found");
    }

    return userPortfolio;
  }

  /**
   * @summary Update current user
   *
   * @param {Types.Object} - current user object
   * @param {IUser} userParams
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  public async updateCurrentUser(
    currentUserObject: IUser,
    userAttributes: IUser,
    files: IFileRequestParam
  ): Promise<IUser | { message: string; deleted: boolean }> {
    const currentUser = currentUserObject;
    let userObject = userAttributes || {};

    const userId = GeneralHelper.getObjectId(currentUser.id);

    // Username validation
    if (!isEmpty(userObject.username)) {
      const usernameToBeUpdated = await this.$userValidationService.prepareUsernameForUpdate(
        userId,
        userObject.username,
        currentUser.username,
        currentUser.lastUsernameUpdatedAt
      );

      if (usernameToBeUpdated) {
        userObject.username = usernameToBeUpdated;
        userObject.lastUsernameUpdatedAt = new Date();
      }
    }

    // Email Validations
    if (!isEmpty(userObject.email)) {
      const emailToBeUpdated = await this.$userValidationService.prepareEmailForUpdate(
        userId,
        userObject.email,
        currentUser.email
      );

      if (emailToBeUpdated) {
        userObject.email = emailToBeUpdated;
      }
    }

    // Save user files
    userObject = this.saveUserFiles(files, userObject);

    const user = (await User.findById(userId).select("+streamChatToken +dateOfBirth")) as IUser;
    const updatedUser = await this.userObjectUpdater(userObject, user);
    return updatedUser;
  }

  /**
   * @summary Get users profile by user id
   *
   * @param {ObjectId} userId - Id to search user with
   * @param {Boolean} includeSensitiveInfo
   * @return {Object}
   */
  @Trace()
  public async getFullUserProfileById(
    userId: Types.ObjectId | string,
    includeSensitiveInfo = false
  ): Promise<LeanDocument<IUser> | null> {
    let select = UserFields.OTHER_USER_FIELDS;

    if (includeSensitiveInfo) {
      select = UserFields.SENSITIVE_FIELDS;
    }

    const user = await this.users
      .findOne({ _id: userId, status: UserStatus.ACTIVE })
      .select(select)
      .populate({ path: "mainOrganization" })
      .populate({ path: "steam" })
      .populate({ path: "twitch" })
      .populate({ path: "xbox" })
      .populate({ path: "leagueOfLegends.stats" })
      .populate({ path: "overwatch.stats" })
      .populate({ path: "rocketLeague.stats" })
      .populate({ path: "pubg.stats" })
      .populate({ path: "stats" })
      .populate({ path: "pathway" })
      .populate({ path: "youtubeChannel" })
      .populate({ path: "stats" })
      .lean();
    // .cache(0, getUserCacheKey(userId, includeSensitiveInfo));

    if (!user) {
      return null;
    }
    // Tacking on user streaks
    user.streaks = await this.$streaksService.lookupDailyStreaks(user._id);
    // Get GamePlatforms
    user.gamePlatforms = [];
    const gamePlatforms = await this.$gamePlatformService.getGamePlatforms(user._id);
    if (gamePlatforms) {
      user.gamePlatforms = gamePlatforms;
    }

    // Leaderboard Lookup
    let leaderboard = {} as IUserLeaderboard;
    leaderboard = await this.$efuseEngagementLeaderboardService.lookupUserLeaderboard(user._id);
    user.leaderboard = leaderboard.leaderboard;
    user.activeLeaderboard = leaderboard.activeLeaderboard;

    // User Badges
    user.badges = [] as IPopulatedUserBadge[];
    user.badges = await this.$userBadgeService.findAllByUserId(user._id);

    user.associatedGames = (await this.lookupUserGames(user._id)) as LeanDocument<IGame>[];

    return user;
  }

  /**
   * @summary Remove third party auth token
   *
   * @param {ObjectId} userId - Id of user
   * @param {String} service
   * @return {Promise}
   */
  @Trace()
  private async removeThirdPartyAuthToken(userId: Types.ObjectId | string, service: string): Promise<boolean> {
    try {
      const secureObject: ISecure = (await Secure.findOne({ user: userId }).select(service)) as ISecure;

      if (!secureObject) {
        throw Failure.BadRequest("Secure object not found!");
      }

      secureObject[service] = undefined;

      await secureObject.save({ validateBeforeSave: false });

      return true;
    } catch (error) {
      this.logger?.error({ message: error.message }, `Error while removing ${service} tokens`);
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary Discord Authentication
   *
   * @param {string} code - auth code
   * @param {Typed.ObjectId} userId
   * @return {Promise<IUser>}
   */
  @Trace()
  public async discordAuth(code: string, userId: Types.ObjectId): Promise<IUser> {
    interface DiscordAuthResponse {
      discordVerified: boolean;
      discordUserId: string;
      discordUserName: string;
      discordUserUniqueId: string;
      discordAccessToken: string;
      discordTokenType: string;
      discordRefreshToken: string;
      discordTokenScope: string;
      discordTokenExpiresAt: string;
    }
    try {
      const userDiscordObject = <DiscordAuthResponse>await this.$authService.authenticateDiscord(code);

      const user = (await this.users.findById(userId)) as IUser;
      const localUserObject: IUser = Object.assign(user, userDiscordObject) as IUser;
      const updatedUser = await this.returnUpdatedUser(localUserObject);
      return updatedUser;
    } catch (error) {
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary Facebook Authentication
   *
   * @param {string} accessToken - Access Token
   * @param {Typed.ObjectId} userId
   * @return {Promise<IUser>}
   */
  @Trace()
  public async facebookAuth(accessToken: string, userId: Types.ObjectId): Promise<IUser> {
    interface FBResponse {
      facebookVerified: boolean;
      facebookID: string;
    }
    try {
      const userFacebookObject: FBResponse = (await this.$authService.authenticateFacebook(accessToken)) as FBResponse;

      const user = (await this.users.findById(userId)) as IUser;
      const localUserObject = Object.assign(user, userFacebookObject);
      const updatedUser = await this.returnUpdatedUser(localUserObject);
      return updatedUser;
    } catch (error) {
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary Twitch Authentication
   *
   * @param {string} accessToken - auth code
   * @param {Typed.ObjectId} userId
   * @return {Promise<IUser>}
   */
  @Trace()
  public async twitchAuth(accessToken: string, userId: Types.ObjectId): Promise<IUser> {
    interface TwitchResponse {
      twitchVerified: boolean;
      twitchID: string;
      twitchUserName: string;
      twitchDisplayName: string;
      twitchProfileImage: string;
      twitchAccessToken: string;
      twitchRefreshToken: string;
      twitchAccessTokenExpiresAt: string;
      twitchScope: string;
      twitchTokenType: string;
    }

    try {
      const userTwitchAuth: TwitchResponse = (await this.$authService.twitchAuth(accessToken)) as TwitchResponse;

      const user = (await this.users.findById(userId)) as IUser;

      const localUserObject: IUser = Object.assign(user, userTwitchAuth) as IUser;
      const updatedUser = await this.returnUpdatedUser(localUserObject);

      this.$twitchAsyncService.subscribeCronsAsync(user._id);
      this.$twitchClipAsyncService.syncTwitchClipsAsync(user._id, OwnerKind.USER);
      this.$twitchFollowersAsyncService.syncTwitchFollowersAsync(user._id, OwnerKind.USER);
      this.$twitchVideoAsyncService.syncTwitchVideosAsync(user._id, OwnerKind.USER);
      this.$twitchAccountInfoAsyncService.syncTwitchInfoAsync(user._id, OwnerKind.USER);
      await TwitchHandlerLib.subscribeToStreamChangeWebhookAsync(user._id, OwnerKind.USER);

      return updatedUser;
    } catch (error) {
      this.logger?.error("Got error while authenticating account twitch ");
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary Block User
   *
   * @param {string} blockedUserId - auth code
   * @param {user} IUser
   * @return {Promise<object>}
   */
  @Trace()
  public async blockUser(blockedUserId: string, user: IUser): Promise<object> {
    const blockedUserObjectId = GeneralHelper.getObjectId(blockedUserId);
    try {
      await this.users.findOneAndUpdate({ _id: user._id }, { $addToSet: { blockedUsers: blockedUserObjectId } });
      await Friend.deleteMany({
        $or: [
          { follower: user._id, followee: blockedUserId },
          { follower: blockedUserId, followee: user._id }
        ]
      });

      await this.$homeFeedAsyncService.removeHomeFeedOnUserBlockAsync(blockedUserId, user._id);
      await this.$homeFeedAsyncService.removeHomeFeedOnUserBlockeeAsync(blockedUserId, user._id);

      return {
        message: "User has been successfully blocked",
        flashType: "success"
      };
    } catch (error) {
      this.logger?.error("Got error while authenticating account twitch ");
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary Unblock User
   *
   * @param {string} blockedUserId - auth code
   * @param {IUser} user
   * @return {Promise<IUser>}
   */
  @Trace()
  public async unblockUser(blockedUserId: string, user: IUser): Promise<object> {
    try {
      const blockedUserObjectId = GeneralHelper.getObjectId(blockedUserId);
      const updatedUser = await this.users.findByIdAndUpdate(
        { _id: user._id },
        { $pull: { blockedUsers: blockedUserObjectId } },
        { lean: true }
      );

      return {
        user: updatedUser,
        message: "User has been successfully unblocked",
        flashType: "success"
      };
    } catch (error) {
      this.logger?.error(error);
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary blockedUsers
   *
   * @param {Typed.ObjectId} userId
   * @return {Promise<Types.ObjectId[] | undefined>}
   */
  @Trace()
  public async blockedUsers(userId: Types.ObjectId): Promise<Types.ObjectId[] | undefined> {
    try {
      const retrievedUser = await this.users
        .findById(userId)
        .select("blockedUsers")
        .populate("blockedUsers", "profilePicture username name")
        .lean();

      return retrievedUser?.blockedUsers;
    } catch {
      throw Failure.BadRequest("Unable to retrieve blockedUsers");
    }
  }

  /**
   * @summary Unlink External Accounts
   *
   * @param {string} service - service
   * @param {Typed.ObjectId} userId
   * @return {Promise<IUser>}
   */
  @Trace()
  public async unLinkExternalAccounts(
    owner: Types.ObjectId | string,
    service: string,
    ownerKind?: string
  ): Promise<IUser | null> {
    if (!owner) {
      throw Failure.BadRequest("owner is required");
    }

    // Get the user object
    const user: IUser | null = await this.users.findOne({ _id: owner }).select("+twitchID +twitchAccessToken");

    // Get the credentials object
    const oauthCredential = await this.$oauthCredentialService.findOne(
      { owner: String(owner), service },
      {},
      { lean: true }
    );

    // Make sure user exists
    if (ownerKind === OwnerKind.USER || (oauthCredential && oauthCredential.ownerKind === OwnerKind.USER)) {
      if (!user) {
        throw Failure.UnprocessableEntity("unable to find user");
      }
    }

    try {
      switch (service) {
        case "discord":
          // Set only when we have a user object
          if (user) {
            user.discordVerified = false;
            user.discordUserId = undefined;
            user.discordUserName = undefined;
            user.discordServer = undefined;
            user.discordAccessToken = undefined;
            user.discordTokenType = undefined;
            user.discordRefreshToken = undefined;
            user.discordTokenScope = undefined;
            user.discordTokenExpiresAt = undefined;
            user.discordWebhooks = undefined;
          }

          break;

        case OAuthServiceKind.TWITCH:
          if (user) {
            try {
              // unsubscribe twitch webhook
              await subscribeToStreamChanges(
                BACKEND_DOMAIN,

                // todo: investigate
                // @ts-ignore
                user._id,
                <string>user.twitchID,
                <string>user.twitchAccessToken,
                "unsubscribe"
              );
            } catch (error) {
              this.logger?.error(error);
            }

            // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
            user.twitchVerified = false;
            user.twitchID = undefined;
            user.twitchUserName = undefined;
            user.twitchDisplayName = undefined;
            user.twitchProfileImage = undefined;
            user.twitchAccessToken = undefined;
            user.twitchRefreshToken = undefined;
            user.twitchAccessTokenExpiresAt = undefined;
            user.twitchScope = undefined;
            user.twitchTokenType = undefined;
            user.twitch = undefined;

            // Remove user's cron subscriptions
            this.$twitchAsyncService.unsubscribeCronsAsync(user._id);
          }

          if (oauthCredential) {
            // delete saved oauth credentials and account info
            await this.$twitchAccountInfoService.deleteByOwner(String(owner));
            await this.$oauthCredentialService.deleteByOwnerAndService(String(owner), OAuthServiceKind.TWITCH);
          }

          break;
        case "steam":
          if (user) {
            user.steamId = undefined;
            user.steamUsername = undefined;
            user.steamVerified = false;
          }

          break;
        case "linkedin":
          if (user) {
            user.linkedin = undefined;
            user.linkedinVerified = false;
            await this.removeThirdPartyAuthToken(user._id, "linkedin");
          }

          break;
        case "bnet":
          if (user) {
            user.battlenetVerified = false;
            user.battlenetId = undefined;
            await this.removeThirdPartyAuthToken(user._id, "bnet");
          }

          break;
        case OAuthServiceKind.TWITTER:
          if (user) {
            user.twitterVerified = false;
            user.twitterId = undefined;
            user.twitterUsername = undefined;
            user.twitterfollowersCount = undefined;
            user.twitterOauthToken = undefined;
            user.twitterOauthTokenSecret = undefined;
          }

          if (oauthCredential) {
            // delete saved oauth credentials and account info
            await this.$twitterAccountInfoService.deleteByOwner(String(owner));
            await this.$oauthCredentialService.deleteByOwnerAndService(String(owner), OAuthServiceKind.TWITTER);
          }

          break;
        case "facebook":
          if (user) {
            user.facebookVerified = false;
            user.facebookID = undefined;
            user.facebookAccessToken = undefined;
            user.facebookTokenType = undefined;
            user.facebookAccessTokenExpiresAt = undefined;
            user.facebookPages = undefined;
          }

          break;
        case "xboxlive":
          if (user) {
            user.xboxVerified = false;
            user.xboxUserId = undefined;
            await this.$xboxLiveService.deleteOne({ user: user._id });
          }

          break;
        case "snapchat":
          if (user) {
            user.snapchatVerified = false;
            user.snapchat = undefined;
            await this.removeThirdPartyAuthToken(user._id, "snapchat");
          }

          break;
        case OAuthServiceKind.GOOGLE:
          if (user) {
            user.googleVerified = false;
            user.google = undefined;
            await this.removeThirdPartyAuthToken(user._id, "google");
            await this.$youtubeService.removeAssociatedChannel(user.youtubeChannel, user?.id);
            user.youtubeChannel = undefined;
          }

          if (oauthCredential) {
            // delete saved oauth credentials and account info
            await this.$googleAccountInfoService.deleteByOwner(String(owner));
            await this.$oauthCredentialService.deleteByOwnerAndService(String(owner), OAuthServiceKind.GOOGLE);
          }

          // Unsubscribing user from youtube crons
          this.$youtubeVideoAsyncService.unsubscribeCronsAsync(
            owner,
            oauthCredential ? oauthCredential?.ownerKind : OwnerKind.USER
          );

          break;
        case OAuthServiceKind.RIOT:
          if (oauthCredential) {
            await Promise.all([
              this.$oauthCredentialService.deleteByOwnerAndService(String(owner), OAuthServiceKind.RIOT),
              this.$riotProfileService.deleteByOwner(String(owner)),
              this.$valorantStatsService.deleteByOwner(String(owner))
            ]);
          } else {
            Failure.BadRequest(`Owner is not linked to ${service}.`);
          }

          break;
        case OAuthServiceKind.STATESPACE:
          if (oauthCredential) {
            await Promise.all([
              this.$oauthCredentialService.deleteByOwnerAndService(String(owner), OAuthServiceKind.STATESPACE),
              this.$aimlabStatsService.deleteByOwner(String(owner))
            ]);

            this.$aimlabAsyncService.unsubscribeCronsAsync(String(owner));
          } else {
            Failure.BadRequest(`Owner is not linked to ${service}.`);
          }

          break;
        default:
          break;
      }

      if (user) {
        await user.save({ validateBeforeSave: false });
      }

      return user;
    } catch (error) {
      throw Failure.BadRequest(error.message);
    }
  }

  /* ****************** Private methods ****************** */

  @Trace()
  private async returnUpdatedUser(user: IUser): Promise<IUser> {
    if (user.__v) delete user.__v;

    const updatedUser = await user.save({ validateBeforeSave: false });
    return updatedUser;
  }

  @Trace()
  private async userObjectUpdater(
    userObject: IUser,
    user: IUser
  ): Promise<IUser | { message: string; deleted: boolean }> {
    try {
      let localUserObject: IUser | null = userObject;
      localUserObject = Object.assign(user, localUserObject);

      if (user.__v) delete user.__v;

      const updatedUser = await localUserObject.save({ validateBeforeSave: false });

      return updatedUser;
    } catch (error) {
      this.logger?.error(error);
      throw Failure.BadRequest(error);
    }
  }

  @Trace()
  private saveUserFiles(files: IFileRequestParam, userObject: IUser): IUser {
    let savedUserObject: any = userObject;
    const { profilePicture, headerImage, resume } = files;
    savedUserObject = GeneralHelper.saveFile(profilePicture, "profilePicture", userObject);
    savedUserObject = GeneralHelper.saveFile(headerImage, "headerImage", userObject);
    savedUserObject = GeneralHelper.saveFile(resume, "resume", userObject);
    return savedUserObject;
  }

  /**
   * @summary Set password of user
   *
   * @param {ObjectId} userId - Id of user
   * @param {String} password - Password to set
   * @return {Promise}
   */
  @Trace()
  public async setPassword(userId: Types.ObjectId, password: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      bcrypt.hash(
        password,
        10,

        async (err, hash) => {
          if (err) {
            this.logger?.error({ err: err.message }, `user: ${userId}] Error setting password | bcrypt`);
            return reject(err);
          }
          try {
            // Set the password in an existing secure object
            const secureObject: ISecure = (await Secure.findOne({ user: userId })) as ISecure;
            if (secureObject) {
              secureObject.history.push({
                password: secureObject.password,
                createdAt: secureObject.updatedAt || new Date(),
                updatedAt: new Date()
              });
              secureObject.password = hash;
              await secureObject.save();
              return resolve(true);
            }

            // If secure object doesn't exist create a new one

            await new Secure({ password: hash, user: userId }).save();
            return resolve(true);
          } catch (error) {
            this.logger?.error({ err: error.message }, `user: ${userId}] Error setting password | bcrypt`);
            return reject(error);
          }
        }
      );
    });
  }

  /**
   *  @summary Get games associated to User
   *
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IGame[]>}
   */
  @Trace()
  private async lookupUserGames(userId: Types.ObjectId | string): Promise<DocumentType<Game>[]> {
    try {
      const games = await this.$userGameService.getAssociatedGames(userId);

      return games;
    } catch (error) {
      this.logger?.error(error, "Error in lookupUserGames");
      return [];
    }
  }

  /**
   * @summary Generate Reset Password Link & Send Email to user using Braze
   *
   * @param {string} email
   */
  @Trace()
  public async generateLinkAndSendResetPasswordEmail(email: string): Promise<void> {
    const token: string = crypto.randomBytes(32).toString("hex");
    const tokenExpiry: Date = moment().add(1, "h").toDate(); // expires after 1 hour

    const user: IUser | null = await this.users.findOneAndUpdate(
      { email },
      { resetPasswordToken: token, resetPasswordExpires: tokenExpiry },
      { new: true, projection: { email: 1, resetPasswordToken: 1 } }
    );

    if (!user) {
      throw Failure.UnprocessableEntity("User not found with this email");
    }

    this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.RESET_PASSWORD, {
      trigger_properties: { password_reset_url: `${DOMAIN}?token=${user.resetPasswordToken}` },
      recipients: [{ external_user_id: String(user._id) }]
    });
  }

  /**
   * @summary Unlink Game Account
   *
   * @param {string} game - game
   * @param {IUser} user
   * @return {Promise<{success: boolean, user: IUser>}
   */
  @Trace()
  public async unlinkGameAccount(user: IUser, game: string): Promise<{ success: boolean; user: IUser }> {
    try {
      switch (game) {
        case "lol":
          user.leagueOfLegends = {};
          break;
        case "overwatch":
          user.overwatch = {} as IUserOverwatchStats;
          break;
        default:
          break;
      }

      await user.save({ validateBeforeSave: false });
      return { success: true, user };
    } catch (error) {
      throw Failure.BadRequest(error);
    }
  }

  /**
   * @summary Remove Pic
   *
   * @param {string} type - auth code
   * @param {IUser} user
   * @return {Promise<{success: boolean, user: IUser}>}
   */
  @Trace()
  public async removePic(user: IUser, type: string): Promise<{ success: boolean; user: IUser }> {
    let updateQuery: UpdateQuery<IUser> | undefined;

    if (type === "profile") {
      const { filename, contentType, url } = SchemaHelper.fileSchema;

      updateQuery = {
        profilePicture: { filename: filename.default, contentType: contentType.default, url: url.default }
      };
    }

    if (type === "header") {
      updateQuery = { headerImage: {} };
    }

    if (!updateQuery) {
      throw Failure.BadRequest("Invalid type");
    }

    // Update user
    const updatedUser = await this.update(user._id, updateQuery);

    return { success: true, user: updatedUser };
  }

  /**
   * @summary get user id by alias
   *
   * @param {string} slug
   *
   * @return {Promise<{id?: Types.ObjectId | string}>}
   */
  @Trace()
  public async alias(slug: string): Promise<{ id?: Types.ObjectId | string }> {
    try {
      const result = await User.findOne({
        slug
      }).select("_id");

      if (result === null) {
        this.logger?.warn(`Invalid user alias/slug: ${slug}`);
        return {};
      }
      return { id: result._id };
    } catch (error) {
      this.logger?.error(error);
      throw Failure.BadRequest(error);
    }
  }

  @Trace()
  public validateUserUpdateFields(user: IUser): boolean {
    let result = true;

    Object.keys(user).forEach((element) => {
      if (!ALLOWED_USER_PATCH_FIELDS.includes(element)) {
        result = false;
      }
    });

    return result;
  }

  /**
   * @summary User Watcher to read change streams
   *
   * @param {IMongodbWatcherResponse<LeanDocument<IUser>>} data
   *
   * @returns {void}
   */
  @Trace({ service: SERVICE, name: "Watcher" })
  public static Watcher(data: IMongodbWatcherResponse<LeanDocument<IUser>>): void {
    const userHelperService = new UserHelperService();

    try {
      if (data.operationType === ChangeStreamOperationTypeEnum.DELETE) {
        // Remove from algolia
        userHelperService.$userSearchService.deleteFromAlgoliaAsync(data?.documentKey?._id);
      } else {
        // Sync Data with algolia
        userHelperService.$userSearchService.syncWithAlgoliaAsync(data.fullDocument);
      }
    } catch (error) {
      userHelperService.logger?.error(error, "Error reading data in user watcher");
    }
  }

  /**
   * @summary Get user's twitch accessToken & refreshToken
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<ITokenResponse | null>}
   */
  @Trace()
  public async getTwitchAccountCredentials(userId: ObjectId): Promise<ITokenResponse | null> {
    // First check the new oauthCredentials collection
    const credentials = await this.$oauthCredentialService.findOne(
      {
        owner: String(userId),
        ownerKind: OwnerKind.USER,
        service: OAuthServiceKind.TWITCH
      },
      {},
      { lean: true }
    );

    // If we find credentials return
    if (credentials) {
      return { token: credentials.accessToken as string, refreshToken: credentials.refreshToken as string };
    }

    // May be it's an old user where twitch info is stored in User collection
    const user = await this.findOne({ _id: userId }, { twitchAccessToken: 1, twitchRefreshToken: 1 }, { lean: true });

    if (user && !!(user.twitchAccessToken && user.twitchRefreshToken)) {
      return { token: user.twitchAccessToken as string, refreshToken: user.twitchRefreshToken as string };
    }

    return null;
  }

  /**
   * @summary Find out whether user has a twitch account linked or not
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async hasTwitchAccount(userId: ObjectId): Promise<boolean> {
    const tokenObject = await this.getTwitchAccountCredentials(userId);

    return !isEmpty(tokenObject);
  }
}
