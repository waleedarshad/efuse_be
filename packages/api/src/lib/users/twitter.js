const {
  TWITTER_ACCESS_TOKEN_SECRET,
  TWITTER_ACCESS_TOKEN,
  TWITTER_API_KEY,
  TWITTER_API_SECRET
} = require("@efuse/key-store");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const Twitter = require("twitter");

const { User } = require("../../backend/models/User");
const TwitterCoreAsync = require("../../async/core/twitter");

const getTwitterAccountDetails = async (twitterUsername, oAuthToken, oAuthTokenSecret) => {
  try {
    // get twitter stats
    const client = new Twitter({
      consumer_key: TWITTER_API_KEY,
      consumer_secret: TWITTER_API_SECRET,
      access_token_key: oAuthToken,
      access_token_secret: oAuthTokenSecret
    });
    const userInfo = await client.get(`https://api.twitter.com/1.1/users/show.json?screen_name=${twitterUsername}`, {});

    return userInfo;
  } catch (error) {
    logger.error(error, "Error fetching twitter account info");
    return;
  }
};

/**
 * @summary Get twitter followers
 *
 * @param {String} twitterUsername - User's twitter name
 * @return {Promise}
 */
const getTwitterFollowers = async (twitterUsername) => {
  try {
    const query = twitterUsername ? { twitterUsername } : { twitterUsername: { $ne: null } };

    // todo: investigate
    // @ts-ignore
    const users = await User.find(query).select(
      "_id email twitterUsername twitterOauthToken twitterOauthTokenSecret twitterfollowersCount"
    );
    if (!users || users.length === 0) {
      return;
    }
    shuffle(users);
    return await Promise.all(
      users.map(async (user) => {
        logger.info(
          {
            email: user.email,
            twitterUsername: user.twitterUsername
          },
          "User found"
        );
        try {
          // NOTE: The Twitter client should be initialized after fetching the user and using the user's twitter creds if they exist (fall back to our app creds)
          const client = new Twitter({
            consumer_key: TWITTER_API_KEY,
            consumer_secret: TWITTER_API_SECRET,
            access_token_key: user.twitterOauthToken ? user.twitterOauthToken : TWITTER_ACCESS_TOKEN,
            access_token_secret: user.twitterOauthTokenSecret
              ? user.twitterOauthTokenSecret
              : TWITTER_ACCESS_TOKEN_SECRET
          });

          const userInfo = await client.get(
            `https://api.twitter.com/1.1/users/show.json?screen_name=${user.twitterUsername}`,
            {}
          );
          user.twitterfollowersCount = userInfo.followers_count;
          await user.save({ validateBeforeSave: false });

          logger.info(`Updated followers count for ${user.email} to ${userInfo.followers_count}`);
        } catch (error) {
          logger.error(error, "Error updating twitter followers count");
        }
        return;
      })
    );
  } catch (error) {
    logger.error(error, "Error fetching twitter followers count");
    return;
  }
};

/**
 * @summary Shuffle array
 *
 * @param {Array} array - Array to shuffle
 */
const shuffle = (array) => {
  array.sort(() => Math.random() - 0.5);
};

/**
 * @summary Cron to get twitter followers
 */
const getTwitterFollowersCountCron = async () => {
  // todo: investigate
  // @ts-ignore
  return await getTwitterFollowers(null);
};

/**
 * @summary Async method to add job to queue
 *
 * @param {String} twitterUsername - User's twitter name
 */
const getTwitterFollowersAsync = async (twitterUsername) => {
  return await TwitterCoreAsync.getTwitterFollowersQueue.add({
    twitterUsername
  });
};

module.exports.getTwitterAccountDetails = getTwitterAccountDetails;
module.exports.getTwitterFollowers = getTwitterFollowers;
module.exports.getTwitterFollowersAsync = getTwitterFollowersAsync;
module.exports.getTwitterFollowersCountCron = getTwitterFollowersCountCron;
module.exports.shuffle = shuffle;
