export enum BrazeEnum {
  CONSTANT_FEATURE = "BRAZE"
}

export enum BrazeCampaignName {
  RESET_PASSWORD = "resetPasswordCampaignId",
  OPPORTUNITY_APPLICATION_RECEIVED = "opportunityApplicationReceivedCampaignId",
  OPPORTUNITY_APPLICATION_ACCEPTED = "opportunityApplicationAcceptedCampaignId",
  OPPORTUNITY_APPLICANT_APPLIED = "applicantAppliedCampaignId",
  USER_FOLLOW = "userFollowNotificationCampaignId",
  POST_HYPE = "hypedNotificationCampaignId",
  POST_MENTION = "postMentionCampaignId",
  COMMENT = "commentNotificationCampaignId",
  COMMENT_REPLY = "commentReplyNotificationCampaignId",
  COMMENT_HYPE = "commentHypeNotificationCampaignId",
  COMMENT_MENTION = "commentMentionCampaignId",
  COMMENT_ON_POST_YOU_HYPED = "commentOnPostYouHypedNotificationCampaignId",
  STAFF_ADDED_TO_ERENA_EVENT = "staffAddedtoeRenaCampaignId",
  DIRECT_MESSAGE = "directMessageCampaignId",
  LEAGUE_TEAM_CREATED = "leagueTeamCreatedCampaignId",
  LEAGUE_CREATED = "leagueCreatedCampaignId",
  ORGANIZATION_CREATED = "organizationCreatedCampaignId"
}
