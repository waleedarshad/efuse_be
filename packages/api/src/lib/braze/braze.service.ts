import { Braze, ICampaignOptions, ICampaignResponse } from "@efuse/braze-rest-client";
import { Failure } from "@efuse/contracts";
import { Queue } from "bull";

import { IBrazeConstants } from "../../backend/interfaces";
import { BaseService } from "../base/base.service";
import { ConstantsService } from "../constants";
import { Service, Trace } from "../decorators";
import { BrazeCampaignName, BrazeEnum } from "./braze.enum";
import Config from "../../async/config";
import { Notification } from "../../backend/models/Notification";

const SERVICE = "braze.server";

@Service("braze.server")
export class BrazeService extends BaseService {
  private $braze: Braze;
  private $constantService: ConstantsService;

  private $sendCampaignMessageQueue: Queue;

  constructor() {
    super();

    this.$braze = new Braze();
    this.$constantService = new ConstantsService();

    this.$sendCampaignMessageQueue = Config.initQueue("sendCampaignMessage");
  }

  /**
   * @summary Send a campaign message using braze
   *
   * @param {string} campaignName
   * @param {ICampaignOptions} options
   *
   * @returns {Promise<ICampaignResponse>}
   */
  @Trace()
  private async sendCampaignMessage(
    campaignName: BrazeCampaignName,
    options: Omit<ICampaignOptions, "campaign_id">
  ): Promise<ICampaignResponse> {
    // Make sure campaignName exists
    if (!campaignName) {
      throw Failure.BadRequest("campaignName is required");
    }

    // Fetch the campaign ID from campaignName
    const variables = await this.getConstants();

    // Make sure campaignID exists
    if (!variables[campaignName]) {
      throw Failure.BadRequest("Campaign ID not found");
    }

    // Check if campaign is sending to one individual with user id
    if (options.recipients && options.recipients.length === 1 && options.recipients[0].external_user_id) {
      const receiverUserId = options.recipients[0].external_user_id;

      // Fetching current unread count and passing to braze (used for icon badge)
      const unreadNotificationCount = await Notification.countDocuments({
        receiver: receiverUserId,
        read: false,
        readCount: false
      });

      if (unreadNotificationCount && options.trigger_properties) {
        // Adding 1 to account for this new notification
        options.trigger_properties.badgeCount = unreadNotificationCount + 1;
      }
    }

    const response = await this.$braze.sendCampaignMessage({
      ...options,
      campaign_id: variables[campaignName] as string
    });

    return response.data;
  }

  /**
   * @summary Send a campaign message using braze via a bull queue
   *
   * @param {string} campaignName
   * @param {ICampaignOptions} options
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public sendCampaignMessageAsync(
    campaignName: BrazeCampaignName,
    options: Omit<ICampaignOptions, "campaign_id">
  ): void {
    this.$sendCampaignMessageQueue
      .add({ campaignName, options })
      .catch((error) => this.logger?.error(error, "Error adding job to async queue sendCampaignMessageAsync"));
  }

  /**
   * @summary Load constants for Braze
   *
   * @returns {Promise<IBrazeConstants>}
   */
  @Trace()
  public async getConstants(): Promise<IBrazeConstants> {
    const variables: IBrazeConstants = <IBrazeConstants>(
      await this.$constantService.getVariables(BrazeEnum.CONSTANT_FEATURE)
    );

    if (!variables) {
      throw Failure.BadRequest("Error while loading constants");
    }

    return variables;
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const brazeService = new BrazeService();

    // sendCampaignMessageQueue
    brazeService.$sendCampaignMessageQueue
      .process((job: { data: { campaignName: BrazeCampaignName; options: Omit<ICampaignOptions, "campaign_id"> } }) => {
        const { campaignName, options } = job.data;

        return brazeService.sendCampaignMessage(campaignName, options);
      })
      .catch((error) => brazeService.logger?.error(error, "Error while processing sendCampaignMessageQueue"));
  }
}
