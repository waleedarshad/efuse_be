import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";
import { ILOLProfile, IRiotProfile, IUserInfo, IValorantProfile } from "@efuse/passport-riot";
import { Optional } from "@efuse/types";
import { Types } from "mongoose";

import { OAuthCredentialService } from "../oauth-credential.service";
import { Service, Trace } from "../decorators";
import { ExternalAccountAuthService } from "./external-account-auth.service";
import { RiotProfileService } from "../riot/riot-profile/riot-profile.service";
import { ValorantApiQueueService } from "../valorant/valorant-api-queue.service";
import { ValorantAsyncService } from "../valorant/valorant-async.service";
import { IOAuthCredential } from "../../backend/interfaces";

@Service("riot-auth.service")
export class RiotAuthService extends ExternalAccountAuthService {
  private $oauthCredentialService: OAuthCredentialService;
  private $riotProfileService: RiotProfileService;
  private $valorantApiQueueService: ValorantApiQueueService;
  private $valorantAsyncService: ValorantAsyncService;

  constructor() {
    super("riot");

    this.$oauthCredentialService = new OAuthCredentialService();
    this.$riotProfileService = new RiotProfileService();
    this.$valorantApiQueueService = new ValorantApiQueueService();
    this.$valorantAsyncService = new ValorantAsyncService();
  }

  /**
   * @summary Store user's riot profile in DB
   *
   * @param {Types.ObjectId | string} owner
   * @param {OwnerKind} ownerKind
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {IRiotProfile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeProfile(
    owner: Types.ObjectId | string,
    ownerKind: OwnerKind,
    accessToken: string,
    refreshToken: string,
    profile: IRiotProfile
  ): Promise<void> {
    if (!owner) {
      throw Failure.BadRequest("Missing owner param");
    }

    if (!ownerKind) {
      throw Failure.BadRequest("Missing ownerKind");
    }

    await this.saveOAuthCredential(accessToken, refreshToken, owner.toString(), ownerKind);
    const savedProfile = await this.storeRiotProfile(owner, ownerKind, profile);

    if (savedProfile.valorantProfile) {
      await this.$valorantAsyncService.retrieveAndSaveValorantUserStatsForPlayer(
        <string>owner,
        <string>savedProfile.valorantProfile.shard,
        <string>savedProfile.valorantProfile.gameName,
        <string>savedProfile.valorantProfile.tagLine,
        <string>savedProfile.valorantProfile.puuid
      );
    }
  }

  /**
   * @summary Store OAuth Credentials in DB
   *
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {string} owner
   * @param {OwnerKind} ownerKind
   */
  @Trace()
  private async saveOAuthCredential(
    accessToken: string,
    refreshToken: string,
    owner: string,
    ownerKind: OwnerKind
  ): Promise<void> {
    // save the oauth credentials for future use
    const oAuthCredential: Partial<IOAuthCredential> = {
      accessToken,
      refreshToken,
      // todo: fix
      // @ts-ignore
      owner,
      ownerKind,
      service: <string>OAuthServiceKind.RIOT
    };

    await this.$oauthCredentialService.create(oAuthCredential as IOAuthCredential);
  }

  /**
   * @summary Store Riot profile in DB
   *
   * @param {Types.ObjectId | string} owner
   * @param {OwnerKind} ownerKind
   * @param {IRiotProfile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async storeRiotProfile(
    owner: Types.ObjectId | string,
    ownerKind: OwnerKind,
    riotProfile: IRiotProfile
  ): Promise<IRiotProfile> {
    const {
      userInfo,
      lolProfile,
      valorantProfile
    }: { userInfo: IUserInfo; lolProfile: ILOLProfile; valorantProfile: IValorantProfile } = riotProfile;

    const valShard = await this.$valorantApiQueueService.fetchValorantActiveShard(<string>valorantProfile.puuid);

    const savedProfile = await this.$riotProfileService.update(
      { owner, ownerKind },
      {
        owner,
        ownerKind,
        userInfo: {
          success: userInfo.success,
          cpid: userInfo.cpid,
          sub: userInfo.sub,
          jti: userInfo.jti
        },
        leagueOfLegendsProfile: {
          success: lolProfile.success,
          id: lolProfile.id,
          accountId: lolProfile.accountId,
          puuid: lolProfile.puuid,
          name: lolProfile.name,
          profileIconId: lolProfile.profileIconId,
          revisionDate: lolProfile.revisionDate,
          summonerLevel: lolProfile.summonerLevel,
          shard: <Optional<string>>userInfo.cpid
        },
        valorantProfile: {
          success: valorantProfile.success,
          puuid: valorantProfile.puuid,
          gameName: valorantProfile.gameName,
          tagLine: valorantProfile.tagLine,
          shard: valShard.activeShard
        },
        updatedAt: new Date()
      },
      { new: true, upsert: true, setDefaultsOnInsert: true }
    );

    return <never>savedProfile;
  }
}
