import { Failure } from "@efuse/contracts";
import { Profile } from "passport";

import { ISecure } from "../../backend/interfaces/secure";
import { Service, Trace } from "../decorators";
import { SecureService } from "../secure/secure.service";
import { ExternalAccountAuthService } from "./external-account-auth.service";

@Service("snapchat-auth.service")
export class SnapchatAuthService extends ExternalAccountAuthService {
  private $secureService: SecureService;

  constructor() {
    super("snapchat");

    this.$secureService = new SecureService();
  }

  /**
   * @summary Store user's snapchat profile in DB
   *
   * @param {string} userId
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Profile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeProfile(
    userId: string,
    accessToken: string,
    refreshToken: string,
    profile: Profile
  ): Promise<void> {
    if (!userId) {
      throw Failure.BadRequest("userId not found");
    }

    // Store credentials in secure object
    const secureObjectToBeUpdated: Partial<ISecure> = <Partial<ISecure>>{ snapchat: { accessToken, refreshToken } };
    await this.$secureService.update({ user: userId }, secureObjectToBeUpdated);

    await this.$userService.update(userId, {
      snapchatVerified: true,

      snapchat: { id: profile.id, displayName: profile.displayName, bitmoji: (<any>profile).bitmoji }
    });
  }
}
