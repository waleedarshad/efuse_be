import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";
import { v4 as uuidv4 } from "uuid";
import { IExternalAuthRequestInfo } from "../../backend/interfaces/external-auth-request-info";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { RedisCacheService } from "../redis-cache.service";
import { UserService } from "../users/user.service";

const CACHE_KEY = "external_account_auth";
const CONSENT_KEY_EXPIRY = 3 * 60; // 3 minutes
const CONSENT_KEY = `${CACHE_KEY}:consent`;
const STATE_KEY = `${CACHE_KEY}:state`;
const STATE_KEY_EXPIRY = 10 * 60; // 10 minutes

@Service("external-account-auth.service")
export class ExternalAccountAuthService extends BaseService {
  private $provider: string;
  private $consentKey: string;
  private $stateKey: string;

  protected $redisCacheService: RedisCacheService;
  protected $userService: UserService;

  constructor(provider: string) {
    super();

    this.$provider = provider;
    this.$consentKey = `${CONSENT_KEY}:${this.$provider}`;
    this.$stateKey = `${STATE_KEY}:${this.$provider}`;

    this.$redisCacheService = new RedisCacheService();
    this.$userService = new UserService();
  }

  /**
   * @summary Store ownerId in redis with a unique token as key
   *
   * @param {Types.ObjectId | string} ownerId
   *
   * @returns {Promise<string>}
   */
  @Trace()
  public async generateConsentToken(ownerId: string, ownerKind: OwnerKind, creatorId?: string): Promise<string> {
    if (!ownerId) {
      throw Failure.BadRequest("ownerId not found");
    }

    const token = this.generateUUID();
    const key = `${this.$consentKey}:${token}`;
    const value = <IExternalAuthRequestInfo>{ ownerKind, ownerId, requestingUserId: <string>creatorId || ownerId };

    await this.$redisCacheService.writeToCache(key, JSON.stringify(value), CONSENT_KEY_EXPIRY);

    return token;
  }

  /**
   * @summary Verify consent token & generate state
   *
   * @param {string} consentToken
   *
   * @return {Promise<string>}
   */
  @Trace()
  public async generateState(consentToken: string): Promise<string> {
    if (!consentToken) {
      throw Failure.BadRequest("Token is required");
    }

    const consentKey = `${this.$consentKey}:${consentToken}`;

    const owner = await this.$redisCacheService.readFromCache(consentKey);

    if (!owner) {
      throw Failure.BadRequest("An error occurred while linking account", { consentKey });
    }

    const state = this.generateUUID();
    const key = `${this.$stateKey}:${state}`;
    const value = String(owner);

    await this.$redisCacheService.writeToCache(key, value, STATE_KEY_EXPIRY);

    return state;
  }

  /**
   * @summary Verify state & get userID
   *
   * @param {string} state
   *
   * @return {Promise<string>}
   */
  @Trace()
  public async verifyState(state: string): Promise<IExternalAuthRequestInfo> {
    const key = `${this.$stateKey}:${state}`;

    const authRequestInfo = await this.$redisCacheService.readFromCache(key);

    if (authRequestInfo) {
      return <IExternalAuthRequestInfo>JSON.parse(authRequestInfo);
    }

    throw Failure.InternalServerError("Unable to retrieve auth state", { key });
  }

  private generateUUID(): string {
    return `${uuidv4()}-${Date.now()}`;
  }
}
