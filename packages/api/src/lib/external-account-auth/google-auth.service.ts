import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";
import { Profile } from "passport";
import { Types } from "mongoose";

import { ExternalAccountAuthService } from "./external-account-auth.service";
import { GoogleAccountInfoService } from "../google-account-info.service";
import { IGoogleAccountInfo } from "../../backend/interfaces";
import { io } from "../../backend/config/get-io-object";
import { IOAuthCredential } from "../../backend/interfaces";
import { ISecure } from "../../backend/interfaces/secure";
import { OAuthCredentialService } from "../../lib/oauth-credential.service";
import { SecureService } from "../secure/secure.service";
import { Service, Trace } from "../decorators";
import { YoutubeService } from "../youtube/youtube.service";
import { YoutubeVideoAsyncService } from "../youtube/youtube-video-async.service";
import { YoutubeChannel } from "../../backend/models/youtube-channel/youtube-channel.model";

@Service("google-auth.service")
export class GoogleAuthService extends ExternalAccountAuthService {
  private $secureService: SecureService;
  private $youtubeService: YoutubeService;
  private $youtubeVideoAsyncService: YoutubeVideoAsyncService;

  private oauthCredentialService: OAuthCredentialService;
  private googleAccountInfoService: GoogleAccountInfoService;

  constructor() {
    super("google");

    this.$secureService = new SecureService();
    this.$youtubeService = new YoutubeService();
    this.oauthCredentialService = new OAuthCredentialService();
    this.googleAccountInfoService = new GoogleAccountInfoService();
    this.$youtubeVideoAsyncService = new YoutubeVideoAsyncService();
  }

  /**
   * @summary Store user's google profile in DB
   *
   * @param {string} userId
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Profile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeAccountInformation(
    owner: string,
    ownerKind: OwnerKind,
    requestingUserId: string,
    accessToken: string,
    refreshToken: string,
    profile: Profile
  ): Promise<void> {
    if (!owner) {
      throw Failure.BadRequest("owner not found");
    }

    if (!ownerKind) {
      throw Failure.BadRequest("owner kind not found");
    }

    if (!this.isValidObjectId(owner)) {
      throw Failure.BadRequest("owner is malformed");
    }

    const jsonProfile = (<any>profile)._json;

    // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
    let user;
    if (ownerKind === OwnerKind.USER) {
      user = await this.$userService.findOne(
        { _id: owner },
        { _id: 1, googleVerified: 1, google: 1, youtubeChannel: 1 }
      );

      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      // Store credentials in secure object
      const secureObjectToBeUpdated: Partial<ISecure> = <Partial<ISecure>>{ google: { accessToken, refreshToken } };
      await this.$secureService.update({ user: Types.ObjectId(owner) }, secureObjectToBeUpdated);

      // Store google profile details
      const userObjectToBeUpdated = {
        googleVerified: true,
        google: {
          id: profile.id,
          name: jsonProfile.name,
          profile: jsonProfile.profile,
          picture: jsonProfile.picture,
          gender: jsonProfile.gender,
          locale: jsonProfile.locale
        }
      };

      await this.$userService.update(owner, userObjectToBeUpdated);
    }

    // save the oauth credentials
    const oAuthCredential: Partial<IOAuthCredential> = {
      accessToken,
      refreshToken,
      owner,
      ownerKind,
      service: OAuthServiceKind.GOOGLE
    };

    // Subscribe to YoutubeCrons
    this.$youtubeVideoAsyncService.subscribeCronsAsync(owner, ownerKind);

    await this.oauthCredentialService.create(oAuthCredential as IOAuthCredential);

    let youtubeChannel: YoutubeChannel | undefined;
    // If youtube channel information throws error, then it should not break oauth flow
    try {
      youtubeChannel = await this.$youtubeService.saveChannelInformation(
        refreshToken,
        accessToken,
        owner,
        ownerKind,
        user
      );

      // Fetch youtube videos when we have successfully retrieved channel info
      this.$youtubeVideoAsyncService.syncYoutubeVideosAsync(owner, ownerKind);
    } catch (error) {
      this.logger?.error(error, "Something went wrong while fetching youtube-channel list");
    }

    // save the google account info
    const googleAccountInfo: Partial<IGoogleAccountInfo> = {
      owner,
      ownerKind,
      id: jsonProfile.id,
      name: jsonProfile.name,
      verified: true,
      picture: jsonProfile.picture
    };

    await this.googleAccountInfoService.create(googleAccountInfo as IGoogleAccountInfo);

    // notify the front end using socket.io that the user successfully updated their link to google
    if (!requestingUserId) {
      this.logger?.warn("Unable to identify user to send linking success event to.");
    } else {
      io.to(requestingUserId).emit("GOOGLE_EXTERNAL_ACCOUNT_LINKING", {
        owner,
        service: OAuthServiceKind.GOOGLE,
        status: "success",
        data: {
          id: youtubeChannel?.id,
          accountName: youtubeChannel?.title || jsonProfile.name,
          supporterCount: youtubeChannel?.statistics?.subscriberCount || null,
          supporterViews: youtubeChannel?.statistics?.viewCount || null,
          contentCount: youtubeChannel?.statistics?.videoCount || null
        }
      });
    }
  }
}
