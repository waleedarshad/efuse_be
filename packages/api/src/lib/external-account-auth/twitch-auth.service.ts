import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";

import { ExternalAccountAuthService } from "./external-account-auth.service";
import { io } from "../../backend/config/get-io-object";
import { IOAuthCredential } from "../../backend/interfaces";
import { ISecure } from "../../backend/interfaces/secure";
import { ITwitchAccountInfo, ITwitchAuthProfile } from "../../backend/interfaces";
import { OAuthCredentialService } from "../oauth-credential.service";
import { SecureService } from "../secure/secure.service";
import { Service, Trace } from "../decorators";
import { TwitchAccountInfoAsyncService } from "../twitch/twitch-account-info-async.service";
import { TwitchAccountInfoService } from "../twitch-account-info.service";
import { TwitchAsyncService } from "../twitch/twitch-async.service";
import { TwitchClipAsyncService } from "../twitch/twitch-clip-async.service";
import { TwitchFollowerAsyncService } from "../twitch/twitch-follower-async.service";
import { TwitchVideoAsyncService } from "../twitch/twitch-video-async.service";
import TwitchHandlerLib from "../creatortools/twitchHandler";

@Service("twitch-auth.service")
export class TwitchAuthService extends ExternalAccountAuthService {
  private $secureService: SecureService;
  private $twitchAccountInfoService: TwitchAccountInfoService;
  private $oauthCredentialService: OAuthCredentialService;
  private $twitchAsyncService: TwitchAsyncService;
  private $twitchClipAsyncService: TwitchClipAsyncService;
  private $twitchFollowerAsyncService: TwitchFollowerAsyncService;
  private $twitchVideoAsyncService: TwitchVideoAsyncService;
  private $twitchAccountInfoAsyncService: TwitchAccountInfoAsyncService;

  constructor() {
    super("twitch.js");

    this.$secureService = new SecureService();
    this.$twitchAccountInfoService = new TwitchAccountInfoService();
    this.$oauthCredentialService = new OAuthCredentialService();
    this.$twitchAsyncService = new TwitchAsyncService();
    this.$twitchClipAsyncService = new TwitchClipAsyncService();
    this.$twitchFollowerAsyncService = new TwitchFollowerAsyncService();
    this.$twitchVideoAsyncService = new TwitchVideoAsyncService();
    this.$twitchAccountInfoAsyncService = new TwitchAccountInfoAsyncService();
  }

  /**
   * @summary Store user's twitch profile in DB
   *
   * @param {string} userId
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Profile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  // TODO: remove userId and set owner/ownerkind
  public async storeProfile(
    owner: string,
    ownerKind: OwnerKind,
    requestingUserId: string,
    accessToken: string,
    refreshToken: string,
    profile: ITwitchAuthProfile
  ): Promise<void> {
    if (!owner) {
      throw Failure.BadRequest("Missing owner param");
    }

    if (!ownerKind) {
      throw Failure.BadRequest("Missing ownerKind");
    }

    // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
    if (ownerKind === OwnerKind.USER) {
      // Store credentials in secure object
      const secureObjectToBeUpdated: Partial<ISecure> = <Partial<ISecure>>{ twitch: { accessToken, refreshToken } };
      await this.$secureService.update({ user: owner }, secureObjectToBeUpdated);

      // TODO: Remove storage of tokens in User model & update all relevant twitch services to avoid using tokens from User Model
      await this.$userService.update(owner, {
        twitchVerified: true,
        twitchID: profile.id,
        twitchUserName: profile.login,
        twitchDisplayName: profile.display_name,
        twitchProfileImage: profile.profile_image_url,
        twitchAccessToken: accessToken,
        twitchRefreshToken: refreshToken,
        twitchEmail: profile.email
      });
    }

    await this.saveOAuthCredential(accessToken, refreshToken, owner, ownerKind);

    // create accountInfo
    const twitchAccountInfoPartial: Partial<ITwitchAccountInfo> = {
      owner,
      ownerKind,
      verified: true,
      accountId: profile.id,
      username: profile.login,
      displayName: profile.display_name,
      profileImage: profile.profile_image_url,
      accountEmail: profile.email
    };

    const existingTwitchAccountInfo = <ITwitchAccountInfo>await this.$twitchAccountInfoService.findOne({ owner });

    if (existingTwitchAccountInfo) {
      await this.$twitchAccountInfoService.updateOne({ owner }, twitchAccountInfoPartial);
    } else {
      await this.$twitchAccountInfoService.create(twitchAccountInfoPartial as ITwitchAccountInfo);
    }

    // update account info and followers now
    await this.$twitchAccountInfoAsyncService.syncTwitchInfo(owner, ownerKind);

    await this.$twitchFollowerAsyncService.syncTwitchFollowers(owner, ownerKind);

    const updatedAccountInfo = <ITwitchAccountInfo>await this.$twitchAccountInfoService.findOne({ owner });

    // notify the front end using socket.io that the user successfully updated their link to google
    if (!requestingUserId) {
      this.logger?.warn("Unable to identify user to send linking success event to.");
    } else {
      io.to(requestingUserId).emit("TWITCH_EXTERNAL_ACCOUNT_LINKING", {
        owner,
        service: <string>OAuthServiceKind.TWITCH,
        status: "success",
        data: {
          id: updatedAccountInfo?.accountId,
          accountName: updatedAccountInfo?.displayName,
          supporterCount: updatedAccountInfo?.followers
        }
      });
    }

    // TODO: [org-clout] support other twitch info updates
    if (ownerKind === OwnerKind.USER) {
      this.$twitchAsyncService.subscribeCronsAsync(owner);

      // Fetch Twitch clips
      this.$twitchClipAsyncService.syncTwitchClipsAsync(owner, ownerKind);

      // Fetch twitch videos
      this.$twitchVideoAsyncService.syncTwitchVideosAsync(owner, ownerKind);

      // Subscribe user to stream webhook
      await TwitchHandlerLib.subscribeToStreamChangeWebhookAsync(owner, ownerKind);
    }
  }

  private async saveOAuthCredential(
    accessToken: string,
    refreshToken: string,
    owner: string,
    ownerKind: OwnerKind
  ): Promise<void> {
    // save the oauth credentials for future use
    const oAuthCredential: Partial<IOAuthCredential> = {
      accessToken,
      refreshToken,
      owner,
      ownerKind,
      service: <string>OAuthServiceKind.TWITCH
    };
    await this.$oauthCredentialService.create(oAuthCredential as IOAuthCredential);
  }
}
