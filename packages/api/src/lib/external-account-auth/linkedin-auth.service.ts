import { Failure } from "@efuse/contracts";
import { Profile } from "passport";

import { ISecure } from "../../backend/interfaces/secure";
import { Service, Trace } from "../decorators";
import { SecureService } from "../secure/secure.service";
import { ExternalAccountAuthService } from "./external-account-auth.service";

@Service("linkedin-auth.service")
export class LinkedinAuthService extends ExternalAccountAuthService {
  private $secureService: SecureService;

  constructor() {
    super("linkedin");

    this.$secureService = new SecureService();
  }

  /**
   * @summary Store user's linkedin profile in DB
   *
   * @param {string} userId
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Profile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeProfile(
    userId: string,
    accessToken: string,
    refreshToken: string,
    profile: Profile
  ): Promise<void> {
    if (!userId) {
      throw Failure.BadRequest("userId not found");
    }

    // Store credentials in secure object
    const secureObjectToBeUpdated: Partial<ISecure> = <Partial<ISecure>>{ linkedin: { accessToken, refreshToken } };
    await this.$secureService.update({ user: userId }, secureObjectToBeUpdated);

    // Store linkedin profile details
    await this.$userService.update(userId, {
      linkedinVerified: true,
      linkedin: {
        id: profile.id,
        displayName: profile.displayName,

        name: { givenName: profile.name?.givenName || "", familyName: (<any>profile).name?.familyName },
        photos: profile.photos?.map((p: { value: string }): string => p.value) || []
      }
    });
  }
}
