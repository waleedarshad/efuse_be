import { Failure } from "@efuse/contracts";
import { Profile } from "passport";

import { ISecure } from "../../backend/interfaces/secure";
import { Service, Trace } from "../decorators";
import { SecureService } from "../secure/secure.service";
import { ExternalAccountAuthService } from "./external-account-auth.service";

@Service("bnet-auth.service")
export class BnetAuthService extends ExternalAccountAuthService {
  private $secureService: SecureService;

  constructor() {
    super("bnet");

    this.$secureService = new SecureService();
  }

  /**
   * @summary Store user's bnet profile in DB
   *
   * @param {string} userId
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Profile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeProfile(
    userId: string,
    accessToken: string,
    refreshToken: string,
    profile: Profile
  ): Promise<void> {
    if (!userId) {
      throw Failure.BadRequest("userId not found");
    }

    // Store credentials in secure object
    const secureObjectToBeUpdated: Partial<ISecure> = <Partial<ISecure>>{ bnet: { accessToken, refreshToken } };
    await this.$secureService.update({ user: userId }, secureObjectToBeUpdated);

    // Store bnet profile details
    await this.$userService.update(userId, { battlenetId: profile.id, battlenetVerified: true });
  }
}
