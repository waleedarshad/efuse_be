import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";

import { AimlabAsyncService } from "../aimlab";
import { ExternalAccountAuthService } from "./external-account-auth.service";
import { IOAuthCredential } from "../../backend/interfaces";
import { OAuthCredentialService } from "../oauth-credential.service";
import { Service, Trace } from "../decorators";

@Service("state-space-auth.service")
export class StateSpaceAuthService extends ExternalAccountAuthService {
  private $aimlabAsyncService: AimlabAsyncService;
  private $oauthCredentialService: OAuthCredentialService;

  constructor() {
    super("state-space");

    this.$aimlabAsyncService = new AimlabAsyncService();
    this.$oauthCredentialService = new OAuthCredentialService();
  }

  /**
   * @summary Store user's state-space profile in DB
   *
   * @param {string} userId
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Profile} profile
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeProfile(
    owner: string,
    ownerKind: OwnerKind,
    accessToken: string,
    refreshToken: string,

    profile?: any
  ): Promise<void> {
    if (!owner) {
      throw Failure.BadRequest("Missing owner param");
    }

    if (!ownerKind) {
      throw Failure.BadRequest("Missing ownerKind");
    }

    await this.saveOAuthCredential(accessToken, refreshToken, owner, ownerKind);

    this.$aimlabAsyncService.subscribeCronsAsync(owner);
  }

  private async saveOAuthCredential(
    accessToken: string,
    refreshToken: string,
    owner: string,
    ownerKind: OwnerKind
  ): Promise<void> {
    const credentials: Partial<IOAuthCredential> = {
      accessToken,
      refreshToken,
      owner,
      ownerKind,
      service: <string>OAuthServiceKind.STATESPACE
    };

    await this.$oauthCredentialService.create(credentials as IOAuthCredential);
  }
}
