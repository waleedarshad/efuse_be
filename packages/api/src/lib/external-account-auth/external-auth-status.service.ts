import { OAuthServiceKind } from "@efuse/entities";
import { OAuthCredentialService } from "../oauth-credential.service";
import { BaseService } from "../base/base.service";
import { Service } from "../decorators";
import { RiotProfileService } from "../riot/riot-profile/riot-profile.service";

@Service("external-auth-status.service")
export class ExternalAuthStatusService extends BaseService {
  private credentialService: OAuthCredentialService = new OAuthCredentialService();
  private riotProfileService: RiotProfileService = new RiotProfileService();

  /**
   * Retrieve a user's connection status for the specified service
   * @param service
   * @param owner
   * @returns
   */
  public async getOwnerConnectionStatus(service: OAuthServiceKind, owner: string): Promise<boolean> {
    let authService: OAuthServiceKind = service;

    if (service === OAuthServiceKind.VALORANT) {
      authService = OAuthServiceKind.RIOT;
    }

    let result = await this.credentialService.getConnectionStatus(owner, authService);

    if (result && service === OAuthServiceKind.VALORANT) {
      // do special check because Riot can auth Valorant OR League of Legends
      const riotProfile = await this.riotProfileService.findOne({ owner });

      if (!riotProfile?.valorantProfile?.success) {
        result = false;
      }
    }

    return result;
  }
}
