import { Failure } from "@efuse/contracts";

import { ITrackingEvent } from "../../backend/interfaces";
import { TrackingEvent, TrackingEventModel } from "../../backend/models/tracking-event";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import Config from "../../async/config";
import { ConstantsService } from "../constants";

@Service("tracking-event.service")
export class TrackingEventService extends BaseService {
  private readonly trackingEvents: TrackingEventModel<ITrackingEvent> = TrackingEvent;
  private constantsService: ConstantsService;

  private storeSegmentEventQueue: any;

  constructor() {
    super();
    this.constantsService = new ConstantsService();

    this.storeSegmentEventQueue = Config.initQueue("storeSegmentEvent");
  }

  /**
   * @summary Store event in DB
   *
   * @param {ITrackingEvent} event
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async create(event: ITrackingEvent): Promise<void> {
    try {
      if (!event || !event.name) {
        throw Failure.BadRequest("event params not found");
      }

      // Fetch constants
      const storableEvents = <{ events: string[] }>await this.constantsService.getVariables("STORABLE_SEGMENTS_EVENTS");

      // Make sure constants are present in DB
      if (!storableEvents || storableEvents.events.length === 0) {
        this.logger?.info("No events found in STORABLE_SEGMENTS_EVENTS constants");
        return;
      }

      // Check whether current event is to be stored
      if (!storableEvents.events.includes(event.name)) {
        this.logger?.info("Event is not found in events array in STORABLE_SEGMENTS_EVENTS constants");
        return;
      }

      // Make sure identity is a valid ObjectId
      const identityUserId = event.identity.userId;
      if (identityUserId && !this.isValidObjectId(identityUserId)) {
        event.identity.anonymousId = identityUserId as string;
        delete event.identity.userId;
      }

      const doc = <ITrackingEvent>event;
      await this.trackingEvents.create(doc);
    } catch (error) {
      this.logger?.debug(error, "Error while storing event");
    }
  }

  /**
   * @summary Store event in DB asynchronously using Bull Queue
   *
   * @param {ITrackingEvent} event
   *
   * @return {Promise<void>}
   */
  @Trace()
  public createAsync(event: ITrackingEvent): void {
    this.storeSegmentEventQueue.add({ event });
  }

  /**
   * Async Processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "tracking-event.service" })
  public static InitAsyncProcessors(): void {
    const trackingEventsService = new TrackingEventService();

    trackingEventsService.storeSegmentEventQueue
      .process((job: { data: { event: ITrackingEvent } }) => {
        return trackingEventsService.create(job.data.event);
      })
      .catch((error) => trackingEventsService.logger?.error(error, "Error while processing queue storeSegmentEvent"));
  }
}
