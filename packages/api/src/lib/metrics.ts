const COMMON_METRICS = {
  START: "start",
  FINISH: "finish",
  ERROR: "error"
};

export const LEAGUE_OF_LEGENDS = {
  CHECK_USER_LEAGUE_DATA_CRON: {
    START: `CheckUsersLOLInfoCron.${COMMON_METRICS.START}`,
    FINISH: `CheckUsersLOLInfoCron.${COMMON_METRICS.FINISH}`,
    ERROR: `CheckUsersLOLInfoCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_LEAGUE_DATA_CRON: {
    START: `FetchLeagueOfLegendsDataCron.${COMMON_METRICS.START}`,
    FINISH: `FetchLeagueOfLegendsDataCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchLeagueOfLegendsDataCron.${COMMON_METRICS.ERROR}`
  },
  GET_LEAGUE_OF_LEGENDS_STATS: {
    START: `getLeagueOfLegendsStats.${COMMON_METRICS.START}`,
    FINISH: `getLeagueOfLegendsStats.${COMMON_METRICS.FINISH}`,
    ERROR: `getLeagueOfLegendsStats.${COMMON_METRICS.ERROR}`
  },
  GET_USER_STATS: {
    START: `getUserLeagueOfLegendsStats.${COMMON_METRICS.START}`,
    FINISH: `getUserLeagueOfLegendsStats.${COMMON_METRICS.FINISH}`,
    ERROR: `getUserLeagueOfLegendsStats.${COMMON_METRICS.ERROR}`
  },
  PROCESS_DOWNLOADED_CHAMPION_DATA: {
    START: `processDownloadedChampionData.${COMMON_METRICS.START}`,
    FINISH: `processDownloadedChampionData.${COMMON_METRICS.FINISH}`,
    ERROR: `processDownloadedChampionData.${COMMON_METRICS.ERROR}`
  },
  SYNC_ALL_LOL_STATS_CRON: {
    START: `SyncAllLeagueOfLegendsStatsCron.${COMMON_METRICS.START}`,
    FINISH: `SyncAllLeagueOfLegendsStatsCron.${COMMON_METRICS.FINISH}`,
    ERROR: `SyncAllLeagueOfLegendsStatsCron.${COMMON_METRICS.ERROR}`
  },
  SYNC_USERS_LOL_STATS: {
    START: `syncUserLeagueOfLegendsStats.${COMMON_METRICS.START}`,
    FINISH: `syncUserLeagueOfLegendsStats.${COMMON_METRICS.FINISH}`,
    ERROR: `syncUserLeagueOfLegendsStats.${COMMON_METRICS.ERROR}`
  },
  VERIFY_SUMMONER_BY_USER_ID: {
    START: `verifyLeagueOfLegendsSummonerByUserId.${COMMON_METRICS.START}`,
    FINISH: `verifyLeagueOfLegendsSummonerByUserId.${COMMON_METRICS.FINISH}`,
    ERROR: `verifyLeagueOfLegendsSummonerByUserId.${COMMON_METRICS.ERROR}`
  },
  VERIFY_SUMMONER: {
    START: `verifyLeagueOfLegendsSummoner.${COMMON_METRICS.START}`,
    FINISH: `verifyLeagueOfLegendsSummoner.${COMMON_METRICS.FINISH}`,
    ERROR: `verifyLeagueOfLegendsSummoner.${COMMON_METRICS.ERROR}`
  },
  UPDATE_RIOT_INFO: {
    START: `updateRiotInfo.${COMMON_METRICS.START}`,
    FINISH: `updateRiotInfo.${COMMON_METRICS.FINISH}`,
    ERROR: `updateRiotInfo.${COMMON_METRICS.ERROR}`
  }
};

export const OPPORTUNITIES = {
  CLOSE_OPPORTUNITIES_BY_DUE_DATE: {
    START: `closeOpportunitiesByDueDate.${COMMON_METRICS.START}`,
    FINISH: `closeOpportunitiesByDueDate.${COMMON_METRICS.FINISH}`,
    ERROR: `closeOpportunitiesByDueDate.${COMMON_METRICS.ERROR}`
  }
};

export const PIPELINE_LEADERBOARD = {
  GENERATE_AIMLAB_PIPELINE_LEADERBOARD: {
    START: `generateAimlabPipelineLeaderboard.${COMMON_METRICS.START}`,
    FINISH: `generateAimlabPipelineLeaderboard.${COMMON_METRICS.FINISH}`,
    ERROR: `generateAimlabPipelineLeaderboard.${COMMON_METRICS.ERROR}`
  },
  GENERATE_LEAGUE_OF_LEGENDS_PIPELINE_LEADERBOARD: {
    START: `generateLeagueOfLegendsPipelineLeaderboard.${COMMON_METRICS.START}`,
    FINISH: `generateLeagueOfLegendsPipelineLeaderboard.${COMMON_METRICS.FINISH}`,
    ERROR: `generateLeagueOfLegendsPipelineLeaderboard.${COMMON_METRICS.ERROR}`
  },
  GENERATE_VALORANT_PIPELINE_LEADERBOARD: {
    START: `generateValorantPipelineLeaderboard.${COMMON_METRICS.START}`,
    FINISH: `generateValorantPipelineLeaderboard.${COMMON_METRICS.FINISH}`,
    ERROR: `generateValorantPipelineLeaderboard.${COMMON_METRICS.ERROR}`
  }
};

export const S3_METRICS = {
  DELETE_MEDIA: {
    START: `deleteMediaFromS3.${COMMON_METRICS.START}`,
    FINISH: `deleteMediaFromS3.${COMMON_METRICS.FINISH}`,
    ERROR: `deleteMediaFromS3.${COMMON_METRICS.ERROR}`
  },
  DOWNLOAD_AND_UPLOAD_MEDIA: {
    START: `downloadFileAndUploadToS3.${COMMON_METRICS.START}`,
    FINISH: `downloadFileAndUploadToS3.${COMMON_METRICS.FINISH}`,
    ERROR: `downloadFileAndUploadToS3.${COMMON_METRICS.ERROR}`
  },
  DOWNLOAD_MEDIA: {
    START: `downloadMediaFromS3.${COMMON_METRICS.START}`,
    FINISH: `downloadMediaFromS3.${COMMON_METRICS.FINISH}`,
    ERROR: `downloadMediaFromS3.${COMMON_METRICS.ERROR}`
  },
  HEAD_OBJECT: {
    START: `getMetadataWithoutFileDownload.${COMMON_METRICS.START}`,
    FINISH: `getMetadataWithoutFileDownload.${COMMON_METRICS.FINISH}`,
    ERROR: `getMetadataWithoutFileDownload.${COMMON_METRICS.ERROR}`
  },
  UPLOAD_MEDIA: {
    START: `uploadMediaToS3.${COMMON_METRICS.START}`,
    FINISH: `uploadMediaToS3.${COMMON_METRICS.FINISH}`,
    ERROR: `uploadMediaToS3.${COMMON_METRICS.ERROR}`
  },
  MULTIPART_UPLOAD: {
    CREATE_MULTIPART_UPLOAD: {
      START: `createMultipartUpload.${COMMON_METRICS.START}`,
      FINISH: `createMultipartUpload.${COMMON_METRICS.FINISH}`,
      ERROR: `createMultipartUpload.${COMMON_METRICS.ERROR}`
    },
    GET_SIGNED_URLS: {
      START: `getSignedUrl.${COMMON_METRICS.START}`,
      FINISH: `getSignedUrl.${COMMON_METRICS.FINISH}`,
      ERROR: `getSignedUrl.${COMMON_METRICS.ERROR}`
    },
    ABORT_MULTIPART_UPLOAD: {
      START: `abortMultipartUpload.${COMMON_METRICS.START}`,
      FINISH: `abortMultipartUpload.${COMMON_METRICS.FINISH}`,
      ERROR: `abortMultipartUpload.${COMMON_METRICS.ERROR}`
    },
    COMPLETE_MULTIPART_UPLOAD: {
      START: `completeMultipartUpload.${COMMON_METRICS.START}`,
      FINISH: `completeMultipartUpload.${COMMON_METRICS.FINISH}`,
      ERROR: `completeMultipartUpload.${COMMON_METRICS.ERROR}`
    }
  }
};

export const SEARCH = {
  START: `search.${COMMON_METRICS.START}`,
  FINISH: `search.${COMMON_METRICS.FINISH}`,
  ERROR: `search.${COMMON_METRICS.ERROR}`,
  SEARCH_FOR_ORG: {
    START: `searchForOrganizations.${COMMON_METRICS.START}`,
    FINISH: `searchForOrganizations.${COMMON_METRICS.FINISH}`,
    ERROR: `searchForOrganizations.${COMMON_METRICS.ERROR}`
  },
  SEARCH_FOR_USER: {
    START: `searchForUsers.${COMMON_METRICS.START}`,
    FINISH: `searchForUsers.${COMMON_METRICS.FINISH}`,
    ERROR: `searchForUsers.${COMMON_METRICS.ERROR}`
  }
};

export const TRANSCODE_VIDEO = {
  START: `transcodeVideo.${COMMON_METRICS.START}`,
  FINISH: `transcodeVideo.${COMMON_METRICS.FINISH}`,
  ERROR: `transcodeVideo.${COMMON_METRICS.ERROR}`
};

export const MEDIA_CONVERT_JOB = {
  START: `mediaConvertJob.${COMMON_METRICS.START}`,
  FINISH: `mediaConvertJob.${COMMON_METRICS.FINISH}`,
  ERROR: `mediaConvertJob.${COMMON_METRICS.ERROR}`
};

export const TRANSCODE_IMAGE = {
  START: `transcodeImage.${COMMON_METRICS.START}`,
  FINISH: `transcodeImage.${COMMON_METRICS.FINISH}`,
  ERROR: `transcodeImage.${COMMON_METRICS.ERROR}`
};

export const OVERWATCH = {
  GET_USER_OVERWATCH_STATS: {
    START: `getUserOverwatch.${COMMON_METRICS.START}`,
    FINISH: `getUserOverwatch.${COMMON_METRICS.FINISH}`,
    ERROR: `getUserOverwatch.${COMMON_METRICS.ERROR}`
  },
  SYNC_USER_OVERWATCH_STATS: {
    START: `syncUserOverStats.${COMMON_METRICS.START}`,
    FINISH: `syncUserOverStats.${COMMON_METRICS.FINISH}`,
    ERROR: `syncUserOverStats.${COMMON_METRICS.ERROR}`
  },
  SYNC_ALL_OVERWATCH_STATS_CRON: {
    START: `syncAllUserOverStats.${COMMON_METRICS.START}`,
    FINISH: `syncAllUserOverStats.${COMMON_METRICS.FINISH}`,
    ERROR: `syncAllUserOverStats.${COMMON_METRICS.ERROR}`
  }
};

export const YOUTUBE = {
  SYNC_USERS_YOUTUBE_CHANNEL_STATS: {
    START: `syncUserYoutubeStats.${COMMON_METRICS.START}`,
    FINISH: `syncUserYoutubeStats.${COMMON_METRICS.FINISH}`,
    ERROR: `syncUserYoutubeStats.${COMMON_METRICS.ERROR}`
  }
};

export const VALORANT = {
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_NA_CRON: {
    START: `FetchAndSaveValorantRecentMatchesNACron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesNACron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesNACron.${COMMON_METRICS.ERROR}`
  },
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_LATAM_CRON: {
    START: `FetchAndSaveValorantRecentMatchesLatamCron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesLatamCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesLatamCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_KR_CRON: {
    START: `FetchAndSaveValorantRecentMatchesKRCron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesKRCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesKRCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_CRON: {
    START: `FetchAndSaveValorantRecentMatchesCron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_EU_CRON: {
    START: `FetchAndSaveValorantRecentMatchesEUCron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesEUCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesEUCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_BR_CRON: {
    START: `FetchAndSaveValorantRecentMatchesBRCron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesBRCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesBRCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_AP_CRON: {
    START: `FetchAndSaveValorantRecentMatchesAPCron.${COMMON_METRICS.START}`,
    FINISH: `FetchAndSaveValorantRecentMatchesAPCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchAndSaveValorantRecentMatchesAPCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_VALORANT_DATA_CRON: {
    START: `FetchValorantDataCron.${COMMON_METRICS.START}`,
    FINISH: `FetchValorantDataCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchValorantDataCron.${COMMON_METRICS.ERROR}`
  },
  FETCH_VALORANT_LEADERBOARD_CRON: {
    START: `FetchValorantLeaderboardCron.${COMMON_METRICS.START}`,
    FINISH: `FetchValorantLeaderboardCron.${COMMON_METRICS.FINISH}`,
    ERROR: `FetchValorantLeaderboardCron.${COMMON_METRICS.ERROR}`
  },
  PROCESS_AGENT_DATA: {
    START: `processAgentData.${COMMON_METRICS.START}`,
    FINISH: `processAgentData.${COMMON_METRICS.FINISH}`,
    ERROR: `processAgentData.${COMMON_METRICS.ERROR}`
  }
};

export const ERENA = {
  UPDATE_ERENA_EVENT_STATUS_CRON: {
    START: `updateErenaEventStatusCron.${COMMON_METRICS.START}`,
    FINISH: `updateErenaEventStatusCron.${COMMON_METRICS.FINISH}`,
    ERROR: `updateErenaEventStatusCron.${COMMON_METRICS.ERROR}`
  }
};

export const MULTIPART_UPLOAD_RESOLVER = {
  INIT_MULTIPART_UPLOAD: {
    START: `initMultipartUploadQuery.${COMMON_METRICS.START}`,
    FINISH: `initMultipartUploadQuery.${COMMON_METRICS.FINISH}`,
    ERROR: `initMultipartUploadQuery.${COMMON_METRICS.ERROR}`
  },
  COMPLETE_MULTIPART_UPLOAD: {
    START: `completeMultipartUploadQuery.${COMMON_METRICS.START}`,
    FINISH: `completeMultipartUploadQuery.${COMMON_METRICS.FINISH}`,
    ERROR: `completeMultipartUploadQuery.${COMMON_METRICS.ERROR}`
  }
};
