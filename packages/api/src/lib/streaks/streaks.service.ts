import moment from "moment";
import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { Streak } from "../../backend/models/streak";
import { IStreak, IValidStreak, IValidStreakInfo, IUserStreak } from "../../backend/interfaces/streak";
import { initQueue } from "../../async/config";
import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { updateUserTags } from "../onesignal";
import { UserService } from "../users/user.service";
@Service("streaks.service")
export class StreaksService extends BaseService {
  private $userService: UserService;
  constructor() {
    super();
    this.$userService = new UserService();
  }
  private validStreaks: IValidStreak = {
    dailyUsage: {
      description: "A user using eFuse at least once every 24 hours",
      checkFunc: () => true // This checkFunc will always be true because this means a user was already online
    },
    dailyComment: {
      description: "A user has commented on a post at least once every 24 hours",
      checkFunc: () => true // Setting to true as we are invoking it after the creation of comment
    }
  };

  /**
   * Return yesterday's localeDateString in en-US format
   * e.g. 07/15/2020
   */
  private getYesterdayDateString(): string {
    const now = new Date();
    const yesterday = moment(now).subtract(1, "d").toDate();
    return yesterday.toLocaleDateString("en-US");
  }

  /**
   * Return today's localeDateString in en-US format
   * e.g. 07/16/2020
   */
  private getDateString(): string {
    return new Date().toLocaleDateString("en-US");
  }

  /**
   * Function to check on a daily streak.  If it doesn't exist it will take care of
   * creating it
   * @param {String} userId       User ID
   * @param {String} streakName   Unique streak same
   * @param {String=} event       Name of the event triggering the check. Ex: Post Create
   */
  @Trace()
  public async checkStreak(userId: string, streakName: string, event = ""): Promise<void> {
    const streak = this.validStreaks[streakName];

    if (event) {
      this.logger?.info({ userId, streakName, event }, `checkStreak called by event: ${event}`);
    }

    if (!streak) {
      this.logger?.error(`Invalid streak name: ${streakName}`);
      return;
    }

    this.updateStreakAsync(userId, streakName, event).catch((error) => {
      this.logger?.error(error, "Error running updateStreakAsync");
    });
  }

  /**
   * Function to extend or create a daily streak
   * @param {String} userId       User ID
   * @param {String} streakName   Unique streak same
   * @param {String=} event       Name of the event triggering the check. Ex: Post Create
   */
  @Trace()
  private async updateStreak(userId: string, streakName: string, event: string): Promise<void> {
    const validStreak: IValidStreakInfo = this.validStreaks[streakName];
    // Fire off the streak checking function
    const result = validStreak.checkFunc();

    if (result) {
      // This means the streak has been achieved.  Let's update the streak object and set redis key
      const user = await this.$userService.findOne({ _id: userId });
      const today = this.getDateString();

      const streaks: IStreak[] | null = await Streak.find({
        user: user?._id,
        streakName,
        isActive: true
      }).sort({ createdAt: -1 });

      let streak: IStreak | null = streaks.length > 0 ? streaks[0] : null;

      if (!streak) {
        streak = new Streak({
          user,
          streakName,
          mostRecentActiveStreakDay: this.getDateString(),
          streakDuration: 1
        });

        try {
          await streak.save();
          this.logger?.info({ userId, streakName, event }, `Created new streak`);
        } catch (error) {
          // NOTE: If we get here it is (likely) because there is a race condition because multiple requests were
          // fired simultaneously and the object was created right before this.  Let's just ignore for the time being.
          // Ideally we find a better way to fire this particular streak off without the race conditions.
          this.logger?.warn({ error }, "Unable to create streak.  Likely because of a race condition.  Moving on.");
        }

        return;
      }

      const mostRecentActiveStreakDay = streak.updatedAt
        ? streak.updatedAt.toLocaleDateString("en-US")
        : streak.mostRecentActiveStreakDay;

      if (mostRecentActiveStreakDay !== today) {
        streak.mostRecentActiveStreakDay = this.getDateString();
        streak.streakDuration += 1;
        this.logger?.info({ userId, streakName, event }, `Incremented streak for user.`);
      }

      await streak.save();

      // Send data to OneSignal
      try {
        await updateUserTags(userId, {
          streakDuration: streak.streakDuration,
          streakName: streak.streakName,
          mostRecentActiveStreakDay: streak.mostRecentActiveStreakDay,
          streakCreatedAt: streak.createdAt,
          streakUpdatedAt: streak.updatedAt
        });
      } catch (error) {
        this.logger?.warn({ error }, "OneSignal Library Error - sending streak data to one signal");
      }
    } else {
      this.logger?.info({ result, validStreak }, "User has not achieved streak.  Maybe next time?");
    }
  }

  /**
   * @summary Method to get streaks
   *
   * @param {Object} query - Query to fetch streaks
   * @param {Object} opts - { projection = {}, sort = {}, limit = 0, lean = false }
   *
   * @return {Array} Array of streaks
   */
  @Trace()
  public async getStreaksByQuery(query: any = {}, opts: any = {}): Promise<IStreak[]> {
    const { projection = {}, sort = {}, limit = 0, lean = false } = opts;

    try {
      let streaks = Streak.find(query, projection).sort(sort).limit(limit);

      if (lean) {
        streaks = streaks.lean();
      }

      const results: IStreak[] = await streaks;

      return results;
    } catch (error) {
      this.logger?.error({ message: error.message, stack: error.stack }, "Error fetching streaks");
      throw error;
    }
  }

  /**
   * @summary Method to get total streaks length
   *
   * @param {Object} query - Query to fetch streaks length
   * @return {Number} Length of streaks
   */
  @Trace()
  public async streaksLength(query = {}): Promise<number> {
    try {
      const streaks = await Streak.find(query).lean();
      return streaks.length;
    } catch (error) {
      this.logger?.error({ message: error.message, stack: error.stack }, "Error fetching streaks length");
      throw error;
    }
  }

  /**
   * @summary Get streak by query
   *
   * @param {Object} query - Query to fetch streak
   * @return {Object} Streak
   */
  @Trace()
  public async getStreakByQuery(query: any): Promise<IStreak> {
    const streak: IStreak | null = await Streak.findOne(query);

    if (!streak) {
      throw Failure.UnprocessableEntity("Streak not found");
    }

    return streak;
  }

  /**
   * Cron job to find all in-active streaks and add to queue
   */
  @Trace({ name: "endStreakCron", service: "streaks.service" })
  public static async endStreakCron(): Promise<void> {
    const service = new StreaksService();
    try {
      /**
       * First we need to determine the oldest allowed date for a streak.  Since we operate in UTC
       * the end of the day happens at 7PM ET / 4PM PT.  Unfortunately with these time zone differences
       * if someone in PT uses eFuse at 1PM, and then again the next day at 4:30pm, they technically will lose their
       * streak even though they used it in two days.  So to minimize this occurring (and it definitely has), we're
       * going to add an 8 hour grace period to help minimize someone using eFuse in two different days within their
       * time zone but losing their streak.  I'm using the PT time zone offset to determine the 8 hours (PT == GMT-8)
       *
       */
      const yesterday = moment()
        .subtract(1, "d")
        .startOf("day")
        // Add an 24 hour grace period to account for time zones.  See above.
        .subtract(24, "h")
        .toDate();
      const streaks: IStreak[] = await Streak.find({
        isActive: true,
        updatedAt: { $lt: yesterday }
      })
        .select("_id mostRecentActiveStreakDay")
        .lean();

      if (!streaks || streaks.length === 0) {
        service.logger?.info(`[$lt: ${yesterday.toString()}] No in-active streaks found!`);
        return;
      }

      for (const streak of streaks) {
        try {
          await service.endStreakAsync(streak._id);
        } catch (error) {
          service.logger?.error({ error: error.message, streakId: streak._id }, "Error ending streak");
        }
      }

      service.logger?.info("All in-active streaks are added to queue");
      return;
    } catch (error) {
      service.logger?.error(error, "Error while ending streaks");
    }
  }

  /**
   * Method to mark streak as in-active
   * @param {String} streakId Streak ID
   */
  @Trace()
  private async endStreak(streakId: string): Promise<void> {
    try {
      const streak: IStreak | null = await Streak.findOne({ _id: streakId }).select("_id isActive user");

      if (!streak) {
        throw Failure.UnprocessableEntity("Streak not found");
      }

      streak.isActive = false;

      await streak.save();

      this.logger?.info({ streakId: streak._id, user: streak.user }, "Streak ended");
      return;
    } catch (error) {
      this.logger?.error(error, "Error while ending a streak");
    }
  }

  /**
   * Add job to endStreak queue
   * @param {String} streakId Streak ID
   */
  private async endStreakAsync(streakId: string): Promise<void> {
    await this.endStreakQueue.add({ streakId });
  }

  /**
   * Async function for updateStreak
   * @param {String} userId User ID
   * @param {String} streakName Streak Name
   * @param {String=} event Name of the event triggering the check. Ex: Post Create
   */
  private async updateStreakAsync(userId: string, streakName: string, event: string): Promise<void> {
    await this.updateStreakAsyncQueue.add({ userId, streakName, event });
  }

  /**
   * Async Queue for the updateStreakAsync function
   */
  private updateStreakAsyncQueue = initQueue("updateStreakAsync");

  /**
   * Async Queue for the endStreak function
   */
  private endStreakQueue = initQueue("endStreak", {
    limiter: { max: 25, duration: 1000 }
  });

  /**
   * Initialize the async handlers
   */
  @Trace({ name: "initAsyncProcessors", service: "streaks.service" })
  public static initAsyncProcessors(): void {
    const service = new StreaksService();

    service.updateStreakAsyncQueue
      .process((job) => {
        const { userId, streakName, event = "" } = job.data;
        return service.updateStreak(userId, streakName, event);
      })
      .catch((error) => {
        service.logger?.error(error, "Error processing updateStreakAsyncQueue");
      });

    service.endStreakQueue
      .process((job) => {
        const { streakId } = job.data;
        return service.endStreak(streakId);
      })
      .catch((error) => {
        service.logger?.error(error, "Error processing endStreakQueue");
      });
  }

  public async lookupDailyStreaks(userId: Types.ObjectId | string): Promise<IUserStreak> {
    const streaks = {} as IUserStreak;
    if (!userId) {
      return streaks;
    }

    const dailyStreaks = [
      { key: "daily", value: "dailyUsage" },
      { key: "dailyComment", value: "dailyComment" }
    ];

    for (const dailyStreak of dailyStreaks) {
      const streak = await Streak.findOne({
        user: userId,
        streakName: dailyStreak.value,
        isActive: true
      })
        .sort({ updatedAt: -1 })
        .select("streakDuration isActive -_id")
        .lean()
        .cache(60);
      streaks[dailyStreak.key] = streak || { isActive: false };
    }

    return streaks;
  }
}
