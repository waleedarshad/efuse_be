import { FilterQuery, QueryOptions } from "mongoose";

import { IStreak } from "../../backend/interfaces/streak";
import { StreakModel, Streak } from "../../backend/models/streak";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";

@Service("streaks-base.service")
export class StreaksBaseService extends BaseService {
  protected readonly $streaks: StreakModel<IStreak> = Streak;

  public async findOne(filter: FilterQuery<IStreak>): Promise<IStreak | null>;
  public async findOne(filter: FilterQuery<IStreak>, projection: any): Promise<IStreak | null>;
  public async findOne(
    filter: FilterQuery<IStreak>,
    projection: any,
    options: QueryOptions | null
  ): Promise<IStreak | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IStreak>,
    projection?: any,
    options?: QueryOptions | null
  ): Promise<IStreak | null> {
    const streak = await this.$streaks.findOne(filter, projection, options);

    return streak;
  }
}
