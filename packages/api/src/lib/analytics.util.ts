import { StatsD } from "hot-shots";
import tracer, { Tracer } from "dd-trace";

const DEFAULT_ENV = "staging";
/**
 * EFuseAnalyticsUtil provides a static singleton for tracing and recording metrics
 */
export class EFuseAnalyticsUtil {
  private static efTracer: Tracer;
  private static efStats: StatsD;

  public static get Tracer(): Tracer {
    EFuseAnalyticsUtil.efTracer = tracer.init({
      analytics: true,
      logInjection: true,
      env: process.env.NODE_ENV || DEFAULT_ENV,
      service: "be",
      runtimeMetrics: true
    });

    return this.efTracer;
  }

  public static get Stats(): StatsD {
    if (!EFuseAnalyticsUtil.efStats) {
      EFuseAnalyticsUtil.efStats = new StatsD({
        prefix: "efuse.",
        globalTags: { env: process.env.NODE_ENV || DEFAULT_ENV }
      });
    }

    return this.efStats;
  }

  public static incrementMetric = (eventName: string, value = 1, tags: string[] = []): void => {
    if (EFuseAnalyticsUtil.Stats) {
      EFuseAnalyticsUtil.Stats.increment(eventName, value, tags);
    }
  };

  public static incrementGauge = (eventName: string, value: number, tags: string[] = []): void => {
    if (EFuseAnalyticsUtil.Stats) {
      EFuseAnalyticsUtil.Stats.gauge(eventName, value, tags);
    }
  };
}
