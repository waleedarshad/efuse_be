import moment from "moment";

export class RankingUtil {
  public static getTimeConstraint(createdAt: Date, timeBucket: number): number {
    // Time difference from now in minutes divided by timeBucket constant and rounding down
    // This accomplishes us to bucket up posts age based on their time to make it more consistent.
    return Math.floor(moment().diff(new Date(createdAt), "m") / timeBucket);
  }
}
