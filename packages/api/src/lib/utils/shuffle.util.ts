export class ShuffleUtil {
  /**
   * This is the Fisher-Yates (aka Knuth) Shuffle, in case you want to look it up
   *
   * From my understanding, every possible shuffled result has an equal chance of existing.
   */
  public static knuthShuffle<T>(items: T[]): T[] {
    if (items.length <= 1) {
      return items;
    }

    let currentIndex = items.length;

    while (currentIndex != 0) {
      const randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      [items[currentIndex], items[randomIndex]] = [items[randomIndex], items[currentIndex]];
    }

    return items;
  }
}
