import { Failure } from "@efuse/contracts";
import { Types, QueryOptions, FilterQuery } from "mongoose";

import { FeedsBaseService } from "../feeds-base.service";
import { Service, Trace } from "../decorators";
import { AutomatedFeedModel, AutomatedFeed } from "../../backend/models/automated-feed";
import { IAutomatedFeed } from "../../backend/interfaces/automated-feed";
import { EFuseAnalyticsUtil } from "../analytics.util";
import { FeedService } from "../feeds/feed.service";

@Service("automated-feed.service")
export class AutomatedFeedService extends FeedsBaseService {
  private readonly automatedFeeds: AutomatedFeedModel<IAutomatedFeed> = AutomatedFeed;
  private feedService: FeedService;

  constructor() {
    super();

    this.feedService = new FeedService();
  }

  /**
   * @summary Create a new automated feed
   *
   * @param {IAutomatedFeed} automatedFeedBody
   *
   * @return {Promise<IAutomatedFeed>}
   */
  @Trace()
  public async create(automatedFeedBody: IAutomatedFeed): Promise<IAutomatedFeed> {
    if (!automatedFeedBody) {
      throw Failure.BadRequest("params are not found");
    }

    const doc = automatedFeedBody;
    const automatedFeed = await this.automatedFeeds.create(doc);
    return automatedFeed;
  }

  public async findOne(filter: FilterQuery<IAutomatedFeed>): Promise<IAutomatedFeed | null>;
  public async findOne(filter: FilterQuery<IAutomatedFeed>, projection: any): Promise<IAutomatedFeed | null>;
  public async findOne(
    filter: FilterQuery<IAutomatedFeed>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IAutomatedFeed | null>;
  @Trace()
  public async findOne(
    filter?: FilterQuery<IAutomatedFeed>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IAutomatedFeed | null> {
    const automatedFeed = await this.automatedFeeds.findOne(filter, projection, options);
    return automatedFeed;
  }

  /**
   * @summary Process automated feed when stream has started
   *
   * @param {Types.ObjectId | string} userId
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async processAutomatedFeed(userId: Types.ObjectId | string): Promise<void> {
    try {
      this.logger?.info({ userId }, "Processing automated feed");
      EFuseAnalyticsUtil.incrementMetric("twitch.postStartStreamMessage.calls");

      const message = await this.automatedFeeds.findOne({
        user: userId,
        automatedType: "startStream"
      });

      if (!message) {
        this.logger?.warn(`User ${userId as string} does not have any automated messages`);
        return;
      }

      message.lastSentAt = new Date();

      // Resolve promises concurrenlty
      await Promise.all([
        this.feedService.create(
          userId,
          message.timelineable,
          message.timelineableType,
          message.verified,
          message.platform,
          message.mentions,
          message.content,
          message.media,
          message.mediaObjects,
          message.twitchClip,
          undefined,
          false,
          undefined,
          undefined,
          undefined,
          undefined,
          undefined,
          message.crossPosts,
          message.associatedGame
        ),
        message.save()
      ]);

      EFuseAnalyticsUtil.incrementMetric("twitch.postStartStreamMessage.success");
    } catch (error) {
      this.logger?.error(error, "Error in processAutomatedFeed ");
      throw error;
    }
  }
}
