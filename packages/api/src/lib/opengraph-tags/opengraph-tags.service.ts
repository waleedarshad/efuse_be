import * as Bull from "bull";
import { randomBytes } from "crypto";
import ogs from "open-graph-scraper";
import { URL } from "url";
import * as Config from "../../async/config";
import { ILink } from "../../backend/interfaces/link";
import { Link } from "../../backend/models/link";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

interface IOGConstant {
  img: string;
  title: string;
  description: string;
}

interface ILinkPreview {
  img: string;
  description: string;
  domain: string;
  title: string;
}

const OPENGRAPH_TAGS_SERVICE = "opengraph-tags.service";

@Service(OPENGRAPH_TAGS_SERVICE)
export class OpengraphTagsService extends BaseModelService<ILink> {
  private scrapAndStoreOGTagQueue: Bull.Queue<any>;

  constructor() {
    super(Link);

    this.scrapAndStoreOGTagQueue = Config.initQueue("scrapAndStoreOGTag");
  }

  /**
   * @summary Async Queue processors
   */
  @Trace({ name: "InitAsyncProcessors", service: OPENGRAPH_TAGS_SERVICE })
  public static InitAsyncProcessors(): void {
    const opengraphTagsService = new OpengraphTagsService();

    opengraphTagsService.scrapAndStoreOGTagQueue
      .process((job: { data: { url: string } }) => opengraphTagsService.scrapAndStoreOGTag(job.data.url))
      .catch((error) => {
        opengraphTagsService.logger?.error(error, "Error while processing queue scrapAndStoreOGTagQueue");
      });
  }

  /**
   * @summary Loop through each Link object & update OG Tags
   */
  @Trace({ name: "UpdateOGTagsCron", service: OPENGRAPH_TAGS_SERVICE })
  public static async UpdateOGTagsCron(): Promise<void> {
    const opengraphTagsService = new OpengraphTagsService();

    try {
      const links = await opengraphTagsService.find({}, {}, { lean: true });

      if (!links || links.length === 0) {
        opengraphTagsService.logger!.info("No links found to scrape & update openGrapTags");
        return;
      }

      await Promise.all(links.map((link) => opengraphTagsService.scrapAndStoreOGTagAsync(link.url)));
    } catch (error) {
      throw opengraphTagsService.handleError(error, "Error in updateOGTags cron");
    }
  }

  /**
   * @summary Create link object
   *
   * @param {string} url
   *
   * @return {Promise<ILink>}
   */
  @Trace()
  public async createLink(url: string): Promise<ILink> {
    const existingLink = await this.findOne({ url });

    // return link if it is already created
    if (existingLink) {
      return existingLink;
    }

    // Generate shortName
    const shortName = await this.generateShortName();

    // Create new link
    const linkDoc = <ILink>{
      shortName,
      url,
      opengraph: {}
    };

    // Save the doc
    const newLink = await this.create(linkDoc);
    return newLink;
  }

  /**
   * @summary Create link objects with default OG tags & add urls in async queue to scrap OG tags
   *
   * @param {string[]} urls - URLs to be scrapped
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async createLinksAndFetchOGTags(urls: Set<string>): Promise<void> {
    try {
      const linkPromises: Promise<ILink>[] = [];

      // Loop each url & create link
      for (const url of urls) {
        this.logger!.info(`Creating link object for url ${url}`);
        linkPromises.push(this.createLink(url));
      }

      // Resolve promises concurrently
      await Promise.all(linkPromises);

      // Set jobs for scrapping OG tags
      await this.scrapOGTagsForEachUrl(urls);
    } catch (error) {
      this.logger!.error(error, "Error in createLinksAndFetchOGTags");
    }
  }

  /**
   * @summary Generate unique shortName for a url
   *
   * @returns {Promise<string>}
   */
  @Trace()
  public async generateShortName(): Promise<string> {
    // Generate shortName
    const shortName = randomBytes(4).toString("hex");

    // Make sure it is unique
    const link = await this.findOne({ shortName }, {}, { lean: true });

    // Generate again
    if (link) {
      return this.generateShortName();
    }

    return shortName;
  }

  /**
   * @summary Get corresponding Link objects for urls
   *
   * @param {string[]} urls
   *
   * @return {Promise<LinkQuery[]>}
   */
  @Trace()
  public async getCorrespondingOGTags(urls: string[]): Promise<ILink[]> {
    const links = await this.find({ url: { $in: urls } }, {}, { lean: true });

    /**
     * $in query does not respect the order of url in the array, due to which
     * records were not in the correct order. Sorting result on the basis of
     * index of url in urls array
     */
    links.sort((a, b) => urls.findIndex((url) => a.url === url) - urls.findIndex((url) => b.url === url));

    return links;
  }

  /**
   * @summary Scrape a particular url to fetch OG tags & store in DB
   *
   * @param {string} url - url to be scrapped
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async scrapAndStoreOGTag(url: string): Promise<void> {
    try {
      this.logger!.info(`Scraping OG tags for url ${url}`);
      const data = await ogs({ url });

      if (data.error) {
        this.logger!.error(data.result.errorDetails, `Error ${data.result.error} while scraping url ${url}`);
        return;
      }

      const { result } = data as ogs.SuccessResult & { result: { ogSiteName: string | undefined } };

      const opengrapTags = <ILinkPreview>{
        // todo: investigate & fix
        // @ts-ignore
        img: result.ogImage?.url,
        title: result.ogTitle,
        description: result.ogDescription,
        domain: result.ogSiteName || new URL(url).hostname
      };

      this.logger!.info(opengrapTags, `Storing OG tags for url ${url}`);
      await this.updateOne({ url }, { $set: { opengraph: opengrapTags } }, { lean: true });
    } catch (error) {
      this.logger!.error(error, `Error while scraping url ${url}`);
    }
  }

  /**
   * @summary Async Bull Queue to scrap a particular url to fetch OG tags & store in DB
   *
   * @param {string} url - url to be scrapped
   *
   */
  @Trace()
  public async scrapAndStoreOGTagAsync(url: string): Promise<void> {
    await this.scrapAndStoreOGTagQueue.add({ url });
  }

  /**
   * @summary Iterate urls array & add each url to bull queue for scrapping OG tags
   *
   * @param {string[]} urls
   */
  @Trace()
  public async scrapOGTagsForEachUrl(urls: Set<string>): Promise<void> {
    // Iterate & add each url to async queue for scraping OG tags
    for (const url of urls) {
      this.logger?.info(`Adding url to async queue for scrapping OG tags ${url}`);

      await this.scrapAndStoreOGTagAsync(url);
    }
  }
}
