const { Logger } = require("@efuse/logger");
const camelcaseKeys = require("camelcase-keys");
const { maxBy } = require("lodash");
const { ObjectId } = require("mongodb");

const { AWS } = require("../../backend/config/aws-instance");
const Config = require("../../async/config");
const { Feed } = require("../../backend/models/Feed");

const comprehend = new AWS.Comprehend();
const logger = Logger.create({ name: "lib/feeds/comprehend.js" });

/**
 * @summary Run comprehend analysis for Feed content
 *
 * @param {ObjectId} feedId - ID of feed to run comprehend analysis against
 */
const comprehendContent = async (feedId) => {
  try {
    const feed = await Feed.findOne({ _id: feedId }).select("_id content metadata");
    if (!feed) {
      logger.info({ feedId }, "Feed not found");
      return;
    }
    if (!feed.content) {
      logger.info({ feedId }, "Feed content not found");
      return;
    }
    const params = { Text: feed.content };
    const detectedLanguage = await comprehend.detectDominantLanguage(params).promise();
    const confidentLanguage = maxBy(detectedLanguage.Languages, "Score");
    if (!confidentLanguage) {
      logger.info("Confident language not detected");
      return;
    }
    params.LanguageCode = confidentLanguage.LanguageCode;
    // todo: investigate
    // @ts-ignore
    const detectedSentiment = await comprehend.detectSentiment(params).promise();
    // todo: investigate
    // @ts-ignore
    const detectedEntities = await comprehend.detectEntities(params).promise();
    const comprehendAnalysis = {
      ...camelcaseKeys(detectedLanguage, { deep: true }),
      sentiments: camelcaseKeys(detectedSentiment, { deep: true }),
      ...camelcaseKeys(detectedEntities, { deep: true })
    };
    feed.metadata.comprehendAnalysis = comprehendAnalysis;
    await feed.save({ validateBeforeSave: false });
    return;
  } catch (error) {
    logger.error({ message: error.message, stack: error.stack }, "Error while comprehending text");
    return;
  }
};

/**
 * Async Queue for the comprehendContent
 */
const comprehendContentQueue = Config.initQueue("comprehendContent", {
  limiter: { max: 10, duration: 1000 }
});

/**
 * @summary Adds job to the queue to run comprehend analysis for Feed content. Rate limited to 10 requests in a second
 *
 * @param {ObjectId} feedId - ID of feed to run comprehend analysis against
 */
const comprehendContentAsync = async (feedId) => {
  return await comprehendContentQueue.add({ feedId });
};

/**
 * Initialize the async handlers
 */
const initProcessors = async () => {
  comprehendContentQueue.process((job) => {
    return comprehendContent(job.data.feedId);
  });
};

module.exports.comprehendContent = comprehendContent;
module.exports.comprehendContentAsync = comprehendContentAsync;
module.exports.initProcessors = initProcessors;
