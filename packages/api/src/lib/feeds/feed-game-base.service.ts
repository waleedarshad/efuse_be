// TODO: Copied from the OpportunityGameBaseService. Can likely be generacized and shared.

import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions, PaginateResult, PaginateOptions } from "mongoose";

import { IFeedGame } from "../../backend/interfaces/feed/feed-game";
import { FeedGame, FeedGameModel } from "../../backend/models/feed-game";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("feed-game-base.service")
export class FeedGameBaseService extends BaseService {
  protected readonly $feedGames: FeedGameModel<IFeedGame> = FeedGame;

  /**
   * @summary Update a particular feed game object
   *
   * @param {FilterQuery<IFeedGame>} condition
   * @param {Partial<IFeedGame>} body
   * @param {QueryOptions | null | undefined} options
   *
   * @returns {Promise<IFeedGame>}
   */
  @Trace()
  public async update(
    condition: FilterQuery<IFeedGame>,
    body: Partial<IFeedGame>,
    options: QueryOptions | null | undefined
  ): Promise<IFeedGame> {
    if (!body) {
      throw Failure.BadRequest("body is missing");
    }

    // Update the corresponding object as per condition
    const feedGame: IFeedGame | null = await this.$feedGames.findOneAndUpdate(condition, { ...body }, options);

    // Make sure object is present
    if (!feedGame) {
      throw Failure.UnprocessableEntity("FeedGame not found");
    }

    return feedGame;
  }

  public async findOne(condition: FilterQuery<IFeedGame>): Promise<IFeedGame | null>;
  public async findOne(
    condition: FilterQuery<IFeedGame>,
    projection: QueryOptions | null | undefined
  ): Promise<IFeedGame | null>;
  public async findOne(
    condition: FilterQuery<IFeedGame>,
    projection: unknown,
    options: QueryOptions | null | undefined
  ): Promise<IFeedGame | null>;
  @Trace()
  public async findOne(
    condition: FilterQuery<IFeedGame>,
    projection?: unknown,
    options?: QueryOptions | null | undefined
  ): Promise<IFeedGame | null> {
    const feedGame = await this.$feedGames.findOne(condition, projection, options);

    return feedGame;
  }

  /**
   * @summary Get paginated feed games
   *
   * @param {FilterQuery<IFeedGame>} filter
   * @param {PaginateOptions} options
   *
   * @returns {Promise<PaginateResult<IFeedGame>>}
   */
  @Trace()
  public async getPaginatedFeedGames(
    filter: FilterQuery<IFeedGame>,
    options: PaginateOptions
  ): Promise<PaginateResult<IFeedGame>> {
    const feedGames = await this.$feedGames.paginate(filter, options);

    return feedGames;
  }

  /**
   * @summary Get a Count of number of documents
   *
   * @param {FilterQuery<IFeedGame>} filter
   *
   * @returns {Promise<number>}
   */
  public async countDocuments(filter?: FilterQuery<IFeedGame>): Promise<number> {
    let count = 0;

    count = await (filter ? this.$feedGames.countDocuments(filter) : this.$feedGames.countDocuments());

    return count;
  }
}
