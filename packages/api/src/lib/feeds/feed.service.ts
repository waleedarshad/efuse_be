import { Failure } from "@efuse/contracts";
import { Queue } from "bull";
import { isEmpty } from "lodash";
import {
  FilterQuery,
  Model,
  PaginateResult,
  QueryOptions,
  Types,
  UpdateQuery,
  LeanDocument,
  PaginateOptions,
  PaginateModel
} from "mongoose";

import { FeedsBaseService } from "../feeds-base.service";
import { Service, Trace } from "../decorators";
import { Feed } from "../../backend/models/Feed";
import { IFeed, IMetadata } from "../../backend/interfaces/feed";
import { IFeedList } from "../../backend/interfaces/feed/feed";
import { IPostFeedItem } from "../../backend/interfaces/feed/post-feed-item";
import { IMention } from "../../backend/interfaces/mention";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";
import { HomeFeedService } from "../home-feed/home-feed.service";
import { IHomeFeed, IHomeFeedAggregate } from "../../backend/interfaces/home-feed";
import { IMedia, IResponse } from "../../backend/interfaces/media";
import { IUser } from "../../backend/interfaces/user";
import { IOrganization } from "../../backend/interfaces/organization";
import { CrossPostPlatforms } from "../cross-post-platforms";
import { TwitterService } from "../twitter/twitter.service";
import { FeedGameService } from "./feed-game.service";
import { YoutubeVideoService } from "../youtube/youtube-video.service";
import { BrazeCampaignName } from "../../lib/braze/braze.enum";
import { BrazeService } from "../../lib/braze/braze.service";

import { FeedRankService } from "../lounge/feed-rank.service";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { OrganizationsService } from "../organizations/organizations.service";
import { StreaksService } from "../streaks/streaks.service";
import { TokenLedgerAction, TokenLedgerKind } from "../../backend/interfaces";
import { TokenLedgerService } from "../token-ledger";
import * as FeedLib from ".";
import ComprehendLib from "./comprehend";
import Config from "../../async/config";
import DiscordHandlers from "../creatortools/discordHandlers";
import FacebookHandlers from "../creatortools/facebookHandlers";
import NotificationsHelper from "../../backend/helpers/notifications.helper";
import PostHookLib from "../hooks/post";
import PushNotificationLib from "../notifications/push";
import { TwitchClipAsyncService } from "../twitch/twitch-clip-async.service";
import UserLib from "../users";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { sanitizeContent } from "../../backend/helpers/feed.helper";
import { GiphyService } from "../giphy/giphy.service";
import { Roles } from "../users/roles";
import { DeleteResult } from "../../backend/types";
import { LoungeFeedService } from "../lounge/lounge-feed.service";
import { Giphy } from "../../backend/models/giphy/giphy.model";
import { HomeFeedAsyncService } from "../home-feed/home-feed-async.service";

@Service("feed.service")
export class FeedService extends FeedsBaseService {
  private readonly feeds: Model<IFeed> & PaginateModel<IFeed> = Feed;

  private feedGameService: FeedGameService;
  private feedRankService: FeedRankService;
  private homeFeedService: HomeFeedService;
  private organizationACLService: OrganizationACLService;
  private organizationsService: OrganizationsService;
  private streaksService: StreaksService;
  private tokenLedgerService: TokenLedgerService;
  private twitterCrossPostingQueue: Queue;
  private youtubeVideoService: YoutubeVideoService;
  private brazeService: BrazeService;
  private homeFeedAsyncService: HomeFeedAsyncService;
  private twitchClipAsyncService: TwitchClipAsyncService;

  constructor() {
    super();

    this.feedGameService = new FeedGameService();
    this.homeFeedService = new HomeFeedService();
    this.organizationACLService = new OrganizationACLService();
    this.organizationsService = new OrganizationsService();
    this.streaksService = new StreaksService();
    this.tokenLedgerService = new TokenLedgerService();
    this.feedRankService = new FeedRankService();
    this.youtubeVideoService = new YoutubeVideoService();
    this.giphyService = new GiphyService();
    this.brazeService = new BrazeService();
    this.homeFeedAsyncService = new HomeFeedAsyncService();
    this.twitchClipAsyncService = new TwitchClipAsyncService();

    this.twitterCrossPostingQueue = Config.initQueue("twitterCrossPosting");
  }

  /**
   * Async Processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "feed.service" })
  public static InitAsyncProcessors(): void {
    const feedService = new FeedService();

    feedService.twitterCrossPostingQueue
      .process((job: { data: { userId: string; content: string; mediaId: string; mediaIds: string[] } }) => {
        const { userId, content, mediaId, mediaIds } = job.data;

        return feedService.twitterCrossPosting(userId, content, mediaId, mediaIds);
      })
      .catch((error) => feedService.logger?.error(error, "Error while processing twitterCrossPostingQueue"));
  }

  /**
   * @summary Create Feed, HomeFeed, LoungeFeed, Cross Posts & return IHomeFeedAggregate
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} timelineable
   * @param {string} timelineableType
   * @param {boolean} verified
   * @param {string} [platform="N/A"]
   * @param {IMention[]} [mentions=[]]
   * @param {string | undefined} content
   * @param {Types.ObjectId | string | undefined} mediaId
   * @param {Types.ObjectId[] | string[] | undefined} mediaIds
   * @param {{Types.ObjectId | string} | undefined} twitchClipId
   * @param {Types.ObjectId | string | undefined} scheduledFeedId
   * @param {boolean | undefined} shared
   * @param {Types.ObjectId | undefined} sharedFeed
   * @param {Types.ObjectId | string | undefined} sharedTimeline
   * @param {Types.ObjectId | string | undefined} sharedTimelineable
   * @param {string | undefined} sharedTimelineableType
   * @param {Types.ObjectId | undefined} sharedPostUserId
   * @param {string[] | undefined} crossPosts
   * @param {Types.ObjectId | string | undefined} associatedGame
   * @param {Types.ObjectId | string | undefined} youtubeVideoId
   *
   * @returns {Promise<IHomeFeedAggregate>}
   */
  @Trace()
  public async create(
    userId: Types.ObjectId | string,
    timelineable: Types.ObjectId | string,
    timelineableType: string,
    verified: boolean,
    platform = "N/A",
    mentions: IMention[] = [],
    content?: string,
    mediaId?: Types.ObjectId | string,
    mediaIds?: Types.ObjectId[] | string[],
    twitchClipId?: Types.ObjectId | string,
    scheduledFeedId?: Types.ObjectId | string,
    shared?: boolean,
    sharedFeed?: Types.ObjectId,
    sharedTimeline?: Types.ObjectId | string,
    sharedTimelineable?: Types.ObjectId | string,
    sharedTimelineableType?: string,
    sharedPostUserId?: Types.ObjectId,
    crossPosts?: string[],
    associatedGame?: Types.ObjectId | string,
    youtubeVideoId?: Types.ObjectId | string
  ): Promise<IHomeFeedAggregate> {
    const homeFeed = await this.createFeedAndPerformActions(
      userId,
      timelineable,
      timelineableType,
      verified,
      platform,
      mentions,
      content,
      mediaId,
      mediaIds,
      twitchClipId,
      scheduledFeedId,
      shared,
      sharedFeed,
      sharedTimeline,
      sharedTimelineable,
      sharedTimelineableType,
      sharedPostUserId,
      crossPosts,
      associatedGame,
      youtubeVideoId
    );

    // Depopulate homeFeed
    homeFeed.depopulate("user");
    homeFeed.depopulate("timelineable");
    homeFeed.depopulate("feed");

    // Build feed aggregate
    const aggregatedFeed: IHomeFeedAggregate = (await FeedLib.aggregateFeedPost(
      homeFeed.toObject(),
      // todo: investigate
      // @ts-ignore
      userId
    )) as IHomeFeedAggregate;

    return aggregatedFeed;
  }

  /**
   * @summary Create Feed, HomeFeed, LoungeFeed, Cross Posts & return IHomeFeed
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} timelineable
   * @param {string} timelineableType
   * @param {boolean} verified
   * @param {string} [platform="N/A"]
   * @param {IMention[]} [mentions=[]]
   * @param {string | undefined} content
   * @param {Types.ObjectId | string | undefined} mediaId
   * @param {Types.ObjectId[] | string[] | undefined} mediaIds
   * @param {{Types.ObjectId | string} | undefined} twitchClipId
   * @param {Types.ObjectId | string | undefined} scheduledFeedId
   * @param {boolean | undefined} shared
   * @param {Types.ObjectId | undefined} sharedFeed
   * @param {Types.ObjectId | string | undefined} sharedTimeline
   * @param {Types.ObjectId | string | undefined} sharedTimelineable
   * @param {string | undefined} sharedTimelineableType
   * @param {Types.ObjectId | undefined} sharedPostUserId
   * @param {string[] | undefined} crossPosts
   * @param {Types.ObjectId | string | undefined} associatedGame
   * @param {Types.ObjectId | string | undefined} youtubeVideoId
   *
   * @returns {Promise<IHomeFeedAggregate>}
   */
  public async createFeedAndPerformActions(
    userId: Types.ObjectId | string,
    timelineable: Types.ObjectId | string,
    timelineableType: string,
    verified: boolean,
    platform = "N/A",
    mentions: IMention[] = [],
    content?: string,
    mediaId?: Types.ObjectId | string,
    mediaIds?: Types.ObjectId[] | string[],
    twitchClipId?: Types.ObjectId | string,
    scheduledFeedId?: Types.ObjectId | string,
    shared?: boolean,
    sharedFeed?: Types.ObjectId,
    sharedTimeline?: Types.ObjectId | string,
    sharedTimelineable?: Types.ObjectId | string,
    sharedTimelineableType?: string,
    sharedPostUserId?: Types.ObjectId,
    crossPosts?: string[],
    associatedGame?: Types.ObjectId | string,
    youtubeVideoId?: Types.ObjectId | string
  ): Promise<IHomeFeed> {
    // Make sure required params are present
    if (!userId || !timelineable || !timelineableType) {
      throw Failure.BadRequest("Make sure to pass userId, timelineable & timelineableType");
    }

    // todo: investigate & fix
    // @ts-ignore
    const user: IUser = (await UserLib.getUserById(userId)) as IUser;

    // Make sure if post is empty that there is media attached
    if (isEmpty(content)) {
      if (isEmpty(mediaId) && isEmpty(mediaIds) && isEmpty(twitchClipId) && isEmpty("youtubeVideoId")) {
        this.logger?.error("Invalid post data.  This should never happen.  Must be enforced by client");
        throw Failure.BadRequest("Post must have text or media attached");
      }
    }

    // Make sure when sharing a post that all of the shared post fields exist
    if (shared) {
      if (
        isEmpty(sharedFeed) ||
        isEmpty(sharedTimeline) ||
        isEmpty(sharedTimelineable) ||
        isEmpty(sharedTimelineableType) ||
        isEmpty(sharedPostUserId)
      ) {
        this.logger?.error("Invalid shared post data.  This should never happen.  Must be enforced by client");
        throw Failure.BadRequest("Invalid shared post");
      }
    }

    // Set post verified & Make sure user can post onBehalfOfOrganization
    // let postVerified = verified;
    if (timelineableType === "organizations") {
      // const { organization } =
      await this.organizationACLService.authorizedToAccess(userId, timelineable, OrganizationAccessTypes.ANY);

      // Set verified
      // postVerified = organization.verified?.status || false;
    }

    // while processing scheduledFeed make sure we are not creating duplicates
    if (scheduledFeedId) {
      const existingFeed = await this.findOne(
        { scheduledFeed: scheduledFeedId },
        { _id: 1 },
        // todo: investigate correct usage, is this from a plugin?
        <never>{ autopopulate: false }
      );
      if (existingFeed) {
        this.logger?.info(
          { scheduledFeedId, feedId: existingFeed._id },
          "corresponding scheduled feed has already been created"
        );
        throw Failure.BadRequest("Scheduled feed has already been processed");
      }
    }

    // Create Feed Object & store in DB
    const doc = <IFeed>{
      user: userId,
      twitchClip: twitchClipId,
      scheduledFeed: scheduledFeedId,
      mentions,
      content,
      shared,
      sharedFeed,
      sharedTimeline,
      sharedTimelineable,
      sharedTimelineableType,
      platform,
      youtubeVideo: youtubeVideoId
    };

    const feed: IFeed = await this.feeds.create(doc);

    const timelineableObjectId = new Types.ObjectId(timelineable);
    const userObjectId = new Types.ObjectId(userId);
    const homeFeedBody = <IHomeFeed>{
      user: userObjectId,
      feed: feed._id,
      timelineable: timelineableObjectId,
      original: true,
      timelineableType
    };

    // Create corresponding home & update media
    const homeFeedMediaPromise: Promise<IHomeFeed | IMedia | IResponse>[] = [this.homeFeedService.create(homeFeedBody)];
    if (mediaId) {
      homeFeedMediaPromise.push(this.mediaService.update(mediaId, { mediaable: feed._id }));
    }

    if (mediaIds && !isEmpty(mediaIds)) {
      homeFeedMediaPromise.push(this.mediaService.updateMany(mediaIds, { mediaable: feed._id }));
    }

    // Resolve promises concurrently
    const homeFeedMediaResolvedPromises = await Promise.all(homeFeedMediaPromise);

    const homeFeed: IHomeFeed = homeFeedMediaResolvedPromises[0] as IHomeFeed;

    // Send notifications to anyone mentioned in post
    await this.feedMentionsNotification(user, mentions, homeFeed._id);

    // TODO: instantiating here to avoid a circle dependency. We should look into possibly moving this function into its own service or something.
    const loungeFeedService = new LoungeFeedService();
    // Async promises
    const asyncPromises = [
      // save hash tags
      // todo: investigate
      // @ts-ignore
      PostHookLib.saveHashtags(feed._id),

      // Add feed to followers
      this.homeFeedAsyncService.updateFollowersHomeFeedAsync(feed._id, timelineable, timelineableType),

      // Add feed to lounge
      loungeFeedService.updateLoungeFeedAsync(userId, feed._id, timelineable, timelineableType, verified),

      // Check Streak
      this.streaksService.checkStreak(userId.toString(), "dailyUsage", "Post Create")
    ];

    // Perform comprehend analysis if content is present
    if (feed.content) {
      // todo: investigate
      // @ts-ignore
      asyncPromises.push(ComprehendLib.comprehendContentAsync(feed._id));
    }

    // Send share notification
    if (shared) {
      asyncPromises.push(NotificationsHelper.shareNotification(user, sharedPostUserId, sharedTimeline, homeFeed._id));
    }

    // Process twitch clip
    if (feed.twitchClip) {
      asyncPromises.push(this.twitchClipAsyncService.uploadTwitchClipAsync(feed._id, feed.twitchClip));
    }

    const sanitizeText = sanitizeContent(content, mentions);
    // Twitter Cross posting
    if (crossPosts?.includes(CrossPostPlatforms.TWITTER)) {
      asyncPromises.push(
        this.twitterCrossPostingAsync(
          String(userId),
          sanitizeText || "",
          mediaId as string,
          mediaIds as Types.ObjectId[] | string[]
        )
      );
    }

    // Discord Cross posting
    if (crossPosts?.includes(CrossPostPlatforms.DISCORD)) {
      asyncPromises.push(DiscordHandlers.crossPostToDiscordAsync(userId, sanitizeText, mediaId, mediaIds));
    }

    // Facebook Cross posting
    if (crossPosts?.includes(CrossPostPlatforms.FACEBOOK)) {
      asyncPromises.push(FacebookHandlers.crossPostToFacebookPageAsync(userId, feed.content, mediaId, mediaIds));
    }

    // Associate game
    if (associatedGame) {
      asyncPromises.push(this.feedGameService.associateGame(feed._id, associatedGame));
    }

    // Resolve all async promises concurrently
    await Promise.all(asyncPromises);

    // give the user credits for creating the post
    await this.tokenLedgerService.createEntry(TokenLedgerAction.newPost, TokenLedgerKind.credit, userId);

    return homeFeed;
  }

  public async find(filter: FilterQuery<IFeed>): Promise<IFeed[]>;
  public async find(filter: FilterQuery<IFeed>, projection: any): Promise<IFeed[]>;
  public async find(
    filter: FilterQuery<IFeed>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IFeed[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IFeed>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IFeed[]> {
    const feeds = await this.feeds.find(filter, projection, options);
    return feeds;
  }

  public async findOne(filter: FilterQuery<IFeed>): Promise<IFeed | null>;
  public async findOne(filter: FilterQuery<IFeed>, projection: any): Promise<IFeed | null>;
  public async findOne(
    filter: FilterQuery<IFeed>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IFeed | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IFeed>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IFeed | null> {
    const feeds = await this.feeds.findOne(filter, projection, options);
    return feeds;
  }

  @Trace()
  public async getFollowingFeed(
    userId: Types.ObjectId | string,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    // Make sure page param is positive
    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    const feed = await this.homeFeedService.getFollowingFeed(userId, pageParam, limitParam);
    return feed;
  }

  /**
   * @summary Get paginated user feed by Id
   *
   * @param userId
   *
   * @return {Promise<PaginateResult<IFeedList[]>>}
   * @memberof FeedService
   */
  @Trace()
  public async getPaginatedUserFeed(
    currentUserId: string | Types.ObjectId,
    userId: Types.ObjectId | string,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    // Make sure page param is positive
    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    const homeFeed = await this.homeFeedService.getUserFeed(currentUserId, userId, pageParam, limitParam);

    return homeFeed;
  }

  /**
   * @summary Get paginated organization feed
   *
   * @param {string | Types.ObjectId} currentUserId current UserID
   * @param {string} orgId Organization Id against to fetch feed
   * @param {number} page page number
   * @param {number} limit limit
   *
   * @return {Promise<PaginateResult<IFeedList>>}
   * @memberof FeedService
   */
  @Trace()
  public async getPaginatedOrganizationFeed(
    currentUserId: string | Types.ObjectId,
    orgId: string,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    // Make sure page param is positive
    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    const organization: IOrganization = await this.organizationsService.getOrganizationById(orgId);
    const orgCaptains = organization.captains || [];
    const userIds = [...orgCaptains];

    if (organization.user) {
      userIds.push(organization.user);
    }

    const homeFeed = await this.homeFeedService.getOrganizationHomeFeed(
      currentUserId,
      userIds,
      orgId,
      pageParam,
      limitParam
    );

    return homeFeed;
  }

  @Trace()
  public async performSoftDelete(userId: Types.ObjectId | string): Promise<DeleteResult> {
    const result: DeleteResult = await (<any>this.feeds).delete({ user: userId });

    return result;
  }

  @Trace()
  public async performSoftUndelete(userId: Types.ObjectId | string): Promise<DeleteResult> {
    const result: DeleteResult = await (<any>this.feeds).restore({ user: userId });

    return result;
  }

  @Trace()
  public async performSoftDeletePost(feedId: Types.ObjectId | string): Promise<DeleteResult> {
    const result: DeleteResult = await (<any>this.feeds).delete({
      _id: feedId
    });

    return result;
  }

  /**
   * @summary Boost a particular feed by setting boosted to true
   *
   * @param {Object} currentUser - object of current user i.e req.user
   * @param {ObjectId} feedId - ID of the feed to be boosted
   * @param {Boolean} boosted - true/false
   *
   */
  @Trace()
  public async toggleFeedBoost(
    currentUser: IUser,
    feedId: Types.ObjectId,
    boosted: boolean,
    usePoints = false
  ): Promise<boolean> {
    // Make sure only eFuse admin is allowed
    if (!usePoints && (isEmpty(currentUser) || isEmpty(currentUser.roles) || !currentUser.roles.includes("admin"))) {
      this.logger?.info("currentUser is not admin | Access Denied");
      throw Failure.Unauthorized("You are not authorized to perform this action!");
    }

    // Validate feedId
    if (!this.isValidObjectId(feedId)) {
      this.logger?.info({ feedId }, "feedId is malfored");
      throw Failure.BadRequest("feedId is malformed");
    }

    // Fetch feed
    const feed = await this.findOne({ _id: feedId }, { _id: 1, metadata: 1 });

    // Check feed presence
    if (!feed) {
      this.logger?.info({ feedId }, "Feed not found!");
      throw Failure.NotFound("Feed not found!");
    }

    // update feed
    if (!feed.metadata) {
      feed.metadata = <IMetadata>{ boosted };
    } else {
      feed.metadata.boosted = boosted;
    }

    // remove points from boosting before saving
    if (usePoints) {
      await this.tokenLedgerService.createEntry(TokenLedgerAction.boostPost, TokenLedgerKind.debit, currentUser._id);
    }

    await feed.save({ validateBeforeSave: false });

    // Re-calculate feedRank
    await this.feedRankService.calculateAndStoreFeedRankAsync(feedId);

    return true;
  }

  /**
   * @summary Cross-Post to twitter
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} content
   * @param {Types.ObjectId | string} mediaId
   * @param {Types.ObjectId[] | string[]} mediaIds
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async twitterCrossPosting(
    userId: string,
    content: string,
    mediaId: string,
    mediaIds: Types.ObjectId[] | string[]
  ): Promise<void> {
    const twitterService: TwitterService = new TwitterService(userId);
    try {
      let tweetUrl = "";
      if (mediaId) {
        const { url } = await twitterService.createTweet(content, mediaId);
        tweetUrl = url;
      } else {
        const { url } = await twitterService.createMediaTweet(content, mediaIds);
        tweetUrl = url;
      }

      await PushNotificationLib.sendNotification(userId, "Tweet posted successfully", tweetUrl, "hype");
    } catch (error) {
      this.logger?.warn(error);
      await PushNotificationLib.sendNotification(userId, "Tweet failed to post", "BLAH", "hype");
    }
  }

  /**
   * @summary Cross-Post to twitter using async queue
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} content
   * @param {Types.ObjectId | string} mediaId
   * @param {Types.ObjectId[] | string[]} mediaIds
   *
   * @returns {void}
   */
  @Trace()
  public async twitterCrossPostingAsync(
    userId: Types.ObjectId | string,
    content: string,
    mediaId: Types.ObjectId | string,
    mediaIds: Types.ObjectId[] | string[]
  ): Promise<void> {
    await this.twitterCrossPostingQueue.add({ userId, content, mediaId, mediaIds });
  }

  /**
   *
   * @param {string | Types.ObjectId} feedId
   * @param {IPostFeedItem} feedObject {Updated feedObject params}
   *
   * @returns {Promise<IPostFeedItem>} return updated post object
   */
  @Trace()
  public async updateFeed(
    currentUserId: Types.ObjectId,
    feedId: string | Types.ObjectId,
    feedObject: IPostFeedItem
  ): Promise<IFeed> {
    await this.updateFeedPost(currentUserId, feedId, feedObject);

    const updatedFeed = <IFeed>(
      (<unknown>await this.homeFeedService.getHomeFeedPost(feedObject.parentFeedId, currentUserId.toHexString()))
    );

    if (!updatedFeed) {
      throw Failure.UnprocessableEntity("feed not found");
    }

    return <IFeed>{ ...updatedFeed, _id: updatedFeed.feedId };
  }

  /**
   *
   * @param {string | Types.ObjectId} feedId
   * @param {IPostFeedItem} feedObject {Updated feedObject params}
   */
  @Trace()
  public async updateFeedPost(
    currentUserId: Types.ObjectId,
    feedId: string | Types.ObjectId,
    feedObject: IPostFeedItem
  ): Promise<LeanDocument<IFeed>> {
    const feed = await this.feeds.findOne({ _id: feedId });

    if (!feed) {
      throw Failure.UnprocessableEntity("feed not found");
    }
    // Begin Media Upload
    if (feedObject.media) {
      await this.mediaService.updateMedia(currentUserId, feedId, "feeds", feedObject.media);
    }

    if (!isEmpty(feedObject.giphy)) {
      const giphy = await this.giphyService.uploadGiphyAsMedia(
        feedObject.giphy as Giphy,
        currentUserId,
        "feeds",
        feed._id
      );
      feed.giphy = giphy._id;
    }

    if (feedObject.associatedGame) {
      await this.feedGameService.associateGame(feed._id, feedObject.associatedGame._id);
    }

    if (feedObject.twitchClip) {
      const twitchClipId = feedObject.twitchClip;
      // Make sure it is a valid ObjectId
      if (!Types.ObjectId.isValid(twitchClipId)) {
        throw Failure.BadRequest("invalid twitchClip ID");
      }

      const existingTwitchClip = await this.twitchClipService.findOne(
        { _id: twitchClipId, user: currentUserId },
        { _id: 1 },
        { lean: true }
      );

      if (!existingTwitchClip) {
        throw Failure.UnprocessableEntity("Twitch clip not found");
      }

      // will 'feed.twtichClip' always exist? telling typescript it will be since it appears this will always be the case.
      await this.twitchClipAsyncService.uploadTwitchClipAsync(feed._id, feed.twitchClip!);

      feed.twitchClip = feedObject.twitchClip;
    }

    const contentUpdated = feed.content !== feedObject.text && feedObject.text;
    feed.content = feedObject.text;

    const updatedMentionsList = feedObject.mentions
      ?.filter((mention) => mention.id)
      .map(
        (mention) =>
          ({
            display: mention.display,
            id: mention.id,
            type: mention.type,
            childIndex: mention.childIndex,
            plainTextIndex: mention.plainTextIndex,
            index: mention.index
          } as IMention)
      );

    feed.mentions = updatedMentionsList;

    // Handle youtube video update
    if (isEmpty(feedObject.associatedYoutubeVideo)) {
      feed.youtubeVideo = undefined;
    } else {
      // Verify Ownership
      await this.youtubeVideoService.validateYoutubeOwnership(currentUserId, feedObject.associatedYoutubeVideo?._id);

      feed.youtubeVideo = feedObject.associatedYoutubeVideo?._id;
    }

    await feed.save();

    if (contentUpdated) {
      // todo: investigate
      // @ts-ignore
      await ComprehendLib.comprehendContentAsync(feed._id);
    }
    return feed.toObject();
  }

  /**
   * @summary Update a particular feed object
   *
   * @param {FilterQuery<IFeed>} filter
   * @param {UpdateQuery<IFeed>} body
   * @param {QueryOptions | null} options
   *
   * @returns {Promise<IFeed>}
   */
  @Trace()
  public async update(
    filter: FilterQuery<IFeed>,
    body: UpdateQuery<IFeed>,
    options?: QueryOptions | null
  ): Promise<IFeed> {
    const feed = await this.feeds.findOneAndUpdate(filter, body, options);

    if (!feed) {
      throw Failure.UnprocessableEntity("Feed not found");
    }

    return feed;
  }

  public async deletePost(user: IUser, feedId: Types.ObjectId | string): Promise<boolean> {
    try {
      const userId = user._id;

      // Find the feed
      const feed = await this.validateFeed(feedId);

      // Make sure user is the owner of feed
      if (String(feed.user) !== String(userId) && !user.roles.includes(Roles.ADMIN)) {
        throw Failure.Forbidden("You are not allowed to delete this post");
      }

      const deletePost = await this.performSoftDeletePost(feedId);

      if (deletePost?.ok && deletePost?.n && deletePost?.n > 0) {
        const feedObjectId = GeneralHelper.getObjectId(feedId);

        await this.feedRankService.removeObject(feedObjectId);

        await this.homeFeedAsyncService.removeFeedAsync(feedId);

        return true;
      }
      return false;
    } catch (error) {
      throw this.handleError(error, "Unable to delete Post");
    }
  }

  /**
   * @summary Validate Feed
   *
   * @param {Types.ObjectId | string} feedId
   *
   * @returns {Promise<IFeed>}
   */
  @Trace()
  public async validateFeed(feedId: Types.ObjectId | string): Promise<IFeed> {
    // Make sure feedId is valid
    if (!this.isValidObjectId(feedId)) {
      throw Failure.BadRequest("Invalid feedId");
    }

    // Find out comment
    const feed = await this.findOne({ _id: feedId }, {}, { autopopulate: false, lean: true });

    // Make sure comment exists
    if (!feed) {
      throw Failure.UnprocessableEntity("Feed not found");
    }

    return feed;
  }

  /**
   *
   * @param {string | Types.ObjectId} sender
   * @param {IMention[]} mentions
   * @param {string | Types.ObjectId} feedId
   */
  @Trace()
  public async feedMentionsNotification(
    sender: IUser,
    mentions: IMention[],
    feedId: string | Types.ObjectId
  ): Promise<void> {
    if (mentions && mentions.length > 0) {
      const senderId = sender.id;
      mentions.forEach(async (mention) => {
        const receiverId = mention.id;
        if (mention.type !== "org" && String(senderId) !== String(receiverId)) {
          this.brazeService.sendCampaignMessageAsync(BrazeCampaignName.POST_MENTION, {
            trigger_properties: {
              sender_user_id: senderId,
              sender_username: sender.username,
              sender_name: sender.name,
              feed_id: String(feedId)
            },
            recipients: [{ external_user_id: String(receiverId) }]
          });
        }
      });
    }
  }

  /**
   * @summary Get paginated feed
   *
   * @param {FilterQuery<IFeed>} filter
   * @param {PaginateOptions} options
   *
   * @returns {Promise<PaginateResult<IFeed>>}
   */
  @Trace()
  public async getPaginatedFeed(filter: FilterQuery<IFeed>, options: PaginateOptions): Promise<PaginateResult<IFeed>> {
    const feed = await this.feeds.paginate(filter, options);

    return feed;
  }
}
