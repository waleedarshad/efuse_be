export enum GamePostsPaginationLimitEnum {
  DEFAULT = 10,
  MAX = 25
}
