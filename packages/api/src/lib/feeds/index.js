// 3rd party libraries
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { isEmpty } = require("lodash");
const { ObjectId } = require("mongodb");

// Models
const { Feed } = require("../../backend/models/Feed");
const { Friend } = require("../../backend/models/friend");
const { GiphyModel } = require("../../backend/models/giphy/giphy.model");
const { HypeModel } = require("../../backend/models/hype/hype.model");
const { Organization } = require("../../backend/models/Organization");
const { OrganizationFollower } = require("../../backend/models/organization-follower");

// Internal helpers & libraries
const UserLib = require("../users");
const Config = require("../../async/config");

const WhitelistHelper = require("../../backend/helpers/whitelist_helper");

const { OpengraphTagsService } = require("../opengraph-tags/opengraph-tags.service");
const { FeedRankService } = require("../lounge/feed-rank.service");
const { MediaService } = require("../media/media.service");
const { BlockedUsersService } = require("../blocked-users/blocked-users.service");
const { HypeService } = require("../hype/hype.service");
const { FeedGameService } = require("./feed-game.service");
const { YoutubeVideoBaseService } = require("../youtube/youtube-video-base.service");
const { HypeEnum } = require("@efuse/entities");

const feedRankService = new FeedRankService();
const openGraphTagsService = new OpengraphTagsService();
const mediaService = new MediaService();
const blockedUsersService = new BlockedUsersService();
const hypeService = new HypeService();
const feedGameService = new FeedGameService();
const youtubeVideoBaseService = new YoutubeVideoBaseService();

/**
 * @summary Aggregate feed post
 *
 * @param {Object} feedObjectToBeAggregated - Feed data
 * @param {string} currentUserId - Id of the current user
 * @return {Promise<any>} feed
 */
const aggregateFeedPost = async (feedObjectToBeAggregated, currentUserId) => {
  const feed = { ...feedObjectToBeAggregated };

  const like = await HypeModel.findOne({
    user: currentUserId,
    objType: HypeEnum.FEED,
    objId: feed.feed
  }).lean();

  if (like) {
    feed.like = like;
  }

  // TODO: Convert to its own function and in the future get from redis
  feed.associatedFeed = await Feed.findOne({
    _id: feed.feed,
    "reports.reportedBy": { $ne: currentUserId },
    deleted: { $ne: true }
  }).lean();

  // If this is true that means the post was deleted yet still showing up in homefeed
  // Let's throw a warning to make sure someone eventually takes a look
  if (!feed.associatedFeed) {
    logger.warn(
      `Found a homefeed that doesn't have associatedFeed.  Probably means lambda didn't clean up.: ${JSON.stringify(
        feed
      )}`
    );
    // Removing it from featured feed
    await feedRankService.removeObject(feed.feed);
    return;
  }

  // TODO: Convert to its own function and in the future get from redis
  feed.associatedUser = await getAssociatedUser(feed.associatedFeed.user);

  if (!feed.associatedUser) {
    logger.warn(`Found a homefeed that doesn't have associatedUser. ${feed._id}`);
    // Removing it from featured feed
    await feedRankService.removeObject(feed.feed);
    return;
  }

  if (feed.associatedFeed.giphy) {
    feed.associatedGiphy = await GiphyModel.findOne({
      _id: feed.associatedFeed.giphy
    }).lean();
  }
  // TODO: Convert to its own function and in the future get from redis
  feed.associatedFeed.author = feed.associatedUser;

  // Fetch associated media
  // todo: investigate
  // @ts-ignore
  const associatedMedia = await mediaService.getAssociatedMedia(currentUserId, feed.feed, "feeds");
  if (associatedMedia.media) {
    feed.associatedMedia = associatedMedia.media;
  }
  feed.attachments = associatedMedia.medias;

  // TODO: Convert to its own function and in the future get from redis
  if (feed.timelineableType === "organizations") {
    const associatedTimelineable = await Organization.findOne({
      _id: feed.timelineable
    }).lean();
    if (associatedTimelineable) {
      feed.associatedTimelineable = associatedTimelineable;
    }
  }

  if (feed.associatedFeed.sharedTimelineableType === "organizations") {
    logger.info("Shared Timelineable", feed.sharedTimelineable);
    const associatedSharedTimelineable = await Organization.findOne({
      _id: feed.associatedFeed.sharedTimelineable
    }).lean();

    if (associatedSharedTimelineable) {
      feed.associatedFeed.associatedSharedTimelineable = associatedSharedTimelineable;
    }
  }

  if (feed.associatedFeed.sharedFeed) {
    const associatedSharedFeed = await Feed.findOne({
      _id: feed.associatedFeed.sharedFeed
    }).lean();
    if (associatedSharedFeed) {
      associatedSharedFeed.author = await UserLib.getUserById(associatedSharedFeed.user, {
        dailyStreak: true
      });
      if (associatedSharedFeed.giphy) {
        feed.associatedGiphy = await GiphyModel.findOne({
          _id: associatedSharedFeed.giphy
        }).lean();
      }
      feed.associatedFeed.associatedSharedFeed = associatedSharedFeed;
    }
  }

  // Fetch associated media
  const sharedAssociatedMedia = await mediaService.getAssociatedMedia(
    currentUserId,
    feed.associatedFeed.sharedFeed,
    "feeds"
  );

  if (sharedAssociatedMedia.media) {
    feed.associatedFeed.sharedFeedMedia = sharedAssociatedMedia.media;
  }

  feed.associatedFeed.attachments = sharedAssociatedMedia.medias;

  const hypeCount = await hypeService.getPostHypeCount(feed.associatedFeed._id);
  feed.associatedFeed.hypes = hypeCount + Number(feed.associatedFeed.likes);

  // todo: investigate
  // @ts-ignore
  const userHypeCount = await hypeService.getPostHypeCount(feed.associatedFeed._id, currentUserId);
  feed.associatedFeed.userHypes = userHypeCount;

  // Fetch OG tags for shared links
  const opengraphTags = await openGraphTagsService.getCorrespondingOGTags(feed.associatedFeed.metadata?.sharedLinks);
  if (!isEmpty(opengraphTags)) {
    feed.associatedFeed.opengraphTags = opengraphTags;
  }

  const associatedGame = await feedGameService.getAssociatedGame(feed.associatedFeed._id);

  if (associatedGame) {
    feed.associatedGame = associatedGame;
  }

  // Append Associated YoutubeVideo
  const { youtubeVideo } = feed.associatedFeed;

  if (youtubeVideo) {
    feed.associatedYoutubeVideo = await getYoutubeVideo(youtubeVideo);
  }

  return feed;
};

const hashTagQueue = Config.initQueue("saveHashTag");

const saveHashtagsAsync = async (postId) => {
  logger.info({ postId }, "saving hashtag");
  hashTagQueue.add({ postId });
};

/**
 * @summary Extract and update post hastags
 *
 * @param {ObjectId} postId - Id of post
 */
const extractAndUpdateHashTags = async (postId) => {
  try {
    const feed = await Feed.findOne({ _id: postId });
    const hashtags = feed.content.split(" ").filter((v) => v.startsWith("#"));
    logger.info({ hashtags }, "hashtags");
    if (!feed) {
      return;
    }
    feed.metadata.hashtags = hashtags;
    await feed.save();
  } catch (error) {
    logger.error({ error }, "Error in extractAndUpdateHashTags ");
    throw error;
  }
};

const initProcessors = async () => {
  hashTagQueue.process((job) => {
    const { postId } = job.data;
    return extractAndUpdateHashTags(postId);
  });
};

/**
 * buildAssociatedFeed takes a feedObject and currentUserId and builds a fully populated Feed
 * object.  It essentially populates all of the references in the original feed
 * (user, organizations, etc) and returns a fully populated Feed objectThe typical use case here
 *
 * @param {Object} feedObjectToBeAggregated Original Feed ID to build
 * @param {string} currentUserId User for which the Feed needs to be tailored to
 *
 * @return {Promise<any>} Fully populated Feed object
 */
const buildAssociatedFeed = async (feedObjectToBeAggregated, currentUserId) => {
  let feedObject = { ...feedObjectToBeAggregated };

  // todo: investigate
  // @ts-ignore
  const { feed, user } = feedObject;

  const currentUserIdString = String(currentUserId);
  // todo: investigate
  // @ts-ignore
  const hiddenUsers = await blockedUsersService.getBlockedOrBlockedByUsers(currentUserIdString);
  const associatedFeed = await Feed.findOne({
    _id: feed,
    shared: false
  }).lean();

  // If the feed doesn't exist, don't return anything
  if (isEmpty(associatedFeed)) {
    logger.info(`associatedFeed ${feed} not found, might be deleted`);
    // Removing it from featured feed
    await feedRankService.removeObject(feed);
    return {};
  }

  const shouldAuthorBeHidden = hiddenUsers.includes(associatedFeed.user.toHexString());
  const wasPostReportedByUser =
    associatedFeed.reports.filter((report) => report.reportedBy.toHexString() === currentUserIdString).length > 0;

  if (shouldAuthorBeHidden || wasPostReportedByUser) {
    return {};
  }

  // todo: investigate
  // @ts-ignore
  feedObject.associatedFeed = associatedFeed;

  const associatedUser = await getAssociatedUser(associatedFeed.user);

  // If the user for the given feed doesn't exist, don't return anything
  if (isEmpty(associatedUser)) {
    logger.info(`associatedUser ${associatedFeed.user} not found, might be deleted`);
    // Removing it from featured feed
    await feedRankService.removeObject(feed);
    return {};
  }

  associatedFeed.associatedUser = associatedUser;
  // todo: investigate
  // @ts-ignore
  if (feedObject.associatedFeed && feedObject.associatedFeed.giphy) {
    feedObject.associatedGiphy = await GiphyModel.findOne({ _id: feedObject.associatedFeed.giphy }).lean();
  }

  // todo: investigate
  // @ts-ignore
  if (feedObject.timelineableType === "organizations") {
    const associatedTimelineable = await Organization.findOne({ _id: feedObject.timelineable }).lean();
    if (associatedTimelineable) {
      // todo: investigate
      // @ts-ignore
      feedObject.associatedTimelineable = associatedTimelineable;
    }

    const organizationFollower = await OrganizationFollower.findOne({
      // todo: investigate
      // @ts-ignore
      followee: feedObject.timelineable,
      follower: String(currentUserId)
    }).lean();

    if (!isEmpty(organizationFollower)) {
      // todo: investigate
      // @ts-ignore
      feedObject.organizationFollower = organizationFollower;
    }
  }

  const like = await HypeModel.findOne({
    objType: HypeEnum.FEED,
    objId: feedObject.associatedFeed._id,
    user: currentUserId
  }).lean();

  if (!isEmpty(like)) {
    feedObject.like = like;
  }

  // Add the total hypeCount to the feed
  // todo: investigate
  // @ts-ignore
  const hypeCount = await hypeService.getPostHypeCount(feedObject.associatedFeed._id);
  // todo: investigate
  // @ts-ignore
  feedObject.associatedFeed.hypes = hypeCount + feedObject.associatedFeed.likes;

  // Add total # of hypes for currentUser on
  // todo: investigate
  // @ts-ignore
  const userHypeCount = await hypeService.getPostHypeCount(feedObject.associatedFeed._id, currentUserId);
  // todo: investigate
  // @ts-ignore
  feedObject.associatedFeed.userHypes = userHypeCount;

  const friend = await Friend.findOne({
    followee: user,
    follower: currentUserId
  }).lean();
  if (!isEmpty(friend)) {
    feedObject.friend = friend;
  }

  // Fetch associated media
  // todo: investigate
  // @ts-ignore
  const associatedMedia = await mediaService.getAssociatedMedia(currentUserId, feed, "feeds");
  if (associatedMedia.media) {
    feedObject.mediaFiles = associatedMedia.media;
  }
  // todo: investigate
  // @ts-ignore
  feedObject.attachments = associatedMedia.medias;

  // Fetch OG tags for shared links
  const opengraphTags = await openGraphTagsService.getCorrespondingOGTags(associatedFeed.metadata?.sharedLinks);
  if (!isEmpty(opengraphTags)) {
    associatedFeed.opengraphTags = opengraphTags;
  }

  const associatedGame = await feedGameService.getAssociatedGame(associatedFeed._id);

  if (associatedGame) {
    feedObject.associatedGame = associatedGame;
  }

  // Append Associated YoutubeVideo
  const { youtubeVideo } = associatedFeed;

  if (youtubeVideo) {
    feedObject.associatedYoutubeVideo = await getYoutubeVideo(youtubeVideo);
  }

  return feedObject;
};

/**
 * @summary Get the associated user object
 *
 * @param {ObjectId} userId - ID of the user
 *
 * @return {Promise<any>} User
 */
const getAssociatedUser = async (userId) => {
  // todo: investigate
  // @ts-ignore
  return await UserLib.getUserById(userId, {
    populate: ["twitch", "stats"],
    dailyStreak: true,
    projection: WhitelistHelper.socialAccountLinkFields,
    userBadges: true
  });
};

const getYoutubeVideo = async (youtubeVideoId) => {
  const youtubeVideo = await youtubeVideoBaseService.findOne(
    { _id: youtubeVideoId },
    { kind: 0, etag: 0 },
    { lean: true }
  );

  return youtubeVideo;
};

module.exports.aggregateFeedPost = aggregateFeedPost;
module.exports.buildAssociatedFeed = buildAssociatedFeed;
module.exports.extractAndUpdateHashTags = extractAndUpdateHashTags;
module.exports.getAssociatedUser = getAssociatedUser;
module.exports.initProcessors = initProcessors;
module.exports.saveHashtagsAsync = saveHashtagsAsync;
