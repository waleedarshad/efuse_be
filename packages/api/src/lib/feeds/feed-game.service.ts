// TODO: Copied from the OpportunityGameService. Can likely be generacized and shared.

import { Types } from "mongoose";

import { GameBaseService } from "../game/game-base.service";
import { IFeedGame } from "../../backend/interfaces/feed/feed-game";
import { IPopulatedFeedGame } from "../../backend/interfaces/feed/populated-feed-game";
import { Service, Trace } from "../decorators";
import { FeedGameBaseService } from "./feed-game-base.service";
import { IGame } from "../../backend/interfaces";

@Service("feed-game.service")
export class FeedGameService extends FeedGameBaseService {
  private $gameBaseService: GameBaseService;

  constructor() {
    super();

    this.$gameBaseService = new GameBaseService();
  }

  /**
   * @summary Associate game with feed
   *
   * @param {Types.ObjectId | string} feedId
   * @param {Types.ObjectId | string} gameId
   *
   * @returns {Promise<IFeedGame | null>}
   */
  @Trace()
  public async associateGame(
    feedId: Types.ObjectId | string,
    gameId: Types.ObjectId | string
  ): Promise<IFeedGame | null> {
    // Make sure gameId is valid & exists in our DB
    const gameParam = await this.$gameBaseService.validateGame(gameId);

    let feedGame: IFeedGame | null = null;

    // Update existing game or create new one
    if (gameParam) {
      feedGame = await this.update(
        { feed: feedId },
        { feed: feedId, game: gameParam, updatedAt: new Date() },
        { new: true, lean: true, setDefaultsOnInsert: true, upsert: true }
      );
    }

    return feedGame;
  }

  /**
   * @summary Get game associated with the feed
   *
   * @param {Types.ObjectId | string} feedId
   *
   * @returns {Promise<IGame | null>}
   */
  @Trace()
  public async getAssociatedGame(feedId: Types.ObjectId | string): Promise<IGame | null> {
    const feedGame = (<unknown>(
      await this.findOne({ feed: feedId }, {}, { populate: "game" })
    )) as IPopulatedFeedGame | null;

    if (feedGame?.game) {
      return feedGame.game;
    }

    return null;
  }
}
