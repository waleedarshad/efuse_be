import axios, { AxiosInstance, AxiosResponse } from "axios";
import { TWITCH_CLIENT_ID } from "@efuse/key-store";
import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base";
import { TwitchEndpoint } from "../twitch/twitch-endpoint";
import { UserHelperService } from "../users/user-helper.service";
import { ObjectId } from "../../backend/types";
import { OAuthCredentialService } from "../oauth-credential.service";
import {
  ITwitchTokenResponse,
  ITwitchUserQueryParams,
  ITwitchUserResponse,
  ITwitchClipPaginationResponse,
  ITwitchClipBroadcasterQueryParams,
  ITwitchClipGameQueryParams,
  ITwitchClipIdQueryParams,
  ITwitchGameQueryParams,
  ITwitchGameNameQueryParam,
  ITwitchGameIdQueryParam,
  ITwitchGamePaginationResponse,
  ITokenResponse
} from "../../backend/interfaces";
import { RedisCacheService } from "../redis-cache.service";

const USER_TOKEN_KEY = "twitch_oauth_token:user";
const EFUSE_TOKEN_KEY = "twitch_oauth_token:efuse";
const USER_TOKEN_KEY_EXPIRY = 60 * 60; // 1 hour
const EFUSE_TOKEN_KEY_EXPIRY = 24 * 60 * 60; // 24 hours

@Service("twitch-base.service")
export class TwitchBaseService extends BaseService {
  protected $http: AxiosInstance;

  protected $userHelperService: UserHelperService;
  protected $oauthCredentialsService: OAuthCredentialService;

  protected $redisCacheService: RedisCacheService;

  constructor() {
    super();

    this.$http = axios.create({ baseURL: TwitchEndpoint.API_ENDPOINT, headers: { "Client-ID": TWITCH_CLIENT_ID } });

    this.$userHelperService = new UserHelperService();
    this.$oauthCredentialsService = new OAuthCredentialService();

    this.$redisCacheService = new RedisCacheService();
  }

  private setAuthorizationHeader(token: string) {
    // Set the Authorization Header
    this.$http.defaults.headers.common.Authorization = `Bearer ${token}`;
  }

  /**
   * @summary Get eFuse Twitch OAuth Token
   *
   * @returns {Promise<ITokenResponse>}
   */
  @Trace()
  public async eFuseOAuthToken(): Promise<ITokenResponse> {
    // Read token from cache
    const tokenObject = await this.getEFuseTokenFromCache();

    // Return if token is found
    if (tokenObject) {
      return tokenObject;
    }

    // Get the new Token
    const { data } = await axios.post<ITwitchTokenResponse>(TwitchEndpoint.CLIENT_CREDENTIALS_ENDPOINT);

    // Build object
    const newTokenObject = { token: data.access_token, refreshToken: data.refresh_token };

    // Store in redis
    await this.cacheEFuseToken(newTokenObject);

    return newTokenObject;
  }

  /**
   * @summary Get User Twitch OAuth Token
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<ITokenResponse>}
   */
  @Trace()
  public async userOAuthToken(userId: ObjectId): Promise<ITokenResponse> {
    // Read token from cache
    const tokenObject = await this.getUserTokenFromCache(userId);

    // Return if token is found
    if (tokenObject) {
      return tokenObject;
    }

    // Get user's credentials
    const newTokenObject = await this.$userHelperService.getTwitchAccountCredentials(userId);

    // Throw error when token is not found
    if (!newTokenObject) {
      throw Failure.BadRequest("Please connect twitch account");
    }

    // Store in redis
    await this.cacheUserToken(userId, newTokenObject);

    return newTokenObject;
  }

  /**
   * @summary Set OAuth Token
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<string>}
   */
  @Trace()
  public async setOAuthToken(userId: ObjectId | undefined): Promise<string> {
    // If we want to make a public query, then fallback to eFuse token
    if (!userId) {
      this.logger?.info("userId not received | Falling back to eFuse Token");

      const { token, refreshToken } = await this.eFuseOAuthToken();

      this.setAuthorizationHeader(token);

      return refreshToken;
    }

    const { token, refreshToken } = await this.userOAuthToken(userId);

    this.logger?.info("Using user's twitch accessToken");

    this.setAuthorizationHeader(token);

    return refreshToken;
  }

  /**
   * @summary Refresh Twitch Access Token & update OAuthCredentials object
   *
   * @param {ObjectId | undefined} userId
   * @param {string} refreshToken
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async refreshToken(userId: ObjectId | undefined, refreshToken: string): Promise<void> {
    try {
      this.logger?.info("Refreshing user's twitch accessToken");

      // Refresh Token
      const { data }: AxiosResponse<ITwitchTokenResponse> = await this.$http.post(
        `${TwitchEndpoint.REFRESH_TOKEN_ENDPOINT}&refresh_token=${refreshToken}`
      );

      // Build query to find existing credentials
      const query = { owner: String(userId), ownerKind: OwnerKind.USER, service: OAuthServiceKind.TWITCH };

      // Update or create new oauthCredentials Object
      await this.$oauthCredentialsService.updateOne(
        query,
        { accessToken: data.access_token, refreshToken: data.refresh_token, ...query },
        { upsert: true, setDefaultsOnInsert: true, new: true, lean: true }
      );

      if (userId) {
        await this.cacheUserToken(userId, { token: data.access_token, refreshToken: data.refresh_token });
      }

      this.logger?.info("Using user's refreshed twitch accessToken");

      this.setAuthorizationHeader(data.access_token);
    } catch (error) {
      this.logger?.info(error, "Error refreshing user's token | Falling back to eFuse Token");

      // Fallback to eFuse Token
      const { token } = await this.eFuseOAuthToken();

      this.setAuthorizationHeader(token);
    }
  }

  /**
   * @summary Set the Token, Make request to Twitch API & Handles the Refresh Token stuff
   *
   * @param {ObjectId | undefined} userId
   * @param {string} url
   *
   * @returns {Promise<T>}
   */
  @Trace()
  public async request<T>(userId: ObjectId | undefined, url: string): Promise<T> {
    // Set OauthToken
    const refreshToken = await this.setOAuthToken(userId);

    try {
      this.logger?.info({ url }, "Making request to Twitch API");

      // Make Request
      const response: AxiosResponse<T> = await this.$http.get(encodeURI(url));

      // Return data
      return response.data;
    } catch (error) {
      // Refresh Token if its 401
      if (error.response?.status === 401) {
        await this.refreshToken(userId, refreshToken);

        this.logger?.info({ url }, "Retry | Making request to Twitch API");
        // Make request with the refreshed token
        const response: AxiosResponse<T> = await this.$http.get(encodeURI(url));

        // Return data;
        return response.data;
      }

      // If it reaches here then we have some other issue
      throw this.handleError(error);
    }
  }

  /**
   * @summary Implements twitch's Get Users API
   *
   * @param {ObjectId | undefined} userId
   * @param {ITwitchUserQueryParams} queryParams
   *
   * @link https://dev.twitch.tv/docs/api/reference#get-users
   *
   * @returns {Promise<ITwitchUserResponse[]>}
   */
  @Trace()
  public async getUsers(
    userId: ObjectId | undefined,
    queryParams?: ITwitchUserQueryParams
  ): Promise<ITwitchUserResponse[]> {
    let endpoint = "/users";

    // Set the query params
    if (queryParams?.id && queryParams?.login) {
      endpoint += `?${queryParams.id}&${queryParams.login}`;
    } else if (queryParams?.id) {
      endpoint += `?${queryParams.id}`;
    } else if (queryParams?.login) {
      endpoint += `?${queryParams.login}`;
    }

    const { data } = await this.request<{ data: ITwitchUserResponse[] }>(userId, endpoint);

    return data;
  }

  /**
   * @summary Implements the Get Twitch Clips API
   *
   * @param {ObjectId | undefined} userId
   * @param {ITwitchClipBroadcasterQueryParams | ITwitchClipGameQueryParams | ITwitchClipIdQueryParams} queryParams
   *
   * @link https://dev.twitch.tv/docs/api/reference#get-clips
   *
   * @returns {Promise<ITwitchClipPaginationResponse>}
   */
  public async getClips(
    userId: ObjectId | undefined,
    queryParams: ITwitchClipBroadcasterQueryParams | ITwitchClipGameQueryParams | ITwitchClipIdQueryParams
  ): Promise<ITwitchClipPaginationResponse> {
    let endpoint = "/clips";

    // Set the required query params
    if ((queryParams as ITwitchClipBroadcasterQueryParams).broadcaster_id) {
      endpoint += `?${(queryParams as ITwitchClipBroadcasterQueryParams).broadcaster_id}`;
    } else if ((queryParams as ITwitchClipGameQueryParams).game_id) {
      endpoint += `?${(queryParams as ITwitchClipGameQueryParams).game_id}`;
    } else if ((queryParams as ITwitchClipIdQueryParams).id) {
      endpoint += `?${(queryParams as ITwitchClipIdQueryParams).id}`;
    } else {
      throw Failure.BadRequest("Required params are not provided");
    }

    // Set after query param
    if (queryParams.after) {
      endpoint += `&after=${queryParams.after}`;
    }

    // Set before query param
    if (queryParams.before) {
      endpoint += `&before=${queryParams.before}`;
    }

    // Set ended_at query param
    if (queryParams.ended_at) {
      endpoint += `&ended_at=${queryParams.ended_at}`;
    }

    // Set first query param
    if (queryParams.first) {
      endpoint += `&first=${queryParams.first}`;
    }

    // Set started_at query param
    if (queryParams.started_at) {
      endpoint += `&started_at=${queryParams.started_at}`;
    }

    // Get Clips
    const response = await this.request<ITwitchClipPaginationResponse>(userId, endpoint);

    return response;
  }

  /**
   * @summary Implements the Get Game Twitch API
   *
   * @param {ObjectId | undefined} userId
   * @param {ITwitchGameQueryParams | ITwitchGameNameQueryParam | ITwitchGameIdQueryParam} queryParams
   *
   * @returns {Promise<ITwitchGamePaginationResponse>}
   */
  public async getGames(
    userId: ObjectId | undefined,
    queryParams: ITwitchGameQueryParams | ITwitchGameNameQueryParam | ITwitchGameIdQueryParam
  ): Promise<ITwitchGamePaginationResponse> {
    let endpoint = "/games";

    // Set the query params
    if ((queryParams as ITwitchGameQueryParams).name && (queryParams as ITwitchGameQueryParams).id) {
      endpoint += `?${(queryParams as ITwitchGameQueryParams).name}&${(queryParams as ITwitchGameQueryParams).id}`;
    } else if ((queryParams as ITwitchGameNameQueryParam).name) {
      endpoint += `?${(queryParams as ITwitchGameNameQueryParam).name}`;
    } else if ((queryParams as ITwitchGameIdQueryParam).id) {
      endpoint += `?${(queryParams as ITwitchGameIdQueryParam).id}`;
    }

    // Get Games
    const response = await this.request<ITwitchGamePaginationResponse>(userId, endpoint);

    return response;
  }

  /**
   * @summary Get eFuse token from redis
   *
   * @returns {Promise<ITokenResponse | null>}
   */
  @Trace()
  public async getEFuseTokenFromCache(): Promise<ITokenResponse | null> {
    try {
      this.logger?.info("Reading eFuse twitch token from redis");

      const tokenObject = await this.$redisCacheService.readFromCache(EFUSE_TOKEN_KEY);

      if (!tokenObject) {
        this.logger?.info("eFuse twitch token not found in redis");

        return null;
      }

      this.logger?.info("Found eFuse twitch token in redis");
      return <ITokenResponse>JSON.parse(tokenObject);
    } catch (error) {
      this.logger?.error(error, "Error reading eFuse token from cache");

      return null;
    }
  }

  @Trace()
  public async getUserTokenFromCache(userId: ObjectId): Promise<ITokenResponse | null> {
    try {
      this.logger?.info("Reading user's twitch token from redis");

      const tokenObject = await this.$redisCacheService.readFromCache(this.buildUserCacheKey(userId));

      if (!tokenObject) {
        this.logger?.info("User's twitch token not found in redis");

        return null;
      }

      this.logger?.info("Found user's twitch token in redis");
      return <ITokenResponse>JSON.parse(tokenObject);
    } catch (error) {
      this.logger?.error(error, "Error reading user token from redis");

      return null;
    }
  }

  /**
   * @summary Build user cache key
   *
   * @param {ObjectId} userId
   *
   * @returns {string}
   */
  private buildUserCacheKey(userId: ObjectId): string {
    return `${USER_TOKEN_KEY}:${userId}`;
  }

  /**
   * @summary Cache user token in redis
   *
   * @param {ObjectId} userId
   * @param {ITokenResponse} tokenObject
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async cacheUserToken(userId: ObjectId, tokenObject: ITokenResponse): Promise<void> {
    this.logger?.info("Added User's twitch token to redis");

    await this.$redisCacheService.writeToCache(
      this.buildUserCacheKey(userId),
      JSON.stringify(tokenObject),
      USER_TOKEN_KEY_EXPIRY
    );
  }

  /**
   * @summary Cache eFuse token in redis
   *
   * @param {ITokenResponse} tokenObject
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async cacheEFuseToken(tokenObject: ITokenResponse): Promise<void> {
    this.logger?.info("Added eFuse twitch token to redis");

    await this.$redisCacheService.writeToCache(EFUSE_TOKEN_KEY, JSON.stringify(tokenObject), EFUSE_TOKEN_KEY_EXPIRY);
  }
}
