import { TWITCH_CLIENT_ID, TWITCH_SECRET } from "@efuse/key-store";

const CREDENTIALS = `client_id=${TWITCH_CLIENT_ID}&client_secret=${TWITCH_SECRET}`;

export class TwitchEndpoint {
  public static API_ENDPOINT = "https://api.twitch.tv/helix";
  public static OAUTH_TOKEN_ENDPOINT = "https://id.twitch.tv/oauth2/token";
  public static CLIENT_CREDENTIALS_ENDPOINT = `${TwitchEndpoint.OAUTH_TOKEN_ENDPOINT}?grant_type=client_credentials&${CREDENTIALS}`;
  public static REFRESH_TOKEN_ENDPOINT = `${TwitchEndpoint.OAUTH_TOKEN_ENDPOINT}?grant_type=refresh_token&${CREDENTIALS}`;
}
