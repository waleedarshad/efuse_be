import { unitOfTime } from "moment";
import axios, { AxiosInstance } from "axios";
import { TWITCH_CLIENT_ID } from "@efuse/key-store";

import { IPopulatedUserTrackCron } from "../../backend/interfaces";
import { Service, Trace } from "../decorators";
import { CronEntityKind, CronKindEnum } from "../track-cron/track-cron.enum";
import { TrackCronService } from "../track-cron/track-cron.service";
import { TwitchCronsEnum } from "./twitch.enum";
import { IUser } from "../../backend/interfaces/user";
import { TwitchEndpoint } from "./twitch-endpoint";

const POPULATED_FIELDS = "_id twitchID deleted status";
const LIMIT = 15;
const INTERVAL = 24;
const UNIT: unitOfTime.DurationConstructor = "h";

@Service("twitch-async.service")
export class TwitchAsyncService extends TrackCronService<IPopulatedUserTrackCron> {
  protected $http: AxiosInstance;

  constructor() {
    super(
      CronKindEnum.TWITCH,
      Object.values(TwitchCronsEnum),
      CronEntityKind.USER,
      POPULATED_FIELDS,
      LIMIT,
      INTERVAL,
      UNIT
    );

    this.$http = axios.create({ baseURL: TwitchEndpoint.API_ENDPOINT, headers: { "Client-ID": TWITCH_CLIENT_ID } });
  }

  /**
   * @summary Get Oauth token
   */
  @Trace()
  protected async setOauthToken(): Promise<void> {
    try {
      const response = await axios.post(TwitchEndpoint.CLIENT_CREDENTIALS_ENDPOINT);

      this.$http.defaults.headers.common.Authorization = `Bearer ${response.data.access_token}`;
    } catch (error) {
      const errorObject = error.response?.data || error;
      this.logger?.error(errorObject, "Error retrieving Twitch Oauth Token");
      throw errorObject;
    }
  }

  /**
   * @summary Twitch fields to be fetched from user object
   */
  protected get twitchFields(): Record<string, number> {
    return {
      _id: 1,
      username: 1,
      email: 1,
      twitchVerified: 1,
      twitchID: 1,
      twitchUserName: 1,
      twitchDisplayName: 1,
      twitchProfileImage: 1,
      twitchAccessToken: 1,
      twitchRefreshToken: 1,
      twitchAccessTokenExpiresAt: 1,
      twitchScope: 1,
      twitchTokenType: 1,
      twitch: 1,
      twitchLink: 1,
      twitchFollowersCount: 1
    };
  }

  /**
   * Async Processors
   */
  @Trace({ service: "twitch-async.service", name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const twitchAsyncService = new TwitchAsyncService();

    // Track Cron async processors
    twitchAsyncService.asyncProcessors();
  }

  /**
   * @summary Set Cron to run on the basis of TrackCron service
   *
   * @param {string} methodName
   * @param {(user: IUser) => void} cronProcessor
   *
   * @returns {Promise<void>}
   */
  @Trace()
  protected async executeCron(methodName: string, cronProcessor: (user: IUser) => void): Promise<void> {
    try {
      const cronUsers: IPopulatedUserTrackCron[] = await this.getEntities(methodName);

      if (cronUsers.length === 0) {
        this.logger?.info(`NO user found in trackCron for ${methodName}`);
        return;
      }

      for (const cronUser of cronUsers) {
        const user = cronUser.entity;

        // Make sure user is valid
        const isValid = this.validateUserEntity(methodName, user);

        if (!isValid) {
          this.logger?.info({ cronUser }, "Cron can't be processed");

          // Skip iteration
          continue;
        }

        // Fire cron processor
        cronProcessor(user);

        // Update status
        this.markCronAsProcessing(methodName, user._id);
      }
    } catch (error) {
      this.logger?.error(error, `Error running cron ${methodName}`);
    }
  }
}
