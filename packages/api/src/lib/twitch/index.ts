import axios from "axios";
import { Logger } from "@efuse/logger";
const logger = Logger.create();

import { BACKEND_DOMAIN, TWITCH_CLIENT_ID, TWITCH_SECRET } from "@efuse/key-store";
import { Twitch } from "../../backend/models/Twitch";
import { User } from "../../backend/models/User";
import { userWentOnline, userWentOffline } from "../hooks/twitch";
import { EFuseAnalyticsUtil } from "../analytics.util";

import { AutomatedFeedService } from "../automated-feed/automated-feed.service";

import { twitchFields } from "../../backend/helpers/whitelist_helper";
import { ObjectId } from "mongoose";

/**
 * @summary Subscribe to stream changes
 *
 * @param {String} BACKEND_DOMAIN - backend domain name
 * @param {ObjectId} userId - Id of user
 * @param {String} twitchId - Twitch id of user
 * @param {String} twitchAccessToken - Twitch access token
 * @return {Promise<any>}
 */
export const subscribeToStreamChanges = async (
  BACKEND_DOMAIN: string,
  userId: ObjectId,
  twitchId: string,
  twitchAccessToken: string,
  mode = "subscribe"
): Promise<any> => {
  try {
    EFuseAnalyticsUtil.incrementMetric(`twitch.${mode}ToTwitchStreamChanges.calls`);
    const payload = {
      "hub.callback": `${BACKEND_DOMAIN}/api/public/webhooks/twitch_webhook?userId=${userId}`,
      "hub.mode": mode,
      "hub.topic": `https://api.twitch.tv/helix/streams?user_id=${twitchId}`,
      "hub.lease_seconds": 864000
    };
    logger.info(payload, `Sending payload to twitch webhook ${mode}`);

    // todo: investigate
    // @ts-ignore
    const response = await axios.post("https://api.twitch.tv/helix/webhooks/hub", payload, {
      headers: {
        Authorization: `Bearer ${twitchAccessToken}`,
        "Client-ID": TWITCH_CLIENT_ID
      }
    });
    EFuseAnalyticsUtil.incrementMetric(`twitch.${mode}ToTwitchStreamChanges.success`);
    logger.info(`Successfully ${mode} to twitch webhook`);
    return { payload, response };
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric(`twitch.${mode}ToTwitchStreamChanges.failed`);
    if (error.response && error.response.status === 401) {
      logger.info(`User ${userId} Twitch access token expired`);
      try {
        const refreshedAccessToken = await refreshTwitchAccessToken(userId);
        return await subscribeToStreamChanges(BACKEND_DOMAIN, userId, twitchId, refreshedAccessToken, mode);
      } catch (error) {
        logger.error(error, `Error while refreshing token to ${mode} webhook`);
        throw error;
      }
    }
    error.user = userId;
    logger.error(error, `Failed to ${mode} to user twitch webhooks`);
  }
};

/**
 * @summary Verify subscription of webhook
 *
 * @param {String} BACKEND_DOMAIN - backend domain name
 * @param {ObjectId} userId - Id of user
 * @param {String} challenge
 * @return {Promise<string | undefined>}
 */
export const verifyWebhookSubscription = async (
  BACKEND_DOMAIN: string,
  userId: ObjectId,
  challenge: string
): Promise<string | undefined> => {
  try {
    EFuseAnalyticsUtil.incrementMetric("twitch.verifyTwitchWebhookSubscription.calls");
    const user = await User.findById(userId).select(twitchFields).populate("twitch");

    // todo: investigate
    // @ts-ignore
    if (!user) throw new Error("User not found", { user: userId });

    let { twitch } = user;
    if (!twitch) {
      twitch = new Twitch({ webhookSubscription: {} });

      // todo: investigate
      // @ts-ignore
      await twitch.save();

      // todo: investigate
      // @ts-ignore
      user.twitch = twitch._id;
      await user.save({ validateBeforeSave: false });
    }

    // todo: investigate
    // @ts-ignore
    if (!twitch.webhookSubscription) twitch.webhookSubscription = {};

    // todo: investigate
    // @ts-ignore
    twitch.webhookSubscription.isActive = true;

    // todo: investigate
    // @ts-ignore
    twitch.webhookSubscription.callback = `${BACKEND_DOMAIN}/api/public/webhooks/twitch_webhook?userId=${userId}`;

    // todo: investigate
    // @ts-ignore
    twitch.webhookSubscription.expiresAt = new Date(Date.now() + 864000 * 1000);

    // todo: investigate
    // @ts-ignore
    await twitch.save({ validateBeforeSave: false });
    logger.info(`Just validated twitch webhook for user ${user.email || user._id}`);
    EFuseAnalyticsUtil.incrementMetric("twitch.verifyTwitchWebhookSubscription.success");
    return challenge;
  } catch (error) {
    logger.error(`[${userId}] Error verifying twitch webhook`);
    logger.error(error);
    EFuseAnalyticsUtil.incrementMetric("twitch.verifyTwitchWebhookSubscription.failure");
    return;
  }
};

/**
 * @summary Handle chabged webhook stream
 *
 * @param {ObjectId} userId - Id of user
 * @param {Array} webhookData - Webhook data
 * @returns {Promise<any>}
 */
export const handleStreamChangedWebhook = async (userId: ObjectId, webhookData: Array<any>): Promise<any> => {
  logger.info({ userId, webhookData }, "handleStreamChangedWebhook");
  if (!webhookData || (Array.isArray(webhookData) && webhookData.length === 0)) return await setStreamOffline(userId);
  return await updateStreamOnlineData(userId, webhookData[0]);
};

/**
 * @summary Turn stream off
 *
 * @param {ObjectId} userId - Id of user
 * @return {Promise<any>}
 */
export const setStreamOffline = async (userId: ObjectId): Promise<any> => {
  EFuseAnalyticsUtil.incrementMetric("twitch.setStreamOffline.calls");
  try {
    const user = await User.findById(userId).select(twitchFields).populate("_id twitch");
    if (!user) {
      logger.error(`[${userId}] Cannot find user to set twitch subscription offline.`);
      return;
    }

    // Make sure user has twitch account linked & has not unlinked it.
    if (!user.twitch) {
      logger.error(
        `[${userId}] Cannot find user's twitch account to set stream offline. User may have unlinked twitch account`
      );
      return;
    }

    // todo: investigate
    // @ts-ignore
    user.twitch.stream.isLive = false;
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.title = null;
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.game = null;
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.endedAt = new Date();
    // todo: investigate
    // @ts-ignore
    await user.twitch.save({ validateBeforeSave: false });

    logger.info("Set twitch stream to offline", {
      user: user.email || user._id,
      twitch: user.twitch
    });

    userWentOffline(user, "twitch");

    EFuseAnalyticsUtil.incrementMetric("twitch.setStreamOffline.success");

    return user;
  } catch (error) {
    logger.error(`[${userId}] Failure to set twitch stream offline`, error);
    EFuseAnalyticsUtil.incrementMetric("twitch.setStreamOffline.failure");
  }
};

/**
 * @summary Update stream online data
 *
 * @param {ObjectId} userId - Id of user
 * @param {any} data - Data to change
 * @return {Promise<any>}
 */
export const updateStreamOnlineData = async (userId: ObjectId, data: any): Promise<any> => {
  logger.info({ userId, data }, "updateStreamOnlineData");
  try {
    EFuseAnalyticsUtil.incrementMetric("twitch.updateStreamOnline.calls");
    const user = await User.findById(userId).select(twitchFields).populate("twitch");
    if (!user) {
      logger.error(`[${userId}] Cannot find user to update twitch subscription`);
      return;
    }

    // Make sure user has twitch account linked & has not unlinked it.
    if (!user.twitch) {
      logger.error(
        `[${userId}] Cannot find user's twitch account to update twitch subscription. User may have unlinked twitch account`
      );
      return;
    }

    // todo: investigate
    // @ts-ignore
    const justWentLive = user.twitch.stream.isLive ? false : true;
    logger.info({ data, justWentLive }, "Updating user Twitch stream data");

    // todo: investigate
    // @ts-ignore
    user.twitch.stream.isLive = true;
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.title = data.title;
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.viewerCount = data.viewer_count;
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.startedAt = new Date();
    // todo: investigate
    // @ts-ignore
    user.twitch.stream.endedAt = null;
    // todo: investigate
    // @ts-ignore
    await user.twitch.save({ validateBeforeSave: false });
    logger.info(
      {
        twitch: user.twitch
      },
      "Set twitch stream to online"
    );

    EFuseAnalyticsUtil.incrementMetric("twitch.updateStreamOnline.success");
    userWentOnline(user, "twitch", data.title);

    // todo: investigate
    // @ts-ignore
    if (justWentLive && user.twitch.sendStartStreamMessage) {
      logger.info(
        {
          user: user.email || user._id,
          stream: user.twitchDisplayName
        },
        "Stream went live and should post automated message"
      );

      const automatedFeedService = new AutomatedFeedService();

      // todo: investigate
      // @ts-ignore
      await automatedFeedService.processAutomatedFeed(userId);
    }

    return user;
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("twitch.updateStreamOnline.failure");
    logger.error({ error }, "Error in updateStreamOnline");
    throw error;
  }
};

/**
 * @summary Toggle twitch automated message
 *
 * @param {ObjectId} userId - Id of user
 * @param {String} automatedType - Automation type i.e startStream
 */
export const toggleTwitchAutomatedMessageOn = async (userId: ObjectId, automatedType: string) => {
  try {
    logger.info({ userId, automatedType, twitchFields }, "toggleTwitchAutomatedMessageOn");
    const user = await User.findById(userId).select(twitchFields).populate("twitch");

    // todo: investigate
    // @ts-ignore
    if (!user.twitch) {
      throw new Error(`User ${userId} does not have a linked Twitch`);
    }

    // todo: investigate
    // @ts-ignore
    if (automatedType === "startStream") user.twitch.sendStartStreamMessage = true;

    // todo: investigate
    // @ts-ignore
    await user.twitch.save({ validateBeforeSave: false });

    // todo: investigate
    // @ts-ignore
    await user.save({ validateBeforeSave: false });
  } catch (error) {
    logger.error({ error }, "error in toggleTwitchAutomatedMessageOn");
    throw error;
  }
};

/**
 * @summary Refresh twitch access token
 *
 * @param {ObjectId} userId - Id of user
 * @return {Promise<string>}
 */
export const refreshTwitchAccessToken = async (userId: ObjectId): Promise<string> => {
  let user;
  try {
    user = await User.findById(userId).select(twitchFields);

    // todo: investigate
    // @ts-ignore
    logger.info(`Refreshing Twitch access token for user ${user.email || user._id}`);

    // todo: investigate
    // @ts-ignore
    if (!user.twitchRefreshToken) {
      // todo: investigate
      // @ts-ignore
      logger.error(`User ${user.email || user._id} does not have a Twitch refresh token`);
      // TODO: unlink twitch account here

      // todo: investigate
      // @ts-ignore
      throw new Error(`User ${user.email || user._id} does not have a Twitch refresh token`);
    }

    // todo: investigate
    // @ts-ignore
    const { data } = await axios.post(
      // todo: investigate
      // @ts-ignore
      `https://id.twitch.tv/oauth2/token?grant_type=refresh_token&refresh_token=${user.twitchRefreshToken}&client_id=${TWITCH_CLIENT_ID}&client_secret=${TWITCH_SECRET}`
    );

    const { access_token, refresh_token } = data;

    // todo: investigate
    // @ts-ignore
    user.twitchAccessToken = access_token;

    // todo: investigate
    // @ts-ignore
    user.twitchRefreshToken = refresh_token;

    // todo: investigate
    // @ts-ignore
    await user.save({ validateBeforeSave: false });

    // todo: investigate
    // @ts-ignore
    return user.twitchAccessToken;
  } catch (error) {
    // todo: investigate
    // @ts-ignore
    error.user = user.email || user._id || userId;
    logger.error("Failed to refresh user's Twitch access token", { error });
    throw error;
  }
};

/**
 * @summary Re-subscribe twitch webhook token that is expiring
 */
export const resubExpiringTwitchWebhooks = async () => {
  try {
    const expiringSubs = Twitch.find({
      "webhookSubscription.isActive": true,
      "webhookSubscription.expiresAt": {
        $lte: new Date(Date.now() + 24 * 60 * 60 * 1000)
      }
    });

    // todo: investigate
    // @ts-ignore
    for (const sub of expiringSubs) {
      EFuseAnalyticsUtil.incrementMetric("twitch.resubscribeTwitchWebhookSubscription.calls");
      const user = await User.findOne({ twitch: sub._id }).select(twitchFields);

      // todo: investigate
      // @ts-ignore
      await subscribeToStreamChanges(BACKEND_DOMAIN, user._id, user.twitchId, user.twitchAccessToken);
      EFuseAnalyticsUtil.incrementMetric("twitch.resubscribeTwitchWebhookSubscription.success");
    }
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("twitch.resubscribeTwitchWebhookSubscription.failure");
    logger.warn({ error }, "Something went wrong while resubscribing to Twitch webhooks");
  }
};
