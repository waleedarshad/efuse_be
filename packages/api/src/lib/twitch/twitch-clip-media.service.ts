import { TwitchClipModel } from "../../backend/models/twitch-clip.model";
import { Media } from "../../backend/models/media";

import { uploadFileFromUrl } from "../uploadFileFromUrl";
import { IMedia } from "../../backend/interfaces/media";
import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { Types } from "mongoose";

@Service("twitch-clip-media.service")
export class TwitchClipMediaService extends BaseService {
  constructor() {
    super();
  }
  /**
   * @summary Upload a twitch clip to s3 bucket & create corresponding media object for feed
   *
   * @param {Types.ObjectId} feedId - Targeted feedID
   * @param {Types.ObjectId | string} twitchClipId - ID of twitchClip object which is required to be uploaded to s3
   */
  @Trace()
  public async uploadTwitchClip(feedId: Types.ObjectId, twitchClipId: Types.ObjectId | string) {
    try {
      const twitchClip = await TwitchClipModel.findOne({ _id: twitchClipId }).select(
        "_id clip_id thumbnail_url user hosted hostedFile hostedAt hostingResult hostingError"
      );
      if (!twitchClip) {
        this.logger?.info({ feedId, twitchClipId }, "Twitch clip not found");
        return;
      }
      // If clip is already uploaded to S3 Just create the new Media Object for feed
      if (twitchClip.hosted && twitchClip.hostedFile) {
        const { user, hostedFile } = twitchClip;
        const { filename, url } = hostedFile;

        // are 'user', 'filename', 'url' always here? telling typescript it will be for now
        return await this.storeMedia(feedId, user!, filename!, url!);
      }
      // Upload twitch clip to s3 from url
      const clipUrl = twitchClip.thumbnail_url.split("-preview")[0] + ".mp4";
      const filename = `${twitchClip.clip_id}.mp4`;

      twitchClip.hostedAt = new Date();
      try {
        const s3Response = await uploadFileFromUrl(clipUrl, filename);
        const uploadedVideoUrl = s3Response.Location;

        // is 'twitchClip.user' always here? telling typescript it will be for now
        const media: IMedia = await this.storeMedia(feedId, twitchClip.user!, filename, uploadedVideoUrl);
        twitchClip.hosted = true;
        twitchClip.hostingResult = "SUCCESS";

        const hostedFile = {
          url: media.file.url!,
          contentType: "video/mp4",
          filename,
          originalUrl: uploadedVideoUrl
        };

        twitchClip.hostedFile = hostedFile;
      } catch (error) {
        const errMsg = error.message;
        twitchClip.hostingResult = "ERROR";
        twitchClip.hostingError = errMsg;
      }
      return await twitchClip.save();
    } catch (error) {
      this.logger?.error({ error: error.message }, "Error while storing twitch clip");
      return;
    }
  }

  /**
   * @summary Store the Media object for twitch clip post
   *
   * @param {Types.ObjectId | String} feedId
   * @param {ObjectId} userId
   * @param {String} filename
   * @param {String} url
   *
   * @return {Promise<IMedia>} Media
   */
  private storeMedia(
    feedId: Types.ObjectId | string,
    userId: Types.ObjectId,
    filename: string,
    url: string
  ): Promise<IMedia> {
    try {
      return new Media({
        user: userId,
        mediaable: feedId,
        mediaableType: "feeds",
        file: {
          url,
          filename,
          contentType: "video/mp4"
        }
      }).save();
    } catch (error) {
      this.logger?.error({ error: error.message }, "Error while storing media object");
      throw new Error(error);
    }
  }
}
