import { Types } from "mongoose";
import { AxiosResponse } from "axios";
import { Queue } from "bull";
import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";

import { ITwitchAccountInfo } from "../../backend/interfaces";
import { IUser } from "../../backend/interfaces/user";
import { Service, Trace } from "../decorators";
import { TwitchAccountInfoService } from "../twitch-account-info.service";
import { TwitchAsyncService } from "./twitch-async.service";
import { TwitchCronsEnum } from "./twitch.enum";
import { UserService } from "../users/user.service";
import Config from "../../async/config";

interface ITwitchFollowerResponse {
  total: number;
}

@Service("twitch-follower.async.service")
export class TwitchFollowerAsyncService extends TwitchAsyncService {
  private $syncTwitchFollowersQueue: Queue;

  private $twitchAccountInfoService: TwitchAccountInfoService;
  private $userService: UserService;

  constructor() {
    super();

    this.$syncTwitchFollowersQueue = Config.initQueue("syncTwitchFollowers", { limiter: { max: 5, duration: 1000 } });

    this.$twitchAccountInfoService = new TwitchAccountInfoService();
    this.$userService = new UserService();
  }

  /**
   * @summary Sync Twitch followers count
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   *
   * @returns {Promise<number>}
   */
  @Trace()
  public async syncTwitchFollowers(owner: Types.ObjectId | string, ownerKind: string): Promise<number> {
    try {
      const twitchAccountInfo: ITwitchAccountInfo | null = await this.$twitchAccountInfoService.findOne({
        owner: owner as string
      });

      let user: IUser | null | undefined;
      let twitchId: string | undefined;

      // use accountId from account info if it's there
      if (twitchAccountInfo?.accountId) {
        twitchId = twitchAccountInfo.accountId;
      }

      // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
      // get the twitch account id from the user instead
      if (!twitchId && ownerKind === OwnerKind.USER) {
        user = await this.$userService.findOne({ _id: owner }, this.twitchFields);

        if (!user) {
          throw Failure.UnprocessableEntity("User not found");
        }

        twitchId = user.twitchID;
      }

      if (!twitchId) {
        throw Failure.BadRequest("twitchID not found");
      }

      // Set Token
      await this.setOauthToken();

      const response: AxiosResponse<ITwitchFollowerResponse> = await this.$http.get(
        `/users/follows?first=1&to_id=${twitchId}`
      );
      const { total } = response.data;

      this.logger?.info({ owner, ownerKind, total }, "Total followers found!");

      // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
      if (ownerKind === OwnerKind.USER && user) {
        user.twitchFollowersCount = total;
        await user.save({ validateBeforeSave: false });
      }

      const updatedAccountInfo: any = { owner, ownerKind, followers: total };

      await (twitchAccountInfo
        ? this.$twitchAccountInfoService.updateOne({ owner: owner as string }, updatedAccountInfo)
        : this.$twitchAccountInfoService.create(updatedAccountInfo));

      // Update cron
      this.markCronAsSuccess(TwitchCronsEnum.TWITCH_FOLLOWERS, owner);

      return total;
    } catch (error) {
      // Update CRON status

      this.markCronAsFailure(TwitchCronsEnum.TWITCH_FOLLOWERS, owner, error.message);

      throw this.handleError(error, "Error while syncing twitch followers");
    }
  }

  /**
   * @summary Sync Twitch followers count using bull queue
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   *
   * @returns {Promise<number>}
   */
  @Trace()
  public syncTwitchFollowersAsync(owner: Types.ObjectId | string, ownerKind: string): void {
    this.$syncTwitchFollowersQueue
      .add({ owner, ownerKind })
      .catch((error) => this.logger?.error(error, "Error adding to queue syncTwitchClipsQueue"));
  }

  /**
   * @summary Cron job to switch followers count
   *
   * @returns {Promise<void>}
   */
  @Trace({ service: "twitch-follower-async.service", name: TwitchCronsEnum.TWITCH_FOLLOWERS })
  public static async SyncTwitchFollowersCron(): Promise<void> {
    const twitchFollowerAsyncService = new TwitchFollowerAsyncService();

    const cronProcessor = (user: IUser): void => {
      twitchFollowerAsyncService.syncTwitchFollowersAsync(user._id, OwnerKind.USER);
    };

    await twitchFollowerAsyncService.executeCron(TwitchCronsEnum.TWITCH_FOLLOWERS, cronProcessor);
  }

  /**
   * Async Processors
   */
  @Trace({ service: "twitch-follower-async.service", name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const twitchFollowerAsyncService = new TwitchFollowerAsyncService();

    // syncTwitchFollowersQueue
    twitchFollowerAsyncService.$syncTwitchFollowersQueue
      .process((job: { data: { owner: Types.ObjectId | string; ownerKind: string } }) => {
        const { owner, ownerKind } = job.data;
        return twitchFollowerAsyncService.syncTwitchFollowers(owner, ownerKind);
      })
      .catch((error) =>
        twitchFollowerAsyncService.logger?.error(error, "Error while processing syncTwitchFollowersQueue")
      );
  }
}
