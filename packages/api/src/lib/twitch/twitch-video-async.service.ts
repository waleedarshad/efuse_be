import { Types } from "mongoose";
import { Queue } from "bull";
import { AxiosResponse } from "axios";
import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";

import { ITwitchVideo } from "../../backend/interfaces";
import { TwitchVideo, TwitchVideoModel } from "../../backend/models/twitch-video";
import { Service, Trace } from "../decorators";
import { TwitchAsyncService } from "./twitch-async.service";
import Config from "../../async/config";
import { UserService } from "../users/user.service";
import { IUser } from "../../backend/interfaces/user";
import { TwitchCronsEnum } from "./twitch.enum";

const SERVICE = "twitch-video-async.service";

interface ITwitchVideoResponse {
  pagination?: { cursor: string };
  data: ITwitchVideo[];
}

@Service(SERVICE)
export class TwitchVideoAsyncService extends TwitchAsyncService {
  private readonly $twitchVideos: TwitchVideoModel<ITwitchVideo> = TwitchVideo;

  private $storeTwitchVideosQueue: Queue;
  private $syncTwitchVideosQueue: Queue;

  private $userService: UserService;

  constructor() {
    super();

    this.$storeTwitchVideosQueue = Config.initQueue("storeTwitchVideos");

    this.$syncTwitchVideosQueue = Config.initQueue("syncTwitchVideos", { limiter: { max: 5, duration: 1000 } });

    this.$userService = new UserService();
  }

  /**
   * @summary Sync Twitch Videos
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncTwitchVideos(owner: Types.ObjectId | string, ownerKind: string): Promise<void> {
    try {
      await this.setOauthToken();

      const user: IUser | null = await this.$userService.findOne({ _id: owner }, this.twitchFields, { lean: true });

      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      if (!user.twitchID) {
        this.logger?.info({ user }, "twitchID not found");
        throw Failure.UnprocessableEntity("twitchID not found");
      }

      await this.fetchTwitchVideos(user.twitchID, user._id);

      // Update Cron
      this.markCronAsSuccess(TwitchCronsEnum.TWITCH_VIDEOS, user._id);
    } catch (error) {
      this.logger?.error(error, "Error syncing twitch videos");

      // Update cron

      this.markCronAsFailure(TwitchCronsEnum.TWITCH_VIDEOS, owner, error.message);
    }
  }

  /**
   * @summary Sync Twitch Videos using bull queue
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncTwitchVideosAsync(owner: Types.ObjectId | string, ownerKind: string): void {
    this.$syncTwitchVideosQueue
      .add({ owner, ownerKind })
      .catch((error) => this.logger?.error(error, "Error adding videos to queue syncTwitchVideosQueue"));
  }

  /**
   * @summary Cron job to sync twitch videos
   *
   * @returns {Promise<void>}
   */
  @Trace({ service: SERVICE, name: TwitchCronsEnum.TWITCH_VIDEOS })
  public static async SyncTwitchVideosCron(): Promise<void> {
    const twitchVideosAsyncService = new TwitchVideoAsyncService();

    const cronProcessor = (user: IUser): void => {
      twitchVideosAsyncService.syncTwitchVideosAsync(user._id, OwnerKind.USER);
    };

    await twitchVideosAsyncService.executeCron(TwitchCronsEnum.TWITCH_VIDEOS, cronProcessor);
  }

  /**
   * @summary Fetch twitch videos for a particular user
   *
   * @param {String} twitchID - Id of user twitch account
   * @param {Types.ObjectId | string} userId
   * @param {string} cursor
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async fetchTwitchVideos(twitchID: string, userId: Types.ObjectId | string, cursor?: string): Promise<void> {
    let queryParams = `first=100&user_id=${twitchID}`;

    if (cursor) {
      queryParams = `${queryParams}&after=${cursor}`;
    }

    // Fetch Videos
    const response: AxiosResponse<ITwitchVideoResponse> = await this.$http.get(`/videos?${queryParams}`);

    const { data, pagination } = response.data;

    this.storeVideosAsync(userId, data);

    if (pagination && pagination.cursor) {
      await this.fetchTwitchVideos(twitchID, userId, pagination.cursor);
    }
  }

  /**
   * @summary Store videos in DB using bulk operations
   *
   * @param {Types.ObjectId | string} userId
   * @param {ITwitchVideo[]} videos
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async storeVideos(userId: Types.ObjectId | string, videos: ITwitchVideo[]): Promise<void> {
    try {
      if (!videos || videos.length === 0) {
        this.logger?.info({ userId }, "No videos found for user");
        return;
      }

      const bulkOperations: any[] = videos.map((video: ITwitchVideo) => {
        const data = {
          ...video,
          video_id: video.id,
          user: userId,
          jobRanAt: Date.now()
        };

        const { id, ...rest } = data;

        return {
          updateOne: {
            filter: {
              user: userId,
              video_id: video.id
            },
            update: rest,
            upsert: true,
            setDefaultsOnInsert: true
          }
        };
      });

      const response = await this.$twitchVideos.bulkWrite(bulkOperations);

      this.logger?.info(
        { upserted: response.upsertedCount, updated: response.modifiedCount, userId },
        "Twitch videos synced for user"
      );
    } catch (error: unknown) {
      this.logger?.error({ error, userId }, "Error storing twitch videos for user");
    }
  }

  /**
   * @summary Store videos in DB using bulk operations via bull queue
   *
   * @param {Types.ObjectId | string} userId
   * @param {ITwitchVideo[]} videos
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private storeVideosAsync(userId: Types.ObjectId | string, videos: ITwitchVideo[]): void {
    this.$storeTwitchVideosQueue
      .add({ userId, videos })
      .catch((error) => this.logger?.error(error, "Error adding videos to queue storeTwitchVideosQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const twitchVideoAsyncService = new TwitchVideoAsyncService();

    // storeTwitchVideosQueue
    twitchVideoAsyncService.$storeTwitchVideosQueue
      .process((job: { data: { userId: Types.ObjectId | string; videos: ITwitchVideo[] } }) => {
        const { userId, videos } = job.data;
        return twitchVideoAsyncService.storeVideos(userId, videos);
      })
      .catch((error) => twitchVideoAsyncService.logger?.error(error, "Error while processing storeTwitchVideosQueue"));

    // syncTwitchVideosQueue
    twitchVideoAsyncService.$syncTwitchVideosQueue
      .process((job: { data: { owner: Types.ObjectId | string; ownerKind: string } }) => {
        const { owner, ownerKind } = job.data;
        return twitchVideoAsyncService.syncTwitchVideos(owner, ownerKind);
      })
      .catch((error) => twitchVideoAsyncService.logger?.error(error, "Error while processing syncTwitchVideosQueue"));
  }
}
