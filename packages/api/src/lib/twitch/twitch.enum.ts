export enum TwitchCronsEnum {
  TWITCH_CLIPS = "SyncTwitchClipsCron",
  TWITCH_FOLLOWERS = "SyncTwitchFollowersCron",
  TWITCH_FRIENDS = "SyncTwitchFriendsCron",
  TWITCH_INFO = "SyncTwitchInfoCron",
  TWITCH_VIDEOS = "SyncTwitchVideosCron"
}
