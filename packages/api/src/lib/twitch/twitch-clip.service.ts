import { FilterQuery, QueryOptions } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { TwitchClipModel, TwitchClip } from "../../backend/models/twitch-clip.model";

type TwitchClipDocument = DocumentType<TwitchClip>;
@Service("twitch-clip.service")
export class TwitchClipService extends BaseService {
  private readonly twitchClips = TwitchClipModel;

  public async findOne(filter: FilterQuery<TwitchClipDocument>): Promise<TwitchClipDocument | null>;
  public async findOne(filter: FilterQuery<TwitchClipDocument>, projection: any): Promise<TwitchClipDocument | null>;
  public async findOne(
    filter: FilterQuery<TwitchClipDocument>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<TwitchClipDocument | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<TwitchClipDocument>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<TwitchClipDocument | null> {
    const twitchClip = await this.twitchClips.findOne(filter, projection, options);
    return twitchClip;
  }
}
