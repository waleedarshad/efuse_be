import { Types } from "mongoose";
import { Queue } from "bull";
import { AxiosResponse } from "axios";
import { Failure } from "@efuse/contracts";

import { OwnerKind } from "@efuse/entities";
import { ITwitchFriend } from "../../backend/interfaces";
import { TwitchFriend, TwitchFriendModel } from "../../backend/models/twitch-friend";
import { Service, Trace } from "../decorators";
import { TwitchAsyncService } from "./twitch-async.service";
import Config from "../../async/config";
import { TwitchCronsEnum } from "./twitch.enum";
import { UserService } from "../users/user.service";

import { IUser } from "../../backend/interfaces/user";

interface ITwitchFriendResponse {
  pagination?: { cursor: string };
  data: ITwitchFriend[];
}

@Service("twitch-friend-async.service")
export class TwitchFriendAsyncService extends TwitchAsyncService {
  private $twitchFriends: TwitchFriendModel<ITwitchFriend> = TwitchFriend;

  private $storeTwitchFriendsQueue: Queue;
  private $syncTwitchFriendsQueue: Queue;

  private $userService: UserService;

  constructor() {
    super();

    this.$storeTwitchFriendsQueue = Config.initQueue("storeFriends");

    this.$syncTwitchFriendsQueue = Config.initQueue("syncTwitchFriends", { limiter: { max: 5, duration: 1000 } });

    this.$userService = new UserService();
  }

  /**
   * @summary Sync Twitch Friends
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {string} idParam
   * @param {string} friendType
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncTwitchFriends(
    owner: Types.ObjectId | string,
    ownerKind: string,
    idParam: string,
    friendType: string
  ): Promise<void> {
    try {
      await this.setOauthToken();

      const user: IUser | null = await this.$userService.findOne({ _id: owner }, this.twitchFields, { lean: true });

      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      if (!user.twitchID) {
        this.logger?.info({ user }, "twitchID not found");
        throw Failure.UnprocessableEntity("twitchID not found");
      }

      await this.fetchTwitchFriends(user._id, idParam, friendType);

      // Update Cron
      this.markCronAsSuccess(TwitchCronsEnum.TWITCH_FRIENDS, user._id);
    } catch (error) {
      this.logger?.error(error, "Error syncing twitch friends");

      // Update cron

      this.markCronAsFailure(TwitchCronsEnum.TWITCH_FRIENDS, owner, error.message);
    }
  }

  /**
   * @summary Sync Twitch Friends
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {string} idParam
   * @param {string} friendType
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncTwitchFriendsAsync(
    owner: Types.ObjectId | string,
    ownerKind: string,
    idParam: string,
    friendType: string
  ): void {
    this.$syncTwitchFriendsQueue
      .add({ owner, ownerKind, idParam, friendType })
      .catch((error) => this.logger?.error(error, "Error adding friends to queue syncTwitchFriendsQueue"));
  }

  /**
   * @summary Cron job to sync twitch friends
   *
   * @returns {Promise<void>}
   */
  @Trace({ service: "twitch-friend-async.service", name: TwitchCronsEnum.TWITCH_FRIENDS })
  public static async SyncTwitchFriendsCron(): Promise<void> {
    const twitchFriendAsyncService = new TwitchFriendAsyncService();

    const cronProcessor = (user: IUser): void => {
      twitchFriendAsyncService.syncTwitchFriendsAsync(user._id, OwnerKind.USER, `to_id=${user.twitchID}`, "follower");
      twitchFriendAsyncService.syncTwitchFriendsAsync(user._id, OwnerKind.USER, `from_id=${user.twitchID}`, "followee");
    };

    await twitchFriendAsyncService.executeCron(TwitchCronsEnum.TWITCH_FRIENDS, cronProcessor);
  }

  /**
   * @summary Fetch twitch friends for a particular user
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} idParam
   * @param {string} friendType
   * @param {string} cursor
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async fetchTwitchFriends(
    userId: Types.ObjectId | string,
    idParam: string,
    friendType: string,
    cursor?: string
  ): Promise<void> {
    let queryParams = `first=100&${idParam}`;

    if (cursor) {
      queryParams = `${queryParams}&after=${cursor}`;
    }

    // Fetch Friends
    const response: AxiosResponse<ITwitchFriendResponse> = await this.$http.get(`/users/follows?${queryParams}`);

    const { data, pagination } = response.data;

    this.storeFriendsAsync(userId, data, friendType);

    if (pagination && pagination.cursor) {
      await this.fetchTwitchFriends(userId, idParam, friendType, pagination.cursor);
    }
  }

  /**
   * @summary Store friends in DB using bulk operations
   *
   * @param {Types.ObjectId | string} userId
   * @param {ITwitchFriend[]} friends
   * @param {string} friendType
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async storeFriends(
    userId: Types.ObjectId | string,
    friends: ITwitchFriend[],
    friendType: string
  ): Promise<void> {
    try {
      if (!friends || friends.length === 0) {
        this.logger?.info({ userId }, `No ${friendType} found for user`);
        return;
      }

      const bulkOperations: any[] = friends.map((friend) => {
        return {
          updateOne: {
            filter: {
              user: userId,
              from_id: friend.from_id,
              to_id: friend.to_id
            },
            update: {
              ...friend,
              user: userId,
              jobRanAt: Date.now()
            },
            upsert: true,
            setDefaultsOnInsert: true
          }
        };
      });

      const response = await this.$twitchFriends.bulkWrite(bulkOperations);
      this.logger?.info(
        { upserted: response.upsertedCount, updated: response.modifiedCount, userId },
        `Twitch ${friendType} synced for user`
      );
    } catch (error) {
      this.logger?.error({ error, userId }, `Error storing twitch ${friendType} for user`);
    }
  }

  /**
   * @summary Store friends in DB using bulk operations via bull queue
   *
   * @param {Types.ObjectId | string} userId
   * @param {ITwitchFriend[]} friends
   * @param {string} friendType
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private storeFriendsAsync(userId: Types.ObjectId | string, friends: ITwitchFriend[], friendType: string): void {
    this.$storeTwitchFriendsQueue
      .add({ userId, friends, friendType })
      .catch((error) => this.logger?.error(error, "Error adding friends to queue storeTwitchFriendsQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: "twitch-friend-async.service", name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const twitchFriendAsyncService = new TwitchFriendAsyncService();

    // storeTwitchFriendsQueue
    twitchFriendAsyncService.$storeTwitchFriendsQueue
      .process((job: { data: { userId: Types.ObjectId | string; friends: ITwitchFriend[]; friendType: string } }) => {
        const { userId, friends, friendType } = job.data;
        return twitchFriendAsyncService.storeFriends(userId, friends, friendType);
      })
      .catch((error) =>
        twitchFriendAsyncService.logger?.error(error, "Error while processing storeTwitchFriendsQueue")
      );

    // syncTwitchFriendsQueue
    twitchFriendAsyncService.$syncTwitchFriendsQueue
      .process(
        (job: { data: { owner: Types.ObjectId | string; ownerKind: string; idParam: string; friendType: string } }) => {
          const { owner, ownerKind, idParam, friendType } = job.data;
          return twitchFriendAsyncService.syncTwitchFriends(owner, ownerKind, idParam, friendType);
        }
      )
      .catch((error) => twitchFriendAsyncService.logger?.error(error, "Error while processing syncTwitchFriendsQueue"));
  }
}
