import { Types } from "mongoose";
import { Queue } from "bull";
import { AxiosResponse } from "axios";
import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";

import { Service, Trace } from "../decorators";
import { TwitchAsyncService } from "./twitch-async.service";
import { TwitchClipModel } from "../../backend/models/twitch-clip.model";
import { ITwitchClip } from "../../backend/interfaces/twitch-clip";
import Config from "../../async/config";
import { UserService } from "../users/user.service";
import { IUser } from "../../backend/interfaces/user";
import { TwitchCronsEnum } from "./twitch.enum";
// import {TwitchClipMediaService} from "./twitch-clip-media.service";
import TwitchClipAsync from "../../async/twitch-service/twitch-clip";

interface ITwitchClipResponse {
  pagination?: { cursor: string };
  data: ITwitchClip[];
}

@Service("twitch-clip-async.service")
export class TwitchClipAsyncService extends TwitchAsyncService {
  private readonly $twitchClips = TwitchClipModel;

  private $storeTwitchClipsQueue: Queue;
  private $syncTwitchClipsQueue: Queue;
  // private $uploadTwitchClipQueue: Queue;

  private $userService: UserService;
  // private $twitchClipMediaService: TwitchClipMediaService;

  constructor() {
    super();

    this.$storeTwitchClipsQueue = Config.initQueue("storeTwitchClips");
    this.$syncTwitchClipsQueue = Config.initQueue("syncTwitchClips", { limiter: { max: 5, duration: 1000 } });
    // this.$uploadTwitchClipQueue = Config.initQueue("uploadTwitchClip");

    this.$userService = new UserService();
    // this.$twitchClipMediaService = new TwitchClipMediaService();
  }

  /**
   * @summary Sync Twitch clips of user
   *
   * @param {Types.ObjectId | string} owner - Id of user
   * @param {string} ownerKind
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async syncTwitchClips(owner: Types.ObjectId | string, ownerKind: string): Promise<void> {
    try {
      await this.setOauthToken();

      const user: IUser | null = await this.$userService.findOne({ _id: owner }, this.twitchFields, { lean: true });

      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      if (!user.twitchID) {
        this.logger?.info({ user }, "twitchID not found");
        throw Failure.UnprocessableEntity("twitchID not found");
      }

      await this.fetchTwitchClips(user.twitchID, user._id);

      // Update cron
      this.markCronAsSuccess(TwitchCronsEnum.TWITCH_CLIPS, user._id);
    } catch (error) {
      this.logger?.error(error, "Error syncing twitch clips");

      // Update cron

      this.markCronAsFailure(TwitchCronsEnum.TWITCH_CLIPS, owner, error.message);
    }
  }

  /**
   * @summary Sync Twitch clips of user using bull queue
   *
   * @param {Types.ObjectId | string} owner - Id of user
   * @param {string} ownerKind
   *
   * @return {Promise<void>}
   */
  @Trace()
  public syncTwitchClipsAsync(owner: Types.ObjectId | string, ownerKind: string): void {
    this.$syncTwitchClipsQueue
      .add({ owner, ownerKind })
      .catch((error) => this.logger?.error(error, "Error adding clips to queue syncTwitchClipsQueue"));
  }

  /**
   * @summary Uploading of twitch clip asynchronously by adding job to queue
   *
   * @param {Types.ObjectId} feedId
   * @param {Types.ObjectId | string} twitchClipId
   */
  @Trace()
  public async uploadTwitchClipAsync(feedId: Types.ObjectId, twitchClipId: Types.ObjectId | string) {
    return await TwitchClipAsync.uploadTwitchClipQueue.add({
      feedId,
      twitchClipId
    });
  }

  /**
   * @summary Cron job to sync twitch clips
   *
   * @returns {Promise<void>}
   */
  @Trace({ service: "twitch-clip-async.service", name: TwitchCronsEnum.TWITCH_CLIPS })
  public static async SyncTwitchClipsCron(): Promise<void> {
    const twitchClipAsyncService = new TwitchClipAsyncService();

    const cronProcessor = (user: IUser): void => {
      twitchClipAsyncService.syncTwitchClipsAsync(user._id, OwnerKind.USER);
    };

    await twitchClipAsyncService.executeCron(TwitchCronsEnum.TWITCH_CLIPS, cronProcessor);
  }

  /**
   * @summary Get twitch clips
   *
   * @param {string} broadcasterId - broadcaster id
   * @param {Types.ObjectId | string} userId - Id of user
   * @param {string} cursor
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async fetchTwitchClips(
    broadcasterId: string,
    userId: Types.ObjectId | string,
    cursor?: string
  ): Promise<void> {
    let queryParams = `first=100&broadcaster_id=${broadcasterId}`;

    if (cursor) {
      queryParams = `${queryParams}&after=${cursor}`;
    }

    // Fetch clips
    const response: AxiosResponse<ITwitchClipResponse> = await this.$http.get(`/clips?${queryParams}`);

    const { data, pagination } = response.data;

    this.storeTwitchClipsAsync(userId, data);

    if (pagination && pagination.cursor) {
      await this.fetchTwitchClips(broadcasterId, userId, pagination.cursor);
    }
  }

  /**
   * @summary Store twitch clips in DB
   *
   * @param {Types.ObjectId y| string} userId
   * @param {ITwitchClip[]} clips
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async storeTwitchClips(userId: Types.ObjectId | string, clips: ITwitchClip[]): Promise<void> {
    try {
      if (!clips || clips.length === 0) {
        this.logger?.info({ userId }, "No clips found for user");
        return;
      }

      const bulkOperations: any[] = clips.map((clip) => {
        const data = {
          ...clip,
          clip_id: clip.id,
          user: userId,
          jobRanAt: Date.now()
        };

        delete data.id;

        return {
          updateOne: {
            filter: { clip_id: clip.id, user: userId },
            update: data,
            upsert: true,
            setDefaultsOnInsert: true
          }
        };
      });

      const response = await this.$twitchClips.bulkWrite(bulkOperations);
      this.logger?.info(
        { upserted: response.upsertedCount, updated: response.modifiedCount, userId },
        "Twitch clips synced for user"
      );
    } catch (error) {
      this.logger?.error({ error, userId }, "Error storing twitch clips for user");
    }
  }

  /**
   * @summary Store twitch clips in DB using bull queue
   *
   * @param {Types.ObjectId | string} userId
   * @param {ITwitchClip[]} clips
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private storeTwitchClipsAsync(userId: Types.ObjectId | string, clips: ITwitchClip[]): void {
    this.$storeTwitchClipsQueue
      .add({ userId, clips })
      .catch((error) => this.logger?.error(error, "Error adding clips to queue storeTwitchClipsQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: "twitch-clip-async.service", name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const twitchClipAsyncService = new TwitchClipAsyncService();

    // storeTwitchClipsQueue
    twitchClipAsyncService.$storeTwitchClipsQueue
      .process((job: { data: { userId: Types.ObjectId | string; clips: ITwitchClip[] } }) => {
        const { userId, clips } = job.data;
        return twitchClipAsyncService.storeTwitchClips(userId, clips);
      })
      .catch((error) => twitchClipAsyncService.logger?.error(error, "Error while processing storeTwitchClipsQueue"));

    // syncTwitchClipsQueue
    twitchClipAsyncService.$syncTwitchClipsQueue
      .process((job: { data: { owner: Types.ObjectId | string; ownerKind: string } }) => {
        const { owner, ownerKind } = job.data;
        return twitchClipAsyncService.syncTwitchClips(owner, ownerKind);
      })
      .catch((error) => twitchClipAsyncService.logger?.error(error, "Error while processing syncTwitchClipsQueue"));
  }
}
