import { Types, Model } from "mongoose";
import { OwnerKind } from "@efuse/entities";
import { AxiosResponse } from "axios";
import { Failure } from "@efuse/contracts";
import { Queue } from "bull";

import { Service, Trace } from "../decorators";
import { TwitchAsyncService } from "./twitch-async.service";
import { TwitchAccountInfoService } from "../twitch-account-info.service";
import { TwitchCronsEnum } from "./twitch.enum";
import { UserService } from "../users/user.service";
import { IUser, ITwitch } from "../../backend/interfaces";
import { Twitch } from "../../backend/models/Twitch";
import Config from "../../async/config";

const SERVICE = "twitch-account-info-async.service";

interface ITwitchInfoResponse {
  data: Array<{
    id: string;
    login: string;
    display_name: string;
    type: string;
    broadcaster_type: string;
    description: string;
    profile_image_url: string;
    offline_image_url: string;
    view_count: number;
    email: string;
    created_at: Date;
  }>;
}

@Service(SERVICE)
export class TwitchAccountInfoAsyncService extends TwitchAsyncService {
  private readonly $twitch: Model<ITwitch> = Twitch;

  private $syncTwitchInfoQueue: Queue;

  private $twitchAccountInfoService: TwitchAccountInfoService;
  private $userService: UserService;

  constructor() {
    super();

    this.$syncTwitchInfoQueue = Config.initQueue("syncTwitchInfo", { limiter: { max: 5, duration: 1000 } });

    this.$twitchAccountInfoService = new TwitchAccountInfoService();
    this.$userService = new UserService();
  }

  /**
   * @summary Sync user or organization's twitch info
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncTwitchInfo(owner: Types.ObjectId | string, ownerKind: string): Promise<void> {
    try {
      const twitchAccountInfo = await this.$twitchAccountInfoService.findOne({ owner: String(owner) });

      let user: IUser | null | undefined;
      let twitchId: string | undefined;

      // use accountId from account info if it's there
      if (twitchAccountInfo?.accountId) {
        twitchId = twitchAccountInfo.accountId;
      }

      // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
      // get the twitch account id from the user instead
      if (!twitchId && ownerKind === OwnerKind.USER) {
        user = await this.$userService.findOne({ _id: owner }, this.twitchFields);

        if (!user) {
          throw Failure.UnprocessableEntity("User not found");
        }

        twitchId = user.twitchID;
      }

      await this.setOauthToken();

      // Fetch Twitch Info
      const response: AxiosResponse<ITwitchInfoResponse> = await this.$http.get(`/users?id=${twitchId}`);

      const data = response.data.data[0];

      // TODO: [org-clout] Remove this user specific logic once all clout cards have been updated
      if (ownerKind === OwnerKind.USER && user?.twitchID) {
        const twitchObj = {
          viewCount: data.view_count,
          displayName: data.display_name,
          profileImageUrl: data.profile_image_url,
          description: data.description,
          type: data.type,
          broadcasterType: data.broadcaster_type
        };

        if (user?.twitch) {
          await this.$twitch.findOneAndUpdate({ _id: user.twitch }, { $set: twitchObj });
        } else {
          const twitch = await this.$twitch.create(twitchObj);
          user.twitch = twitch._id as string;

          this.logger?.info({ twitch }, "Updating twitch object of user");

          await user.save({ validateBeforeSave: false });

          this.logger?.info({ id: user.twitch }, "updated twitch id");
        }
      }

      const updatedAccountInfo: any = {
        owner,
        ownerKind,
        username: data.login,
        viewCount: data.view_count,
        displayName: data.display_name,
        profileImage: data.profile_image_url,
        description: data.description,
        type: data.type,
        broadcasterType: data.broadcaster_type,
        verified: true
      };

      await (twitchAccountInfo
        ? this.$twitchAccountInfoService.updateOne({ owner: String(owner) }, updatedAccountInfo)
        : this.$twitchAccountInfoService.create(updatedAccountInfo));

      // Update Cron
      this.markCronAsSuccess(TwitchCronsEnum.TWITCH_INFO, owner);
    } catch (error) {
      this.logger?.error(error, "Error syncing twitch info");

      // Update cron

      this.markCronAsFailure(TwitchCronsEnum.TWITCH_INFO, owner, error.message);
    }
  }

  /**
   * @summary Sync user or organization's twitch info
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncTwitchInfoAsync(owner: Types.ObjectId | string, ownerKind: string): void {
    this.$syncTwitchInfoQueue
      .add({ owner, ownerKind })
      .catch((error) => this.logger?.error(error, "Error adding to queue syncTwitchInfoQueue"));
  }

  /**
   * @summary Cron job to sync twitch account info
   *
   * @returns {Promise<void>}
   */
  @Trace({ service: SERVICE, name: TwitchCronsEnum.TWITCH_INFO })
  public static async SyncTwitchInfoCron(): Promise<void> {
    const twitchAccountInfoAsyncService = new TwitchAccountInfoAsyncService();

    const cronProcessor = (user: IUser): void => {
      twitchAccountInfoAsyncService.syncTwitchInfoAsync(user._id, OwnerKind.USER);
    };

    await twitchAccountInfoAsyncService.executeCron(TwitchCronsEnum.TWITCH_INFO, cronProcessor);
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const twitchAccountInfoAsyncService = new TwitchAccountInfoAsyncService();

    // syncTwitchInfoQueue
    twitchAccountInfoAsyncService.$syncTwitchInfoQueue
      .process((job: { data: { owner: Types.ObjectId | string; ownerKind: string } }) => {
        const { owner, ownerKind } = job.data;
        return twitchAccountInfoAsyncService.syncTwitchInfo(owner, ownerKind);
      })
      .catch((error) =>
        twitchAccountInfoAsyncService.logger?.error(error, "Error while processing syncTwitchInfoQueue")
      );
  }
}
