import { Failure } from "@efuse/contracts";
import axiosLib, { AxiosInstance, AxiosResponse } from "axios";

import { RIOT_API_KEY } from "@efuse/key-store";
import { BaseService } from "./base/base.service";
import { Service, Trace } from "./decorators";
import { IChampionMasteryDTO, ILeagueEntryDTO, ISummonerDTO } from "../backend/interfaces/leagueOfLegends";

@Service("riot-LoL-api.service")
export class RiotLoLApiService extends BaseService {
  private axios: AxiosInstance = axiosLib.create({
    headers: {
      "X-Riot-Token": RIOT_API_KEY
    }
  });

  /**
   * Retrieves a summoner's champion masteries
   *
   * @param {string} summonerId
   * @param {string} region
   * @return {*}  {Promise<void>}
   * @memberof RiotApiService
   */
  @Trace()
  public async fetchChampionMasteriesForSummoner(
    summonerId: string,
    region: string
  ): Promise<IChampionMasteryDTO[] | null> {
    const requestUrl = encodeURI(
      `${this.getRiotRootUrl(region, "lol")}champion-mastery/v4/champion-masteries/by-summoner/${summonerId}`
    );

    const result = <AxiosResponse<IChampionMasteryDTO[]>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchChampionMasteriesForSummoner", `${summonerId}:${region}`);
    });

    return result?.data ? result.data : null;
  }

  /**
   * Retrieves a summoner's league entry data
   *
   * @param {string} summonerId
   * @param {string} region
   * @return {*}  {Promise<void>}
   * @memberof RiotApiService
   */
  @Trace()
  public async fetchLeagueEntriesForSummoner(summonerId: string, region: string): Promise<ILeagueEntryDTO[] | null> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region, "lol")}league/v4/entries/by-summoner/${summonerId}`);

    const result = <AxiosResponse<ILeagueEntryDTO[]>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchLeagueEntriesForSummoner", `${summonerId}:${region}`);
    });

    return result?.data ? result.data : null;
  }

  /**
   * Retrieve a summoner's ID by summoner name
   *
   * @param {string} summonerId
   * @param {string} region
   * @return {*}  {Promise<void>}
   * @memberof RiotApiService
   */
  @Trace()
  public async fetchSummonerInfoByName(nickname: string, region: string): Promise<ISummonerDTO | null> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region, "lol")}summoner/v4/summoners/by-name/${nickname}`);

    const result = <AxiosResponse<ISummonerDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchSummonerInfoByName", `${nickname}:${region}`);
    });

    return result?.data ? result.data : null;
  }

  /**
   * Retrieve a summoner's info by summoner id
   *
   * @param {string} summonerId
   * @param {string} region
   * @return {*}  {Promise<void>}
   * @memberof RiotApiService
   */
  @Trace()
  public async fetchSummonerInfoByID(summonerId: string, region: string): Promise<ISummonerDTO | null> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region, "lol")}summoner/v4/summoners/${summonerId}`);

    const result = <AxiosResponse<ISummonerDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchSummonerInfoByID", `${summonerId}:${region}`);
    });

    return result?.data ? result.data : null;
  }

  /**
   * Retrieve a user's third party code previously set in the LoL settings
   *
   * @param {string} summonerId
   * @param {string} region
   * @return {*}  {Promise<void>}
   * @memberof RiotApiService
   */
  @Trace()
  public async fetchThirdPartyCode(summonerId: string, region: string): Promise<string | null> {
    const requestUrl = encodeURI(
      `${this.getRiotRootUrl(region, "lol")}platform/v4/third-party-code/by-summoner/${summonerId}`
    );

    const result = <AxiosResponse<string>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchThirdPartyCode", `${summonerId}:${region}`);
    });

    return result?.data ? result.data : null;
  }

  /**
   * Handle riot specific errors.
   * @param error
   * @param warnMessage
   */
  @Trace()
  private handleRiotError(error: any, request: string, warnMessage?: string): void {
    let riotError = error;
    let message = error?.message?.message || error?.message;

    // if data wasn't found, don't freak out.
    // just return, this happens and we don't want to terminate the async job
    if (error?.response?.status === 404) {
      this.logger?.warn({ request, info: warnMessage }, `Unable to find resource`);

      return;
    }

    if (!message) {
      try {
        riotError = JSON.parse(error.error);
        message = riotError?.status?.message;
      } catch (parseError) {
        message = parseError;
      }
    }

    throw Failure.InternalServerError(message, riotError);
  }

  /**
   * Return the root url for the api request
   *
   * @param region E.G. "na1"
   * @param service  E.G. "lol"
   * @returns string url
   */
  private getRiotRootUrl(region: string, service: string): string {
    return `https://${region.toLowerCase()}.api.riotgames.com/${service}/`;
  }
}
