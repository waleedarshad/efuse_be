import { Failure } from "@efuse/contracts";
import { Types, QueryOptions, FilterQuery } from "mongoose";

import { ITokenActionValues, ITokenLedgerEntry, TokenLedgerAction, TokenLedgerKind } from "../../backend/interfaces";
import { TokenLedgerEntry, TokenLedgerEntryModel } from "../../backend/models/token-ledger/token-ledger-entry";
import { ConstantsService } from "../constants";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

const TOKEN_ACTION_VALUES_CONSTANT_NAME = "TOKEN_ACTION_VALUES";

@Service("token-ledger.service")
export class TokenLedgerService extends BaseService {
  private readonly model: TokenLedgerEntryModel<ITokenLedgerEntry> = TokenLedgerEntry;
  private constantsService: ConstantsService = new ConstantsService();

  /**
   * Create Ledger Entry
   *
   * @param {ITokenLedgerEntry} item The score object
   *
   * @returns {Promise<ITokenLedgerEntry>} The created ledger entry object with id
   *
   * @memberof TokenLedgerService
   */
  @Trace()
  public async createEntry(
    action: TokenLedgerAction,
    ledgerKind: TokenLedgerKind,
    user: Types.ObjectId | string
  ): Promise<ITokenLedgerEntry> {
    try {
      if (!action) {
        throw Failure.BadRequest("Unable to create ledger entry, missing 'action'");
      }
      if (!ledgerKind) {
        throw Failure.BadRequest("Unable to create ledger entry, missing 'ledgerKind'");
      }
      if (!user) {
        throw Failure.BadRequest("Unable to create ledger entry, missing 'user'");
      }

      const values = await this.getTokenActionValues();
      const value = values[action];
      const userBalanceEntry = await this.getUserBalanceEntry(user);
      let balance = userBalanceEntry ? userBalanceEntry.balance : 0;

      if (ledgerKind === TokenLedgerKind.credit) {
        balance += value;
      } else if (ledgerKind === TokenLedgerKind.debit) {
        balance -= value;
      }

      if (balance < 0) {
        throw Failure.Forbidden("User does not have enough tokens to perform this action.");
      }

      const newLedgerEntry = {
        action,
        ledgerKind,
        user,
        value,
        balance
      };

      const doc = <ITokenLedgerEntry>newLedgerEntry;
      const result: ITokenLedgerEntry = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Fetches the latest entry for a user. This contains the active balance.
   *
   * @param {Types.ObjectId | string} user The user to find the balance entry for
   *
   * @returns {Promise<ITokenLedgerEntry>} The latest ledger entry for the user
   *
   * @memberof TokenLedgerService
   */
  public async getUserBalanceEntry(user: Types.ObjectId | string): Promise<ITokenLedgerEntry> {
    const balanceEntry = await this.findOne({ user }, null, { sort: { createdAt: -1 } });

    return <ITokenLedgerEntry>balanceEntry;
  }

  /**
   *  Find an entry based on its ID.
   *
   * @param {string} id - The ledger entry id
   *
   * @returns {Promise<ITokenLedgerEntry} The ledger entry corresponding to the ID
   *
   * @memberof TokenLedgerService
   */
  public async find(filter: FilterQuery<ITokenLedgerEntry>): Promise<ITokenLedgerEntry[]>;
  public async find(filter: FilterQuery<ITokenLedgerEntry>, projection: any): Promise<ITokenLedgerEntry[]>;
  public async find(
    filter: FilterQuery<ITokenLedgerEntry>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<ITokenLedgerEntry[]>;
  @Trace()
  public async find(
    filter: FilterQuery<ITokenLedgerEntry>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<ITokenLedgerEntry[]> {
    const ledgerEntries = await this.model.find(filter, projection, options);
    return ledgerEntries;
  }

  public async findOne(filter: FilterQuery<ITokenLedgerEntry>): Promise<ITokenLedgerEntry | null>;
  public async findOne(filter: FilterQuery<ITokenLedgerEntry>, projection: any): Promise<ITokenLedgerEntry | null>;
  public async findOne(
    filter: FilterQuery<ITokenLedgerEntry>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<ITokenLedgerEntry | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<ITokenLedgerEntry>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ITokenLedgerEntry | null> {
    const ledgerEntry = await this.model.findOne(filter, projection, options);
    return ledgerEntry;
  }

  /**
   * Delete an entry
   *
   * @param {Promise<ITokenLedgerEntry>} id The id of the entry to delete
   *
   * @return {Promise<ITokenLedgerEntry>} The deleted entry
   *
   * @memberof TokenLedgerService
   */
  @Trace()
  public async delete(id: string): Promise<ITokenLedgerEntry> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to delete item, missing 'id'");
      }

      const identifier: Types.ObjectId = Types.ObjectId(id);
      const item: ITokenLedgerEntry | null = await this.model.findByIdAndRemove(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async getTokenActionValues(): Promise<ITokenActionValues> {
    const tokenActionValues = <ITokenActionValues>(
      await this.constantsService.getVariables(TOKEN_ACTION_VALUES_CONSTANT_NAME)
    );

    return tokenActionValues;
  }
}
