import { FilterQuery, QueryOptions, Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { Service, Trace } from "../decorators";
import { UserExperience, UserExperienceModel } from "../../backend/models/UserExperience";
import { IUserExperience } from "../../backend/interfaces/user-experience";

import { BaseService } from "../base/base.service";

import { IExperience } from "../../backend/interfaces/experience";
import { ExperienceService } from "./experience.service";
@Service("user-experience.service")
export class UserExperienceService extends BaseService {
  private readonly userExperience: UserExperienceModel<IUserExperience> = UserExperience;
  private readonly $experienceService: ExperienceService;
  constructor() {
    super();
    this.$experienceService = new ExperienceService();
  }

  public async findOne(filter: FilterQuery<IUserExperience>): Promise<IUserExperience | null>;
  public async findOne(filter: FilterQuery<IUserExperience>, projection: any): Promise<IUserExperience | null>;
  public async findOne(
    filter: FilterQuery<IUserExperience>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IUserExperience | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IUserExperience>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserExperience | null> {
    const userExperience = await this.userExperience.findOne(filter, projection, options);
    return userExperience;
  }

  public async findOneAndUpdate(conditions: FilterQuery<IUserExperience>, update: any): Promise<IUserExperience>;
  public async findOneAndUpdate(
    conditions: FilterQuery<IUserExperience>,
    update: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserExperience | null>;
  @Trace()
  public async findOneAndUpdate(
    conditions: FilterQuery<IUserExperience>,
    update: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserExperience | null> {
    const userExperience = await this.userExperience.findOneAndUpdate(conditions, update, options);
    return userExperience;
  }
  /**
   * @summary Retrieves the list of userExperiences for a user and whether or not they should be displayed
   *
   * @param  {String} userId - Pass the userID for the desired user to update
   * @return {Array} Returns the list of userExperiences
   * @example let experiences = await this.$userExperienceService.getDisplayedUserExperiences(req.user.id);
   */

  @Trace()
  public async getDisplayedUserExperiences(
    userId: Types.ObjectId
  ): Promise<[{ experienceId: Types.ObjectId; name: string; display: boolean }]> {
    const experiences = await this.$experienceService.getAllExperiences();
    const displayedUserExperiences = await this.findDisplayExperiences(experiences, userId);
    return displayedUserExperiences;
  }

  /** @summary Finds all experiences to display for a user given a list of experiences and a userId
   *
   * @param  {Array} experiences - A list of all experiences you'd like to check
   * @param  {String} userId - Pass the userID for the desired user
   * @return {Array} Returns the array of the userExperiences a user should/shouldn't display
   * @example let displayedUserExperiences = await this.$userExperienceService.findDisplayExperiences(experiences, userId);
   */
  @Trace()
  public async findDisplayExperiences(
    experiences: IExperience[],
    userId: Types.ObjectId
  ): Promise<[{ experienceId: Types.ObjectId; name: string; display: boolean }]> {
    const returnedExperiences = experiences.map(async (exp: IExperience) => {
      const experience = await this.getUserExperience(exp.id, userId);

      if (experience && experience.metrics.globalViews > 0) {
        return { experienceId: exp.id, name: exp.name, display: false };
      }
      return experience
        ? { experienceId: exp.id, name: exp.name, display: true }
        : { experienceId: exp.id, name: exp.name, display: false };
    });
    const response = await Promise.all(returnedExperiences).then((experienceResponse) => {
      this.logger?.info(experienceResponse);
      return experienceResponse;
    });
    return <[{ experienceId: Types.ObjectId; name: string; display: boolean }]>response;
  }

  @Trace()
  public async getUserExperience(
    experienceId: string | Types.ObjectId,
    userId: string | Types.ObjectId
  ): Promise<IUserExperience | null> {
    const experience = await this.userExperience.findOne({
      experience: experienceId,
      user: userId
    });
    return experience;
  }

  /**
   * @summary Creates a new userExperience for every currently available experience in the data for the given user
   *
   * @param  {String} userId - Pass the userID for the desired user to create userExperiences for
   * @return {Array} Returns the array of the newly created userExperiences
   * @example await this.$userExperienceService.createAllUserExperiences(createdUser._id);
   */
  @Trace()
  public async createAllUserExperiences(userId): Promise<IUserExperience[]> {
    const experiences = await this.$experienceService.getAllExperiences();
    const addedExperiences = experiences.map(async (exp: IExperience) => {
      return this.createUserExperience(exp.id, userId);
    });
    const newExperiences = await Promise.all(addedExperiences);
    return newExperiences;
  }

  /**
   * @summary Create user experience
   * @param {IExperience} experienceParams
   *
   * @returns {Promise<IExperience}>
   */
  @Trace()
  public async createUserExperience(
    experienceId: Types.ObjectId | string,
    userId: Types.ObjectId
  ): Promise<IUserExperience> {
    const experience = await this.$experienceService.findOne({ _id: experienceId });

    const userExperience: IUserExperience | null = await this.findOne({ experience: experience?.id, user: userId });
    if (userExperience) throw Failure.BadRequest("User experience already exists");

    const userSubViews = experience?.subViews.map((v) => {
      return { name: v, views: 0, skips: 0 };
    });

    const newUserExperience: IUserExperience = await this.userExperience.create({
      experience: experienceId,
      user: userId,
      metrics: {
        subViews: userSubViews
      }
    });

    return newUserExperience;
  }
}
