import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { isEmpty } from "lodash";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { Experience, ExperienceModel } from "../../backend/models/experience.model";

@Service("experience.service")
export class ExperienceService extends BaseService {
  private readonly experience = ExperienceModel;

  /**
   * @summary Get All user experience
   *
   * @returns {DocumentType<Experience>}
   */
  @Trace()
  public async getAllExperiences(): Promise<DocumentType<Experience>[]> {
    return this.experience.find();
  }

  /**
   * @summary find experience
   *
   * @return {DocumentType<Experience>}
   */
  public async findOne(filter: FilterQuery<DocumentType<Experience>>): Promise<DocumentType<Experience> | null>;
  public async findOne(
    filter: FilterQuery<DocumentType<Experience>>,
    projection: any
  ): Promise<DocumentType<Experience> | null>;
  public async findOne(
    filter: FilterQuery<DocumentType<Experience>>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<DocumentType<Experience> | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<DocumentType<Experience>>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<DocumentType<Experience> | null> {
    const experience: DocumentType<Experience> | null = await this.experience.findOne(filter, projection, options);
    return experience;
  }

  /**
   * @summary find experience
   *
   * @return {DocumentType<Experience>[]}
   */
  public async find(filter: FilterQuery<DocumentType<Experience>>): Promise<DocumentType<Experience>[]>;
  public async find(
    filter: FilterQuery<DocumentType<Experience>>,
    projection: any
  ): Promise<DocumentType<Experience>[]>;
  public async find(
    filter: FilterQuery<DocumentType<Experience>>,
    projection: any,
    options: QueryOptions
  ): Promise<DocumentType<Experience>[]>;
  @Trace()
  public async find(
    filter: FilterQuery<DocumentType<Experience>>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<DocumentType<Experience>[]> {
    const experiences: DocumentType<Experience>[] = await this.experience.find(filter, projection, options);
    return experiences;
  }

  /**
   * @summary Create user experience
   * @param {DocumentType<Experience>} experienceParams
   *
   * @returns {Promise<DocumentType<Experience>}>
   */
  @Trace()
  public async create(experienceParams: DocumentType<Experience>): Promise<DocumentType<Experience>> {
    if (isEmpty(experienceParams)) {
      throw Failure.BadRequest("experience params are missing");
    }
    const experience = await this.experience.create(experienceParams);
    return experience;
  }
}
