const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { UserExperience } = require("../../backend/models/UserExperience");
const { ExperienceModel } = require("../../backend/models/experience.model");

/**
 * @summary Retrieves the list of userExperiences for a user and whether or not they should be displayed
 *
 * @param  {String} userId - Pass the userID for the desired user to update
 * @returns {Promise<any[]>} Returns the list of userExperiences
 * @example let experiences = await experienceslib.getDisplayedUserExperiences(req.user.id);
 */
const getDisplayedUserExperiences = async (userId) => {
  const experiences = await getAllExperiences();
  const displayedUserExperiences = await findDisplayExperiences(experiences, userId);
  return displayedUserExperiences;
};

/**
 * @summary Creates a new userExperience for the given userId and experienceId
 *
 * @param  {String} experienceId - Pass the experienceID for the desired experience
 * @param  {String} userId - Pass the userID for the desired user
 * @returns {Promise<any[]>} Returns the new userExperience
 * @example const newUserExperience = await experienceslib.createUserExperience(body.experience, req.user.id);
 */
const createUserExperience = async (experienceId, userId) => {
  const experience = await getExperienceById(experienceId);

  // todo: investigate
  // @ts-ignore
  const userExperience = await getUserExperience(experience.id, userId);

  // todo: investigate
  // @ts-ignore
  if (userExperience) return { error: "User experience already exists" };

  // todo: investigate
  // @ts-ignore
  const userSubViews = experience.subViews.map((v) => {
    return { name: v, views: 0, skips: 0 };
  });

  const newUserExperience = await new UserExperience({
    experience: experienceId,
    user: userId,
    metrics: {
      // todo: investigate
      // @ts-ignore
      ...experience.metrics,
      subViews: userSubViews
    }
  }).save();

  // todo: investigate
  // @ts-ignore
  return newUserExperience;
};

const getAllExperiences = async () => {
  return ExperienceModel.find();
};

/**
 * @summary Creates a new userExperience for every currently available experience in the data for the given user
 *
 * @param  {String} userId - Pass the userID for the desired user to create userExperiences for
 * @returns {Promise<any[]>} Returns the array of the newly created userExperiences
 * @example await experienceslib.createAllUserExperiences(createdUser._id);
 */
const createAllUserExperiences = async (userId) => {
  const experiences = await getAllExperiences();
  const addedExperiences = experiences.map((exp) => {
    return createUserExperience(exp.id, userId);
  });

  return addedExperiences;
};

const getExperienceById = (experienceId) => {
  return ExperienceModel.findOne({
    _id: experienceId
  });
};

const getUserExperience = (experienceId, userId) => {
  return UserExperience.findOne({
    experience: experienceId,
    user: userId
  });
};

/**
 * @summary Finds all experiences to display for a user given a list of experiences and a userId
 *
 * @param  {Array} experiences - A list of all experiences you'd like to check
 * @param  {String} userId - Pass the userID for the desired user
 * @returns {Promise<any[]>} Returns the array of the userExperiences a user should/shouldn't display
 * @example let displayedUserExperiences = await findDisplayExperiences(experiences, userId);
 */
const findDisplayExperiences = async (experiences, userId) => {
  const returnedExperiences = experiences.map(async (exp) => {
    const experience = await getUserExperience(exp.id, userId);
    if (experience && experience.metrics.globalViews > 0) {
      return { experienceId: exp.id, name: exp.name, display: false };
    } else if (experience) {
      return { experienceId: exp.id, name: exp.name, display: true };
    } else {
      return { experienceId: exp.id, name: exp.name, display: false };
    }
  });
  const response = await Promise.all(returnedExperiences).then((experienceResponse) => {
    logger.info(experienceResponse);
    return experienceResponse;
  });
  return response;
};

module.exports.createAllUserExperiences = createAllUserExperiences;
module.exports.createUserExperience = createUserExperience;
module.exports.findDisplayExperiences = findDisplayExperiences;
module.exports.getAllExperiences = getAllExperiences;
module.exports.getDisplayedUserExperiences = getDisplayedUserExperiences;
module.exports.getExperienceById = getExperienceById;
module.exports.getUserExperience = getUserExperience;
