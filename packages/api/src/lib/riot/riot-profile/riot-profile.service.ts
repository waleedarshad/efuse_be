import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions, Types } from "mongoose";

import { Service, Trace } from "../../decorators";
import { BaseService } from "../../base/base.service";
import { RiotProfile, RiotProfileModel } from "../../../backend/models/riot-profile";
import { IRiotProfile } from "../../../backend/interfaces";

@Service("riot-profile.service")
export class RiotProfileService extends BaseService {
  protected $riotProfiles: RiotProfileModel<IRiotProfile> = RiotProfile;

  public async find(filter: FilterQuery<IRiotProfile>): Promise<IRiotProfile[]>;
  public async find(filter: FilterQuery<IRiotProfile>, projection: any): Promise<IRiotProfile[]>;
  public async find(
    filter: FilterQuery<IRiotProfile>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IRiotProfile[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IRiotProfile>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IRiotProfile[]> {
    const profile = await this.$riotProfiles.find(filter, projection, options);
    return profile;
  }

  public async findOne(filter: FilterQuery<IRiotProfile>): Promise<IRiotProfile | null>;
  public async findOne(filter: FilterQuery<IRiotProfile>, projection: any): Promise<IRiotProfile | null>;
  public async findOne(
    filter: FilterQuery<IRiotProfile>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IRiotProfile | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IRiotProfile>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IRiotProfile | null> {
    const profile = await this.$riotProfiles.findOne(filter, projection, options);
    return profile;
  }

  /**
   * @summary Update a particular profile
   *
   * @param {FilterQuery<IRiotProfile>} condition
   * @param {Partial<IRiotProfile>} body
   * @param {QueryOptions | null | undefined} options
   *
   * @returns {Promise<IRiotProfile>}
   */
  @Trace()
  public async update(
    condition: FilterQuery<IRiotProfile>,
    body: Partial<IRiotProfile>,
    options: QueryOptions | null | undefined
  ): Promise<IRiotProfile> {
    if (!body) {
      throw Failure.BadRequest("body is missing");
    }

    // Update the corresponding object as per condition
    const riotProfile: IRiotProfile | null = await this.$riotProfiles
      .findOneAndUpdate(condition, { ...body }, options)
      .exec();

    // Make sure object is present
    if (!riotProfile) {
      throw Failure.UnprocessableEntity("Riot Profile not found");
    }

    return riotProfile;
  }

  /**
   * @summary Delete profile by owner
   *
   * @param {Types.ObjectId | string} owner
   *
   * @returns {Promise<IRiotProfile | null>}
   */
  @Trace()
  public async deleteByOwner(owner: Types.ObjectId | string): Promise<IRiotProfile | null> {
    const profile = await this.$riotProfiles.findOneAndDelete({ owner });
    return profile;
  }
}
