var Airtable = require("airtable");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

Airtable.configure({
  endpointUrl: "https://api.airtable.com",
  apiKey: "keyof8oaF4Twsqhlb"
});

/**
 * @summary Get data from the Airtable
 *
 * @param {String} baseName - The name of base
 * @param {String} viewName - The name of view
 * @param {String} baseId - The base id
 * @return {Promise<Airtable.Records<Airtable.FieldSet>>} results
 */
const getAirtableDatav2 = async (baseName, viewName, baseId) => {
  try {
    const base = Airtable.base(baseId);
    const results = await base(baseName).select({ view: viewName }).all();

    return results;
  } catch (e) {
    logger.error(`[${baseId} / ${baseName} / ${viewName}] Error getting results from base: ${e.message}`);

    return [];
  }
};

module.exports.getAirtableDatav2 = getAirtableDatav2;
