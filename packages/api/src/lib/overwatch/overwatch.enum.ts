export enum SegmentType {
  competitive = "competitive",
  casual = "casual"
}
