import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";

import { BaseService } from "../base/base.service";
import { IOverwatchStatsResponse } from "../../backend/interfaces/overwatch/overwatch-stats";
import { OverwatchSegmentData } from "../../backend/models/overwatch/types/segment.model";

import { IUser } from "../../backend/interfaces/user";
import { IUserOverwatchStats } from "../../backend/interfaces/user-overwatch-stats";
import { OVERWATCH } from "../metrics";
import { OverwatchQueueService } from "./overwatch-api-queue.service";
import { OverwatchStats, OverwatchStatsModel } from "../../backend/models/overwatch/overwatch.model";
import { SegmentType } from "./overwatch.enum";
import { Service, Trace } from "../decorators";
import { User } from "../../backend/models/User";

@Service("overwatch.service")
export class OverwatchService extends BaseService {
  private model = OverwatchStatsModel;
  private overwatchQueueService: OverwatchQueueService = new OverwatchQueueService();

  /**
   * Updates user's overwatch stats to match the passed info
   *
   * @param userId
   * @param IUserOverwatchStats
   *
   * @return {IUserOverwatchStats} The updated data
   * @memberof OverwatchService
   */
  @Trace()
  public async updateOverwatchStats(
    userId: string,
    userOverwatchStats: IUserOverwatchStats
  ): Promise<IUserOverwatchStats> {
    this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.START);
    try {
      let retrievedUser: IUser | null = await User.findOne({ _id: userId });

      // Fetch stats & store in DB
      retrievedUser = await this.storeStats(userOverwatchStats.nickname, userOverwatchStats.platform, retrievedUser);

      if (!retrievedUser?.overwatch?.stats) {
        throw Failure.InternalServerError("Unable to save overwatch stats");
      }

      this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.FINISH);

      return retrievedUser.overwatch;
    } catch (error) {
      this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.ERROR);
      throw this.handleError(
        Failure.BadRequest(
          `[${userOverwatchStats.nickname} | ${userOverwatchStats.platform}] Error getting overwatch stats. Please check the overwatch name and region.`,
          this.buildError(error)
        )
      );
    }
  }

  /**
   * get player's overwatch stats
   *
   * @param platform
   * @param nickname
   *
   * @return {OverwatchStats} The stats data
   * @memberof OverwatchService
   */
  @Trace()
  public async getOverwatchStats(platform: string, nickname: string): Promise<OverwatchStats> {
    this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.START);
    try {
      this.logger?.info({ nickname, platform }, "Attempting to retrieve overwatch stats.");

      // Get overwatchStats
      const overwatchStats: IOverwatchStatsResponse = await this.overwatchQueueService.getOverwatchStatsByField(
        nickname,
        platform
      );

      const updatedOverwatchStats = <OverwatchStats>{ leaderboardScore: 0 };
      const updatedSegments: OverwatchSegmentData[] = [];

      overwatchStats.segments?.forEach((segment) => {
        const segmentData = <OverwatchSegmentData>(<unknown>{
          realm: segment.attributes?.realm || "",
          expiryDate: segment.expiryDate || "",
          stats: {},
          metadata: { name: segment.metadata?.name || "" },
          segmentType: segment.type || ""
        });

        Object.keys(segment.stats).forEach((key) => {
          segmentData.stats[key] = {
            displayValue: segment.stats[key].displayValue,
            percentile: segment.stats[key].percentile
          };
        });
        updatedSegments.push(segmentData);

        // Set the leaderboardScore field
        if (segmentData.realm === SegmentType.competitive) {
          const wins = Number(segmentData.stats?.wins?.displayValue);

          // Make sure we get a valid number
          if (!Number.isNaN(wins)) {
            updatedOverwatchStats.leaderboardScore = wins;
          }
        }
      });

      updatedOverwatchStats.platformInfo = overwatchStats.platformInfo;
      updatedOverwatchStats.userInfo = overwatchStats.userInfo;
      updatedOverwatchStats.segments = updatedSegments;
      updatedOverwatchStats.expiryDate = overwatchStats.expiryDate;

      this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.FINISH);

      return updatedOverwatchStats as OverwatchStats;
    } catch (error) {
      this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.ERROR);
      throw this.handleError(
        Failure.BadRequest(
          `[${nickname} | ${platform}] Error getting overwatch stats. Please check the overwatch name and region.`,
          this.buildError(error)
        )
      );
    }
  }

  /**
   * Get a user's overwatch stats
   *
   * @param {Types.ObjectId} userID the user to reterive stats for
   *
   * @return {Promise<OverwatchStats>}
   * @memberof OverwatchService
   */
  public async getUserOverwatchStats(userID: Types.ObjectId): Promise<OverwatchStats> {
    this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.START);

    try {
      const user: IUser | null = await User.findOne({ _id: userID }).select("overwatch");

      if (!user?.overwatch?.nickname) {
        throw Failure.BadRequest("User does not have a overwatch account configured.");
      }

      const statsDoc = await this.model.findById(user.overwatch.stats);

      if (!statsDoc) {
        throw Failure.NotFound("Unable to find user's overwatch stats");
      }

      this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.FINISH);

      return statsDoc;
    } catch (error) {
      this.recordMetric(OVERWATCH.GET_USER_OVERWATCH_STATS.ERROR);
      throw this.handleError(
        Failure.BadRequest(`Error getting overwatch info for user: ${userID.toHexString()}`, this.buildError(error))
      );
    }
  }

  /**
   * Retrieve and save Overwatch stats for specified user
   *
   * @param {Types.ObjectId} userID the user to sync stats for
   *
   * @return {Promise<void>} the updated stats
   * @memberof OverwatchService
   */
  @Trace()
  public async syncUserOverwatchStats(userID: Types.ObjectId): Promise<void> {
    this.recordMetric(OVERWATCH.SYNC_USER_OVERWATCH_STATS.START);
    try {
      let retrievedUser: IUser | null = await User.findOne({ _id: userID });

      retrievedUser = await this.storeStats(
        retrievedUser?.overwatch?.nickname,
        retrievedUser?.overwatch?.platform,
        retrievedUser
      );

      if (!retrievedUser?.overwatch?.stats) {
        throw Failure.InternalServerError("Unable to save overwatch stats");
      }

      this.recordMetric(OVERWATCH.SYNC_USER_OVERWATCH_STATS.FINISH);
    } catch (error) {
      this.recordMetric(OVERWATCH.SYNC_USER_OVERWATCH_STATS.ERROR);

      throw this.handleError(
        Failure.BadRequest(
          "Error syncing overwatch stats. Please check the overwatch name and platform.",
          this.buildError(error)
        )
      );
    }
  }

  /**
   * update stats for all user who has configured overwatch account
   * This static method is meant be used as a cron task
   * @param {Types.ObjectId} userID the user to sync stats for
   *
   * @return {Promise<IUser>} the updated user
   * @memberof OverwatchService
   */
  @Trace({ name: "SyncAllUserOverwatchStatsCron", service: "overwatch.service" })
  public static async SyncAllUserOverwatchStatsCron(): Promise<void> {
    const overwatchService = new OverwatchService();
    overwatchService.recordMetric(OVERWATCH.SYNC_ALL_OVERWATCH_STATS_CRON.START);

    try {
      const usersWithOverwatchStats = <IUser[]>await User.find({
        "overwatch.nickname": { $ne: null },
        "overwatch.platform": { $ne: null }
      }).select("overwatch");

      usersWithOverwatchStats.forEach((overWatchUser) => {
        overwatchService
          .syncUserOverwatchStats(overWatchUser._id)
          .then(() => {
            overwatchService.logger?.info(
              `Successfully updated Overwatch stats: User|${overWatchUser._id.toHexString()} overwatch|${<string>(
                overWatchUser.overwatch?.nickname
              )} Platform|${overWatchUser.overwatch?.platform}`
            );
          })
          .catch((error) => {
            throw overwatchService.handleError(error, `Failed to sync stats for ${overWatchUser._id.toHexString()}`);
          });
      });
      overwatchService.recordMetric(OVERWATCH.SYNC_ALL_OVERWATCH_STATS_CRON.FINISH);
    } catch (error) {
      overwatchService.recordMetric(OVERWATCH.SYNC_ALL_OVERWATCH_STATS_CRON.ERROR);
      throw overwatchService.handleError(Failure.BadRequest("Unable to sync all overwatch stats.", error));
    }
  }

  /**
   * @summary Store overwatch stats in DB
   *
   * @param {string} nickname
   * @param {string} platform
   * @param {IUser | undefined} user
   *
   * @returns {Promise<IUser>}
   */
  @Trace()
  private async storeStats(
    nickname: string | undefined,
    platform: string | undefined,
    user: IUser | undefined | null
  ): Promise<IUser> {
    if (!user) {
      throw Failure.UnprocessableEntity("user not found");
    }

    if (!nickname) {
      throw Failure.BadRequest("nickname not found");
    }

    if (!platform) {
      throw Failure.BadRequest("platform not found");
    }

    // Get overwatchStats
    const overwatchStats = await this.getOverwatchStats(platform, nickname);

    let updatedOverwatchStats = {} as OverwatchStats;

    if (user.overwatch?.stats) {
      // Find existing stats object
      const statsDoc = await this.model.findById(user.overwatch.stats);

      // Update fields
      if (statsDoc) {
        statsDoc.platformInfo = overwatchStats.platformInfo;
        statsDoc.userInfo = overwatchStats.userInfo;
        statsDoc.segments = overwatchStats.segments;
        statsDoc.expiryDate = overwatchStats.expiryDate;
        statsDoc.leaderboardScore = overwatchStats.leaderboardScore;

        updatedOverwatchStats = await statsDoc.save();
      }
    } else {
      // Create new object
      const statsDoc = overwatchStats;

      updatedOverwatchStats = await this.model.create(statsDoc);
    }

    user.overwatch = { nickname, platform, stats: updatedOverwatchStats?._id };

    await user.save({ validateBeforeSave: false });

    return user;
  }

  @Trace()
  private buildError(error: any): Record<string, unknown> {
    const errorObject: Record<string, unknown> = error.isAxiosError ? error.response?.data : error;
    return errorObject;
  }
}
