import { Failure } from "@efuse/contracts";
import axiosLib from "axios";
import { Queue } from "bull";

import { BaseService } from "../base/base.service";
import { IOverwatchStatsResponse } from "../../backend/interfaces/overwatch/overwatch-stats";
import { OVERWATCH_API_KEY } from "@efuse/key-store";
import { Service, Trace } from "../decorators";
import * as Config from "../../async/config";

//  Using same limit as we are using for league of legends

const OVERWATCH_API_LIMITS = {
  max: 100, // 50
  duration: 120000 // 1000
};

const axios = axiosLib.create({
  baseURL: "https://public-api.tracker.gg/v2/overwatch/standard",
  headers: { "TRN-API-KEY": OVERWATCH_API_KEY }
});

const QUEUE_NAME = "overwatch-api-calls";

@Service("overwatch-api-queue.service")
export class OverwatchQueueService extends BaseService {
  private static overwatchQueue: Queue;

  /**
   * The OverwatchQueue is a rate limited implementation of the Bull Queue used to execute ALL queries to the Overwatch API.
   * When one of the public function on the `OverwatchQueueService` are called this queue is used to execute the
   * underlying api call.
   *
   * @memberof OverwatchQueueService
   */
  @Trace()
  private static get OverwatchQueue(): Queue {
    if (!OverwatchQueueService.overwatchQueue) {
      // attempt to get existing queue before creating a new one
      const owQueue: Queue = Config.getQueue(QUEUE_NAME);

      if (owQueue) {
        OverwatchQueueService.overwatchQueue = owQueue;
      } else {
        OverwatchQueueService.overwatchQueue = Config.initQueue(QUEUE_NAME, {
          limiter: OVERWATCH_API_LIMITS,
          defaultJobOptions: { removeOnComplete: true }
        });

        OverwatchQueueService.overwatchQueue
          .process(async (job) => {
            const { platform, nickname } = job.data.params;

            switch (job.data.funcName) {
              case "getOverwatchStatsByField":
                return OverwatchQueueService.getOverwatchStatsByFieldQueued(nickname, platform);
              default:
                throw Failure.InternalServerError(`No queue service function named ${<string>job.data.funcName}`);
            }
          })
          .catch((error) => {
            const overwatchQueueService = new OverwatchQueueService();

            overwatchQueueService.logger?.error(error, "Error resolving overwatchQueue promise");
          });
      }
    }

    return OverwatchQueueService.overwatchQueue;
  }

  /**
   * Retrieve the a player's stats based on their nickname and platform.
   * Note: this function creates a queue task in order to not exceed rate limits, it may be significantly delayed.
   *
   * @param nickname
   * @param platform
   *
   * @returns <string> overwatch stats
   * @memberof OverwatchQueueService
   */
  @Trace()
  public async getOverwatchStatsByField(nickname: string, platform: string): Promise<IOverwatchStatsResponse> {
    const data = {
      funcName: "getOverwatchStatsByField",
      params: {
        nickname,
        platform
      }
    };

    const job = await OverwatchQueueService.OverwatchQueue.add(data, { priority: 1 });

    const result = await job.finished();
    return <IOverwatchStatsResponse>result;
  }

  /**
   * Retrieve the player's profile stats.
   *
   * @param nickname
   * @param platform
   *
   * @returns <string> summoner id
   * @memberof OverwatchQueueService
   */
  @Trace()
  private static async getOverwatchStatsByFieldQueued(
    nickname: string,
    platform: string
  ): Promise<IOverwatchStatsResponse> {
    let name = nickname;
    if (platform === "battlenet") name = encodeURIComponent(name);

    const metaResponse = await axios.get(`/profile/${platform}/${name}/`);

    return <IOverwatchStatsResponse>metaResponse.data.data;
  }
}
