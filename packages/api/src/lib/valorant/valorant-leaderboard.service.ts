import { Failure } from "@efuse/contracts";
import { Types, QueryOptions, FilterQuery } from "mongoose";

import { IValorantLeaderboardPlayerDTO } from "../../backend/interfaces/valorant/valorant-leaderboard-player-dto";
import {
  ValorantLeaderboardPlayer,
  ValorantLeaderboardPlayerModel
} from "../../backend/models/valorant/valorant-leaderboard-player";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("valorant-leaderboard-player.service")
export class ValorantLeaderboardPlayerService extends BaseService {
  private readonly model: ValorantLeaderboardPlayerModel<IValorantLeaderboardPlayerDTO> = ValorantLeaderboardPlayer;

  /**
   * Create Leaderboard Player
   *
   * @param {IValorantLeaderboardPlayerDTO} item The leaderboard player object
   *
   * @returns {Promise<IValorantLeaderboardPlayerDTO>} The created leaderboard player object with id
   *
   * @memberof ValorantLeaderboardPlayerService
   */
  @Trace()
  public async create(item: IValorantLeaderboardPlayerDTO): Promise<IValorantLeaderboardPlayerDTO> {
    try {
      if (!item) {
        throw Failure.BadRequest("Unable to create item, missing 'item'");
      }

      const doc = <IValorantLeaderboardPlayerDTO>item;
      const result: IValorantLeaderboardPlayerDTO = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Create Many Leaderboard Player
   *
   * @param {IValorantLeaderboardPlayerDTO} items The leaderboard player objects
   *
   * @returns {Promise<IValorantLeaderboardPlayerDTO[]>} The created leaderboard player objects
   *
   * @memberof ValorantLeaderboardPlayerService
   */
  @Trace()
  public async createMany(items: IValorantLeaderboardPlayerDTO[]): Promise<IValorantLeaderboardPlayerDTO[]> {
    try {
      if (!items) {
        throw Failure.BadRequest("Unable to create items, missing 'items'");
      }

      const docs = items.map((item) => <IValorantLeaderboardPlayerDTO>item);
      const result = await this.model.insertMany(docs);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *  Find a leaderboard player based on the its ID.
   *
   * @param {string} id - The leaderboard player id
   *
   * @returns {Promise<IValorantLeaderboardPlayerDTO} The eRena leaderboard player corresponding to the ID
   *
   * @memberof ValorantLeaderboardPlayerService
   */
  @Trace()
  public async findById(id: string | Types.ObjectId): Promise<IValorantLeaderboardPlayerDTO> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to find item, missing 'id'");
      }

      const identifier: Types.ObjectId = typeof id === "string" ? Types.ObjectId(id) : id;

      const item: IValorantLeaderboardPlayerDTO | null = await this.model.findById(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *  Find a leaderboard player
   *
   * @param {FilterQuery<IValorantLeaderboardPlayerDTO>} filter - The leaderboard player query
   *
   * @returns {Promise<IValorantLeaderboardPlayerDTO[]} The eRena leaderboard player(s) corresponding to the search query
   *
   * @memberof ValorantLeaderboardPlayerService
   */
  public async find(filter: FilterQuery<IValorantLeaderboardPlayerDTO>): Promise<IValorantLeaderboardPlayerDTO[]>;
  public async find(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>,
    projection: any
  ): Promise<IValorantLeaderboardPlayerDTO[]>;
  public async find(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantLeaderboardPlayerDTO[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantLeaderboardPlayerDTO[]> {
    const results = await this.model.find(filter, projection, options);

    return results;
  }

  public async findOne(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>
  ): Promise<IValorantLeaderboardPlayerDTO | null>;
  public async findOne(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>,
    projection: any
  ): Promise<IValorantLeaderboardPlayerDTO | null>;
  public async findOne(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantLeaderboardPlayerDTO | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IValorantLeaderboardPlayerDTO>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantLeaderboardPlayerDTO | null> {
    const result = await this.model.findOne(filter, projection, options);

    return result;
  }

  /**
   * Clear all leaderboard players for a specific region. This should be used before inserting new player data
   *
   * @param {string} region The regions of the leaderboard players to delete
   *
   * @return {Promise<boolean>} True if successful
   *
   * @memberof ValorantLeaderboardPlayerService
   */
  @Trace()
  public async delete(filter: FilterQuery<IValorantLeaderboardPlayerDTO>): Promise<void> {
    try {
      if (!filter) {
        throw Failure.BadRequest("Unable to delete, missing 'filter'");
      }

      await this.model.deleteMany(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
