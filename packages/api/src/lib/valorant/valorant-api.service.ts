import { Failure } from "@efuse/contracts";
import axiosLib, { AxiosInstance, AxiosResponse } from "axios";

import { VALORANT_API_KEY } from "@efuse/key-store";
import { IValorantContentDTO } from "../../backend/interfaces/valorant/valorant-content-dto";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { IValorantLeaderboardDTO } from "../../backend/interfaces/valorant/valorant-leaderboard-dto";
import { IValorantMatchListDTO } from "../../backend/interfaces/valorant/valorant-match-list-dto";
import { IValorantMatchDTO } from "../../backend/interfaces/valorant/valorant-match-dto";
import { IValorantActiveShardDTO } from "../../backend/interfaces/valorant/valorant-active-shard-dto";
import { ValorantQueueType } from "./valorant-queue-type-enum";
import { IValorantRecentMatchesDTO } from "../../backend/interfaces/valorant/valorant-recent-matches-dto";
import { RiotRegion } from "./riot-region.enum";
import { IRiotAccountDTO } from "../../backend/interfaces/riot/riot-account-dto";

enum ValorantService {
  RIOT = "riot",
  VAL = "val"
}

/**
 * The ValorantApiService is meant to be the only point of interaction with the official Valorant API.
 *
 * Make sure to not directly use any of these endpoints in code, except in "valorant-api-queue.service.ts".
 * They need to be rate limited or else we will get alot of 429 errors.
 *
 * @link https://developer.riotgames.com/
 */
@Service("valorant-api.service")
export class ValorantApiService extends BaseService {
  private axios: AxiosInstance = axiosLib.create({
    headers: {
      "X-Riot-Token": String(VALORANT_API_KEY)
    }
  });

  /**
   * Retrieve Riot account info
   *
   * Note: It doesn't matter what region we use. All regions have access to all accounts. Its recommended to use the one closest to us.
   *
   * @link https://developer.riotgames.com/apis#account-v1/GET_getByPuuid
   *
   * @return {Promise<IRiotAccountDTO>}
   * @memberof RiotApiService
   */
  @Trace()
  public async fetchRiotAccountInfo(region: RiotRegion, puuid: string): Promise<IRiotAccountDTO> {
    const requestUrl = `${this.getRiotRootUrl(region, ValorantService.RIOT)}/account/v1/accounts/by-puuid/${puuid}`;

    const result = <AxiosResponse<IRiotAccountDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchRiotAccountInfo");
    });

    return result?.data;
  }

  /**
   * Retrieve Valorant's Content collection.
   * This contains a lot of information about the game.
   *
   * @link https://developer.riotgames.com/apis#val-content-v1/GET_getContent
   *
   * @return {Promise<IValorantContentDTO>}
   * @memberof ValorantApiService
   */
  @Trace()
  public async fetchValorantContent(region: string): Promise<IValorantContentDTO> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region)}/content/v1/contents`);

    const result = <AxiosResponse<IValorantContentDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchValorantContent");
    });

    return result?.data;
  }

  /**
   * Retrieve Valorant's Leaderboard
   *
   * @link https://developer.riotgames.com/apis#val-ranked-v1/GET_getLeaderboard
   *
   * @return {Promise<IValorantLeaderboardDTO[]>}
   * @memberof ValorantApiService
   */
  @Trace()
  public async fetchValorantLeaderboard(
    region: string,
    actId: string,
    startIndex = 0,
    size = 200
  ): Promise<IValorantLeaderboardDTO> {
    const requestUrl = encodeURI(
      `${this.getRiotRootUrl(region)}/ranked/v1/leaderboards/by-act/${actId}?size=${size}&startIndex=${startIndex}`
    );

    const result = <AxiosResponse<IValorantLeaderboardDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchValorantLeaderboard");
    });

    return result?.data;
  }

  /**
   * Retrieve Player's match list based on player uuid
   *
   * @link https://developer.riotgames.com/apis#val-match-v1/GET_getMatchlist
   *
   * @return {Promise<IValorantMatchListDTO>}
   * @memberof ValorantApiService
   */
  @Trace()
  public async fetchValorantMatchList(region: string, puuid: string): Promise<IValorantMatchListDTO> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region)}/match/v1/matchlists/by-puuid/${puuid}`);

    const result = <AxiosResponse<IValorantMatchListDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchValorantMatchList");
    });

    return result?.data;
  }

  /**
   * Retrieve match based on matchId
   *
   * @link https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
   *
   * @return {Promise<IValorantMatchDTO>}
   * @memberof ValorantApiService
   */
  @Trace()
  public async fetchValorantMatch(region: string, matchId: string): Promise<IValorantMatchDTO> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region)}/match/v1/matches/${matchId}`);

    const result = <AxiosResponse<IValorantMatchDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchValorantMatch");
    });

    return result?.data;
  }

  /**
   * Retrieve a queues match list of uuids from the last 10 minutes
   * Generally only competitive queue will be used
   *
   * @link https://developer.riotgames.com/apis#val-match-v1/GET_getRecent
   *
   * @return {Promise<IValorantRecentMatchesDTO>}
   * @memberof ValorantApiService
   */
  @Trace()
  public async fetchValorantRecentMatchesByQueue(
    region: string,
    queue: ValorantQueueType
  ): Promise<IValorantRecentMatchesDTO> {
    const requestUrl = encodeURI(`${this.getRiotRootUrl(region)}/match/v1/recent-matches/by-queue/${queue}`);

    const result = <AxiosResponse<IValorantRecentMatchesDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchValorantRecentMatchesByQueue");
    });

    return result?.data;
  }

  /**
   *  Retrieve the player's active shard (NA, EU, etc..)
   *
   * @link https://developer.riotgames.com/apis#account-v1/GET_getActiveShard
   *
   * @param puuid The Player's id from Riot
   * @returns  {Promise<IValorantActiveShardDTO>}
   */
  @Trace()
  public async fetchActiveShard(puuid: string): Promise<IValorantActiveShardDTO> {
    const requestUrl = encodeURI(
      `https://americas.api.riotgames.com/riot/account/v1/active-shards/by-game/val/by-puuid/${puuid}`
    );

    const result = <AxiosResponse<IValorantActiveShardDTO>>await this.axios.get(requestUrl).catch((error) => {
      this.handleRiotError(error, "fetchActiveShard");
    });

    return result?.data;
  }

  /**
   * Handle riot specific errors.
   * @param error
   * @param warnMessage
   */
  @Trace()
  private handleRiotError(error: any, request: string, warnMessage?: string): void {
    let riotError = error;
    let message = error?.message?.message || error?.message;

    // if data wasn't found, don't freak out.
    // just return, this happens and we don't want to terminate the async job
    if (error?.response?.status === 404) {
      this.logger?.warn({ request, info: warnMessage }, `Unable to find resource`);

      return;
    }

    if (!message) {
      try {
        riotError = JSON.parse(error.error);
        message = riotError?.status?.message;
      } catch (parseError) {
        message = parseError;
      }
    }

    throw Failure.InternalServerError(message, riotError);
  }

  /**
   * Return the root url for the api request
   *
   * @param region E.G. "na1"
   * @param service  E.G. "val"
   * @returns string url
   */
  private getRiotRootUrl(region: string, service: ValorantService = ValorantService.VAL): string {
    return `https://${region}.api.riotgames.com/${service}`;
  }
}
