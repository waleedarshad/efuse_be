import { Failure } from "@efuse/contracts";
import { IValorantStats } from "../../backend/interfaces/valorant/valorant-stats";
import { ValorantStats, ValorantStatsModel } from "../../backend/models/valorant/valorant-stats";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

@Service("valorant-stats.service")
export class ValorantStatsService extends BaseModelService<IValorantStats> {
  private readonly model: ValorantStatsModel<IValorantStats> = ValorantStats;

  constructor() {
    super(ValorantStats);
  }

  @Trace()
  public async deleteByOwner(owner: string): Promise<IValorantStats | null> {
    try {
      if (!owner) {
        throw Failure.BadRequest("Unable to delete item, missing 'owner'");
      }

      const item: IValorantStats | null = await this.model.findOneAndDelete({ user: owner }).exec();

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
