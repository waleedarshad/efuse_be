import { Failure } from "@efuse/contracts";
import { QueryOptions, FilterQuery } from "mongoose";
import { IValorantActDTO } from "../../backend/interfaces/valorant/valorant-act-dto";

import { IValorantContentDTO } from "../../backend/interfaces/valorant/valorant-content-dto";
import { ValorantContent, ValorantContentModel } from "../../backend/models/valorant/valorant-content";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

export const competitiveTierRankNames = {
  3: "Iron 1",
  4: "Iron 2",
  5: "Iron 3",
  6: "Bronze 1",
  7: "Bronze 2",
  8: "Bronze 3",
  9: "Silver 1",
  10: "Silver 2",
  11: "Silver 3",
  12: "Gold 1",
  13: "Gold 2",
  14: "Gold 3",
  15: "Plat 1",
  16: "Plat 2",
  17: "Plat 3",
  18: "Diamond 1",
  19: "Diamond 2",
  20: "Diamond 3",
  21: "Immortal",
  22: "Immortal",
  23: "Immortal",
  24: "Radiant"
};

@Service("valorant-content.service")
export class ValorantContentService extends BaseService {
  private readonly model: ValorantContentModel<IValorantContentDTO> = ValorantContent;

  /**
   * Creates or replaces an existing Valorant Content item based on region
   *
   * @param {IValorantContentDTO} item The content object
   *
   * @returns {Promise<IValorantContentDTO>} The created content item
   *
   * @memberof ValorantContentService
   */
  @Trace()
  public async createOrReplace(item: IValorantContentDTO): Promise<IValorantContentDTO> {
    try {
      if (!item.region) {
        throw Failure.BadRequest("Missing 'region' on item");
      }
      await this.delete(item.region);

      const result: IValorantContentDTO = await this.model.create(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async find(filter: FilterQuery<IValorantContentDTO>): Promise<IValorantContentDTO[]>;
  public async find(filter: FilterQuery<IValorantContentDTO>, projection: any): Promise<IValorantContentDTO[]>;
  public async find(
    filter: FilterQuery<IValorantContentDTO>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantContentDTO[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IValorantContentDTO>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantContentDTO[]> {
    const content = await this.model.find(filter, projection, options);
    return content;
  }

  public async findOne(filter: FilterQuery<IValorantContentDTO>): Promise<IValorantContentDTO | null>;
  public async findOne(filter: FilterQuery<IValorantContentDTO>, projection: any): Promise<IValorantContentDTO | null>;
  public async findOne(
    filter: FilterQuery<IValorantContentDTO>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantContentDTO | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IValorantContentDTO>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantContentDTO | null> {
    const content = await this.model.findOne(filter, projection, options);
    return content;
  }

  @Trace()
  public async delete(region: string): Promise<IValorantContentDTO | null> {
    try {
      if (!region) {
        throw Failure.BadRequest("Unable to create item, missing 'region'");
      }

      const item: IValorantContentDTO | null = await this.model.findOneAndDelete({ region }).lean();

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async getActiveEpisode(region: string): Promise<IValorantActDTO> {
    const content = await this.findOne({ region }, null, { lean: true });
    const activeEpisode = content?.acts.find((act) => act.type === "episode" && act.isActive);

    return <IValorantActDTO>activeEpisode;
  }

  @Trace()
  public async getActiveAct(region: string): Promise<IValorantActDTO> {
    const content = await this.findOne({ region }, null, { lean: true });
    const activeAct = content?.acts.find((act) => act.type === "act" && act.isActive);

    return <IValorantActDTO>activeAct;
  }
}
