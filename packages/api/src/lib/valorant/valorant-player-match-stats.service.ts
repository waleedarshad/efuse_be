import { QueryOptions, FilterQuery } from "mongoose";

import { IValorantPlayerMatchStats } from "../../backend/interfaces/valorant/valorant-player-match-stats";
import {
  ValorantPlayerMatchStats,
  ValorantPlayerMatchStatsModel
} from "../../backend/models/valorant/valorant-player-match-stats";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("valorant-player-match-stats.service")
export class ValorantPlayerMatchStatsService extends BaseService {
  private readonly model: ValorantPlayerMatchStatsModel<IValorantPlayerMatchStats> = ValorantPlayerMatchStats;

  /**
   * Creates a Valorant match
   *
   * @param {IValorantPlayerMatchStats} item The content object
   *
   * @returns {Promise<IValorantPlayerMatchStats>} The created content item
   *
   * @memberof ValorantMatchService
   */
  @Trace()
  public async create(
    item: Partial<IValorantPlayerMatchStats> | IValorantPlayerMatchStats
  ): Promise<IValorantPlayerMatchStats> {
    try {
      const doc = item;
      const result: IValorantPlayerMatchStats = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createMany(
    items: Partial<IValorantPlayerMatchStats>[] | IValorantPlayerMatchStats[]
  ): Promise<IValorantPlayerMatchStats[]> {
    try {
      const result: IValorantPlayerMatchStats[] = await this.model.insertMany(items);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async exists(filter: FilterQuery<IValorantPlayerMatchStats>): Promise<boolean> {
    const match = await this.model.exists(filter);

    return match;
  }

  public async find(filter: FilterQuery<IValorantPlayerMatchStats>): Promise<IValorantPlayerMatchStats[]>;
  public async find(
    filter: FilterQuery<IValorantPlayerMatchStats>,
    projection: any
  ): Promise<IValorantPlayerMatchStats[]>;
  public async find(
    filter: FilterQuery<IValorantPlayerMatchStats>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantPlayerMatchStats[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IValorantPlayerMatchStats>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantPlayerMatchStats[]> {
    const match = await this.model.find(filter, projection, options).exec();
    return match;
  }

  public async findOne(filter: FilterQuery<IValorantPlayerMatchStats>): Promise<IValorantPlayerMatchStats | null>;
  public async findOne(
    filter: FilterQuery<IValorantPlayerMatchStats>,
    projection: any
  ): Promise<IValorantPlayerMatchStats | null>;
  public async findOne(
    filter: FilterQuery<IValorantPlayerMatchStats>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantPlayerMatchStats | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IValorantPlayerMatchStats>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantPlayerMatchStats | null> {
    const match = await this.model.findOne(filter, projection, options).exec();
    return match;
  }
}
