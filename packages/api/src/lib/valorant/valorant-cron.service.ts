import { Failure } from "@efuse/contracts";
import { cloneDeep } from "lodash";
import { IValorantAgentDTO } from "../../backend/interfaces/valorant/valorant-agent-dto";
import { IValorantApiVersion } from "../../backend/interfaces/valorant/valorant-api-version";
import { IValorantLeaderboardPlayerDTO } from "../../backend/interfaces/valorant/valorant-leaderboard-player-dto";
import { ValorantAgent, ValorantAgentModel } from "../../backend/models/valorant/valorant-agent";
import { ValorantApiVersionModel, ValorantApiVersion } from "../../backend/models/valorant/valorant-api-version";
import { S3ContentTypes, S3Service } from "../aws/s3.service";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { VALORANT } from "../metrics";
import { ValorantApiDataService } from "./valorant-api-data.service";
import { ValorantApiQueueService } from "./valorant-api-queue.service";
import { ValorantContentService } from "./valorant-content.service";
import { ValorantLeaderboardPlayerService } from "./valorant-leaderboard.service";
import { RiotProfileService } from "../riot/riot-profile/riot-profile.service";
import { ValorantRegions } from "./valorant-regions-enum";
import { ValorantAsyncService } from "./valorant-async.service";
import { ValorantQueueType } from "./valorant-queue-type-enum";

const ROOT_BUCKET = "static/valorant";
const VALORANT_CRON_SERVICE = "valorant-cron.service";

const VALORANT_REGIONS = Object.values(ValorantRegions);

@Service(VALORANT_CRON_SERVICE)
export class ValorantCronService extends BaseService {
  private valorantApiVersionModel: ValorantApiVersionModel<IValorantApiVersion> = ValorantApiVersion;
  private valorantAgentModel: ValorantAgentModel<IValorantAgentDTO> = ValorantAgent;

  private riotProfileService: RiotProfileService;
  private s3Service: S3Service;
  private valorantApiDataService: ValorantApiDataService;
  private valorantApiQueueService: ValorantApiQueueService;
  private valorantLeaderboardPlayerService: ValorantLeaderboardPlayerService;
  private valorantContentService: ValorantContentService;
  private valorantAsyncService = new ValorantAsyncService();

  public constructor() {
    super();

    this.riotProfileService = new RiotProfileService();
    this.s3Service = new S3Service();
    this.valorantApiDataService = new ValorantApiDataService();
    this.valorantApiQueueService = new ValorantApiQueueService();
    this.valorantLeaderboardPlayerService = new ValorantLeaderboardPlayerService();
    this.valorantContentService = new ValorantContentService();
    this.valorantAsyncService = new ValorantAsyncService();
  }

  @Trace({ name: "FetchAndSaveUserMatchlistCron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveUserMatchlistCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const asyncService = new ValorantAsyncService();

    try {
      const valorantProfiles = await cronService.riotProfileService.find(
        {
          valorantProfile: { $exists: true },
          "valorantProfile.shard": { $exists: true }
        },
        { valorantProfile: true },
        { lean: true }
      );

      if (!valorantProfiles || valorantProfiles.length === 0) {
        cronService.logger?.info(`No Valorant Profiles to fetch matches for.`);
        return;
      }

      cronService.logger?.info(
        `Begin fetching user matchlist and pre-aggregating stats for ${valorantProfiles.length} valorant player(s)`
      );

      // Not using promise.all or allSettled as it could possibly cause memory issues because they will hold all results
      for (const profile of valorantProfiles) {
        asyncService
          .fetchMatchlistAndPreaggregateUserStats(
            <string>profile?.valorantProfile?.shard,
            <string>profile?.valorantProfile?.puuid
          )
          .catch((error: any) => {
            cronService.logger?.error(
              error,
              `Could not queue matchlist fetching for puuid: ${profile.valorantProfile.puuid} - ${profile.valorantProfile.shard}`
            );
          });
      }

      cronService.logger?.info(`Successfully queued match fetching and pre-aggregating stats for Valorant players`);
    } catch (error) {
      throw cronService.handleError(error, "Unable to refresh valorant stats cron");
    }
  }

  // North america valorant
  @Trace({ name: "FetchAndSaveValorantRecentMatchesNACron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveValorantRecentMatchesNACron(): Promise<void> {
    const cronService = new ValorantCronService();
    const region = ValorantRegions.NA;
    const valorantMetric = VALORANT.FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_NA_CRON;

    await cronService.queueRecentValorantMatchesForFetchingAndSaving(region, valorantMetric);
  }

  // LATAM valorant
  @Trace({ name: "FetchAndSaveValorantRecentMatchesLatamCron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveValorantRecentMatchesLatamCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const region = ValorantRegions.LATAM;
    const valorantMetric = VALORANT.FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_LATAM_CRON;

    await cronService.queueRecentValorantMatchesForFetchingAndSaving(region, valorantMetric);
  }

  // Korea valorant
  @Trace({ name: "FetchAndSaveValorantRecentMatchesKRCron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveValorantRecentMatchesKRCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const region = ValorantRegions.KR;
    const valorantMetric = VALORANT.FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_KR_CRON;

    await cronService.queueRecentValorantMatchesForFetchingAndSaving(region, valorantMetric);
  }

  // Europe valorant
  @Trace({ name: "FetchAndSaveValorantRecentMatchesEUCron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveValorantRecentMatchesEUCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const region = ValorantRegions.EU;
    const valorantMetric = VALORANT.FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_EU_CRON;

    await cronService.queueRecentValorantMatchesForFetchingAndSaving(region, valorantMetric);
  }

  // Brazil valorant
  @Trace({ name: "FetchAndSaveValorantRecentMatchesBRCron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveValorantRecentMatchesBRCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const region = ValorantRegions.BR;
    const valorantMetric = VALORANT.FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_BR_CRON;

    await cronService.queueRecentValorantMatchesForFetchingAndSaving(region, valorantMetric);
  }

  // AP valorant
  @Trace({ name: "FetchAndSaveValorantRecentMatchesAPCron", service: VALORANT_CRON_SERVICE })
  public static async FetchAndSaveValorantRecentMatchesAPCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const region = ValorantRegions.AP;
    const valorantMetric = VALORANT.FETCH_AND_SAVE_VALORANT_RECENT_MATCHES_AP_CRON;

    await cronService.queueRecentValorantMatchesForFetchingAndSaving(region, valorantMetric);
  }

  @Trace({ name: "FetchValorantDataCron", service: VALORANT_CRON_SERVICE })
  public static async FetchValorantDataCron(): Promise<void> {
    const service = new ValorantCronService();

    service.recordMetric(VALORANT.FETCH_VALORANT_DATA_CRON.START);

    try {
      const currentSavedVersion = await service.valorantApiVersionModel.findOne();
      const availableVersion = await service.valorantApiDataService.getAvailableApiVersionData();

      if (!availableVersion) {
        throw Failure.InternalServerError("Unable to retrieve Valorant api version data.");
      }

      if (currentSavedVersion && availableVersion.version === currentSavedVersion.version) {
        service.logger?.info("Valorant has not updated content. Exiting.");

        return;
      }

      const agentData = await service.valorantApiDataService.getAvailableAgentData();

      if (!agentData) {
        throw Failure.InternalServerError("Unable to retrieve Valorant agent data.");
      }

      const processedAgents = await Promise.all(
        agentData.map((agent: IValorantAgentDTO) => service.processAgentData(agent))
      );

      if (!processedAgents || processedAgents.length === 0) {
        throw Failure.InternalServerError("Failed to process agents");
      }
      if (currentSavedVersion) {
        currentSavedVersion.version = availableVersion.version;
        currentSavedVersion.branch = availableVersion.branch;
        currentSavedVersion.buildVersion = availableVersion.buildVersion;
        currentSavedVersion.buildDate = availableVersion.buildDate;

        await currentSavedVersion.save();
      } else {
        await service.valorantApiVersionModel.create(availableVersion);
      }

      service.recordMetric(VALORANT.FETCH_VALORANT_DATA_CRON.FINISH);
    } catch (error) {
      service.recordMetric(VALORANT.FETCH_VALORANT_DATA_CRON.ERROR);
      throw service.handleError(error, "Error fetching Valorant Data (cron)");
    }
  }

  @Trace({ name: "FetchValorantContentDataCron", service: VALORANT_CRON_SERVICE })
  public static async FetchValorantContentDataCron(): Promise<void> {
    const service = new ValorantCronService();
    for (const region of VALORANT_REGIONS) {
      const content = await service.valorantApiQueueService.fetchValorantContent(region);
      if (!content) {
        throw Failure.InternalServerError(`Error fetching Valorant content for region ${region} (cron)`);
      }

      // save the content in the database for future reference without querying
      content.region = region;

      await service.valorantContentService.createOrReplace(content);
    }
  }

  @Trace({ name: "ProcessValorantLeaderboards", service: VALORANT_CRON_SERVICE })
  public static async ProcessValorantLeaderboardsCron(): Promise<void> {
    const service = new ValorantCronService();

    service.recordMetric(VALORANT.FETCH_VALORANT_LEADERBOARD_CRON.START);

    try {
      await Promise.allSettled(VALORANT_REGIONS.map((region) => service.fetchValorantLeaderboard(region)));

      service.recordMetric(VALORANT.FETCH_VALORANT_LEADERBOARD_CRON.FINISH);
    } catch (error) {
      service.recordMetric(VALORANT.FETCH_VALORANT_LEADERBOARD_CRON.ERROR);
      throw service.handleError(error, "Error fetching Valorant Leaderboard (cron)");
    }
  }

  @Trace({ name: "RefreshAllValorantStatsCron", service: VALORANT_CRON_SERVICE })
  public static async RefreshAllValorantStatsCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const asyncService = new ValorantAsyncService();

    try {
      const valorantProfiles = await cronService.riotProfileService.find(
        {
          valorantProfile: { $exists: true },
          "valorantProfile.shard": { $exists: true }
        },
        {},
        { lean: true }
      );

      if (!valorantProfiles || valorantProfiles.length === 0) {
        cronService.logger?.info(`No Valorant Profiles to refresh stats for.`);
        return;
      }

      cronService.logger?.info(`Queuing stats refresh for ${valorantProfiles.length} users`);

      // Not using promise.all or allSettled as it could possibly cause memory issues because they will hold all results
      for (const profile of valorantProfiles) {
        asyncService
          .retrieveAndSaveValorantUserStatsForPlayer(
            <string>profile.owner,
            <string>profile.valorantProfile.shard,
            <string>profile.valorantProfile.gameName,
            <string>profile.valorantProfile.tagLine,
            <string>profile.valorantProfile.puuid
          )
          .catch((error: any) => {
            cronService.logger?.error(
              error,
              `Could not queue matchlist fetching for puuid: ${profile.valorantProfile.puuid} - ${profile.valorantProfile.shard}`
            );
          });
      }

      cronService.logger?.info(`Successfully queued retrieving and saving user stats for player`);
    } catch (error) {
      throw cronService.handleError(error, "Unable to refresh valorant stats cron");
    }
  }

  @Trace({ name: "UpdateValorantNameCron", service: VALORANT_CRON_SERVICE })
  public static async UpdateValorantGameNamesCron(): Promise<void> {
    const cronService = new ValorantCronService();
    const asyncService = new ValorantAsyncService();

    try {
      const valorantProfiles = await cronService.riotProfileService.find(
        {
          valorantProfile: { $exists: true }
        },
        { _id: 1, valorantProfile: 1 }
      );

      if (!valorantProfiles || valorantProfiles.length === 0) {
        cronService.logger?.info(`No valorant profiles found for name updates.`);
        return;
      }

      await Promise.allSettled(
        valorantProfiles.map((profile) => asyncService.updateValorantGameNameForPlayer(profile))
      );

      cronService.logger?.info(`Successfully queued Valorant Players for name update`);
    } catch (error) {
      throw cronService.handleError(error, "Unable to refresh valorant stats cron");
    }
  }

  @Trace()
  private async queueRecentValorantMatchesForFetchingAndSaving(
    region: ValorantRegions,
    valorantMetric: {
      START: string;
      FINISH: string;
      ERROR: string;
    }
  ): Promise<void> {
    const queue = ValorantQueueType.COMPETITIVE;

    this.recordMetric(valorantMetric.START);

    try {
      const recentMatchIds = await this.valorantApiQueueService.fetchValorantRecentMatches(region, queue);
      if (!recentMatchIds || recentMatchIds.matchIds.length === 0) {
        throw Failure.NotFound(`${region} - ${queue}: No recent matchids were found`);
      }

      this.logger?.info(
        `Begin queuing ${recentMatchIds.matchIds.length} recent Valorant matches for ${region} - ${queue} (cron)`
      );

      const { matchIds } = recentMatchIds;

      // do not use Promise.all or allSettled.
      // those will hold returns and eventually node will run out of memory
      matchIds.forEach((id) =>
        this.valorantAsyncService.addFetchAndSaveValorantRecentMatchToQueue(id, region).catch((error: any) => {
          this.logger?.error(error, `Could not queue match: ${id} - ${region}`);
        })
      );
      this.logger?.info(`Finished queuing recent Valorant matches for ${region} - ${queue} (cron)`);
    } catch (error) {
      this.logger?.error({ region, queue });
      throw this.handleError(error, "Function: fetchRecentMatchIdsAndQueueForFetchingAndSaving");
    }
  }

  // TODO: look into moving private functions to valorant async service or valorant service

  @Trace()
  private async fetchValorantLeaderboard(region: string): Promise<void> {
    const activeAct = await this.valorantContentService.getActiveAct(region);
    if (!activeAct) {
      throw Failure.InternalServerError("Error fetching Valorant active act for leaderboard (cron)");
    }

    if (!activeAct) {
      throw Failure.InternalServerError("Error identifying active act for leaderboard (cron)");
    }

    // get all the leaderboard players
    const leaderboardPlayers = await this.getValorantLeaderboardPlayersForRegion(region, activeAct.id);

    if (!leaderboardPlayers) {
      throw Failure.InternalServerError("Unable to retrieve leaderboard players");
    }

    // delete existing players before saving new ones for the same region/act
    await this.valorantLeaderboardPlayerService.delete({ region });

    // save all the new ones
    const createdLeaderboardPlayers = await this.valorantLeaderboardPlayerService.createMany(leaderboardPlayers);

    if (!createdLeaderboardPlayers) {
      throw Failure.InternalServerError("Unable to save leaderboard players", {
        leaderboardPlayers,
        createdLeaderboardPlayers
      });
    }
  }

  @Trace()
  private async getValorantLeaderboardPlayersForRegion(
    region: string,
    actId: string
  ): Promise<IValorantLeaderboardPlayerDTO[]> {
    const allLeaderboardPlayers: IValorantLeaderboardPlayerDTO[] = [];
    let count = 0;
    let totalPlayers = 1;

    // make calls to get valorant leaderboard until we have them all.
    while (count < totalPlayers) {
      const leaderboard = await this.valorantApiQueueService.fetchValorantLeaderboard(region, actId, count);

      if (leaderboard?.players) {
        // set the act and region on the player
        const leaderboardPlayers = leaderboard.players.map((player) => {
          const localPlayer = cloneDeep(player);

          localPlayer.actId = leaderboard.actId;
          localPlayer.region = region;

          return localPlayer;
        });

        allLeaderboardPlayers.push(...leaderboardPlayers);

        count += leaderboard.players.length;
        totalPlayers = leaderboard.totalPlayers;
      }
    }

    // valorant will anonymize players if they request it. There's no reason to store data if we don't know the user
    // so just filter those out by checking the id length
    return allLeaderboardPlayers.filter((player) => player?.puuid?.length > 0);
  }

  /**
   * Takes the agent and downloads the images, uploads them to S3 and returns a new agent with updated urls
   * @param agent The agent to process
   */
  @Trace()
  private async processAgentData(agent: IValorantAgentDTO): Promise<IValorantAgentDTO> {
    this.recordMetric(VALORANT.PROCESS_AGENT_DATA.START);

    const rootBucket = `${ROOT_BUCKET}/agents/${agent.uuid}`;
    const displayIconS3Url = `${rootBucket}/displayicon.png`;
    const displayIconSmallS3Url = `${rootBucket}/displayiconsmall.png`;
    const bustPortraitS3Url = `${rootBucket}/bustportrait.png`;
    const fullPortraitS3Url = `${rootBucket}/fullportrait.png`;
    try {
      let displayIconSuccess = true;
      if (agent.displayIcon) {
        displayIconSuccess = await this.s3Service.downloadMediaAndUploadToS3(
          agent.displayIcon,
          displayIconS3Url,
          S3ContentTypes.PNG
        );
      }

      let displayIconSmallSuccess = true;
      if (agent.displayIconSmall) {
        displayIconSmallSuccess = await this.s3Service.downloadMediaAndUploadToS3(
          agent.displayIconSmall,
          displayIconSmallS3Url,
          S3ContentTypes.PNG
        );
      }

      let bustPortraitSuccess = true;
      if (agent.bustPortrait) {
        bustPortraitSuccess = await this.s3Service.downloadMediaAndUploadToS3(
          agent.bustPortrait,
          bustPortraitS3Url,
          S3ContentTypes.PNG
        );
      }

      let fullPortraitSuccess = true;
      if (agent.fullPortrait) {
        fullPortraitSuccess = await this.s3Service.downloadMediaAndUploadToS3(
          agent.fullPortrait,
          fullPortraitS3Url,
          S3ContentTypes.PNG
        );
      }

      if (!displayIconSuccess || !displayIconSmallSuccess || !bustPortraitSuccess || !fullPortraitSuccess) {
        throw Failure.InternalServerError("Failed to move Valorant images to S3", agent);
      }

      const existingAgent = await this.valorantAgentModel.findOne({ uuid: agent.uuid });

      let savedAgent: IValorantAgentDTO;
      if (existingAgent) {
        existingAgent.displayIcon = displayIconS3Url;
        existingAgent.displayIconSmall = displayIconSmallS3Url;
        existingAgent.bustPortrait = bustPortraitS3Url;
        existingAgent.fullPortrait = fullPortraitS3Url;

        savedAgent = await existingAgent.save();
      } else {
        const newAgent = cloneDeep(agent);
        newAgent.displayIcon = displayIconS3Url;
        newAgent.displayIconSmall = displayIconSmallS3Url;
        newAgent.bustPortrait = bustPortraitS3Url;
        newAgent.fullPortrait = fullPortraitS3Url;

        savedAgent = await this.valorantAgentModel.create(newAgent);
      }

      this.recordMetric(VALORANT.PROCESS_AGENT_DATA.FINISH);
      return savedAgent;
    } catch (error) {
      this.recordMetric(VALORANT.PROCESS_AGENT_DATA.ERROR);
      throw this.handleError(error, "Error processing downloaded agent data");
    }
  }
}
