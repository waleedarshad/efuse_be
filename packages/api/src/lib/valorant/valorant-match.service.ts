import { Failure } from "@efuse/contracts";

import { IValorantMatchDTO } from "../../backend/interfaces/valorant/valorant-match-dto";
import { ValorantMatch, ValorantMatchModel } from "../../backend/models/valorant/valorant-match";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

@Service("valorant-match.service")
export class ValorantMatchService extends BaseModelService<IValorantMatchDTO> {
  private readonly model: ValorantMatchModel<IValorantMatchDTO> = ValorantMatch;

  constructor() {
    super(ValorantMatch);
  }

  @Trace()
  public async deleteByRegion(region: string): Promise<IValorantMatchDTO | null> {
    try {
      if (!region) {
        throw Failure.BadRequest("Unable to create item, missing 'region'");
      }

      const item: IValorantMatchDTO | null = await this.model.findOneAndDelete({ region }).exec();

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
