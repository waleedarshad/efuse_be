export enum ValorantQueueType {
  COMPETITIVE = "competitive",
  SPIKE_RUSH = "spikerush",
  TOURNAMENT_MODE = "tournamentmode",
  UNRATED = "unrated"
}
