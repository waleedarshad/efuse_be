export enum RiotRegion {
  AMERICAS = "americas",
  EUROPE = "europe",
  ASIA = "asia"
}
