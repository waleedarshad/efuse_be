import axios, { AxiosResponse } from "axios";
import { IValorantAgentDTO } from "../../backend/interfaces/valorant/valorant-agent-dto";
import { IValorantApiVersion } from "../../backend/interfaces/valorant/valorant-api-version";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

/**
 * The ValorantApiDataService is meant to only interact with the third party api
 * located at https://dash.valorant-api.com/
 *
 * This API is the easiest way to retrieve art from the game.
 * Since Valorant doesn't provide and official way to pull art other than
 * pulling directly from the game files, this allows us to consistently update data
 * via cron.
 */
@Service("valorant-api-data.service")
export class ValorantApiDataService extends BaseService {
  @Trace()
  public getAvailableApiVersionData(): Promise<IValorantApiVersion> {
    try {
      const apiVersion = axios
        .get("https://valorant-api.com/v1/version")
        .then((value: AxiosResponse) => (<{ status: number; data: IValorantApiVersion }>value?.data)?.data);
      return apiVersion;
    } catch (error) {
      throw this.handleError(error, "Error fetching available Valorant api version");
    }
  }

  @Trace()
  public getAvailableAgentData(): Promise<IValorantAgentDTO[]> {
    try {
      const agentData = axios
        .get("https://valorant-api.com/v1/agents")
        .then((value: AxiosResponse) => (<{ status: number; data: IValorantAgentDTO[] }>value?.data)?.data);
      return agentData;
    } catch (error) {
      throw this.handleError(error, "Error fetching available Valorant agent data");
    }
  }
}
