export enum ValorantRegions {
  NA = "na",
  AP = "ap",
  BR = "br",
  EU = "eu",
  KR = "kr",
  LATAM = "latam"
}
