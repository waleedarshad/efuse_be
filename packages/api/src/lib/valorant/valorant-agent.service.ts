import { IValorantAgentDTO } from "../../backend/interfaces/valorant/valorant-agent-dto";
import { ValorantAgent } from "../../backend/models/valorant/valorant-agent";
import { BaseModelService } from "../base";
import { Service } from "../decorators";

@Service("valorant-agent.service")
export class ValorantAgentService extends BaseModelService<IValorantAgentDTO> {
  constructor() {
    super(ValorantAgent);
  }
}
