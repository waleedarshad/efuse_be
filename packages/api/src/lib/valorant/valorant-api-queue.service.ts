import { Queue, Job } from "bull";
import { Failure } from "@efuse/contracts";
import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import * as Config from "../../async/config";
import { ValorantApiService } from "./valorant-api.service";
import { IValorantContentDTO } from "../../backend/interfaces/valorant/valorant-content-dto";
import { IValorantLeaderboardDTO } from "../../backend/interfaces/valorant/valorant-leaderboard-dto";
import { IValorantMatchListDTO } from "../../backend/interfaces/valorant/valorant-match-list-dto";
import { IValorantMatchDTO } from "../../backend/interfaces/valorant/valorant-match-dto";
import { IValorantActiveShardDTO } from "../../backend/interfaces/valorant/valorant-active-shard-dto";
import { ValorantQueueType } from "./valorant-queue-type-enum";
import { ValorantRegions } from "./valorant-regions-enum";
import { IValorantRecentMatchesDTO } from "../../backend/interfaces/valorant/valorant-recent-matches-dto";
import { ValorantAsyncService } from "./valorant-async.service";
import { IRiotAccountDTO } from "../../backend/interfaces/riot/riot-account-dto";
import { RiotRegion } from "./riot-region.enum";

// Valorant base rate limiter is 30,000 jobs per 10 minutes or ~ (50/s)
// Riot is very particular about their limits and we kept hitting the limits
// so we are dropping them down by 2 to try to avoid this
const VALORANT_API_LIMITS = {
  ACCOUNT_INFO: {
    max: 98,
    duration: 6000 // 6 seconds
  },
  BASE: {
    max: 48,
    duration: 1000 // 1 second
  },
  CONTENT: {
    max: 58,
    duration: 60000 // 60 seconds
  },
  LEADERBOARD: {
    max: 8,
    duration: 10000 // 10 seconds
  },
  MATCHLIST: {
    max: 118,
    duration: 60000 // 60 seconds
  },
  MATCHES: {
    max: 58,
    duration: 60000 // 60 seconds
  },
  RECENT_MATCHES: {
    max: 58,
    duration: 60000 // 60 seconds
  },
  SHARD: {
    max: 19998,
    duration: 10000 // 10 seconds
  }
};

const ACCOUNT_INFO_QUEUE_NAME = "valorant-riot-account-info-api-calls";
const BASE_QUEUE_NAME = "valorant-api-calls";
const CONTENT_QUEUE_NAME = "valorant-content-api-calls";
const LEADERBOARD_QUEUE_NAME = "valorant-leaderboard-api-calls";
const MATCHLIST_QUEUE_NAME = "valorant-matchlist-api-calls";
const MATCH_QUEUE_NAME = "valorant-match-api-calls";
const RECENT_MATCHES_QUEUE_NAME = "valorant-recent-matches-api-calls";
const SHARD_QUEUE_NAME = "valorant-shard-api-calls";
const VALORANT_API_QUEUE_SERVICE = "valorant-api-queue.service";

const enum QueueMethods {
  fetchRiotAccountInfo = "fetchRiotAccountInfoQueued",
  fetchValorantContentQueued = "fetchValorantContentQueued",
  fetchValorantLeaderboardQueued = "fetchValorantLeaderboardQueued",
  fetchValorantMatchlistQueued = "fetchValorantMatchlistQueued",
  fetchValorantMatchQueued = "fetchValorantMatchQueued",
  fetchValorantRecentMatchesQueued = "fetchValorantRecentMatchesQueued",
  fetchActiveShard = "fetchActiveShard"
}

/**
 * Riot is mean and have different rate limits for different endpoints. See this page for details: https://developer.riotgames.com/app/497605/info
 * This unfortunately complicates the queue process quite a bit. I have tried to add comments whenever something is funky.
 *
 * TLDR: We have a queue that is based on region. This queue executes all requests on the other queues.
 *
 * But yeah, "Here be dragons."
 */
@Service(VALORANT_API_QUEUE_SERVICE)
export class ValorantApiQueueService extends BaseService {
  // this is the base queue and is restricted by region.
  private static riotAccountInfoQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantBaseQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantLeaderboardQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantContentQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantMatchlistQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantMatchQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantRecentMatchQueue: Map<string, Queue> = new Map<string, Queue>();
  private static valorantShardQueue: Map<string, Queue> = new Map<string, Queue>();

  /**
   * Use `ValorantApi` getter not this member
   */
  private static $valorantApi: ValorantApiService;
  private static get valorantApi(): ValorantApiService {
    if (!ValorantApiQueueService.$valorantApi) {
      ValorantApiQueueService.$valorantApi = new ValorantApiService();
    }

    return ValorantApiQueueService.$valorantApi;
  }

  /**
   * Once the base queue has allowed the job to be executed we need to add that job to the appropriate queue so it actually executes the request
   */
  @Trace({ name: "handleBaseQueueJob", service: VALORANT_API_QUEUE_SERVICE })
  private static async handleBaseQueueJob(
    job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>
  ): Promise<any> {
    const { funcName, params } = job.data;

    let localJob: Job;
    switch (funcName) {
      case QueueMethods.fetchRiotAccountInfo:
        // add to the content queue
        localJob = await ValorantApiQueueService.RiotAccountInfoQueue(<RiotRegion>params.region).add(job.data);
        break;
      case QueueMethods.fetchValorantContentQueued:
        // add to the content queue
        localJob = await ValorantApiQueueService.ValorantContentQueue(params.region).add(job.data, { priority: 5 });
        break;
      case QueueMethods.fetchValorantLeaderboardQueued:
        // add to the leaderboard queue
        localJob = await ValorantApiQueueService.ValorantLeaderboardQueue(params.region).add(job.data, { priority: 5 });
        break;
      case QueueMethods.fetchValorantMatchlistQueued:
        // add to the matchlist queue
        localJob = await ValorantApiQueueService.ValorantMatchlistQueue(params.region).add(job.data, { priority: 1 });
        break;
      case QueueMethods.fetchValorantMatchQueued:
        // add to the match queue
        localJob = await ValorantApiQueueService.ValorantMatchQueue(params.region).add(job.data, { priority: 1 });
        break;
      case QueueMethods.fetchValorantRecentMatchesQueued:
        // add to the recent match queue
        localJob = await ValorantApiQueueService.ValorantRecentMatchesQueue(params.region).add(job.data, {
          priority: 3
        });
        break;
      case QueueMethods.fetchActiveShard:
        // add to the shard queue
        localJob = await ValorantApiQueueService.ValorantShardQueue().add(job.data, { priority: 1 });
        break;
      default:
        throw Failure.InternalServerError(`No queue service function named ${<string>funcName}`, {
          queueName: BASE_QUEUE_NAME,
          data: job?.data
        });
    }

    const result = await localJob.finished();

    return result;
  }

  /**
   * The ValorantQueue is a rate limited implementation of the Bull Queue used to execute ALL queries to the Riot Valorant API.
   * When one of the public functions on the `ValorantApiQueueService` are called this queue is used to execute the
   * underlying api call on the queue that handles requests for that endpoint because Riot likes to rate limit each endpoint differently
   *
   * Note: Riot rate limits based on region, thus why we have separate queues
   *
   * @memberof ValorantApiQueueService
   */
  @Trace({ name: "ValorantBaseQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantBaseQueue(regionName: string): Queue {
    const queueName = `${BASE_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.valorantBaseQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantBaseQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantBaseQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.BASE,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantBaseQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) =>
            ValorantApiQueueService.handleBaseQueueJob(job)
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantBaseQueue[queueName];
  }

  @Trace({ name: "RiotAccountInfoQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static RiotAccountInfoQueue(regionName: RiotRegion): Queue {
    const queueName = `${ACCOUNT_INFO_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.riotAccountInfoQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.riotAccountInfoQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.riotAccountInfoQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.ACCOUNT_INFO,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.riotAccountInfoQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            const { region, puuid } = job.data.params;

            return ValorantApiQueueService.fetchRiotAccountInfoQueued(<RiotRegion>region, puuid);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.riotAccountInfoQueue[queueName];
  }

  @Trace({ name: "ValorantContentQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantContentQueue(regionName: string): Queue {
    const queueName = `${CONTENT_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.valorantContentQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantContentQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantContentQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.CONTENT,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantContentQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            return ValorantApiQueueService.fetchValorantContentQueued(job.data.params.region);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantContentQueue[queueName];
  }

  @Trace({ name: "ValorantLeaderboardQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantLeaderboardQueue(regionName: string): Queue {
    const queueName = `${LEADERBOARD_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.valorantLeaderboardQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantLeaderboardQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantLeaderboardQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.LEADERBOARD,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantLeaderboardQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            const { actId, region, size, startIndex } = job.data.params;

            return ValorantApiQueueService.fetchValorantLeaderboardQueued(region, actId, startIndex, size);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantLeaderboardQueue[queueName];
  }

  @Trace({ name: "ValorantMatchlistQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantMatchlistQueue(regionName: string): Queue {
    const queueName = `${MATCHLIST_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.valorantMatchlistQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantMatchlistQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantMatchlistQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.MATCHLIST,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantMatchlistQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            const { region, puuid } = job.data.params;

            return ValorantApiQueueService.fetchValorantMatchlistQueued(region, puuid);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantMatchlistQueue[queueName];
  }

  @Trace({ name: "ValorantMatchQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantMatchQueue(regionName: string): Queue {
    const queueName = `${MATCH_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.valorantMatchQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantMatchQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantMatchQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.MATCHES,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantMatchQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            const { region, matchId } = job.data.params;

            return ValorantApiQueueService.fetchValorantMatchesQueued(region, matchId);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantMatchQueue[queueName];
  }

  // This shouldn't be necessary because this will be called every 5 minutes, but better safe than sorry
  @Trace({ name: "ValorantRecentMatchesQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantRecentMatchesQueue(regionName: string): Queue {
    const queueName = `${RECENT_MATCHES_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!ValorantApiQueueService.valorantRecentMatchQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantRecentMatchQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantRecentMatchQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.RECENT_MATCHES,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantRecentMatchQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            const { region, queue } = job.data.params;

            return ValorantApiQueueService.fetchValorantRecentMatchesQueued(region, queue);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantRecentMatchQueue[queueName];
  }

  @Trace({ name: "ValorantShardQueue", service: VALORANT_API_QUEUE_SERVICE })
  private static ValorantShardQueue(): Queue {
    const queueName = `${SHARD_QUEUE_NAME}`;

    if (!ValorantApiQueueService.valorantShardQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const valQueue: Queue = <Queue>Config.getQueue(queueName);

      if (valQueue) {
        ValorantApiQueueService.valorantShardQueue[queueName] = valQueue;
      } else {
        // create a new queue
        ValorantApiQueueService.valorantShardQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: VALORANT_API_LIMITS.SHARD,
          defaultJobOptions: { removeOnComplete: true }
        });

        ValorantApiQueueService.valorantShardQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: ValorantQueueParams }>) => {
            const { puuid } = job.data.params;

            return ValorantApiQueueService.fetchValorantActiveShardQueued(puuid);
          }
        );
      }
    }

    return <Queue>ValorantApiQueueService.valorantShardQueue[queueName];
  }

  /**
   * Region does not matter because all regions have access to all account. Recommended to use closest one.
   */
  @Trace()
  public async fetchRiotAccountInfo(region: RiotRegion, puuid: string): Promise<IRiotAccountDTO> {
    const data = {
      funcName: QueueMethods.fetchRiotAccountInfo,
      params: {
        puuid,
        region
      }
    };

    const job = await ValorantApiQueueService.ValorantBaseQueue(region).add(data);

    return <IRiotAccountDTO>await job.finished();
  }

  @Trace()
  public async fetchValorantContent(region: string): Promise<IValorantContentDTO> {
    const data = {
      funcName: QueueMethods.fetchValorantContentQueued,
      params: {
        region
      }
    };

    // low priority
    const job = await ValorantApiQueueService.ValorantBaseQueue(region).add(data, { priority: 5 });

    return <IValorantContentDTO>await job.finished();
  }

  @Trace()
  public async fetchValorantLeaderboard(
    region: string,
    actId: string,
    startIndex: number,
    size?: number
  ): Promise<IValorantLeaderboardDTO> {
    const data = {
      funcName: QueueMethods.fetchValorantLeaderboardQueued,
      params: {
        region,
        actId,
        startIndex,
        size
      }
    };

    const job = await ValorantApiQueueService.ValorantBaseQueue(region).add(data, { priority: 5 });

    return <IValorantLeaderboardDTO>await job.finished();
  }

  @Trace()
  public async fetchValorantMatchList(region: string, puuid: string): Promise<IValorantMatchListDTO> {
    const data = {
      funcName: QueueMethods.fetchValorantMatchlistQueued,
      params: {
        region,
        puuid
      }
    };

    const job = await ValorantApiQueueService.ValorantBaseQueue(region).add(data, { priority: 1 });

    return <IValorantMatchListDTO>await job.finished();
  }

  @Trace()
  public async fetchValorantMatch(region: string, matchId: string): Promise<IValorantMatchDTO | null> {
    const data = {
      funcName: QueueMethods.fetchValorantMatchQueued,
      params: {
        region,
        matchId
      }
    };

    const job = await ValorantApiQueueService.ValorantBaseQueue(region).add(data, { priority: 1 });

    return <IValorantMatchDTO>await job.finished();
  }

  @Trace()
  public async fetchValorantRecentMatches(
    region: string,
    queue: ValorantQueueType
  ): Promise<IValorantRecentMatchesDTO> {
    const data = {
      funcName: QueueMethods.fetchValorantRecentMatchesQueued,
      params: {
        region,
        queue
      }
    };
    const job = await ValorantApiQueueService.ValorantBaseQueue(region).add(data, { priority: 1 });

    return <IValorantRecentMatchesDTO>await job.finished();
  }

  @Trace()
  public async fetchValorantActiveShard(puuid: string): Promise<IValorantActiveShardDTO> {
    const data = {
      funcName: QueueMethods.fetchActiveShard,
      params: {
        puuid
      }
    };

    // the region doesn't matter for this request, so we're going to sue the least used region (latam)
    const job = await ValorantApiQueueService.ValorantBaseQueue(ValorantRegions.LATAM).add(data, { priority: 1 });

    return <IValorantActiveShardDTO>await job.finished();
  }

  /**
   *  QUEUED METHODS
   *  These are executed on the Queue and are rate limited
   */
  @Trace({ name: QueueMethods.fetchRiotAccountInfo, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchRiotAccountInfoQueued(region: RiotRegion, puuid: string): Promise<IRiotAccountDTO> {
    const result = await ValorantApiQueueService.valorantApi.fetchRiotAccountInfo(region, puuid);

    return result;
  }

  @Trace({ name: QueueMethods.fetchValorantContentQueued, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchValorantContentQueued(region: string): Promise<IValorantContentDTO> {
    const result = await ValorantApiQueueService.valorantApi.fetchValorantContent(region);

    return result;
  }

  @Trace({ name: QueueMethods.fetchValorantLeaderboardQueued, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchValorantLeaderboardQueued(
    region: string,
    actId: string,
    startIndex: number,
    size: number
  ): Promise<IValorantLeaderboardDTO> {
    const result = await ValorantApiQueueService.valorantApi.fetchValorantLeaderboard(region, actId, startIndex, size);

    return result;
  }

  @Trace({ name: QueueMethods.fetchValorantMatchlistQueued, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchValorantMatchlistQueued(region: string, puuid: string): Promise<IValorantMatchListDTO> {
    const result = await ValorantApiQueueService.valorantApi.fetchValorantMatchList(region, puuid);

    return result;
  }

  @Trace({ name: QueueMethods.fetchValorantMatchQueued, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchValorantMatchesQueued(region: string, matchId: string): Promise<IValorantMatchDTO | null> {
    try {
      const result = await ValorantApiQueueService.valorantApi.fetchValorantMatch(region, matchId);
      return result;
    } catch (error) {
      // 429 means Too Many Requests have happened and match will be requeued.

      if (error?.message?.includes("429")) {
        const apiQueueService = new ValorantApiQueueService();
        apiQueueService.logger?.warn(
          error,
          "Too many requests have occurred for Valorant API queue service. Will attempt requeue. If you see many of these messages then rate limiting needs adjusted for Valorant."
        );

        // just add it to queue and don't wait for it or else we'll be here forever.
        const valorantAsyncService = new ValorantAsyncService();
        valorantAsyncService
          .addFetchAndSaveValorantRecentMatchToQueue(matchId, <ValorantRegions>region)
          .catch((error) => apiQueueService.handleError(error));

        return null;
      }

      throw error;
    }
  }

  @Trace({ name: QueueMethods.fetchValorantRecentMatchesQueued, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchValorantRecentMatchesQueued(
    region: string,
    queue: ValorantQueueType
  ): Promise<IValorantRecentMatchesDTO> {
    const result = await ValorantApiQueueService.valorantApi.fetchValorantRecentMatchesByQueue(region, queue);

    return result;
  }

  @Trace({ name: QueueMethods.fetchActiveShard, service: VALORANT_API_QUEUE_SERVICE })
  private static async fetchValorantActiveShardQueued(puuid: string): Promise<IValorantActiveShardDTO> {
    const result = await ValorantApiQueueService.valorantApi.fetchActiveShard(puuid);

    return result;
  }
}

interface ValorantQueueParams {
  gameName: string;
  gameTag: string;
  puuid: string;
  region: string | RiotRegion;
  actId: string;
  startIndex: number;
  size: number;
  matchId: string;
  queue: ValorantQueueType;
}
