import { cloneDeep } from "lodash";
import { Types } from "mongoose";
import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";

import { io } from "../../backend/config/get-io-object";
import { IValorantMatchListDTO } from "../../backend/interfaces/valorant/valorant-match-list-dto";
import { IValorantStats } from "../../backend/interfaces/valorant/valorant-stats";
import { IAgentPlayerStats } from "../../backend/interfaces/valorant/valorant-agent-player-stats";

import { ValorantApiQueueService } from "./valorant-api-queue.service";
import { competitiveTierRankNames, ValorantContentService } from "./valorant-content.service";
import { ValorantLeaderboardPlayerService } from "./valorant-leaderboard.service";
import { ValorantMatchListService } from "./valorant-match-list.service";
import { IValorantMatchDTO } from "../../backend/interfaces/valorant/valorant-match-dto";
import { IValorantAgentDTO } from "../../backend/interfaces/valorant/valorant-agent-dto";
import { ValorantRegions } from "./valorant-regions-enum";
import { IValorantMatchRoundResultDTO } from "../../backend/interfaces/valorant/valorant-match-round-result-dto";
import { IValorantPlayerRoundStatsDTO } from "../../backend/interfaces/valorant/valorant-player-round-stats-dto";
import { IValorantKillDTO } from "../../backend/interfaces/valorant/valorant-kill-dto";
import { IValorantDamageDTO } from "../../backend/interfaces/valorant/valorant-damage-dto";
import { RedisCacheService } from "../redis-cache.service";
import { IValorantMatchListEntryDTO } from "../../backend/interfaces/valorant/valorant-match-list-entry-dto";
import { IRiotProfile } from "../../backend/interfaces";
import { RiotRegion } from "./riot-region.enum";
import { RiotProfileService } from "../riot/riot-profile/riot-profile.service";
import { ValorantQueueType } from "./valorant-queue-type-enum";
import { IValorantLeaderboardPlayerDTO } from "../../backend/interfaces/valorant/valorant-leaderboard-player-dto";
import { IValorantActDTO } from "../../backend/interfaces/valorant/valorant-act-dto";
import { IValorantPlayerMatchStats } from "../../backend/interfaces/valorant/valorant-player-match-stats";
import { IValorantMatchPlayerDTO } from "../../backend/interfaces/valorant/valorant-match-player-dto";
import { ValorantPlayerMatchStatsService } from "./valorant-player-match-stats.service";
import { ValorantStatsService } from "./valorant-stats.service";
import { ValorantAgentService } from "./valorant-agent.service";
import { ValorantMatchService } from "./valorant-match.service";

type AdjustedPlayerStats = { kills: IValorantKillDTO[]; damage: IValorantDamageDTO[] };
type AdjustedRoundResults = { playerStats: AdjustedPlayerStats[] };

const CACHE_KEY_MATCHES = "valorant_matches";
const MATCHES_KEY_EXPIRY = 60 * 15; // 15 minutes

/**
 * The ValorantService is meant to interact with the queue service to distill stats for individual accounts
 */
@Service("valorant.service")
export class ValorantService extends BaseService {
  private redisCacheService: RedisCacheService;
  private riotProfileService: RiotProfileService;
  private valorantAgentService: ValorantAgentService;
  private valorantApiQueueService: ValorantApiQueueService;
  private valorantContentService: ValorantContentService;
  private valorantLeaderboardPlayerService: ValorantLeaderboardPlayerService;
  private valorantMatchService: ValorantMatchService;
  private valorantMatchListService: ValorantMatchListService;
  private valorantPlayerMatchStatsService: ValorantPlayerMatchStatsService;
  private valorantStatsService: ValorantStatsService;

  public constructor() {
    super();

    this.redisCacheService = new RedisCacheService();
    this.riotProfileService = new RiotProfileService();
    this.valorantAgentService = new ValorantAgentService();
    this.valorantApiQueueService = new ValorantApiQueueService();
    this.valorantContentService = new ValorantContentService();
    this.valorantLeaderboardPlayerService = new ValorantLeaderboardPlayerService();
    this.valorantMatchService = new ValorantMatchService();
    this.valorantMatchListService = new ValorantMatchListService();
    this.valorantPlayerMatchStatsService = new ValorantPlayerMatchStatsService();
    this.valorantStatsService = new ValorantStatsService();
  }

  @Trace()
  public async fetchAndSaveValorantRecentMatch(matchId: string, region: ValorantRegions): Promise<void> {
    try {
      if (!matchId) {
        // return because match id is null
        return;
      }

      const alreadySaved = await this.valorantMatchAlreadySaved(matchId);

      if (alreadySaved) {
        // return because match already saved
        return;
      }

      await this.fetchAdjustAndSaveValorantMatch(region, matchId);
    } catch (error) {
      throw this.handleError(error, `${region}: Failed to fetch and save recent valorant matches`);
    }
  }

  /**
   * This method pulls the new match list from Valorant and compares it to match data we already have.
   *
   * If there are new matches they are pulled down and saved in the database.
   */
  @Trace()
  public async retrieveAndSaveUserMatches(region: string, puuid: string): Promise<void> {
    try {
      // fetch match list
      // keep in mind that the match list generally does not contain all matches
      const fetchedMatchList: IValorantMatchListDTO = await this.valorantApiQueueService.fetchValorantMatchList(
        region,
        puuid
      );

      if (!fetchedMatchList) {
        this.logger?.info({ region, puuid }, "No matchlist was found");
        return;
      }

      // do not use a Promise.all or else the entire thing could fail if one riot match call fails
      await Promise.allSettled(
        fetchedMatchList.history.map(async (matchEntry: IValorantMatchListEntryDTO) => {
          const alreadySaved = await this.valorantMatchAlreadySaved(matchEntry.matchId);

          if (!alreadySaved) {
            return this.fetchAdjustAndSaveValorantMatch(region, matchEntry.matchId);
          }

          return matchEntry.matchId;
        })
      );
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async retrieveAndSaveValorantStatsForPlayer(
    userId: string,
    region: string,
    gameName: string,
    tagLine: string,
    puuid: string
  ): Promise<void> {
    try {
      const activeAct = await this.valorantContentService.getActiveAct(region);
      const activeEpisode = await this.valorantContentService.getActiveEpisode(region);
      // get leaderboard stats
      const playerLeaderboardEntry = await this.valorantLeaderboardPlayerService.findOne({ puuid }, null, {
        lean: true
      });

      const foundValorantPlayerStats: IValorantPlayerMatchStats[] = await this.valorantPlayerMatchStatsService.find(
        {
          puuid,
          actId: activeAct.id,
          queueId: ValorantQueueType.COMPETITIVE
        },
        null,
        { sort: { timeStarted: -1 }, lean: true }
      );

      let playerStats = this.createBasicPlayerStats(
        userId,
        gameName,
        tagLine,
        puuid,
        activeAct,
        activeEpisode,
        playerLeaderboardEntry,
        foundValorantPlayerStats[0]
      );

      // compile stats for games
      if (foundValorantPlayerStats && foundValorantPlayerStats.length > 0) {
        playerStats.lastMatchTime = foundValorantPlayerStats[0].timeStarted;
        const aggregatedStats = this.compileGameStats(foundValorantPlayerStats);
        if (aggregatedStats) {
          playerStats = <IValorantStats>{ ...playerStats, ...aggregatedStats };
        }

        // retrieve agent info and put on agent stats
        const agentMatchStats = await this.getAgents(playerStats.agentMatchStats);
        playerStats.agentMatchStats = agentMatchStats;
      }

      const savedStats = await this.createOrUpdateValorantStats(userId, playerStats);

      io.to(userId).emit("VALORANT_GAME_STATS_UPDATED", {
        owner: userId,
        status: "success",
        data: savedStats
      });
    } catch (error) {
      throw this.handleError(error, "Failed to fetch and save player stats");
    }
  }

  @Trace()
  public async updateValorantGameNameForPlayer(riotProfile: IRiotProfile): Promise<void> {
    // return because no riot profile available
    if (!riotProfile) {
      this.logger?.warn(riotProfile, "No valorant profile found for name update");
      return;
    }

    const adjustedRiotProfile = riotProfile;

    const { puuid, gameName, tagLine } = riotProfile?.valorantProfile;

    // return because somehow puuid is empty
    if (!puuid) {
      this.logger?.warn(riotProfile, `No puuid could be found`);
      return;
    }

    try {
      const foundRiotAccountDto = await this.valorantApiQueueService.fetchRiotAccountInfo(RiotRegion.AMERICAS, puuid);

      // something is really wrong if we can't find riot account from puuid
      if (!foundRiotAccountDto) {
        this.logger?.warn(riotProfile, `No riot account info could be found for ${puuid}`);
        return;
      }

      if (gameName !== foundRiotAccountDto.gameName) {
        adjustedRiotProfile.valorantProfile.gameName = foundRiotAccountDto.gameName;
      }

      if (tagLine !== foundRiotAccountDto.tagLine) {
        adjustedRiotProfile.valorantProfile.tagLine = foundRiotAccountDto.tagLine;
      }

      await this.checkForDifferencesAndUpdateValorantNames(
        adjustedRiotProfile,
        <string>gameName,
        <string>tagLine,
        puuid
      );
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async valorantMatchAlreadySaved(matchId: string): Promise<boolean> {
    try {
      const redisKey: string = this.getRedisKeyForMatch(matchId);
      const foundMatchId: string | null = await this.redisCacheService.readFromCache(redisKey);

      const matchExistsInRedis: boolean = foundMatchId !== null && foundMatchId.length > 0;

      // exit immediately if match exists in redis
      if (matchExistsInRedis) {
        return matchExistsInRedis;
      }

      const matchExists = await this.valorantMatchService.exists({ "matchInfo.matchId": matchId });

      if (matchExists) {
        return matchExists;
      }

      const matchStatsAggregated = await this.valorantPlayerMatchStatsService.exists({ matchId });

      return matchStatsAggregated;
    } catch (error) {
      throw this.handleError(error, "Error trying to find saved match");
    }
  }

  @Trace()
  private async checkForDifferencesAndUpdateValorantNames(
    adjustedRiotProfile: IRiotProfile,
    originalGameName: string,
    originalTagLine: string,
    puuid: string
  ): Promise<void> {
    const valorantProfileGameName = adjustedRiotProfile.valorantProfile.gameName;
    const valorantProfileTagLine = adjustedRiotProfile.valorantProfile.tagLine;

    try {
      // only save if something is different.
      if (valorantProfileTagLine !== originalTagLine || valorantProfileGameName !== originalGameName) {
        await this.riotProfileService.update({ _id: <Types.ObjectId>adjustedRiotProfile._id }, adjustedRiotProfile, {
          new: false
        });
      }

      const foundValorantStats = await this.valorantStatsService.findOne(
        { puuid },
        { gameName: 1, tagLine: 1 },
        { lean: true }
      );

      // only save if something is different.
      if (
        foundValorantStats?.gameName !== valorantProfileGameName ||
        foundValorantStats?.tagLine !== valorantProfileTagLine
      ) {
        await this.valorantStatsService.updateOne(
          { puuid },
          { gameName: valorantProfileGameName, tagLine: valorantProfileTagLine },
          { lean: true }
        );
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Since we are saving all matches, we are trying to save only what we need. We still save most data, but
   * roundResult[] is reduced to only keeping kills and damage objects.
   *
   * Also trying to keep its basic structure so its easier to add data in future.
   *
   * If matchId is returned then it is a completed match with relevant data, null otherwise
   *
   * @param match
   */
  @Trace()
  private async aggregateAndSavePlayerMatchStats(valorantMatch: IValorantMatchDTO): Promise<void> {
    try {
      const matchAlreadyAggregated = await this.valorantPlayerMatchStatsService.exists({
        matchId: valorantMatch.matchInfo.matchId
      });

      // exit because match stats have already been aggregated
      if (matchAlreadyAggregated) {
        return;
      }

      const { players } = valorantMatch;

      if (!players || players.length === 0) {
        this.logger?.warn(valorantMatch, "No player information is available");
        return;
      }

      const allPlayerMatchStats: Partial<IValorantPlayerMatchStats>[] = players.map((player: IValorantMatchPlayerDTO) =>
        this.aggregatePlayerMatchStats(player, valorantMatch)
      );

      await this.valorantPlayerMatchStatsService.createMany(allPlayerMatchStats);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  // return partial because IValorantPlayerMatchStats extends document
  @Trace()
  private aggregatePlayerMatchStats(
    player: IValorantMatchPlayerDTO,
    valorantMatch: IValorantMatchDTO
  ): Partial<IValorantPlayerMatchStats> {
    const team = valorantMatch.teams.find((t) => t.teamId === player.teamId);
    // guarantees won is false if null
    const won = !!team?.won;

    // iterate over every match's round results to tally up the damage
    let damage = 0;
    const { roundResults } = valorantMatch;

    roundResults.forEach((roundResult: IValorantMatchRoundResultDTO) => {
      // find the right player:
      const roundResultPlayerStats = roundResult.playerStats.find((stats) => stats.puuid === player.puuid);

      if (roundResultPlayerStats) {
        const damageValues = roundResultPlayerStats.damage.map((value) => value.damage);

        const roundDamage = damageValues.reduce((partialSum, value) => partialSum + value, 0);
        damage += roundDamage;
      }
    });

    return {
      actId: valorantMatch.matchInfo.seasonId,
      agent: player.characterId,
      assists: player?.stats?.assists,
      competitiveTier: player.competitiveTier,
      damage,
      deaths: player?.stats?.deaths,
      gameMode: valorantMatch.matchInfo.gameMode,
      kills: player?.stats?.kills,
      matchId: valorantMatch.matchInfo.matchId,
      puuid: player.puuid,
      queueId: valorantMatch.matchInfo.queueId,
      timePlayed: valorantMatch.matchInfo.gameLengthMillis,
      timeStarted: valorantMatch.matchInfo.gameStartMillis,
      won
    };
  }

  @Trace()
  private compileGameStats(playerStats: IValorantPlayerMatchStats[]): Partial<IValorantStats> {
    // damage, wins, losses, kills, deaths, assists
    let damage = 0;
    let wins = 0;
    let losses = 0;
    let kills = 0;
    let deaths = 0;
    let assists = 0;
    let competitiveTier = -1;

    const agentStats: Map<string, IAgentPlayerStats> = new Map<string, IAgentPlayerStats>();

    for (const [index, stats] of playerStats.entries()) {
      if (stats) {
        if (index === 0) {
          // set relevant values from most recent match
          competitiveTier = stats.competitiveTier;
        }

        // get the existing agent, or init one
        const currentAgent = <IAgentPlayerStats>(agentStats.get(stats.agent) || {
          agent: stats.agent,
          wins: 0,
          losses: 0,
          kills: 0,
          deaths: 0,
          assists: 0,
          damage: 0,
          timePlayed: 0
        });

        // time played
        currentAgent.timePlayed += stats.timePlayed;

        if (stats.won) {
          // wins
          currentAgent.wins += 1;
          wins += 1;
        } else {
          // losses
          currentAgent.losses += 1;
          losses += 1;
        }

        // kills
        currentAgent.kills += stats.kills;
        kills += stats.kills;

        // deaths
        currentAgent.deaths += stats.deaths;
        deaths += stats.deaths;

        // assists
        currentAgent.assists += stats.assists;
        assists += stats.assists;

        currentAgent.damage += stats.damage;
        damage += stats.damage;

        agentStats.set(stats.agent, currentAgent);
      }
    }

    const agentStatValues = [...agentStats.values()];
    agentStatValues.sort((a, b) => b.timePlayed - a.timePlayed);

    const result = {
      damage,
      wins,
      losses,
      kills,
      deaths,
      assists,
      competitiveTier,
      agentMatchStats: agentStatValues
    };

    return result;
  }

  private async createOrUpdateValorantStats(userId: string, playerStats: IValorantStats): Promise<IValorantStats> {
    try {
      let savedStats: IValorantStats;
      const existingStats = await this.valorantStatsService.findOne({ user: userId }, {}, { lean: true });

      if (existingStats) {
        savedStats = <IValorantStats>await this.valorantStatsService.updateOne(
          { user: existingStats.user },
          playerStats,
          {
            new: true,
            lean: true
          }
        );
      } else {
        savedStats = await this.valorantStatsService.create(playerStats);
      }

      return savedStats;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  private createBasicPlayerStats(
    userId: string,
    gameName: string,
    tagLine: string,
    puuid: string,
    activeAct: IValorantActDTO,
    activeEpisode: IValorantActDTO,
    leaderboard?: IValorantLeaderboardPlayerDTO | null,
    latestMatchStats?: IValorantPlayerMatchStats | null
  ): IValorantStats {
    const playerStats = <IValorantStats>(<unknown>{
      user: userId,
      gameName,
      tagLine,
      actId: activeAct.id,
      actName: activeAct.name,
      episodeId: activeEpisode.id,
      episodeName: activeEpisode.name,
      puuid
    });

    // set basic stats from leaderboard
    // If there are no entries the user hasn't played enough ranked games to be on the leaderboard
    if (leaderboard) {
      playerStats.competitiveTier = leaderboard.competitiveTier;
      playerStats.rankedRating = leaderboard.rankedRating;
      playerStats.leaderboardRank = leaderboard.leaderboardRank;
    } else if (latestMatchStats) {
      playerStats.competitiveTier = latestMatchStats.competitiveTier;
    }

    if (playerStats.competitiveTier) {
      playerStats.competitiveTierName = <string>competitiveTierRankNames[playerStats.competitiveTier];
      playerStats.competitiveTierImage = `static/valorant/tiers/Tier_${playerStats.competitiveTier}.png`;
    }

    return playerStats;
  }

  @Trace()
  private createAdjustedPlayerStats(playerStats: IValorantPlayerRoundStatsDTO[]): AdjustedPlayerStats[] {
    const adjustedPlayerStats: AdjustedPlayerStats[] = playerStats.map((stats: IValorantPlayerRoundStatsDTO) => {
      const { kills, damage } = stats;

      return { kills, damage };
    });

    return adjustedPlayerStats;
  }

  /**
   * Since we are saving all matches, we are trying to save only what we need. We still save most data, but
   * roundResult[] is reduced to only keeping kills and damage objects.
   *
   * Also trying to keep its basic structure so its easier to add data in future.
   *
   * If matchId is returned then it is a completed match with relevant data, null otherwise
   *
   * @param match
   */
  @Trace()
  private async fetchAdjustAndSaveValorantMatch(
    region: ValorantRegions | string,
    matchId: string
  ): Promise<string | null> {
    try {
      const match = await this.valorantApiQueueService.fetchValorantMatch(region, matchId);

      if (!match) {
        this.logger?.info(match, `No data could be found for match with id "${matchId}"`);
        // return because no match to save
        return null;
      }

      if (!match?.matchInfo?.isCompleted || match.roundResults.length === 0) {
        this.logger?.debug(match, `Incomplete or no round results could be found for match with id "${matchId}"`);
        // return because no real data to save
        return null;
      }

      if (match.matchInfo.queueId !== ValorantQueueType.COMPETITIVE) {
        this.logger?.debug(match, "Match is not a competitive match");
        // return because match is not competitive and therefore not needed
        return null;
      }

      // aggregate new matches stats
      this.aggregateAndSavePlayerMatchStats(match).catch((error) => this.handleError(error));

      const adjustedRoundResults: AdjustedRoundResults[] = [];

      match.roundResults.forEach((roundResult: IValorantMatchRoundResultDTO) => {
        const playerStats = this.createAdjustedPlayerStats(roundResult.playerStats);
        const round: AdjustedRoundResults = { playerStats };
        adjustedRoundResults.push(round);
      });

      const { matchInfo, players, coaches, teams } = match;

      const redisKey: string = this.getRedisKeyForMatch(match.matchInfo.matchId);

      await this.redisCacheService.writeToCache(redisKey, match.matchInfo.matchId, MATCHES_KEY_EXPIRY);

      const adjustedMatch = <IValorantMatchDTO>(<unknown>{
        matchInfo,
        players,
        coaches,
        teams,
        roundResults: adjustedRoundResults
      });

      await this.valorantMatchService.create(adjustedMatch);

      return matchId;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async getAgents(agentMatchStats: IAgentPlayerStats[]): Promise<IAgentPlayerStats[]> {
    const localAgentStats = cloneDeep(agentMatchStats);

    try {
      for (const agentMatchStat of localAgentStats) {
        const agent = await this.valorantAgentService.findOne({ uuid: agentMatchStat.agent }, null, { lean: true });
        agentMatchStat.agentData = <IValorantAgentDTO>agent;
      }

      return localAgentStats;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private getRedisKeyForMatch(matchId: string): string {
    return `${CACHE_KEY_MATCHES}:${matchId}`;
  }
}
