import { Queue, Job, JobOptions } from "bull";
import { Failure } from "@efuse/contracts";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import * as Config from "../../async/config";
import { ValorantService } from "./valorant.service";
import { ValorantRegions } from "./valorant-regions-enum";
import { ValorantQueueType } from "./valorant-queue-type-enum";
import { IRiotProfile } from "../../backend/interfaces";

const VALORANT_BASE_QUEUE = "valorant-async-queue";
const VALORANT_ASYNC_SERVICE = "valorant-async.service";

enum ValorantQueue {
  UPDATE_VALORANT_NAME_QUEUE = "update-valorant-name-for-player-queue",
  USER_VALORANT_STATS_QUEUE = "user-valorant-stats-queue",
  VALORANT_FETCH_MATCH_QUEUE = "valorant-fetch-match-queue",
  VALORANT_FETCH_MATCHLIST_AND_USER_STATS_QUEUE = "valorant-fetch-matchlist-and-user-stats-queue"
}

enum ValorantQueueFunctions {
  FETCH_AND_SAVE_VALORANT_RECENT_MATCH = "fetchAndSaveValorantRecentMatch",
  UPDATE_VALORANT_NAME_FOR_PLAYER = "updateValorantNameForPlayer",
  VALORANT_MATCHLIST_AND_USER_STATS = "valorantMatchlistAndUserStats",
  VALORANT_USER_STATS = "valorantUserStats"
}

interface ValorantBaseJobParams {
  funcName: ValorantQueueFunctions;
  region: string;
}

interface ValorantRecentMatchJobParam extends ValorantBaseJobParams {
  matchId: string;
}

interface QueueValorantRecentMatchesJobParam extends ValorantBaseJobParams {
  queue: ValorantQueueType;
}

interface UpdateValorantNameJobParams {
  funcName: ValorantQueueFunctions;
  valorantProfile: IRiotProfile;
}

interface ValorantUserStatsJobParams extends ValorantBaseJobParams {
  tagLine: string;
  userId: string;
  gameName: string;
  puuid: string;
}

interface ValorantMatchListAndUserStatsJobParams extends ValorantBaseJobParams {
  puuid: string;
}

type ValorantJob = Job<
  | QueueValorantRecentMatchesJobParam
  | UpdateValorantNameJobParams
  | ValorantRecentMatchJobParam
  | ValorantUserStatsJobParams
>;

@Service(VALORANT_ASYNC_SERVICE)
export class ValorantAsyncService extends BaseService {
  private valorantService: ValorantService;
  private static valorantQueue: Map<string, Queue> = new Map<string, Queue>();

  public constructor() {
    super();

    this.valorantService = new ValorantService();
  }

  @Trace({ name: "InitAsyncProcessors", service: VALORANT_ASYNC_SERVICE })
  public static InitAsyncProcessors(): void {
    const service = new ValorantAsyncService();
    const valorantQueueList: string[] = Object.values(ValorantQueue);

    valorantQueueList.forEach((queueName) => {
      const newQueue = ValorantAsyncService.ValorantBaseQueue(queueName);
      service.logger?.info(`Created Valorant Queue: ${newQueue.name}`);
    });
  }

  @Trace({ name: "handleQueueJob", service: VALORANT_ASYNC_SERVICE })
  private static async handleQueueJob(job: ValorantJob): Promise<any> {
    const valorantService = new ValorantService();
    const asyncService = new ValorantAsyncService();

    // we have been having an issue where valorant-fetch-match-queue has undefined data.
    // this is temporary until we figure out how it is happening
    // For more info: https://efuse.atlassian.net/browse/EF-7327
    if (job == null || job.data == null) {
      asyncService.logger?.debug(
        {
          shouldAlwaysBeHere: "shouldAlwaysBeHere",
          data: job?.data,
          job
        },
        `Found undefined job or data`
      );

      return;
    }

    const { funcName } = job.data;

    switch (funcName) {
      case ValorantQueueFunctions.FETCH_AND_SAVE_VALORANT_RECENT_MATCH: {
        const jobRecentMatchData = <ValorantRecentMatchJobParam>job.data;

        return valorantService.fetchAndSaveValorantRecentMatch(
          jobRecentMatchData.matchId,
          <ValorantRegions>jobRecentMatchData.region
        );
      }
      case ValorantQueueFunctions.UPDATE_VALORANT_NAME_FOR_PLAYER: {
        const jobUpdateValorantNameData = <UpdateValorantNameJobParams>job.data;

        return valorantService.updateValorantGameNameForPlayer(jobUpdateValorantNameData.valorantProfile);
      }
      case ValorantQueueFunctions.VALORANT_USER_STATS: {
        const jobUserStatsData = <ValorantUserStatsJobParams>job.data;

        return valorantService.retrieveAndSaveValorantStatsForPlayer(
          jobUserStatsData.userId,
          jobUserStatsData.region,
          jobUserStatsData.gameName,
          jobUserStatsData.tagLine,
          jobUserStatsData.puuid
        );
      }
      case ValorantQueueFunctions.VALORANT_MATCHLIST_AND_USER_STATS: {
        const matchlistData = <ValorantMatchListAndUserStatsJobParams>job.data;

        return valorantService.retrieveAndSaveUserMatches(matchlistData.region, matchlistData.puuid);
      }
      default:
        throw Failure.InternalServerError(`No queue service function named ${job?.data?.funcName}`, {
          shouldAlwaysBeHere: "shouldAlwaysBeHere",
          data: job?.data,
          job
        });
    }
  }

  /**
   * Valorant Queue allows adding new valorant functions for queue more easily
   */
  @Trace({ name: "ValorantBaseQueue", service: VALORANT_ASYNC_SERVICE })
  private static ValorantBaseQueue(valorantQueueName: string): Queue {
    const queueName = `${VALORANT_BASE_QUEUE}:${valorantQueueName.toLowerCase()}`;

    if (!ValorantAsyncService.valorantQueue[queueName]) {
      // attempt to get existing queue before creating a new one

      const valorantQueue: Queue = Config.getQueue(queueName);

      if (valorantQueue) {
        ValorantAsyncService.valorantQueue[queueName] = valorantQueue;
      } else {
        // create a new queue

        ValorantAsyncService.valorantQueue[queueName] = Config.initQueue(queueName);

        ValorantAsyncService.valorantQueue[queueName].process((job: ValorantJob) => this.handleQueueJob(job));
      }
    }

    return <Queue>ValorantAsyncService.valorantQueue[queueName];
  }

  /**
   * Warning: Because of how demanding this job can be do not use it in something like a Promise.all or Promise.allSettled as it will cause memory spikes
   */
  @Trace()
  public async addFetchAndSaveValorantRecentMatchToQueue(matchId: string, region: ValorantRegions): Promise<void> {
    const data: ValorantRecentMatchJobParam = {
      funcName: ValorantQueueFunctions.FETCH_AND_SAVE_VALORANT_RECENT_MATCH,
      matchId,
      region
    };

    if (!matchId || matchId.length === 0) {
      return;
    }

    const matchFound = await this.valorantService.valorantMatchAlreadySaved(matchId);

    if (matchFound) {
      return;
    }

    // This is IMPORTANT. Redis is getting overloaded. Because of rate limiting from Riot the queue builds up faster than we can process.
    // This is causing Redis memory to explode and cause issues. We are removing jobs whether they complete or fail to save space and it allows them to be readded.
    // By setting job id to match id we are making it so that a match can't be in the queue more than once.
    const bullOptions: JobOptions = {
      jobId: matchId,
      removeOnComplete: true,
      removeOnFail: true
    };

    try {
      const job = await ValorantAsyncService.ValorantBaseQueue(ValorantQueue.VALORANT_FETCH_MATCH_QUEUE).add(
        data,
        bullOptions
      );

      return <void>await job.finished();
    } catch (error) {
      this.logger?.error({ createdData: data });
      throw this.handleError(error, "Function: addFetchAndSaveValorantRecentMatchToQueue");
    }
  }

  @Trace()
  public async fetchMatchlistAndPreaggregateUserStats(region: string, puuid: string): Promise<void> {
    const data: ValorantMatchListAndUserStatsJobParams = {
      funcName: ValorantQueueFunctions.VALORANT_MATCHLIST_AND_USER_STATS,
      region,
      puuid
    };

    try {
      const job = await ValorantAsyncService.ValorantBaseQueue(
        ValorantQueue.VALORANT_FETCH_MATCHLIST_AND_USER_STATS_QUEUE
      ).add(data);

      return <void>await job.finished();
    } catch (error) {
      this.logger?.error({ createdData: data });
      throw this.handleError(error, "Function: fetchMatchlistAndPreaggregateUserStats");
    }
  }

  @Trace()
  public async retrieveAndSaveValorantUserStatsForPlayer(
    userId: string,
    region: string,
    gameName: string,
    tagLine: string,
    puuid: string
  ): Promise<void> {
    const data: ValorantUserStatsJobParams = {
      funcName: ValorantQueueFunctions.VALORANT_USER_STATS,
      userId,
      gameName,
      tagLine,
      region,
      puuid
    };

    try {
      const job = await ValorantAsyncService.ValorantBaseQueue(ValorantQueue.USER_VALORANT_STATS_QUEUE).add(data);

      return <void>await job.finished();
    } catch (error) {
      this.logger?.error({ createdData: data });
      throw this.handleError(error, "Function: queueRecentValorantMatchesForFetchingAndSaving");
    }
  }

  @Trace()
  public async updateValorantGameNameForPlayer(valorantProfile: IRiotProfile): Promise<void> {
    const data: UpdateValorantNameJobParams = {
      funcName: ValorantQueueFunctions.UPDATE_VALORANT_NAME_FOR_PLAYER,
      valorantProfile
    };

    try {
      await ValorantAsyncService.ValorantBaseQueue(ValorantQueue.UPDATE_VALORANT_NAME_QUEUE).add(data, {
        priority: 2
      });
    } catch (error) {
      this.logger?.error({ createdData: data });
      throw this.handleError(error, "Function: queueRecentValorantMatchesForFetchingAndSaving");
    }
  }
}
