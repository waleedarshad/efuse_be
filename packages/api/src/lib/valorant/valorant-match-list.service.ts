import { Failure } from "@efuse/contracts";
import { QueryOptions, FilterQuery } from "mongoose";

import { IValorantMatchListDTO } from "../../backend/interfaces/valorant/valorant-match-list-dto";
import { ValorantMatchList, ValorantMatchListModel } from "../../backend/models/valorant/valorant-match-list";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("valorant-match-list.service")
export class ValorantMatchListService extends BaseService {
  private readonly model: ValorantMatchListModel<IValorantMatchListDTO> = ValorantMatchList;

  @Trace()
  public async createOrReplace(item: IValorantMatchListDTO): Promise<IValorantMatchListDTO> {
    try {
      if (!item.puuid) {
        throw Failure.BadRequest("Missing 'puuid' on item");
      }
      await this.deleteByPuuid(item.puuid);

      const doc = <IValorantMatchListDTO>item;
      const result: IValorantMatchListDTO = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async find(filter: FilterQuery<IValorantMatchListDTO>): Promise<IValorantMatchListDTO[]>;
  public async find(filter: FilterQuery<IValorantMatchListDTO>, projection: any): Promise<IValorantMatchListDTO[]>;
  public async find(
    filter: FilterQuery<IValorantMatchListDTO>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantMatchListDTO[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IValorantMatchListDTO>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantMatchListDTO[]> {
    const match = await this.model.find(filter, projection, options).exec();
    return match;
  }

  public async findOne(filter: FilterQuery<IValorantMatchListDTO>): Promise<IValorantMatchListDTO | null>;
  public async findOne(
    filter: FilterQuery<IValorantMatchListDTO>,
    projection: any
  ): Promise<IValorantMatchListDTO | null>;
  public async findOne(
    filter: FilterQuery<IValorantMatchListDTO>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IValorantMatchListDTO | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IValorantMatchListDTO>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IValorantMatchListDTO | null> {
    const matchList = await this.model.findOne(filter, projection, options).exec();
    return matchList;
  }

  @Trace()
  public async deleteByPuuid(puuid: string): Promise<IValorantMatchListDTO | null> {
    try {
      if (!puuid) {
        throw Failure.BadRequest("Unable to delete item, missing 'puuid'");
      }

      const item: IValorantMatchListDTO | null = await this.model.findOneAndDelete({ puuid }, { lean: true }).exec();

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
