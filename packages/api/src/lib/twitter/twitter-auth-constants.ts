import { BACKEND_DOMAIN, TWITTER_API_KEY, TWITTER_API_SECRET } from "@efuse/key-store";

export class TwitterAuthConstants {
  public static readonly BASE_URL = "https://api.twitter.com/oauth";
  public static readonly CALLBACK_URL = `${BACKEND_DOMAIN}/api/auth/twitter/callback`;
  public static readonly OAUTH_SIGNATURE_METHOD = "HMAC-SHA1";
  public static readonly OAUTH_VERSION = "1.0";
  public static readonly TWITTER_API_KEY = TWITTER_API_KEY;
  public static readonly TWITTER_API_SECRET = TWITTER_API_SECRET;
}
