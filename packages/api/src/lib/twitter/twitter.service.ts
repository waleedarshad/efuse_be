import { Failure } from "@efuse/contracts";
import fs from "fs";
import createError from "http-errors";
import { Types } from "mongoose";
import { GetObjectOutput } from "aws-sdk/clients/s3";

import { Media } from "../../backend/models/media";
import { BaseService } from "../base/base.service";
import { IUser } from "../../backend/interfaces/user";
import { Service, Trace } from "../decorators";
import { TWITTER_API_KEY, TWITTER_API_SECRET } from "@efuse/key-store";
import { TwitterAPIMediaUploadResponse, TwitterServiceCreateTweetResponse } from "../../backend/interfaces/twitter";
import UserLib from "../users";
import { S3Service } from "../aws/s3.service";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";
import { MediaUtilService } from "../media/media-util.service";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const Twitter = require("twitter-lite");

enum TwitterMediaCategory {
  IMAGE = "tweet_image",
  VIDEO = "tweet_video",
  GIF = "tweet_gif"
}

enum TwitterMediaProcessingStatus {
  PENDING = "pending",
  SUCCEEDED = "succeeded",
  FAILED = "failed",
  IN_PROGRESS = "in_progress"
}

const MAX_FILE_CHUNK_BYTES = 3 * 1024 * 1024; // Setting it to 3 MB

/**
 * Twitter Service
 *
 * @example
 * const TwitterService = require('lib/twitter/twitter.service')
 *
 * const twitterService = new TwitterService(userId)
 *
 */
@Service("twitter.service")
export class TwitterService extends BaseService {
  private userId: string;
  private s3Service: S3Service;
  private chunkUploadFlag = "enable_twitter_chunk_upload";
  private mediaUtilService: MediaUtilService;

  constructor(userId: string) {
    super();
    this.userId = userId;
    this.s3Service = new S3Service();
    this.mediaUtilService = new MediaUtilService();
  }

  /**
   * @param  {string} subdomain The twitter-specific subdomain to create a client
   * @returns Promise<Twitter>
   */
  @Trace()
  private async getApiClient(subdomain = "api"): Promise<typeof Twitter> {
    const user = (await UserLib.getUserById(this.userId, {
      projection: { twitterOauthToken: 1, twitterOauthTokenSecret: 1 }
    })) as IUser;

    const { twitterOauthToken, twitterOauthTokenSecret } = user;

    if (!twitterOauthToken || !twitterOauthTokenSecret) {
      const msg = "Twitter account not linked.  Please re-link twitter and try again";
      this.logger?.warn({ userId: this.userId }, msg);
      throw createError(403, msg);
    }

    return new Twitter({
      subdomain,
      version: "1.1",
      consumer_key: TWITTER_API_KEY,
      consumer_secret: TWITTER_API_SECRET,
      access_token_key: twitterOauthToken,
      access_token_secret: twitterOauthTokenSecret
    });
  }

  /**
   * Get a Twitter client that is pointed to their media "upload" API
   *
   * @returns Promise<Twitter>
   */
  public async getUploadClient(): Promise<typeof Twitter> {
    return this.getApiClient("upload");
  }

  /**
   * Create a new tweet with an option media argument
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-update
   *
   * @param  {string} content The content for the tweet
   * @param  {string} mediaId The mediaId for the object ID
   * @returns Promise<void>
   */
  @Trace()
  public async createTweet(content: string, mediaId?: string): Promise<TwitterServiceCreateTweetResponse> {
    let mediaIds = "";

    if (mediaId) {
      const mediaUploadResponse = (await this.chunkUploadEnabled())
        ? await this.uploadMediaInChunks(mediaId)
        : await this.uploadImage(mediaId);
      if (mediaUploadResponse) {
        mediaIds = mediaUploadResponse.media_id_string;
      }
    }

    const apiClient = await this.getApiClient();

    const result = await apiClient.post("statuses/update", {
      status: content,
      media_ids: mediaIds
    });

    this.logger?.info({ result }, "Post sent to twitter");

    const { user, id } = result;
    const response = {
      url: `https://twitter.com/${<string>user.name}/status/${<string>id}`,
      id
    };
    return response;
  }

  /**
   * Create a new tweet with an option media argument
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/tweets/post-and-engage/api-reference/post-statuses-update
   *
   * @param  {string} content The content for the tweet
   * @param  {string} mediaId The mediaId for the object ID
   * @returns Promise<void>
   */
  @Trace()
  public async createMediaTweet(
    content: string,
    mediaIds?: Types.ObjectId[] | string[]
  ): Promise<TwitterServiceCreateTweetResponse> {
    let uploadedMediaIds: string[] = [];
    const mediaPromise: Promise<TwitterAPIMediaUploadResponse | undefined>[] = [];
    const chunkMediaUploadEnabled = await this.chunkUploadEnabled();
    mediaIds?.forEach((mediaId) => {
      mediaPromise.push(chunkMediaUploadEnabled ? this.uploadMediaInChunks(mediaId) : this.uploadImage(mediaId));
    });
    const mediaObjects = await Promise.all(mediaPromise);
    uploadedMediaIds = mediaObjects.map((m) => m?.media_id_string || "");

    const apiClient = await this.getApiClient();

    const result = await apiClient.post("statuses/update", {
      status: content,
      media_ids: uploadedMediaIds.join()
    });

    this.logger?.info({ result }, "Post sent to twitter");

    const { user, id } = result;
    const response = {
      url: `https://twitter.com/${<string>user.name}/status/${<string>id}`,
      id
    };
    return response;
  }

  /**
   * Given a mediaId string, upload the image to twitter and return the twitter media id
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-upload
   *
   * @param  {string} uploadImage(mediaId)
   * @returns Promise
   */
  @Trace()
  public async uploadImage(mediaId: string | Types.ObjectId): Promise<TwitterAPIMediaUploadResponse | undefined> {
    const media = await Media.findById(mediaId);

    if (!media) {
      this.logger?.info({ mediaId }, "Media file not found for post");
      return;
    }

    this.logger?.info({ media }, "Media file for post");
    const { filename, contentType } = media.file;
    const s3Object = await this.s3Service.downloadMediaFromS3(`uploads/media/${<string>filename}`);

    // Make sure dimensions are supported
    this.validateImageDimensions(contentType, s3Object);

    const base64Image = (s3Object.Body as Buffer).toString("base64");

    const uploadClient = await this.getUploadClient();

    const mediaUploadResponse: TwitterAPIMediaUploadResponse = await uploadClient.post("media/upload", {
      media_data: base64Image
    });

    this.logger?.info({ mediaUploadResponse }, "Twitter media upload response");
    return mediaUploadResponse;
  }

  @Trace()
  public async uploadMediaInChunks(mediaId: Types.ObjectId | string): Promise<TwitterAPIMediaUploadResponse> {
    try {
      const uploadClient = await this.getUploadClient();
      const uploadResponse: TwitterAPIMediaUploadResponse | undefined = await this.grabMediaAndBeginUpload(
        uploadClient,
        mediaId
      );
      if (!uploadResponse) {
        throw this.handleError(new Error("Response not received"));
      }

      // Wait for the upload to finish
      await this.waitAndCheckStatus(uploadClient, uploadResponse.media_id_string, uploadResponse);
      return uploadResponse;
    } catch (error) {
      throw this.handleError(error, "Error while uploading media in chunks");
    }
  }

  /**
   * @summary Upload media on twitter in chunks
   *
   * @param {typeof Twitter} uploadClient
   * @param {Types.ObjectId | string} mediaId
   *
   * @returns {Promise<TwitterAPIMediaUploadResponse | undefined>}
   */
  @Trace()
  public async grabMediaAndBeginUpload(
    uploadClient: typeof Twitter,
    mediaId: string | Types.ObjectId
  ): Promise<TwitterAPIMediaUploadResponse | undefined> {
    return new Promise<TwitterAPIMediaUploadResponse | undefined>((resolve, reject) => {
      (async () => {
        try {
          const media = await Media.findById(mediaId);

          if (!media) {
            this.logger?.info({ mediaId }, "Media file not found for post");
            return reject(new Error("Media file not found"));
          }

          this.logger?.info({ media }, "Media file for post");
          const { filename, contentType } = media.file;

          // Download file from S3
          const s3Object = await this.s3Service.downloadMediaFromS3(`uploads/media/${<string>filename}`);

          // Make sure dimensions are supported
          this.validateImageDimensions(contentType, s3Object);

          const filePath = `/tmp/${filename as string}`;
          this.logger?.info({ filePath }, "Writing tmp file");

          // Write file
          await fs.promises.writeFile(filePath, s3Object.Body as string);

          // Call INIT command
          const initResponse: TwitterAPIMediaUploadResponse | undefined = await this.init(
            uploadClient,
            contentType as string,
            fs.statSync(filePath).size
          );

          if (!initResponse) {
            return reject(new Error("Response not received from INIT Command"));
          }

          // Read file
          const mediaFile = fs.createReadStream(filePath, { highWaterMark: MAX_FILE_CHUNK_BYTES });

          // Defaults to support upload
          let segmentIndex = 0;
          let isUploading = false;
          let isFileStreamEnded = false;

          // Read file in chunks
          mediaFile.on("data", (chunk) => {
            (async () => {
              // Pause our file stream from emitting `data` events until the upload of this chunk completes.
              // Any data that becomes available will remain in the internal buffer.
              mediaFile.pause();
              isUploading = true;

              // Call APPEND Command
              await this.append(uploadClient, initResponse.media_id_string, chunk.toString("base64"), segmentIndex);

              isUploading = false;

              // Increment segmentIndex before next chunk
              segmentIndex += 1;

              // Check if the uploading has finished
              if (!isUploading && isFileStreamEnded) {
                const finalizeResponse = await this.finalize(uploadClient, initResponse.media_id_string);
                await fs.promises.unlink(filePath);
                return resolve(finalizeResponse);
              }

              // Resume media
              mediaFile.resume();
            })().catch((error) => reject(error));
          });

          // End File Uploading
          mediaFile.on("end", () => {
            (async () => {
              // Mark our file streaming complete, and if done, send FINALIZE command.
              isFileStreamEnded = true;

              if (!isUploading && isFileStreamEnded) {
                const finalizeResponse = await this.finalize(uploadClient, initResponse.media_id_string);
                await fs.promises.unlink(filePath);
                return resolve(finalizeResponse);
              }
            })().catch((error) => reject(error));
          });
        } catch (error) {
          this.logger?.error(error, "Something went wrong while upload media in chunks");
          return reject(error);
        }
      })().catch((error) => {
        throw this.handleError(error, "Something went wrong while chunk uploading");
      });
    });
  }

  /**
   * @summary Initiate Twitter Media Upload in chunks
   *
   * @param {typeof Twitter} client
   * @param {string} mimeType
   * @param {number} fileSize
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-upload-init
   *
   * @returns {Promise<TwitterAPIMediaUploadResponse>} Twitter Media upload response
   */
  @Trace()
  private async init(
    client: typeof Twitter,
    mimeType: string,
    fileSize: number
  ): Promise<TwitterAPIMediaUploadResponse> {
    const lowerCaseMimeType = mimeType.toLowerCase();
    let mediaCategory = "";

    // https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/uploading-media/media-best-practices
    // if it is a GIF
    if (lowerCaseMimeType.includes("gif")) {
      mediaCategory = TwitterMediaCategory.GIF;
    }
    // if it is a Video
    else if (lowerCaseMimeType.includes("video")) {
      mediaCategory = TwitterMediaCategory.VIDEO;
    }
    // if it is an Image
    else {
      mediaCategory = TwitterMediaCategory.IMAGE;
    }

    this.logger?.info({ mimeType, fileSize, mediaCategory }, "INIT Step Begin");

    // Send INIT command with file info and get back a media_id_string we can use it to APPEND chunks

    const response: TwitterAPIMediaUploadResponse = await client.post("media/upload", {
      command: "INIT",
      total_bytes: fileSize,
      mediaType: lowerCaseMimeType,
      media_category: mediaCategory
    });

    this.logger?.info({ response }, "INIT Step End");
    return response;
  }

  /**
   * @summary Send APPEND command for media object with id `media_id`
   *
   * @param {typeof Twitter} client
   * @param {string} mediaIdString media_id_string received from Twitter after sending INIT command
   * @param {string} chunk Base64-encoded String chunk of the media file
   * @param {number} segmentIndex Index of the segment
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-upload-append
   *
   * @returns {Promise<string>}
   */
  @Trace()
  private async append(
    client: typeof Twitter,
    mediaIdString: string,
    chunk: string,
    segmentIndex: number
  ): Promise<string> {
    try {
      this.logger?.info({ mediaIdString, segmentIndex }, `APPEND Step ${segmentIndex} Begin`);

      const response = await client.post("media/upload", {
        command: "APPEND",
        media_id: mediaIdString,
        media: chunk,
        segment_index: segmentIndex
      });
      this.logger?.info({ response }, `APPEND Step ${segmentIndex} End`);
      return "Chunk Uploaded";
    } catch (error) {
      this.logger?.error(error, "Error in append step");
      throw error;
    }
  }

  /**
   * @summary Send FINALIZE command for media object with id `media_id`
   * @param {typeof Twitter} client
   * @param {string} mediaIdString
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/post-media-upload-finalize
   *
   * @returns {Promise<TwitterAPIMediaUploadResponse | undefined>} Twitter Media upload response
   */
  @Trace()
  private async finalize(client: typeof Twitter, mediaIdString: string): Promise<TwitterAPIMediaUploadResponse> {
    try {
      this.logger?.info({ mediaIdString }, "FINALIZE Step Begin");

      const response: TwitterAPIMediaUploadResponse = await client.post("media/upload", {
        command: "FINALIZE",
        media_id: mediaIdString
      });

      this.logger?.info({ response }, "FINALIZE Step END");
      return response;
    } catch (error) {
      this.logger?.error(error, "Error in finalize step");
      throw error;
    }
  }

  /**
   * @summary Send STATUS command to check the status of upload
   *
   * @param {typeof Twitter} client
   * @param {string} mediaIdString
   *
   * @link https://developer.twitter.com/en/docs/twitter-api/v1/media/upload-media/api-reference/get-media-upload-status
   *
   * @returns {Promise<TwitterAPIMediaUploadResponse>}
   */
  @Trace()
  private async status(client: typeof Twitter, mediaIdString: string): Promise<TwitterAPIMediaUploadResponse> {
    try {
      this.logger?.info({ mediaIdString }, "STATUS Step Begin");

      const response: TwitterAPIMediaUploadResponse = await client.get("media/upload", {
        command: "STATUS",
        media_id: mediaIdString
      });
      this.logger?.info({ response }, "STATUS Step End");
      return response;
    } catch (error) {
      this.logger?.error(error, "Error in status step");
      throw error;
    }
  }

  /**
   * @summary Wait for upload to complete & periodically check the status of upload
   *
   * @param {typeof Twitter} client
   * @param {string} mediaIdString
   * @param {TwitterAPIMediaUploadResponse} uploadResponse
   */
  @Trace()
  private async waitAndCheckStatus(
    client: typeof Twitter,
    mediaIdString: string,
    uploadResponse: TwitterAPIMediaUploadResponse
  ): Promise<void> {
    let response = uploadResponse;

    // See if processing info is available in response
    const processingInfo = response.processing_info;
    if (processingInfo) {
      const { state, error } = processingInfo;

      // Handle Failure
      if (state === TwitterMediaProcessingStatus.FAILED) {
        throw this.handleError(new Error(error?.message || "Twitter returned error while uploading"));
      }
      // Handle Pending
      else if (state === TwitterMediaProcessingStatus.PENDING || state === TwitterMediaProcessingStatus.IN_PROGRESS) {
        const checkAfterSeconds = processingInfo.check_after_secs;
        this.logger?.info(`Waiting for ${checkAfterSeconds} seconds`);
        // Wait to check status again
        await new Promise((resolve) => setTimeout(resolve, checkAfterSeconds * 1000));
        this.logger?.info(`Checking status again after Waiting for ${checkAfterSeconds} seconds`);
        response = await this.status(client, mediaIdString);
        this.logger?.info(response, "Response received for status");

        // Recursively call again
        await this.waitAndCheckStatus(client, mediaIdString, response);
      }
      // Handle Success
      else if (state !== TwitterMediaProcessingStatus.SUCCEEDED) {
        this.logger?.info(response, "Upload response");
        throw this.handleError(new Error("Failed to upload media chunk due to unknown reason"));
      }
    }
  }

  /**
   * @summary Check whether chunk upload feature is enabled for user or not
   *
   * @returns {Promise<boolean>}
   */
  @Trace()
  private async chunkUploadEnabled(): Promise<boolean> {
    const enabled = (await FeatureFlagService.hasFeature(this.chunkUploadFlag, this.userId)) as boolean;
    return enabled;
  }

  /**
   * @summary Make sure image dimensions are supported
   *
   * @param {string | undefined} contentType
   * @param {GetObjectOutput} s3Object
   *
   */
  public validateImageDimensions(contentType: string | undefined, s3Object: GetObjectOutput): void {
    // Make sure dimensions are supported
    if (contentType?.includes("image")) {
      const isValid = this.mediaUtilService.areDimensionsValid(s3Object.Body as Buffer);
      if (!isValid) {
        throw Failure.BadRequest("Image dimensions are not supported");
      }
    }
  }
}
