import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";
import axios from "axios";
import { randomBytes, createHmac } from "crypto";
import { Types } from "mongoose";
import { URLSearchParams } from "url";

import { BaseService } from "../base/base.service";
import { IOAuthCredential, ITwitterAccountInfo } from "../../backend/interfaces";
import {
  IOAuthParams,
  IOAuthRequestTokenResponse,
  IOAuthAccessTokenResponse
} from "../../backend/interfaces/twitter/auth";
import { OAuthCredentialService } from "../oauth-credential.service";
import { Service, Trace } from "../decorators";
import { TwitterAccountInfoService } from "../twitter-account-info.service";
import { TwitterAuthConstants } from "./twitter-auth-constants";
import { UserService } from "../users/user.service";
import TwitterCoreLib from "../users/twitter";

@Service("twitter-auth.service")
export class TwitterAuthService extends BaseService {
  private userService: UserService;
  private oauthCredentialService: OAuthCredentialService;
  private twitterAccountInfoService: TwitterAccountInfoService;

  constructor() {
    super();

    this.userService = new UserService();
    this.oauthCredentialService = new OAuthCredentialService();
    this.twitterAccountInfoService = new TwitterAccountInfoService();
  }

  @Trace()
  public async getOauthRequestToken(): Promise<IOAuthRequestTokenResponse> {
    try {
      const apiURL = `${TwitterAuthConstants.BASE_URL}/request_token`;
      const oauthSignature = this.generateSignature({ oauth_callback: TwitterAuthConstants.CALLBACK_URL }, apiURL);
      const response = await axios.post(apiURL, {}, { headers: { Authorization: `OAuth ${oauthSignature}` } });

      const tokenResponse: IOAuthRequestTokenResponse = <IOAuthRequestTokenResponse>(
        (<any>Object).fromEntries(new URLSearchParams(response.data as string))
      );

      return tokenResponse;
    } catch (error) {
      throw this.handleException(error);
    }
  }

  @Trace()
  public async saveOAuthAccessToken(
    ownerId: Types.ObjectId | string,
    ownerKind: OwnerKind,
    oauthToken: string,
    oauthVerifier: string
  ): Promise<ITwitterAccountInfo> {
    try {
      if (!oauthToken || !oauthVerifier) {
        throw Failure.BadRequest("oauth token are missing");
      }

      const response = await axios.post(
        `${TwitterAuthConstants.BASE_URL}/access_token`,
        {},
        { params: { oauth_token: oauthToken, oauth_verifier: oauthVerifier } }
      );

      const tokenResponse: IOAuthAccessTokenResponse = <IOAuthAccessTokenResponse>(
        (<any>Object).fromEntries(new URLSearchParams(response.data as string))
      );

      const accountInfo = {
        twitterId: tokenResponse.user_id,
        twitterOauthToken: tokenResponse.oauth_token,
        twitterOauthTokenSecret: tokenResponse.oauth_token_secret,
        twitterUsername: tokenResponse.screen_name,
        twitterVerified: true
      };

      this.logger?.info(accountInfo, "Updating Account Info with Twitter OAuth Results");

      // TODO: in the future we can remove this check.
      // we need to move all of front end/mobile to use the new collections first

      const owner = typeof ownerId === "string" ? ownerId : (<Types.ObjectId>ownerId).toHexString();
      if (ownerKind === OwnerKind.USER) {
        // Update user object
        await this.userService.update(ownerId, accountInfo);

        // update the followers
        await TwitterCoreLib.getTwitterFollowersAsync(tokenResponse.screen_name);
      }

      const twitterAccountDetails: any = await TwitterCoreLib.getTwitterAccountDetails(
        tokenResponse.screen_name,
        tokenResponse.oauth_token,
        tokenResponse.oauth_token_secret
      );

      // save the oauth credentials
      const oAuthCredential: Partial<IOAuthCredential> = {
        accessToken: tokenResponse.oauth_token,
        accessTokenSecret: tokenResponse.oauth_token_secret,
        owner,
        ownerKind,
        service: OAuthServiceKind.TWITTER
      };

      await this.oauthCredentialService.create(oAuthCredential as IOAuthCredential);

      // save the twitter account info
      // follower is saved in the `getTwitterFollowers`
      const twitterAccountInfo: Partial<ITwitterAccountInfo> = {
        owner,
        ownerKind,
        name: twitterAccountDetails.name,
        username: tokenResponse.screen_name,
        verified: true,
        followers: twitterAccountDetails.followers_count,
        description: twitterAccountDetails.description,
        verifiedByTwitter: twitterAccountDetails.verified,
        profileImage: twitterAccountDetails.profile_image_url_https,
        profileBannerImage: twitterAccountDetails.profile_banner_url
      };

      await this.twitterAccountInfoService.create(twitterAccountInfo as ITwitterAccountInfo);

      return <ITwitterAccountInfo>twitterAccountInfo;
    } catch (error) {
      throw this.handleException(error);
    }
  }

  private baseString(params: IOAuthParams, separator: string): string {
    return Object.keys(params)
      .sort()
      .map((key: string): string => `${key}=${params[key] as string}`)
      .join(separator);
  }

  private generateSignature(params: Partial<IOAuthParams>, apiURL: string): string {
    const oauthParams: IOAuthParams = {
      ...params,
      oauth_consumer_key: TwitterAuthConstants.TWITTER_API_KEY,
      oauth_version: TwitterAuthConstants.OAUTH_VERSION,
      oauth_signature_method: TwitterAuthConstants.OAUTH_SIGNATURE_METHOD,
      oauth_timestamp: this.timestamp,
      oauth_nonce: this.oauthNonce
    };

    // Generate base string for signature
    const signatureBaseString = `POST&${encodeURIComponent(apiURL)}&${encodeURIComponent(
      this.baseString(oauthParams, "&")
    )}`;

    // Generate oauthSignature
    const oauthSignature = createHmac("sha1", `${encodeURIComponent(TwitterAuthConstants.TWITTER_API_SECRET)}&`)
      .update(signatureBaseString)
      .digest("base64");

    oauthParams.oauth_signature = encodeURIComponent(oauthSignature);

    // Return signature
    return this.baseString(oauthParams, ",");
  }

  private handleException(error: any): Failure {
    if (error.isAxiosError) {
      const { errors } = error?.response?.data;
      return Failure.BadRequest("Error while authenticating twitter account", errors);
    }
    return Failure.InternalServerError(error.message);
  }

  private get oauthNonce(): string {
    return randomBytes(6).toString("hex");
  }

  private get timestamp(): string {
    return (Date.now() / 1000).toFixed();
  }
}
