import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";

import { Service, Trace } from "../decorators";

import { Giphy, GiphyModel } from "../../backend/models/giphy/giphy.model";
import { S3ContentTypes, S3Service, S3_URL } from "../aws/s3.service";
import { IMedia } from "../../backend/interfaces/media";
import { MediaService } from "../media/media.service";
import { BaseModelService } from "../base";

@Service("giphy.service")
export class GiphyService extends BaseModelService<DocumentType<Giphy>> {
  private readonly giphy = GiphyModel;

  private s3Service: S3Service;
  private mediaService: MediaService;

  constructor() {
    super(GiphyModel);

    this.s3Service = new S3Service();
    this.mediaService = new MediaService();
  }

  @Trace()
  public async createOrUpdate(giphyBody: Giphy): Promise<Giphy> {
    const giphyResponse = await this.updateOne(
      { id: giphyBody.id },
      { ...giphyBody, updatedAt: new Date() },
      {
        upsert: true,
        lean: true,
        new: true,
        setDefaultsOnInsert: true
      }
    );
    return giphyResponse;
  }

  /**
   * @summary Download & store giphy as a media object
   *
   * @param {Giphy} giphyBody
   * @param {Types.ObjectId | string} userId
   * @param {string} mediaableType
   * @param {Types.ObjectId | string} mediaable
   *
   * @returns {Promise<IMedia>}
   */
  @Trace()
  public async uploadGiphyAsMedia(
    giphyBody: Giphy,
    userId: Types.ObjectId | string,
    mediaableType: string,
    mediaable?: Types.ObjectId | string
  ): Promise<IMedia> {
    const giphy = await this.createOrUpdate(giphyBody);

    // Download Giphy to store as Media
    const bucketUrl = `uploads/media/${giphy?.slug}.gif`;
    const successfulUpload = await this.s3Service.downloadMediaAndUploadToS3(
      giphy?.images?.downsized_medium?.url || "",
      bucketUrl,
      S3ContentTypes.GIF
    );

    if (!successfulUpload) {
      throw Failure.BadRequest("Something went wrong while uploading Giphy");
    }

    // Store as Media Object
    const mediaParams: IMedia = <IMedia>{
      user: userId,
      mediaableType,
      file: { url: `${S3_URL}/${bucketUrl}`, contentType: S3ContentTypes.GIF, filename: `${giphy?.slug}.gif` }
    };

    if (mediaable) {
      mediaParams.mediaable = mediaable as Types.ObjectId;
    }

    const media = await this.mediaService.create(mediaParams);

    return media;
  }
}
