import { BaseService } from "../base/base.service";
import { Friend, FriendModel } from "../../backend/models/friend";
import { IFriend } from "../../backend/interfaces/friend";
import { Service, Trace } from "../decorators";

@Service("follower.service")
export class FollowerService extends BaseService {
  private db: FriendModel<IFriend> = Friend;

  @Trace()
  public async connected(firstUser: string, secondUser: string): Promise<boolean> {
    const connections = await this.db.count({
      $or: [
        { $and: [{ followee: firstUser }, { follower: secondUser }] },
        { $and: [{ followee: secondUser }, { follower: firstUser }] }
      ]
    });

    return connections > 1;
  }
}
