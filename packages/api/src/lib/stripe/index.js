const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { STRIPE_SECRET_KEY } = require("@efuse/key-store");

// todo: investigate
// @ts-ignore
const stripe = require("stripe")(STRIPE_SECRET_KEY);
const { PaymentProfile } = require("../../backend/models/PaymentProfile");
const { PayoutAccount } = require("../../backend/models/PayoutAccount");
const StripeAsync = require("../../async/core/stripe");
const { initQueue } = require("../../async/config");

/**
 * Async Queue for the updatePaymentProfileAsync function
 */
const updatePaymentProfileAsyncQueue = initQueue("updatePaymentProfile");

/**
 * Async function for updateStripePaymentProfile
 * @param {String} paymentProfileId Id of payment profile
 */
const updatePaymentProfileAsync = async (paymentProfileId) => {
  updatePaymentProfileAsyncQueue.add({ paymentProfileId });
};

/**
 * Initialize the async handlers
 */
const initAsyncProcessors = () => {
  updatePaymentProfileAsyncQueue.process((job) => {
    const { paymentProfileId } = job.data;
    updateStripePaymentProfile(paymentProfileId).catch((error) =>
      logger.error(error, "Error resolving updateStripePaymentProfile promise")
    );
  });
};

const updateStripePaymentProfile = async (paymentProfileId) => {
  try {
    // Get the payment profile for the requested user
    const profile = await PaymentProfile.findById(paymentProfileId);

    // Throw error if profile does not exist
    if (!profile) throw "Could not find PaymentProfile for this id.";

    profile.type = "STRIPE";

    await profile.save();

    logger.info("Successfully saved payment profile");
    return;
  } catch (error) {
    logger.error(error, "Error updating payment profile.");
    throw error;
  }
};

const createStripeCustomer = async (email, name, userId) => {
  try {
    // create new stripe customer with email
    const customer = await stripe.customers.create({
      email,
      name,
      metadata: {
        userId
      }
    });
    // Store stripe customer id in mongo
    await new PaymentProfile({
      user: userId,
      customerId: customer.id,
      type: "STRIPE"
    }).save();
    logger.info(userId, "Created stripe customer");
  } catch (error) {
    logger.error(error, "Stripe Library Error: ");
    throw error;
  }
};

const createStripeCustomerAsync = async (email, name, userId) => {
  return await StripeAsync.createStripeCustomerQueue.add({
    email,
    name,
    userId
  });
};

const getStripeCustomerCards = async (userId) => {
  try {
    // Get the payment profile for the requested user
    const profile = await PaymentProfile.findOne({ user: userId, type: "STRIPE" });

    // Throw error if profile does not exist
    if (!profile) throw "Could not find PaymentProfile for user.";

    // get all cards belonging to a customer
    const cards = await stripe.paymentMethods.list({
      customer: profile.customerId,
      type: "card"
    });

    return cards;
  } catch (error) {
    logger.error(error, "Stripe Library Error: ");
    throw error;
  }
};

const deleteStripeCustomerCard = async (paymentMethodId) => {
  try {
    // detach payment method from customer
    const response = await stripe.paymentMethods.detach(paymentMethodId);
    return response;
  } catch (error) {
    logger.error("Stripe Library Error: ", error);
    throw error;
  }
};

const createStripeCustomerCard = async (userId, token) => {
  try {
    // Get the payment profile for the requested user
    const profile = await PaymentProfile.findOne({ user: userId, type: "STRIPE" });

    // Throw error if profile does not exist
    if (!profile) throw "Could not find PaymentProfile for user.";

    // get all cards belonging to a customer
    const response = await stripe.customers.createSource(profile.customerId, {
      source: token
    });

    return response;
  } catch (error) {
    logger.error("Stripe Library Error: ", error);
    throw error;
  }
};

const createStripePayment = async (userId, source, amount, description) => {
  try {
    // throw error if no source was passed to this function
    if (!source) throw "Source is required for payment";

    // throw error if amount is more than $501 - this is the max amount allowed
    // todo: investigate
    // @ts-ignore
    if (!amount > 501) throw "Charge amount must be less than $501";

    // Get the payment profile for the requested user
    const profile = await PaymentProfile.findOne({ user: userId, type: "STRIPE" });

    // Throw error if profile does not exist
    if (!profile) throw "Could not find PaymentProfile for user.";

    // stripe requires all amounts to be an integer representing cents
    const amountCents = Math.floor(amount * 100);

    // create paymentIntent to charge customer off session
    const response = await stripe.paymentIntents.create({
      amount: amountCents,
      currency: "usd",
      customer: profile.customerId,
      description,
      payment_method: source,
      off_session: true,
      confirm: true
    });

    return response;
  } catch (error) {
    logger.error("Stripe Library Error: ", error);
    throw error;
  }
};

const getStripeSetupIntent = async (userId) => {
  try {
    // Get the payment profile for the requested user
    const profile = await PaymentProfile.findOne({ user: userId, type: "STRIPE" });

    // Throw error if profile does not exist
    if (!profile) throw "Could not find PaymentProfile for user.";

    // Create an setup intent for FE stripe.js
    const intent = await stripe.setupIntents.create({
      customer: profile.customerId
    });

    return intent;
  } catch (error) {
    logger.error("Stripe Library Error: ", error);
    throw error;
  }
};

const oauthToken = async (code, userId) => {
  try {
    // finish oAuth code flow for creating a new stripe connect account
    const response = await stripe.oauth.token({
      grant_type: "authorization_code",
      code
    });

    if (response) {
      // Store newly created stripe connect account in mongo
      await new PayoutAccount({
        stripeAccountId: response.stripe_user_id,
        stripePublishableKey: response.stripe_publishable_key,
        user: userId,
        scope: response.scope,
        accessToken: response.access_token,
        refreshToken: response.refresh_token
      }).save();
    } else {
      throw "Response not found from stripe.oauth.token()";
    }

    // authenicated with newly created stripe connect account

    // todo: investigate
    // @ts-ignore
    var stripeConnect = require("stripe")(response.access_token);

    // add user id to stripe account within account meta data
    await stripeConnect.accounts.update(response.stripe_user_id, {
      metadata: { efuse_user_id: userId }
    });

    return response;
  } catch (error) {
    logger.error("Stripe Library Error: ", error);
    throw error;
  }
};

const getPayoutAccount = async (userId) => {
  try {
    // get all payout account from mongo, if one exists
    const payoutAccount = await PayoutAccount.findOne({
      user: userId,
      active: true
    }).lean();

    // user does not have payout account
    if (!payoutAccount) return null;

    const payoutAccountStripe = await stripe.accounts.retrieve(payoutAccount.stripeAccountId);

    // Create an setup intent for FE stripe.js
    const link = await stripe.accounts.createLoginLink(payoutAccount.stripeAccountId);

    if (!link) throw "Unable to retrieve Stripe Dashboard link";

    // authenicated with newly created stripe connect account

    // todo: investigate
    // @ts-ignore
    var stripeConnect = require("stripe")(payoutAccount.accessToken);

    // get stripe connect account balance
    const balance = await stripeConnect.balance.retrieve();
    // get stripe connect account payouts
    const payouts = await stripeConnect.payouts.list();

    return {
      id: payoutAccount.stripeAccountId,
      scope: payoutAccount.scope,
      link: link.url,
      balance,
      payouts,
      payout_settings: {
        enabled: payoutAccountStripe.payouts_enabled,
        external_account: payoutAccountStripe.external_accounts.data,
        schedule: payoutAccountStripe.settings.payouts.schedule
      }
    };
  } catch (error) {
    logger.error("Stripe Library Error: ", error);
    throw error;
  }
};

module.exports.createStripeCustomer = createStripeCustomer;
module.exports.createStripeCustomerAsync = createStripeCustomerAsync;
module.exports.createStripeCustomerCard = createStripeCustomerCard;
module.exports.createStripePayment = createStripePayment;
module.exports.deleteStripeCustomerCard = deleteStripeCustomerCard;
module.exports.getPayoutAccount = getPayoutAccount;
module.exports.getStripeCustomerCards = getStripeCustomerCards;
module.exports.getStripeSetupIntent = getStripeSetupIntent;
module.exports.initAsyncProcessors = initAsyncProcessors;
module.exports.oauthToken = oauthToken;
module.exports.updatePaymentProfileAsync = updatePaymentProfileAsync;
