export enum CronKindEnum {
  AIMLAB = "aimlab",
  TWITCH = "twitch",
  YOUTUBE = "youtube"
}

export enum CronStatusEnum {
  SUCCESS = "success",
  ERROR = "error",
  PROCESSING = "processing"
}

export enum CronEntityKind {
  USER = "users",
  ORGANIZATION = "organizations"
}
