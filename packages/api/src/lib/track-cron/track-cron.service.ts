import { Types, QueryOptions, FilterQuery } from "mongoose";
import moment, { unitOfTime } from "moment";
import { Queue } from "bull";
import { UserStatus } from "@efuse/entities";

import { IOrganization, ITrackCron, IUser } from "../../backend/interfaces";
import { TrackCron, TrackCronModel } from "../../backend/models/track-cron";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import Config from "../../async/config";
import { CronEntityKind, CronStatusEnum } from "./track-cron.enum";
import { OrganizationPublishStatusEnum } from "../organizations/organization.enum";

@Service("track-cron.service")
export class TrackCronService<T> extends BaseService {
  protected $trackCron: TrackCronModel<ITrackCron> = TrackCron;

  private $subscribeCronQueue: Queue;
  private $unsubscribeCronQueue: Queue;
  private $updateCronQueue: Queue;

  private $kind: string;
  private $crons: string[];
  private $defaultEntityKind: string;
  private $entityFields: string;
  private $limit: number;
  private $interval: number;
  private $unit: unitOfTime.DurationConstructor;

  constructor(
    kind: string,
    crons: string[],
    entityKind: string,
    entityFields: string,
    limit = 15,
    interval = 24,
    unit: unitOfTime.DurationConstructor = "h"
  ) {
    super();

    this.$kind = kind;
    this.$defaultEntityKind = entityKind;
    this.$crons = crons;
    this.$limit = limit;
    this.$interval = interval;
    this.$unit = unit;
    this.$entityFields = entityFields;

    this.$subscribeCronQueue = Config.initQueue(`subscribe${kind}CronQueue`);

    this.$unsubscribeCronQueue = Config.initQueue(`unsubscribe${kind}CronQueue`);

    this.$updateCronQueue = Config.initQueue(`update${kind}CronQueue`);
  }

  public async find(filter: FilterQuery<ITrackCron>): Promise<T[]>;
  public async find(filter: FilterQuery<ITrackCron>, projection: any): Promise<T[]>;
  public async find(
    filter: FilterQuery<ITrackCron>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<T[]>;
  @Trace()
  public async find(
    filter: FilterQuery<ITrackCron>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<T[]> {
    const trackCron = (<unknown>await this.$trackCron.find(filter, projection, options)) as T[];
    return trackCron;
  }

  /**
   * @summary Subscribe to cron
   *
   * @param {Types.ObjectId | string} entity
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async subscribeCron(entity: Types.ObjectId | string, entityKind?: string): Promise<void> {
    const bulkOperations: any[] = this.$crons.map((cron: string) => {
      const query = { entity, kind: this.$kind, entityKind: entityKind || this.$defaultEntityKind, method: cron };
      return this.buildBulkUpdateObject(query);
    });

    const response = await this.$trackCron.bulkWrite(bulkOperations);
    this.logger?.info(
      { upserted: response.upsertedCount, updated: response.modifiedCount, entity },
      `Subscribed ${entityKind || this.$defaultEntityKind} to ${this.$kind} Crons`
    );
  }

  /**
   * @summary Subscribe to cron using bull queue
   *
   * @param {Types.ObjectId | string} entity
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public subscribeCronsAsync(entity: Types.ObjectId | string, entityKind?: string): void {
    this.$subscribeCronQueue
      .add({ entity, entityKind })
      .catch((error) => this.logger?.error(error, "Error while adding to queue subscribeCronQueue"));
  }

  /**
   * @summary Unsubscribe crons
   *
   * @param {Types.ObjectId | string} entity
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async unsubscribeCron(entity: Types.ObjectId | string, entityKind?: string): Promise<void> {
    await this.$trackCron.deleteMany({ entity, entityKind: entityKind || this.$defaultEntityKind, kind: this.$kind });

    this.logger?.info({ entity }, `Unsubscribed ${entityKind || this.$defaultEntityKind} to ${this.$kind} Crons`);
  }

  /**
   * @summary Unsubscribe crons using bull queue
   *
   * @param {Types.ObjectId | string} entity
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public unsubscribeCronsAsync(entity: Types.ObjectId | string, entityKind?: string): void {
    this.$unsubscribeCronQueue
      .add({ entity, entityKind })
      .catch((error) => this.logger?.error(error, "Error while adding to queue unsubscribeCronQueue"));
  }

  /**
   * @summary Get those entities where cron is not run in X interval
   *
   * @param {string} method
   * @param {string} entityFields
   * @param {number} limit
   * @param {number} interval
   * @param {unitOfTime.DurationConstructor} unit
   *
   * @returns {Promise<T[]>}
   */
  public async getEntities(method: string): Promise<T[]>;
  public async getEntities(method: string, entityFields: string): Promise<T[]>;
  public async getEntities(method: string, entityFields: string, limit: number): Promise<T[]>;
  public async getEntities(
    method: string,
    entityFields: string,
    limit: number,
    interval: number,
    unit: unitOfTime.DurationConstructor
  ): Promise<T[]>;
  @Trace()
  public async getEntities(
    method: string,
    entityFields: string = this.$entityFields,
    limit: number = this.$limit,
    interval: number = this.$interval,
    unit: unitOfTime.DurationConstructor = this.$unit
  ): Promise<T[]> {
    const timeAgo: Date = moment().subtract(interval, unit).toDate();

    const trackCrons: T[] = await this.find(
      {
        $and: [
          { method, kind: this.$kind },
          { $or: [{ lastRanAt: { $exists: false } }, { lastRanAt: { $lte: timeAgo } }] }
        ]
      },
      {},
      // todo: investigate how to correctly use `populate`
      <never>{ populate: { path: "entity", select: entityFields }, limit, sort: { lastRanAt: 1 } }
    );

    return trackCrons;
  }

  /**
   * @summary Update a particular object
   *
   * @param {FilterQuery<ITrackCron>} query
   * @param {Partial<ITrackCron>} body
   *
   * @returns {Promise<ITrackCron | null>}
   */
  @Trace()
  public async update(query: FilterQuery<ITrackCron>, body: Partial<ITrackCron>): Promise<ITrackCron | null> {
    const trackCron: ITrackCron | null = await this.$trackCron
      .findOneAndUpdate(query, { ...body }, { new: true })
      .lean();

    return trackCron;
  }

  /**
   * @summary Update a particular object using bull queue
   *
   * @param {FilterQuery<ITrackCron>} query
   * @param {Partial<ITrackCron>} body
   *
   * @returns {void}
   */
  @Trace()
  public updateAsync(query: FilterQuery<ITrackCron>, body: Partial<ITrackCron>): void {
    this.$updateCronQueue
      .add({ query, body })
      .catch((error) => this.logger?.error(error, "Error while adding to queue updateCronQueue"));
  }

  /**
   * @summary Update a particular object using bull queue
   *
   * @param {string} method
   * @param {Types.ObjectId | string} entity
   * @param {Partial<ITrackCron>} body
   *
   * @returns {void}
   */
  @Trace()
  public track(method: string, entity: Types.ObjectId | string, body: Partial<ITrackCron>, entityKind?: string): void {
    const query: FilterQuery<ITrackCron> = {
      method,
      entity,
      entityKind: entityKind || this.$defaultEntityKind,
      kind: this.$kind
    };

    this.updateAsync(query, body);
  }

  /**
   * @summary Track cron success status
   *
   * @param {string} methodName
   * @param {Types.ObjectId | string} entity
   *
   * @returns {void}
   */
  @Trace()
  protected markCronAsSuccess(methodName: string, entity: Types.ObjectId | string, entityKind?: string): void {
    // Update status
    this.track(methodName, entity, { lastStatus: CronStatusEnum.SUCCESS, lastProcessedAt: new Date() }, entityKind);
  }

  /**
   * @summary Track cron failure status
   *
   * @param {string} methodName
   * @param {Types.ObjectId | string} entity
   * @param {string} errorMessage
   *
   * @returns {void}
   */
  @Trace()
  protected markCronAsFailure(
    methodName: string,
    entity: Types.ObjectId | string,
    errorMessage: string,
    entityKind?: string
  ): void {
    // Update status
    this.track(
      methodName,
      entity,
      { lastStatus: CronStatusEnum.ERROR, lastProcessedAt: new Date(), errorMessage },
      entityKind
    );
  }

  /**
   * @summary Track cron processing status
   *
   * @param {string} methodName
   * @param {Types.ObjectId | string} entity
   *
   * @returns {void}
   */
  @Trace()
  protected markCronAsProcessing(methodName: string, entity: Types.ObjectId | string, entityKind?: string): void {
    // Update status
    this.track(
      methodName,
      entity,
      {
        lastStatus: CronStatusEnum.PROCESSING,
        lastRanAt: new Date()
      },
      entityKind
    );
  }

  /**
   * @summary Init Async Processors specific to service
   */
  protected asyncProcessors(): void {
    // subscribeCronQueue
    this.$subscribeCronQueue
      .process((job: { data: { entity: Types.ObjectId | string } }) => {
        return this.subscribeCron(job.data.entity);
      })
      .catch((error) => this.logger?.error(error, `Error while processing subscribe${this.$kind}CronQueue`));

    // unsubscribeCronQueue
    this.$unsubscribeCronQueue
      .process((job: { data: { entity: Types.ObjectId | string } }) => {
        return this.unsubscribeCron(job.data.entity);
      })
      .catch((error) => this.logger?.error(error, `Error while processing unsubscribe${this.$kind}CronQueue`));

    // updateQueue
    this.$updateCronQueue
      .process((job: { data: { query: FilterQuery<ITrackCron>; body: Partial<ITrackCron> } }) => {
        const { query, body } = job.data;
        return this.update(query, body);
      })
      .catch((error) => this.logger?.error(error, `Error while processing update${this.$kind}Queue`));
  }

  /**
   * @summary Validate user object before processing cron
   *
   * @param {string} methodName
   * @param {IUser} user
   *
   * @returns {boolean}
   */
  @Trace()
  protected validateUserEntity(methodName: string, user: IUser): boolean {
    /**
     * Adding these logs to find out junk users & how we can further improve it
     */

    // Make sure user exists
    if (!user) {
      this.logger?.info("User not found");

      return false;
    }

    // Make sure user is not blocked
    if (user.status === UserStatus.BLOCKED) {
      this.logger?.info({ user }, "User is blocked");

      // Update status
      this.markCronAsFailure(methodName, user._id, "User is blocked", CronEntityKind.USER);

      return false;
    }

    // Make sure user is not deleted
    if (user.deleted) {
      this.logger?.info({ user }, "User has been deleted");

      // Update status
      this.markCronAsFailure(methodName, user._id, "User has been deleted", CronEntityKind.USER);

      return false;
    }

    return true;
  }

  /**
   * @summary Validate organization object before processing cron
   *
   * @param {string} methodName
   * @param {IOrganization} organization
   *
   * @returns {boolean}
   */
  @Trace()
  protected validateOrganizationEntity(methodName: string, organization: IOrganization): boolean {
    /**
     * Adding these logs to find out junk organizations & how we can further improve it
     */

    // Make sure organization exists
    if (!organization) {
      this.logger?.info("Organization not found");

      return false;
    }

    // Make sure organization is not deleted
    if (organization.deleted) {
      this.logger?.info({ organization }, "Organization has been deleted");

      // Update status
      this.markCronAsFailure(
        methodName,
        organization._id,
        "Organization has been deleted",
        CronEntityKind.ORGANIZATION
      );

      return false;
    }

    // Make sure organization is visible
    if (organization.publishStatus === OrganizationPublishStatusEnum.VISIBLE) {
      this.logger?.info({ organization }, "Organization is not visible");

      // Update status
      this.markCronAsFailure(methodName, organization._id, "Organization is not visible", CronEntityKind.ORGANIZATION);

      return false;
    }

    return true;
  }

  /**
   * @summary Build bulk update query
   *
   * @param {Record<string, unknown>} query
   *
   * @returns {Record<string, unknown>}
   */
  private buildBulkUpdateObject(query: Record<string, unknown>): Record<string, unknown> {
    return {
      updateOne: {
        filter: query,
        update: { ...query, updatedAt: Date.now() },
        upsert: true, // create new object if it is not already there
        setDefaultsOnInsert: true, // this would set the schema defaults
        ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
      }
    };
  }
}
