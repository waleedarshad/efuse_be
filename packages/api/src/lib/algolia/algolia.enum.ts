export enum AlgoliaIndicesEnum {
  LEARNING_ARTICLES = "learning_articles",
  OPPORTUNITIES = "opportunities",
  ORGANIZATIONS = "organizations",
  USERS = "users",
  GAMES = "games"
}
