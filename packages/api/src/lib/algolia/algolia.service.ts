import algoliasearch, { SearchClient, SearchIndex } from "algoliasearch";
import {
  SearchOptions,
  MultipleQueriesQuery,
  MultipleQueriesOptions,
  MultipleQueriesResponse
} from "@algolia/client-search";
import { RequestOptions } from "@algolia/transporter";
import { ALGOLIA_APP_ID, ALGOLIA_ADMIN_API_KEY } from "@efuse/key-store";
import { OwnerKind } from "@efuse/entities";
import { Queue } from "bull";
import { LeanDocument } from "mongoose";
import moment from "moment";

import { cloneDeep } from "lodash";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { IAlgoliaFormattedOwner, IOrganization, IUser } from "../../backend/interfaces";
import Config from "../../async/config";
import { UserFields } from "../users/user-fields";

const SERVICE = "algolia.service";

@Service(SERVICE)
export class AlgoliaService extends BaseService {
  public $client: SearchClient;

  private $syncObjectWithAlgolia: Queue;
  private $deleteObjectFromAlgoliaQueue: Queue;

  constructor() {
    super();

    this.$client = algoliasearch(ALGOLIA_APP_ID, ALGOLIA_ADMIN_API_KEY);

    this.$syncObjectWithAlgolia = Config.initQueue("syncObjectWithAlgolia");

    this.$deleteObjectFromAlgoliaQueue = Config.initQueue("deleteObjectFromAlgolia");
  }

  /**
   * @summary Sync object with Algolia
   *
   * @param {string} indexName
   * @param {Record<string, unknown>} object
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncObject(indexName: string, object: Record<string, unknown>): Promise<void> {
    try {
      // Initialize index
      const index: SearchIndex = this.$client.initIndex(indexName);

      // Set createdAt as unix timestamp
      if (object.createdAt) {
        object.createdAtUnix = moment(object.createdAt as Date).unix();
      }

      // Set updatedAt as unix timestamp
      if (object.updatedAt) {
        object.updatedAtUnix = moment(object.updatedAt as Date).unix();
      }

      // Sync object with algolia
      await index.saveObject({ ...object, objectID: object._id });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Sync object with Algolia using bull queue
   *
   * @param {string} indexName
   * @param {Record<string, unknown>} object
   *
   * @returns {void}
   */
  @Trace()
  public syncObjectAsync(indexName: string, object: Record<string, unknown>): void {
    this.$syncObjectWithAlgolia
      .add({ indexName, object })
      .catch((error) => this.logger?.error(error, "Error while adding to queue syncObjectWithAlgolia"));
  }

  /**
   * @summary Sync multiple objects with Algolia
   *
   * @param {string} indexName
   * @param {Array<Record<string, unknown>>} objects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncObjects(indexName: string, objects: Array<Record<string, unknown>>): Promise<void> {
    try {
      // Initialize index
      const index: SearchIndex = this.$client.initIndex(indexName);

      const formattedObjects = objects.map((object) => ({ ...object, objectID: object._id }));

      // Sync objects with algolia
      await index.saveObjects(formattedObjects);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Clear objects for a particular index on Algolia
   *
   * @param {string} indexName
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async clearObjects(indexName: string): Promise<void> {
    try {
      // Initialize index
      const index: SearchIndex = this.$client.initIndex(indexName);

      // Clear objects
      await index.clearObjects();
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Delete object from Algolia
   *
   * @param {string} indexName
   * @param {string} objectID
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async deleteObject(indexName: string, objectID: string): Promise<void> {
    try {
      // Initialize index
      const index: SearchIndex = this.$client.initIndex(indexName);

      // Delete object from algolia
      await index.deleteObject(objectID);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Delete object from Algolia using bull queue
   *
   * @param {string} indexName
   * @param {string} objectID
   *
   * @returns {void}
   */
  @Trace()
  public deleteObjectAsync(indexName: string, objectID: string): void {
    this.$deleteObjectFromAlgoliaQueue
      .add({ indexName, objectID })
      .catch((error) => this.logger?.error(error, "Error while adding to queue deleteObjectFromAlgoliaQueue"));
  }

  /**
   * @summary Format owner object to send to algolia
   *
   * @param {IUser | IOrganization | null} owner
   * @param {string} ownerType
   *
   * @returns {IAlgoliaFormattedOwner | null}
   */
  @Trace()
  public formatOwner(owner: IUser | IOrganization | null, ownerType: string): IAlgoliaFormattedOwner | null {
    let formattedOwner: IAlgoliaFormattedOwner | null = null;

    if (owner) {
      if (ownerType === OwnerKind.ORGANIZATION) {
        const organization: IOrganization = owner as IOrganization;

        formattedOwner = {
          _id: organization._id as string,
          name: organization.name,
          username: organization.name, // Passing name as username so that we can index it
          verified: organization.verified?.status || false
        };
      } else {
        const user: IUser = owner as IUser;

        formattedOwner = {
          _id: user._id,
          name: user.name,
          username: user.username,
          verified: user.verified || false
        };
      }
    }

    return formattedOwner;
  }

  /**
   * @summary Build user object to be synced with algolia
   *
   * @param {LeanDocument<IUser>} user
   *
   * @returns {Record<string, unknown>}
   */
  @Trace()
  public buildUserObject(user: LeanDocument<IUser>): Record<string, unknown> {
    // Redact User fields
    for (const field of UserFields.REDACTED_ALGOLIA_FIELDS.split(" ")) {
      if (field in user) {
        delete user[field];
      }
    }

    return {
      ...user,
      socialExperience: user.socialExperience?.slice(0, 5) || [],
      portfolioEvents: user.portfolioEvents?.slice(0, 5) || [],
      portfolioHonors: user.portfolioHonors?.slice(0, 5) || [],
      businessExperience: user.businessExperience?.slice(0, 5) || [],
      educationExperience: user.educationExperience?.slice(0, 5) || [],
      esportsExperience: user.esportsExperience?.slice(0, 5) || []
    };
  }

  /**
   * @summary Search using algolia index
   *
   * @param {string} indexName
   * @param {string} query
   * @param {number} limit
   * @param {string} userId
   * @param {RequestOptions & SearchOptions} requestOptions
   *
   * @returns {Promise<SearchResponse<T>>}
   */
  @Trace()
  public async search<T>(
    indexName: string,
    query: string,
    limit: number,
    userId?: string,
    requestOptions?: RequestOptions & SearchOptions
  ): Promise<T[]> {
    try {
      const index: SearchIndex = this.$client.initIndex(indexName);

      const headers: Record<string, string> | undefined = userId ? { "X-Algolia-UserToken": userId } : undefined;

      let options: RequestOptions & SearchOptions = { hitsPerPage: limit, headers };

      if (requestOptions) {
        options = { ...requestOptions, ...options };
      }

      const response = await index.search<T>(query, options);

      // Fetching the DB object
      const results: T[] = response.hits.map((hit) => {
        const { objectID, _highlightResult, _distinctSeqID, _rankingInfo, _snippetResult, ...dbObject } = hit;

        return (<unknown>this.cleanOIDs(dbObject)) as T;
      });

      return results;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Use algoliaClient.search method for searching multiple queries
   *
   * @param queries
   * @param requestOptions
   * @returns
   */
  @Trace()
  public async nativeSearch<T>(
    queries: readonly MultipleQueriesQuery[],
    requestOptions?: RequestOptions & MultipleQueriesOptions
  ): Promise<MultipleQueriesResponse<T>> {
    const results = await this.$client.search<T>(queries, requestOptions);

    return results;
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const algoliaService = new AlgoliaService();

    // syncObjectWithAlgolia
    algoliaService.$syncObjectWithAlgolia
      .process((job: { data: { indexName: string; object: Record<string, unknown> } }) => {
        const { indexName, object } = job.data;
        return algoliaService.syncObject(indexName, object);
      })
      .catch((error) => algoliaService.logger?.error(error, "Error while processing syncObjectWithAlgolia"));

    // deleteObjectFromAlgoliaQueue
    algoliaService.$deleteObjectFromAlgoliaQueue
      .process((job: { data: { indexName: string; objectID: string } }) => {
        const { indexName, objectID } = job.data;
        return algoliaService.deleteObject(indexName, objectID);
      })
      .catch((error) => algoliaService.logger?.error(error, "Error while processing deleteObjectFromAlgoliaQueue"));
  }

  /**
   * Clean all OIDs set on the object and replaces the property with the OID value
   * Note: this only cleans are the root level of the object.
   */
  @Trace()
  private cleanOIDs(obj: any): any {
    const localObj = cloneDeep(obj);

    for (const property in localObj) {
      if (localObj[property]?.$oid) {
        localObj[property] = localObj[property]?.$oid;
      }
    }

    return localObj;
  }
}
