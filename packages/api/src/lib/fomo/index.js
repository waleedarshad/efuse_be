const { FeatureFlagService } = require("../../lib/feature-flags/feature-flag.service");
const { sendNotification } = require("../notifications/push");
const FomoAsync = require("../../async/notifications/fomo");

/**
 * @summary call sendNotification method to send notification to a user whenever he is followed by another user
 *
 * @param {object} follower - follower information i.e id and name
 * @param {object} followee - followee information i,e id
 */
const fomoNotifyFollowers = async (follower, followee) => {
  const hasFomoFollowerNotification = await FeatureFlagService.hasFeature("fomo_follower_notification", followee.id);
  const title = "Note";
  const message = `You have been followed by ${follower.name}`;
  const type = "success";

  if (hasFomoFollowerNotification) {
    await sendNotification(followee.id, title, message, type);
  }
};

/**
 * @summary Called the async method that creates queue to send notification
 *
 * @param {object} follower - follower information i.e id and name
 * @param {object} followee - followee information i,e id
 */
const initFomoNotifyFollowers = (follower, followee) => {
  FomoAsync.fomoNotifyFollowersAsync(follower, followee);
};

module.exports.initFomoNotifyFollowers = initFomoNotifyFollowers;
module.exports.fomoNotifyFollowers = fomoNotifyFollowers;
