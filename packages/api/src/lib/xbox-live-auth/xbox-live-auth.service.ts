import { DocumentType } from "@typegoose/typegoose";

import { XboxLiveAuth, XboxLiveAuthModel } from "../../backend/models/xbox-live-auth/xbox-live-auth.model";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

@Service("xbox-live-auth")
export class XboxLiveAuthService extends BaseModelService<DocumentType<XboxLiveAuth>> {
  private $xboxModal = XboxLiveAuthModel;
  constructor() {
    super(XboxLiveAuthModel);
  }

  /**
   * @summary Create new xboxlive object
   *
   * @param {DocumentType<XboxLiveAuth>} body
   *
   * @returns {Promise<DocumentType<XboxLiveAuth>>}
   */
  @Trace()
  public async create(body: Partial<DocumentType<XboxLiveAuth>>): Promise<DocumentType<XboxLiveAuth>> {
    const xboxLiveAuth = await this.$xboxModal.create(body);
    return xboxLiveAuth;
  }
}
