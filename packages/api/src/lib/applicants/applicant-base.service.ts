import { QueryOptions, FilterQuery, PaginateResult, PaginateOptions, Types } from "mongoose";
import { FavoriteRelatedModel } from "@efuse/entities";
import { isEmpty } from "lodash";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { Applicant, ApplicantModel } from "../../backend/models/applicant";
import { Favorite, FavoriteModel } from "../../backend/models/favorite";
import { IApplicant, IFavorite, IOpportunity, IUser } from "../../backend/interfaces";
import { ApplicantEntityTypeEnum } from "./applicant.enum";

interface Applicant extends Omit<IApplicant, "user"> {
  user: IUser;
}

@Service("applicant-base.service")
export class ApplicantBaseService extends BaseService {
  protected $applicants: ApplicantModel<IApplicant> = Applicant;
  protected $favorite: FavoriteModel<IFavorite> = Favorite;

  public async find(filter: FilterQuery<IApplicant>): Promise<IApplicant[]>;
  public async find(filter: FilterQuery<IApplicant>, projection?: any): Promise<IApplicant[]>;
  public async find(
    filter: FilterQuery<IApplicant>,
    projection: any,
    options?: QueryOptions | null | undefined
  ): Promise<IApplicant[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IApplicant>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IApplicant[]> {
    const applicants = await this.$applicants.find(filter, projection, options);
    return applicants;
  }

  public async findOne(filter: FilterQuery<IApplicant>): Promise<IApplicant | null>;
  public async findOne(filter: FilterQuery<IApplicant>, projection: any): Promise<IApplicant | null>;
  public async findOne(
    filter: FilterQuery<IApplicant>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IApplicant | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IApplicant>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IApplicant | null> {
    const applicant = await this.$applicants.findOne(filter, projection, options);
    return applicant;
  }

  @Trace()
  public async countDocuments(criteria: FilterQuery<IApplicant>): Promise<number> {
    const count = await this.$applicants.countDocuments(criteria);
    return count;
  }

  /**
   * @summary Get paginated applicants collection
   *
   * @param {string} currentUserId
   * @param {FilterQuery<IApplicant>} filter
   * @param {PaginateOptions} options
   *
   * @returns {Promise<PaginateResult<IApplicant>>}
   */
  @Trace()
  public async getPaginatedApplicants(
    currentUserId: string,
    filter: FilterQuery<IApplicant>,
    options: PaginateOptions
  ): Promise<PaginateResult<IApplicant>> {
    const applicants = await this.$applicants.paginate(filter, options);

    // Check associated favorite applicants
    const updatedApplicants = await this.checkFavorite(applicants.docs, currentUserId);

    applicants.docs = updatedApplicants;

    return applicants;
  }

  /**
   * @summary  Check whether applicants have a associated favorite object
   *
   * @param {any} paginatedApplicants
   * @param {string} currentUserId
   *
   * @returns {Array<IApplicant>}
   */
  @Trace()
  public async checkFavorite(
    paginatedApplicants: Array<IApplicant>,
    currentUserId: string
  ): Promise<Array<IApplicant>> {
    const applicantsWithFavorite: Array<IApplicant> = [];

    for (const applicant of paginatedApplicants) {
      // Set favorite to true
      applicant.favorite = true;

      const filter: FilterQuery<IFavorite> = {
        relatedDoc: <Types.ObjectId | string>applicant._id,
        owner: currentUserId,
        relatedModel: FavoriteRelatedModel.APPLICANTS
      };

      const foundFavorite: IFavorite | null = await this.$favorite.findOne(filter, {}, { lean: true });
      applicant.favorite = !isEmpty(foundFavorite);
      applicantsWithFavorite.push(applicant);
    }

    return applicantsWithFavorite;
  }

  /**
   * @summary Get associated Applicants Avatar
   *
   * @param {IOpportunity} opportunity - Opportunity Object
   *
   * @return {Promise<void>} Opportunity Object with applicants populated
   */
  @Trace()
  public async getAssociatedApplicantsAvatar(opportunity: IOpportunity): Promise<void> {
    const { _id } = opportunity;

    const applicants: Applicant[] = await Applicant.find({
      entity: _id,
      entityType: ApplicantEntityTypeEnum.OPPORTUNITY
    })
      .populate("user", "name profilePicture") // only pull the user's name and profilePicture
      .select("-candidateQuestions") // exclude candidate questions
      .sort({
        createdAt: -1
      })
      .limit(7)
      .lean();

    if (applicants) {
      const mappedApplicants: Applicant[] = [];

      for (const applicant of applicants) {
        if (applicant.user) {
          applicant.profilePicture = applicant.user.profilePicture;
        }
        delete applicant._id;
        mappedApplicants.push(applicant);
      }
      opportunity.applicants = mappedApplicants;
    }
  }
}
