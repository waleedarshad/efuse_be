export enum ApplicantStatusEnum {
  NO_ACTION = "noaction",
  ACCEPTED = "accepted",
  UNDECIDED = "undecided",
  DECLINED = "declined"
}

export enum ApplicantEntityTypeEnum {
  OPPORTUNITY = "opportunities",
  ERENA_TOURNAMENT = "erenatournaments"
}

export enum ApplicantLimitEnum {
  DEFAULT = 10,
  MAX = 25
}
