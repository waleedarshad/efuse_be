export class ApplicantFeatureFlags {
  public static readonly ENABLE_APPLICANT_APPLY_EMAIL = "enable_applicant_apply_email";
  public static readonly ENABLE_APPLICATION_ACCEPTED_EMAIL = "enable_application_accepted_email";
}
