import { Failure } from "@efuse/contracts";
import { groupBy, isEmpty } from "lodash";
import { Types, SchemaType } from "mongoose";

import { BaseService } from "../base/base.service";
import { IOpportunityRequirementsResponse, IOpportunityRequirements, IUser } from "../../backend/interfaces";
import { OpportunityRequirementBaseService } from "../opportunities/opportunity-requirement-base.service";
import { Service, Trace } from "../decorators";
import { UserService } from "../users/user.service";

interface SchemaString extends SchemaType {
  instance: string;
}

@Service("validate-requirements.service")
export class ValidateRequirementsService extends BaseService {
  private $opportunityRequirementBaseService: OpportunityRequirementBaseService;
  private $userService: UserService;

  constructor() {
    super();

    this.$opportunityRequirementBaseService = new OpportunityRequirementBaseService();
    this.$userService = new UserService();
  }

  /**
   * @summary Fetch the opportunity requirements & validate whether user meets it ot not
   *
   * @param {ObjectId} userId - ID of user to be validated
   * @param {ObjectId} requirementIds - IDs of opportunity requirements
   *
   * @return {Object} {canApply: Boolean, requirementsMet: [], requirementsNotMet: []}
   */
  @Trace()
  public async validateByRequirementIds(
    userId: Types.ObjectId | string,
    requirementIds: Array<Types.ObjectId | string>
  ): Promise<IOpportunityRequirementsResponse> {
    const requirementsMet: IOpportunityRequirements[] = [];
    const requirementsNotMet: IOpportunityRequirements[] = [];

    // Fetching Opportunity Requirements
    const opportunityRequirements = await this.$opportunityRequirementBaseService.findByIds(requirementIds);

    // If no requirements are found we allow the applicant to apply
    if (!requirementIds || requirementIds.length === 0) {
      this.logger?.info("No requirements found in opportunity!");
      return { canApply: true, requirementsMet, requirementsNotMet };
    }

    // Segregate between custom rules vs automated rules
    const customRules = opportunityRequirements.filter((or) => or.custom);
    const automatedRules = opportunityRequirements.filter((or) => !or.custom);

    if (customRules.length > 0) {
      // TODO implement custom logic
    }

    if (automatedRules.length > 0) {
      /**
       * Grouping by model name so we only query DB once per each model
       * i.e {User: []}
       */
      const modelNameKeysGroup = groupBy(automatedRules, "target");

      // Loop through each model key, adding DB query & applying validation
      for (const modelNameKey of Object.keys(modelNameKeysGroup)) {
        // modelNameKey is the key i.e User
        this.logger?.info(`Loading model ${modelNameKey}`);

        if (modelNameKey !== "User") {
          this.logger?.error("Functionality not implemented");

          // Skipping the iteration as functionality is not implemented

          continue;
        }

        // Selecting only those fields from DB which are required to be validated
        const projection: Record<string, number> = Object.assign(
          {},
          ...modelNameKeysGroup[modelNameKey]
            .map((obj) => obj.property)
            .join(" ")
            .split(" ")
            .map((prop) => ({ [prop]: 1 }))
        ) as Record<string, number>;

        const dbObject = await this.$userService.findOne({ _id: userId }, { _id: 1, ...projection });

        if (!dbObject) {
          throw Failure.UnprocessableEntity("User not found");
        }

        // Apply validation
        for (const requirement of modelNameKeysGroup[modelNameKey]) {
          // Here we get individual opportunity requirement object
          this.logger?.info(`Validating rule ${requirement.name}`);

          // Splitting to see if requirement is based upon multiple fields
          const properties = requirement.property.split(" ");

          // Validating requirement
          const { failed } = this.implementRulesRecursively(dbObject, properties);

          // Requirement is not met
          if (failed.length > 0) {
            requirementsNotMet.push(requirement);

            continue;
          }

          // Requirement is met
          requirementsMet.push(requirement);
        }
      }
    }

    return {
      canApply: requirementsNotMet.length === 0,
      requirementsMet,
      requirementsNotMet
    };
  }

  /**
   * @summary Check each property recursively
   *
   * @param {IUser} dbObject
   * @param {string[]} properties
   * @param {string[]} passed
   * @param {string[]} failed
   *
   * @returns { failed: string[]; passed: string[] }
   */
  private implementRulesRecursively(
    dbObject: IUser,
    properties: string[] = [],
    passed: string[] = [],
    failed: string[] = []
  ): { failed: string[]; passed: string[] } {
    const property = properties[0];

    // Removing element at Zeroth index
    properties.shift();

    // Do next iteration only if we have properties to verify
    const conditionForNextIteration = properties.length > 0;

    // Get fields from schema
    const schemaPath: SchemaString = dbObject.schema.path(property) as SchemaString;

    // if field doesn't exist in DB
    if (!schemaPath) {
      // We simply skip the iteration
      if (conditionForNextIteration) {
        return this.implementRulesRecursively(dbObject, properties, passed, failed);
      }

      // End recursion if it is the last element
      return { passed, failed };
    }

    // Fetching value even if it is a nested object
    // e.g profilePicture.url

    const value: string | boolean | Array<unknown> = (<unknown>(
      property.split(".").reduce((accumulator, key) => accumulator[key], dbObject)
    )) as string | boolean | Array<unknown>;

    switch (schemaPath.instance) {
      case "Boolean":
        (value as boolean) ? passed.push(property) : failed.push(property);
        break;
      case "String":
        isEmpty(value as string) ? failed.push(property) : passed.push(property);
        break;
      case "Array":
        isEmpty(value) || (value as Array<unknown>).length === 0 ? failed.push(property) : passed.push(property);
        break;
      default:
        // Passing if it hits the default case
        passed.push(property);
        break;
    }
    // Recursively call next step
    if (conditionForNextIteration) {
      return this.implementRulesRecursively(dbObject, properties, passed, failed);
    }

    // End recursion if it is the last element
    return { passed, failed };
  }
}
