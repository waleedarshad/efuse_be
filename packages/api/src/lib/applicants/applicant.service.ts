import { Failure } from "@efuse/contracts";
import { DOMAIN } from "@efuse/key-store";
import { isEmpty } from "lodash";
import { FilterQuery, Types } from "mongoose";

import { ApplicantBaseService } from "./applicant-base.service";
import { ApplicantEntityTypeEnum, ApplicantStatusEnum } from "./applicant.enum";
import { ApplicantFeatureFlags } from "./applicant-feature-flags";
import { BrazeCampaignName } from "../braze/braze.enum";
import { BrazeService } from "../braze/braze.service";
import { ERenaTournamentService } from "../erena/erena-tournament.service";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";
import { IOpportunity, IERenaTournament, IApplicant } from "../../backend/interfaces";
import { OpportunityBaseService } from "../opportunities/opportunity-base.service";
import { OpportunityRankService } from "../opportunities/opportunity-rank.service";
import { Service, Trace } from "../decorators";
import { StreaksService } from "../streaks/streaks.service";
import { ValidateRequirementsService } from "./validate-requirements.service";
import Config from "../../async/config";
import NotificationsHelper from "../../backend/helpers/notifications.helper";

const SERVICE = "applicant.service";

@Service(SERVICE)
export class ApplicantService extends ApplicantBaseService {
  private brazeService: BrazeService;
  private $erenaTournamentService: ERenaTournamentService;
  private $opportunityBaseService: OpportunityBaseService;
  private $validateRequirementsService: ValidateRequirementsService;
  private $opportunityRankService: OpportunityRankService;
  private $streaksService: StreaksService;

  private notifyApplicantQueue: any;

  constructor() {
    super();

    this.brazeService = new BrazeService();
    this.$erenaTournamentService = new ERenaTournamentService();
    this.$opportunityBaseService = new OpportunityBaseService();
    this.$validateRequirementsService = new ValidateRequirementsService();
    this.$opportunityRankService = new OpportunityRankService();
    this.$streaksService = new StreaksService();

    this.notifyApplicantQueue = Config.initQueue("notifyApplicant");
  }

  /**
   * @summary Send email to applicant
   *
   * @param {Types.ObjectId | string} userId
   * @param {BrazeCampaignName} campaignName
   * @param {IOpportunity} opportunity
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async notify(
    userId: Types.ObjectId | string,
    campaignName: BrazeCampaignName,
    opportunity: IOpportunity
  ): Promise<void> {
    try {
      if (!campaignName) {
        throw Failure.BadRequest("campaignName is required");
      }

      this.brazeService.sendCampaignMessageAsync(campaignName, {
        trigger_properties: {
          opportunityTitle: opportunity.title,
          opportunityURL: `${DOMAIN}/o/${opportunity.shortName}`,
          opportunityImage: opportunity.image.url
        },
        recipients: [{ external_user_id: String(userId) }]
      });
    } catch (error) {
      this.logger?.error(error, "Error while sending email to applicant");
    }
  }

  /**
   * @summary Send email to applicant asynchronously
   *
   * @param {Types.ObjectId | string} userId
   * @param {BrazeCampaignName} campaignName
   * @param {IOpportunity} opportunity
   *
   * @return {void}
   */
  @Trace()
  public notifyAsync(
    userId: Types.ObjectId | string,
    campaignName: BrazeCampaignName,
    opportunity: IOpportunity
  ): void {
    this.notifyApplicantQueue.add({ userId, campaignName, opportunity });
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const applicantService = new ApplicantService();

    applicantService.notifyApplicantQueue.process(
      (job: { data: { userId: string; campaignName: BrazeCampaignName; opportunity: IOpportunity } }) => {
        const { userId, campaignName, opportunity } = job.data;
        applicantService.notify(userId, campaignName, opportunity).catch((error) => {
          applicantService.logger?.error(error, "error occurred while processing notifyApplicantQueue");
        });
      }
    );
  }

  /**
   * @summary Get Entity on the basis of entityType
   *
   * @param {Types.ObjectId | string} entityId
   * @param {string} entityType
   *
   * @returns {IOpportunity | IERenaTournament}
   */
  @Trace()
  public async getEntity(
    entityId: Types.ObjectId | string,
    entityType: string
  ): Promise<IOpportunity | IERenaTournament> {
    let entity: IOpportunity | IERenaTournament | null | undefined;

    switch (entityType) {
      case ApplicantEntityTypeEnum.OPPORTUNITY:
        entity = await this.$opportunityBaseService.findOne({ _id: entityId });

        break;
      case ApplicantEntityTypeEnum.ERENA_TOURNAMENT:
        entity = await this.$erenaTournamentService.getByIdOrSlug(entityId as string);

        break;
      default:
        throw Failure.BadRequest("Invalid entityType");
    }

    if (!entity) {
      throw Failure.UnprocessableEntity("Entity not found");
    }

    return entity;
  }

  /**
   * @summary Apply for an opportunity or erenaEvent based on entityType
   *
   * @param {Types.ObjectId | string} userId
   * @param {Partial<IApplicant>} body
   *
   * @returns {Promise<IApplicant>}
   */
  @Trace()
  public async create(userId: Types.ObjectId | string, body: Partial<IApplicant>): Promise<IApplicant> {
    // Make sure ID exists
    if (!userId) {
      throw Failure.BadRequest("userId is required");
    }

    // Make sure body exists
    if (!body || isEmpty(body)) {
      throw Failure.BadRequest("body is required");
    }

    // Make sure entityType exists
    if (!body.entityType) {
      throw Failure.BadRequest("entityType is required");
    }

    // Make sure entityType is supported
    if (!Object.values(ApplicantEntityTypeEnum).includes(body.entityType as ApplicantEntityTypeEnum)) {
      throw Failure.BadRequest("Invalid entityType");
    }

    // Make sure entity exists
    if (!body.entity) {
      throw Failure.BadRequest("entity is required");
    }

    // Make entity is a valid ObjectID
    if (!this.isValidObjectId(body.entity)) {
      throw Failure.BadRequest("Invalid ID");
    }

    // Make sure user has not already applied
    const existingApplicant: IApplicant | null = await this.findOne(
      { user: userId, entity: body.entity, entityType: body.entityType },
      {},
      { autopopulate: false }
    );

    // Return applicant if it already exists
    if (existingApplicant) {
      return existingApplicant;
    }

    // Get entity
    const entity = await this.getEntity(body.entity, body.entityType);

    // Make sure user meets all the requirements
    const { canApply, requirementsMet, requirementsNotMet } =
      await this.$validateRequirementsService.validateByRequirementIds(userId, entity.requirements);

    if (!canApply) {
      throw Failure.BadRequest("Requirements are not met", { requirementsMet, requirementsNotMet });
    }

    // Build Object
    const doc = <FilterQuery<IApplicant>>{ entity: body.entity, entityType: body.entityType, user: userId };

    // Store candidate questions
    if (body.candidateQuestions) {
      doc.candidateQuestions = body.candidateQuestions;
    }

    // Save in DB
    const applicant = await this.$applicants.create(doc);

    // Opportunity Specific stuff
    if (body.entityType === ApplicantEntityTypeEnum.OPPORTUNITY) {
      // Re-calculate opportunity rank
      this.$opportunityRankService.setObjectRankAsync(body.entity);

      /**
       * TODO: The content for the notifications seems to be opportunity specific, we need to refactor
       * it to be generic or create new Braze notifications
       */
      NotificationsHelper.applyNotification(userId, body.entity).catch((error) =>
        this.logger?.error(error, "Error sending notification to applicant")
      );

      // Send Email Notification to applicant
      const enableApplicantApplyEmail = await FeatureFlagService.hasFeature(
        ApplicantFeatureFlags.ENABLE_APPLICANT_APPLY_EMAIL,
        String(userId)
      );

      if (enableApplicantApplyEmail) {
        this.notifyAsync(userId, BrazeCampaignName.OPPORTUNITY_APPLICATION_RECEIVED, entity as IOpportunity);
      }
    }

    // Update streak
    await this.$streaksService.checkStreak(String(userId), "dailyUsage", "Applicant Applied");

    return applicant;
  }

  /**
   * @summary Update applicant status
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} applicantId
   * @param {Types.ObjectId | string} opportunityId
   * @param {boolean} sendEmail
   * @param {string} status
   *
   * @returns  {Promise<IApplicant | null>}
   */
  @Trace()
  public async updateApplicantStatus(
    userId: Types.ObjectId | string,
    applicantId: Types.ObjectId | string,
    opportunityId: Types.ObjectId | string,
    sendEmail: boolean,
    status: string
  ): Promise<IApplicant | null> {
    const opportunity = await this.$opportunityBaseService.findOne({ _id: opportunityId });

    if (!opportunity) {
      throw Failure.BadRequest("Invalid Opportunity ID");
    }

    // todo: investigate & fix
    // @ts-ignore
    if (!opportunity.user.equals(userId)) {
      throw Failure.BadRequest("You are not allowed to perform this action");
    }

    const updatedApplicant: any = await this.$applicants.findOneAndUpdate(
      { _id: applicantId },
      { $set: { status, sendEmail } },
      { new: true }
    );

    // Send email notification to applicant only when application is accepted
    const enableApplicationAcceptedEmail = await FeatureFlagService.hasFeature(
      ApplicantFeatureFlags.ENABLE_APPLICATION_ACCEPTED_EMAIL,
      String(updatedApplicant.user)
    );

    if (enableApplicationAcceptedEmail && sendEmail === true && status === ApplicantStatusEnum.ACCEPTED) {
      this.notifyAsync(updatedApplicant.user, BrazeCampaignName.OPPORTUNITY_APPLICATION_ACCEPTED, opportunity);
    }
    return updatedApplicant;
  }
}
