import { calculatePortfolioProgress } from "../users/portfolio-progress";

describe("calculatePortfolioProgess", () => {
  const userNew = {
    profilePicture: { filename: "mindblown_white.png" },
    bio: "",
    educationExperience: [],
    businessExperience: [],
    businessSkills: [],
    gamingSkills: [],
    portfolioEvents: [],
    portfolioHonors: []
  };

  const userWithPic = {
    ...userNew,
    profilePicture: { filename: "uploadedImage.png" }
  };

  const userWithBio = {
    ...userNew,
    bio: "Sphynx of black quarts, judge my vow!"
  };

  const userWithOther = {
    ...userNew,
    gamingSkills: ["some skill"]
  };

  const userWithCompletePortfolio = {
    ...userNew,
    profilePicture: { filename: "uploadedImage.png" },
    bio: "Sphynx of black quartz, judge my vow!",
    gamingSkills: ["some skill"],
    businessExperience: ["some business exp"]
  };

  const userWithIrrelevantFields = {
    ...userNew,
    someStringField: "test",
    someArrayField: ["test"]
  };

  it.each<[any, number]>([
    [userNew, 0],
    [userWithPic, 33],
    [userWithBio, 33],
    [userWithOther, 33],
    [userWithCompletePortfolio, 100],
    [userWithIrrelevantFields, 0]
  ])("calculates the portfilio progress", (user, expected) => {
    const result = calculatePortfolioProgress(user);
    expect(result).toEqual(expected);
  });
});
