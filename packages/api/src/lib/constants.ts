import { ConstantModel, Constant } from "../backend/models/constant.model";
import { BaseModelService } from "./base";
import { Service, Trace } from "./decorators";
import { DocumentType } from "@typegoose/typegoose";

@Service("constants.service")
export class ConstantsService extends BaseModelService<DocumentType<Constant>> {
  protected readonly $constant = ConstantModel;

  constructor() {
    super(ConstantModel);
  }
  /**
   * @summary Get variables stored in constants by ID
   *
   * @param {String} feature - feature the constants belong to i.e FEED_RANK_ALGORITHM
   * @param {Boolean} cache - query should be cached or not
   * @param {Number} cacheDuration - Cache duration in seconds
   *
   * @return {Any} variables
   */
  @Trace()
  public async getVariables(feature: string, cache = true, cacheDuration = 60): Promise<any> {
    // Fetch constant by feature
    let constantQuery = this.$constant.findOne({ feature }, {}, { lean: true });

    // Apply cache
    if (cache) {
      constantQuery = constantQuery.cache(cacheDuration);
    }

    // Return variables
    const constant = await constantQuery;

    return constant?.variables;
  }
}
