import { Failure } from "@efuse/contracts";
import { isEmpty, uniq } from "lodash";
import { Types } from "mongoose";

import { BaseService } from "./base";
import { Service } from "./decorators";

import { TwitchClipService } from "./twitch/twitch-clip.service";
import { MediaService } from "./media/media.service";
import { GiphyService } from "./giphy/giphy.service";
import { S3Service } from "./aws/s3.service";
import { YoutubeVideoService } from "./youtube/youtube-video.service";

import { Giphy } from "../backend/models/giphy/giphy.model";
import { IMedia } from "../backend/interfaces/media";
import { File } from "../backend/interfaces/file";

import * as BlacklistLib from "./blacklist/blacklist";

interface IResponse {
  giphy?: Giphy;
  media: IMedia;
  mediaObjects: IMedia[];
}

@Service("feeds-base.service")
export class FeedsBaseService extends BaseService {
  protected twitchClipService: TwitchClipService;
  protected mediaService: MediaService;
  protected giphyService: GiphyService;
  protected s3Service: S3Service;
  protected $youtubeVideoService: YoutubeVideoService;

  constructor() {
    super();

    this.twitchClipService = new TwitchClipService();
    this.mediaService = new MediaService();
    this.giphyService = new GiphyService();
    this.s3Service = new S3Service();
    this.$youtubeVideoService = new YoutubeVideoService();
  }

  public async validateAndCreateAttachments(
    userId: Types.ObjectId,
    username: string,
    content?: string,
    twitchClipId?: string,
    file?: File,
    files?: File[],
    giphyParams?: Giphy,
    youtubeVideoId?: Types.ObjectId | string
  ): Promise<IResponse> {
    const response: IResponse = <IResponse>{};

    // Make sure content is not blacklisted
    if (content) {
      const blacklisted = await BlacklistLib.validateFeedContent(content, username);
      if (blacklisted) {
        throw Failure.BadRequest("Post has been blacklisted");
      }
    }

    // Make sure user owns twitch clip
    if (twitchClipId) {
      // Make sure it is a valid ObjectId
      if (!Types.ObjectId.isValid(twitchClipId)) {
        throw Failure.BadRequest("invalid twitchClip ID");
      }

      const existingTwitchClip = await this.twitchClipService.findOne(
        { _id: twitchClipId, user: userId },
        { _id: 1 },
        { lean: true }
      );

      if (!existingTwitchClip) {
        throw Failure.UnprocessableEntity("Twitch clip not found");
      }
    }

    if (file && file.url) {
      const mediaParams: IMedia = <IMedia>{ user: userId, mediaableType: "feeds", file };
      response.media = await this.mediaService.create(mediaParams);
    }

    if (files && this.isValid(files)) {
      const mediaFiles: IMedia[] = [];
      files.forEach((mediaFile) => {
        if (mediaFile && mediaFile.url) {
          const mediaParams: IMedia = <IMedia>{ user: userId, mediaableType: "feeds", file: mediaFile };
          mediaFiles.push(mediaParams);
        }
      });
      const mediaObjects = await this.mediaService.createMediaObjects(mediaFiles);
      response.mediaObjects = mediaObjects;
    }

    if (giphyParams && !isEmpty(giphyParams) && isEmpty(response.media) && isEmpty(response.mediaObjects)) {
      const media = await this.giphyService.uploadGiphyAsMedia(giphyParams, userId, "feeds");
      response.media = media;
    }

    // Validate youtube ownership
    if (youtubeVideoId) {
      await this.$youtubeVideoService.validateYoutubeOwnership(userId, youtubeVideoId);
    }

    return response;
  }

  public isValid(files: File[]): boolean {
    const contentTypes = uniq(files.map((k) => k.contentType)) as string[];
    const fileType = contentTypes[0];

    if (files.length === 0) {
      return false;
    }
    if (contentTypes.length > 1) {
      throw Failure.BadRequest("Only one video or up to 4 images are allowed at a time");
    }
    if (fileType && fileType.startsWith("image") && files.length > 4) {
      throw Failure.BadRequest("4 images are allowed at a time");
    }
    if (fileType && fileType.startsWith("video") && files.length > 1) {
      throw Failure.BadRequest("Only one video is allowed at a time");
    }
    return true;
  }

  public isfile(file: never): boolean {
    return file && (this.isEmpty(file) || (Array.isArray(file) && this.isEmpty(file[0])));
  }

  private isEmpty(obj: never): boolean {
    return Object.keys(obj).length === 0;
  }
}
