import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { LandingPageModel, LandingPage } from "../../backend/models/landing-page.model";

@Service("landing-page.service")
export class LandingPageService extends BaseService {
  private readonly $landingPage = LandingPageModel;

  public async findOne(filter: FilterQuery<DocumentType<LandingPage>>): Promise<DocumentType<LandingPage> | null>;
  public async findOne(
    filter: FilterQuery<DocumentType<LandingPage>>,
    projection: any
  ): Promise<DocumentType<LandingPage> | null>;
  public async findOne(
    filter: FilterQuery<DocumentType<LandingPage>>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<DocumentType<LandingPage> | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<DocumentType<LandingPage>>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<DocumentType<LandingPage> | null> {
    const landingPage = await this.$landingPage.findOne(filter, projection, options);
    return landingPage;
  }

  /**
   * @summary Get a landing page object by slug
   *
   * @param {string} slug
   *
   * @return {DocumentType<LandingPage>}
   */
  @Trace()
  public async findOneBySlug(slug: string): Promise<DocumentType<LandingPage>> {
    if (!slug) {
      throw Failure.BadRequest("slug is not found");
    }

    const landingPage = await this.findOne({ slug, active: true });

    if (!landingPage) {
      throw Failure.UnprocessableEntity("No active landing page found against slug");
    }

    return landingPage;
  }
}
