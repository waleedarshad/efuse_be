import { LandingPageService } from "./landing-page.service";
import { landingPageMock } from "../../graphql/schema/test-utils/mock-landing-page";

describe("landing-page.service", () => {
  let subject: LandingPageService;

  const page = landingPageMock();
  const slug = page.slug;

  beforeEach(() => {
    subject = new LandingPageService();
  });

  it("ensures findOne is being called with the correct parameters", async () => {
    LandingPageService.prototype.findOne = jest.fn().mockResolvedValue(true);
    await subject.findOneBySlug(slug);

    expect(LandingPageService.prototype.findOne).toHaveBeenCalledWith({ slug: slug, active: true });
  });
});
