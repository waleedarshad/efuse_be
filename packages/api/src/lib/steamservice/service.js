const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/steamservice/index.js" });
const axiosLib = require("axios");

// todo: investigate
// @ts-ignore
const axios = axiosLib.create({
  baseURL: "http://api.steampowered.com"
});

const { STEAM_API_KEY } = require("@efuse/key-store");

const GAME_DATA_ENDPOINT = "/IPlayerService";
const USER_DATA_ENDPOINT = "/ISteamUser";
const USER_STATS_ENDPOINT = "/ISteamUserStats";

function Steam(steamid) {
  this.steamid = steamid;
}

Steam.prototype = {
  async setPlayerSummaries() {
    return this.makeRequest(
      `${USER_DATA_ENDPOINT}/GetPlayerSummaries/v0002/?steamids=${this.steamid}&`,
      "playerSummary"
    );
  },
  async setRecentlyPlayedGames() {
    return this.makeRequest(
      `${GAME_DATA_ENDPOINT}/GetRecentlyPlayedGames/v0001/?steamid=${this.steamid}&`,
      "recentlyPlayedGames"
    );
  },
  async setFriendList() {
    return this.makeRequest(
      `${USER_DATA_ENDPOINT}/GetFriendList/v0001/?steamid=${this.steamid}&relationship=friend&`,
      "steamFriend",
      "friendslist"
    );
  },
  async setPlayerAchievements(gameId) {
    return this.makeRequest(
      `${USER_STATS_ENDPOINT}/GetPlayerAchievements/v0001/?steamid=${this.steamid}&appid=${gameId}&`,
      "steamAchievement"
    );
  },
  async setOwnedGame() {
    return this.makeRequest(
      `${GAME_DATA_ENDPOINT}/GetOwnedGames/v0001?steamid=${this.steamid}&include_appinfo=true&`,
      "steamOwnedGame"
    );
  },
  async makeRequest(url, key, responseKey = "response") {
    const endPoint = `${url}key=${STEAM_API_KEY}&format=json`;
    try {
      return new Promise(async (resolve, reject) => {
        try {
          const response = await axios.get(endPoint);

          this[key] = response.data[responseKey];
        } catch (error) {
          // Steam throws a 401 error when user's profile is not public
          if (error.response?.status === 401) {
            return resolve(this);
          }

          return reject(error.response?.data);
        }
        resolve(this);
      });
    } catch (error) {
      logger.error(error, "Error while hitting steam API");
      return error;
    }
  }
};

module.exports = Steam;
