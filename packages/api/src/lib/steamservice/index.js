const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/steamservice/index.js" });
const { isEmpty } = require("lodash");
const { Failure } = require("@efuse/contracts");

const { User } = require("../../backend/models/User");
const { Steam } = require("../../backend/models/Steam");
const { SteamFriend } = require("../../backend/models/SteamFriend");
const { SteamOwnedGame } = require("../../backend/models/SteamOwnedGame");

const SteamService = require("./service");
const SteamServiceAsync = require("../../async/steam-service");

const syncSteamProfile = async (userId) => {
  try {
    const user = await User.findOne({ _id: userId }).select("_id steamId steam");

    if (!user) {
      throw Failure.UnprocessableEntity("User not found");
    }

    const logMsg = `[${user._id}]`;
    const steam = new SteamService(user.steamId);
    await steam.setPlayerSummaries();
    await steam.setRecentlyPlayedGames();
    if (isEmpty(steam)) {
      logger.info({ userId }, "Steam profile info not found!");
      return;
    }

    // todo: investigate
    // @ts-ignore
    const { recentlyPlayedGames, playerSummary } = steam;
    let formattedGames = [];
    if (!isEmpty(recentlyPlayedGames) && !isEmpty(recentlyPlayedGames.games)) {
      formattedGames = recentlyPlayedGames.games.map((game) => {
        const mediaUrl = `http://media.steampowered.com/steamcommunity/public/images/apps/${game.appid}/`;
        return {
          appid: game.appid,
          name: game.name,
          playtime2weeks: game.playtime_2weeks,
          playtimeForever: game.playtime_forever,
          imgIconUrl: `${mediaUrl}${game.img_icon_url}.jpg`,
          imgLogoUrl: `${mediaUrl}${game.img_logo_url}.jpg`,
          playtimeWindowsForever: game.playtime_windows_forever,
          playtimeMacForever: game.playtime_mac_forever,
          playtimeLinuxForever: game.playtime_linux_forever
        };
      });
    }
    let playerSummaryObject = {};
    let lastlogoff = null;
    let timecreated = null;
    if (!isEmpty(playerSummary) && playerSummary.players.length > 0) {
      playerSummaryObject = {
        ...playerSummary.players[0]
      };
      if (playerSummaryObject.lastlogoff) {
        lastlogoff = new Date(playerSummaryObject.lastlogoff * 1000);
      }
      if (playerSummaryObject.timecreated) {
        timecreated = new Date(playerSummaryObject.timecreated * 1000);
      }
    }
    playerSummaryObject = {
      ...playerSummaryObject,
      lastlogoff,
      timecreated,
      recentlyPlayedGames: formattedGames
    };
    if (user.steam) {
      return await Steam.findOneAndUpdate({ _id: user.steam }, { $set: playerSummaryObject });
    }
    logger.info(`${logMsg} Creating new steam playerSummary`);
    const newSteamObject = await new Steam(playerSummaryObject).save();
    user.steam = newSteamObject._id;
    await user.save({ validateBeforeSave: false });
    logger.info(`${logMsg} Steam profile Synced`);
    return;
  } catch (error) {
    logger.error(error, `[${userId}] Error syncing steam profile`);
    return;
  }
};

const syncSteamProfileAsync = async (userId) => {
  return await SteamServiceAsync.syncSteamProfileQueue.add({ userId });
};

const syncSteamProfileCron = async () => {
  try {
    const users = await getUsersWithSteamAccount();

    if (users) {
      for (const user of users) {
        await syncSteamProfileAsync(user._id);
      }

      logger.info("All jobs added to queue to sync steam profile");
    }
  } catch (error) {
    logger.error(error, "Error adding jobs to sync steam profile");
  }
};

const syncSteamFriends = async (userId) => {
  try {
    const user = await User.findOne({ _id: userId }).select("_id steamId steam").lean();

    if (!user) {
      throw Failure.UnprocessableEntity("User not found");
    }

    const steam = new SteamService(user.steamId);
    await steam.setFriendList();
    if (isEmpty(steam)) {
      logger.info({ userId }, "Steam not found!");
      return;
    }

    // todo: investigate
    // @ts-ignore
    if (isEmpty(steam.steamFriend) || isEmpty(steam.steamFriend.friends)) {
      logger.info({ userId }, "Steam friendlist not found!");
      return;
    }
    // todo: investigate
    // @ts-ignore
    const { friends } = steam.steamFriend;
    const formattedData = friends.map((friend) => {
      return {
        updateOne: {
          filter: { steamid: friend.steamid, user: user._id },
          update: {
            steamid: friend.steamid,
            relationship: friend.relationship,
            friendSince: new Date(friend.friend_since * 1000),
            user: user._id,
            steam: user.steam,
            updatedAt: Date.now()
          },
          upsert: true,
          setDefaultsOnInsert: true
        }
      };
    });
    const res = await SteamFriend.bulkWrite(formattedData);
    logger.info({ upserted: res.upsertedCount, updated: res.modifiedCount }, `Steam friends synced for user ${userId}`);
    return;
  } catch (error) {
    logger.error(error, "Error 2");
    logger.error(error, `[${userId}] Error syncing steam friends`);
    return;
  }
};

const syncSteamFriendsAsync = async (userId) => {
  return await SteamServiceAsync.syncSteamFriendsQueue.add({ userId });
};

const syncSteamFriendsCron = async () => {
  try {
    const users = await getUsersWithSteamAccount();

    if (users) {
      for (const user of users) {
        await syncSteamFriendsAsync(user._id);
      }

      logger.info("All jobs added to queue to sync steam friends");
    }
  } catch (error) {
    logger.error(error, "Error adding jobs to sync steam friends");
  }
};

const syncSteamGames = async (userId) => {
  try {
    const user = await User.findOne({ _id: userId }).select("_id steamId steam").lean();

    if (!user) {
      throw Failure.UnprocessableEntity("User not found");
    }

    const steam = new SteamService(user.steamId);
    await steam.setOwnedGame();
    if (isEmpty(steam)) {
      logger.info({ userId }, "Steam not found!");
      return;
    }

    // todo: investigate
    // @ts-ignore
    if (isEmpty(steam.steamOwnedGame) || isEmpty(steam.steamOwnedGame.games)) {
      logger.info({ userId }, "Steam games not found!");
      return;
    }
    // todo: investigate
    // @ts-ignore
    const { games } = steam.steamOwnedGame;
    const formattedData = games.map((game) => {
      const mediaUrl = `http://media.steampowered.com/steamcommunity/public/images/apps/${game.appid}/`;
      return {
        updateOne: {
          filter: { appid: game.appid, user: user._id },
          update: {
            appid: game.appid,
            name: game.name,
            playtimeForever: game.playtime_forever,
            imgIconUrl: `${mediaUrl}${game.img_icon_url}.jpg`,
            imgLogoUrl: `${mediaUrl}${game.img_logo_url}.jpg`,
            hasCommunityVisibleStats: game.has_community_visible_stats,
            playtimeWindowsForever: game.playtime_windows_forever,
            playtimeMacForever: game.playtime_mac_forever,
            playtimeLinuxForever: game.playtime_linux_forever,
            user: user._id,
            steam: user.steam,
            updatedAt: Date.now()
          },
          upsert: true,
          setDefaultsOnInsert: true
        }
      };
    });
    const res = await SteamOwnedGame.bulkWrite(formattedData);
    return;
  } catch (error) {
    logger.error(error, `[${userId}] Error syncing steam games`);
    return;
  }
};

const syncSteamGamesAsync = async (userId) => {
  return await SteamServiceAsync.syncSteamGamesQueue.add({ userId });
};

const syncSteamGamesCron = async () => {
  try {
    const users = await getUsersWithSteamAccount();

    if (users) {
      for (const user of users) {
        await syncSteamGamesAsync(user._id);
      }

      logger.info("All jobs added to queue to sync steam games");
    }
  } catch (error) {
    logger.error(error, "Error adding jobs to sync steam games");
  }
};

const getUsersWithSteamAccount = async () => {
  // todo: investigate
  // @ts-ignore
  const users = await User.find({ steamId: { $exists: true, $nin: ["", null] } })
    .select("_id")
    .lean();

  if (isEmpty(users)) {
    logger.info("No users found with steam account");
    return;
  }

  return users;
};

module.exports.getUsersWithSteamAccount = getUsersWithSteamAccount;
module.exports.syncSteamFriends = syncSteamFriends;
module.exports.syncSteamFriendsAsync = syncSteamFriendsAsync;
module.exports.syncSteamFriendsCron = syncSteamFriendsCron;
module.exports.syncSteamGames = syncSteamGames;
module.exports.syncSteamGamesAsync = syncSteamGamesAsync;
module.exports.syncSteamGamesCron = syncSteamGamesCron;
module.exports.syncSteamProfile = syncSteamProfile;
module.exports.syncSteamProfileAsync = syncSteamProfileAsync;
module.exports.syncSteamProfileCron = syncSteamProfileCron;
