const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { RECURLY_PRIVATE_API_KEY } = require("@efuse/key-store");
const recurly = require("recurly");

const client = new recurly.Client(RECURLY_PRIVATE_API_KEY);
const { PaymentProfile } = require("../../backend/models/PaymentProfile");
const { initQueue } = require("../../async/config");

const createRecurlyAccount = async (userId, firstName, lastName, email) => {
  try {
    const newRecurlyAccountObj = {
      code: userId,
      firstName: firstName,
      lastName: lastName,
      email: email
    };

    // Create recurly account
    const account = await client.createAccount(newRecurlyAccountObj);

    if (!account.id) throw "Failed to create recurly account";

    // Store recurly customer id in db
    const profile = await new PaymentProfile({
      user: userId,
      customerId: account.id,
      type: "RECURLY"
    }).save();

    if (!profile) throw "Failed to create payment profile";

    logger.info("Created Recurly account and payment profile");

    return account;
  } catch (err) {
    if (err instanceof recurly.errors.ValidationError) {
      // If the request was not valid, you may want to tell your user
      // why. You can find the invalid params and reasons in err.params
      logger.error(err, "Recurly Failed validation");
      throw "Recurly failed validation";
    } else {
      // If we don't know what to do with the err, we should
      // probably re-raise and let our web framework and logger handle it
      logger.error(err, "Recurly Failed validation");
      throw "Unknown Recurly validation error";
    }
  }
};

const getRecurlyAccount = async (userId) => {
  try {
    logger.info("Attempting to get Recurly Account for user: ", userId);

    const paymentProfile = await PaymentProfile.findOne({
      user: userId,
      type: "RECURLY"
    }).lean();

    if (!paymentProfile) throw "Recurly account has not been created.";

    const account = await client.getAccount(paymentProfile.customerId);

    if (!account) throw "Failed to get Recurrly account.";

    return account;
  } catch (err) {
    if (err instanceof recurly.errors.ValidationError) {
      // If the request was not valid, you may want to tell your user
      // why. You can find the invalid params and reasons in err.params
      logger.error(err, "Recurly Failed validation");
      throw "Recurly failed validation";
    } else {
      // If we don't know what to do with the err, we should
      // probably re-raise and let our web framework and logger handle it

      console.log(err);
      logger.error(err, "Recurly Failed validation");
      throw "Unknown Recurly validation error";
    }
  }
};

/**
 * Async Queue for the createRecurlyAccountAsync function
 */
const createRecurlyAccountAsyncQueue = initQueue(
  "createRecurlyAccount",
  { limiter: { max: 6, duration: 1000 } } // Recurly rate limit is 400 req/min ~= 6.6 req/sec
);

/**
 * Async function for createRecurlyAccount
 * @param {String} userId Id of user
 * @param {String} firstName First name of user
 * @param {String} lastName First name of user
 * @param {String} email Email of user
 */
const createRecurlyAccountAsync = async (userId, firstName, lastName, email) => {
  createRecurlyAccountAsyncQueue.add({ userId, firstName, lastName, email });
};

/**
 * Initialize the async handlers
 */
const initAsyncProcessors = () => {
  createRecurlyAccountAsyncQueue
    .process((job) => {
      const { userId, firstName, lastName, email } = job.data;
      return createRecurlyAccount(userId, firstName, lastName, email);
    })
    .catch((error) => logger.error(error, "Error resolving createRecurlyAccountAsyncQueue promise"));
};

module.exports.createRecurlyAccount = createRecurlyAccount;
module.exports.createRecurlyAccountAsync = createRecurlyAccountAsync;
module.exports.getRecurlyAccount = getRecurlyAccount;
module.exports.initAsyncProcessors = initAsyncProcessors;
