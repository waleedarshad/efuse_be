import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions, Types, UpdateQuery } from "mongoose";

import { BaseService } from "../base/base.service";
import { ILabsFeature } from "../../backend/interfaces";
import { LabsFeature, LabsModel } from "../../backend/models/labs-feature";
import { Service, Trace } from "../decorators";

@Service("labsservice")
export class LabsService extends BaseService {
  private model: LabsModel<ILabsFeature> = LabsFeature;

  @Trace()
  public async create(item: ILabsFeature): Promise<ILabsFeature[]> {
    if (!item) {
      throw Failure.BadRequest("Unable to create item, missing 'item'");
    }

    const doc = <ILabsFeature>item;
    const result = await this.model.create(doc);

    return [result];
  }

  @Trace()
  public async delete(id: string): Promise<ILabsFeature[]> {
    if (!id) {
      throw Failure.BadRequest("Unable to create item, missing 'id'");
    }

    const identifier: Types.ObjectId = this.toObjectId(id);
    const item: ILabsFeature | null = await this.model.findByIdAndRemove(identifier);

    if (!item) {
      throw Failure.UnprocessableEntity("Unable to find requested item");
    }

    return [item];
  }

  public async find(filter: FilterQuery<ILabsFeature>): Promise<ILabsFeature[]>;
  public async find(filter: FilterQuery<ILabsFeature>, projection?: any): Promise<ILabsFeature[]>;
  @Trace()
  public async find(
    filter: FilterQuery<ILabsFeature>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<ILabsFeature[]> {
    const items: ILabsFeature[] = await this.model.find(filter, projection, options).lean();
    return items;
  }

  @Trace()
  public async findById(id: string): Promise<ILabsFeature[]> {
    if (!id) {
      throw Failure.BadRequest("Unable to find item, missing 'id'");
    }

    const identifier: Types.ObjectId = this.toObjectId(id);
    const item: ILabsFeature | null = await this.model.findOne({ _id: identifier }).lean();

    if (!item) {
      throw Failure.UnprocessableEntity("Unable to find requested item");
    }

    return [item];
  }

  public async findOne(filter: FilterQuery<ILabsFeature>): Promise<ILabsFeature[]>;
  public async findOne(filter: FilterQuery<ILabsFeature>, projection?: any): Promise<ILabsFeature[]>;
  @Trace()
  public async findOne(
    filter: FilterQuery<ILabsFeature>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ILabsFeature[]> {
    const item: ILabsFeature | null = await this.model.findOne(filter, projection, options);

    if (!item) {
      throw Failure.UnprocessableEntity("Unable to find requested item");
    }

    return [item];
  }

  @Trace()
  public async retrieve(): Promise<ILabsFeature[]> {
    const items = await this.model.find({});

    if (!items) {
      throw Failure.UnprocessableEntity("Unable to retrieve");
    }

    return items;
  }

  @Trace()
  public async update(id: string, item: UpdateQuery<ILabsFeature>): Promise<ILabsFeature[]> {
    if (!id) {
      throw Failure.BadRequest("Unable to update item, missing 'id'");
    }

    if (!item) {
      throw Failure.BadRequest("Unable to update item, missing 'item'");
    }

    const identifier: Types.ObjectId = this.toObjectId(id);
    const updated: ILabsFeature | null = await this.model
      .findOneAndUpdate({ _id: identifier }, { ...item }, { new: true })
      .lean();

    if (!updated) {
      throw Failure.UnprocessableEntity("Unable to update requested item");
    }

    return [updated];
  }

  @Trace()
  private toObjectId(id: string): Types.ObjectId {
    return Types.ObjectId.createFromHexString(id);
  }
}
