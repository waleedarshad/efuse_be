import { Failure } from "@efuse/contracts";
import { Model, FilterQuery, QueryOptions } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { Secure } from "../../backend/models/Secure";
import { ISecure } from "../../backend/interfaces/secure";

@Service("secure.service")
export class SecureService extends BaseService {
  private $secureModel: Model<ISecure> = Secure;

  public async findOne(filter: FilterQuery<ISecure>): Promise<ISecure | null>;
  public async findOne(filter: FilterQuery<ISecure>, projection: any): Promise<ISecure | null>;
  public async findOne(
    filter: FilterQuery<ISecure>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<ISecure | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<ISecure>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ISecure | null> {
    const secure = await this.$secureModel.findOne(filter, projection, options);
    return secure;
  }

  /**
   * @summary Update a particular secure object
   *
   * @param {FilterQuery<ISecure>} filter
   * @param {Partial<ISecure>} secureBody
   *
   * @returns {Promise<ISecure>}
   */
  @Trace()
  public async update(filter: FilterQuery<ISecure>, secureBody: Partial<ISecure>): Promise<ISecure> {
    // Make sure params are passed
    if (!secureBody) {
      throw Failure.BadRequest("secureBody is missing");
    }

    // Update the corresponding object as per condition
    const updateSecureObject: ISecure | null = await this.$secureModel
      .findOneAndUpdate(filter, { ...secureBody }, { new: true })
      .lean();

    // Make sure secure object exists
    if (!updateSecureObject) {
      throw Failure.UnprocessableEntity("secure object not found");
    }

    return updateSecureObject;
  }
}
