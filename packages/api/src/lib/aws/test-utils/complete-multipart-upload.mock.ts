import { mock } from "jest-mock-extended";
import S3 from "aws-sdk/clients/s3";

import { UUID } from "./constants.mock";

export const completeMultipartUploadOutputMock = (
  params: S3.CompleteMultipartUploadRequest
): S3.CompleteMultipartUploadOutput => {
  const output = mock<S3.CompleteMultipartUploadOutput>();

  output.Bucket = params.Bucket;
  output.Key = params.Key;
  output.ETag = UUID;
  output.Location = `https://${params.Bucket}.s3.amazonaws.com/${params.Key}`;

  return output;
};
