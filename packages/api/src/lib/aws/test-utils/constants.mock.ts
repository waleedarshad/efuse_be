import { AWS_S3_BUCKET } from "../../../config";

Date.now = jest.fn(() => 1634882810578);

export const UUID = "a6af85ee-9fcf-48e1-bbb1-3d4cc40df2b3";
export const BUCKET = AWS_S3_BUCKET;
export const UPLOAD_ID = "ibZBv_75gd9r8lH_gqXatLdxMVpAlj6ZQjEs.OwyF3953YdwbcQnMA2BLGn8Lx12fQNICtMw5KyteFeHw.Sjng--";
export const EXTENSION = "mp4";
export const MOCKED_FILE_NAME = `${UUID}-${Date.now()}.${EXTENSION}`;
