import { mock } from "jest-mock-extended";
import S3 from "aws-sdk/clients/s3";

import { UPLOAD_ID, BUCKET, MOCKED_FILE_NAME } from "./constants.mock";

export const createMultipartUploadOutputMock = (): S3.CreateMultipartUploadOutput => {
  const output = mock<S3.CreateMultipartUploadOutput>();

  output.UploadId = UPLOAD_ID;
  output.Bucket = BUCKET;
  output.Key = MOCKED_FILE_NAME;

  return output;
};
