export const buildSignedURL = (operation: string, params: any): string =>
  `https://${params.Bucket}.s3.amazonaws.com/${params.Key}?UploadId=${params.UploadId}&PartNumber=${params.PartNumber}&Operation=${operation}`;
