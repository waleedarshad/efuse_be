import { Failure } from "@efuse/contracts";
import { mock } from "jest-mock-extended";
import S3 from "aws-sdk/clients/s3";

import { S3Service, S3SignedURLOperations } from "./s3.service";
import { S3MediaDirectory } from "../media/transcode-media.enum";
import {
  createMultipartUploadOutputMock,
  buildSignedURL,
  UUID,
  BUCKET,
  EXTENSION,
  MOCKED_FILE_NAME,
  UPLOAD_ID,
  completeMultipartUploadOutputMock
} from "./test-utils";

const MockedMultipartOutput = createMultipartUploadOutputMock();
const MockedAbortMultipartUploadOutput = mock<S3.AbortMultipartUploadOutput>();

const S3MockedInstance = {
  promise: jest.fn(),
  createMultipartUpload: jest.fn().mockReturnThis(),
  getSignedUrl: jest.fn((operation: string, params: any): string => buildSignedURL(operation, params)),
  abortMultipartUpload: jest.fn().mockReturnThis(),
  completeMultipartUpload: jest
    .fn(
      (params: S3.CompleteMultipartUploadRequest): S3.CompleteMultipartUploadOutput =>
        completeMultipartUploadOutputMock(params)
    )
    .mockReturnThis()
};

jest.mock("aws-sdk/clients/s3", () => jest.fn(() => S3MockedInstance));

jest.mock("uuid", () => ({ v4: jest.fn(() => UUID) }));

describe("s3.service", () => {
  let instance: S3Service;

  beforeEach(() => {
    instance = new S3Service();
  });

  describe("generateFileName", () => {
    it("returns a unique fileName with the provided extension", () => {
      const fileName = instance.generateFileName(EXTENSION);

      expect(fileName).toEqual(MOCKED_FILE_NAME);
    });

    it("throws an error when extension is not provided", () => {
      expect(() => instance.generateFileName("")).toThrowError(Failure.BadRequest("extension is missing"));
    });
  });

  describe("createMultipartUpload", () => {
    S3MockedInstance.promise.mockResolvedValue(MockedMultipartOutput);

    it("begins the multipart upload", async () => {
      const response = await instance.createMultipartUpload(MOCKED_FILE_NAME);

      expect(response).toEqual(MockedMultipartOutput);
    });

    it("throws an error when fileKey is not passed", async () => {
      await expect(() => instance.createMultipartUpload("")).rejects.toThrowError(
        Failure.BadRequest("fileKey is missing")
      );
    });
  });

  describe("getSignedURLForUploadPart", () => {
    it("returns signedURL to upload a part", async () => {
      const params = {
        Bucket: BUCKET,
        Key: `${S3MediaDirectory.upload}/${MOCKED_FILE_NAME}`,
        UploadId: UUID,
        PartNumber: 1
      };

      const mockedURL = buildSignedURL(S3SignedURLOperations.UPLOAD_PART, params);

      const url = await instance.getSignedURLForUploadPart(
        MOCKED_FILE_NAME,
        params.UploadId,
        params.PartNumber,
        S3MediaDirectory.upload
      );

      expect(url).toEqual(mockedURL);
    });

    it("throws an error when fileKey is not passed", async () => {
      await expect(() => instance.getSignedURLForUploadPart("", "", 1)).rejects.toThrowError(
        Failure.BadRequest("fileKey is missing")
      );
    });

    it("throws an error when uploadId is not passed", async () => {
      await expect(() => instance.getSignedURLForUploadPart(MOCKED_FILE_NAME, "", 1)).rejects.toThrowError(
        Failure.BadRequest("uploadId is missing")
      );
    });
  });

  describe("abortMultipartUpload", () => {
    it("aborts multipart upload", async () => {
      S3MockedInstance.promise.mockResolvedValue(MockedAbortMultipartUploadOutput);

      const response = await instance.abortMultipartUpload(MOCKED_FILE_NAME, UPLOAD_ID);

      expect(response).toEqual(MockedAbortMultipartUploadOutput);
    });

    it("throws an error when fileKey is not passed", async () => {
      await expect(() => instance.abortMultipartUpload("", "")).rejects.toThrowError(
        Failure.BadRequest("fileKey is missing")
      );
    });

    it("throws an error when uploadId is not passed", async () => {
      await expect(() => instance.abortMultipartUpload(MOCKED_FILE_NAME, "")).rejects.toThrowError(
        Failure.BadRequest("uploadId is missing")
      );
    });
  });

  describe("completeMultipartUpload", () => {
    it("completes a multipart upload", async () => {
      const params = {
        Key: `${S3MediaDirectory.upload}/${MOCKED_FILE_NAME}`,
        Bucket: BUCKET,
        UploadId: UPLOAD_ID,
        MultipartUpload: { Parts: [{ ETag: UUID, PartNumber: 1 }] }
      };

      const mockedOutput = completeMultipartUploadOutputMock(params);

      S3MockedInstance.promise.mockResolvedValue(mockedOutput);

      const response = await instance.completeMultipartUpload(
        params.Key,
        params.UploadId,
        params.MultipartUpload.Parts
      );

      expect(response).toEqual(mockedOutput);
    });

    it("throws an error when fileKey is not passed", async () => {
      await expect(() => instance.completeMultipartUpload("", "", [])).rejects.toThrowError(
        Failure.BadRequest("fileKey is missing")
      );
    });

    it("throws an error when uploadId is not passed", async () => {
      await expect(() => instance.completeMultipartUpload(MOCKED_FILE_NAME, "", [])).rejects.toThrowError(
        Failure.BadRequest("uploadId is missing")
      );
    });

    it("throws an error when parts are not passed", async () => {
      await expect(() => instance.completeMultipartUpload(MOCKED_FILE_NAME, UPLOAD_ID, [])).rejects.toThrowError(
        Failure.BadRequest("parts is missing")
      );
    });
  });
});
