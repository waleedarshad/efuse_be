import S3 from "aws-sdk/clients/s3";
import { AWSError } from "aws-sdk";
import axios, { AxiosResponse } from "axios";
import { v4 as uuidv4 } from "uuid";
import { Failure } from "@efuse/contracts";

import { AWS_S3_BUCKET } from "../../config";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { S3_METRICS } from "../metrics";
import { S3MediaDirectory } from "../media/transcode-media.enum";

/**
 * List of content types that are acceptable to pass to `uploadMediaToS3`
 *
 * @link https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types
 */
export enum S3ContentTypes {
  JPEG = "image/jpeg",
  PNG = "image/png",
  GIF = "image/gif",
  XML = "application/xml"
}

/**
 * List of available ACL types provided by AWS
 *
 * @link https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
 */
export enum S3ACL {
  PRIVATE = "private",
  PUBLIC_READ = "public-read",
  PUBLIC_READ_WRITE = "public-read-write"
}

export const S3_URL = `https://${AWS_S3_BUCKET}.s3.amazonaws.com`;

export enum S3SignedURLOperations {
  UPLOAD_PART = "uploadPart"
}

/**
 * S3Service provides the methods required to download and upload from the eFuse S3 bucket
 */
@Service("S3.service")
export class S3Service extends BaseService {
  private s3: S3;

  constructor() {
    super();

    this.s3 = new S3();
  }

  /**
   *@summary Download a file from S3
   *
   * @param {String} filename The file name (including full path) to download
   *
   * @return {S3.GetObjectOutput}
   *
   * @memberof S3Service
   * @link https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#getObject-property
   */
  @Trace()
  public downloadMediaFromS3(filename: string): Promise<S3.GetObjectOutput> {
    this.recordMetric(S3_METRICS.DOWNLOAD_MEDIA.START);

    try {
      return new Promise<S3.GetObjectOutput>((resolve) => {
        const params = { Bucket: AWS_S3_BUCKET, Key: filename };

        this.s3.getObject(params, (err: AWSError, data: S3.GetObjectOutput) => {
          if (err) {
            throw err;
          } else {
            this.recordMetric(S3_METRICS.DOWNLOAD_MEDIA.FINISH);

            resolve(data);
          }
        });
      });
    } catch (error) {
      this.recordMetric(S3_METRICS.DOWNLOAD_MEDIA.ERROR);
      throw this.handleError(error, "Error downloading media from S3");
    }
  }

  /**
   * Upload a file to the eFuse S3 bucket.
   *
   * @param body File data to be uploaded
   * @param filename The filename including file path within the bucket
   * @param contentType MimeType of the file. Refer to S3Service.ContentTypes for options
   *
   * @return {S3.GetObjectOutput}
   *
   * @memberof S3Service
   * @link https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#putObject-property
   */
  @Trace()
  public uploadMediaToS3(
    body: S3.Body,
    destinationBucketUrl: string,
    contentType: S3ContentTypes,
    acl: S3ACL = S3ACL.PUBLIC_READ
  ): Promise<S3.PutObjectOutput> {
    this.recordMetric(S3_METRICS.UPLOAD_MEDIA.START);

    try {
      return new Promise<S3.GetObjectOutput>((resolve) => {
        const params = {
          Bucket: AWS_S3_BUCKET,
          Key: destinationBucketUrl,
          Body: body,
          ContentType: contentType,
          ACL: acl
        };

        this.s3.putObject(params, (err: AWSError, data: S3.PutObjectOutput) => {
          if (err) {
            throw err;
          } else {
            this.recordMetric(S3_METRICS.UPLOAD_MEDIA.FINISH);
            resolve(data);
          }
        });
      });
    } catch (error) {
      this.recordMetric(S3_METRICS.UPLOAD_MEDIA.ERROR);
      throw this.handleError(error, "Error uploading file to S3");
    }
  }

  /**
   * Upload media to the eFuse S3 bucket after downloading it from an external source.
   *
   * @param sourceUrl Url to download the original file from
   * @param destinationBucketUrl The filename including file path within the bucket
   * @param contentType MimeType of the file. Refer to S3Service.ContentTypes for options
   * @param acl ACL type for uploaded file, defaults to public-read
   *
   * @return {boolean} true if successfully uploaded
   *
   * @memberof S3Service
   */
  @Trace()
  public async downloadMediaAndUploadToS3(
    sourceUrl: string,
    destinationBucketUrl: string,
    contentType: S3ContentTypes,
    acl: S3ACL = S3ACL.PUBLIC_READ
  ): Promise<boolean> {
    this.recordMetric(S3_METRICS.DOWNLOAD_AND_UPLOAD_MEDIA.START);

    try {
      const downloadedFile: Buffer = await axios
        .get(sourceUrl, { responseType: "arraybuffer" })
        .then((value: AxiosResponse) => Buffer.from(value.data, "binary"));

      const uploadResult = await this.uploadMediaToS3(downloadedFile, destinationBucketUrl, contentType, acl);

      this.recordMetric(S3_METRICS.DOWNLOAD_AND_UPLOAD_MEDIA.FINISH);

      return uploadResult?.ETag !== null;
    } catch (error) {
      this.recordMetric(S3_METRICS.DOWNLOAD_AND_UPLOAD_MEDIA.ERROR);
      throw this.handleError(error, "Error moving file to S3.");
    }
  }

  @Trace()
  public async deleteMediaFromS3(
    fileKey: string // The fileKey including file path within the bucket
  ): Promise<S3.DeleteObjectOutput> {
    try {
      return new Promise<S3.DeleteObjectOutput>((resolve) => {
        const params = {
          Bucket: AWS_S3_BUCKET,
          Key: fileKey
        };
        this.recordMetric(S3_METRICS.DELETE_MEDIA.START);
        this.s3.deleteObject(params, (err: AWSError, data: S3.DeleteObjectOutput) => {
          if (err) {
            throw err;
          } else {
            this.recordMetric(S3_METRICS.DELETE_MEDIA.FINISH);
            resolve(data);
          }
        });
      });
    } catch (error) {
      this.recordMetric(S3_METRICS.DELETE_MEDIA.ERROR);
      throw this.handleError(error, "Error Deleting media from S3");
    }
  }

  /**
   * @summary Retrieves metadata from an object without returning the object itself
   *
   * @param {string} fileKey
   *
   * @returns {Promise<S3.HeadObjectOutput>}
   *
   * @link https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#headObject-property
   * @memberof S3Service
   */
  @Trace()
  public async getMetadataWithoutFileDownload(fileKey: string): Promise<S3.HeadObjectOutput> {
    try {
      const params = { Bucket: AWS_S3_BUCKET, Key: fileKey };

      this.recordMetric(S3_METRICS.HEAD_OBJECT.START);

      const response = await this.s3.headObject(params).promise();

      this.recordMetric(S3_METRICS.HEAD_OBJECT.FINISH);

      return response;
    } catch (error) {
      this.recordMetric(S3_METRICS.HEAD_OBJECT.ERROR);

      throw this.handleError(error, "Error retrieving metadata from object using HeadObject");
    }
  }

  /**
   * @summary Stream a file from S3
   *
   * @param {string} fileKey
   * @param {string} bucket
   *
   * @returns {unknown}
   */
  @Trace()
  public getFileStream(fileKey: string, bucket: string = AWS_S3_BUCKET): unknown {
    try {
      const params = { Bucket: bucket, Key: fileKey };

      const stream = this.s3.getObject(params).createReadStream();
      return stream;
    } catch (error) {
      throw this.handleError(error, "Error streaming file");
    }
  }

  /**
   * @summary Generate unique file name
   *
   * @param {string} extension
   *
   * @returns {string}
   */
  @Trace()
  public generateFileName(extension: string): string {
    if (!extension) {
      throw Failure.BadRequest("extension is missing");
    }

    return `${uuidv4()}-${Date.now()}.${extension}`;
  }

  /**
   * @summary Create multipart Upload
   *
   * @param {string} fileKey
   * @param {S3ACL} acl
   *
   * @returns {Promise<S3.CreateMultipartUploadOutput>}
   */
  @Trace()
  public async createMultipartUpload(
    fileKey: string,
    directory = S3MediaDirectory.upload,
    acl: S3ACL = S3ACL.PUBLIC_READ
  ): Promise<S3.CreateMultipartUploadOutput> {
    try {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.CREATE_MULTIPART_UPLOAD.START);

      if (!fileKey) {
        throw Failure.BadRequest("fileKey is missing");
      }

      const params = { Key: `${directory}/${fileKey}`, Bucket: AWS_S3_BUCKET, ACL: acl };

      const response = await this.s3.createMultipartUpload(params).promise();

      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.CREATE_MULTIPART_UPLOAD.FINISH);

      return response;
    } catch (error) {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.CREATE_MULTIPART_UPLOAD.ERROR);

      throw this.handleError(error, "Error creating multipart upload");
    }
  }

  /**
   * @summary Generate SignedURLs to upload parts
   *
   * @param {string} fileKey
   * @param {string} uploadId
   * @param {number} partNumber
   *
   * @returns {Promise<string>}
   */
  @Trace()
  public async getSignedURLForUploadPart(
    fileKey: string,
    uploadId: string,
    partNumber: number,
    directory = S3MediaDirectory.upload
  ): Promise<string> {
    try {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.GET_SIGNED_URLS.START);

      if (!fileKey) {
        throw Failure.BadRequest("fileKey is missing");
      }

      if (!uploadId) {
        throw Failure.BadRequest("uploadId is missing");
      }

      if (partNumber === undefined || partNumber === null) {
        throw Failure.BadRequest("partNumber is missing");
      }

      const params = {
        Key: `${directory}/${fileKey}`,
        UploadId: uploadId,
        PartNumber: partNumber,
        Bucket: AWS_S3_BUCKET
      };

      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.GET_SIGNED_URLS.FINISH);

      return this.s3.getSignedUrl(S3SignedURLOperations.UPLOAD_PART, params);
    } catch (error) {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.GET_SIGNED_URLS.ERROR);

      throw this.handleError(error, "Error generating signedURL for uploadPart");
    }
  }

  /**
   * @summary Abort a multipart Upload
   *
   * @param {string} fileKey
   * @param {string} uploadId
   * @param {string} directory
   *
   * @returns {Promise<S3.AbortMultipartUploadOutput>}
   */
  public async abortMultipartUpload(
    fileKey: string,
    uploadId: string,
    directory = S3MediaDirectory.upload
  ): Promise<S3.AbortMultipartUploadOutput> {
    try {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.ABORT_MULTIPART_UPLOAD.START);

      if (!fileKey) {
        throw Failure.BadRequest("fileKey is missing");
      }

      if (!uploadId) {
        throw Failure.BadRequest("uploadId is missing");
      }

      const params = {
        Key: `${directory}/${fileKey}`,
        UploadId: uploadId,
        Bucket: AWS_S3_BUCKET
      };

      const response = await this.s3.abortMultipartUpload(params).promise();

      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.ABORT_MULTIPART_UPLOAD.FINISH);

      return response;
    } catch (error) {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.ABORT_MULTIPART_UPLOAD.ERROR);

      throw this.handleError(error, "Error aborting multipart upload");
    }
  }

  /**
   * @summary Complete a multipart upload
   *
   * @param {string} fileKey
   * @param {string} uploadId
   * @param {S3.CompletedPartList} parts
   * @param {string} directory
   *
   * @returns {Promise<S3.CompleteMultipartUploadOutput>}
   */
  public async completeMultipartUpload(
    fileKey: string,
    uploadId: string,
    parts: S3.CompletedPartList,
    directory = S3MediaDirectory.upload
  ): Promise<S3.CompleteMultipartUploadOutput> {
    try {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.COMPLETE_MULTIPART_UPLOAD.START);

      if (!fileKey) {
        throw Failure.BadRequest("fileKey is missing");
      }

      if (!uploadId) {
        throw Failure.BadRequest("uploadId is missing");
      }

      if (!parts || parts.length === 0) {
        throw Failure.BadRequest("parts is missing");
      }

      const params = {
        Key: `${directory}/${fileKey}`,
        UploadId: uploadId,
        Bucket: AWS_S3_BUCKET,
        MultipartUpload: { Parts: parts }
      };

      const response = await this.s3.completeMultipartUpload(params).promise();

      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.COMPLETE_MULTIPART_UPLOAD.FINISH);

      return response;
    } catch (error) {
      this.recordMetric(S3_METRICS.MULTIPART_UPLOAD.COMPLETE_MULTIPART_UPLOAD.ERROR);

      throw this.handleError(error, "Error completing multipart upload");
    }
  }
}
