export class OrganizationAccessTypes {
  public static readonly CAPTAIN = "CAPTAIN";
  public static readonly OWNER = "OWNER";

  public static readonly CAPTAIN_ONLY: string[] = [OrganizationAccessTypes.CAPTAIN];
  public static readonly OWNER_ONLY: string[] = [OrganizationAccessTypes.OWNER];
  public static readonly ANY: string[] = [OrganizationAccessTypes.CAPTAIN, OrganizationAccessTypes.OWNER];
}
