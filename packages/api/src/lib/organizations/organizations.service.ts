import { Failure } from "@efuse/contracts";
import { FilterQuery, LeanDocument, Types } from "mongoose";
import { ChangeStreamOperationTypeEnum, LeagueJoinStatus } from "@efuse/entities";

import {
  EditableOrganization,
  IMongodbWatcherResponse,
  IOrganization,
  IOrganizationRole,
  IOrganizationUserRole,
  IUser,
  KickMemberResponse,
  UpdateCaptainResponse,
  BanMemberResponse,
  UnBanMemberResponse
} from "../../backend/interfaces";
import { OrganizationAccessTypes } from "./organization-access-types";
import { Service, Trace } from "../decorators";
import { Member } from "../../backend/models/member";
import { OrganizationFollower } from "../../backend/models/organization-follower";
import { OrganizationRequest } from "../../backend/models/organization-request";
import { OrganizationACLService } from "./organization-acl.service";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { OrganizationBaseService } from "./organization-base.service";
import { OrganizationSearchService } from "./organization-search.service";
import { OrganizationInviteCodeTypesEnum, OrganizationMemberStatusEnum } from "./organization.enum";
import { OrganizationGameService } from "./organization-game.service";
import { Organization, OrganizationModel } from "../../backend/models/Organization";
import NotificationsHelper from "../../backend/helpers/notifications.helper";
import { InviteCode } from "../../backend/models/invite-code";
import { Environments } from "../environments";
import { ObjectId } from "../../backend/types";
import { MemberService } from "../member/member.service";
import { LeagueService } from "../leagues";
import {
  LeaguePermissionsEnum,
  OrganizationPermissionSubjects,
  OrganizationUserRoleEnum,
  PermissionActionsEnum
} from "../permissions/enums";

const SERVICE = "organizations.service";

@Service(SERVICE)
export class OrganizationsService extends OrganizationBaseService {
  private organizationModel: OrganizationModel<IOrganization> = Organization;

  private leagueService: LeagueService;
  private $organizationACLService: OrganizationACLService;
  private $organizationGameService: OrganizationGameService;
  private $organizationSearchService: OrganizationSearchService;
  private $memberService: MemberService;

  constructor() {
    super();

    this.leagueService = new LeagueService();
    this.$organizationACLService = new OrganizationACLService();
    this.$organizationSearchService = new OrganizationSearchService();
    this.$organizationGameService = new OrganizationGameService();
    this.$memberService = new MemberService();
  }

  /**
   * @summary Retrieves organization to edit
   *
   * @param {IUser} currentUser - Object of current user
   * @param {string} organizationId - ID of the organization to retrieve
   *
   * @return {EditableOrganization}
   */
  @Trace()
  public async getOrgToEdit(currentUser: IUser, organizationId: string): Promise<EditableOrganization> {
    const { organization } = await this.$organizationACLService.authorizedToAccess(
      currentUser,
      organizationId,
      OrganizationAccessTypes.ANY
    );

    return { canEdit: true, organization };
  }

  /**
   * @summary Get organization by Id
   *
   * @param {string} organizationId
   *
   * @returns {Promise<IOrganization}
   * @memberof OrganizationsService
   */
  @Trace()
  public async getOrganizationById(organizationId: string): Promise<IOrganization> {
    return await this.$organizationACLService.validateOrganization(organizationId);
  }

  @Trace()
  public async getUserOrgRoles(userId: ObjectId, onlyLeagueOrgs = false): Promise<IOrganizationUserRole> {
    let foundOrgs: IOrganization[] = await this.getOrganizationsByUserId(userId, true);

    if (onlyLeagueOrgs) {
      const foundOrgLeagues = await Promise.all(foundOrgs.map((org) => this.returnOrgIfAssociatedWithLeagues(org)));
      foundOrgs =
        foundOrgLeagues && foundOrgLeagues.length > 0
          ? <IOrganization[]>foundOrgLeagues.filter((org) => org != null)
          : [];
    }

    let adjustedOutput: IOrganizationRole[] = [];
    if (foundOrgs && foundOrgs.length > 0) {
      adjustedOutput = await Promise.all(foundOrgs.map((org) => this.adjustOutputForUserOrgRole(org, userId)));
    }

    const finalizedOutput: IOrganizationUserRole = { user: userId, organizationRoles: adjustedOutput };

    return finalizedOutput;
  }

  /**
   * Gets Organizations where a user is the owner or a captain
   *
   * Included flag so that it can be extended to also get organizations where user is a member
   *
   * Note: It is possible for an org owner to not be a member
   */
  @Trace()
  public async getOrganizationsByUserId(userId: ObjectId, includeOrgsWhereMember = false): Promise<IOrganization[]> {
    let foundOrganizations: IOrganization[] = [];

    foundOrganizations = await this.find(
      { $or: [{ user: userId }, { captains: <Types.ObjectId>userId }] },
      {},
      { lean: true }
    );

    if (includeOrgsWhereMember) {
      const foundOwnerCaptainOrgIds: Types.ObjectId[] = foundOrganizations.map((org) => org._id);

      const foundMember = await this.$memberService.find(
        { user: userId, organization: { $nin: foundOwnerCaptainOrgIds }, deleted: { $ne: true } },
        { organization: true },
        { lean: true }
      );
      const organizationIds = foundMember.map((member) => member.organization);

      const foundMemberOrganizations = await this.find({ _id: { $in: organizationIds } }, {}, { lean: true });
      foundOrganizations = [...foundMemberOrganizations, ...foundOrganizations];
    }

    return foundOrganizations;
  }

  /**
   * @summary Changes organization owner
   *
   * @param {IUser} currentUser - Object of current user
   * @param {string} organizationId - ID of the organization to change owner
   * @param {string} newOwnerId - ID of the user to be marked as new owner
   *
   * @return { message: string; user: string }
   */
  @Trace()
  public async changeOwner(
    currentUser: IUser,
    organizationId: string,
    newOwnerId: string
  ): Promise<{ message: string; user: string }> {
    const organization = await this.$organizationACLService.validateOrganization(organizationId);
    const newOwner = await this.$organizationACLService.validateUser(newOwnerId);

    // check whether current user is able to change owner
    this.$organizationACLService.canPerformAction(currentUser, organization, OrganizationAccessTypes.OWNER_ONLY);

    this.logger?.info({ userId: newOwnerId, organizationId }, "Changing owner of the organization");

    // mark user as owner
    organization.user = newOwner._id;
    await organization.save({ validateBeforeSave: false });

    // remove old owner in permission system
    // await this.$organizationACLService.removeUserOrgRole(
    //   organizationId,
    //   currentUser._id.toHexString(),
    //   OrganizationUserRoleEnum.OWNER
    // );
    // add new owner in permission system
    // await this.$organizationACLService.addUserOrgRole(organizationId, newOwnerId, OrganizationUserRoleEnum.OWNER);

    return { message: "Organization owner changed successfully", user: String(newOwner._id) };
  }

  /**
   * @summary Kicks a particular member out of organization
   *
   * @param {IUser} currentUser - Object of current user i.e req.user
   * @param {string} organizationId - ID of the organization to kick from i.e params.id
   * @param {string} userId - ID of the user to be kicked
   *
   * @return {KickMemberResponse}
   */
  @Trace()
  public async kickMember(currentUser: IUser, organizationId: string, userId: string): Promise<KickMemberResponse> {
    const organization = await this.$organizationACLService.validateOrganization(organizationId);
    const userToKick = await this.$organizationACLService.validateUser(userId);

    await this.removeAsCaptain(currentUser, organizationId, userId);

    this.logger?.info(
      { userId },
      "Removing user from Member, OrganizationFollower, and OrganizationRequest collections"
    );

    await Promise.all([
      // removing user as member of organization
      Member.findOneAndDelete({ organization: organizationId, user: userId }),

      // removing user's organization request
      OrganizationRequest.findOneAndDelete({
        organization: organizationId,
        user: userId
      }),

      // removing user organization follower
      OrganizationFollower.findOneAndDelete({
        followee: organizationId,
        follower: userId
      })
    ]);

    // remove old user in permission system
    // await this.$organizationACLService.removeUserOrgRole(organizationId, userId, OrganizationUserRoleEnum.MEMBER);

    // Fetching all members to send as response
    const members = await Member.find({ organization: organizationId });
    return {
      members,
      message: "User has been successfully kicked out of the organization",
      flashType: "success"
    };
  }

  /**
   * @summary Add a particular user a captain of organization
   *
   * @param {IUser} currentUser - Object of current user i.e req.user
   * @param {string} organizationId - ID of the organization i.e params.id
   * @param {string} userId - ID of user to be made captain
   *
   * @return {Promise<UpdateCaptainResponse>} { targetUser, organization, message, flashType, captains }
   */
  @Trace()
  public async addAsCaptain(
    currentUser: IUser,
    organizationId: string,
    userId: string
  ): Promise<UpdateCaptainResponse> {
    // Validate organization id & fetch organization
    const organization = await this.$organizationACLService.validateOrganization(organizationId);

    // Check whether user is able to add captain
    await this.$organizationACLService.canPerformAction(currentUser, organization);

    // Validate user id
    await this.$organizationACLService.validateUser(userId);

    const successResponse = {
      captains: organization.captains,
      message: "User has been promoted to captain's role",
      flashType: "success"
    };

    // Check if user is already captain
    if (organization.captains?.includes(Types.ObjectId(userId))) {
      this.logger?.info("Already captain of organization");
      return successResponse;
    }

    // Add captain
    this.logger?.info("Promoting user as captain of organization");
    organization.captains?.push(Types.ObjectId(userId));
    await this.update({ _id: organizationId }, organization);

    return successResponse;
  }

  /**
   * @summary Removes a particular user from captains array
   *
   * @param {IUser} currentUser - Object of current user i.e req.user
   * @param {string} organizationId - ID of the organization to remove captain from i.e params.id
   * @param {string} userId - ID of the user to be removed from captains array
   *
   * @return {UpdateCaptainResponse}
   */
  @Trace()
  public async removeAsCaptain(
    currentUser: IUser,
    organizationId: string,
    userId: string
  ): Promise<UpdateCaptainResponse> {
    const organization = await this.$organizationACLService.validateOrganization(organizationId);
    const user = await this.$organizationACLService.validateUser(userId);

    this.$organizationACLService.canPerformAction(currentUser, organization);

    this.logger?.info({ userId }, "Removing user from captains array");
    const updatedOrganization = await this.organizations.findOneAndUpdate(
      { _id: organizationId },
      { $pull: { captains: user._id } },
      { new: true }
    );

    // remove old captain in permission system
    // await this.$organizationACLService.removeUserOrgRole(organizationId, userId, OrganizationUserRoleEnum.CAPTAIN);

    return {
      captains: updatedOrganization?.captains,
      flashType: "success",
      message: "User has been demoted from the captain's role"
    };
  }

  /**
   * @summary Update about section of organization
   *
   * @param {IUser} currentUser
   * @param {ObjectId | string} organizationId
   * @param {Partial<IOrganization>} body
   *
   * @returns {Promise<IOrganization>}
   */
  @Trace()
  public async updateAbout(
    currentUser: IUser,
    organizationId: Types.ObjectId | string,
    body: Partial<IOrganization> | undefined
  ): Promise<IOrganization> {
    // Make sure about body is passed
    if (!body || !body.about) {
      throw Failure.BadRequest("body is required");
    }

    // Make sure user is captain or owner
    await this.$organizationACLService.authorizedToAccess(currentUser, organizationId, OrganizationAccessTypes.ANY);

    // Build query & body
    const updateQuery: FilterQuery<IOrganization> = <FilterQuery<IOrganization>>{ _id: organizationId };
    const updateBody: Partial<IOrganization> = <Partial<IOrganization>>{ about: body.about };

    // Update organization
    return await this.update(updateQuery, updateBody);
  }

  /**
   * @summary Get organization's ID by id or shortName
   *
   * @param {ObjectId | string | undefined} idOrShortName
   *
   * @returns {Promise<ObjectId | undefined>}
   */
  @Trace()
  public async getOrganizationIdByIdOrShortName(
    idOrShortName: Types.ObjectId | string | undefined
  ): Promise<Types.ObjectId | undefined> {
    if (!idOrShortName) {
      return;
    }

    if (this.isValidObjectId(idOrShortName)) {
      return new Types.ObjectId(idOrShortName);
    }

    // Lookup for organization by shortName
    const dbOrganization = await this.findOne({ shortName: idOrShortName as string }, { _id: 1 });

    // Set ID when object exists in DB
    return dbOrganization ? new Types.ObjectId(dbOrganization._id) : undefined;
  }

  /**
   * @summary return user organizations
   * @param {ObjectId} id
   *
   * @returns {Promise<IOrganization[]>}
   */
  @Trace()
  public async getUserOrganizations(id: Types.ObjectId | string): Promise<IOrganization[]> {
    const userId = GeneralHelper.getObjectId(id);
    if (!userId) {
      throw Failure.BadRequest("userId is required");
    }

    // TODO: [perms] Update to use permission system

    return this.organizations.aggregate([
      {
        $lookup: {
          from: "members",
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$user", userId] }]
                }
              }
            }
          ],
          as: "memberCollection"
        }
      },
      {
        $match: { memberCollection: { $ne: [] } }
      },
      {
        $project: { memberCollection: 0 }
      }
    ]);
  }

  /**
   * This determines if the user has the permissions to create leagues for any organization.
   * @param userId The ID of the user to check.
   * @returns Whether or not the user can create leagues.
   */
  @Trace()
  public async canUserCreateLeagues(userId: ObjectId): Promise<boolean> {
    const orgs = await this.getOrganizationsByUserId(userId, true);

    for (let i = 0; i < orgs.length; i++) {
      // TODO: This could be a potential performance problem. Keep an eye on it.
      if (await this.leagueService.canOrgCreateLeagues(orgs[i]._id)) {
        return true;
      }
    }

    return false;
  }

  /**
   * This determines if the user has the permissions to create leagues for an organization.
   * @param userId The ID of the user to check.
   * @param orgId The ID of the organization to check the user's permissions for.
   * @returns Whether or not the user can create leagues.
   */
  @Trace()
  public async canUserCreateLeaguesForOrg(userId: ObjectId, orgId: ObjectId): Promise<boolean> {
    // see if the league can create an org, this is the faster check as we don't have to query orgs
    if (!(await this.leagueService.canOrgCreateLeagues(orgId))) {
      return false;
    }
    // if the org can create leagues, check if the user is allowed to do so on behalf of the org.
    const org = await this.getOrganizationById(orgId.toString());

    return this.isOwner(org, userId) || this.isCaptain(org, userId);
  }

  /**
   * @summary Update games of an organization
   *
   * @param {IUser} currentUser
   * @param {ObjectId | string} organizationId
   * @param {Array<ObjectId | string>} games
   *
   * @returns {Promise<IOrganization>}
   */
  @Trace()
  public async updateGames(
    currentUser: IUser,
    organizationId: Types.ObjectId | string,
    games: Array<Types.ObjectId | string> | undefined
  ): Promise<IOrganization> {
    // Make sure gameIds are passed
    if (games === undefined || games === null) {
      throw Failure.BadRequest("Missing parameter games");
    }

    // Make sure gameIds are valid object ids
    games.forEach((game) => {
      if (!this.isValidObjectId(game)) {
        throw Failure.BadRequest("Invalid game ID");
      }
    });

    // Make sure user is captain or owner
    await this.$organizationACLService.authorizedToAccess(currentUser, organizationId, OrganizationAccessTypes.ANY);

    // Build query & body
    const updateQuery: FilterQuery<IOrganization> = <FilterQuery<IOrganization>>{ _id: organizationId };
    const updateBody: Partial<IOrganization> = <Partial<IOrganization>>{ games };

    // Update organization
    return await this.update(updateQuery, updateBody);
  }

  @Trace()
  public async createOrganization(
    creator: IUser,
    organization: Partial<IOrganization>,
    gameIds: ObjectId[]
  ): Promise<IOrganization> {
    const newOrganization = await this.organizationModel.create(organization);

    await this.associateGames(creator._id, newOrganization._id, gameIds);
    await this.createInviteCodes(newOrganization);

    // add owner permission for creator
    // await this.$organizationACLService.addUserOrgRole(
    //   newOrganization._id.toHexString(),
    //   creator._id.toHexString(),
    //   OrganizationUserRoleEnum.OWNER
    // );

    if (process.env.NODE_ENV === Environments.PRODUCTION) {
      await NotificationsHelper.sendNewOrganizationEmail("org_creation@efuse.io", newOrganization, creator._id);
    }

    return newOrganization;
  }

  @Trace()
  public async associateGames(userId: Types.ObjectId, organizationId: string, gameIds: ObjectId[]): Promise<void> {
    try {
      // Make sure valid ids are passed
      await this.$organizationGameService.associateGames(userId, organizationId, gameIds);
    } catch (error) {
      this.logger?.error(error, "Error while associating games");
    }
  }

  /**
   * @summary Organization Watcher to read change streams
   *
   * @param {IMongodbWatcherResponse<LeanDocument<IOpportunity>>} data
   *
   * @returns {void}
   */
  @Trace({ service: SERVICE, name: "Watcher" })
  public static Watcher(data: IMongodbWatcherResponse<LeanDocument<IOrganization>>): void {
    const organizationsService = new OrganizationsService();

    try {
      if (data.operationType === ChangeStreamOperationTypeEnum.DELETE) {
        organizationsService.$organizationSearchService.deleteFromAlgoliaAsync(data?.documentKey?._id);
      } else {
        // Sync Data with algolia
        organizationsService.$organizationSearchService.syncWithAlgoliaAsync(data.fullDocument);
      }
    } catch (error) {
      organizationsService.logger?.error(error, "Error reading data in organization watcher");
    }
  }

  /**
   * Determines if the user is the owner of the organization.
   * @param organization The organization to verify ownership of.
   * @param userId The user to check for ownership.
   * @returns
   */
  @Trace()
  public isOwner(organization: IOrganization, userId: ObjectId): boolean {
    return organization.user?.equals(userId) ?? false;
  }

  /**
   * Determines if the user is a captain of the organization.
   * @param organization The organization to verify captains for.
   * @param userId The user to check for the captain role.
   * @returns
   */
  @Trace()
  public isCaptain(organization: IOrganization, userId: ObjectId): boolean {
    if (organization.captains && organization.captains.length > 0) {
      return organization.captains.some((captain) => captain?.equals(userId));
    }

    return false;
  }

  /**
   * Determines if the user is a member of the organization.
   * @param organization The organization to verify members for.
   * @param userId The user to check for membership.
   * @returns
   */
  @Trace()
  public async isMember(organization: IOrganization, userId: ObjectId): Promise<boolean> {
    const exists = await this.$memberService.exists({
      user: userId,
      organization: organization._id,
      deleted: { $ne: true }
    });

    return exists;
  }

  /**
   * Gets the role for the supplied user on the supplied organization.
   * @param organization The organization to check for roles on.
   * @param userId The user to get the role of.
   * @returns
   */
  @Trace()
  public async getUserRole(organization: IOrganization, userId: ObjectId): Promise<OrganizationUserRoleEnum> {
    if (this.isOwner(organization, userId)) {
      return OrganizationUserRoleEnum.OWNER;
    }

    if (this.isCaptain(organization, userId)) {
      return OrganizationUserRoleEnum.CAPTAIN;
    }

    if (await this.isMember(organization, userId)) {
      return OrganizationUserRoleEnum.MEMBER;
    }

    return OrganizationUserRoleEnum.NONE;
  }

  @Trace()
  private async adjustOutputForUserOrgRole(organization: IOrganization, userId: ObjectId): Promise<IOrganizationRole> {
    return {
      role: await this.getUserRole(organization, userId),
      organization: organization
    };
  }

  @Trace()
  private async createInviteCodes(organization: IOrganization): Promise<void> {
    // Create 2 new invite codes: one for members, and one for followers
    await new InviteCode({
      type: OrganizationInviteCodeTypesEnum.ORGANIZATION_MEMBER_INVITE,
      organization: organization._id,
      code: GeneralHelper.randomString(5, "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ")
    }).save();

    await new InviteCode({
      type: OrganizationInviteCodeTypesEnum.ORGANIZATION,
      organization: organization._id,
      code: GeneralHelper.randomString(5, "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ")
    }).save();
  }

  @Trace()
  private async findIfOrgHasCreateLeaguePermission(organization: IOrganization): Promise<boolean> {
    return this.$organizationACLService.orgHasPermission(
      organization._id,
      LeaguePermissionsEnum.LEAGUES,
      PermissionActionsEnum.ACCESS
    );
  }

  @Trace()
  private async returnOrgIfAssociatedWithLeagues(organization: IOrganization): Promise<IOrganization | null> {
    if (await this.findIfOrgIsAssociatedWithLeagues(organization)) {
      return organization;
    }

    return null;
  }

  @Trace()
  private async findIfOrgIsAssociatedWithLeagues(organization: IOrganization): Promise<boolean> {
    const hasLeagueAccessPromise = this.findIfOrgHasCreateLeaguePermission(organization);

    const hasCreateLeaguePromise = this.$organizationACLService.orgHasPermission(
      organization._id,
      OrganizationPermissionSubjects.CREATE_LEAGUE,
      PermissionActionsEnum.ACCESS
    );

    // TODO: this is temporary until permissions are fully setup
    const foundJoinedOrPendingLeaguePromise = this.leagueService.getJoinedOrPendingLeagues(
      organization._id,
      LeagueJoinStatus.JOINED_AND_PENDING
    );

    const finishedPromiseArray = await Promise.all([
      hasLeagueAccessPromise,
      hasCreateLeaguePromise,
      foundJoinedOrPendingLeaguePromise
    ]);

    const hasLeagueAccess = finishedPromiseArray[0];
    const hasCreateLeagueAccess = finishedPromiseArray[1];
    const foundJoinedOrPendingLeague = finishedPromiseArray[2];

    return (
      hasLeagueAccess || hasCreateLeagueAccess || (foundJoinedOrPendingLeague && foundJoinedOrPendingLeague.length > 0)
    );
  }

  /**
   * @summary Ban a particular member of organization
   *
   * @param {IUser} currentUser - Object of current user i.e req.user
   * @param {string} organizationId - ID of the organization to ban user from i.e params.id
   * @param {string} userId - ID of the user to be banned
   *
   * @return {Promise<BanMemberResponse>}
   */
  @Trace()
  public async banMember(currentUser: IUser, organizationId: string, userId: string): Promise<BanMemberResponse> {
    // Removing user as captain, Note it will automatically validate access
    await this.removeAsCaptain(currentUser, organizationId, userId);

    // Make sure userId is valid
    await this.$organizationACLService.validateUser(userId);

    this.logger?.info({ userId }, "Marking status as banned for user in Member & OrganizationRequest object");
    // Using Promise.all as it will try to resolve all promises concurrently
    const resolvedPromises = await Promise.all([
      // Ban Member
      // resolvedPromises[0] = Member
      this.$memberService.update({ organization: organizationId, user: userId }, { isBanned: true }),

      // Ban OrganizationRequest
      // resolvedPromises[1] = OrganizationRequest
      OrganizationRequest.findOneAndUpdate(
        { organization: organizationId, user: userId },
        { status: OrganizationMemberStatusEnum.BANNED },
        { new: true }
      )
    ]);

    // Return response
    return {
      bannedMember: resolvedPromises[0],
      message: "Member has been successfully banned from creating new organization posts",
      flashType: "success"
    };
  }

  /**
   * @summary Unban a particular member of organization
   *
   * @param {IUser} currentUser - Object of current user i.e req.user
   * @param {string} organizationId - ID of the organization to Unban user from i.e params.id
   * @param {string} userId - ID of the user to be Unbanned
   *
   * @return {Promise<UnBanMemberResponse>}
   */
  @Trace()
  public async unBanMember(currentUser: IUser, organizationId: string, userId: string): Promise<UnBanMemberResponse> {
    // Validate organization id & fetch organization
    const organization = await this.$organizationACLService.validateOrganization(organizationId);

    // Check whether user is able to unban
    await this.$organizationACLService.canPerformAction(currentUser, organization);

    // Make sure userId is valid
    await this.$organizationACLService.validateUser(userId);

    this.logger?.info({ userId }, "Marking status as unbanned for user in Member & OrganizationRequest object");
    // Using Promise.all as it will try to resolve all promises concurrently
    const resolvedPromises = await Promise.all([
      // Ban Member
      // resolvedPromises[0] = Member
      this.$memberService.update({ organization: organizationId, user: userId }, { isBanned: false }),

      // Ban OrganizationRequest
      // resolvedPromises[1] = OrganizationRequest
      OrganizationRequest.findOneAndUpdate(
        { organization: organizationId, user: userId },
        { status: OrganizationMemberStatusEnum.APPROVED },
        { new: true }
      )
    ]);

    // Return response
    return {
      unBannedMember: resolvedPromises[0],
      message: "Member can now create posts on organization timeline",
      flashType: "success"
    };
  }

  // @Trace()
  // public async addCaptainLeaguePermission(organizationId: string): Promise<boolean> {
  //   return await this.$organizationACLService.addOrgPermission(
  //     organizationId,
  //     LeaguePermissionsEnum.LEAGUES,
  //     PermissionActionsEnum.WRITE,
  //     [OrganizationUserRoleEnum.CAPTAIN]
  //   );
  // }

  // @Trace()
  // public async addOrgLeagueCreatePolicy(organizationId: string): Promise<boolean> {
  //   return await this.$organizationACLService.addOrgPermission(
  //     organizationId,
  //     LeaguePermissionsEnum.LEAGUES,
  //     PermissionActionsEnum.WRITE
  //   );
  // }
}
