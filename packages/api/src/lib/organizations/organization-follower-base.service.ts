import { FilterQuery, QueryOptions } from "mongoose";

import { Service, Trace } from "../decorators";
import { IOrganizationFollower } from "../../backend/interfaces";
import { OrganizationFollower, OrganizationFollowerModel } from "../../backend/models/organization-follower";
import { BaseModelService } from "../base";

@Service("organization-follower-base.service")
export class OrganizationFollowerBaseService extends BaseModelService<IOrganizationFollower> {
  protected readonly $organizationFollowers: OrganizationFollowerModel<IOrganizationFollower> = OrganizationFollower;

  constructor() {
    super(OrganizationFollower);
  }

  public async findOne(filter: FilterQuery<IOrganizationFollower>): Promise<IOrganizationFollower | null>;
  public async findOne(
    filter: FilterQuery<IOrganizationFollower>,
    projection: any
  ): Promise<IOrganizationFollower | null>;
  public async findOne(
    filter: FilterQuery<IOrganizationFollower>,
    projection: any,
    options: QueryOptions | null
  ): Promise<IOrganizationFollower | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IOrganizationFollower>,
    projection?: any,
    options?: QueryOptions | null
  ): Promise<IOrganizationFollower | null> {
    const organizationFollower = await this.$organizationFollowers.findOne(filter, projection, options);

    return organizationFollower;
  }
}
