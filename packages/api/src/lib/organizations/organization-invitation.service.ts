import { IOrganizationInvitation } from "../../backend/interfaces";
import { OrganizationInvitation } from "../../backend/models/OrganizationInvitation";
import NotificationsHelper from "../../backend/helpers/notifications.helper";
import { BaseModelService } from "../base";
import { Service } from "../decorators";
import { UserService } from "../users/user.service";
import { QueryOptions, Types } from "mongoose";
import { OrganizationsService } from "./organizations.service";

type ObjectId = string | Types.ObjectId;

@Service("organization-invitation.service")
export class OrganizationInvitationService extends BaseModelService<IOrganizationInvitation> {
  private organizationService: OrganizationsService;
  private userService: UserService;

  constructor() {
    super(OrganizationInvitation);

    this.organizationService = new OrganizationsService();
    this.userService = new UserService();
  }

  public async sendInvitation(
    inviterId: ObjectId,
    inviteeId: ObjectId,
    organizationId: ObjectId
  ): Promise<IOrganizationInvitation> {
    const leanOption: QueryOptions = { lean: true };

    try {
      const foundInvite = await this.findOne({ inviter: inviterId, invitee: inviteeId, organization: organizationId });

      // return if invite already sent
      if (foundInvite) {
        return foundInvite;
      }

      // parallelize calls
      const results = await Promise.all([
        this.userService.findOne({ _id: inviterId }, {}, leanOption),
        this.organizationService.findOne({ _id: organizationId }, {}, leanOption)
      ]);

      const inviterUser = results[0];
      const organization = results[1];

      const invite = <IOrganizationInvitation>(<unknown>{
        status: "invited",
        inviter: inviterId,
        invitee: inviteeId,
        organization: organizationId
      });

      const orgInvitation = await this.create(invite);

      NotificationsHelper.organizationInvitationNotification(inviterUser, orgInvitation, organization).catch((error) =>
        this.logger?.error(error, "Error resolving organizationInvitationNotification promise")
      );

      return orgInvitation;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
