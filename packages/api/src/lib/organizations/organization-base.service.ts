import { FilterQuery, QueryOptions } from "mongoose";
import { Failure } from "@efuse/contracts";

import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { Organization, OrganizationModel } from "../../backend/models/Organization";
import { IOrganization } from "../../backend/interfaces";

// TODO: This should really be rewritten to remove this OrganizationBaseService but for now it is
// fine to change the extended class to the BaseModelService instead of the BaseModel.
@Service("organization-base.service")
export class OrganizationBaseService extends BaseModelService<IOrganization> {
  protected organizations: OrganizationModel<IOrganization> = Organization;

  constructor() {
    super(Organization);
  }

  public async findOne(filter: FilterQuery<IOrganization>): Promise<IOrganization | null>;
  public async findOne(filter: FilterQuery<IOrganization>, projection: any): Promise<IOrganization | null>;
  public async findOne(
    filter: FilterQuery<IOrganization>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IOrganization | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IOrganization>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOrganization | null> {
    const organization = await this.organizations.findOne(filter, projection, options);
    return organization;
  }

  public async find(filter: FilterQuery<IOrganization>): Promise<IOrganization[]>;
  public async find(filter: FilterQuery<IOrganization>, projection?: any): Promise<IOrganization[]>;
  public async find(
    filter: FilterQuery<IOrganization>,
    projection: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOrganization[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IOrganization>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOrganization[]> {
    const organizations = await this.organizations.find(filter, projection, options);
    return organizations;
  }

  /**
   * @summary Update a particular organization
   *
   * @param {IUser} currentUser
   * @param {Types.ObjectId | string} organizationId
   * @param {FilterQuery<IOrganization>} filter
   * @param {Partial<IOrganization>} body
   * @param {string[]} accessTypes
   *
   * @returns {Promise<IOrganization>}
   */
  @Trace()
  public async update(filter: FilterQuery<IOrganization>, body: Partial<IOrganization>): Promise<IOrganization> {
    const updatedOrganization: IOrganization | null = await this.organizations
      .findOneAndUpdate(filter, { ...body }, { new: true })
      .lean();

    if (!updatedOrganization) {
      throw Failure.UnprocessableEntity("Organization not found");
    }

    return updatedOrganization;
  }
}
