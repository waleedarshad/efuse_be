import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { DocumentType } from "@typegoose/typegoose";

import { Service, Trace } from "../decorators";
import { OrganizationGameBaseService } from "./organization-game-base.service";
import { IOrganization } from "../../backend/interfaces";
import { OrganizationACLService } from "./organization-acl.service";
import { OrganizationAccessTypes } from "./organization-access-types";
import { GameBaseService } from "../game/game-base.service";
import { Game } from "../../backend/models/game.model";

@Service("organization-game.service")
export class OrganizationGameService extends OrganizationGameBaseService {
  private $gameBaseService: GameBaseService;
  private $organizationACLService: OrganizationACLService;

  constructor() {
    super();

    this.$gameBaseService = new GameBaseService();
    this.$organizationACLService = new OrganizationACLService();
  }

  /**
   * @summary Associate games with the organization
   *
   * @param {Types.ObjectId | string} organizationId
   * @param {Array<Types.ObjectId | string>} gameIds
   *
   * @returns {Promise<IOrganization>}
   */
  @Trace()
  public async associateGames(
    userId: Types.ObjectId | string,
    organizationId: Types.ObjectId | string,
    gameIds: Array<Types.ObjectId | string>
  ): Promise<IOrganization> {
    // Make sure gameIds are valid object ids
    gameIds.forEach((gameId) => {
      if (!this.isValidObjectId(gameId)) {
        throw Failure.BadRequest("Invalid game ID");
      }
    });

    // Make sure user is owner, captain or admin
    const { organization } = await this.$organizationACLService.authorizedToAccess(
      userId,
      organizationId,
      OrganizationAccessTypes.ANY
    );

    // Make sure game exists in our DB
    const games = await this.$gameBaseService.find({ _id: { $in: gameIds } });

    // IDs that exist in our DB
    const idsToStore: string[] = games.map((game) => String(game._id));

    // Associate with organization & Store in DB
    await this.bulkUpdate(organizationId, idsToStore);

    return organization;
  }

  /**
   * @summary Get games associated to an organization
   *
   * @param {Types.ObjectId | string} organizationId
   *
   * @returns {Promise<Game[]>}
   */
  @Trace()
  public async getAssociatedGames(organizationId: Types.ObjectId | string): Promise<DocumentType<Game>[]> {
    const organizationGames = await this.find({ organization: organizationId });

    const games = await this.$gameBaseService.find({ _id: { $in: organizationGames.map((og) => og.game) } });

    return games;
  }
}
