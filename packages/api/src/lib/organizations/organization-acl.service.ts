import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";

import { IEfusePermission, IOrganization, IUser } from "../../backend/interfaces";
import { Organization, OrganizationModel } from "../../backend/models/Organization";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { UserService } from "../users/user.service";
import { OrganizationAccessTypes } from "./organization-access-types";
import { EFusePermissionsService } from "../permissions/efuse-permissions.service";
import { PermissionActionsEnum } from "../permissions/enums";

// TODO: [perms] update with logic for permission system
// this file should include logic to add permissions as well so it is contained in one place
@Service("organization-acl.service")
export class OrganizationACLService extends BaseService {
  // private permissionsService: PermissionsService;
  private eFusePermissionService: EFusePermissionsService;
  private userService: UserService;
  private readonly organizationModel: OrganizationModel<IOrganization> = Organization;

  constructor() {
    super();

    // this.permissionsService = new PermissionsService();
    this.eFusePermissionService = new EFusePermissionsService();
    this.userService = new UserService();
  }

  /**
   * Remove all permission related to an org
   * @date 8/11/2021
   *
   * @param {string} orgId What org are we removing permissions for?
   * @returns {Promise<boolean>}
   */
  // public async deleteAllOrgPermissions(orgId: string): Promise<boolean> {
  //   const orgPolicies = await this.permissionsService.findPolicies("", "", "", orgId);
  //   const orgGroupPolicies = await this.permissionsService.findGroupPolicies("", "", orgId);
  //
  //   const orgPoliciesDeleteResult = await this.permissionsService.deletePolicies(orgPolicies);
  //   const orgGroupPoliciesDeleteResult = await this.permissionsService.deleteGroupPolicies(orgGroupPolicies);
  //
  //   return orgPoliciesDeleteResult && orgGroupPoliciesDeleteResult;
  // }

  /**
   * Add a new permission to an organization.
   * The permission will also be assigned to all the roles passed in via the "roles" parameter.
   * @date 9/3/2021
   *
   * @param {string} orgId What org are we adding a new permission for?
   * @param {string} object What object (page, dataset, etc..) are we adding a new permission for?
   * @param {string} action What action (all, read, write, access, etc..) are we allowing with this permission?
   */
  public async addOrgPermission(orgId: string, object: string, action: string): Promise<boolean> {
    const permission = <IEfusePermission>{
      subject: orgId,
      object,
      action: <PermissionActionsEnum>action,
      tenant: orgId
    };
    const orgPermission = await this.eFusePermissionService.create(permission);

    return orgPermission != null;
  }

  /**
   * Check if org has a permission.
   * @date 9/15/2021
   *
   * @param {string} orgId What org are we checking permission for?
   * @param {string} object What object (page, dataset, etc..) are we checking permission for?
   * @param {string} action What action (all, read, write, access, etc..) is allowed for this permission?
   */
  public async orgHasPermission(orgId: string, object: string, action: string): Promise<boolean> {
    const orgHasPermission = await this.eFusePermissionService.exists({
      subject: orgId,
      object,
      action: <PermissionActionsEnum>action
    });

    return orgHasPermission;
  }

  /**
   * Removes all policies for object/action combination assigned to the org and/or the org's roles
   * @date 9/3/2021
   *
   * @param {string} orgId What org are we remove a permission from?
   * @param {string} object What object (page, dataset, etc..) removing?
   * @param {string} action What action (all, read, write, access, etc..) are we removing?
   */
  public async removeOrgPermission(orgId: string, object?: string, action?: string): Promise<boolean> {
    const filter = { subject: orgId.toString() };

    if (object) {
      filter["object"] = object;
    }

    if (action) {
      filter["action"] = action;
    }

    return await this.eFusePermissionService.deleteOne(filter);
  }

  /**
   * Add a new role permission to an organization
   * @date 8/11/2021
   *
   * @param {string} orgId What org are we adding a new role permission for?
   * @param {(OrganizationUserRoleEnum | string)} role What role are we adding a new permission for?
   * @param {string} object What object (page, dataset, etc..) are we adding a new permission for?
   * @param {string} action What action (all, read, write, access, etc..) are we allowing with this permission?
   * @returns {Promise<boolean>}
   */
  // public async addOrgRolePermission(
  //   orgId: string,
  //   role: OrganizationUserRoleEnum | string,
  //   object: string,
  //   action: string
  // ): Promise<boolean> {
  //   return await this.permissionsService.addPolicy({ subject: role, object, action, tenant: orgId });
  // }

  /**
   * Add org role to user
   * @date 8/11/2021
   *
   * @param {string} orgId What org are we adding a new role permission to?
   * @param {string} userId What user are we giving a new role?
   * @param {OrganizationUserRoleEnum} role What role are we giving to the user?
   * @returns {Promise<boolean>}
   */
  // public async addUserOrgRole(orgId: string, userId: string, role: OrganizationUserRoleEnum): Promise<boolean> {
  //   return await this.permissionsService.addGroupPolicy({
  //     subject: userId,
  //     policySubject: role,
  //     tenant: orgId
  //   });
  // }

  /**
   * Remove a user's org role
   * @date 8/11/2021
   *
   * @param {string} orgId What org are we removing the user role for?
   * @param {string} userId What user are we removing the role for?
   * @param {?OrganizationUserRoleEnum} [role] What role are we removing from the user?
   * @returns {Promise<boolean>}
   */
  // public async removeUserOrgRole(orgId: string, userId: string, role?: OrganizationUserRoleEnum): Promise<boolean> {
  //   const orgGroupPolicies = await this.permissionsService.findGroupPolicies(userId, role, orgId);
  //   return await this.permissionsService.deleteGroupPolicies(orgGroupPolicies);
  // }

  /**
   * @summary Validates userId & fetches user. Throws error if user is not found or blocked
   *
   * @param {ObjectId | string} userId - ID of the user to fetch
   *
   * @return {IUser}
   */
  @Trace()
  public async validateUser(userId: Types.ObjectId | string): Promise<IUser> {
    if (!this.isValidObjectId(userId)) {
      throw Failure.BadRequest("User id is malformed");
    }

    const user = await this.userService.findOne({ _id: userId }, { _id: 1, status: 1, roles: 1 });

    if (!user) {
      this.logger!.error({ userId }, "User not found");
      throw Failure.UnprocessableEntity("User not found");
    }

    if (user.status === "Blocked") {
      this.logger!.error({ userId }, "User has been blocked");
      throw Failure.BadRequest("User has been blocked");
    }

    return user;
  }

  /**
   * @summary Validates organizationId & fetches organization. Throws error if organization is not found.
   *
   * @param {ObjectId | string} organizationId - ID of the organization to fetch
   *
   * @return {IOrganization}
   */
  @Trace()
  public async validateOrganization(organizationId: Types.ObjectId | string): Promise<IOrganization> {
    if (!this.isValidObjectId(organizationId)) {
      throw Failure.BadRequest("Organization id is malformed");
    }

    const organization = await this.organizationModel.findById(organizationId);

    if (!organization) {
      this.logger!.error({ organizationId }, "Organization not found");
      throw Failure.UnprocessableEntity("Unable to find requested organization");
    }

    return organization;
  }

  // TODO: [perms] Update to use Permission System
  /**
   * @summary Validates whether user has access to perform certain action on organization
   *
   * @param {IUser} currentUser - Object of current user
   * @param {IOrganization} organization - ID of the organization
   * @param {Array} requiredAccess - Type of access user must have to perform action
   *
   * @return {IOrganization} - The organization the currentUser wants to act on
   */
  @Trace()
  public canPerformAction(
    currentUser: IUser,
    organization: IOrganization,
    requiredAccess: string[] = OrganizationAccessTypes.ANY
  ): boolean {
    // TODO: [perms] Pull User Perms
    // Pull user perms for org?
    // check for specific role based on required access?

    // allow if user is eFuse admin
    if (currentUser?.roles?.includes("admin")) {
      this.logger!.info("User is eFuse admin | Access Granted");
      return true;
    }

    // apply rule only when OWNER level access if required
    // allow if user is organization's owner
    if (
      requiredAccess.includes(OrganizationAccessTypes.OWNER) &&
      String(organization.user) === String(currentUser._id)
    ) {
      this.logger!.info("User is owner of organization | Access Granted");
      return true;
    }

    // apply rule only when CAPTAIN level access if required
    // allow if user is organization's captain
    if (requiredAccess.includes(OrganizationAccessTypes.CAPTAIN) && organization.captains?.includes(currentUser._id)) {
      this.logger!.info("User is captain of organization | Access Granted");
      return true;
    }

    // if it reaches here then user is not efuse admin, captain, nor owner
    // or if requiredAccess is empty array
    this.logger!.error("currentUser is not eFuse admin, organization, captain nor owner");
    throw Failure.Forbidden("You are not authorized to perform this action");
  }

  // TODO: [perms] Update to user permission system
  /**
   * @summary Get organization if user is allowed to access it i.e user has particular access level
   *
   * @param {ObjectId | IUser | string} currentUser
   * @param {ObjectId | string} organizationId
   * @param {string[]} accessTypes
   *
   * @return {Promise<{ organization: IOrganization; user: IUser }>}
   */
  @Trace()
  public async authorizedToAccess(
    currentUser: Types.ObjectId | IUser | string,
    organizationId: Types.ObjectId | string,
    accessTypes: string[]
  ): Promise<{ organization: IOrganization; user: IUser }> {
    let user = currentUser as IUser;
    // Fetch user if ID is passed
    if (currentUser instanceof Types.ObjectId || typeof currentUser === "string") {
      user = await this.validateUser(String(currentUser));
    }

    // Fetch organization
    const organization = await this.validateOrganization(String(organizationId));

    // Make sure user is allowed to access organization
    this.canPerformAction(user, organization, accessTypes);

    return { organization, user };
  }
}
