import { Failure } from "@efuse/contracts";
import { LeanDocument } from "mongoose";
import { Queue } from "bull";

import { AlgoliaIndicesEnum } from "../algolia/algolia.enum";
import { AlgoliaService } from "../algolia/algolia.service";
import { Service, Trace } from "../decorators";
import { OrganizationBaseService } from "./organization-base.service";
import { IOrganization, IPopulatedOrganizationUser } from "../../backend/interfaces";
import Config from "../../async/config";
import { OrganizationPublishStatusEnum } from "./organization.enum";
import { UserService } from "../users/user.service";
import { UserFields } from "../users/user-fields";
import { ObjectId } from "../../backend/types";

const SERVICE = "organization-search.service";

@Service(SERVICE)
export class OrganizationSearchService extends OrganizationBaseService {
  public $index = `${process.env.NODE_ENV}_${AlgoliaIndicesEnum.ORGANIZATIONS}`;

  private $syncOrganizationsWithAlgoliaQueue: Queue;

  private $algoliaService: AlgoliaService;
  private $userService: UserService;

  constructor() {
    super();

    this.$syncOrganizationsWithAlgoliaQueue = Config.initQueue("syncOrganizationsWithAlgolia");

    this.$algoliaService = new AlgoliaService();
    this.$userService = new UserService();
  }

  /**
   * @summary Search Organizations using AlgoliaSearch
   *
   * @param {string} query
   * @param {number} limit
   * @param {string} userId
   *
   * @returns {Promise<IOrganization[]>}
   */
  @Trace()
  public async search(query: string, limit: number, userId?: string): Promise<IOrganization[]> {
    const results = await this.$algoliaService.search<IPopulatedOrganizationUser>(this.$index, query, limit, userId);

    // Make sure to depopulate author so that it can be resolved by graphql
    const opportunities: IOrganization[] = results.map(
      (result: IPopulatedOrganizationUser) => <IOrganization>{ ...result, user: result.user?._id || "" }
    );

    return opportunities;
  }

  /**
   * @summary Populate owner & sync with algolia
   *
   * @param {LeanDocument<IOrganization>} organization
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncWithAlgolia(organization: LeanDocument<IOrganization>): Promise<void> {
    try {
      // Remove organization from algolia if is not visible
      if (organization.publishStatus?.toString() !== OrganizationPublishStatusEnum.VISIBLE || organization.deleted) {
        this.logger?.info({ _id: organization._id }, "Removing organization from algolia");

        await this.$algoliaService.deleteObject(this.$index, organization._id);

        // Short Circuit
        return;
      }

      // Fetch the user
      const user = await this.$userService.findOne(
        { _id: organization.user },
        UserFields.ALGOLIA_FIELDS_TO_BE_SELECTED,
        { lean: true }
      );

      // Make sure user exists
      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      // Sync with algolia
      await this.$algoliaService.syncObject(this.$index, {
        ...organization,
        user: this.$algoliaService.buildUserObject(user)
      });

      this.logger?.info({ _id: organization._id }, "Synced organization with algolia");
    } catch (error) {
      this.logger?.error(error, "Error while syncing organization with algolia");
    }
  }

  /**
   * @summary Populate owner & sync with algolia using bull queue
   *
   * @param {LeanDocument<IOrganization>} organization
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncWithAlgoliaAsync(organization: LeanDocument<IOrganization>): void {
    this.$syncOrganizationsWithAlgoliaQueue
      .add({ organization })
      .catch((error) => this.logger?.error(error, "Error while adding to queue syncOrganizationsWithAlgoliaQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const organizationSearchService = new OrganizationSearchService();

    // syncOrganizationsWithAlgoliaQueue
    organizationSearchService.$syncOrganizationsWithAlgoliaQueue
      .process((job: { data: { organization: LeanDocument<IOrganization> } }) => {
        return organizationSearchService.syncWithAlgolia(job.data.organization);
      })
      .catch((error) =>
        organizationSearchService.logger?.error(error, "Error while processing syncOrganizationsWithAlgoliaQueue")
      );
  }

  /**
   * @summary Remove from algolia
   *
   * @param {ObjectId} organizationId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async deleteFromAlgolia(organizationId: ObjectId): Promise<void> {
    this.logger?.info({ organizationId }, "Removing organization from algolia");

    await this.$algoliaService.deleteObject(this.$index, <string>organizationId);
  }

  /**
   * @summary Remove from algolia asynchronously using bull queue
   *
   * @param {ObjectId} organizationId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public deleteFromAlgoliaAsync(organizationId: ObjectId): void {
    this.logger?.info({ organizationId }, "Removing organization from algolia");

    this.$algoliaService.deleteObjectAsync(this.$index, <string>organizationId);
  }
}
