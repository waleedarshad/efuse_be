import { FilterQuery, QueryOptions, Types } from "mongoose";

import { IBulkWriteResponse, IOrganizationGame } from "../../backend/interfaces";
import { OrganizationGame, OrganizationGameModel } from "../../backend/models/organization-game";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("organization-game-base.service")
export class OrganizationGameBaseService extends BaseService {
  protected readonly $organizationGames: OrganizationGameModel<IOrganizationGame> = OrganizationGame;

  public async find(condition: FilterQuery<IOrganizationGame>): Promise<IOrganizationGame[]>;
  public async find(condition: FilterQuery<IOrganizationGame>, projection: unknown): Promise<IOrganizationGame[]>;
  public async find(
    condition: FilterQuery<IOrganizationGame>,
    projection: unknown,
    options: QueryOptions
  ): Promise<IOrganizationGame[]>;
  @Trace()
  public async find(
    condition: FilterQuery<IOrganizationGame>,
    projection?: unknown,
    options?: QueryOptions
  ): Promise<IOrganizationGame[]> {
    const organizationGames = await this.$organizationGames.find(condition, projection, <any>options);
    return organizationGames;
  }

  /**
   * @summary Bulk update organization games
   *
   * @param {Types.ObjectId | string} organizationId
   * @param {Array<Types.ObjectId | string>} gameIds
   *
   * @returns {Promise<IBulkWriteResponse>}
   */
  @Trace()
  public async bulkUpdate(
    organizationId: Types.ObjectId | string,
    gameIds: Array<Types.ObjectId | string>
  ): Promise<IBulkWriteResponse> {
    const organizationQuery: Record<string, unknown> = { organization: organizationId };

    // Create or update organization games
    const bulkOperations: any[] = gameIds.map((gameId) => {
      const query: Record<string, unknown> = { ...organizationQuery, game: gameId };
      return {
        updateOne: {
          filter: query,
          update: { ...query, game: gameId, updatedAt: Date.now() },
          upsert: true, // create new object if it is not already there
          setDefaultsOnInsert: true, // this would set the schema defaults
          ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
        }
      };
    });

    // Delete games which are not passed
    bulkOperations.push({ deleteMany: { filter: { ...organizationQuery, game: { $nin: gameIds } } } });

    // Execute bulkWrite
    const response = await this.$organizationGames.bulkWrite(bulkOperations);

    return {
      upserted: response.upsertedCount || 0,
      updated: response.upsertedCount || 0,
      modified: response.modifiedCount || 0,
      deleted: response.deletedCount || 0,
      matched: response.matchedCount || 0
    };
  }

  /**
   * @summary Get a Count of number of documents
   *
   * @param {FilterQuery<IOrganizationGame>} filter
   *
   * @returns {Promise<number>}
   */
  public async countDocuments(filter?: FilterQuery<IOrganizationGame>): Promise<number> {
    let count = 0;

    count = await (filter ? this.$organizationGames.countDocuments(filter) : this.$organizationGames.countDocuments());

    return count;
  }
}
