export enum OrganizationPublishStatusEnum {
  DRAFT = "draft",
  VISIBLE = "visible",
  HIDDEN = "hidden"
}

export enum OrganizationStatusEnum {
  OPEN = "Open",
  CLOSED = "Closed",
  INVITE = "Invite",
  REQUEST = "Request"
}

export enum OrganizationInviteCodeTypesEnum {
  ORGANIZATION = "ORGANIZATION",
  ORGANIZATION_MEMBER_INVITE = "ORGANIZATION_MEMBER_INVITE"
}

export enum OrganizationMemberStatusEnum {
  BANNED = "banned",
  APPROVED = "approved"
}
