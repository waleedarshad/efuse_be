import { FilterQuery, QueryOptions } from "mongoose";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { IOrganizationRequest } from "../../backend/interfaces";
import { OrganizationRequest, OrganizationRequestModel } from "../../backend/models/organization-request";

@Service("organization-request-base.service")
export class OrganizationRequestBaseService extends BaseService {
  protected readonly $organizationRequests: OrganizationRequestModel<IOrganizationRequest> = OrganizationRequest;

  public async findOne(filter: FilterQuery<IOrganizationRequest>): Promise<IOrganizationRequest | null>;
  public async findOne(
    filter: FilterQuery<IOrganizationRequest>,
    projection: any
  ): Promise<IOrganizationRequest | null>;
  public async findOne(
    filter: FilterQuery<IOrganizationRequest>,
    projection: any,
    options: QueryOptions | null
  ): Promise<IOrganizationRequest | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IOrganizationRequest>,
    projection?: any,
    options?: QueryOptions | null
  ): Promise<IOrganizationRequest | null> {
    const organizationRequest = await this.$organizationRequests.findOne(filter, projection, options);

    return organizationRequest;
  }
}
