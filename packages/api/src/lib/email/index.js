const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const sgMail = require("@sendgrid/mail");
const { SENDGRID_API_KEY } = require("@efuse/key-store");

/**
 * @summary Send email to a person from efuse
 *
 * @param {String} to - Email of the person to send email
 * @param {String} template_id - Id of the email template
 * @param {Object} dynamic_template_data - Data t send in email
 */
const sendEmail = async (to, template_id, dynamic_template_data) => {
  try {
    if (SENDGRID_API_KEY === undefined) {
      logger.error("SENDGRID_API_KEY environment variable is undefined.");
      throw new Error("Environment variable not set.");
    }

    sgMail.setApiKey(SENDGRID_API_KEY);

    const msg = {
      template_id,
      dynamic_template_data,
      to,
      from: { name: "eFuse", email: "info@efuse.gg" }
    };

    // sendMultiple() will send individual emails to multiple recipients ... this will not be able to see the other recipients
    // todo: investigate
    // @ts-ignore
    return await sgMail.sendMultiple(msg);
  } catch (error) {
    logger.error("Email Library Error: ", error);

    if (error.response) {
      logger.error(error.response.body);
      throw error.response.body;
    } else {
      throw error;
    }
  }
};

module.exports.sendEmail = sendEmail;
