const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/twitchservice/index.js" });
const axiosLib = require("axios");

const { TWITCH_CLIENT_ID, TWITCH_SECRET } = require("@efuse/key-store");

// todo: investigate
// @ts-ignore
const axios = axiosLib.create({
  baseURL: "https://api.twitch.tv/helix",
  headers: { "Client-ID": TWITCH_CLIENT_ID }
});

const { Livestream } = require("../../backend/models/LiveStream");
const TwitchServiceAsync = require("../../async/twitch-service");

/**
 * @summary Get Oauth token
 */
const setOAuthTokenOnAxios = async () => {
  try {
    const response = await axios.post(
      `https://id.twitch.tv/oauth2/token?client_id=${TWITCH_CLIENT_ID}&client_secret=${TWITCH_SECRET}&grant_type=client_credentials`
    );
    axios.defaults.headers.common["Authorization"] = `Bearer ${response.data.access_token}`;
  } catch (error) {
    const errorData = error.response.data;
    logger.error(errorData, "Error retrieving Twitch Oauth Token");
    return;
  }
};

/**
 * @summary Get and update stream info
 *
 * @param {Array} users
 */
const fetchAndUpdateStreamInfo = async (users) => {
  try {
    await setOAuthTokenOnAxios();
    const arrayOfUserIds = await Promise.all(users.map((user) => `user_id=${user.twitchID}`));

    const userIdQueryParams = arrayOfUserIds.join("&");

    const queryParams = `first=100&${userIdQueryParams}`;

    logger.info({ queryParams }, "Fetching twitch streams");

    const response = await axios.get(`/streams?${queryParams}`);

    const { data } = response.data;

    if (!data || data.length === 0) {
      logger.info({ queryParams }, "No twitch streams found!");
      return;
    }

    logger.info({ total: data.length }, "Total streams found!");

    const streamsWithGameIds = data.filter((obj) => obj.game_id);

    const gameIds = streamsWithGameIds.map((obj) => `id=${obj.game_id}`);

    logger.info({ gameIds }, "gameIds received");

    const gameResponse = await new Promise(async (resolve, reject) => {
      try {
        const response = await axios.get(`/games?first=100&${gameIds.join("&")}`);

        return resolve({ data: response.data.data, success: true });
      } catch (error) {
        logger.error(error, "Error fetching game data");

        return resolve({ success: false });
      }
    });

    const gameData = gameResponse.success ? gameResponse.data : [];

    const formattedData = await Promise.all(
      data.map((obj) => {
        const user = users.find((user) => user.twitchID === obj.user_id);

        const object = {
          user: user ? user._id : null,
          game: gameData.find((game) => game.id === obj.game_id),
          thumbnailUrl: obj.thumbnail_url,
          views: obj.viewer_count,
          streamTitle: obj.title
        };

        if (!object.user) {
          delete object.user;
        }

        if (!object.game) {
          delete object.game;
        }

        return object;
      })
    );

    await storeStreamDataAsync(formattedData);
    logger.info("All twitch clips have been added to queue for storage");
    return;
  } catch (error) {
    return await logError(error, "Error fetching streams from twitch");
  }
};

/**
 * @summary Store stream Data
 *
 * @param {Array} streams
 */
const storeStreamData = async (streams) => {
  try {
    if (!streams || streams.length === 0) {
      logger.info("Got no active streams to store in DB");
      return;
    }
    const formattedData = await Promise.all(
      streams.map((stream) => {
        return {
          updateOne: {
            filter: { user: stream.user, streamTitle: stream.streamTitle },
            update: stream
          }
        };
      })
    );
    const res = await Livestream.bulkWrite(formattedData);
    logger.info({ updated: res.modifiedCount }, "Twitch live streams synced");
    return;
  } catch (error) {
    logger.error(error, "Error while storing stream data");
  }
};

/**
 * @summary Async method to add job to queue
 *
 * @param {Array} streams
 */
const storeStreamDataAsync = async (streams) => {
  return await TwitchServiceAsync.storeStreamDataQueue.add({ streams });
};

/**
 * @summary Log error
 *
 * @param {Object} error - Error Object
 * @param {String} description - Error description
 */
const logError = async (error, description) => {
  const { message, response } = error;
  let errMsg = message;
  if (response && response.data) {
    const { message } = response.data;
    if (message) {
      errMsg = message;
    }
  }
  logger.error({ error: errMsg }, description);
  return;
};

module.exports.fetchAndUpdateStreamInfo = fetchAndUpdateStreamInfo;
module.exports.storeStreamData = storeStreamData;
