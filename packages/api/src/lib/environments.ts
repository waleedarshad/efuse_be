export class Environments {
  public static readonly PRODUCTION: string = "production";
  public static readonly SANDBOX: string = "sandbox";
  public static readonly STAGING: string = "staging";

  public static readonly ALL: string[] = [Environments.PRODUCTION, Environments.SANDBOX, Environments.STAGING];
  public static readonly PRODUCTION_ONLY: string[] = [Environments.PRODUCTION];
  public static readonly PRODUCTION_AND_STAGING: string[] = [Environments.PRODUCTION, Environments.STAGING];
  public static readonly SANDBOX_ONLY: string[] = [Environments.SANDBOX];
  public static readonly STAGING_ONLY: string[] = [Environments.STAGING];
}
