import { Types } from "mongoose";
import { ObjectId } from "../../backend/types";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { EntityRoleEntityKindEnum, PermissionActionsEnum, ResourceOwnerKindEnum } from "./enums";
import { IPermissionsDatabaseService } from "./interfaces";
import { EFusePermissionsDatabaseService } from "./services/efuse-permissions-database.service";
import { RoleValue } from "./types/role-value.type";

@Service("ef-permission.service")
export class EFPermissionService extends BaseService {
  private permissionDatabaseService: IPermissionsDatabaseService;

  constructor(permissionDatabaseService: IPermissionsDatabaseService) {
    super();

    this.permissionDatabaseService = permissionDatabaseService;
  }

  public static new(): EFPermissionService {
    const databaseService = EFusePermissionsDatabaseService.new();

    return new EFPermissionService(databaseService);
  }

  @Trace()
  public async entityHasPermission(
    entityId: ObjectId,
    entityKind: EntityRoleEntityKindEnum,
    resourceValue: string,
    action: PermissionActionsEnum,
    resourceOwner?: ObjectId,
    resourceOwnerKind?: ResourceOwnerKindEnum
  ): Promise<boolean> {
    try {
      return this.permissionDatabaseService.entityHasPermission(entityId, entityKind, resourceValue, action, {
        owner: <Types.ObjectId>resourceOwner,
        ownerKind: resourceOwnerKind
      });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async entityHasRole(entityId: ObjectId, roleIds: ObjectId[]): Promise<boolean> {
    try {
      return this.permissionDatabaseService.hasRoles(entityId, roleIds);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async entityHasRoleByRoleValue(entityId: ObjectId, roleValues: RoleValue[]): Promise<boolean> {
    try {
      return this.permissionDatabaseService.hasRolesByRoleValues(entityId, roleValues);
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
