export * from "./entity-role.model";
export * from "./permission.model";
export * from "./resource-owner.model";
export * from "./resource.model";
export * from "./role-owner.model";
export * from "./role.model";
