import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose, index, Ref } from "@typegoose/typegoose";
import { timestamps } from "../../../backend/helpers/timestamp.helper";
import { OwnerType } from "@efuse/entities";
import { Role } from "./role.model";

@plugin(timestamps)
@index({ owner: 1, role: 1 }, { unique: true })
export class RoleOwner {
  @prop({ required: true })
  owner!: Types.ObjectId;

  @prop({ type: String, enum: OwnerType, required: true })
  ownerKind!: OwnerType;

  @prop({ ref: Role, required: true })
  role!: Ref<Role>;
}

const roleOwnerSchema = buildSchema(RoleOwner);

export const RoleOwnerModel = addModelToTypegoose(model("roleowner", roleOwnerSchema), RoleOwner);
