import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose, Ref, index } from "@typegoose/typegoose";

import { timestamps } from "../../../backend/helpers/timestamp.helper";
import { PermissionActionsEnum, PermissionGranteeKindEnum } from "../enums";
import { Resource } from "./resource.model";

@plugin(timestamps)
@index({ grantee: 1, resource: 1 }, { unique: true })
export class Permission {
  @prop({ type: String, enum: PermissionActionsEnum, required: true })
  action!: PermissionActionsEnum;

  @prop({ required: true })
  grantee!: Types.ObjectId;

  @prop({ type: String, enum: PermissionGranteeKindEnum, required: true })
  granteeKind!: PermissionGranteeKindEnum;

  @prop({ ref: Resource, required: true })
  resource!: Ref<Resource>;
}

const permissionSchema = buildSchema(Permission);

export const PermissionModel = addModelToTypegoose(model("permission", permissionSchema), Permission);
