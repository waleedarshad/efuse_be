import { model, Schema, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";

import { timestamps } from "../../../backend/helpers/timestamp.helper";
import { ResourceKindEnum } from "../enums";

@plugin(timestamps)
export class Resource {
  @prop({ type: String, enum: ResourceKindEnum, required: true })
  kind!: ResourceKindEnum;

  @prop({ type: String, required: true })
  name!: string;

  // Value here can range from an object ID to a unique string, e.g. `clippy-tier1-subscriber`.
  @prop({ type: Schema.Types.Mixed, required: true })
  value!: Types.ObjectId | string;
}

const resourceSchema = buildSchema(Resource);

export const ResourceModel = addModelToTypegoose(model("resource", resourceSchema), Resource);
