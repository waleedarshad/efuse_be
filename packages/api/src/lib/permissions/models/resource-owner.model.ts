import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose, index } from "@typegoose/typegoose";
import { timestamps } from "../../../backend/helpers/timestamp.helper";
import { ResourceOwnerKindEnum } from "../enums";

@plugin(timestamps)
@index({ owner: 1, resource: 1 }, { unique: true })
export class ResourceOwner {
  @prop({ required: true })
  owner!: Types.ObjectId;

  @prop({ type: String, enum: ResourceOwnerKindEnum, required: true })
  ownerKind!: ResourceOwnerKindEnum;

  @prop({ required: true })
  resource!: Types.ObjectId;
}

const resourceOwnerSchema = buildSchema(ResourceOwner);

export const ResourceOwnerModel = addModelToTypegoose(model("resourceowner", resourceOwnerSchema), ResourceOwner);
