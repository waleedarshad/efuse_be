import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { model } from "mongoose";
import { timestamps } from "../../../backend/helpers/timestamp.helper";
import { RoleValue } from "../types/role-value.type";

@plugin(timestamps)
export class Role {
  @prop({ type: String, required: true })
  name!: string;

  @prop({ type: String, required: false, unique: true })
  value?: RoleValue;
}

const roleSchema = buildSchema(Role);

export const RoleModel = addModelToTypegoose(model("role", roleSchema), Role);
