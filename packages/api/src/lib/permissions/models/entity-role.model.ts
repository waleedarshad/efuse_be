import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose, Ref } from "@typegoose/typegoose";

import { timestamps } from "../../../backend/helpers/timestamp.helper";
import { Role } from "./role.model";
import { EntityRoleEntityKindEnum } from "../enums";

@plugin(timestamps)
export class EntityRole {
  @prop({ required: true })
  entity!: Types.ObjectId;

  @prop({ type: String, enum: EntityRoleEntityKindEnum, required: true })
  entityKind!: EntityRoleEntityKindEnum;

  @prop({ ref: Role, required: true })
  role!: Ref<Role>;
}

const entityRoleSchema = buildSchema(EntityRole);

export const EntityRoleModel = addModelToTypegoose(model("entityrole", entityRoleSchema), EntityRole);
