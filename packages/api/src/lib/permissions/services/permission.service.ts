import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../../base";
import { Permission, PermissionModel } from "../models";

export class PermissionService extends BaseModelService<DocumentType<Permission>> {
  constructor() {
    super(PermissionModel);
  }
}
