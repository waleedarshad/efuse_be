import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../../base";
import { EntityRole, EntityRoleModel } from "../models";

export class EntityRoleService extends BaseModelService<DocumentType<EntityRole>> {
  constructor() {
    super(EntityRoleModel);
  }
}
