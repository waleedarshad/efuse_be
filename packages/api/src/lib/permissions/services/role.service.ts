import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../../base";
import { Role, RoleModel } from "../models";

export class RoleService extends BaseModelService<DocumentType<Role>> {
  constructor() {
    super(RoleModel);
  }
}
