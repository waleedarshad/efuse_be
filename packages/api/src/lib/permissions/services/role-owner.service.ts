import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../../base";
import { RoleOwner, RoleOwnerModel } from "../models";

export class RoleOwnerService extends BaseModelService<DocumentType<RoleOwner>> {
  constructor() {
    super(RoleOwnerModel);
  }
}
