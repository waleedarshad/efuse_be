import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../../base";
import { ResourceOwner, ResourceOwnerModel } from "../models";

export class ResourceOwnerService extends BaseModelService<DocumentType<ResourceOwner>> {
  constructor() {
    super(ResourceOwnerModel);
  }
}
