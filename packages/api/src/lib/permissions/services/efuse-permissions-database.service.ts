import { FilterQuery, Types } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { isObjectId, ObjectId } from "../../../backend/types";
import { EntityRole, Permission, Resource, ResourceOwner, Role, RoleOwner } from "../models";
import {
  EntityRoleService,
  PermissionService,
  ResourceOwnerService,
  ResourceService,
  RoleOwnerService,
  RoleService
} from "./index";
import { EntityRoleEntityKindEnum, PermissionActionsEnum, PermissionGranteeKindEnum } from "../enums";
import {
  IEntityRole,
  IOptionalCreateRoleArgs,
  IPermission,
  IPermissionsDatabaseService,
  IResource,
  IResourceOwner,
  IRole,
  IRoleOwner
} from "../interfaces";
import { isEmpty } from "class-validator";
import { Failure } from "@efuse/contracts";
import { Service, Trace } from "@efuse/decorators";
import { BaseService } from "../../base";
import { RoleValue } from "../types/role-value.type";

// TODO:
//  1. Should have pass { lean: true } to all of the Mongoose queries?
//  2. Standardize ID naming system.

@Service({ name: "efuse-permissions-database.service" })
export class EFusePermissionsDatabaseService extends BaseService implements IPermissionsDatabaseService {
  private entityRoleService: EntityRoleService;
  private permissionService: PermissionService;
  private resourceOwnerService: ResourceOwnerService;
  private resourceService: ResourceService;
  private roleOwnerService: RoleOwnerService;
  private roleService: RoleService;

  constructor(
    entityRoleService: EntityRoleService,
    permissionService: PermissionService,
    resourceOwnerService: ResourceOwnerService,
    resourceService: ResourceService,
    roleOwnerService: RoleOwnerService,
    roleService: RoleService
  ) {
    super();

    this.entityRoleService = entityRoleService;
    this.permissionService = permissionService;
    this.resourceOwnerService = resourceOwnerService;
    this.resourceService = resourceService;
    this.roleOwnerService = roleOwnerService;
    this.roleService = roleService;
  }

  @Trace()
  public static new(): EFusePermissionsDatabaseService {
    const entityRoleService = new EntityRoleService();
    const permissionService = new PermissionService();
    const resourceOwnerService = new ResourceOwnerService();
    const resourceService = new ResourceService();
    const roleOwnerService = new RoleOwnerService();
    const roleService = new RoleService();

    return new EFusePermissionsDatabaseService(
      entityRoleService,
      permissionService,
      resourceOwnerService,
      resourceService,
      roleOwnerService,
      roleService
    );
  }

  @Trace()
  public async assignRoleByRoleValue(
    roleValue: RoleValue,
    entityId: ObjectId,
    entityKind: EntityRoleEntityKindEnum
  ): Promise<IEntityRole> {
    try {
      const foundRole = await this.roleService.findOne({ value: roleValue }, { _id: true }, { lean: true });

      if (!foundRole) {
        throw Failure.NotFound("Cannot find role from role value", { foundRole, roleValue, entityId, entityKind });
      }

      return this.assignRole(foundRole._id, entityId, entityKind);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async assignRole(
    roleId: ObjectId,
    entityId: ObjectId,
    entityKind: EntityRoleEntityKindEnum
  ): Promise<IEntityRole> {
    try {
      const foundEntityRole = await this.entityRoleService.findOne(
        { role: roleId, entity: entityId },
        {},
        { lean: true }
      );

      if (foundEntityRole) {
        // The role property is defined as a Typegoose Ref<> object which is ultimately an ObjectId
        // but Typescript is unaware of this.
        return <IEntityRole>foundEntityRole;
      }

      // The role property is defined as a Typegoose Ref<> object which is ultimately an ObjectId
      // but Typescript is unaware of this.
      return <IEntityRole>(
        await this.entityRoleService.create(<DocumentType<EntityRole>>{ role: roleId, entity: entityId, entityKind })
      );
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createRole(newRole: IRole, optionalCreateRoleArgs?: IOptionalCreateRoleArgs): Promise<IRole> {
    try {
      if (isEmpty(newRole)) {
        throw Failure.BadRequest("Role is empty", newRole);
      }

      const role = await this.roleService.create(<DocumentType<Role>>newRole);

      await this.handleOptionalArgumentsForRoleCreation(role._id, optionalCreateRoleArgs);

      return role;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async deleteRole(roleId: ObjectId, roleToMoveEntitiesTo?: ObjectId): Promise<boolean> {
    try {
      if (roleToMoveEntitiesTo) {
        // TODO: Do we need to await here?
        await this.entityRoleService.updateMany(
          { role: roleId },
          { role: roleToMoveEntitiesTo },
          { lean: true, new: true }
        );
      }

      const finishedPromises = await Promise.all([
        this.roleService.delete(roleId),
        this.roleOwnerService.deleteOne({ role: roleId }, { lean: true }),
        this.entityRoleService.deleteMany({ role: roleId }, { lean: true }),
        this.permissionService.deleteMany(
          { grantee: roleId, granteeKind: PermissionGranteeKindEnum.ROLE },
          { lean: true }
        )
      ]);

      return finishedPromises != null;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async deleteManyRoles(roleIds: ObjectId[], roleToMoveEntitiesTo?: ObjectId): Promise<boolean>;
  public async deleteManyRoles(
    filter: Partial<IRole>,
    owner?: Partial<IRoleOwner>,
    roleToMoveEntitiesTo?: ObjectId
  ): Promise<boolean>;
  @Trace()
  public async deleteManyRoles(
    value: ObjectId[] | Partial<IRole>,
    ownerOrNewRole?: Partial<IRoleOwner> | ObjectId,
    roleToMoveEntitiesTo?: ObjectId
  ): Promise<boolean> {
    try {
      let roleIds: ObjectId[];
      let newRole: ObjectId | undefined;
      let owner: Partial<IRoleOwner> | undefined;
      const promises: Promise<boolean>[] = [];

      if (Array.isArray(value)) {
        roleIds = value;
        newRole = <ObjectId>ownerOrNewRole;
      } else {
        const roles = await this.findRoles(value);
        newRole = roleToMoveEntitiesTo;
        owner = <Partial<IRoleOwner> | undefined>ownerOrNewRole;

        // TODO: This should probably use whatever the MongoDB equivalent of a join is instead of
        // getting everything and then filtering the list in code.
        roleIds = (owner ? await this.filterRolesByOwner(roles, owner) : roles).map((role) => role._id);
      }

      // [PERFORMANCE WATCH]
      // This is not nearly as efficient as using the single-call deleteMany base method, but that gives us no good way
      // of ensuring cascading deletions. This shouldn't be used too often so I think it is fine for now. If we
      // determine it to be a performance problem we can fix it then.
      for (const roleId of roleIds) {
        promises.push(this.deleteRole(roleId, newRole));
      }

      return (await Promise.all(promises)).every((promise) => promise);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async findOneRole(id: ObjectId): Promise<IRole | null>;
  public async findOneRole(filter: Partial<IRole>): Promise<IRole | null>;
  @Trace()
  public async findOneRole(value: ObjectId | Partial<IRole>): Promise<IRole | null> {
    try {
      let filter: FilterQuery<DocumentType<Role>> = {};

      if (isObjectId(value)) {
        if (!this.isValidObjectId(value)) {
          throw Failure.BadRequest("Invalid role id", { id: value });
        }

        filter = {
          _id: value
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Role filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Role>>>value;
      }

      return this.roleService.findOne(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async findRoles(ids: ObjectId[]): Promise<IRole[]>;
  public async findRoles(filter: Partial<IRole>): Promise<IRole[]>;
  @Trace()
  public async findRoles(value: ObjectId[] | Partial<IRole>): Promise<IRole[]> {
    try {
      let filter: FilterQuery<DocumentType<Role>> = {};

      if (Array.isArray(value)) {
        if (
          value.some((id) => {
            !this.isValidObjectId(id);
          })
        ) {
          throw Failure.BadRequest("Invalid role id");
        }

        filter = {
          _id: { $in: value }
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Role filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Role>>>value;
      }

      return this.roleService.find(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findRolesByRoleValues(roleValues: RoleValue[]): Promise<IRole[]> {
    try {
      const foundRoles = await this.roleService.find({ value: { $in: roleValues } }, {}, { lean: true });

      if (!foundRoles || foundRoles.length === 0) {
        this.logger?.debug("No roles found for role value", { roleValues });

        return [];
      }

      return foundRoles;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findRolesByOwner(ownerId: ObjectId): Promise<IRole[]> {
    try {
      const foundRoleOwners = await this.roleOwnerService.find({ owner: ownerId }, { role: true }, { lean: true });

      if (!foundRoleOwners || foundRoleOwners.length === 0) {
        this.logger?.debug("No roles found for owner", { ownerId });

        return [];
      }

      const roleIds = foundRoleOwners.map((roleOwner) => roleOwner.role);

      return this.roleService.find({ _id: { $in: roleIds } }, {}, { lean: true });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findRolesForEntity(entityId: ObjectId, entityKind?: EntityRoleEntityKindEnum): Promise<IRole[]> {
    try {
      const query = entityKind ? { entity: entityId, entityKind } : { entity: entityId };

      const foundEntityRoles = await this.entityRoleService.find(query, { role: true }, { lean: true });

      if (!foundEntityRoles || foundEntityRoles.length === 0) {
        this.logger?.debug("No permission entity roles found", { entityId });

        return [];
      }

      const roleIds = foundEntityRoles.map((entityRole) => entityRole.role);

      return this.roleService.find({ _id: { $in: roleIds } }, {}, { lean: true });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async hasRolesByRoleValues(entityId: ObjectId, roleValues: RoleValue[]): Promise<boolean> {
    try {
      const foundRoles = await this.roleService.find({ value: { $in: roleValues } }, { _id: true }, { lean: true });

      if (!foundRoles || foundRoles.length === 0) {
        this.logger?.debug("No role found from the role value", { entityId, roleValues, foundRoles });

        return false;
      }

      const roleIds = foundRoles.map((role) => role._id);

      return this.hasRoles(entityId, roleIds);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async hasRole(entityId: ObjectId, roleId: ObjectId): Promise<boolean> {
    try {
      return this.entityRoleService.exists({
        entity: entityId,
        role: roleId
      });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async hasRoles(entityId: ObjectId, roleIds: ObjectId[]): Promise<boolean> {
    try {
      return this.entityRoleService.exists({
        entity: entityId,
        role: { $in: roleIds }
      });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async unassignRole(roleId: ObjectId, entityId: ObjectId): Promise<boolean> {
    try {
      return this.entityRoleService.deleteOne({ role: roleId, entity: entityId }, { lean: true });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async updateRole(roleId: ObjectId, body: Partial<IRole>): Promise<IRole> {
    try {
      if (!this.isValidObjectId(roleId)) {
        throw Failure.BadRequest("Invalid role id", { roleId });
      }

      return this.roleService.updateOne({ _id: roleId }, body);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createPermission(permission: IPermission): Promise<IPermission> {
    try {
      // The resource property is defined as a Typegoose Ref<> object which is ultimately an ObjectId
      // but Typescript is unaware of this.
      return <IPermission>await this.permissionService.create(<DocumentType<Permission>>permission);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async deletePermission(id: ObjectId): Promise<boolean> {
    try {
      return this.permissionService.delete(id);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async deleteManyPermissions(ids: ObjectId[]): Promise<boolean>;
  public async deleteManyPermissions(filter: Partial<IPermission>): Promise<boolean>;
  @Trace()
  public async deleteManyPermissions(value: ObjectId[] | Partial<IPermission>): Promise<boolean> {
    try {
      let filter: FilterQuery<DocumentType<Permission>>;

      if (Array.isArray(value)) {
        filter = {
          _id: { $in: value }
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Permission filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Permission>>>value;
      }

      return this.permissionService.deleteMany(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async findOnePermission(id: ObjectId): Promise<IPermission | null>;
  public async findOnePermission(filter: Partial<IPermission>): Promise<IPermission | null>;
  @Trace()
  public async findOnePermission(value: ObjectId | Partial<IPermission>): Promise<IPermission | null> {
    try {
      let filter: FilterQuery<DocumentType<Permission>> = {};

      if (isObjectId(value)) {
        if (!this.isValidObjectId(value)) {
          throw Failure.BadRequest("Invalid permission id", { id: value });
        }

        filter = {
          _id: value
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Permission filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Permission>>>value;
      }

      // The resource property is defined as a Typegoose Ref<> object which is ultimately an ObjectId
      // but Typescript is unaware of this.
      return <IPermission>await this.permissionService.findOne(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async findPermissions(ids: ObjectId[]): Promise<IPermission[]>;
  public async findPermissions(filter: Partial<IPermission>): Promise<IPermission[]>;
  @Trace()
  public async findPermissions(value: ObjectId[] | Partial<IPermission>): Promise<IPermission[]> {
    try {
      let filter: FilterQuery<DocumentType<Permission>> = {};

      if (Array.isArray(value)) {
        if (
          value.some((id) => {
            !this.isValidObjectId(id);
          })
        ) {
          throw Failure.BadRequest("Invalid permission id");
        }

        filter = {
          _id: { $in: value }
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Permission filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Permission>>>value;
      }

      // The resource property is defined as a Typegoose Ref<> object which is ultimately an ObjectId
      // but Typescript is unaware of this.
      return <IPermission[]>await this.permissionService.find(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  // TODO: Combine this into an overload with entityHasPermission that is simply hasPermission().
  @Trace()
  public async roleHasPermission(
    roleId: ObjectId,
    resourceValue: string,
    action: PermissionActionsEnum
  ): Promise<boolean> {
    try {
      const foundResources = await this.findResources({ value: resourceValue });

      const foundEmptyValue = isEmpty(foundResources);
      if (foundEmptyValue) {
        this.logger?.debug("No resources found", { roleId, resourceValue, foundResources });

        return !foundEmptyValue;
      }

      const resourceIds: ObjectId[] = foundResources.map((resource) => resource._id);

      // here we find permissions for either roles related to the entity or permissions given directly to an entity
      const foundAppropriatePermissions = await this.permissionService.find(
        {
          resource: { $in: resourceIds },
          action: action,
          $or: [{ grantee: roleId }]
        },
        {},
        {
          lean: true
        }
      );

      return !isEmpty(foundAppropriatePermissions);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * This takes into consideration entities with roles and entities given a direct permission
   */
  @Trace()
  public async entityHasPermission(
    entityId: ObjectId,
    entityKind: EntityRoleEntityKindEnum,
    resourceValue: string,
    action: PermissionActionsEnum,
    owner?: Partial<IResourceOwner>
  ): Promise<boolean> {
    try {
      const resourceQuery = await this.createResourceQueryForCheckingPermissions(resourceValue, owner);

      const [foundRoles, foundResources] = await Promise.all([
        this.findRolesForEntity(entityId, entityKind),
        this.resourceService.find(resourceQuery, {}, { lean: true })
      ]);

      const foundEmptyValue = !foundResources || foundResources.length === 0;
      if (foundEmptyValue) {
        this.logger?.debug("No resources found", { entityId, resourceValue, foundRoles, foundResources });

        return !foundEmptyValue;
      }

      const roleIds: ObjectId[] = foundRoles.map((role) => role._id);
      const resourceIds: ObjectId[] = foundResources.map((resource) => resource._id);

      // here we find permissions for either roles related to the entity or permissions given directly to an entity
      const foundAppropriatePermissions = await this.permissionService.exists({
        resource: { $in: resourceIds },
        action: action,
        $or: [{ grantee: { $in: roleIds } }, { grantee: entityId }]
      });

      return foundAppropriatePermissions;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async updatePermission(id: ObjectId, body: Partial<IPermission>): Promise<IPermission> {
    try {
      if (!this.isValidObjectId(id)) {
        throw Failure.BadRequest("Invalid permission id", { id });
      }

      if (isEmpty(body)) {
        throw Failure.BadRequest("Permission is empty", body);
      }

      // The resource property is defined as a Typegoose Ref<> object which is ultimately an ObjectId
      // but Typescript is unaware of this.
      return <IPermission>await this.permissionService.updateOne({ _id: id }, body);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  // TODO: Management for the related tables:
  // 1. ResourceOwner
  // 2. RoleInheritance
  // 3. RoleOwner

  // TODO: Return both the Resource and ResourceOwner?
  @Trace()
  public async createResource(resource: IResource, owner?: Omit<IResourceOwner, "resource">): Promise<IResource> {
    try {
      const result = await this.resourceService.create(<DocumentType<Resource>>resource);

      if (result && owner) {
        const newOwner = <DocumentType<ResourceOwner>>{
          ...owner,
          resource: result._id
        };
        const ownerResult = await this.resourceOwnerService.create(newOwner);
      }

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async deleteResource(id: ObjectId): Promise<boolean> {
    try {
      const resourceDeleted = await this.resourceService.delete(id);
      const resourceOwnersDeleted = await this.resourceOwnerService.deleteMany({ resource: id });
      const permissionsDeleted = await this.permissionService.deleteMany({ resource: id });

      return resourceDeleted && resourceOwnersDeleted && permissionsDeleted;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async deleteManyResources(ids: ObjectId[]): Promise<boolean>;
  public async deleteManyResources(filter: Partial<IResource>, owner?: Partial<IResourceOwner>): Promise<boolean>;
  @Trace()
  public async deleteManyResources(
    value: ObjectId[] | Partial<IResource>,
    owner?: Partial<IResourceOwner>
  ): Promise<boolean> {
    try {
      let resourceIds: ObjectId[];
      const promises: Promise<boolean>[] = [];

      if (Array.isArray(value)) {
        resourceIds = value;
      } else {
        const resources = await this.findResources(value);

        // TODO: This should probably use whatever the MongoDB equivalent of a join is instead of
        // getting everything and then filtering the list in code.
        resourceIds = (owner ? await this.filterResourcesByOwner(resources, owner) : resources).map(
          (resource) => resource._id
        );
      }

      // [PERFORMANCE WATCH]
      // This is not nearly as efficient as using the single-call deleteMany base method, but that gives us no good way
      // of ensuring cascading deletions. This shouldn't be used too often so I think it is fine for now. If we
      // determine it to be a performance problem we can fix it then.
      for (const resourceId of resourceIds) {
        promises.push(this.deleteResource(resourceId));
      }

      return (await Promise.all(promises)).every((promise) => promise);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async findOneResource(id: ObjectId): Promise<IResource | null>;
  public async findOneResource(filter: Partial<IResource>): Promise<IResource | null>;
  @Trace()
  public async findOneResource(value: ObjectId | Partial<IResource>): Promise<IResource | null> {
    try {
      let filter: FilterQuery<DocumentType<Resource>> = {};

      if (isObjectId(value)) {
        if (!this.isValidObjectId(value)) {
          throw Failure.BadRequest("Invalid resource id", { id: value });
        }

        filter = {
          _id: value
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Resource filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Resource>>>value;
      }

      return this.resourceService.findOne(filter);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async findResources(ids: ObjectId[]): Promise<IResource[]>;
  public async findResources(filter: Partial<IResource>, owner?: Partial<IResourceOwner>): Promise<IResource[]>;
  @Trace()
  public async findResources(
    value: ObjectId[] | Partial<IResource>,
    owner?: Partial<IResourceOwner>
  ): Promise<IResource[]> {
    try {
      let filter: FilterQuery<DocumentType<Resource>> = {};

      if (Array.isArray(value)) {
        if (
          value.some((id) => {
            !this.isValidObjectId(id);
          })
        ) {
          throw Failure.BadRequest("Invalid resource id");
        }

        filter = {
          _id: { $in: value }
        };
      } else {
        if (isEmpty(value)) {
          throw Failure.BadRequest("Resource filter is empty", value);
        }

        filter = <FilterQuery<DocumentType<Resource>>>value;
      }

      const resources = await this.resourceService.find(filter);

      // TODO: This should probably use whatever the MongoDB equivalent of a join is instead of
      // getting everything and then filtering the list in code.
      return owner ? this.filterResourcesByOwner(resources, owner) : resources;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async updateResource(id: ObjectId, body: Partial<IResource>): Promise<IResource> {
    try {
      if (!this.isValidObjectId(id)) {
        throw Failure.BadRequest("Invalid resource id", { id });
      }

      if (isEmpty(body)) {
        throw Failure.BadRequest("Resource is empty", body);
      }

      return this.resourceService.updateOne({ _id: id }, body);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async createResourceQueryForCheckingPermissions(
    resourceValue: string,
    owner?: Partial<IResourceOwner>
  ): Promise<FilterQuery<DocumentType<Resource>>> {
    try {
      let foundResourceOwners: IResourceOwner[] = [];
      if (owner) {
        foundResourceOwners = await this.resourceOwnerService.find(
          { owner: owner.owner, ownerKind: owner.ownerKind },
          {},
          { lean: true }
        );
      }

      const resourceIds: Types.ObjectId[] = foundResourceOwners
        ? foundResourceOwners.map((resourceOwner) => resourceOwner.resource)
        : [];

      const resourceQuery: FilterQuery<DocumentType<Resource>> =
        resourceIds && resourceIds.length > 0
          ? { value: resourceValue, _id: { $in: resourceIds } }
          : { value: resourceValue };

      return resourceQuery;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async handleOptionalArgumentsForRoleCreation(
    roleId: Types.ObjectId,
    optionalCreateRoleArgs?: IOptionalCreateRoleArgs
  ): Promise<void> {
    try {
      if (optionalCreateRoleArgs == null) {
        return void 0;
      }

      const arrayPromises: Promise<any>[] = [];

      const { owner, ownerKind } = optionalCreateRoleArgs;

      if ((owner && ownerKind == null) || (owner == null && ownerKind)) {
        throw Failure.BadRequest("Owner and ownerKind must both be set", { optionalCreateRoleArgs });
      }

      if (owner && ownerKind) {
        const roleOwnerExists = await this.roleOwnerService.exists({ role: roleId, owner });

        if (!roleOwnerExists) {
          const roleOwner = <DocumentType<RoleOwner>>{ role: roleId, owner, ownerKind };
          const roleOwnerPromise = this.roleOwnerService.create(roleOwner);
          arrayPromises.push(roleOwnerPromise);
        }
      }

      if (arrayPromises && arrayPromises.length > 0) {
        await Promise.all(arrayPromises);
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async filterRolesByOwner(roles: IRole[], filter: Partial<IRoleOwner>): Promise<IRole[]> {
    try {
      // TODO: This should probably only happen if the filter has ownerKind set but not owner.
      const owners = await this.roleOwnerService.find(<FilterQuery<DocumentType<RoleOwner>>>filter);

      const filteredRoles = roles.filter((role) => {
        owners.some((owner) => {
          owner.role === role._id;
        });
      });

      return filteredRoles;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async filterResourcesByOwner(resources: IResource[], filter: Partial<IResourceOwner>): Promise<IResource[]> {
    try {
      // TODO: This should probably only happen if the filter has ownerKind set but not owner.
      const owners = await this.resourceOwnerService.find(<FilterQuery<DocumentType<ResourceOwner>>>filter);

      const filteredResources = resources.filter((resource) => {
        owners.some((owner) => {
          owner.resource === resource._id;
        });
      });

      return filteredResources;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
