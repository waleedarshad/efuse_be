import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../../base";
import { Resource, ResourceModel } from "../models";

export class ResourceService extends BaseModelService<DocumentType<Resource>> {
  constructor() {
    super(ResourceModel);
  }
}
