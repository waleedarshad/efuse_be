export enum ResourceOwnerKindEnum {
  ORGANIZATIONS = "organizations",
  USERS = "users"
}
