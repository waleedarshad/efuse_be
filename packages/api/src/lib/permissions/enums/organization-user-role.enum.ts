export enum OrganizationUserRoleEnum {
  OWNER = "owner",
  CAPTAIN = "captain",
  MEMBER = "member",
  NONE = "none"
}
