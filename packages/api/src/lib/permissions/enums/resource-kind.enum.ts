export enum ResourceKindEnum {
  CLIPPY_FEATURE = "clippyFeature",
  LEAGUE = "leagues",
  ORGANIZATION_ACTION = "organizationAction"
}
