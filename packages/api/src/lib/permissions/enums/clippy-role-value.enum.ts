/**
 * Be careful when changing an enum value on this.
 * The values on this are meant to act as an identifier to the clippy roles we setup.
 */
export enum ClippyRoleValueEnum {
  CLIPPY_FREE = "clippyFree"
}
