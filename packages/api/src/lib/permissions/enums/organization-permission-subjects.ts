export enum OrganizationPermissionSubjects {
  PIPELINE_RECRUIT = "pipeline_recruit",
  CREATE_LEAGUE = "create_league"
}
