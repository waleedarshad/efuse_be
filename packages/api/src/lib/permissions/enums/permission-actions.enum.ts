export enum PermissionActionsEnum {
  ACCESS = "access",
  ALL = "all",
  CREATE = "create",
  DELETE = "delete",
  READ = "read",
  UPDATE = "update"
}
