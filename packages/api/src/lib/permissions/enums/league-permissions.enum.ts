export enum LeaguePermissionsEnum {
  LEAGUES = "leagues",
  CREATE_LEAGUE = "create_league"
}
