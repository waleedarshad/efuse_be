export enum PermissionGranteeKindEnum {
  ORGANIZATION = "organization",
  ROLE = "role",
  USER = "user"
}
