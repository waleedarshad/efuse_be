import { Trace } from "@efuse/decorators";
import { ObjectId } from "../../../../backend/types";
import { EntityRoleEntityKindEnum, PermissionActionsEnum } from "../../enums";
import { IPolicy } from "../../interfaces";

export class PipelineRecruitmentGenerator {
  public static resource = "pipeline_recruitment";
  public static action = PermissionActionsEnum.ACCESS;

  @Trace()
  public static generate(entityId: ObjectId, entityKind: EntityRoleEntityKindEnum): IPolicy {
    return {
      entityId: entityId,
      entityKind: entityKind,
      action: this.action,
      resource: this.resource
    };
  }
}
