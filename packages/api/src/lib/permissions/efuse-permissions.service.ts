import { BaseModelService } from "../base";
import { Service } from "../decorators";
import { IEfusePermission } from "../../backend/interfaces";
import { EfusePermission, EfusePermissionModel } from "../../backend/models/efuse-permission";

@Service("efuse-permissions.service")
export class EFusePermissionsService extends BaseModelService<IEfusePermission> {
  private efusePermission: EfusePermissionModel<IEfusePermission>;

  constructor() {
    super(EfusePermission);

    this.efusePermission = EfusePermission;
  }
}
