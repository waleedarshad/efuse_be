import { Trace } from "@efuse/decorators";
import { Types } from "mongoose";
import { LeaguePermissionsEnum, PermissionActionsEnum } from "../../enums";
import { IEfusePermission } from "../../../../backend/interfaces";

// TODO: The methods here should use our existing ObjectId type, but that is going to require moving
// that type to a different package and that isn't the most straightforward with our current state
// of packages being in flux. Definitely don't want to be setting up circular dependencies.

export class LeaguePolicyBuilder {
  /**
   * Gets a policy that represents the permission of one organization to join the host's leagues.
   * @param joiningOrgId The ID of the organization that is joining the host organization's leagues.
   * @param hostOrgId The ID of the organization that is hosting the leagues.
   * @returns A policy representing the permission.
   */
  @Trace()
  public static joinOrgPolicy(
    joiningOrgId: Types.ObjectId | string,
    hostOrgId: Types.ObjectId | string
  ): IEfusePermission {
    return <IEfusePermission>{
      ...this.joinedOrgsPolicy(joiningOrgId),
      tenant: hostOrgId.toString()
    };
  }

  /**
   * Gets a policy that can be used to find all policies where an organization has joined another's leagues.
   * @param orgId The ID of the organization to find joined organizations (leagues) for.
   * @returns A policy representing the permission.
   */
  @Trace()
  public static joinedOrgsPolicy(orgId: Types.ObjectId | string): IEfusePermission {
    return <IEfusePermission>{
      subject: orgId.toString(),
      object: LeaguePermissionsEnum.LEAGUES,
      action: PermissionActionsEnum.ACCESS
    };
  }

  /**
   * Gets a policy that represents the permission of one organization to create leagues for a specific organization (may be the same one).
   * @param orgId The ID of the organization that is being checked for permissions to create leagues.
   * @param hostOrgId The ID of the organization that is hosting the leagues.
   * @returns A policy representing the permission.
   */
  @Trace()
  public static createLeagueForOrgPolicy(
    orgId: Types.ObjectId | string,
    hostOrgId: Types.ObjectId | string
  ): IEfusePermission {
    return <IEfusePermission>{
      ...this.createLeaguePolicy(orgId),
      tenant: hostOrgId.toString()
    };
  }

  /**
   * Gets a policy that can be used to find all policies where an organization can create leagues.
   * @param orgId The ID of the organization to find create leagues policies for.
   * @returns A policy representing the permission.
   */
  @Trace()
  public static createLeaguePolicy(orgId: Types.ObjectId | string): IEfusePermission {
    return <IEfusePermission>{
      subject: orgId.toString(),
      object: LeaguePermissionsEnum.CREATE_LEAGUE,
      action: PermissionActionsEnum.ACCESS
    };
  }
}
