import { ObjectId } from "../../../backend/types";
import { ClippyResourceValueEnum, EFuseResourceValueEnum } from "../enums/resource-enums";

export type PermissionResourceValueType = ObjectId | EFuseResourceValueEnum | ClippyResourceValueEnum;
