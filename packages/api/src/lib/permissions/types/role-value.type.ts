import { ClippyRoleValueEnum } from "../enums";

/**
 * This is a union for the different enums that role value could have.
 */
export type RoleValue = ClippyRoleValueEnum;
