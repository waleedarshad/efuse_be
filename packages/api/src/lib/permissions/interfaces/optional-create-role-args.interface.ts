import { Types } from "mongoose";
import { OwnerType } from "@efuse/entities";

export interface IOptionalCreateRoleArgs {
  owner?: Types.ObjectId;
  ownerKind?: OwnerType;
}
