import { Document, Types } from "mongoose";
import { EntityRoleEntityKindEnum } from "../enums";

// I would love to make this more generic, but right now we're simply too tied to MongoDB for all
// of our objects.
export interface IEntityRole extends Document {
  entity: Types.ObjectId;
  entityKind: EntityRoleEntityKindEnum;
  role: Types.ObjectId;
}
