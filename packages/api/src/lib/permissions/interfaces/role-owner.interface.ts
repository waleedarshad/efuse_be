import { OwnerType } from "@efuse/entities";
import { Document, Types } from "mongoose";

// I would love to make this more generic, but right now we're simply too tied to MongoDB for all
// of our objects.
export interface IRoleOwner extends Document {
  owner: Types.ObjectId;
  ownerKind: OwnerType;
  role: Types.ObjectId;
}
