import { Document, Types } from "mongoose";
import { ResourceKindEnum } from "../enums";

// I would love to make this more generic, but right now we're simply too tied to MongoDB for all
// of our objects.
export interface IResource extends Document {
  kind: ResourceKindEnum;
  name: string;
  value: Types.ObjectId | string;
}
