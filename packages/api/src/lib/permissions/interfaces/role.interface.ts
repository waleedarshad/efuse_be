import { Document } from "mongoose";

// I would love to make this more generic, but right now we're simply too tied to MongoDB for all
// of our objects.
export interface IRole extends Document {
  name: string;
}
