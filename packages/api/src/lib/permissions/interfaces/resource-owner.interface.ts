import { Document, Types } from "mongoose";
import { ResourceOwnerKindEnum } from "../enums";

// I would love to make this more generic, but right now we're simply too tied to MongoDB for all
// of our objects.
export interface IResourceOwner extends Document {
  owner: Types.ObjectId;
  ownerKind: ResourceOwnerKindEnum;
  resource: Types.ObjectId;
}
