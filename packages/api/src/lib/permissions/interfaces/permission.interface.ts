import { Document, Types } from "mongoose";
import { PermissionActionsEnum, PermissionGranteeKindEnum } from "../enums";

// I would love to make this more generic, but right now we're simply too tied to MongoDB for all
// of our objects.
export interface IPermission extends Document {
  action: PermissionActionsEnum;
  grantee: Types.ObjectId;
  granteeKind: PermissionGranteeKindEnum;
  resource: Types.ObjectId;
}
