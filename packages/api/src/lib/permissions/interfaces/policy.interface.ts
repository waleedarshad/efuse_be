import { EntityRoleEntityKindEnum, PermissionActionsEnum, ResourceOwnerKindEnum } from "../enums";
import { PermissionResourceValueType } from "../types/permission-resource-value.type";
import { ObjectId } from "../../../backend/types";

export interface IPolicy {
  entityId: ObjectId;
  entityKind: EntityRoleEntityKindEnum;
  action: PermissionActionsEnum;
  resource: PermissionResourceValueType;
  resourceOwner?: ObjectId;
  resourceOwnerKind?: ResourceOwnerKindEnum;
}
