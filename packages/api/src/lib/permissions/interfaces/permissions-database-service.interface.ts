import { ObjectId } from "../../../backend/types";
import { EntityRoleEntityKindEnum, PermissionActionsEnum } from "../enums";
import { IRole, IOptionalCreateRoleArgs, IEntityRole, IPermission, IResourceOwner, IResource, IRoleOwner } from ".";
import { RoleValue } from "../types/role-value.type";

// TODO: I think this still has a few assumptions about the underlying system. For instance, IDs
// should probably be only strings because that's how Casbin would work if it was the adapter.
export interface IPermissionsDatabaseService {
  assignRoleByRoleValue(
    roleValue: RoleValue,
    entityId: ObjectId,
    entityKind: EntityRoleEntityKindEnum
  ): Promise<IEntityRole>;
  assignRole(roleId: ObjectId, entityId: ObjectId, entityKind: EntityRoleEntityKindEnum): Promise<IEntityRole>;
  createRole(newRole: IRole, optionalCreateRoleArgs?: IOptionalCreateRoleArgs): Promise<IRole>;
  deleteRole(roleId: ObjectId, roleToMoveEntitiesTo?: ObjectId): Promise<boolean>;
  deleteManyRoles(roleIds: ObjectId[], roleToMoveEntitiesTo?: ObjectId): Promise<boolean>;
  deleteManyRoles(
    filter: Partial<IRole>,
    owner?: Partial<IRoleOwner>,
    roleToMoveEntitiesTo?: ObjectId
  ): Promise<boolean>;
  findOneRole(id: ObjectId): Promise<IRole | null>;
  findOneRole(filter: Partial<IRole>): Promise<IRole | null>;
  findRoles(ids: ObjectId[]): Promise<IRole[]>;
  findRoles(filter: Partial<IRole>): Promise<IRole[]>;
  findRolesByOwner(ownerId: ObjectId): Promise<IRole[]>;
  findRolesByRoleValues(roleValues: RoleValue[]): Promise<IRole[]>;
  hasRolesByRoleValues(entityId: ObjectId, roleValues: RoleValue[]): Promise<boolean>;
  findRolesForEntity(entityId: ObjectId, entityKind?: EntityRoleEntityKindEnum): Promise<IRole[]>;
  hasRole(entityId: ObjectId, roleId: ObjectId): Promise<boolean>;
  hasRoles(entityId: ObjectId, roleIds: ObjectId[]): Promise<boolean>;
  updateRole(roleId: ObjectId, body: Partial<IRole>): Promise<IRole>;
  unassignRole(roleId: ObjectId, entityId: ObjectId): Promise<boolean>;

  createPermission(permission: IPermission): Promise<IPermission>;
  deletePermission(id: ObjectId): Promise<boolean>;
  deleteManyPermissions(ids: ObjectId[]): Promise<boolean>;
  deleteManyPermissions(filter: Partial<IPermission>): Promise<boolean>;
  findOnePermission(id: ObjectId): Promise<IPermission | null>;
  findOnePermission(filter: Partial<IPermission>): Promise<IPermission | null>;
  findPermissions(ids: ObjectId[]): Promise<IPermission[]>;
  findPermissions(filter: Partial<IPermission>): Promise<IPermission[]>;
  roleHasPermission(roleId: ObjectId, resourceValue: string, action: PermissionActionsEnum): Promise<boolean>;
  entityHasPermission(
    entityId: ObjectId,
    entityKind: EntityRoleEntityKindEnum,
    resourceValue: string,
    action: PermissionActionsEnum,
    owner?: Partial<IResourceOwner>
  ): Promise<boolean>;
  updatePermission(id: ObjectId, body: Partial<IPermission>): Promise<IPermission>;

  createResource(resource: IResource, owner?: Omit<IResourceOwner, "resource">): Promise<IResource>;
  deleteResource(id: ObjectId): Promise<boolean>;
  deleteManyResources(ids: ObjectId[]): Promise<boolean>;
  deleteManyResources(filter: Partial<IResource>, owner?: Partial<IResourceOwner>): Promise<boolean>;
  findOneResource(id: ObjectId): Promise<IResource | null>;
  findOneResource(filter: Partial<IResource>): Promise<IResource | null>;
  findResources(ids: ObjectId[]): Promise<IResource[]>;
  findResources(filter: Partial<IResource>, owner?: Partial<IResourceOwner>): Promise<IResource[]>;
  findResources(ids: ObjectId[]): Promise<IResource[]>;
  updateResource(id: ObjectId, body: Partial<IResource>): Promise<IResource>;
}
