import { LeanDocument, PaginateResult, Types } from "mongoose";
import { OwnerKind } from "@efuse/entities";
import { uniq } from "lodash";
import { Failure } from "@efuse/contracts";

import {
  LeagueOfLegendsStats,
  LeagueOfLegendsLeaderboard,
  LeagueOfLegendsLeaderboardHistory
} from "../../backend/models/league-of-legends";
import {
  IOverwatchLeaderboard,
  IValorantLeaderboard,
  IAimlabLeaderboard,
  ILeagueOfLegendsLeaderboard,
  IUser,
  IValorantLeaderboardDeprecated,
  IAimlabLeaderboardDeprecated,
  ILeagueOfLegendsLeaderboardDeprecated,
  ILeaderboardGame,
  IGame
} from "../../backend/interfaces";
import { SegmentType } from "../overwatch/overwatch.enum";
import { Service, Trace } from "../decorators";
import { ConstantsService } from "../constants";
import { PipelineLeaderboardBaseService } from "./pipeline-leaderboard-base.service";
import { RiotProfileService } from "../riot/riot-profile/riot-profile.service";
import { RecruitmentProfile, RecruitmentProfileModel } from "../../backend/models/recruitment-profile";
import { IRecruitmentProfile } from "../../backend/interfaces/recruitment-profile";
import { ValorantLeaderboard } from "../../backend/models/valorant/valorant-leaderboard";
import { ValorantStats } from "../../backend/models/valorant/valorant-stats";
import { AimlabLeaderboard, AimlabLeaderboardHistory, AimlabProfile } from "../../backend/models/aimlab";
import { User } from "../../backend/models/User";
import { PipelineLeaderboardType } from "./pipeline-leaderboard-type.enum";
import { PIPELINE_LEADERBOARD } from "../metrics";
import { ILeaderboardBase } from "../../backend/interfaces/leaderboard/leaderboard-base";
import { LeagueOfLegendsQueueType } from "../leagueOfLegends/league-of-legends.enum";
import { ValorantLeaderboardHistory } from "../../backend/models/valorant/valorant-leaderboard-history";
import { GameBaseService } from "../game/game-base.service";

// TODO: possibly move enums and interfaces to a package folder
interface IConstant {
  lolSpots: number;
  overwatchSpots: number;
  valorantSpots: number;
  aimlabSpots: number;
}

enum ConstantFeatureEnum {
  leaderboard = "LEADERBOARD"
}

type AllowedLeaderboardTypes = IAimlabLeaderboard[] | ILeagueOfLegendsLeaderboard[] | IValorantLeaderboard[];

@Service("pipeline-leaderboard.service")
export class PipelineLeaderboardService extends PipelineLeaderboardBaseService {
  protected readonly recruitmentProfileModel: RecruitmentProfileModel<IRecruitmentProfile> = RecruitmentProfile;
  private $constantsService: ConstantsService;
  private $riotProfileService: RiotProfileService;

  constructor() {
    super();

    this.$constantsService = new ConstantsService();
    this.$riotProfileService = new RiotProfileService();
  }

  /**
   *
   * @summary Generate and save Aimlab Leaderboard
   *
   * @returns {Promise<IAimlabLeaderboard[]>}
   */
  @Trace({ name: "GenerateAndSaveAimlabLeaderboard", service: "pipeline-leaderboard.service" })
  public static async GenerateAndSaveAimlabLeaderboard(): Promise<IAimlabLeaderboard[]> {
    const leaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();
    leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_AIMLAB_PIPELINE_LEADERBOARD.START);

    try {
      const aimlabLeaderboard = <IAimlabLeaderboard[]>(
        await leaderboardService.generateSpecifiedLeaderboard(PipelineLeaderboardType.AIM_LAB)
      );

      // this clears the entire collection
      await AimlabLeaderboard.deleteMany({});

      // check to be sure there is something to save. This is to take into account the possibility
      // there is no one on the pipeline leaderboard. That most likely will never happen,
      // but just in case.
      if (aimlabLeaderboard && aimlabLeaderboard.length > 0) {
        await AimlabLeaderboard.insertMany(aimlabLeaderboard);
      }

      const foundLeaderboard = await AimlabLeaderboard.find({});
      leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_AIMLAB_PIPELINE_LEADERBOARD.FINISH);

      return foundLeaderboard;
    } catch (error) {
      leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_AIMLAB_PIPELINE_LEADERBOARD.ERROR);
      throw leaderboardService.handleError(
        Failure.InternalServerError("Failed to generate and save Aimlab Leaderboard!", error)
      );
    }
  }

  /**
   *
   * @summary Generate and save League of legends Leaderboard
   *
   * @returns {Promise<IILeagueOfLegendsLeaderboard[]>}
   */
  @Trace({ name: "GenerateAndSaveLeagueOfLegendsLeaderboard", service: "pipeline-leaderboard.service" })
  public static async GenerateAndSaveLeagueOfLegendsLeaderboard(): Promise<ILeagueOfLegendsLeaderboard[]> {
    const leaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();
    leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_LEAGUE_OF_LEGENDS_PIPELINE_LEADERBOARD.START);

    try {
      const leagueLeaderboard = <ILeagueOfLegendsLeaderboard[]>(
        await leaderboardService.generateSpecifiedLeaderboard(PipelineLeaderboardType.LEAGUE_OF_LEGENDS)
      );

      // this clears the entire collection
      await LeagueOfLegendsLeaderboard.deleteMany({});

      // check to be sure there is something to save. This is to take into account the possibility
      // there is no one on the pipeline leaderboard. That most likely will never happen,
      // but just in case.
      if (leagueLeaderboard && leagueLeaderboard.length > 0) {
        await LeagueOfLegendsLeaderboard.insertMany(leagueLeaderboard);
      }

      const foundLeaderboard = await LeagueOfLegendsLeaderboard.find({});
      leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_LEAGUE_OF_LEGENDS_PIPELINE_LEADERBOARD.FINISH);

      return foundLeaderboard;
    } catch (error) {
      leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_LEAGUE_OF_LEGENDS_PIPELINE_LEADERBOARD.ERROR);
      throw leaderboardService.handleError(
        Failure.InternalServerError("Failed to generate and save Aimlab Leaderboard!", error)
      );
    }
  }

  /**
   *
   * @summary Generate and save Valorant Leaderboard
   *
   * @returns {Promise<IValorantLeaderboard[]>}
   */
  @Trace({ name: "GenerateAndSaveValorantLeaderboard", service: "pipeline-leaderboard.service" })
  public static async GenerateAndSaveValorantLeaderboard(): Promise<IValorantLeaderboard[]> {
    const leaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();
    leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_VALORANT_PIPELINE_LEADERBOARD.START);

    try {
      const riotProfiles = await leaderboardService.$riotProfileService.find(
        { ownerKind: OwnerKind.USER },
        { _id: 1, owner: 1 },
        { lean: 1 }
      );

      const userIdsWithRiotProfile = uniq(riotProfiles.map((rp) => String(rp.owner)));
      const userIds: Types.ObjectId[] = userIdsWithRiotProfile.map((id) => new Types.ObjectId(id));

      const generatedLeaderboard: IValorantLeaderboard[] = <IValorantLeaderboard[]>(
        await leaderboardService.generateSpecifiedLeaderboard(PipelineLeaderboardType.VALORANT, userIds)
      );

      // this clears the entire collection
      await ValorantLeaderboard.deleteMany({});

      // check to be sure there is something to save. This is to take into account the possibility
      // there is no one on the valorant pipeline leaderboard. That most likely will never happen,
      // but just in case.
      if (generatedLeaderboard && generatedLeaderboard.length > 0) {
        await ValorantLeaderboard.insertMany(generatedLeaderboard);
      }

      const foundLeaderboard = await ValorantLeaderboard.find({});
      leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_VALORANT_PIPELINE_LEADERBOARD.FINISH);

      return foundLeaderboard;
    } catch (error) {
      leaderboardService.recordMetric(PIPELINE_LEADERBOARD.GENERATE_VALORANT_PIPELINE_LEADERBOARD.ERROR);
      throw leaderboardService.handleError(
        Failure.InternalServerError("Failed to generate and save Aimlab Leaderboard!", error)
      );
    }
  }

  /**
   * Gets a paginated leaderboard for the selected game.
   * @param gameType
   * @param page
   * @param limit
   * @returns
   */
  @Trace()
  public async getPaginatedLeaderboard(
    gameType: PipelineLeaderboardType,
    page: number,
    limit: number
  ): Promise<PaginateResult<ILeaderboardBase>> {
    // TODO: Modify this to be automatically handled based on the PipelineLeaderboardType enum so we
    // don't have to update this function every time we support a new game.
    switch (gameType) {
      case PipelineLeaderboardType.AIM_LAB: {
        const aimlab = await this.getPaginatedAimlabLeaderboard(page, limit);
        return aimlab;
      }
      case PipelineLeaderboardType.LEAGUE_OF_LEGENDS: {
        const leagueOfLegends = await this.getPaginatedLOLLeaderboard(page, limit);
        return leagueOfLegends;
      }
      case PipelineLeaderboardType.VALORANT: {
        const valorant = await this.getPaginatedValorantLeaderboard(page, limit);
        return valorant;
      }
      default:
        throw this.handleError(Failure.UnprocessableEntity("The requested game leaderboard is not supported."));
    }
  }

  /**
   * Gets a complete LeaderboardGame object for the selected game.
   * @param gameType
   * @param page
   * @param limit
   * @returns
   */
  @Trace()
  public async getLeaderboardGame(
    gameType: PipelineLeaderboardType,
    page: number,
    limit: number
  ): Promise<ILeaderboardGame> {
    const gameService = new GameBaseService();
    const game = await gameService.findByTitle(gameService.convertTypeToTitle(gameType));
    const leaderboard = await this.getPaginatedLeaderboard(gameType, page, limit);

    if (!game) {
      throw this.handleError(Failure.UnprocessableEntity("The requested game was not found."));
    }

    return {
      game: <IGame>game,
      leaderboard: leaderboard
    };
  }

  /**
   * @summary Get Aimlab Leaderboard
   *
   * @returns {Promise<IAimlabLeaderboard>}
   */
  @Trace()
  public async getAimlabLeaderboardByUser(userId: Types.ObjectId): Promise<IAimlabLeaderboard | null> {
    this.logger?.info(userId);
    const leaderboard = await AimlabLeaderboard.findOne({ userId });

    return leaderboard;
  }

  /**
   * @summary Get Aimlab Leaderboard
   *
   * @returns {Promise<IAimlabLeaderboard[]>}
   */
  @Trace()
  public async getPaginatedAimlabLeaderboard(page: number, limit: number): Promise<PaginateResult<IAimlabLeaderboard>> {
    // Make sure page param is positive
    let pageParam = page;
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (limitParam < 1) {
      limitParam = 1;
    }

    const leaderboard = await AimlabLeaderboard.paginate(
      {},
      {
        page: pageParam,
        limit: limitParam,
        sort: { rank: 1 },
        lean: true
      }
    );

    return leaderboard;
  }

  /**
   * @deprecated
   * @summary Get Aimlab Leaderboard
   *
   * TODO: Remove when frontend updated
   *
   * @returns {Promise<IAimlabLeaderboardDeprecated[]>}
   */
  @Trace()
  public async getAimlabLeaderboardDeprecated(): Promise<IAimlabLeaderboardDeprecated[]> {
    const variables: IConstant = await this.getLeaderboardConstants();

    const aggregations = [
      {
        $lookup: {
          from: "aimlabstats",
          localField: "user._id",
          foreignField: "owner",
          as: "stats"
        }
      },
      { $match: { stats: { $ne: [] } } },
      { $unwind: { path: "$stats", preserveNullAndEmptyArrays: false } },
      { $match: { "stats.ranking.skill": { $gt: 0 } } },
      { $sort: { "stats.ranking.skill": -1 } },
      { $limit: variables.valorantSpots },
      { $addFields: { "stats.kind": "AimlabStats" } }
    ];

    const leaderboard: IAimlabLeaderboardDeprecated[] = await this.buildAggregate<IAimlabLeaderboardDeprecated>(
      aggregations,
      {}
    );

    return leaderboard;
  }

  /**
   * @summary Get Aimlab Leaderboard
   *
   * @returns {Promise<ILeagueOfLegendsLeaderboard>}
   */
  @Trace()
  public async getLOLLeaderboardByUser(userId: Types.ObjectId): Promise<ILeagueOfLegendsLeaderboard | null> {
    const leaderboard = await LeagueOfLegendsLeaderboard.findOne({ userId });

    return leaderboard;
  }

  /**
   * @summary Get Leaderboard for LOL
   *
   * @returns {Promise<ILeagueOfLegendsLeaderboard[]>}
   */
  @Trace()
  public async getPaginatedLOLLeaderboard(
    page: number,
    limit: number
  ): Promise<PaginateResult<ILeagueOfLegendsLeaderboard>> {
    // Make sure page param is positive
    let pageParam = page;
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (limitParam < 1) {
      limitParam = 1;
    }

    const leaderboard = await LeagueOfLegendsLeaderboard.paginate(
      {},
      {
        page: pageParam,
        limit: limitParam,
        sort: { rank: 1 },
        lean: true
      }
    );

    return leaderboard;
  }

  /**
   * @deprecated
   * @summary Get Leaderboard for LOL
   *
   * TODO: Remove when frontend updated
   *
   * @returns {Promise<ILeagueOfLegendsLeaderboardDeprecated[]>}
   */
  @Trace()
  public async getLOLLeaderboardDeprecated(): Promise<ILeagueOfLegendsLeaderboardDeprecated[]> {
    const variables: IConstant = await this.getLeaderboardConstants();

    const aggregations = [
      { $match: { "user.leagueOfLegends.stats": { $ne: null } } },
      {
        $lookup: {
          from: "leagueoflegendsstats",
          localField: "user.leagueOfLegends.stats",
          foreignField: "_id",
          as: "leagueOfLegendsStats"
        }
      },
      { $unwind: { path: "$leagueOfLegendsStats", preserveNullAndEmptyArrays: false } },
      {
        $match: { "leagueOfLegendsStats.leaderboardRankTierScore": { $gt: 0 } }
      },
      { $sort: { "leagueOfLegendsStats.leaderboardRankTierScore": -1 } },
      { $limit: variables.lolSpots },
      { $unwind: { path: "$leagueOfLegendsStats.leagueInfo", preserveNullAndEmptyArrays: false } },
      { $match: { "leagueOfLegendsStats.leagueInfo.queueType": LeagueOfLegendsQueueType.rankedSolo5By5 } }
    ];

    const leaderboard: ILeagueOfLegendsLeaderboardDeprecated[] =
      await this.buildAggregate<ILeagueOfLegendsLeaderboardDeprecated>(aggregations, { leagueOfLegendsStats: 1 });

    return leaderboard;
  }

  /**
   * @summary Get Leaderboard for Overwatch
   *
   * @returns {Promise{IOverwatchLeaderboard[]}}
   */
  @Trace()
  public async getOverwatchLeaderboard(): Promise<IOverwatchLeaderboard[]> {
    const variables: IConstant = await this.getLeaderboardConstants();

    const aggregations = [
      { $match: { "user.overwatch.stats": { $ne: null } } },
      {
        $lookup: {
          from: "overwatchstats",
          localField: "user.overwatch.stats",
          foreignField: "_id",
          as: "overwatchStats"
        }
      },
      { $unwind: { path: "$overwatchStats", preserveNullAndEmptyArrays: false } },
      { $match: { "overwatchStats.leaderboardScore": { $gte: 0 } } },
      { $sort: { "overwatchStats.leaderboardScore": -1 } },
      { $limit: variables.overwatchSpots },
      { $unwind: { path: "$overwatchStats.segments", preserveNullAndEmptyArrays: false } },
      { $match: { "overwatchStats.segments.realm": SegmentType.competitive } }
    ];

    const leaderboard: IOverwatchLeaderboard[] = await this.buildAggregate<IOverwatchLeaderboard>(aggregations, {
      overwatchStats: 1
    });

    return leaderboard;
  }

  /**
   * @summary Get Aimlab Leaderboard
   *
   * @returns {Promise<IValorantLeaderboard>}
   */
  @Trace()
  public async getValorantLeaderboardByUser(userId: Types.ObjectId): Promise<IValorantLeaderboard | null> {
    const leaderboard = await ValorantLeaderboard.findOne({ userId });

    return leaderboard;
  }

  /**
   * @summary Get Valorant Leaderboard
   *
   * @returns {Promise<IValorantLeaderboard>}
   */
  @Trace()
  public async getPaginatedValorantLeaderboard(
    page: number,
    limit: number
  ): Promise<PaginateResult<IValorantLeaderboard>> {
    // Make sure page param is positive
    let pageParam = page;
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (limitParam < 1) {
      limitParam = 1;
    }

    const leaderboard = await ValorantLeaderboard.paginate(
      {},
      {
        page: pageParam,
        limit: limitParam,
        sort: { rank: 1 },
        lean: true
      }
    );

    return leaderboard;
  }

  /**
   * @deprecated
   *
   * @summary Get Valorant Leaderboard
   *
   * TODO: Remove when frontend updated
   *
   * @returns {Promise<IValorantLeaderboardDeprecated>}
   */
  @Trace()
  public async getValorantLeaderboardDeprecated(): Promise<IValorantLeaderboardDeprecated[]> {
    const variables: IConstant = await this.getLeaderboardConstants();

    const riotProfiles = await this.$riotProfileService.find(
      { ownerKind: OwnerKind.USER },
      { _id: 1, owner: 1 },
      { lean: 1 }
    );

    const userIdsHavingRiotProfile: string[] = uniq(riotProfiles.map((rp) => String(rp.owner)));

    const userIds: Types.ObjectId[] = userIdsHavingRiotProfile.map((id) => new Types.ObjectId(id));

    const aggregations = [
      { $match: { "user._id": { $in: userIds } } },
      {
        $lookup: {
          from: "valorantstats",
          localField: "user._id",
          foreignField: "user",
          as: "stats"
        }
      },
      { $unwind: { path: "$stats", preserveNullAndEmptyArrays: false } },
      { $sort: { "stats.competitiveTier": -1, "stats.rankedRating": -1 } },
      { $limit: variables.valorantSpots },
      { $addFields: { "stats.kind": "ValorantUserStats" } }
    ];

    const leaderboard: IValorantLeaderboardDeprecated[] = await this.buildAggregate<IValorantLeaderboardDeprecated>(
      aggregations,
      {}
    );

    return leaderboard;
  }

  /**
   *
   * @summary Update Aimlab Leaderboard history
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateAimlabLeaderboardHistory(): Promise<void> {
    try {
      // get the current leaderboard
      const aimlabLeaderboard = await AimlabLeaderboard.find({}).lean();
      // check to be sure there is something to save. This is to take into account the possibility
      // there is no one on the pipeline leaderboard. That most likely will never happen,
      // but just in case.
      if (aimlabLeaderboard && aimlabLeaderboard.length > 0) {
        await AimlabLeaderboardHistory.insertMany(aimlabLeaderboard);
      }
    } catch (error) {
      throw Failure.InternalServerError("Failed to update Aimlab Leaderboard history!", error);
    }
  }

  /**
   *
   * @summary Update League Of Legends Leaderboard history
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateLeagueOfLegendsLeaderboardHistory(): Promise<void> {
    try {
      // get the current leaderboard
      const leagueLeaderboard = await LeagueOfLegendsLeaderboard.find({}).lean();
      // check to be sure there is something to save. This is to take into account the possibility
      // there is no one on the pipeline leaderboard. That most likely will never happen,
      // but just in case.
      if (leagueLeaderboard && leagueLeaderboard.length > 0) {
        await LeagueOfLegendsLeaderboardHistory.insertMany(leagueLeaderboard);
      }
    } catch (error) {
      throw Failure.InternalServerError("Failed to update League of Legends Leaderboard history!", error);
    }
  }

  /**
   *
   * @summary Update Valorant Leaderboard history
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateValorantLeaderboardHistory(): Promise<void> {
    try {
      // get the current leaderboard
      const valorantLeaderboard = await ValorantLeaderboard.find({}).lean();
      // check to be sure there is something to save. This is to take into account the possibility
      // there is no one on the pipeline leaderboard. That most likely will never happen,
      // but just in case.
      if (valorantLeaderboard && valorantLeaderboard.length > 0) {
        await ValorantLeaderboardHistory.insertMany(valorantLeaderboard);
      }
    } catch (error) {
      throw Failure.InternalServerError("Failed to update Valorant Leaderboard!", error);
    }
  }

  /**
   * Add the profile recruitment id to the generated pipeline leaderboards
   * Currently supports Aimlab, League of Legends and Valorant
   *
   * This is slightly abstracted but the leaderboards share many of the same variables including userId and recruitmentProfileId
   * They will use this to add the profile recruitment id
   *
   * @param {AllowedLeaderboardTypes} leaderboard
   * @param recruitmentProfiles
   * @returns
   */
  @Trace()
  private addProfileRecruimentIdToLeaderboard(
    leaderboard: AllowedLeaderboardTypes,
    recruitmentProfiles: LeanDocument<IRecruitmentProfile>[]
  ): AllowedLeaderboardTypes {
    if (leaderboard && leaderboard.length > 0) {
      leaderboard.forEach((leader: ILeaderboardBase) => {
        // this will be fine because all the leaderboards will have this variable in common
        const foundProfile = recruitmentProfiles.find((profile) =>
          leader.userId?.equals(<Types.ObjectId>profile?.user)
        );

        if (foundProfile) {
          // this is another variable that is shared between all leaderboards
          leader.recruitmentProfileId = <Types.ObjectId>foundProfile._id;
        } else {
          throw Failure.NotFound("A profile could not be found for the Leaderboard entry!");
        }
      });
    }

    return leaderboard;
  }

  /**
   * This is needed because the lol stats collection does not contain a user id, so it cannot be done in
   * aggregate without a lookup
   *
   * @returns ILeagueOfLegendsLeaderboard[]
   */
  @Trace()
  private addUserIdsToLeagueOfLegendsLeaderboard(
    leagueLeaderboard: ILeagueOfLegendsLeaderboard[],
    users: IUser[]
  ): ILeagueOfLegendsLeaderboard[] {
    if (leagueLeaderboard && leagueLeaderboard.length > 0) {
      leagueLeaderboard.forEach((leader) => {
        const foundUser = users.find((user) => user?.leagueOfLegends?.stats?.equals(leader?.leagueOfLegendsStatsId));

        // if a user is not found then there is an issue
        if (foundUser) {
          leader.userId = foundUser._id;
        } else {
          throw Failure.NotFound("A user could not be found for the League of Legends Leaderboard entry!");
        }
      });
    }

    return leagueLeaderboard;
  }

  /**
   * Basically this generates the aimlab leaderboard. The only things missing are the recruitment profile ids.
   * They could be done in the aggregation with a lookup, but it would be very slow
   *
   * @param userIds
   * @returns Promise<IAimlabLeaderboard[]>
   */
  @Trace()
  private aggregateForAimlabLeaderboard(userIds: Types.ObjectId[]): Promise<IAimlabLeaderboard[]> {
    return AimlabProfile.aggregate<IAimlabLeaderboard[]>([
      // find all users with matching ids
      { $match: { owner: { $in: userIds } } },
      // sort by the persons ranking skill
      { $sort: { "ranking.skill": -1 } },
      // this pretty much creates the aimlableaderboard collection
      {
        $group: {
          _id: false,
          aimlableaderboard: {
            $push: {
              userId: "$owner",
              aimlabStatsId: "$_id"
            }
          }
        }
      },
      {
        // one of the main reasons for this is that the array index can act as the rank if incremented by 1
        $unwind: {
          path: "$aimlableaderboard",
          includeArrayIndex: "rank"
        }
      },
      // this adjusts the output to be what we want
      {
        $project: {
          _id: 0,
          rank: { $add: ["$rank", 1] }, // increment rank by 1 since it starts at 0
          userId: "$aimlableaderboard.userId",
          aimlabStatsId: "$aimlableaderboard.aimlabStatsId",
          recruitmentProfileId: ""
        }
      }
    ]);
  }

  /**
   * This generates most of the LOL leaderboard. The only things missing are the recruitment profile ids and user ids.
   * They could be done in the aggregation with a lookup, but it would be very slow
   *
   * @param userIds
   * @returns Promise<IValorantLeaderboard[]>
   */
  @Trace()
  private aggregateForLeagueOfLegendsLeaderboard(statIds: Types.ObjectId[]): Promise<ILeagueOfLegendsLeaderboard[]> {
    return LeagueOfLegendsStats.aggregate<ILeagueOfLegendsLeaderboard[]>([
      // find all lol users with matching ids and only find stats with a rank tier score
      { $match: { _id: { $in: statIds }, leaderboardRankTierScore: { $gt: 0 } } },
      // unwind to make it easier to filter info
      {
        $unwind: {
          path: "$leagueInfo",
          preserveNullAndEmptyArrays: false
        }
      },
      // find all people in ranked solo
      { $match: { "leagueInfo.queueType": "RANKED_SOLO_5x5" } },
      // sort by the persons rank score
      { $sort: { leaderboardRankTierScore: -1 } },
      // this pretty much creates the lol leaderboard collection, it'll just need tweaks
      {
        $group: {
          _id: false,
          lolleaderboard: {
            $push: {
              leagueOfLegendsStatsId: "$_id"
            }
          }
        }
      },
      {
        // one of the main reasons for this is that the array index can act as the rank if incremented by 1
        $unwind: {
          path: "$lolleaderboard",
          includeArrayIndex: "rank"
        }
      },
      // this adjusts the output to be what we want
      {
        $project: {
          _id: 0,
          rank: { $add: ["$rank", 1] }, // increment rank by 1 since it starts at 0
          userId: "",
          leagueOfLegendsStatsId: "$lolleaderboard.leagueOfLegendsStatsId",
          recruitmentProfileId: ""
        }
      }
    ]);
  }

  /**
   * Basically this generates the valorant leaderboard. The only things missing are the recruitment profile ids.
   * They could be done in the aggregation with a lookup, but it would be very slow
   *
   * @param userIds
   * @returns Promise<IValorantLeaderboard[]>
   */
  @Trace()
  private aggregateForValorantLeaderboard(userIds: Types.ObjectId[]): Promise<IValorantLeaderboard[]> {
    return ValorantStats.aggregate<IValorantLeaderboard[]>([
      // find all users with matching ids
      { $match: { user: { $in: userIds } } },
      // sort by the persons tier and then by their rating. Note: higher numbers are better
      { $sort: { competitiveTier: -1, rankedRating: -1 } },
      // this pretty much creates the valorantleaderboard collection. it just needs some tweaks
      {
        $group: {
          _id: false,
          valorantleaderboard: {
            $push: {
              userId: "$user",
              valorantStatsId: "$_id"
            }
          }
        }
      },
      {
        // one of the main reasons for this is that the array index can act as the rank if incremented by 1
        $unwind: {
          path: "$valorantleaderboard",
          includeArrayIndex: "rank"
        }
      },
      // this adjusts the output to be what we want
      {
        $project: {
          _id: 0,
          rank: { $add: ["$rank", 1] }, // increment rank by 1 since it starts at 0
          userId: "$valorantleaderboard.userId",
          valorantStatsId: "$valorantleaderboard.valorantStatsId",
          recruitmentProfileId: ""
        }
      }
    ]);
  }

  @Trace()
  private chooseAndGenerateSpecifiedLeaderboard(
    leaderboardType: PipelineLeaderboardType,
    userIds: Types.ObjectId[]
  ): Promise<AllowedLeaderboardTypes> {
    switch (leaderboardType) {
      case PipelineLeaderboardType.AIM_LAB:
        return this.aggregateForAimlabLeaderboard(userIds);
      // LOL needs some more adjustments compared to aimlab or valorant, mainly because user id is not part of the stats collection
      case PipelineLeaderboardType.LEAGUE_OF_LEGENDS:
        return this.getStatIdsAndAggregateLeagueOfLegendsLeaderboard(userIds);
      case PipelineLeaderboardType.VALORANT:
        return this.aggregateForValorantLeaderboard(userIds);
      default:
        throw Failure.ExpectationFailed("A leaderboard could not be determined to generate and save!");
    }
  }

  @Trace()
  private createRecruitmentProfileQuery(userIds?: Types.ObjectId[]): {} {
    const graduationClassesToBeFetched: string[] = this.getGraduationClassesToBeFetched();

    const graduationClassQuery = { $in: graduationClassesToBeFetched };
    const userQuery = { $in: userIds };
    const finalizedQuery =
      userIds && userIds.length > 0
        ? { user: userQuery, graduationClass: graduationClassQuery, profileProgress: 100 }
        : { graduationClass: graduationClassQuery, profileProgress: 100 };

    return finalizedQuery;
  }

  /**
   * This takes the steps to generate the specified leaderboard and returns them
   *
   * @param leaderboardType
   * @param userIds
   * @returns
   */
  @Trace()
  private async generateSpecifiedLeaderboard(
    leaderboardType: PipelineLeaderboardType,
    userIds?: Types.ObjectId[]
  ): Promise<AllowedLeaderboardTypes> {
    const profileQuery = this.createRecruitmentProfileQuery(userIds);

    const foundProfileUsers = await this.recruitmentProfileModel.find(profileQuery).select({ _id: 1, user: 1 }).lean();
    const userIdsFromProfiles = <Types.ObjectId[]>foundProfileUsers.map((profile) => profile.user);

    const adjustedUserIds = await this.removeDeletedUsersFromLeaderboardGeneration(userIdsFromProfiles);

    // return empty array if no ids available
    if (!adjustedUserIds || adjustedUserIds.length === 0) {
      return [];
    }

    const generatedLeaderboard: AllowedLeaderboardTypes = await this.chooseAndGenerateSpecifiedLeaderboard(
      leaderboardType,
      adjustedUserIds
    );

    return this.addProfileRecruimentIdToLeaderboard(generatedLeaderboard, foundProfileUsers);
  }

  /**
   * @summary Get Leaderboard constants
   *
   * @returns {Promise<IConstant>}
   */
  @Trace()
  private async getLeaderboardConstants(): Promise<IConstant> {
    const variables: IConstant = <IConstant>await this.$constantsService.getVariables(ConstantFeatureEnum.leaderboard);
    return variables;
  }

  /**
   * This will start the league of legends leaderboard generation. The only thing missing will be the recruitment profile ids.
   * They could be done in the aggregation with a lookup, but it would be very slow.
   *
   * Lol needs a few more tweaks compared to Aimlab or Valorant because the stats collection is slightly different.
   * Mainly that the users id is not apart of the lol stats collection.
   *
   * @param userIds
   * @returns Promise<ILeagueOfLegendsLeaderboard[]>
   */
  @Trace()
  private async getStatIdsAndAggregateLeagueOfLegendsLeaderboard(
    userIds: Types.ObjectId[]
  ): Promise<ILeagueOfLegendsLeaderboard[]> {
    try {
      // need to get stat ids from users collection
      const users: IUser[] = await User.find({ _id: { $in: userIds } }).select({ _id: 1, leagueOfLegends: 1 });
      const leagueOfLegendsStatIds: Types.ObjectId[] = [];
      users.forEach((user) => {
        // doing this instead of map because it is possible for the leagueOfLegends field to be undefined
        if (user?.leagueOfLegends?.stats) {
          leagueOfLegendsStatIds.push(user.leagueOfLegends.stats);
        }
      });

      const leagueLeaderboard: ILeagueOfLegendsLeaderboard[] = await this.aggregateForLeagueOfLegendsLeaderboard(
        leagueOfLegendsStatIds
      );
      const adjustedleagueLeaderboard: ILeagueOfLegendsLeaderboard[] = this.addUserIdsToLeagueOfLegendsLeaderboard(
        leagueLeaderboard,
        users
      );

      return adjustedleagueLeaderboard;
    } catch (error) {
      throw Failure.InternalServerError("Error occured when trying to generate LOL Leaderboard", error);
    }
  }

  /**
   * There will be issues in frontend if we get a deleted user. So its best to filter them out.
   */
  @Trace()
  private async removeDeletedUsersFromLeaderboardGeneration(userIds: Types.ObjectId[]): Promise<Types.ObjectId[]> {
    const foundAppropriateUsers = await User.find({ _id: { $in: userIds }, deleted: false })
      .select({ _id: 1 })
      .lean();
    const approprateUserIds = foundAppropriateUsers.map((user) => user._id);

    return approprateUserIds;
  }
}
