import { Types } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { IUserLeaderboard, ILeaderboard } from "../../backend/interfaces";
import { Leaderboard } from "../../backend/models/Leaderboard";

// This will be removed once we migrate data
const LEGACY_STREAKS_QUERY = {
  $or: [{ boardType: { $exists: false } }, { boardType: "streaks" }]
};

@Service("efuse-engagement-leaderboard.service")
export class EFuseEngagementLeaderboardService extends BaseService {
  /**
   * @summary user leaderboard lookup
   *
   * @return {Promise<IUserLeaderboard>}
   */
  @Trace()
  public async lookupUserLeaderboard(userId: Types.ObjectId): Promise<IUserLeaderboard> {
    const board: ILeaderboard[] = await Promise.all([
      this.deprecatedGetLeaderboardByUserId(userId),
      this.getLeaderboardByUserId(userId)
    ]);
    const leaderboard: IUserLeaderboard = {
      leaderboard: board[0],
      activeLeaderboard: board[1]
    } as IUserLeaderboard;
    return leaderboard;
  }

  /**
   * @summary Get streaks leaderboard by userId | This method is deprecated & is only added
   * added to support legacy streaks leaderboards. It will be removed once we migrate the data
   * & frontend transitions away from it.
   *
   * @param {ObjectId} userId - ID of user
   * @return {Object} Leaderboard
   */
  @Trace()
  public async deprecatedGetLeaderboardByUserId(userId: Types.ObjectId): Promise<ILeaderboard> {
    return <ILeaderboard>await Leaderboard.findOne({
      user: userId,
      isActive: true,
      ...LEGACY_STREAKS_QUERY
    });
  }

  /**
   * @summary Get active Leaderboards for a user by userId
   *
   * @param {ObjectId} userId - ID of user
   * @return {Object} Leaderboard
   */
  @Trace()
  public async getLeaderboardByUserId(userId: Types.ObjectId): Promise<ILeaderboard> {
    const leaderboards = await Leaderboard.find({ user: userId, isActive: true });

    return <ILeaderboard>leaderboards.reduce((previousValue, currentValue) => {
      const { boardType } = currentValue;
      const key = !boardType ? "streaksLeaderboard" : `${boardType}Leaderboard`;

      previousValue[key] = currentValue;

      return previousValue;
    }, {});
  }
}
