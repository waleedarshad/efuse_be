import { Queue } from "bull";
import { Failure } from "@efuse/contracts";

import { IAimlabLeaderboard, ILeagueOfLegendsLeaderboard, IValorantLeaderboard } from "../../backend/interfaces";
import * as Config from "../../async/config";
import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { PipelineLeaderboardService } from "./pipeline-leaderboard.service";
import { PipelineLeaderboardType } from "./pipeline-leaderboard-type.enum";

enum LeaderboardFunctions {
  GENERATE_AIM_LAB_LEADERBOARD = "generateAndSaveAimlabLeaderboard",
  GENERATE_LEAGUE_OF_LEGENDS_LEADERBOARD = "generateAndSaveLeagueOfLegendsLeaderboard",
  GENERATE_VALORANT_LEADERBOARD = "generateAndSaveValorantLeaderboard"
}

const PIPELINE_LEADERBOARD_GENERATION_QUEUE = "pipeline-leaderboard-generation-queue";
const PIPELINE_LEADERBOARD_QUEUE_SERVICE = "pipeline-leaderboard-queue.service";

@Service(PIPELINE_LEADERBOARD_QUEUE_SERVICE)
export class PipelineLeaderboardQueueService extends BaseService {
  private static leaderboardQueue: Map<string, Queue> = new Map<string, Queue>();

  @Trace({ name: "InitAsyncProcessors", service: PIPELINE_LEADERBOARD_QUEUE_SERVICE })
  public static InitAsyncProcessors(): void {
    const service = new PipelineLeaderboardQueueService();
    const leaderboards: string[] = service.generateLeaderboardList();

    leaderboards.forEach((board) => {
      const newQueue = PipelineLeaderboardQueueService.LeaderboardQueue(board);
      service.logger?.info(`Created Pipeline Leaderboard Queue: ${newQueue.name}`);
    });
  }

  /**
   * LeaderboardQueue allows adding new leaderboard functions for queue more easily
   */
  @Trace({ name: "LeaderboardQueue", service: PIPELINE_LEADERBOARD_QUEUE_SERVICE })
  private static LeaderboardQueue(leaderboardName: string): Queue {
    const queueName = `${PIPELINE_LEADERBOARD_GENERATION_QUEUE}:${leaderboardName.toLowerCase()}`;

    if (!PipelineLeaderboardQueueService.leaderboardQueue[queueName]) {
      // attempt to get existing queue before creating a new one

      const leaderboardQueue: Queue = Config.getQueue(queueName);

      if (leaderboardQueue) {
        PipelineLeaderboardQueueService.leaderboardQueue[queueName] = leaderboardQueue;
      } else {
        // create a new queue

        PipelineLeaderboardQueueService.leaderboardQueue[queueName] = Config.initQueue(queueName);

        PipelineLeaderboardQueueService.leaderboardQueue[queueName].process((job) => {
          switch (job.data.funcName) {
            case LeaderboardFunctions.GENERATE_AIM_LAB_LEADERBOARD:
              return PipelineLeaderboardService.GenerateAndSaveAimlabLeaderboard();
            case LeaderboardFunctions.GENERATE_LEAGUE_OF_LEGENDS_LEADERBOARD:
              return PipelineLeaderboardService.GenerateAndSaveLeagueOfLegendsLeaderboard();
            case LeaderboardFunctions.GENERATE_VALORANT_LEADERBOARD:
              return PipelineLeaderboardService.GenerateAndSaveValorantLeaderboard();
            default:
              throw Failure.InternalServerError(`No queue service function named ${<string>job.data.funcName}`);
          }
        });
      }
    }

    return <Queue>PipelineLeaderboardQueueService.leaderboardQueue[queueName];
  }

  /**
   * Queue the Aimlab pipeline leaderboard for generating and saving
   *
   * @param summonerId
   * @param region
   *
   * @returns Promise<IAimlabLeaderboard[]>
   */
  @Trace()
  public async generateAndSaveAimlabLeaderboardQueued(): Promise<IAimlabLeaderboard[]> {
    const data = {
      funcName: LeaderboardFunctions.GENERATE_AIM_LAB_LEADERBOARD
    };

    const job = await PipelineLeaderboardQueueService.LeaderboardQueue(PipelineLeaderboardType.AIM_LAB).add(data);

    return <IAimlabLeaderboard[]>await job.finished();
  }

  /**
   * Queue the League of Legends pipeline leaderboard for generating and saving
   *
   * @returns Promise<ILeagueOfLegendsLeaderboard[]>
   */
  @Trace()
  public async generateAndSaveLeagueOfLegendsLeaderboardQueued(): Promise<ILeagueOfLegendsLeaderboard[]> {
    const data = {
      funcName: LeaderboardFunctions.GENERATE_LEAGUE_OF_LEGENDS_LEADERBOARD
    };

    const job = await PipelineLeaderboardQueueService.LeaderboardQueue(PipelineLeaderboardType.LEAGUE_OF_LEGENDS).add(
      data
    );

    return <ILeagueOfLegendsLeaderboard[]>await job.finished();
  }

  /**
   * Queue the Valorant pipeline leaderboard for generating and saving
   *
   * @returns Promise<IValorantLeaderboard[]>
   */
  @Trace()
  public async generateAndSaveValorantLeaderboardQueued(): Promise<IValorantLeaderboard[]> {
    const data = {
      funcName: LeaderboardFunctions.GENERATE_VALORANT_LEADERBOARD
    };

    const job = await PipelineLeaderboardQueueService.LeaderboardQueue(PipelineLeaderboardType.VALORANT).add(data);

    return <IValorantLeaderboard[]>await job.finished();
  }

  @Trace()
  private generateLeaderboardList(): string[] {
    const keys: string[] = Object.keys(PipelineLeaderboardType);
    const leaderboardList: string[] = [];
    keys.forEach((key: string) => {
      leaderboardList.push(key);
    });

    return leaderboardList;
  }
}
