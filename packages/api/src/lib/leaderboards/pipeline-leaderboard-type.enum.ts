// TODO: Move to a package folder
export enum PipelineLeaderboardType {
  AIM_LAB = "AIM_LAB",
  LEAGUE_OF_LEGENDS = "LEAGUE_OF_LEGENDS",
  VALORANT = "VALORANT"
}
