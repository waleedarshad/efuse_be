import { Aggregate } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import WhitelistHelper from "../../backend/helpers/whitelist_helper";
import { RecruitmentProfile, RecruitmentProfileModel } from "../../backend/models/recruitment-profile";
import { IRecruitmentProfile } from "../../backend/interfaces/recruitment-profile";

// Note: In JS Date.getMonth starts from 0 e.g January is counted as 0. Here 5 corresponds to June
const MONTH_WHEN_YEAR_END = 5;
const YEAR_WINDOW = 5;

// TODO: Possibly deprecate pipeline-leaderboard-base.service once new features for pipeline are setup
@Service("pipeline-leaderboard-base.service")
export class PipelineLeaderboardBaseService extends BaseService {
  protected readonly recruitmentProfileModel: RecruitmentProfileModel<IRecruitmentProfile> = RecruitmentProfile;

  /**
   * @summary Get aggregate to build leaderboard
   *
   * @param {any[]} aggregations
   * @param {Record<string, number>} projection
   * @param {number} cacheDuration
   *
   * @returns {Aggregate<T[]>}
   */
  @Trace()
  protected buildAggregate<T>(
    aggregations: any[],
    projection: Record<string, number>,
    cacheDuration = 300
  ): Aggregate<T[], any> {
    const graduationClassesToBeFetched: string[] = this.getGraduationClassesToBeFetched();

    const aggregate = this.recruitmentProfileModel
      .aggregate<T>([
        { $match: { graduationClass: { $in: graduationClassesToBeFetched } } },
        {
          $lookup: {
            from: "users",
            localField: "user",
            foreignField: "_id",
            as: "user"
          }
        },
        { $unwind: { path: "$user", preserveNullAndEmptyArrays: false } },

        ...aggregations,
        {
          $project: {
            user: WhitelistHelper.associatedUserFields().$project,
            graduationClass: 1,
            country: 1,
            city: 1,
            state: 1,
            highSchool: 1,
            stats: 1,
            ...projection
          }
        }
      ])
      .allowDiskUse(true) // this is temporary until new pipeline leaderboard generation is completed
      .cache(cacheDuration);

    return <Aggregate<T[], any>>aggregate;
  }

  // TODO: move to pipeline-leaderboard.service.ts once leaderboard feature complete
  protected getGraduationClassesToBeFetched(): string[] {
    /**
     * Year Calculation.
     *
     * For example current year is 2021 then
     *
     * Before July 1st we will show: 2021-2025 (5 graduation years)
     * After July 1st we will show 2022-2026 (5 graduation years).
     */
    const today = new Date();
    const currentYear = today.getFullYear();
    const currentMonth = today.getMonth();

    // Incrementing year after June
    const year = currentMonth > MONTH_WHEN_YEAR_END ? currentYear + 1 : currentYear;

    // Generating Array
    return Array.from({ length: YEAR_WINDOW }, (_, i) => String(year + i));
  }
}
