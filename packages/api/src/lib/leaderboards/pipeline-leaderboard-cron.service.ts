import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { PipelineLeaderboardQueueService } from "./pipeline-leaderboard-queue.service";
import { PipelineLeaderboardService } from "./pipeline-leaderboard.service";

const PIPELINE_LEADERBOARD_CRON = "pipeline-leaderboard-cron.service";

@Service(PIPELINE_LEADERBOARD_CRON)
export class PipelineLeaderboardCronService extends BaseService {
  @Trace({ name: "GenerateAndSaveAimlabLeaderboardCron", service: PIPELINE_LEADERBOARD_CRON })
  public static async GenerateAndSaveAimlabLeaderboardCron(): Promise<void> {
    const leaderboardQueueService = new PipelineLeaderboardQueueService();
    const leaderboardCronService = new PipelineLeaderboardCronService();

    try {
      await leaderboardQueueService.generateAndSaveAimlabLeaderboardQueued();
      leaderboardCronService.logger?.info(`Successfully generated and saved Aimlab leaderboards`);
    } catch (error) {
      throw leaderboardCronService.handleError(error, "Unable to generate and save Aimlab leaderboards");
    }
  }

  @Trace({ name: "GenerateAndSaveLeagueOfLegendsLeaderboardCron", service: PIPELINE_LEADERBOARD_CRON })
  public static async GenerateAndSaveLeagueOfLegendsLeaderboardCron(): Promise<void> {
    const leaderboardQueueService = new PipelineLeaderboardQueueService();
    const leaderboardCronService = new PipelineLeaderboardCronService();

    try {
      await leaderboardQueueService.generateAndSaveLeagueOfLegendsLeaderboardQueued();
      leaderboardCronService.logger?.info(`Successfully generated and saved League of Legends leaderboards`);
    } catch (error) {
      throw leaderboardCronService.handleError(error, "Unable to generate and save League Of Legends leaderboards");
    }
  }

  @Trace({ name: "GenerateAndSaveValorantLeaderboardCron", service: PIPELINE_LEADERBOARD_CRON })
  public static async GenerateAndSaveValorantLeaderboardCron(): Promise<void> {
    const leaderboardQueueService = new PipelineLeaderboardQueueService();
    const leaderboardCronService = new PipelineLeaderboardCronService();

    try {
      await leaderboardQueueService.generateAndSaveValorantLeaderboardQueued();
      leaderboardCronService.logger?.info(`Successfully generated and saved Valorant leaderboards`);
    } catch (error) {
      throw leaderboardCronService.handleError(error, "Unable to generate and save Valorant leaderboards");
    }
  }

  @Trace({ name: "UpdateAimlabLeaderboardHistoryCron", service: PIPELINE_LEADERBOARD_CRON })
  public static async UpdateAimlabLeaderboardHistoryCron(): Promise<void> {
    const leaderboardService = new PipelineLeaderboardService();
    const leaderboardCronService = new PipelineLeaderboardCronService();

    try {
      await leaderboardService.updateAimlabLeaderboardHistory();
      leaderboardCronService.logger?.info(`Successfully updated Aimlab leaderboard history`);
    } catch (error) {
      throw leaderboardCronService.handleError(error, "Unable to update Aimlab leaderboard history");
    }
  }

  @Trace({ name: "UpdateLeagueOfLegendsLeaderboardHistoryCron", service: PIPELINE_LEADERBOARD_CRON })
  public static async UpdateLeagueOfLegendsLeaderboardHistoryCron(): Promise<void> {
    const leaderboardService = new PipelineLeaderboardService();
    const leaderboardCronService = new PipelineLeaderboardCronService();

    try {
      await leaderboardService.updateLeagueOfLegendsLeaderboardHistory();
      leaderboardCronService.logger?.info(`Successfully updated League Of Legends leaderboard history`);
    } catch (error) {
      throw leaderboardCronService.handleError(error, "Unable to update League Of Legends leaderboard history");
    }
  }

  @Trace({ name: "UpdateValorantLeaderboardHistoryCron", service: PIPELINE_LEADERBOARD_CRON })
  public static async UpdateValorantLeaderboardHistoryCron(): Promise<void> {
    const leaderboardService = new PipelineLeaderboardService();
    const leaderboardCronService = new PipelineLeaderboardCronService();

    try {
      await leaderboardService.updateValorantLeaderboardHistory();
      leaderboardCronService.logger?.info(`Successfully updated Valorant leaderboard history`);
    } catch (error) {
      throw leaderboardCronService.handleError(error, "Unable to update Valorant leaderboard history");
    }
  }
}
