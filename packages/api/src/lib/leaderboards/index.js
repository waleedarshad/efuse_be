const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/leaderboards/index.js" });
const lodash = require("lodash");
const { ObjectId } = require("mongodb");
const moment = require("moment");
const createError = require("http-errors");

const { Leaderboard } = require("../../backend/models/Leaderboard");
const { UserStats } = require("../../backend/models/user-stats");

const Config = require("../../async/config");
const { StreaksService } = require("../streaks/streaks.service");
const UserLib = require("../users");

const streaksService = new StreaksService();

// This will be removed once we migrate data
const LEGACY_STREAKS_QUERY = {
  $or: [{ boardType: { $exists: false } }, { boardType: "streaks" }]
};

const BOARD_TYPES = ["streaks", "views", "engagements"];

/**
 * @summary Get active streaks Leaderboard | This method is deprecated & it will return only
 * active streaks leaderboard. It will be removed once we migrate the data
 * & frontend transitions away from it.
 *
 * @param {Number} page - Page Number
 * @param {Number} limit - Number of records per page
 *
 * @return {Promise<any[]>} Paginated collection of leaderboard
 */
const deprecatedGetActiveLeaderboard = async (page = 1, limit = 10) => {
  try {
    const paginatedLeaderboard = await Leaderboard.paginate(
      { isActive: true, ...LEGACY_STREAKS_QUERY },
      { page, limit, sort: { placement: 1 }, lean: true }
    );
    const leaderboards = [];

    for (const leaderboard of paginatedLeaderboard.docs) {
      leaderboards.push(await fetchAssociatedObjects(leaderboard));
    }
    paginatedLeaderboard.docs = leaderboards.filter((l) => !lodash.isEmpty(l));

    // todo: investigate
    // @ts-ignore
    return paginatedLeaderboard;
  } catch (error) {
    logger.error({ message: error.message });
    throw error;
  }
};

/**
 * @summary Get active Leaderboard
 *
 * @param {String} boardType - streaks, views or engagements etc
 * @param {Number} page - Page Number
 * @param {Number} limit - Number of records per page
 *
 * @return {Promise<any[]>} Paginated collection of leaderboard
 */
const getActiveLeaderboard = async (boardType, page = 1, limit = 10) => {
  // Verify param
  if (!BOARD_TYPES.includes(boardType)) {
    throw createError(400, "Invalid board type");
  }

  // Get particular active leaderboard based on boardType
  const paginatedLeaderboard = await Leaderboard.paginate(
    { isActive: true, boardType },
    { page, limit, sort: { placement: 1 }, lean: true }
  );

  // Build leaderboard collection
  const leaderboards = [];

  for (const leaderboard of paginatedLeaderboard.docs) {
    leaderboards.push(await fetchAssociatedObjects(leaderboard));
  }
  paginatedLeaderboard.docs = leaderboards.filter((l) => !lodash.isEmpty(l));

  // todo: investigate
  // @ts-ignore
  return paginatedLeaderboard;
};

/**
 * @summary Fetch all the associated objects & append to leaderboard
 *
 * @param {Object} leaderboard
 * @return {Promise<any>} Leaderboard object
 */
const fetchAssociatedObjects = async (leaderboard) => {
  const user = await UserLib.getUserById(leaderboard.user);
  // If user is not found, most probably user is deleted
  if (!user) {
    leaderboard = {};
    return leaderboard;
  }
  leaderboard.user = user;

  // Fetching streaks only when it is needed
  if (leaderboard.streak) {
    leaderboard.streak = await streaksService.getStreakByQuery({
      _id: leaderboard.streak
    });
  }

  // Fetching user stats only when it is needed
  if (leaderboard.stats) {
    leaderboard.stats = await UserStats.findOne({
      _id: leaderboard.stats
    })
      .select("postViews totalEngagements")
      .lean();
  }

  return leaderboard;
};

/**
 * @summary Get top 3 leaderboards from each type
 *
 * @param {Number} limit - Number of records per boardType
 *
 * @return {Promise<any>} - {streaksLeaderboard, viewsLeaderboard, engagementsLeaderboard}
 */
const getTopperFromAllLeaderboards = async (limit = 3) => {
  // Converting to number

  limit = Number(limit);

  // Fallback to default 3
  if (Number.isNaN(limit) || limit <= 0) {
    limit = 3;
  }

  // Restricting the limit to 10 at max
  if (limit > 10) {
    limit = 10;
  }

  // Get top x streaks

  const streaksLeaderboard = await getActiveLeaderboard("streaks", 1, limit);

  // Get top x views

  const viewsLeaderboard = await getActiveLeaderboard("views", 1, limit);

  // Get top x engagements
  const engagementsLeaderboard = getActiveLeaderboard("engagements", 1, limit);

  // Return the results
  return {
    // todo: investigate
    // @ts-ignore
    streaksLeaderboard: streaksLeaderboard.docs,
    // todo: investigate
    // @ts-ignore
    viewsLeaderboard: viewsLeaderboard.docs,
    // todo: investigate
    // @ts-ignore
    engagementsLeaderboard: engagementsLeaderboard.docs
  };
};

/**
 * @summary Method to create leaderboard from active streaks
 */
const createStreaksLeaderboard = async () => {
  try {
    // Making sure streaks are not created today
    const alreadyCreated = await leaderboardAlreadyCreated("streaks");

    if (alreadyCreated) {
      logger.info("Streaks Leaderboard is already created");
      return;
    }

    logger.info("Fetching active streaks");
    // Fetch active streaks
    const activeStreaks = await streaksService.getStreaksByQuery(
      { isActive: true, streakName: "dailyUsage" },
      { sort: { streakDuration: -1 }, lean: true }
    );

    // Check presence
    const streaksLength = activeStreaks.length;
    if (streaksLength === 0) {
      logger.info("No active streaks found!");
      return;
    }

    // Iterate & set jobs to create leaderboard
    await iterateAndSetLeaderboardJob(streaksLength, activeStreaks, "streaks", "streakDuration", "streak");

    // Cleanup leaderboard
    logger.info("Setting up job to cleanup old leaderboard");
    await deactivateOldLeaderboardAsync("streaks");
  } catch (error) {
    logger.error(
      { message: error.message, stack: error.stack },
      "Error while setting up jobs for leaderboard creation"
    );
  }
};

/**
 * @summary Cronjob to create views leaderboards
 */
const createViewsLeaderboard = async () => {
  await buildUserStatsLeaderboard("views", "postViews");
};

/**
 * @summary Cronjob to create engagements leaderboards
 */
const createEngagementsLeaderboard = async () => {
  await buildUserStatsLeaderboard("engagements", "totalEngagements");
};

/**
 * @summary Build leaderboard from a specific field in UserStats model
 *
 * @param {String} boardType - views or engagements etc
 * @param {String} targetField - postViews or totalEngagements etc
 */
const buildUserStatsLeaderboard = async (boardType, targetField) => {
  try {
    // Making sure streaks are not created today
    const alreadyCreated = await leaderboardAlreadyCreated(boardType);

    if (alreadyCreated) {
      logger.info(`${boardType} Leaderboard is already created`);
      return;
    }

    // Fetching user stats length
    const userStatsLength = await UserStats.estimatedDocumentCount();

    // Verify stats presence
    if (userStatsLength === 0) {
      logger.info(`user stats are not found, exiting without creating ${boardType} leaderboard`);
      return;
    }

    // Get top 2000 stats
    const stats = await UserStats.find({})
      .sort({ [targetField]: -1 })
      .limit(2000)
      .lean();

    // Iterate over stats & set jobs to create leaderboard
    await iterateAndSetLeaderboardJob(userStatsLength, stats, boardType, targetField, "stats");

    // Cleanup old views leaderboards
    logger.info(`Setting up job to cleanup old ${boardType} leaderboards`);
    await deactivateOldLeaderboardAsync(boardType);
  } catch (error) {
    logger.error(
      { message: error.message, stack: error.stack },
      `Error while setting up jobs for ${boardType} leaderboard creation`
    );
  }
};

/**
 * @summary Iterate over collection & set jobs to create leaderboard object
 *
 * @param {Number} totalRecords
 * @param {Array} collection - collection from where leaderboard is to be created
 * @param {String} boardType - type of leaderboard i.e streaks, views etc
 * @param {String} targetValueFieldName - Name of the field leaderboard will be build using e.g streakDuration
 * @param {String} targetObjectContainerName - Name of field where reference to target object will saved e.g streak
 */
const iterateAndSetLeaderboardJob = async (
  totalRecords,
  collection,
  boardType,
  targetValueFieldName,
  targetObjectContainerName
) => {
  logger.info(`Got ${totalRecords} records. Building ${boardType} leaderboard`);
  const iterator = { prevPlacement: 0, prevTargetFieldValue: 0, percentile: 0 };
  let index = 0;

  for (const object of collection) {
    const targetValue = object[targetValueFieldName];
    /**
     * modifying values only when user is not on same spot e.g two users
     * having same streakDuration will have same placement & percentile
     */
    if (iterator.prevTargetFieldValue !== targetValue) {
      // todo: investigate
      // @ts-ignore
      iterator.percentile = Math.round(((index + 0.5) / totalRecords) * 100, 2);
      iterator.prevPlacement = index + 1;
      iterator.prevTargetFieldValue = targetValue;
    }

    await saveLeaderboardAsync({
      placement: iterator.prevPlacement,
      placementPercent: iterator.percentile,
      user: object.user,
      isActive: true,
      boardType,
      [targetObjectContainerName]: object._id,
      streakDuration: targetValue,
      valueAtTheTimeOfCreation: targetValue
    });
    index += 1;
  }
  logger.info(`All jobs added to queue for ${boardType} Leaderboard creation`);
};

/**
 * @summary Save Leaderboard to DB
 *
 * @param {Object} leaderboard
 */
const saveLeaderboard = async (leaderboard) => {
  try {
    logger.info({ leaderboard }, "Creating leaderboard");
    return await new Leaderboard(leaderboard).save();
  } catch (error) {
    logger.error({ error, leaderboard }, "Error while saving leaderboard object");
  }
};

/**
 * @summary Method to deactivate old leaderboard
 *
 * @param {String} boardType - Type of board i.e streaks, views etc
 */
const deactivateOldLeaderboard = async (boardType) => {
  try {
    // This will be removed once we migrate data
    const boardTypeQuery = boardType === "streaks" ? LEGACY_STREAKS_QUERY : { boardType };
    // todo: fix
    // @ts-ignore
    const timestamp = new Date(moment().startOf("day"));

    logger.info(`Cleaning up Leaderboard where createdAt < ${timestamp}`);
    await Leaderboard.updateMany(
      {
        isActive: true,
        createdAt: { $lt: timestamp },
        ...boardTypeQuery
      },
      { isActive: false, updatedAt: Date.now() }
    );
  } catch (error) {
    logger.error({ message: error.message }, "Error while deactivating old leaderboard");
  }
};

/**
 * @summary Get streaks leaderboard by userId | This method is deprecated & is only added
 * added to support legacy streaks leaderboards. It will be removed once we migrate the data
 * & frontend transitions away from it.
 *
 * @param {ObjectId} userId - ID of user
 * @return {Promise<any>} Leaderboard
 */
const deprecatedGetLeaderboardByUserId = async (userId) => {
  return await Leaderboard.findOne({
    user: userId,
    isActive: true,
    ...LEGACY_STREAKS_QUERY
  });
};

/**
 * @summary Get active Leaderboards for a user by userId
 *
 * @param {ObjectId} userId - ID of user
 * @return {Promise<any>} Leaderboard
 */
const getLeaderboardByUserId = async (userId) => {
  const leaderboards = await Leaderboard.find({ user: userId, isActive: true });

  return leaderboards.reduce((previousValue, currentValue) => {
    const { boardType } = currentValue;
    const key = !boardType ? "streaksLeaderboard" : `${boardType}Leaderboard`;
    previousValue[key] = currentValue;
    return previousValue;
  }, {});
};

/**
 * @summary Async method to add job to queue for cleanup
 *
 * @param {String} boardType - Type of board i.e streaks, views etc
 */
const deactivateOldLeaderboardAsync = async (boardType) => {
  return await deactivateOldLeaderboardQueue.add({ boardType });
};

/**
 * @summary Async Queue deactivateOldLeaderboard
 */
const deactivateOldLeaderboardQueue = Config.initQueue("deactivateOldLeaderboard");

/**
 * @summary Async Queue saveLeaderboard
 */
const saveLeaderboardQueue = Config.initQueue("saveLeaderboard");

/**
 * @summary Async method add job to queue for storing leaderboard in DB
 */
const saveLeaderboardAsync = async (leaderboard) => {
  return await saveLeaderboardQueue.add({ leaderboard });
};

/**
 * @summary Init Async Processors
 */
const initAsyncProcessors = () => {
  deactivateOldLeaderboardQueue.process((job) => {
    return deactivateOldLeaderboard(job.data.boardType);
  });

  saveLeaderboardQueue.process((job) => {
    return saveLeaderboard(job.data.leaderboard);
  });
};

const leaderboardAlreadyCreated = async (boardType) => {
  // todo: fix
  // @ts-ignore
  const startOfToday = new Date(moment().startOf("day"));

  const count = await Leaderboard.find({ boardType, createdAt: { $gte: startOfToday } }).countDocuments();

  return count > 0;
};

module.exports.BOARD_TYPES = BOARD_TYPES;
module.exports.buildUserStatsLeaderboard = buildUserStatsLeaderboard;
module.exports.createEngagementsLeaderboard = createEngagementsLeaderboard;
module.exports.createStreaksLeaderboard = createStreaksLeaderboard;
module.exports.createViewsLeaderboard = createViewsLeaderboard;
module.exports.deactivateOldLeaderboard = deactivateOldLeaderboard;
module.exports.deactivateOldLeaderboardAsync = deactivateOldLeaderboardAsync;
module.exports.deactivateOldLeaderboardQueue = deactivateOldLeaderboardQueue;
module.exports.deprecatedGetActiveLeaderboard = deprecatedGetActiveLeaderboard;
module.exports.deprecatedGetLeaderboardByUserId = deprecatedGetLeaderboardByUserId;
module.exports.fetchAssociatedObjects = fetchAssociatedObjects;
module.exports.getActiveLeaderboard = getActiveLeaderboard;
module.exports.getLeaderboardByUserId = getLeaderboardByUserId;
module.exports.getTopperFromAllLeaderboards = getTopperFromAllLeaderboards;
module.exports.initAsyncProcessors = initAsyncProcessors;
module.exports.iterateAndSetLeaderboardJob = iterateAndSetLeaderboardJob;
module.exports.leaderboardAlreadyCreated = leaderboardAlreadyCreated;
module.exports.LEGACY_STREAKS_QUERY = LEGACY_STREAKS_QUERY;
module.exports.saveLeaderboard = saveLeaderboard;
module.exports.saveLeaderboardAsync = saveLeaderboardAsync;
module.exports.saveLeaderboardQueue = saveLeaderboardQueue;
