const axios = require("axios");
const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();

const { Media } = require("../../backend/models/media");
const { User } = require("../../backend/models/User");
const { FacebookPageCrossPost } = require("../../backend/models/FacebookPageCrossPost");

const { createChecksum, getDuplicateStatusCutoffDate } = require("./utils/crossPost");

const FacebookAsync = require("../../async/creator-tools/facebook");

/**
 * @summary Cross post to facebook
 *
 * @param {ObjectId} userId - Id of the user
 * @param {String} content - Content of the post
 * @param {object} mediaId - Media id
 * @return {Promise<undefined>}
 */
const crossPostToFacebookPage = async (userId, content, mediaId, mediaIds) => {
  const checksum = createChecksum(`${userId}+${content}`);
  const duplicateCutoffDate = getDuplicateStatusCutoffDate();

  const existingCrossPost = await FacebookPageCrossPost.find({
    checksum,
    createdAt: { $gt: duplicateCutoffDate }
  });
  if (existingCrossPost.length) {
    logger.warn(
      `This status has already been cross posted. Likely a duplicate lambda call. UserId: ${userId}, Checksum: ${checksum}`
    );
    return;
  }

  const user = await User.findById(userId).select("+facebookPages +facebookPages.accessToken");

  if (!user) {
    logger.error("User not found", { user: userId });
    return;
    // todo: investigate
    // @ts-ignore
  } else if (!user.facebookPages.length) {
    logger.error("User has no Facebook Pages", {
      user: user.email || user._id
    });
    return;
  }

  // NOTE: Currently only supporting single Facebook Page. Need to revisit to support multi pages.
  // todo: investigate
  // @ts-ignore
  const userPageData = user.facebookPages[0];
  if (!userPageData || !userPageData.pageId || !userPageData.accessToken) {
    logger.error("Invalid Facebook Page Data", {
      user: user.email || user._id,
      pageData: userPageData
    });
    return;
  }

  const crossPostDoc = await FacebookPageCrossPost.create({
    user: userId,
    content,
    media: mediaId,
    mediaObjects: mediaIds,
    checksum
  });

  let media = null;
  if (mediaId) {
    media = await Media.findById(mediaId);
  }

  try {
    const data = await sendMessageToFacebookPage(userPageData, content, media);
    await crossPostDoc.markSuccess(data);
    logger.info("Successfully sent cross post to Facebook Page", {
      user: user.email,
      data
    });
  } catch (error) {
    const msg = error.message ? error.message : error;
    logger.error("Failed sending cross post to Facebook Page", {
      user: user.email,
      error: msg
    });
    logger.info("Raw error", error);
    await crossPostDoc.markFailed(error);
    return;
  }
  return;
};

/**
 * @summary Retry cross post to facebook
 *
 * @returns {Promise<undefined>}
 */
const retryCrossPostToFacebookPage = async () => {
  const recentTime = new Date();
  recentTime.setHours(recentTime.getHours() - 4);

  const retryPosts = await FacebookPageCrossPost.find({
    errorCode: { $exists: true },
    retryCount: { $lt: 3 },
    createdAt: { $gte: recentTime },
    sentAt: { $exists: false }
  });

  if (!retryPosts.length) return;

  for (const post of retryPosts) {
    logger.info(`Retrying failed Facebook Page cross post ${post._id}`);
    const user = await User.findById(post.user).select("+facebookPages +facebookPages.accessToken");

    // todo: investigate
    // @ts-ignore
    if (!user.facebookPages.length) continue;

    let media = null;
    if (post.media) {
      media = await Media.findById(post.media);
    }

    try {
      // todo: investigate
      // @ts-ignore
      const data = await sendMessageToFacebookPage(user.facebookPages[0], post.content, media);

      await post.markSuccess(data);
      logger.info("Successfully sent cross post to Facebook Page", {
        // todo: investigate
        // @ts-ignore
        user: user.email,
        data
      });
    } catch (error) {
      const msg = error.message ? error.message : error;
      logger.error("Failed sending cross post to Facebook Page", {
        // todo: investigate
        // @ts-ignore
        user: user.email,
        error: msg
      });
      await post.markFailed(error);
    }
  }

  return;
};

/**
 * @summary Send message to facebook
 *
 * @param {Object} pageData - Users facebook page data
 * @param {String} content - Text message to send to facebook
 * @param {Object} media - Any media to send to facebook
 * @returns {Promise<any>} post
 */
const sendMessageToFacebookPage = async (pageData, content, media = null) => {
  if (media) {
    if (media.file.contentType.startsWith("image")) {
      return await sendImagePostToFacebookPage(pageData, content, media);
    } else if (media.file.contentType.startsWith("video")) {
      return await sendVideoPostToFacebookPage(pageData, content, media);
    }
  }
  return await sendTextPostToFacebookPage(pageData, content);
};

/**
 * @summary Send text message to facebook
 *
 * @param {Object} pageData - Users facebook page data
 * @param {String} content - Message to send to facebook
 * @returns {Promise<any>} { postId: ObjectId };
 */
const sendTextPostToFacebookPage = async (pageData, content) => {
  // todo: investigate
  // @ts-ignore
  const response = await axios.post(`https://graph.facebook.com/${pageData.pageId}/feed`, {
    message: content,
    access_token: pageData.accessToken
  });
  logger.info("Sent cross post to facebook", response);
  return { postId: response.data.id };
};

/**
 * @summary Send image to facebook
 *
 * @param {Object} pageData - Users facebook page data
 * @param {String} content - Text message to send to facebook
 * @param {Object} media - Any media to send to facebook
 * @returns {Promise<any>} { postId: ObjectId pictureId: ObjectId }
 */
const sendImagePostToFacebookPage = async (pageData, content, media) => {
  // todo: investigate
  // @ts-ignore
  const response = await axios.post(`https://graph.facebook.com/${pageData.pageId}/photos`, {
    url: media.file.url,
    message: content,
    access_token: pageData.accessToken
  });
  return { postId: response.data.post_id, pictureId: response.data.id };
};

/**
 * @summary Send video to facebook
 *
 * @param {Object} pageData - Users facebook page data
 * @param {String} content - Text message to send to facebook
 * @param {Object} media - Any media to send to facebook
 * @returns {Promise<any>} { postId: ObjectId videoId: ObjectId }
 */
const sendVideoPostToFacebookPage = async (pageData, content, media) => {
  // todo: investigate
  // @ts-ignore
  const response = await axios.post(`https://graph.facebook.com/${pageData.pageId}/videos`, {
    file_url: media.file.url,
    message: content,
    access_token: pageData.accessToken
  });
  return { postId: response.data.post_id, videoId: response.data.id };
};

const crossPostToFacebookPageAsync = async (userId, content, mediaId, mediaIds) => {
  return await FacebookAsync.crossPostToFacebookPageQueue.add({
    userId,
    content,
    mediaId,
    mediaIds
  });
};

module.exports.crossPostToFacebookPage = crossPostToFacebookPage;
module.exports.crossPostToFacebookPageAsync = crossPostToFacebookPageAsync;
module.exports.retryCrossPostToFacebookPage = retryCrossPostToFacebookPage;
module.exports.sendImagePostToFacebookPage = sendImagePostToFacebookPage;
module.exports.sendMessageToFacebookPage = sendMessageToFacebookPage;
module.exports.sendTextPostToFacebookPage = sendTextPostToFacebookPage;
module.exports.sendVideoPostToFacebookPage = sendVideoPostToFacebookPage;
