import { FilterQuery, UpdateQuery, Types, LeanDocument } from "mongoose";
import { Failure } from "@efuse/contracts";
import { Queue } from "bull";

import { FeedsBaseService } from "../feeds-base.service";
import { IScheduledFeed } from "../../backend/interfaces/scheduled-feed";
import { ScheduledFeedModel, ScheduledFeed } from "../../backend/models/scheduled-feed";
import { FeedService } from "../feeds/feed.service";
import { StreaksService } from "../streaks/streaks.service";

import { Service, Trace } from "../decorators";
import { EFuseAnalyticsUtil } from "../analytics.util";
import { CronStatusEnum } from "../track-cron/track-cron.enum";
import Config from "../../async/config";

const SERVICE = "scheduled-feed.service";

@Service(SERVICE)
export class ScheduledFeedService extends FeedsBaseService {
  private readonly scheduledFeeds: ScheduledFeedModel<IScheduledFeed> = ScheduledFeed;
  private feedService: FeedService;
  private streaksService: StreaksService;

  private $processScheduledPostQueue: Queue;

  constructor() {
    super();

    this.feedService = new FeedService();
    this.streaksService = new StreaksService();

    this.$processScheduledPostQueue = Config.initQueue("processScheduledPost");
  }

  /**
   * @summary Process scheduled post
   */
  @Trace({ service: SERVICE, name: "ProcessScheduledPostsCron" })
  public static async ProcessScheduledPostsCron(): Promise<void> {
    const scheduledFeedService = new ScheduledFeedService();

    try {
      // Fetch posts to be processed
      const scheduledFeed = await scheduledFeedService.scheduledFeeds
        .find(
          { scheduledAt: { $lt: new Date() }, processed: false },
          {},
          // todo: investigate correct usage, is this from a plugin?
          <never>{ autopopulate: false }
        )
        .lean();

      for (const post of scheduledFeed) {
        EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.post.calls");

        try {
          scheduledFeedService.processScheduledPostAsync(post);

          await scheduledFeedService.update(
            { _id: post._id as string },
            { processed: true, jobSetAt: new Date(), status: CronStatusEnum.PROCESSING }
          );
        } catch (error) {
          EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.post.error");

          scheduledFeedService.logger!.error(error, "error while processing a particular scheduled post");
        }
      }

      EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.cron.success");
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.cron.error");

      scheduledFeedService.logger?.error(error, "error while creating scheduled posts");
    }
  }

  /**
   * @summary Process a scheduled post & create the Feed
   *
   * @param {LeanDocument<IScheduledFeed>} scheduledFeed
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async processScheduledPost(scheduledFeed: LeanDocument<IScheduledFeed>): Promise<void> {
    try {
      // Create Feed, HomeFeed, LoungeFeed, Cross Posts
      await this.feedService.createFeedAndPerformActions(
        scheduledFeed.user,
        scheduledFeed.timelineable,
        scheduledFeed.timelineableType,
        scheduledFeed.verified,
        scheduledFeed.platform,
        scheduledFeed.mentions,
        scheduledFeed.content,
        scheduledFeed.media,
        scheduledFeed.mediaObjects,
        scheduledFeed.twitchClip,
        scheduledFeed._id,
        scheduledFeed.shared,
        scheduledFeed.sharedFeed,
        scheduledFeed.sharedTimeline,
        scheduledFeed.sharedTimelineable,
        scheduledFeed.sharedTimelineableType,
        scheduledFeed.sharedPostUser,
        scheduledFeed.crossPosts,
        scheduledFeed.associatedGame,
        scheduledFeed.youtubeVideo
      );

      // Update Streak
      await this.streaksService.checkStreak(scheduledFeed.user.toString(), "dailyUsage", "Scheduled Post");

      // Mark Scheduled feed as success
      await this.markAsSuccess(scheduledFeed._id);
    } catch (error) {
      this.logger?.error(error, "Error while processing Scheduled Feed");

      // Mark Scheduled feed as error

      await this.markAsError(scheduledFeed._id, error?.message || "Something went wrong");
    }
  }

  /**
   * @summary Process a scheduled post & create the Feed using bull queue
   *
   * @param {LeanDocument<IScheduledFeed>} scheduledFeed
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public processScheduledPostAsync(scheduledFeed: LeanDocument<IScheduledFeed>): void {
    this.$processScheduledPostQueue
      .add({ scheduledFeed })
      .catch((error) => this.logger?.error(error, "error adding job to queue processScheduledPostQueue"));
  }

  /**
   * @summary Mark the scheduled feed as success
   *
   * @param {Types.ObjectId | string} scheduledFeedId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async markAsSuccess(scheduledFeedId: Types.ObjectId | string): Promise<void> {
    await this.updateScheduledFeedStatus(scheduledFeedId, { status: CronStatusEnum.SUCCESS });
  }

  /**
   * @summary Mark the scheduled feed as error
   *
   * @param {Types.ObjectId | string} scheduledFeedId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async markAsError(scheduledFeedId: Types.ObjectId | string, errorMessage: string): Promise<void> {
    await this.updateScheduledFeedStatus(scheduledFeedId, { status: CronStatusEnum.ERROR, errorMessage });
  }

  /**
   * @summary Update a particular scheduled feed status
   *
   * @param {Types.ObjectId | string} scheduledFeedId
   * @param {UpdateQuery<IScheduledFeed>} body
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateScheduledFeedStatus(
    scheduledFeedId: Types.ObjectId | string,
    body: UpdateQuery<IScheduledFeed>
  ): Promise<void> {
    try {
      await this.update({ _id: scheduledFeedId }, { ...body, jobCompletedAt: new Date() });
    } catch (error) {
      this.logger?.error(error, "Error updating scheduled feed status");
    }
  }

  /**
   * @summary Update a particular schedule Feed object
   *
   * @param {FilterQuery<IScheduledFeed>} query
   * @param {UpdateQuery<IScheduledFeed>} body
   *
   * @returns {Promise<IScheduledFeed>}
   */
  @Trace()
  public async update(query: FilterQuery<IScheduledFeed>, body: UpdateQuery<IScheduledFeed>): Promise<IScheduledFeed> {
    const scheduledFeed: IScheduledFeed | null = await this.scheduledFeeds
      .findOneAndUpdate(query, body, { new: true })
      .lean();

    if (!scheduledFeed) {
      throw Failure.UnprocessableEntity("Scheduled Feed not found");
    }

    return scheduledFeed;
  }

  /** Async Processors */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const scheduledFeedService = new ScheduledFeedService();

    // processScheduledPostQueue
    scheduledFeedService.$processScheduledPostQueue
      .process((job: { data: { scheduledFeed: IScheduledFeed } }) => {
        return scheduledFeedService.processScheduledPost(job.data.scheduledFeed);
      })
      .catch((error) => scheduledFeedService.logger?.error(error, "Error while processing processScheduledPostQueue"));
  }

  /**
   * @summary Create a new scheduled feed object
   *
   * @param {IScheduledFeed} scheduledFeedBody
   *
   * @return {Promise<IScheduledFeed>}
   */
  @Trace()
  public async create(scheduledFeedBody: IScheduledFeed): Promise<IScheduledFeed> {
    const doc = <IScheduledFeed>scheduledFeedBody;
    const scheduledFeed = await this.scheduledFeeds.create(doc);
    return scheduledFeed;
  }
}
