const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();

const { User } = require("../../backend/models/User");
const { subscribeToStreamChanges } = require("../twitch");
const { BACKEND_DOMAIN } = require("@efuse/key-store");

const { EFuseAnalyticsUtil } = require("../analytics.util");

const TwitchAsync = require("../../async/creator-tools/twitch");

/**
 * @summary Subscribe to stream webhook
 *
 * @param {ObjectId} owner - Id of the user
 */
const subscribeToStreamChangeWebhook = async (owner, ownerKind) => {
  // TODO: [org-clout] add support for orgs
  try {
    EFuseAnalyticsUtil.incrementMetric("twitchWebhook.calls");

    const user = await User.findById(owner, {
      twitchAccessToken: 1,
      twitchID: 1
    });

    const { payload, response } = await subscribeToStreamChanges(
      BACKEND_DOMAIN,
      // todo: investigate
      // @ts-ignore
      user._id,
      // todo: investigate
      // @ts-ignore
      user.twitchID,
      // todo: investigate
      // @ts-ignore
      user.twitchAccessToken
    );
    EFuseAnalyticsUtil.incrementMetric("twitchWebhook.success");
    // todo: investigate
    // @ts-ignore
    logger.info({ user: user.email || owner, payload }, "Successfully subscribed to twitch webhook.");
  } catch (error) {
    error.user = owner;
    logger.error(error, `Failed to subscribe user to twitch stream changes webhook`);
    EFuseAnalyticsUtil.incrementMetric("twitchWebhook.failed");
  }
  return;
};

const subscribeToStreamChangeWebhookAsync = async (owner, ownerKind) => {
  return await TwitchAsync.subscribeToStreamChangeWebhookQueue.add({ owner, ownerKind });
};

module.exports.subscribeToStreamChangeWebhook = subscribeToStreamChangeWebhook;
module.exports.subscribeToStreamChangeWebhookAsync = subscribeToStreamChangeWebhookAsync;
