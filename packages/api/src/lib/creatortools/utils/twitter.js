const fs = require("fs");
const Twitter = require("twitter");
const Twit = require("twit");
const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();
const { OAuthServiceKind } = require("@efuse/entities");

const { Media } = require("../../../backend/models/media");
const { User } = require("../../../backend/models/User");

const { downloadMediaFromS3 } = require("./aws");

const {
  TWITTER_API_KEY,
  TWITTER_API_SECRET,
  TWITTER_ACCESS_TOKEN,
  TWITTER_ACCESS_TOKEN_SECRET
} = require("@efuse/key-store");

const { TwitterAccountInfoService } = require("../../twitter-account-info.service");
const { OAuthCredentialService } = require("../../oauth-credential.service");

/**
 * @summary Get twitter information of the user
 *
 * @param {Object} user - The user making request
 * @return {Object} twitter
 */
const getTwitterNodeClient = (user = null) => {
  if (!user) {
    return new Twitter({
      consumer_key: TWITTER_API_KEY,
      consumer_secret: TWITTER_API_SECRET,
      access_token_key: TWITTER_ACCESS_TOKEN,
      access_token_secret: TWITTER_ACCESS_TOKEN_SECRET
    });
  }

  if (!user.twitterOauthToken || !user.twitterOauthTokenSecret) {
    throw new Error(`User ${user.email} does not have Twitter credentials`);
  }

  return new Twitter({
    consumer_key: TWITTER_API_KEY,
    consumer_secret: TWITTER_API_SECRET,
    access_token_key: user.twitterOauthToken,
    access_token_secret: user.twitterOauthTokenSecret
  });
};

/**
 * @summary Get twitter information of the user
 *
 * @param {Object} user - The user making request
 * @return {Object} twitter
 */
const getTwitClient = (user = null) => {
  if (!user) {
    return new Twitter({
      consumer_key: TWITTER_API_KEY,
      consumer_secret: TWITTER_API_SECRET,
      // todo: investigate
      // @ts-ignore
      access_token: TWITTER_ACCESS_TOKEN,
      access_token_secret: TWITTER_ACCESS_TOKEN_SECRET
    });
  }

  if (!user.twitterOauthToken || !user.twitterOauthTokenSecret) {
    throw new Error(`User ${user.email} does not have Twitter credentials`);
  }

  return new Twit({
    consumer_key: TWITTER_API_KEY,
    consumer_secret: TWITTER_API_SECRET,
    access_token: user.twitterOauthToken,
    access_token_secret: user.twitterOauthTokenSecret
  });
};

/**
 * @summary Upload media files to twitter
 *
 * @param {ObjectId} mediaId - Id of the media to upload to twitter
 * @param {Object} client - The twitter client
 * @param {Object} twitterNodeClient - The twitter client
 * @return {Promise<ObjectId>} twitterMediaId
 */
const uploadMediaToTwitter = async (mediaId, client, twitterNodeClient) => {
  const media = await Media.findById(mediaId);
  // todo: investigate
  // @ts-ignore
  const { filename } = media.file;
  // todo: investigate
  // @ts-ignore
  const s3Object = await downloadMediaFromS3(filename);
  const mediaData = s3Object.Body;
  const fileOut = `/tmp/${filename}`;
  logger.info("Writing tmp file", fileOut);
  await fs.promises.writeFile(fileOut, mediaData);

  const twitterMediaId = await uploadMediaChunks(fileOut, client)
    .then(async (response) => {
      return response.media_id_string || null;
    })
    .catch((error) => {
      logger.error("Failed to upload media to Twitter:", error);
      throw error;
    });

  let uploadResponse;
  try {
    uploadResponse = await uploadMediaChunks(fileOut, client);
    logger.info("Uploaded media to twitter", twitterMediaId);
  } catch (error) {
    logger.error("Failed to upload media to Twitter:", error);
    throw error;
  }

  if (uploadResponse && uploadResponse.processing_info && uploadResponse.processing_info.state === "pending") {
    let uploadFinished = false;
    while (!uploadFinished) {
      const checkResponse = await checkMediaStatus(twitterMediaId, twitterNodeClient);
      const state = checkResponse && checkResponse.processing_info && checkResponse.processing_info.state;
      logger.info("Checking uploaded media status", checkResponse);

      if (state === "succeeded") {
        uploadFinished = true;
        break;
      } else if (!uploadFinished && state === "in_progress") {
        await new Promise((resolve) => setTimeout(resolve, 3000));
      } else if (state === "failed") {
        const err = new Error(`Failed to upload chunked media: ${checkResponse.processing_info.error.message}`);
        // todo: investigate
        // @ts-ignore
        err.response = checkResponse;
        // todo: investigate
        // @ts-ignore
        err.error = checkResponse.processing_info.error;
        throw err;
      } else {
        const err = new Error("Failed to upload chunked media for unknow reason");
        // todo: investigate
        // @ts-ignore
        err.response = checkResponse;
        // todo: investigate
        // @ts-ignore
        err.error = checkResponse.processing_info.error || null;
        throw err;
      }
    }
  }
  await fs.promises.unlink(fileOut);
  return twitterMediaId;
};

/**
 * @summary Upload chunked media file
 *
 * @param {String} filename - File name to upload
 * @param {Object} client - The twitter client
 * @return {Object} data
 */
const uploadMediaChunks = (filename, client) => {
  logger.info("Uploading chunked file: ", filename);
  return new Promise((resolve, reject) => {
    client.postMediaChunked({ file_path: filename }, (err, data, response) => {
      if (err) {
        logger.info("uploadMediaChunks error", err);
        reject(err);
      } else {
        logger.info("uploadMediaChunks data", data);
        resolve(data);
      }
    });
  });
};

/**
 * @summary Check status of the media file
 *
 * @param {ObjectId} mediaId - Media to check its status
 * @param {Object} client - The twitter client
 * @return {Object} data
 */
const checkMediaStatus = (mediaId, client) => {
  logger.info("Checking on twitter media id", mediaId);
  return client
    .get("media/upload", {
      command: "STATUS",
      media_id: mediaId
    })
    .then((data) => data)
    .catch((error) => logger.info("Failed to check uploaded media status", error));
};

/**
 * @summary Post feed content to twitter status
 *
 * @param {String} content - Content of the post
 * @param {ObjectId} twitterMediaId - Media id
 * @param {object} twitterClient - Twitter user Object
 */
const postStatusUpdateToTwitter = async (content, twitterMediaId, twitterClient) => {
  const status = { status: content };
  if (twitterMediaId) {
    status.media_ids = [twitterMediaId];
  }
  return await twitterClient.post("statuses/update", status);
};

/**
 * @summary Unlink twitter account from Efuse
 *
 * @param {ObjectId} userId - User id
 * @return {Promise<any>} updateObj
 */
const unlinkTwitterAccount = async (userId) => {
  const updateObj = {
    twitterVerified: false,
    $unset: {
      twitterId: "",
      twitterUsername: "",
      twitterfollowersCount: "",
      twitterOauthToken: "",
      twitterOauthTokenSecret: ""
    }
  };

  // todo: investigate
  // @ts-ignore
  await User.findOneAndUpdate({ _id: userId }, updateObj, {
    new: true
  });

  let stringUserId = userId;
  if (userId.toHexString) {
    // todo: investigate
    // @ts-ignore
    stringUserId = userId.toHexString();
  }

  // delete saved oauth credentials and account info
  const twitterAccountInfoService = new TwitterAccountInfoService();
  const oauthCredentialService = new OAuthCredentialService();
  // todo: investigate
  // @ts-ignore
  await twitterAccountInfoService.deleteByOwner(stringUserId);
  // todo: investigate
  // @ts-ignore
  await oauthCredentialService.deleteByOwnerAndService(stringUserId, OAuthServiceKind.TWITTER);
};

module.exports.getTwitClient = getTwitClient;
module.exports.getTwitterNodeClient = getTwitterNodeClient;
module.exports.postStatusUpdateToTwitter = postStatusUpdateToTwitter;
module.exports.unlinkTwitterAccount = unlinkTwitterAccount;
module.exports.uploadMediaToTwitter = uploadMediaToTwitter;
