const crypto = require("crypto");

/**
 * @summary Create hash
 *
 * @param {String} str - The String whose hash is to be created
 * @return {String} hash
 */
const createChecksum = (str) => crypto.createHash("md5").update(str, "utf8").digest("hex");

/**
 * @summary Get duplicate status cut off date
 *
 * @return {Number} hour
 */
const getDuplicateStatusCutoffDate = () => {
  const now = new Date();
  return now.setHours(now.getHours() - 4);
};

module.exports.createChecksum = createChecksum;
module.exports.getDuplicateStatusCutoffDate = getDuplicateStatusCutoffDate;
