const AWS = require("aws-sdk");

const s3 = new AWS.S3();
const { NODE_ENV } = require("@efuse/key-store");
/**
 * @deprecated Please use the S3Service in `/lib/aws/s3.service.ts`
 * @summary Download a file from S3
 *
 * @param {String} filename - The file name to download
 * @returns {Promise<any>}} file
 */
const downloadMediaFromS3 = async (filename) => {
  const bucketName = NODE_ENV == "production" ? "efuse" : "staging-efuse";
  return new Promise((resolve, reject) => {
    s3.getObject(
      {
        Bucket: bucketName,
        Key: `uploads/media/${filename}`
      },
      (err, data) => {
        if (err) {
          reject(err);
        } else {
          resolve(data);
        }
      }
    );
  });
};

module.exports.downloadMediaFromS3 = downloadMediaFromS3;
