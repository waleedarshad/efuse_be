const axios = require("axios");
const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();

const { Media } = require("../../backend/models/media");
const { User } = require("../../backend/models/User");
const { DiscordCrossPost } = require("../../backend/models/DiscordCrossPost");

const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");

const { createChecksum } = require("./utils/crossPost");

const DiscordAsync = require("../../async/creator-tools/discord");

/**
 * @summary Cross post to discord
 *
 * @param {ObjectId} userId - Id of the user
 * @param {String} content - Content of the post
 * @param {object} mediaId - Media id
 */
const crossPostToDiscord = async (userId, content, mediaId, mediaIds) => {
  EFuseAnalyticsUtil.incrementMetric("discordCrossPost.calls");
  const checksum = createChecksum(`${userId}+${content}`);

  const user = await User.findById(userId).select("+email");

  // todo: investigate
  // @ts-ignore
  if (!user.discordWebhooks.length) {
    // todo: investigate
    // @ts-ignore
    logger.error({ user: user.email }, "User has no Discord webhooks");
    return;
  }

  const crossPostDoc = await DiscordCrossPost.create({
    user: userId,
    content,
    media: mediaId,
    mediaObjects: mediaIds,
    checksum
  });

  let media = null;
  if (mediaId) {
    media = await Media.findById(mediaId);
  }

  try {
    const response = await sendMessageToDiscord(user, content, media);
    const { data } = response;

    if (response.status > 299) {
      throw data;
    }

    await crossPostDoc.markSuccess();
    EFuseAnalyticsUtil.incrementMetric("discordCrossPost.success");
    // todo: investigate
    // @ts-ignore
    logger.info({ user: user.email, data }, "Successfully sent cross post webhook to Discord");
  } catch (error) {
    // todo: investigate
    // @ts-ignore
    logger.error({ user: user.email, error: error }, "Failed sending webhook to Discord");
    EFuseAnalyticsUtil.incrementMetric("discordCrossPost.failed");
    await crossPostDoc.markFailed(error);
  }
  return;
};

/**
 * @summary Retry cross post to discord
 */
const retryCrossPostToDiscord = async () => {
  const recentTime = new Date();
  recentTime.setHours(recentTime.getHours() - 4);

  const retryPosts = await DiscordCrossPost.find({
    errorCode: { $exists: true },
    retryCount: { $lt: 3 },
    createdAt: { $gte: recentTime },
    sentAt: { $exists: false }
  });

  if (!retryPosts.length) return;

  for (const post of retryPosts) {
    logger.info(`Retrying failed Discord cross post ${post._id}`);
    EFuseAnalyticsUtil.incrementMetric("discordCrossPost.calls");
    const user = await User.findById(post.user);

    let media = null;
    if (post.media) {
      media = await Media.findById(post.media);
    }

    try {
      const response = await sendMessageToDiscord(user, post.content, media);
      const { data } = response;

      if (response.status > 299) {
        throw data;
      }

      await post.markSuccess();
      EFuseAnalyticsUtil.incrementMetric("discordCrossPost.success");
      // todo: investigate
      // @ts-ignore
      logger.info({ user: user.email, data }, "Successfully sent cross post webhook to Discord");
    } catch (error) {
      const msg = error.message ? error.message : error;
      // todo: investigate
      // @ts-ignore
      logger.error({ user: user.email, error: msg }, "Failed sending webhook to Discord");
      EFuseAnalyticsUtil.incrementMetric("discordCrossPost.failed");
      await post.markFailed(error);
    }
  }

  return;
};

/**
 * @summary Send message to discord
 *
 * @param {Object} user - User making the request
 * @param {String} content - Message to send to discord
 * @param {Object} media - Media to send along with nessage
 * @return {Promise<any>} post
 */
const sendMessageToDiscord = async (user, content, media = null) => {
  const payload = {
    username: "eFuse.gg",
    avatar_url: "https://cdn.efuse.gg/uploads/static/global/efuseLogo.png",
    content: `<@${user.discordUserId}> posted a message on [eFuse.gg](https://efuse.gg/users/posts?id=${user._id}).`,
    embeds: [
      {
        author: {
          name: `${user.username ? user.username : user.discordUserName}`,
          url: `https://efuse.gg/users/posts?id=${user._id}`,
          icon_url: `${user.profilePicture && user.profilePicture.url ? user.profilePicture.url : ""}`
        },
        description: `${content}`
      }
    ]
  };

  if (media) {
    if (media.file.contentType.startsWith("image")) {
      payload.embeds[0]["image"] = {
        url: media.file.url
      };
    } else if (media.file.contentType.startsWith("video")) {
      // NOTE: Discord seems to no longer support rich video embeds. Best path forward is to get a thumbnail of the video and hyperlink thumbnail to post url
      // payload.embeds[0]["video"] = {
      //   url: encodeURI(media.file.url)
      // };
      payload.content = `<@${user.discordUserId}> posted a video on [eFuse.gg](https://efuse.gg/users/posts?id=${user._id}).`;
      payload.embeds[0].description = payload.embeds[0].description + `\n[Link to video](${encodeURI(media.file.url)})`;
    }
  }

  const util = require("util");
  logger.info("About to post to discord", util.inspect(payload, false, null, true));

  // todo: investigate
  // @ts-ignore
  return await axios.post(
    `${user.discordWebhooks[0].url}?wait=true`,
    { ...payload },
    {
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    }
  );
};

const crossPostToDiscordAsync = async (userId, content, mediaId, mediaIds) => {
  return await DiscordAsync.crossPostToDiscordQueue.add({
    userId,
    content,
    mediaId,
    mediaIds
  });
};

module.exports.crossPostToDiscord = crossPostToDiscord;
module.exports.crossPostToDiscordAsync = crossPostToDiscordAsync;
module.exports.retryCrossPostToDiscord = retryCrossPostToDiscord;
module.exports.sendMessageToDiscord = sendMessageToDiscord;
