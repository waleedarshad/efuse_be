const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");

const { TwitterCrossPost } = require("../../backend/models/TwitterCrossPost");
const { User } = require("../../backend/models/User");

const { createChecksum, getDuplicateStatusCutoffDate } = require("./utils/crossPost");
const {
  getTwitterNodeClient,
  getTwitClient,
  uploadMediaToTwitter,
  postStatusUpdateToTwitter
} = require("./utils/twitter");

const TwitterAsync = require("../../async/creator-tools/twitter");

/**
 * @summary Cross post to twitter
 *
 * @param {ObjectId} userId - Id of the user
 * @param {String} content - Content of the post
 * @param {object} mediaId - Media id
 */
const crossPostToTwitter = async (userId, content, mediaId) => {
  try {
    EFuseAnalyticsUtil.incrementMetric("twitterCrossPost.calls");
    const checksum = createChecksum(`${userId}+${content}`);
    const duplicateCutoffDate = getDuplicateStatusCutoffDate();

    const existingCrossPost = await TwitterCrossPost.find({
      checksum,
      createdAt: { $gt: duplicateCutoffDate }
    });
    if (existingCrossPost.length > 0) {
      logger.warn(
        `This status has already been cross posted. Likely a duplicate lambda call. UserId: ${userId}, Checksum: ${checksum}`
      );
      return;
    }

    const user = await User.findById(userId).select("+twitterOauthToken +twitterOauthTokenSecret");
    const crossPostDoc = await TwitterCrossPost.create({
      user: userId,
      content,
      media: mediaId,
      checksum
    });
    const twitClient = getTwitClient(user);

    try {
      logger.info({ crossPostDoc }, "Cross posting a Twitter status.", {
        // todo: investigate
        // @ts-ignore
        user: user.email || user._id
      });
      let twitterMediaId;
      if (mediaId) {
        const twitterNodeClient = getTwitterNodeClient(user);
        twitterMediaId = await uploadMediaToTwitter(mediaId, twitClient, twitterNodeClient);
      }

      logger.info("Twitter media id: ", twitterMediaId);
      if (!twitterMediaId) {
        const msg = "Twitter media id is empty.  Marking as failed and not posting to twitter";
        logger.error({ twitterMediaId }, "Twitter media id is empty.  Marking as failed and not posting to twitter");
        throw new Error(msg);
      }

      await postStatusUpdateToTwitter(content, twitterMediaId, twitClient);
      await crossPostDoc.markSuccess();
      EFuseAnalyticsUtil.incrementMetric("twitterCrossPost.success");
      logger.info(`Successfully cross posted a Twitter status.`, {
        // todo: investigate
        // @ts-ignore
        user: user.email || user._id,
        crossPostDoc: crossPostDoc.id
      });
    } catch (error) {
      logger.error({ err: error, user }, `Failed to cross post a Twitter status`, error);
      EFuseAnalyticsUtil.incrementMetric("twitterCrossPost.failed");
      await crossPostDoc.markFailed(error);
    }
  } catch (error) {
    const msg = error.message ? error.message : error;
    logger.error({ err: error, userId }, "Failed while running crossPostToTwitter lambda.", msg);
  }
};

/**
 * @summary Retry cross post to discord
 */
const retryTwitterCrossPosts = async () => {
  try {
    const recentTime = new Date();
    recentTime.setHours(recentTime.getHours() - 4);

    const retryPosts = await TwitterCrossPost.find({
      errorCode: { $exists: true },
      retryCount: { $lt: 3 },
      createdAt: { $gte: recentTime }
    });

    if (retryPosts.length === 0) {
      logger.info("There are no twitter posts to retry");
      return;
    }

    for (const post of retryPosts) {
      logger.info(`Retrying failed Twitter cross post ${post._id}`);
      EFuseAnalyticsUtil.incrementMetric("twitterCrossPost.calls");
      const user = await User.findById(post.user).select("+twitterOauthToken +twitterOauthTokenSecret");
      const twitterClient = getTwitClient(user);
      try {
        logger.info("Cross posting a Twitter status.", {
          // todo: investigate
          // @ts-ignore
          user: user.email || user._id
        });
        // todo: investigate
        // @ts-ignore
        const twitterMediaId = post.media ? await uploadMediaToTwitter(post.media, twitterClient) : null;
        // todo: investigate
        // @ts-ignore
        await postStatusUpdateToTwitter(post.content, twitterMediaId, twitterClient);
        await post.markSuccess();
        logger.info("Successfully cross posted a Twitter status.", {
          // todo: investigate
          // @ts-ignore
          user: user.email || user._id,
          crossPostDoc: post.id
        });
        EFuseAnalyticsUtil.incrementMetric("twitterCrossPost.success");
      } catch (error) {
        logger.error("Failed to cross post a Twitter status", error);
        await post.markFailed(error);
        EFuseAnalyticsUtil.incrementMetric("twitterCrossPost.failed");
      }
    }
  } catch (error) {
    const msg = error.message ? error.message : error;
    logger.error("Failed while running retryTwitterCrossPosts lambda.", msg);
  }
};

const crossPostToTwitterAsync = async (userId, content, mediaId) => {
  const result = await TwitterAsync.crossPostToTwitterQueue.add({
    userId,
    content,
    mediaId
  });
  return result;
};

module.exports.crossPostToTwitter = crossPostToTwitter;
module.exports.crossPostToTwitterAsync = crossPostToTwitterAsync;
module.exports.retryTwitterCrossPosts = retryTwitterCrossPosts;
