export enum Auth0Enum {
  refreshTokenGrantType = "refresh_token"
}

export enum EfuseAuthEnum {
  ISS = "api.efuse.gg",
  AUD = "eFuse-app"
}

export enum AuthenticationMsgsEnum {
  INVALID_LOGIN = "Invalid Login",
  USER_BLOCKED = "You are blocked by Admin, Please contact admin",
  SUCCESS = "Logged in successfully!"
}
