import axios, { AxiosInstance, AxiosResponse } from "axios";
import { AUTH0_CLIENT_ID, AUTH0_CLIENT_SECRET, AUTH0_INTERNAL_ISSUER_URL, AUTH0_SCOPES } from "../../config";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { Auth0Enum } from "./auth.enum";
import { IAuth0RefreshTokenBody, IAuth0RefreshTokenResponse } from "../../backend/interfaces";

@Service("auth0.service")
export class Auth0Service extends BaseService {
  private $http: AxiosInstance;

  constructor() {
    super();

    this.$http = axios.create({ baseURL: AUTH0_INTERNAL_ISSUER_URL.slice(0, -1) });
  }

  /**
   * @summary Refresh access token using auth0 API
   *
   * @param {string} refreshToken
   *
   * @returns {Promise<IAuth0RefreshTokenResponse>}
   */
  @Trace()
  public async refreshAccessToken(refreshToken: string): Promise<IAuth0RefreshTokenResponse> {
    const params: IAuth0RefreshTokenBody = {
      client_id: AUTH0_CLIENT_ID,
      client_secret: AUTH0_CLIENT_SECRET,
      grant_type: Auth0Enum.refreshTokenGrantType,
      scope: AUTH0_SCOPES,
      refresh_token: refreshToken
    };
    const response: AxiosResponse<IAuth0RefreshTokenResponse> = await this.$http.post("/oauth/token", params);
    return response.data;
  }
}
