import { Failure } from "@efuse/contracts";
import {
  DISCORD_API_URL,
  DISCORD_CLIENT_ID,
  DISCORD_CLIENT_SECRET,
  DISCORD_REDIRECT_URI,
  FACEBOOK_CLIENT_ID,
  FACEBOOK_CLIENT_SECRET,
  TWITCH_CLIENT_ID,
  TWITCH_REDIRECT_URL,
  TWITCH_SECRET
} from "@efuse/key-store";
import bcrypt from "bcryptjs";
import { Types } from "mongoose";

import axios from "axios";
import fetch from "node-fetch";
import FormData from "form-data";
import FB from "fb";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { SecureService } from "../secure/secure.service";

const SALT = 10;

@Service("auth.service")
export class AuthService extends BaseService {
  private $secureService: SecureService;

  constructor() {
    super();

    this.$secureService = new SecureService();
  }

  /**
   * @summary Get password of the user
   *
   * @param {Types.ObjectId | string}
   *
   * @returns {Promise<string>}
   */
  @Trace()
  public async getPassword(userId: Types.ObjectId | string): Promise<string> {
    const secureObject = await this.$secureService.findOne(
      { user: String(userId) },
      { _id: 1, password: 1 },
      { lean: true }
    );

    if (!secureObject) {
      throw Failure.BadRequest(`[user: ${String(userId)}] | Password Hash not found`);
    }

    return secureObject.password;
  }

  /**
   * @summary Match password of user
   *
   * @param {Types.ObjectId | string} userId - Id of user
   * @param {string} password - Password to match
   *
   * @return {Promise<boolean>}
   */
  @Trace()
  public async matchPassword(userId: Types.ObjectId | string, password: string): Promise<boolean> {
    const passwordHash = await this.getPassword(userId);

    const isMatched = bcrypt.compareSync(password, passwordHash);

    return isMatched;
  }

  /**
   * @summary Check password history to see if password can be updated
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} password
   *
   * @return {Promise<boolean>}
   */
  @Trace()
  public async matchPasswordHistory(userId: Types.ObjectId | string, password: string): Promise<boolean> {
    const secureObject = await this.$secureService.findOne(
      { user: userId },
      { _id: 1, password: 1, history: 1 },
      { lean: true }
    );

    if (!secureObject) {
      throw Failure.BadRequest("Password not found");
    }

    let isMatched = false;
    if (secureObject.history?.length > 0) {
      // Build an array of password hashes
      const passwordsToBeMatched: string[] = [secureObject.password, ...secureObject.history.map((h) => h.password)];

      for (const passwordHash of passwordsToBeMatched) {
        isMatched = bcrypt.compareSync(password, passwordHash);

        // Break loop when password is matched
        if (isMatched) {
          break;
        }
      }
    }

    return isMatched;
  }

  /**
   * @summary Update password of a user
   *
   * @param {Types.ObjectId | string} userId - Id of user
   * @param {string} password - Password to set
   * @return {Promise<void>}
   */
  @Trace()
  public async updatePassword(userId: Types.ObjectId | string, password: string): Promise<void> {
    const passwordHash = bcrypt.hashSync(password, SALT);

    const secureObject = await this.$secureService.findOne({ user: userId });

    if (!secureObject) {
      // Ideally it should never be empty on password reset
      this.logger?.error(`Secure object for user ${String(userId)} not found while updating password`);
      throw Failure.BadRequest("Something went wrong. Please try again");
    }

    // Set new password & push old password to history
    secureObject.history.push({
      password: secureObject.password,
      createdAt: secureObject.updatedAt || new Date(),
      updatedAt: new Date()
    });
    secureObject.password = passwordHash;

    await secureObject.save();
  }

  /**
   * @summary discord authentication
   * @param {string} code - auth code
   *
   * @return {Promise<any>}
   */
  @Trace()
  public async authenticateDiscord(code: string): Promise<any> {
    const data = new FormData();
    data.append("client_id", DISCORD_CLIENT_ID);
    data.append("client_secret", DISCORD_CLIENT_SECRET);
    data.append("grant_type", "authorization_code");
    data.append("redirect_uri", DISCORD_REDIRECT_URI);
    data.append("scope", "identify email guilds");
    data.append("code", code);

    this.logger?.info("Exchanging code with token");
    const response = await fetch(`${DISCORD_API_URL}oauth2/token`, {
      method: "POST",
      body: data
    });
    const responseData = await response.json();
    this.logger?.info(responseData, "Received response when code is exchanged with token");

    // If response contains error
    if (responseData.error) {
      throw Failure.BadRequest("Error while exchanging code with token");
    }

    const discordAccessToken = responseData.access_token;
    const discordTokenType = responseData.token_type;
    const discordRefreshToken = responseData.refresh_token;
    const discordTokenScope = responseData.scope;
    const expsIn = +responseData.expires_in || 0;
    const discordTokenExpiresAt = new Date(Date.now() + expsIn * 1000);

    this.logger?.info("Fetching my profile info");
    const currentDiscordUser = await axios.get(`${DISCORD_API_URL}users/@me`, {
      headers: { Authorization: `Bearer ${responseData.access_token}` }
    });

    return {
      discordVerified: true,
      discordUserId: currentDiscordUser.data.id,
      discordUserName: currentDiscordUser.data.username,
      discordUserUniqueId: `${currentDiscordUser.data.username}#${currentDiscordUser.data.discriminator}`,
      discordAccessToken,
      discordTokenType,
      discordRefreshToken,
      discordTokenScope,
      discordTokenExpiresAt
    };
  }

  /**
   * @summary authenticate facebook
   * @param {string} accessToken - Access Token
   *
   * @return {Promise<any>}
   */
  @Trace()
  public async authenticateFacebook(accessToken: string): Promise<{ facebookVerified: boolean; facebookID: string }> {
    const FBClient = new FB.Facebook({
      appId: FACEBOOK_CLIENT_ID,
      appSecret: FACEBOOK_CLIENT_SECRET
    });
    FBClient.setAccessToken(accessToken);
    return new Promise((resolve) => {
      FBClient.api("me", (response) => {
        if (!response || response.error) {
          this.logger?.error(response.error);
          throw Failure.BadRequest(response.error);
        }
        resolve({
          facebookVerified: true,
          facebookID: response.id
        });
      });
    });
  }

  /**
   * @summary authenticate twitch
   * @param {string} code - twitch code
   *
   * @return {Promise<>}
   */
  public async twitchAuth(code: string): Promise<any> {
    const response = await axios.post(
      `https://id.twitch.tv/oauth2/token?client_id=${TWITCH_CLIENT_ID}&client_secret=${TWITCH_SECRET}&code=${code}&grant_type=authorization_code&redirect_uri=${TWITCH_REDIRECT_URL}`
    );

    const { access_token, refresh_token, expires_in, scope, token_type } = response.data;

    const tokenExpiresAt = new Date(Date.now() + Number(expires_in) * 1000);

    this.logger?.info("Fetching user profile from twitch");
    const userData = await axios.get("https://api.twitch.tv/helix/users", {
      headers: {
        Authorization: `Bearer ${access_token}`,
        "Client-ID": TWITCH_CLIENT_ID
      }
    });
    this.logger?.info(userData.data, "Received user's profile from twitch");

    const [twitchData] = userData.data.data;

    return {
      twitchVerified: true,
      twitchID: twitchData.id,
      twitchUserName: twitchData.login,
      twitchDisplayName: twitchData.display_name,
      twitchProfileImage: twitchData.profile_image_url,
      twitchAccessToken: access_token,
      twitchRefreshToken: refresh_token,
      twitchAccessTokenExpiresAt: tokenExpiresAt,
      twitchScope: scope,
      twitchTokenType: token_type
    };
  }
}
