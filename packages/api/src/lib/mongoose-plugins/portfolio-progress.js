const { calculatePortfolioProgress } = require("../users/portfolio-progress");

module.exports = (schema) => {
  async function calculateProgress(next) {
    // checking for doc version because we get only current schema id first time instead of complete user object.
    // todo: investigate
    // @ts-ignore
    if (this.__v !== undefined) {
      // ignoring typing until things are moved to typegoose
      // @ts-ignore
      const progress = calculatePortfolioProgress(this);
      this.portfolioProgress = progress;
      this.portfolioProgressUpdatedAt = Date.now;
    }

    next();
  }

  schema.pre("save", calculateProgress);
  schema.pre("update", calculateProgress);
  schema.pre("updateOne", calculateProgress);
  schema.pre("findOneAndUpdate", calculateProgress);
};
