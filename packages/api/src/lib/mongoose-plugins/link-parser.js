const getUrls = require("get-urls");

const { OpengraphTagsService } = require("../opengraph-tags/opengraph-tags.service");

/**
 * @summary Scrape a url for openGraphTags
 * @param {any} schema  mongoose schema
 */
const linkParser = (schema) => {
  const service = new OpengraphTagsService();

  async function parseLinks(next) {
    // Make sure we only fire job when content is modified
    if (this.isModified("content")) {
      const urls = getUrls(this.content, {
        stripWWW: false,
        sortQueryParameters: false
      });
      this.metadata.sharedLinks = [...urls];

      // todo: investigate
      // @ts-ignore
      await service.createLinksAndFetchOGTags(urls);
    }
    next();
  }

  schema.pre("save", parseLinks);
  schema.pre("update", parseLinks);
};

module.exports.linkParser = linkParser;
