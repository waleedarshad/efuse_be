const { SessionService } = require("../session/session.service");

module.exports = async (schema) => {
  const sessionService = new SessionService();

  async function revokeSession(doc, next) {
    if (doc && doc.status === "Blocked") {
      await sessionService.revokeSessionAsync(doc._id);
    }
    next();
  }

  schema.post("save", revokeSession);
  schema.post("update", revokeSession);
  schema.post("findOneAndUpdate", revokeSession);
};
