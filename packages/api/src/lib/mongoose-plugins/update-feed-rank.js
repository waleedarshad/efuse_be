module.exports = (schema, opts = {}) => {
  async function updateRank(doc, next) {
    if (!doc) {
      return next();
    }

    const { FeedRankService } = require("../lounge/feed-rank.service");

    const featuredRankService = new FeedRankService();

    const { objectTypeField, objectIdField } = opts;
    if (!objectIdField || !objectTypeField) {
      return next();
    }
    if (doc[objectTypeField] === "feeds") {
      await featuredRankService.calculateAndStoreFeedRankAsync(doc[objectIdField]);
    }
    return next();
  }

  schema.post("save", updateRank);
  schema.post("update", updateRank);
  schema.post("findOneAndUpdate", updateRank);
};
