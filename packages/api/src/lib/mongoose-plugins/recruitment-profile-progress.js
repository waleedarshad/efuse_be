module.exports = (schema) => {
  async function updateProfileProgress(next) {
    // importing lib
    const ProfileProgressLib = require("../recruitment/profile-progress");

    // Adding document to queue for async calculation
    await ProfileProgressLib.calculateProgressAsync(this);
    next();
  }

  schema.pre("save", updateProfileProgress);
};
