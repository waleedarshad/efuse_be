import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";
import { BaseModelService } from "../base";
import { DocumentType } from "@typegoose/typegoose";

import { Service, Trace } from "../decorators";
import { GamePlatform, GamePlatformModel } from "../../backend/models/game-platform.model";
import { PlatformEnum } from "@efuse/entities";
import { ObjectId } from "../../backend/types";

@Service("game-platforms.service")
export class GamePlatformsService extends BaseModelService<DocumentType<GamePlatform>> {
  constructor() {
    super(GamePlatformModel);
  }

  @Trace()
  public async getGamePlatforms(userId: ObjectId): Promise<DocumentType<GamePlatform>[]> {
    if (!Types.ObjectId.isValid(userId)) {
      throw Failure.BadRequest("invalid User ID");
    }
    const gameplatforms = await this.find({ user: userId }, { _id: 1, platform: 1, displayName: 1 });

    return gameplatforms;
  }

  /**
   * Add a new GamePlatform
   * @param {String} userId User ID
   * @param {String} platform game platform e.g "battlenet", "psn", "xbl" or "steam"
   * @param {String} displayName display name of user for a specific platform
   * @returns {Object} Newly created GamePlatform Object
   */
  @Trace()
  public async addPlatform(userId: ObjectId, platform: PlatformEnum, displayName: string): Promise<GamePlatform> {
    try {
      const params = {
        user: userId,
        platform,
        displayName
      };

      const existingPlatform = await this.findOne(params, {}, { lean: 1 });

      if (existingPlatform) {
        this.logger?.info(params, "Platform already exists");
        throw Failure.BadRequest("Platform already exists");
      }

      const _params = <DocumentType<GamePlatform>>params;
      const gamePlatform = await this.create(_params);
      this.logger?.info({ userId, platform, displayName }, "Platform added");
      return gamePlatform;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Delete user's game platform
   * @param {String} userId User's ID
   * @param {String} platformId Platform's ID
   * @returns {Object} Deleted game platform object
   */
  @Trace()
  public async deleteGamePlatform(userId: ObjectId, platformId: ObjectId): Promise<GamePlatform> {
    try {
      const gamePlatform = await this.findOne({
        _id: platformId,
        user: userId
      });

      if (!gamePlatform) {
        this.logger?.info({ platformId, userId }, "Gameplatform not found");
        throw Failure.BadRequest("Platform not found");
      }

      await gamePlatform.deleteOne();
      return gamePlatform;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
