import { Types } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { BlockedUser, BlockedUserModel } from "../../backend/models/blocked-user/blocked-user.model";
import { Friend } from "../../backend/models/friend";
import { IUser } from "../../backend/interfaces/user";
import { Service, Trace } from "../decorators";
import { User } from "../../backend/models/User";
import { BaseModelService } from "../base";
import { HomeFeedAsyncService } from "../home-feed/home-feed-async.service";

@Service("blocked-users.service")
export class BlockedUsersService extends BaseModelService<DocumentType<BlockedUser>> {
  private homeFeedAsyncService: HomeFeedAsyncService;
  constructor() {
    super(BlockedUserModel);

    this.homeFeedAsyncService = new HomeFeedAsyncService();
  }

  @Trace()
  public async removeFromFollowers(blockedUser: BlockedUser): Promise<void> {
    const blockerId = Types.ObjectId(blockedUser.blockerId);
    const blockeeId = Types.ObjectId(blockedUser.blockeeId);

    await Friend.deleteMany({
      $or: [
        { follower: blockerId, followee: blockeeId },
        { follower: blockeeId, followee: blockerId }
      ]
    });

    await this.homeFeedAsyncService.removeHomeFeedOnUserBlockAsync(blockeeId, blockerId);
    await this.homeFeedAsyncService.removeHomeFeedOnUserBlockeeAsync(blockeeId, blockerId);
  }

  /**
   * Attempted to determine if either user has blocked the other.
   */
  @Trace()
  public async isUserBlocked(userOne: string, userTwo: string): Promise<boolean> {
    const hiddenUsers = await this.getBlockedOrBlockedByUsers(userOne);
    const isHidden = hiddenUsers.find((user) => String(user) === String(userTwo)) !== undefined;

    return isHidden;
  }

  /**
   * Retrieves a list of user ids that have been blocked by or have blocked the passed userId
   */
  @Trace()
  public async getBlockedOrBlockedByUsers(userId: string): Promise<string[]> {
    const blockedUsers = await this.find(
      {
        $or: [{ blockerId: userId }, { blockeeId: userId }]
      },
      {},
      { lean: 1 }
    );

    return blockedUsers.map((blockedUser) => {
      return String(blockedUser.blockerId) === String(userId) ? blockedUser.blockeeId : blockedUser.blockerId;
    });
  }

  @Trace()
  public async blockedUserInfo(userId: string): Promise<IUser[]> {
    const blockedUsers = await this.find({ blockerId: userId }, {}, { lean: 1 });
    const blockedIds = blockedUsers.map((it) => it.blockeeId);
    return User.find({ _id: { $in: blockedIds } }).select("_id profilePicture name username");
  }
}
