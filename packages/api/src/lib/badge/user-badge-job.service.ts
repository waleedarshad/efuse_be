import { UserStatus, StreakName } from "@efuse/entities";
import { DocumentType } from "@typegoose/typegoose";
import moment from "moment";
import { Types } from "mongoose";

import { UserBadgeService } from "./user-badge.service";
import { Streak, StreakModel } from "../../backend/models/streak";
import { IStreakPopulated, IStreakPopulatedUser, IStreak } from "../../backend/interfaces/streak";
import { BadgeService } from "./badge.service";
import SchemaHelper from "../../backend/helpers/schema_helper";
import { SessionService } from "../session/session.service";
import { ISession, IUserBadge } from "../../backend/interfaces";
import { Badge } from "../../backend/models/badge.model";
import { Service, Trace } from "../decorators";

const STREAK_DURATION_FOR_AFFILIATE_BADGE = 30;
const INACTIVITY_PERIOD_FOR_AFFILIATE_BADGE = 28;

@Service("user-badge-job.service")
export class UserBadgeJobService extends UserBadgeService {
  private $streakModel: StreakModel<IStreak> = Streak;

  private $badgeService: BadgeService;
  private $sessionService: SessionService;

  constructor() {
    super();

    this.$badgeService = new BadgeService();
    this.$sessionService = new SessionService();
  }

  /**
   * @summary CronJob to generate Affiliate Badge for users.
   */
  @Trace({ service: "user-badge-job.service", name: "CreateAffiliateBadgeCron" })
  public static async CreateAffiliateBadgeCron(): Promise<void | undefined> {
    const userBadgeJobService = new UserBadgeJobService();

    try {
      // Fetch Affiliate Badge
      const affiliateBadge: DocumentType<Badge> = await userBadgeJobService.$badgeService.affiliateBadge();

      // Make sure affiliate badge is active
      if (!affiliateBadge.active) {
        userBadgeJobService.logger?.info("Affiliate Badge is not active");
        return;
      }

      // Get users where streak >= 30
      const streaks: IStreakPopulated[] = await userBadgeJobService.$streakModel
        .find({
          isActive: true,
          streakDuration: { $gte: STREAK_DURATION_FOR_AFFILIATE_BADGE },
          streakName: StreakName.DAILY_USAGE
        })
        .populate({ path: "user", select: "_id headerImage profilePicture status deleted" })
        .lean();

      // Exit if no active streaks are found
      if (streaks.length === 0) {
        userBadgeJobService.logger?.info("No user has an active streak >= 30");
        return;
      }

      const bulkWriteOperations: any[] = [];

      // Loop through each streak & start creating/updating badges
      for (const streak of streaks) {
        const user: IStreakPopulatedUser = streak.user as IStreakPopulatedUser;

        // Skip iteration when user is not found
        if (!user) {
          userBadgeJobService.logger?.info("User not found");
          continue;
        }

        // Skip iteration when user is blocked
        if (user.status === UserStatus.BLOCKED) {
          userBadgeJobService.logger?.info(`User ${String(user._id)} is blocked`);
          continue;
        }

        // Skip iteration when user is deleted
        if (user.deleted) {
          userBadgeJobService.logger?.info(`User ${String(user._id)} is deleted`);
          continue;
        }

        // Skip iteration when user doesn't has a profilePicture
        if (user.profilePicture.url === SchemaHelper.fileSchema.url.default) {
          userBadgeJobService.logger?.info(`User ${String(user._id)} has not uploaded profilePicture`);
          continue;
        }

        // Skip iteration when user doesn't has a headerImage
        if (user.headerImage.url === SchemaHelper.fileSchemaDefaultsNoImage.url.default) {
          userBadgeJobService.logger?.info(`User ${String(user._id)} has not uploaded headerImage`);
          continue;
        }

        // Generate user badge
        bulkWriteOperations.push(userBadgeJobService.generateUserBadge(user._id, affiliateBadge._id));
      }

      const response = await userBadgeJobService.$userBadge.bulkWrite(bulkWriteOperations);
      userBadgeJobService.logger?.info(
        {
          upserted: response.upsertedCount,
          updated: response.modifiedCount,
          writeErrors: response.result?.writeErrors
        },
        "User Badges has been generated"
      );
    } catch (error) {
      userBadgeJobService.logger?.error(error, "Error while creating Affiliate Badge for users");
    }
  }

  /**
   * @summary Cronjob to remove affiliate badge from user
   */
  @Trace({ service: "user-badge-job.service", name: "RemoveAffiliateBadgeCron" })
  public static async RemoveAffiliateBadgeCron(): Promise<void> {
    const userBadgeJobService = new UserBadgeJobService();

    try {
      // Fetch Affiliate Badge
      const affiliateBadge: DocumentType<Badge> = await userBadgeJobService.$badgeService.affiliateBadge();

      // Get all affiliate badges assigned to users
      const userBadges: IUserBadge[] = await userBadgeJobService.findAllByBadgeId(affiliateBadge._id);

      // Exit when no badges are found
      if (userBadges.length === 0) {
        userBadgeJobService.logger?.info("No user has an active Affiliate Badge");
        return;
      }

      // Create 28 days window
      const daysAgo: Date = moment().subtract(INACTIVITY_PERIOD_FOR_AFFILIATE_BADGE, "d").startOf("d").toDate();

      const badgesToBeDeleted: Types.ObjectId[] = [];

      // Iterate through badges & remove badge from inactive user
      for (const userBadge of userBadges) {
        // Check if user is active in last 28 days
        const session: ISession | null = await userBadgeJobService.$sessionService.findOne(
          { user: userBadge.user, createdAt: { $gte: daysAgo } },
          { _id: 1 },
          { lean: true }
        );

        // Remove badge when user has been inactive
        if (!session) {
          badgesToBeDeleted.push(userBadge._id);
        }
      }

      // Clear badges
      if (badgesToBeDeleted.length > 0) {
        await userBadgeJobService.$userBadge.deleteMany({ _id: { $in: badgesToBeDeleted } });
      }
    } catch (error) {
      userBadgeJobService.logger?.error(error, "Error while removing Affiliate Badge for users");
    }
  }

  /**
   * @summary Update an existing user badge or create a new one
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} affiliateBadgeId
   *
   * @returns {UserBadge}
   */
  private generateUserBadge(
    userId: Types.ObjectId | string,
    affiliateBadgeId: Types.ObjectId | string
  ): Record<string, unknown> {
    const query = { user: userId, badge: affiliateBadgeId };

    return {
      updateOne: {
        filter: query,
        update: { ...query, updatedAt: Date.now() },
        upsert: true, // create new object if it is not already there
        setDefaultsOnInsert: true, // this would set the schema defaults
        ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
      }
    };
  }
}
