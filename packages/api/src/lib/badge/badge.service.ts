import { FilterQuery, QueryOptions } from "mongoose";
import { Failure } from "@efuse/contracts";
import { BadgeSlug } from "@efuse/entities";
import { DocumentType } from "@typegoose/typegoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { BadgeModel, Badge } from "../../backend/models/badge.model";

@Service("badge.service")
export class BadgeService extends BaseService {
  private $badge = BadgeModel;

  public async findOne(filter: FilterQuery<DocumentType<Badge>>): Promise<DocumentType<Badge> | null>;
  public async findOne(filter: FilterQuery<DocumentType<Badge>>, projection: any): Promise<DocumentType<Badge> | null>;
  public async findOne(
    filter: FilterQuery<DocumentType<Badge>>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<DocumentType<Badge> | null>;
  @Trace()
  public async findOne(
    filter?: FilterQuery<DocumentType<Badge>>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<DocumentType<Badge> | null> {
    const badge = await this.$badge.findOne(filter, projection, options);
    return badge;
  }

  /**
   * @summary Get Affiliate Badge
   *
   * @returns {Promise<DocumentType<Badge>>}
   */
  @Trace()
  public async affiliateBadge(): Promise<DocumentType<Badge>> {
    const badge = await this.findOne({ slug: BadgeSlug.AFFILIATE });

    // Make sure badge is present
    if (!badge) {
      throw Failure.UnprocessableEntity("Affiliate Badge not found");
    }

    return badge;
  }
}
