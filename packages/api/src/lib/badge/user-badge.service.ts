import { Types, QueryOptions, FilterQuery } from "mongoose";
import { Failure } from "@efuse/contracts";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { IPopulatedUserBadge, IUserBadge } from "../../backend/interfaces";
import { UserBadgeModel, UserBadge } from "../../backend/models/user-badge";

@Service("user-badge.service")
export class UserBadgeService extends BaseService {
  protected $userBadge: UserBadgeModel<IUserBadge> = UserBadge;

  public async findOne(filter: FilterQuery<IUserBadge>): Promise<IUserBadge | null>;
  public async findOne(filter: FilterQuery<IUserBadge>, projection: any): Promise<IUserBadge | null>;
  public async findOne(
    filter: FilterQuery<IUserBadge>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IUserBadge | null>;
  @Trace()
  public async findOne(
    filter?: FilterQuery<IUserBadge>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserBadge | null> {
    const userBadge = await this.$userBadge.findOne(filter, projection, options);
    return userBadge;
  }

  public async find(filter: FilterQuery<IUserBadge>): Promise<IUserBadge[]>;
  public async find(filter: FilterQuery<IUserBadge>, projection?: any): Promise<IUserBadge[]>;
  public async find(
    filter: FilterQuery<IUserBadge>,
    projection: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserBadge[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IUserBadge>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserBadge[]> {
    const userBadge = await this.$userBadge.find(filter, projection, options);
    return userBadge;
  }

  /**
   * @summary Get user's badge by ID
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} badgeId
   *
   * @returns {Promise<IUserBadge | null>}
   */
  @Trace()
  public async findOneByUserAndBadgeId(
    userId: Types.ObjectId | string,
    badgeId: Types.ObjectId | string
  ): Promise<IUserBadge | null> {
    // Make sure userId is present
    if (!userId) {
      throw Failure.BadRequest("Missing parameter 'userId'");
    }

    // Make sure badgeId is present
    if (badgeId) {
      throw Failure.BadRequest("Missing parameter `badgeId`");
    }

    // Get User's Badge
    const userBadge: IUserBadge | null = await this.findOne({ user: userId, badge: badgeId });

    return userBadge;
  }

  /**
   * @summary Get all badges by badge id
   *
   * @param {string} badgeId
   *
   * @returns {Promise<IUserBadge[]>}
   */
  @Trace()
  public async findAllByBadgeId(badgeId: string | Types.ObjectId): Promise<IUserBadge[]> {
    // Make sure badgeId is present
    if (!badgeId) {
      throw Failure.BadRequest("Missing parameter `badgeId`");
    }

    // Get User's Badge
    const userBadges: IUserBadge[] = await this.find({ badge: badgeId });

    return userBadges;
  }

  /**
   * @summary Get all the badges for the user
   *
   * @param {string} userId
   *
   * @returns {Promise<IPopulatedUserBadge[]>}
   */
  @Trace()
  public async findAllByUserId(userId: Types.ObjectId | string): Promise<IPopulatedUserBadge[]> {
    // Make sure userId is present
    if (!userId) {
      throw Failure.BadRequest("Missing parameter 'userId'");
    }

    // Get badges for user
    const userBadges: IPopulatedUserBadge[] = (<unknown>(
      await this.find({ user: userId }, {}, { populate: "badge" })
    )) as IPopulatedUserBadge[];

    return userBadges;
  }
}
