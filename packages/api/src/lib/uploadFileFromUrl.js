const { AWS_S3_BUCKET } = require("@efuse/key-store");
const { Logger } = require("@efuse/logger");
const aws = require("aws-sdk");
const axios = require("axios");

const { AWS } = require("../backend/config/aws-instance");

const logger = Logger.create();
const s3 = new AWS.S3();

/**
 * @summary Upload file to s3 from the file URL
 *
 * @param {String} url - url from where file is to be uploaded
 * @param {String} fileName - Name for the file
 * @param {String} uploadDirectory - Directory in bucket where file is to be uploaded
 * @return {Promise<any>} {Location: '', ETag: '', Bucket: '', Key: ''}
 */
const uploadFileFromUrl = async (url, fileName, uploadDirectory = "uploads/media") => {
  return new Promise(async (resolve, reject) => {
    try {
      logger.info(`Downloading file from ${url}`);

      // todo: investigate
      // @ts-ignore
      const response = await axios.get(url, { responseType: "stream" });

      logger.info("Download successful | Uploading to s3 bucket");

      const s3Response = await s3
        .upload({
          ContentType: response.headers["content-type"],
          ContentLength: response.headers["content-length"],
          Body: response.data,
          Bucket: AWS_S3_BUCKET,
          Key: `${uploadDirectory}/${fileName}`,
          ACL: "public-read"
        })
        .promise();
      logger.info(`Uploaded to s3 bucket ${s3Response.Location}`);
      return resolve(s3Response);
    } catch (error) {
      const errMsg = error.message;
      if (error.isAxiosError) {
        logger.info({ errMsg }, `Error while downloading file from ${url}`);
      } else {
        logger.error(errMsg, `Error while uploading file to s3 ${url}`);
      }
      return reject(error);
    }
  });
};

module.exports.uploadFileFromUrl = uploadFileFromUrl;
