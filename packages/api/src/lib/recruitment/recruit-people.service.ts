import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";
import { isEmpty } from "lodash";

import { BaseService } from "../base/base.service";
import { IRecruitmentProfile } from "../../backend/interfaces/recruitment-profile";
import {
  IRecruitPeople,
  IRecruitPeopleRequestBody,
  IRecruitPeopleUpdateBody
} from "../../backend/interfaces/recruit-people";
import { RecruitmentProfileService } from "./recruitment-profile.service";
import { RecruitPeople, RecruitPeopleModel } from "../../backend/models/recruit-people";
import { Service, Trace } from "../decorators";

@Service("recruit-people.service")
export class RecruitPeopleService extends BaseService {
  private readonly recruitPeople: RecruitPeopleModel<IRecruitPeople> = RecruitPeople;
  private recruitmentProfileService: RecruitmentProfileService;

  constructor() {
    super();

    this.recruitmentProfileService = new RecruitmentProfileService();
  }

  /**
   * @summary Create recruit people object
   * @param {Types.ObjectId} currentUserId
   * @param {IRecruitPeopleRequestBody} recruitBody
   *
   * @return {Promise<IRecruitPeople>}
   */
  @Trace()
  public async create(currentUserId: Types.ObjectId, recruitBody: IRecruitPeopleRequestBody): Promise<IRecruitPeople> {
    // Fetch profile for user
    const profile = await this.getProfile(currentUserId);

    const errors: { name?: string; type?: string } = {};

    // Make sure name is in body
    if (isEmpty(recruitBody.name)) {
      errors.name = "name is required";
    }

    // Make sure type is in body
    if (isEmpty(recruitBody.type)) {
      errors.type = "type is required";
    }

    if (!isEmpty(errors)) {
      throw Failure.BadRequest("Make sure all required params are passed", errors);
    }

    const doc = <IRecruitPeople>{
      name: recruitBody.name,
      type: recruitBody.type,
      email: recruitBody.email,
      phone: recruitBody.phone,
      recruitmentProfile: profile._id
    };

    const people = await this.recruitPeople.create(doc);
    return people;
  }

  /**
   * @summary Update recruit people object
   * @param {Types.ObjectId} currentUserId
   * @param {string} recruitPeopleId
   * @param {IRecruitPeopleUpdateBody} recruitBody
   *
   * @return {Promise<IRecruitPeople>}
   */
  @Trace()
  public async update(
    currentUserId: Types.ObjectId,
    recruitPeopleId: string,
    recruitBody: IRecruitPeopleUpdateBody
  ): Promise<IRecruitPeople> {
    // Fetch profile for user
    const profile = await this.getProfile(currentUserId);

    // Make sure it is a valid ID
    this.validateId(recruitPeopleId);

    // Fetch people object
    const people = await this.recruitPeople.findOne({ _id: recruitPeopleId, recruitmentProfile: profile._id });

    // Make sure we have people object
    if (!people) {
      throw Failure.UnprocessableEntity("recruitPeople not found");
    }

    // Set name
    if (!isEmpty(recruitBody.name)) {
      people.name = recruitBody.name as string;
    }

    // Set type
    if (!isEmpty(recruitBody.type)) {
      people.type = recruitBody.type as string;
    }

    // Set phone
    if (!isEmpty(recruitBody.phone)) {
      people.phone = recruitBody.phone as string;
    }

    // Set email
    if (!isEmpty(recruitBody.email)) {
      people.email = recruitBody.email as string;
    }

    // Save & return
    await people.save();
    return people;
  }

  /**
   * @summary Fetch all recruit people objects associated with profile
   *
   * @param {Types.ObjectId} currentUserId
   *
   * @return {Promise<IRecruitPeople[]>}
   */
  @Trace()
  public async getAllPeople(currentUserId: Types.ObjectId): Promise<IRecruitPeople[]> {
    // Fetch profile for user
    const profile = await this.getProfile(currentUserId);

    // Fetch all people objects
    const people = await this.recruitPeople.find({ recruitmentProfile: profile._id });
    return people;
  }

  /**
   * @summary Delete a particular people object
   *
   * @param {Types.ObjectId} currentUserId
   * @param {string} recruitPeopleId
   *
   * @return {Promise<IRecruitPeople | null>}
   */
  @Trace()
  public async delete(currentUserId: Types.ObjectId, recruitPeopleId: string): Promise<IRecruitPeople | null> {
    // Fetch profile for user
    const profile = await this.getProfile(currentUserId);

    // Delete people object
    const people = await this.recruitPeople.findOneAndDelete({ _id: recruitPeopleId, recruitmentProfile: profile._id });
    return people;
  }

  /**
   * @summary Fetch profile for current user or raise exception
   *
   * @param {Types.ObjectId} currentUserId
   *
   * @return {Promise<IRecruitmentProfile>}
   */
  @Trace()
  private async getProfile(currentUserId: Types.ObjectId): Promise<IRecruitmentProfile> {
    const profile: IRecruitmentProfile | null = await this.recruitmentProfileService.getRecruitmentProfile(
      currentUserId
    );

    if (!profile) {
      throw Failure.Forbidden("You are not allowed to perform this action");
    }

    return profile;
  }

  @Trace()
  private validateId(id: string): void {
    if (!Types.ObjectId.isValid(id)) {
      throw Failure.BadRequest("id is malformed");
    }
  }
}
