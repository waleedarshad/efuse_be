export enum RecruitmentGames {
  LeagueOfLegends = "league-of-legends",
  Valorant = "valorant",
  AimLab = "aim-lab"
}
