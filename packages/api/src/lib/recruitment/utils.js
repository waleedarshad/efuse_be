const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/recruitment/utils" });
const createError = require("http-errors");
const { isEmpty } = require("lodash");
const { ObjectId } = require("mongodb");

const { FeatureFlagService } = require("../../lib/feature-flags/feature-flag.service");

/**
 * @summary Check whether flag user_recruiting is turned on for user
 *
 * @param {ObjectId} currentUserId - ID of recruiter
 */
const canRecruit = async (currentUserId) => {
  const enableRecruitment = await FeatureFlagService.hasFeature("user_recruiting", String(currentUserId));
  if (!enableRecruitment) {
    throw createError(403, "You are not authorized to perform this action");
  }
};

/**
 * @summary Parse sort object
 *
 * @param {String} sort - Stringified JSON object
 * @return {Object} - Sort object i.e {createdAt: -1}
 */
const getParsedSort = (sort) => {
  // Parse sort
  let parsedSort = { createdAt: -1 };
  if (!isEmpty(sort)) {
    try {
      parsedSort = JSON.parse(sort);
    } catch {
      logger.error({ sort }, "Error while parsing sort | Fallback to default { createdAt: -1 }");
    }
  }

  return parsedSort;
};

const getParsedFilters = (filters) => {
  // Parse filters
  let parsedFilters = {};
  if (!isEmpty(filters)) {
    try {
      parsedFilters = JSON.parse(filters);
    } catch {
      logger.error({ filters }, "Error while parsing filters | Fallback to default {}");
    }
  }
  return parsedFilters;
};

module.exports.canRecruit = canRecruit;
module.exports.getParsedSort = getParsedSort;
module.exports.getParsedFilters = getParsedFilters;
