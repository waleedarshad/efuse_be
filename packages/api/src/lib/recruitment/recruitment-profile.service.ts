import { Failure } from "@efuse/contracts";
import { isEmpty } from "lodash";
import { Types, Model, PaginateResult, FilterQuery, QueryOptions } from "mongoose";

import { FavoriteRelatedModel } from "@efuse/entities";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

import { RecruitmentProfile, RecruitmentProfileModel } from "../../backend/models/recruitment-profile";
import { IRecruitmentProfile } from "../../backend/interfaces/recruitment-profile";
import { User } from "../../backend/models/User";

import { canRecruit, getParsedSort, getParsedFilters } from "./utils";
import * as UserLib from "../users";
import { IUser } from "../../backend/interfaces/user";
import { IRecruitAction, ACTION } from "../../backend/interfaces/recruit/recruit-action";

import NotificationsHelper from "../../backend/helpers/notifications.helper";

import { RecruitActionSelfService } from "./recruit/recruit-action-self.service";
import { RecruitActionOrganizationService } from "./recruit/recruit-action-organization.service";
import { RecruitmentGames } from "./recruitment-enums";
import { IFavorite } from "../../backend/interfaces/favorite";
import { FavoritesService } from "../favorites/favorites.service";

@Service("recruitment-profile.service")
export class RecruitmentProfileService extends BaseService {
  private readonly recruitmentProfiles: RecruitmentProfileModel<IRecruitmentProfile> = RecruitmentProfile;
  private readonly users: Model<IUser> = User;

  private favoriteService: FavoritesService;
  private recruitActionSelfService: RecruitActionSelfService;
  private recruitActionOrganizationService: RecruitActionOrganizationService;

  constructor() {
    super();

    this.favoriteService = new FavoritesService();
    this.recruitActionSelfService = new RecruitActionSelfService();
    this.recruitActionOrganizationService = new RecruitActionOrganizationService();
  }

  /**
   * @summary Create or Update RecruitmentProfile
   *
   * @param {IRecruitmentProfile} profileParams - params
   * @return {Promise<IRecruitmentProfile>} Created or updated RecruitmentProfile Object
   */
  @Trace()
  public async manageProfile(profileParams: IRecruitmentProfile): Promise<IRecruitmentProfile> {
    const recruitmentProfile = await this.findOne({ user: profileParams.user });

    if (!recruitmentProfile) {
      // Create new RecruitmentProfile
      const newProfile: IRecruitmentProfile = await this.recruitmentProfiles.create(profileParams);
      if (process.env.NODE_ENV === "production") {
        await NotificationsHelper.sendNewRecruitedEmail("pipeline_submissions@efuse.io", newProfile.user);
      }

      return newProfile;
    }

    // Check if empty params are posted
    const { ...body } = profileParams;
    if (isEmpty(body)) {
      return recruitmentProfile;
    }

    // Set Params to existing object
    for (const param of Object.keys(profileParams)) {
      recruitmentProfile[param] = profileParams[param];
    }

    // Update & return profile
    await recruitmentProfile.save();
    return recruitmentProfile;
  }

  /**
   * @summary Get RecruitmentProfile of a user
   *
   * @param {Types.ObjectId} userId - ID of user
   * @return {Promise<IRecruitmentProfile | null>} RecruitmentProfile Object
   */
  @Trace()
  public async getRecruitmentProfile(userId: Types.ObjectId): Promise<IRecruitmentProfile | null> {
    const profile = await this.findOne({ user: userId });
    return profile;
  }

  /**
   * @summary Get paginated collection of Recruitment Profiles
   *
   * @param {Types.ObjectId} currentUserId - ID of recruiter
   * @param {string | null} organizationId
   * @param {string} sort - a valid stringified json object i.e JSON.stringify(sort)
   * @param {string} filters - a valid stringified json object i.e JSON.stringify(filters)
   * @param {number} page - Page number
   * @param {number} limit - Number of records per page
   *
   * @return {Promise<PaginateResult<IRecruitmentProfile>>} {docs: [RecruitmentProfile], ...paginationInformation}
   */
  @Trace()
  public async getRecruitmentProfiles(
    currentUserId: Types.ObjectId,
    organizationId: string | null,
    sort: string,
    filters: string,
    page = 1,
    limit = 10
  ): Promise<PaginateResult<IRecruitmentProfile>> {
    // Check user_recruiting flag is on for user
    // todo: investigate
    // @ts-ignore
    await canRecruit(currentUserId);

    // Parse filters
    const parsedFilters: FilterQuery<IRecruitmentProfile> = getParsedFilters(
      filters
    ) as FilterQuery<IRecruitmentProfile>;

    // this section is a big wonky. it is here to allow us to search by user info AND by profile info
    // as well as still filtering on the exact matches (graduation class, country, etc..)
    if (parsedFilters?.$text?.$search) {
      const searchText: string = parsedFilters.$text.$search;

      // do a text search in the users table for any users that have a username, email,
      // first, or last name that matches the search text.
      const users = await this.users.find(
        {
          $or: [{ username: searchText }, { email: searchText }, { name: searchText }]
        },
        { _id: 1 },
        { lean: true }
      );

      if (users?.length > 0) {
        // get just the user ids
        const userIds = users.map((user) => user._id.toHexString());

        // instead of doing a text search on the recruitment profile we are going to do an
        // or query between the text search and a match on the user ids.

        (<any>parsedFilters).$or = [{ $text: parsedFilters.$text }, { user: { $in: userIds } }];

        // delete the text search so we don't restrict by that.
        delete parsedFilters.$text;
      }
    }

    // Parse sort
    const parsedSort = sort ? <object>getParsedSort(sort) : { rank: 1 };

    const aggregate = this.recruitmentProfiles.aggregate(this.buildAggregateQuery(parsedFilters));
    // Get paginated collection
    const paginatedRecruitmentProfiles = await this.recruitmentProfiles.aggregatePaginate(aggregate, {
      page,
      limit,
      sort: parsedSort,
      lean: true
    });

    // Lookup for associated objects
    const recruitmentsCollection: Promise<IRecruitmentProfile | unknown>[] = [];

    for (const recruitmentProfile of paginatedRecruitmentProfiles.docs) {
      recruitmentsCollection.push(
        this.buildRecruitmentProfileObject(recruitmentProfile, currentUserId, organizationId)
      );
    }

    // Resolve promises concurrently
    const recruitmentProfiles = await Promise.all(recruitmentsCollection);

    // Removing empty records
    paginatedRecruitmentProfiles.docs = recruitmentProfiles.filter((doc) => !isEmpty(doc)) as IRecruitmentProfile[];
    return paginatedRecruitmentProfiles;
  }

  /**
   * @summary Get paginated collection of starred Recruitment Profiles
   *
   * @param {Types.ObjectId} currentUserId - ID of recruiter
   * @param {string | null} organizationId
   * @param {string} sort - a valid stringified json object i.e JSON.stringify(sort)
   * @param {string} filters - a valid stringified json object i.e JSON.stringify(filters)
   * @param {number} page - Page number
   * @param {number} limit - Number of records per page
   *
   * @return {Promise<PaginateResult<IRecruitmentProfile>>} {docs: [RecruitmentProfile], ...paginationInformation}
   */
  @Trace()
  public async getStarredRecruitmentProfiles(
    currentUserId: Types.ObjectId,
    organizationId: string | null,
    sort: string,
    filters: string,
    page = 1,
    limit = 10
  ): Promise<PaginateResult<IRecruitmentProfile>> {
    // Check user_recruiting flag is on for user
    // todo: investigate
    // @ts-ignore
    await canRecruit(currentUserId);

    // Parse filters
    const parsedFilters: FilterQuery<IRecruitmentProfile> = getParsedFilters(
      filters
    ) as FilterQuery<IRecruitmentProfile>;

    // Parse sort
    const parsedSort = <object>getParsedSort(sort);

    const filter: FilterQuery<IFavorite> = {
      owner: organizationId || currentUserId,
      relatedModel: FavoriteRelatedModel.RECRUITMENT_PROFILES
    };

    const foundFavoriteRecruits = await this.favoriteService.find(filter);
    const recruitmentProfileIds = foundFavoriteRecruits.map((sr) => sr.relatedDoc);

    // Get starred profiles
    const paginatedStarredProfiles = await RecruitmentProfile.paginate(
      {
        _id: { $in: recruitmentProfileIds },
        $or: [{ isHidden: false }, { isHidden: { $exists: false } }],
        ...parsedFilters
      },
      { page, limit, sort: parsedSort, lean: true }
    );

    // Fetch associated objects
    const recruitmentPromises: Promise<IRecruitmentProfile | unknown>[] = [];

    for (const recruitmentProfile of paginatedStarredProfiles.docs) {
      // Set starred to true
      recruitmentProfile.starred = true;

      // Build object
      recruitmentPromises.push(
        this.buildRecruitmentProfileObject(recruitmentProfile, currentUserId, organizationId, false)
      );
    }

    // Resolving promises concurrently
    const recruitmentProfiles = await Promise.all(recruitmentPromises);

    // Set docs & return collection
    paginatedStarredProfiles.docs = recruitmentProfiles.filter((rp) => !isEmpty(rp)) as IRecruitmentProfile[];
    return paginatedStarredProfiles;
  }

  public async findOne(filter: FilterQuery<IRecruitmentProfile>): Promise<IRecruitmentProfile | null>;
  public async findOne(filter: FilterQuery<IRecruitmentProfile>, projection: any): Promise<IRecruitmentProfile | null>;
  public async findOne(
    filter: FilterQuery<IRecruitmentProfile>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IRecruitmentProfile | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IRecruitmentProfile>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IRecruitmentProfile | null> {
    const recruitmentProfile = await this.recruitmentProfiles.findOne(filter, projection, options);
    return recruitmentProfile;
  }

  @Trace()
  public async validateRecruitmentProfile(recruitmentProfileId: Types.ObjectId | string): Promise<void> {
    // Make sure recruitmentProfileId is passed
    if (!recruitmentProfileId) {
      throw Failure.BadRequest("Recruitment profile ID is required");
    }

    // Make sure it is a valid ID
    if (!Types.ObjectId.isValid(recruitmentProfileId)) {
      throw Failure.BadRequest("invalid recruitment profile ID");
    }

    // Make sure corresponding profile exists
    const recruitmentProfile = await this.findOne({ _id: recruitmentProfileId }, { _id: 1 }, { lean: true });

    if (!recruitmentProfile) {
      throw Failure.UnprocessableEntity("recruitment profile not found");
    }
  }

  private buildAggregateQuery(parsedFilters: any): any[] {
    const result = [
      {
        $match: {
          profileProgress: 100,
          $or: [
            {
              isHidden: false
            },
            {
              isHidden: {
                $exists: false
              }
            }
          ],
          primaryGame: {
            $in: [RecruitmentGames.Valorant, RecruitmentGames.AimLab, RecruitmentGames.LeagueOfLegends]
          },
          ...parsedFilters
        }
      },
      {
        $lookup: {
          from: "valorantleaderboard",
          localField: "_id",
          foreignField: "recruitmentProfileId",
          as: "valorantLeaderboard"
        }
      },
      {
        $unwind: {
          path: "$valorantLeaderboard",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: "leagueoflegendsleaderboard",
          localField: "_id",
          foreignField: "recruitmentProfileId",
          as: "lolLeaderboard"
        }
      },
      {
        $unwind: {
          path: "$lolLeaderboard",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $lookup: {
          from: "aimlableaderboard",
          localField: "_id",
          foreignField: "recruitmentProfileId",
          as: "aimlabLeaderboard"
        }
      },
      {
        $unwind: {
          path: "$aimlabLeaderboard",
          preserveNullAndEmptyArrays: true
        }
      },
      {
        $addFields: {
          rank: {
            $switch: {
              branches: [
                {
                  case: {
                    $eq: ["$primaryGame", RecruitmentGames.Valorant]
                  },
                  then: {
                    $ifNull: ["$valorantLeaderboard.rank", "N/A"]
                  }
                },
                {
                  case: {
                    $eq: ["$primaryGame", RecruitmentGames.LeagueOfLegends]
                  },
                  then: {
                    $ifNull: ["$lolLeaderboard.rank", "N/A"]
                  }
                },
                {
                  case: {
                    $eq: ["$primaryGame", RecruitmentGames.AimLab]
                  },
                  then: {
                    $ifNull: ["$aimlabLeaderboard.rank", "N/A"]
                  }
                }
              ],
              default: "N/A"
            }
          }
        }
      }
    ];

    return result;
  }

  /**
   * @summary Lookup & append associated objects
   *
   * @param {IRecruitmentProfile} recruitmentProfile - RecruitmentProfile Object on which associated objects will be appended
   * @param {Types.ObjectId} currentUserId - ID of recruiter
   *
   * @return {Object} - RecruitmentProfile object with all associated objects
   */
  @Trace()
  private async buildRecruitmentProfileObject(
    recruitmentProfile: IRecruitmentProfile,
    currentUserId: Types.ObjectId,
    organizationId: string | null,
    getStarredRecruit = true,
    getRecruitAction = true
  ): Promise<IRecruitmentProfile | unknown> {
    const completeProfileObject = recruitmentProfile;

    // Lookup user
    // todo: investigate & fix
    // @ts-ignore
    const associatedUser: IUser | unknown = (await UserLib.getUserById(recruitmentProfile.user, {
      projection: {
        twitchUserName: 1,
        twitchVerified: 1,
        leagueOfLegends: 1,
        rocketleague: 1,
        discordUserUniqueId: 1
      },
      populate: ["twitch", "leagueOfLegends.stats"]
    })) as IUser | unknown;

    // return if associated user is not found
    if (isEmpty(associatedUser)) {
      return {};
    }

    // Append user doc
    completeProfileObject.user = associatedUser as IUser;

    if (getStarredRecruit) {
      const ownerId = organizationId || currentUserId;
      this.logger?.info({ organizationId, currentUserId });
      // Check params are available
      if (isEmpty(recruitmentProfile._id)) {
        throw Failure.BadRequest("recruitmentProfileId is required");
      }

      const filter: FilterQuery<IFavorite> = {
        relatedDoc: <Types.ObjectId | string>recruitmentProfile._id,
        owner: ownerId,
        relatedModel: FavoriteRelatedModel.RECRUITMENT_PROFILES
      };

      const foundFavorite: IFavorite | null = await this.favoriteService.findOne(filter, {}, { lean: true });
      completeProfileObject.starred = !isEmpty(foundFavorite);
    }

    if (getRecruitAction) {
      const recruitAction: IRecruitAction | null = await (organizationId
        ? this.recruitActionOrganizationService.getRecruitAction(recruitmentProfile._id, organizationId, currentUserId)
        : this.recruitActionSelfService.getRecruitAction(recruitmentProfile._id, currentUserId));

      // Append action
      completeProfileObject.recruitAction = recruitAction ? recruitAction.action : ACTION.NO_ACTION;
    }

    return completeProfileObject;
  }
}
