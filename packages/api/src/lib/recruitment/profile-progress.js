const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/recruitment/profile_progress.js" });
const { round } = require("lodash");

const { RecruitmentProfile } = require("../../backend/models/recruitment-profile");

const { ConstantsService } = require("../constants");
const Config = require("../../async/config");

const constantsService = new ConstantsService();

/**
 * @summary Check whether to increment progress or not
 *
 * @param {Object} recruitmentProfile - recruitment profile object
 * @param {String} requiredField - name of the required field
 *
 * @return {Boolean} true/false
 */
const incrementProgress = (recruitmentProfile, requiredField) => {
  // check whether field is filled
  const fieldValue = recruitmentProfile[requiredField];
  return fieldValue && fieldValue.trim().length > 0;
};

/**
 * @summary Calculate the recruitment profile progress
 *
 * @param {Object} recruitmentProfile - recruitment profile where progress is to be calculated
 *
 */
const calculateProgress = async (recruitmentProfile) => {
  try {
    // Fetch constants
    const variables = await constantsService.getVariables("RECRUITMENT_PROFILE");

    // Destructure contants
    const { requiredFieldsUSA, requiredFieldsInternational } = variables;

    // Make sure contants are avaiable
    if (!requiredFieldsUSA || !requiredFieldsInternational) {
      logger.error("Constants are not found!");
      return;
    }

    // Defaults
    let progressCount = 0;
    let progressDoneCount = 0;

    // Get required fields
    const requiredFields =
      recruitmentProfile.country === "United States" ? requiredFieldsUSA : requiredFieldsInternational;

    // Check each required field to increment progress
    for (const requiredField of requiredFields) {
      // increment overall progress
      progressCount += 1;

      // check & increment progress
      if (incrementProgress(recruitmentProfile, requiredField)) {
        progressDoneCount += 1;
      }
    }

    // Calculate progress
    const progressPercentage = round((progressDoneCount / progressCount) * 100);

    logger.info(
      {
        profileId: recruitmentProfile._id,
        name: recruitmentProfile.name,
        oldPercentage: recruitmentProfile.profileProgress,
        progressPercentage
      },
      "Updating profile progress"
    );

    // Update profile
    await RecruitmentProfile.findOneAndUpdate(
      { _id: recruitmentProfile._id },
      // todo: investigate
      // @ts-ignore
      {
        $set: {
          profileProgress: progressPercentage,
          profileProgressUpdatedAt: Date.now()
        }
      }
    );
  } catch (error) {
    logger.error({ error }, "Something went wrong while calculating recruitment profile progress");
  }
};

/**
 * @summary Async Queue
 */
const calculateProgressQueue = Config.initQueue("calculateRecruitmentProfileProgress");

/**
 * @summary Async method to calculate recruitment profile progress
 *
 * @param {Object} recruitmentProfile - recruitmentProfile to calculate progress against
 */

const calculateProgressAsync = (recruitmentProfile) => {
  return calculateProgressQueue.add({ recruitmentProfile });
};

/**
 * @summary Async initializers
 */
const initAsyncProcessors = () => {
  calculateProgressQueue.process((job) => {
    return calculateProgress(job.data.recruitmentProfile);
  });
};

module.exports.calculateProgress = calculateProgress;
module.exports.calculateProgressAsync = calculateProgressAsync;
module.exports.incrementProgress = incrementProgress;
module.exports.initAsyncProcessors = initAsyncProcessors;
