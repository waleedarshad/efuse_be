import { Types, FilterQuery } from "mongoose";

import { isEmpty } from "lodash";
import { Failure } from "@efuse/contracts";
import { BaseService } from "../../base/base.service";
import { Trace, Service } from "../../decorators";

import { canRecruit } from "../utils";

import { IRecruitAction, ACTION } from "../../../backend/interfaces/recruit/recruit-action";
import { RecruitAction, RecruitActionModel } from "../../../backend/models/recruit/recruit-action";

type RecruitActionQuery = FilterQuery<
  Pick<IRecruitAction, "recruiterUser" | "recruitmentProfile" | "ownerType" | "owner" | "action">
>;

@Service("recruit-action.service")
export class RecruitActionService extends BaseService {
  private readonly recruitAction: RecruitActionModel<IRecruitAction> = RecruitAction;
  private ownerType: string;

  constructor(ownerType: string) {
    super();
    this.ownerType = ownerType;
  }

  /**
   * @summary Create Action on Recruit
   *
   * @param {Types.ObjectId} currentUserId - ID of the recruiter
   * @param {string} recruitmentProfile - ID of the profile to perform action
   * @param {string} owner - ID of the owner i.e user or organization who is performing action;
   *
   * @return {Promise<IRecruitAction>}
   */
  @Trace()
  public async createORUpdateRecruitAction(
    currentUserId: Types.ObjectId,
    recruitmentProfileId: string,
    owner: string | Types.ObjectId,
    action: string = ACTION.NO_ACTION
  ): Promise<IRecruitAction> {
    await this.validateRequest(currentUserId, recruitmentProfileId, owner);

    const doc = <IRecruitAction>(<unknown>{
      recruiterUser: currentUserId,
      recruitmentProfile: recruitmentProfileId,
      owner,
      ownerType: this.ownerType,
      action
    });

    const existingDocQuery: RecruitActionQuery = {
      recruitmentProfile: recruitmentProfileId,
      owner,
      ownerType: this.ownerType
    };

    // Find existence
    const existingAction = await this.recruitAction.findOne(<FilterQuery<IRecruitAction>>existingDocQuery);
    if (existingAction) {
      if (existingAction.action === action) return existingAction;
      existingAction.action = action;
      await existingAction.save();
      return existingAction;
    }
    const recruitAction = await this.recruitAction.create(doc);
    return recruitAction;
  }

  /**
   * @summary Get Recruit Action Object
   *
   * @param {Types.ObjectId} recruitmentProfileId - ID of the recruit on which action to be performed
   * @param {string} owner -  ID of the owner i-e user or organization who is performing action
   * @param {Types.ObjectId} currentUserId - ID of the recruiter
   *
   * @returns {Promise<IRecruitAction | null}
   */
  public async getRecruitAction(
    recruitmentProfileId: string,
    organizationId: string | Types.ObjectId,
    currentUserId?: Types.ObjectId
  ): Promise<IRecruitAction | null>;
  @Trace()
  public async getRecruitAction(
    recruitmentProfileId: string,
    owner: string | Types.ObjectId
  ): Promise<IRecruitAction | null> {
    if (isEmpty(recruitmentProfileId)) {
      throw Failure.BadRequest("recruitmentProfileId is required");
    }

    const recruitAction = await this.recruitAction.findOne({
      recruitmentProfile: recruitmentProfileId,
      owner,
      ownerType: this.ownerType
    });
    return recruitAction;
  }

  /**
   * @summary Make sure user is allowed to perform action & params are available
   *
   * @param {Types.ObjectId} currentUserId - ID of the recruiter
   * @param {string} recruitmentProfile - ID of the profile on which action performed
   * @param {string | Types.ObjectId} owner - ID of the owner i-e user or organization who is performing action on recruit profile
   *
   * @return {Promise<void>}
   */
  @Trace()
  private async validateRequest(
    currentUserId: Types.ObjectId,
    recruitmentProfile: string,
    owner: string | Types.ObjectId
  ): Promise<void> {
    // Check if user allowed to recruit
    // todo: investigate
    // @ts-ignore
    await canRecruit(currentUserId);

    // Check recruitmentProfileId is available
    if (isEmpty(recruitmentProfile)) {
      throw Failure.BadRequest("recruitmentProfileId is required");
    }

    // Make sure owner is present
    if (isEmpty(owner)) {
      throw Failure.BadRequest("owner is required");
    }
  }
}
