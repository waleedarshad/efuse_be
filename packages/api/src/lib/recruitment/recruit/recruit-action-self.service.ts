import { RecruitActionService } from "./recruit-action.service";

import { Service } from "../../decorators";

@Service("recruit-action-self.service")
export class RecruitActionSelfService extends RecruitActionService {
  constructor() {
    super("users");
  }
}
