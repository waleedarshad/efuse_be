import { Types } from "mongoose";
import { RecruitActionService } from "./recruit-action.service";

import { Service, Trace } from "../../decorators";
import { OrganizationACLService } from "../../organizations/organization-acl.service";
import { IRecruitAction, ACTION } from "../../../backend/interfaces/recruit/recruit-action";
import { OrganizationAccessTypes } from "../../organizations/organization-access-types";

@Service("recruit-action-organization.service")
export class RecruitActionOrganizationService extends RecruitActionService {
  private organizationACLService: OrganizationACLService;
  constructor() {
    super("organizations");

    this.organizationACLService = new OrganizationACLService();
  }

  @Trace()
  public async createORUpdateRecruitAction(
    currentUserId: Types.ObjectId,
    recruitmentProfileId: string,
    organizationId: string,
    action: string | ACTION.NO_ACTION
  ): Promise<IRecruitAction> {
    await this.organizationACLService.authorizedToAccess(currentUserId, organizationId, OrganizationAccessTypes.ANY);

    const recruitAction = await super.createORUpdateRecruitAction(
      currentUserId,
      recruitmentProfileId,
      organizationId,
      action
    );
    return recruitAction;
  }

  @Trace()
  public async getRecruitAction(
    recruitmentProfileId: string,
    organizationId: string,
    currentUserId: Types.ObjectId
  ): Promise<IRecruitAction | null> {
    await this.organizationACLService.authorizedToAccess(currentUserId, organizationId, OrganizationAccessTypes.ANY);

    const recruitAction = await super.getRecruitAction(recruitmentProfileId, organizationId);
    return recruitAction;
  }
}
