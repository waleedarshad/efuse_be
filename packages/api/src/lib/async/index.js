const _ = require("lodash");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { EFuseAnalyticsUtil } = require("../analytics.util");
const Config = require("../../async/config");

const getQueueByNames = async () => {
  const queueArray = await Config.getAllQueues();
  return Object.keys(queueArray);
};

/**
 * Pause bull queue by name
 * @async
 * @param {string} queueName  Queue Name e.g. "globalSearchQueue"
 */
const pauseQueue = async (queueName) => {
  const queueNames = await getQueueByNames();
  if (!queueNames.includes(queueName)) {
    logger.error(`unable to find queue with name ${queueName}`);
  } else {
    const queue = await Config.getQueue(queueName);
    await queue.pause();
  }
};

/**
 * Resume bull queue by name
 * @async
 * @param {string} queueName  Queue Name e.g. "globalSearchQueue"
 */
const resumeQueue = async (queueName) => {
  const queueNames = await getQueueByNames();
  if (!queueNames.includes(queueName)) {
    logger.error(`unable to find queue with name ${queueName}`);
  } else {
    const queue = await Config.getQueue(queueName);
    await queue.resume();
  }
};

/**
 * List stats for bull queue by name
 * @async
 * @param {string} queueName  Queue Name e.g. "globalSearchQueue"
 */

const listStatsByQueue = async (queueName) => {
  const queueNames = await getQueueByNames();

  if (!queueNames.includes(queueName)) {
    logger.error(`unable to find queue with name ${queueName}`);
  } else {
    const queue = await Config.getQueue(queueName);
    const stats = await queue.getJobCounts();

    return stats;
  }
};

const collectBullStats = async () => {
  const queueNames = await getQueueByNames();
  queueNames.forEach(async (name, index) => {
    if (name) {
      const stats = await listStatsByQueue(name);
      _.forOwn(stats, (value, key) => {
        EFuseAnalyticsUtil.incrementMetric(`async.queue.${key}`, value, [`queueName:${name}`]);
      });
    }
  });
};

module.exports.collectBullStats = collectBullStats;
module.exports.getQueueByNames = getQueueByNames;
module.exports.listStatsByQueue = listStatsByQueue;
module.exports.pauseQueue = pauseQueue;
module.exports.resumeQueue = resumeQueue;
