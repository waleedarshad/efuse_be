export enum HypeTypeEnum {
  FEED = "feeds",
  COMMENT = "comments"
}

export enum HypePaginationLimitEnum {
  DEFAULT = 10,
  MAX = 25
}
