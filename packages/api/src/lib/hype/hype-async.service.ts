import { Failure } from "@efuse/contracts";
import { Queue } from "bull";
import { Types, Model } from "mongoose";
import { init as flagSmithInit, setTrait as flagSmithSetTrait } from "flagsmith-nodejs";

import { ObjectId } from "../../backend/types";
import { BaseService } from "../base/base.service";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";
import { HypeService } from "./hype.service";
import { IFeed } from "../../backend/interfaces/feed";

import { io } from "../../backend/config/get-io-object";
import { IUser } from "../../backend/interfaces/user";
import { IUserStats } from "../../backend/interfaces/user-stats";
import { sendNotification } from "../notifications/push";
import { Service, Trace } from "../decorators";
import { UserService } from "../users/user.service";
import { UserStatsService } from "../user-stats/user-stats.service";
import Config from "../../async/config";
import { Feed } from "../../backend/models/Feed";
import { UserStats } from "../../backend/models/user-stats";
import NotificationsHelper from "../../backend/helpers/notifications.helper";
import { BrazeCampaignName } from "../braze/braze.enum";
import { BrazeService } from "../braze/braze.service";
import { HomeFeedService } from "../home-feed/home-feed.service";
import { BULLET_TRAIN_ENVIRONMENT } from "@efuse/key-store";
import { Hype } from "../../backend/models/hype/hype.model";

@Service("hype-async.service")
export class HypeAsyncService extends BaseService {
  private $updateTotalHypesQueue: Queue;
  private $hypedPostNotifyAsyncQueue: Queue;
  private $updateHypeScoreAsyncQueue: Queue;

  private $hypeService: HypeService;
  private $userStatsService: UserStatsService;
  private $userService: UserService;
  private $brazeService: BrazeService;
  private $homeFeedService: HomeFeedService;

  private $feedModel: Model<IFeed> = Feed;

  constructor() {
    super();

    this.$updateTotalHypesQueue = Config.initQueue("updateTotalHypes");
    this.$hypedPostNotifyAsyncQueue = Config.initQueue("hypedPostNotify");
    this.$updateHypeScoreAsyncQueue = Config.initQueue("updateHypeScoreAsync", {
      // In order to limit crushing the database, only allow 3 per second
      limiter: { max: 3, duration: 1000 }
    });

    this.$hypeService = new HypeService();
    this.$userStatsService = new UserStatsService();
    this.$userService = new UserService();
    this.$brazeService = new BrazeService();
    this.$homeFeedService = new HomeFeedService();
  }

  /**
   * @summary Update hype score
   *
   * @param {ObjectId} userId - Id of the user
   * @return {Promise<number | undefined>} hypeScore
   */
  private async updateHypeScore(userId: ObjectId): Promise<number | undefined> {
    this.logger?.info({ userId }, "updateHypeScore starting");
    const calculatedScoreData = await this.$hypeService.calculateHypeScore(userId);

    if (!calculatedScoreData) {
      this.logger?.info({ userId }, "Unable to update hype score. No calculated score data returned.");
      return;
    }

    const { hypeScore, postViews, totalFeedComments, totalFeedCommentHypes, totalHypes } = calculatedScoreData;
    /**
     * Wrapped in try/catch to make sure if any thing is wrong with BT then it doesn't
     * break hype score
     */
    // send hypeScore as trait to BT
    try {
      flagSmithInit({ environmentID: BULLET_TRAIN_ENVIRONMENT });
      flagSmithSetTrait(String(userId), "hypeScore", hypeScore);
    } catch (error) {
      this.logger?.info({ error, userId }, "Unable to send hypeScore to flag smith");
    }

    const user = await this.$userService.findOne({ _id: userId }, { _id: 1, stats: 1 });

    if (!user) {
      this.logger?.info({ userId }, "Unable to update hype score. Invalid user ID");
      return;
    }

    let stats = await UserStats.findOne({ user: user.id });

    if (!stats) {
      stats = new UserStats();
      user.stats = stats;
      await user.save();
    }

    if (hypeScore !== stats.hypeScore) {
      stats.user = user.id;
      stats.hypeScore = hypeScore;
      // TODO(shuff) -> Come up with a hypeScorePercentile calculation
      // stats.hypeScorePercentile =

      stats.hypeScoreUpdatedAt = new Date();
      stats.postViews = postViews;
      stats.totalEngagements = +totalHypes + +totalFeedComments + +totalFeedCommentHypes;
      await stats.save();

      this.logger?.info({ stats }, `Updating hypeScore for user to ${hypeScore} `);
    } else {
      this.logger?.info({ stats }, "hypeScore unchanged");
    }

    return hypeScore;
  }

  /**
   * @summary Update hype score of user asynchronously using bull queue
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateHypeScoreAsync(userId: ObjectId): Promise<void> {
    await this.$updateHypeScoreAsyncQueue.add({ userId });
  }

  /**
   * @summary Update total hypes of the author
   *
   * @param {Types.ObjectId | string} hypeId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateTotalHypes(hypeId: Types.ObjectId | string): Promise<void> {
    try {
      const hype: Hype | null = (await this.$hypeService.findOne(
        { _id: hypeId },
        { _id: 1, hypeCount: 1, objId: 1, objType: 1 },
        // todo: investigate correct usage, is this from a plugin?
        <never>{ autopopulate: false, populate: "objId", lean: true }
      )) as Hype | null;

      if (!hype) {
        throw Failure.UnprocessableEntity(`Hype object ${hypeId as string} not found`);
      }

      const userId = hype.objId.user;

      const userStats = await this.$userStatsService.updateOrCreate(
        { user: userId },
        <Partial<IUserStats>>{ $inc: { totalHypes: hype.hypeCount } },
        <IUserStats>{ totalHypes: hype.hypeCount, user: userId }
      );

      await this.$userService.update(userId, <Partial<IUser>>(<unknown>{ $set: { stats: userStats._id } }));
    } catch (error) {
      this.logger?.error(error, "Error while updating total hypes for user");
    }
  }

  /**
   * @summary Update total hypes of the author asynchronously using bull queue
   *
   * @param {Types.ObjectId | string} hypeId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateTotalHypesAsync(hypeId: Types.ObjectId | string): Promise<void> {
    await this.$updateTotalHypesQueue.add({ hypeId });
  }

  /**
   * @summary Created the message, title and type of the notification and called the method to send notification to the post author
   *
   * @param {Types.ObjectId | string} postHyperId - Id of the user who hyped the post
   * @param {Types.ObjectId | string} postCreatorId - Id of the post author
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async hypedPostNotify(
    postHyperId: Types.ObjectId | string,
    postCreatorId: Types.ObjectId | string
  ): Promise<void> {
    if (String(postHyperId) === String(postCreatorId)) {
      this.logger?.info("User is hyping own post. Not sending notification");
      return;
    }

    const notificationsEnabled = await FeatureFlagService.hasFeature("hyped_post_notification", String(postCreatorId));

    if (notificationsEnabled) {
      const user: IUser | null = await this.$userService.findOne({ _id: postHyperId }, { username: 1 });

      if (!user) {
        throw Failure.UnprocessableEntity(`User ${String(postHyperId)} not found`);
      }

      const title = "YOUR POST WAS HYPED";
      const message = `<div class="username"> ${user.username}</div> \n <span class="action">hyped</span> your post`;
      await sendNotification(postCreatorId, title, message, "hype");
    }
  }

  /**
   * @summary Created the message, title and type of the notification and called the method to send notification to the post author using bull queue
   *
   * @param {Types.ObjectId | string} postHyperId - Id of the user who hyped the post
   * @param {Types.ObjectId | string} postCreatorId - Id of the post author
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public hypedPostNotifyAsync(postHyperId: Types.ObjectId | string, postCreatorId: Types.ObjectId | string): void {
    this.$hypedPostNotifyAsyncQueue.add({ postHyperId, postCreatorId }).catch((error) => {
      this.logger?.error(error, "Error while adding job queue hypedPostNotifyAsyncQueue");
    });
  }

  /**
   * @summary Given a feedId and hypingUserId, it will send a hype notification to the owner of the feed
   *
   * @param {Types.ObjectId | string} feedId - The ID for a feed to retrieve hypes for
   * @param {Types.ObjectId | string} hypingUserId - The userId of the person hyping
   * @param {Hype} hype
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async sendHypeNotificationToPostCreator(
    feedId: Types.ObjectId | string,
    hypingUserId: Types.ObjectId | string,
    hype: Hype
  ): Promise<void> {
    const feed: IFeed | null = await this.$feedModel.findOne(
      { _id: feedId },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false }
    );

    if (!feed) {
      throw Failure.UnprocessableEntity(`Feed ${String(feedId)} not found`);
    }

    const sender: IUser | null = await this.$userService.findOne(
      { _id: hypingUserId },
      { _id: 1, name: 1, username: 1 },
      { lean: true }
    );

    if (!sender) {
      throw Failure.UnprocessableEntity(`User ${String(hypingUserId)} not found`);
    }

    const homeFeed = await this.$homeFeedService.findOne(
      { feed: String(feed._id), user: feed.user },
      { _id: 1 },
      { autopopulate: false }
    );

    if (!homeFeed) {
      throw Failure.UnprocessableEntity("Home feed not found");
    }

    this.hypedPostNotifyAsync(hypingUserId, feed.user);

    await NotificationsHelper.hypeNotification(sender, feed.user, hype, "post", feedId);

    if (String(sender._id) !== String(feed.user)) {
      this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.POST_HYPE, {
        trigger_properties: {
          hyping_username: sender.username,
          hyping_user_id: String(sender._id),
          feed_id: String(homeFeed._id),
          hype_id: String(hype._id)
        },
        recipients: [{ external_user_id: String(feed.user) }]
      });
    }
  }

  /**
   * Given a feedId and userId, it will retrieve hypes for a feed and emit socket events
   *
   * @param {Types.ObjectId | string} feedId - The ID for a feed to retrieve hypes for
   * @param {Types.ObjectId | string} hypingUserId - The userId of the person hyping
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async emitFeedHypesUpdate(
    feedId: Types.ObjectId | string,
    hypingUserId: Types.ObjectId | string
  ): Promise<void> {
    const hypes = await this.$hypeService.getPostHypeCount(feedId);
    const userHypes = await this.$hypeService.getPostHypeCount(feedId, hypingUserId);

    io.in(String(hypingUserId)).emit("SET_FEED_USER_HYPES", { feedId, userHypes });
    io.emit("SET_FEED_HYPES", { feedId, hypes });
  }

  /**
   * Async Processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "hype-async.service" })
  public static InitAsyncProcessors(): void {
    const hypeAsyncService = new HypeAsyncService();

    hypeAsyncService.$updateTotalHypesQueue
      .process((job: { data: { hypeId: string } }) => {
        return hypeAsyncService.updateTotalHypes(job.data.hypeId);
      })
      .catch((error) => hypeAsyncService.logger?.error(error, "Error while processing updateTotalHypesQueue"));

    hypeAsyncService.$hypedPostNotifyAsyncQueue
      .process((job: { data: { postHyperId: string; postCreatorId: string } }) => {
        const { postHyperId, postCreatorId } = job.data;
        return hypeAsyncService.hypedPostNotify(postHyperId, postCreatorId);
      })
      .catch((error) => hypeAsyncService.logger?.error(error, "Error while processing hypedPostNotifyAsyncQueue"));

    hypeAsyncService.$updateHypeScoreAsyncQueue
      .process((job: { data: { userId: ObjectId } }) => {
        const { userId } = job.data;
        return hypeAsyncService.updateHypeScore(userId);
      })
      .catch((error) => hypeAsyncService.logger?.error(error, "Error while processing updateHypeScoreAsyncQueue"));
  }
}
