import { PaginateResult } from "mongoose";
import { uniq, sumBy } from "lodash";

import { TokenLedgerAction, TokenLedgerKind, HypeScoreCalculation } from "../../backend/interfaces";
import { Comment } from "../../backend/models/comment";
import { HomeFeed } from "../../backend/models/HomeFeed";
import { Feed } from "../../backend/models/Feed";
import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { Hype, HypeModel } from "../../backend/models/hype/hype.model";
import { TokenLedgerService } from "../token-ledger";
import { HypeEnum } from "@efuse/entities";

import { ObjectId } from "../../backend/types";
import { HypeResponse } from "../../backend/models/hype/hype-response.model";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { MONGO_PREFERENCE_TAGS } from "../../backend/config/constants";

const MAX_HYPE_COUNT = 43;

@Service("hype.service")
export class HypeService extends BaseModelService<DocumentType<Hype>> {
  constructor() {
    super(HypeModel);
  }

  private $hype = HypeModel;
  private tokenLedgerService = new TokenLedgerService();

  /**
   * @summary Create Hype
   * @param {ObjectId} objId
   * @param {ObjectId} userId
   * @param {number} hypeCount
   * @param {string} objType
   * @returns {Promise<HypeResponse>}
   */
  @Trace()
  public async createHype(
    objId: ObjectId,
    userId: ObjectId,
    hypeCount: number,
    objType = HypeEnum.FEED
  ): Promise<HypeResponse> {
    const response = { newHype: false } as HypeResponse;

    if (hypeCount <= 0 || hypeCount > MAX_HYPE_COUNT) {
      this.logger?.error(
        { userId, objId, hypeCount },
        `Invalid number of hypes. They have to be between 1 and ${MAX_HYPE_COUNT}`
      );
      return response;
    }

    // Make sure user has not already hyped
    const doc = { user: userId, objId, objType, hypeCount };
    const query = { user: userId, objId, objType };

    const result = await this.$hype.findOneAndUpdate(
      { user: userId, objId, objType },
      { $setOnInsert: doc },
      { new: true, upsert: true, rawResult: true }
    );

    response.hype = result.value;
    response.newHype = result.lastErrorObject.updatedExisting === false;

    // credit user for hype
    await this.tokenLedgerService.createEntry(TokenLedgerAction.hype, TokenLedgerKind.credit, userId);

    return response;
  }

  /**
   * @summary Create Comment Hype
   * @param {ObjectId} objId
   * @param {ObjectId} userId
   * @param {number} hypeCount
   * @param {string} objType
   * @returns {Promise<HypeResponse>}
   */
  @Trace()
  public async createCommentHype(
    objId: ObjectId,
    userId: ObjectId,
    hypeCount: number,
    platform: string,
    objType = HypeEnum.COMMENT
  ): Promise<HypeResponse> {
    const response = { newHype: false } as HypeResponse;

    const doc = { user: userId, objId, objType, hypeCount, platform };
    const query = { user: userId, objId, objType };

    const result = await this.$hype.findOneAndUpdate(
      query,
      { $setOnInsert: doc },
      { new: true, upsert: true, rawResult: true }
    );

    response.hype = result.value;
    response.newHype = result.lastErrorObject.updatedExisting === false;

    return response;
  }

  /**
   * @summary Given a feedId, return the number of hypes
   *
   * @param {ObjectId} feedId - The ID for a feed in the feeds collection
   * @param {ObjectId} userId - Valid userId
   *
   * @returns {Promise<number>} Number of hypes on feed
   */
  @Trace()
  public async  getPostHypeCount(feedId: ObjectId, userId?: ObjectId): Promise<number> {
    const query = { objId: feedId, objType: "feeds" } as { objId: ObjectId; objType: HypeEnum; user?: ObjectId };

    if (userId) {
      query.user = userId;
    }

    const hypes = await this.find(query, {}, { lean: true });

    const totalHypesCount = hypes.reduce((acc, hype) => acc + (hype.hypeCount || 1), 0);
    return totalHypesCount;
  }

  /**
   * @summary Given commentId, return number of hypes
   *
   * @param commentId - Comment ID
   * @param userId  - UserId who commented
   * @returns {Promise<number>} Number of hypes on comments
   */
  public async getCommentHypeCount(commentId: ObjectId, userId?: ObjectId): Promise<number> {
    const query = { objId: commentId, objType: "comments" } as { objId: ObjectId; objType: HypeEnum; user?: ObjectId };

    if (userId) {
      query.user = userId;
    }

    const hypes = await this.find(query, {}, { lean: true });

    const totalHypesCount = hypes.reduce((acc, hype) => acc + (hype.hypeCount || 1), 0);
    return totalHypesCount;
  }

  public async getPaginatedHypes(
    objId: ObjectId,
    page: number,
    limit: number,
    objType: HypeEnum
  ): Promise<PaginateResult<Hype>> {
    const paginatedHypes = await this.findPaginated(
      {
        objId: GeneralHelper.getObjectId(objId),
        objType,
        deleted: { $ne: true }
      },
      {
        page,
        limit,
        sort: { createdAt: -1 },
        lean: true
      }
    );

    return paginatedHypes;
  }

  /**
   * @summary Calculate hype score
   *
   * @param {ObjectId} userId - Id of the user
   *
   * @return {Promise<HypeScoreCalculation | undefined>} Hype Score Calculation result
   */
  @Trace()
  public async calculateHypeScore(userId: ObjectId): Promise<HypeScoreCalculation | undefined> {
    try {
      // HYPE_MULTIPLIER = 1;
      // COMMENT_MULTIPLIER = 1;
      // VIEWS_MULTIPLIER = 0.001; // 1000 views per point

      this.logger?.info({ userId }, "calculateHypeScore starting");

      const homeFeed = await HomeFeed.find({
        user: userId,
        timelineableType: "users"
      })
        .select("feed")
        .lean();

      const feedIds = uniq(homeFeed.map((hf) => hf.feed));

      const feeds = await Feed.find({
        _id: { $in: feedIds },
        user: userId
      })
        .select("likes comments metadata")
        .lean()
        .read("secondary", MONGO_PREFERENCE_TAGS) // Read from analytics database to not affect prod
        .cache(600);

      const postViews = sumBy(feeds, "metadata.views");
      const totalHypes = sumBy(feeds, "likes");

      const promises = feeds.map(async (feed) => {
        // Get all comments on a post
        const comments = await Comment.find({ commentable: feed })
          .select("likes user")
          .lean()
          .read("secondary", MONGO_PREFERENCE_TAGS) // Read from analytics database to not affect prod
          .cache(600);

        // TODO make comments by user are filtered out

        // Calculate # of comments on a post
        const totalComments = comments.length;

        // Get a sum of all hypes on a comment
        const totalCommentHypes = sumBy(comments, "likes");

        return { totalComments, totalCommentHypes };
      });

      const feedComments = await Promise.all(promises);
      const totalFeedComments = sumBy(feedComments, "totalComments");
      const totalFeedCommentHypes = sumBy(feedComments, "totalCommentHypes");

      const hypeScore = totalFeedComments + totalFeedCommentHypes + totalHypes;

      return {
        hypeScore,
        totalFeedComments,
        totalFeedCommentHypes,
        totalHypes,
        postViews
      };
    } catch (error) {
      this.logger?.error(`[${userId || "unknown-user"}]: An error occurred calculating hype score for user.`, error);
      return;
    }
  }
}
