import { Failure } from "@efuse/contracts";
import { StreakName } from "@efuse/entities";
import { Types, FilterQuery } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";

import { IUser, IComment, IHomeFeed } from "../../backend/interfaces";
import { BaseService } from "../base/base.service";
import { CommentService } from "../comments/comment.service";
import { Service, Trace } from "../decorators";
import { StreaksService } from "../streaks/streaks.service";
import { HypeAsyncService } from "./hype-async.service";
import { HypeService } from "./hype.service";
import { FeedService } from "../feeds/feed.service";
import { HomeFeedService } from "../home-feed/home-feed.service";
import CommentsLib from "../users/comments";
import { BrazeService } from "../braze/braze.service";
import { BrazeCampaignName } from "../braze/braze.enum";
import { HypeTypeEnum } from "./hype.enum";
import { Hype } from "../../backend/models/hype/hype.model";
import { HypeResponse } from "../../backend/models/hype/hype-response.model";

@Service("hype-actions.service")
export class HypeActionsService extends BaseService {
  private $hypeService: HypeService;
  private $hypeAsyncService: HypeAsyncService;
  private $streakService: StreaksService;
  private $commentService: CommentService;
  private $feedService: FeedService;
  private $brazeService: BrazeService;
  private $homeFeedService: HomeFeedService;

  constructor() {
    super();

    this.$hypeService = new HypeService();
    this.$hypeAsyncService = new HypeAsyncService();
    this.$streakService = new StreaksService();
    this.$commentService = new CommentService();
    this.$feedService = new FeedService();
    this.$brazeService = new BrazeService();
    this.$homeFeedService = new HomeFeedService();
  }
  /**
   * @summary create Hype on feed
   *
   * @param {Types.ObjectId | string} feedId
   * @param {Types.ObjectId | string} userId
   * @param {Int }hypeCount Hype count
   *
   * @returns {Promise<HypeResponse}
   */
  @Trace()
  public async create(
    feedId: Types.ObjectId | string,
    userId: Types.ObjectId | string,
    hypeCount: number
  ): Promise<HypeResponse> {
    const hypeResult: HypeResponse = await this.$hypeService.createHype(feedId, userId, hypeCount);

    // Send notifications only if it is a new hype
    if (hypeResult.newHype && hypeResult.hype) {
      await Promise.all([
        // Update totalHypes
        this.$hypeAsyncService.updateTotalHypesAsync(hypeResult.hype._id),

        // Emit feed hype updates via WS
        this.$hypeAsyncService.emitFeedHypesUpdate(feedId, userId),

        // Send notification to postCreator
        this.$hypeAsyncService.sendHypeNotificationToPostCreator(feedId, userId, hypeResult.hype),

        // Check Streak
        this.$streakService.checkStreak(String(userId), StreakName.DAILY_USAGE, "Hype posts")
      ]);
    }

    return hypeResult;
  }

  /**
   * @summary Create comment hype and send notifications
   *
   * @param {Types.ObjectId | string} commentId  commentId on which hype to be create
   * @param {IUser} user user who hyped the comment
   * @param {HypeCount} hypeCount
   * @returns {Promise<HypeResponse}
   */
  @Trace()
  public async createCommentHypeAndNotify(
    commentId: Types.ObjectId | string,
    user: IUser,
    hypeCount: number,
    platform: string
  ): Promise<HypeResponse> {
    const userId = user._id;

    const comment = await this.$commentService.validateComment(commentId);

    const hypeResult: HypeResponse = await this.$hypeService.createCommentHype(commentId, userId, hypeCount, platform);

    // Send notifications only if it is a new hype
    if (hypeResult.newHype && hypeResult.hype) {
      await Promise.all([
        // increment commnt update count
        this.$commentService.update({ _id: commentId }, { $inc: { likes: 1 } }),

        // Calculate Score
        await this.calculateHypeScore(comment),

        // Check Streak
        this.$streakService.checkStreak(String(userId), StreakName.DAILY_USAGE, "Hype Comment"),

        // Send updates to FE
        // todo: investigate
        // @ts-ignore
        await CommentsLib.initToggleCommentLikeAsync(comment.commentable as Types.ObjectId, userId, commentId, true, 1)
      ]);

      // We need to the homeFeeds._id for the notifcation.
      // TODO: migrate all notifications to use feeds._id instead of homeFeed._id
      const homeFeed: IHomeFeed | null = await this.$homeFeedService.findOne(
        { feed: String(comment.commentable) },
        {},
        { lean: true }
      );

      if (homeFeed && String(userId) !== String(comment.user)) {
        this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.COMMENT_HYPE, {
          trigger_properties: {
            hyping_username: user.username,
            hyping_user_id: String(user._id),
            comment_id: String(commentId),
            feed_id: String(homeFeed._id),
            hype_id: String(hypeResult.hype._id)
          },
          recipients: [{ external_user_id: String(comment.user) }]
        });
      }
    }

    return hypeResult;
  }

  /**
   * @summary Delete comment hype
   *
   * @param {Types.ObjectId | string} commentId  commentId on which hype to be create
   * @param {IUser} user user who hyped the comment
   * @returns {Promise<Hype}
   */
  @Trace()
  public async deleteCommentHype(commentId: Types.ObjectId | string, user: IUser): Promise<Hype | null> {
    const userId = user._id;

    const comment = await this.$commentService.validateComment(commentId);

    const query = { user: userId, objId: comment._id, objType: HypeTypeEnum.COMMENT } as FilterQuery<
      DocumentType<Hype>
    >;

    const existingHype = await this.$hypeService.findOne(query);

    // Can't unhype without hyping the comment first
    if (!existingHype) {
      return null;
    }

    const deletedhype = await this.$hypeService.findOneAndDelete(query);

    // Decrement comment like count
    await this.$commentService.update({ _id: commentId }, { $inc: { likes: -1 } });

    // Send updates to FE
    // todo: investigate
    // @ts-ignore
    await CommentsLib.initToggleCommentLikeAsync(comment.commentable, userId, commentId, true, 1);

    return deletedhype;
  }

  /**
   * @summary calculate score against hype
   *
   * @param {Types.ObjectId | string} comment comment id on which to calculate score
   *
   * @return {Promise<void>}
   */
  @Trace()
  private async calculateHypeScore(comment: IComment): Promise<void> {
    const feed = await this.$feedService.findOne({ _id: comment.commentable });

    if (!feed) {
      throw Failure.UnprocessableEntity("Feed not found");
    }

    try {
      const user = <IUser>(<unknown>feed.user);
      await this.$hypeAsyncService.updateHypeScoreAsync(user._id);
    } catch (error) {
      this.logger?.error(error, "Error while calculating hypeScore async ");
    }
  }
}
