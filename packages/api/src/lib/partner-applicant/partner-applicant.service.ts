import { Types, FilterQuery, QueryOptions } from "mongoose";
import { Failure } from "@efuse/contracts";
import { StreakName } from "@efuse/entities";

import { IPartnerApplicant, IPartnerApplyResponse } from "../../backend/interfaces";
import { IUser } from "../../backend/interfaces/user";
import { PartnerApplicant, PartnerApplicantModel } from "../../backend/models/partner-applicant";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { UserService } from "../users/user.service";
import SchemaHelper from "../../backend/helpers/schema_helper";
import { Streak, StreakModel } from "../../backend/models/streak";
import { IStreak } from "../../backend/interfaces/streak";
import { PartnerApplicantStatusEnum } from "./partner-applicant.enum";

const STREAK_DURATION = 60;

@Service("partner-applicant.service")
export class PartnerApplicantService extends BaseService {
  private readonly $partnerApplicants: PartnerApplicantModel<IPartnerApplicant> = PartnerApplicant;
  private readonly $streaks: StreakModel<IStreak> = Streak;

  private $userService: UserService;

  constructor() {
    super();

    this.$userService = new UserService();
  }

  public async findOne(filter: FilterQuery<IPartnerApplicant>): Promise<IPartnerApplicant | null>;
  public async findOne(filter: FilterQuery<IPartnerApplicant>, projection: any): Promise<IPartnerApplicant | null>;
  public async findOne(
    filter: FilterQuery<IPartnerApplicant>,
    projection: any,
    options: QueryOptions
  ): Promise<IPartnerApplicant | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IPartnerApplicant>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IPartnerApplicant | null> {
    const partnerApplicant = await this.$partnerApplicants.findOne(filter, projection, options);
    return partnerApplicant;
  }

  /**
   * @summary Check whether a user can apply for partner badge
   *
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IPartnerApplyResponse>}
   */
  @Trace()
  public async canApply(userId: Types.ObjectId | string): Promise<IPartnerApplyResponse> {
    try {
      const reasons: string[] = [];

      const user: IUser | null = await this.$userService.findOne(
        { _id: userId },
        {
          bio: 1,
          profilePicture: 1,
          headerImage: 1,
          discordVerified: 1,
          facebookVerified: 1,
          googleVerified: 1,
          "leagueOfLegends.verified": 1,
          "overwatch.stats": 1,
          linkedinVerified: 1,
          "pubg.stats": 1,
          "rocketleague.stats": 1,
          snapchatVerified: 1,
          steamVerified: 1,
          "tikTok.tiktokVerified": 1,
          twitchVerified: 1,
          "youtube.youtubeVerified": 1
        },
        // todo: investigate how to use autopopulate correctly
        <never>{ lean: true, autopopulate: false }
      );

      // Make sure user exists
      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      // Make sure user has not already applied
      const partnerApplicant: IPartnerApplicant | null = await this.findOne({ user: userId });
      if (partnerApplicant) {
        reasons.push("Already applied");
        return { canApply: false, reasons, partnerApplicant };
      }

      // Make sure bio exists
      if (!user.bio) {
        reasons.push("Bio is missing");
      }

      // Make sure profileImage exists
      if (!user.profilePicture?.url || user.profilePicture.url === SchemaHelper.fileSchema.url.toString()) {
        reasons.push("Profile image is not uploaded");
      }

      // Make sure headerImage exists
      if (!user.headerImage?.url || user.headerImage.url === SchemaHelper.fileSchemaDefaultsNoImage.url.toString()) {
        reasons.push("Header image is not uploaded");
      }

      // Make sure user has either a social or game account linked
      if (
        !user.discordVerified &&
        !user.facebookVerified &&
        !user.googleVerified &&
        !user.leagueOfLegends?.verified &&
        !user.overwatch?.stats &&
        !user.linkedinVerified &&
        user.pubg?.stats?.length === 0 &&
        user.rocketleague?.stats.length === 0 &&
        !user.snapchatVerified &&
        !user.steamVerified &&
        !user.tikTok?.tiktokVerified &&
        !user.twitchVerified &&
        !user.youtube?.youtubeVerified
      ) {
        reasons.push("No social or gaming account connected");
      }

      const streak: IStreak | null = await this.$streaks.findOne({
        user: user._id,
        isActive: true,
        streakDuration: { $gte: STREAK_DURATION },
        streakName: StreakName.DAILY_USAGE
      });

      if (!streak) {
        reasons.push("Streak must be greater than or equal to 60");
      }

      return { canApply: reasons.length === 0, reasons };
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Create a new partner object
   *
   * @param {IPartnerApplicant} partnerBody
   *
   * @returns {Promise<IPartnerApplicant>}
   */
  @Trace()
  public async create(partnerBody: IPartnerApplicant): Promise<IPartnerApplicant> {
    // Make sure params are passed
    if (!partnerBody.user) {
      throw Failure.BadRequest("user is required");
    }

    if (!partnerBody.status) {
      throw Failure.BadRequest("status is required");
    }

    const doc = <IPartnerApplicant>partnerBody;
    const partnerApplicant: IPartnerApplicant = await this.$partnerApplicants.create(doc);
    return partnerApplicant;
  }

  /**
   * @summary Apply for Partner Badge
   *
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IPartnerApplicant>}
   */
  @Trace()
  public async apply(userId: Types.ObjectId | string): Promise<IPartnerApplicant> {
    const ableToApply = await this.canApply(userId);

    if (ableToApply.partnerApplicant) {
      return ableToApply.partnerApplicant;
    }

    if (!ableToApply.canApply) {
      throw Failure.BadRequest("You are not eligible to apply for Partner Badge");
    }

    const partnerBody = <IPartnerApplicant>{ user: userId, status: PartnerApplicantStatusEnum.UNDER_REVIEW };

    const partnerApplicant: IPartnerApplicant = await this.create(partnerBody);
    return partnerApplicant;
  }
}
