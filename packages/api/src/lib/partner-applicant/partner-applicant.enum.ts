export enum PartnerApplicantStatusEnum {
  APPROVED = "Approved",
  NOT_APPROVED = "Not Approved",
  UNDER_REVIEW = "Under Review"
}
