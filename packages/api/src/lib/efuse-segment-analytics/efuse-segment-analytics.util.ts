import { SEGMENT_API_KEY } from "@efuse/key-store";
import Analytics from "analytics-node";

import { Environments } from "../environments";
import { IIdentityAnonymous, IIdentityUser, ITrackingEvent } from "../../backend/interfaces";
import { TrackingEventService } from "../tracking-event/tracking-event.service";

export class EFuseSegmentAnalyticsUtil {
  private static instance: Analytics;

  public static get analytics(): Analytics {
    if (!EFuseSegmentAnalyticsUtil.instance) {
      const config = process.env.NODE_ENV === Environments.PRODUCTION ? {} : { flushAt: 1 };
      EFuseSegmentAnalyticsUtil.instance = new Analytics(SEGMENT_API_KEY, config);
    }

    return this.instance;
  }

  public static track = (
    identity: IIdentityUser | IIdentityAnonymous,
    event: string,
    properties?: any,
    context?: any,
    timestamp?: Date,
    integrations?: any
  ): void => {
    if (EFuseSegmentAnalyticsUtil.analytics) {
      EFuseSegmentAnalyticsUtil.analytics.track({ ...identity, event, properties, context, timestamp, integrations });

      const trackingEventService: TrackingEventService = new TrackingEventService();

      const eventToBeStored: ITrackingEvent = <ITrackingEvent>{
        name: event,
        identity: {
          userId: identity.userId as string | undefined,
          anonymousId: identity.anonymousId as string | undefined
        },
        properties: properties?.extraProperties,
        trackedResource: properties?.trackedResource,
        context
      };

      trackingEventService.createAsync(eventToBeStored);
    }
  };
}
