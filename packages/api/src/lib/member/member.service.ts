import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions } from "mongoose";

import { IMember } from "../../backend/interfaces/member";
import { Member, MemberModel } from "../../backend/models/member";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

@Service("member.service")
export class MemberService extends BaseModelService<IMember> {
  private member: MemberModel<IMember>;

  constructor() {
    super(Member);

    this.member = Member;
  }

  /**
   * @summary Create a new member object
   *
   * @param {IMember} memberBody
   *
   * @returns {Promise<IMember>}
   */
  @Trace()
  public async create(memberBody: IMember): Promise<IMember> {
    if (!memberBody) {
      throw Failure.BadRequest("member body is required");
    }

    const doc = <IMember>memberBody;
    const member = await this.member.create(doc);
    return member;
  }

  /**
   * @summary Update a particular member object
   *
   * @param {FilterQuery<IMember>} condition
   * @param {Partial<IMember>} memberBody
   *
   * @returns {IMember}
   */
  @Trace()
  public async update(condition: FilterQuery<IMember>, memberBody: Partial<IMember>): Promise<IMember> {
    if (!memberBody) {
      throw Failure.BadRequest("member body is required");
    }

    const member: IMember | null = await this.member
      .findOneAndUpdate(condition, { ...memberBody }, { new: true })
      .lean();

    if (!member) {
      throw Failure.UnprocessableEntity("member not found");
    }

    return member;
  }

  public async findOne(filter: FilterQuery<IMember>): Promise<IMember | null>;
  public async findOne(filter: FilterQuery<IMember>, projection: any): Promise<IMember | null>;
  public async findOne(
    filter: FilterQuery<IMember>,
    projection: any,
    options: QueryOptions | null
  ): Promise<IMember | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IMember>,
    projection?: any,
    options?: QueryOptions | null
  ): Promise<IMember | null> {
    const member = await this.member.findOne(filter, projection, options);

    return member;
  }
}
