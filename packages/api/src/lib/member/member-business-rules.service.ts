import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";
import { Queue } from "bull";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { IMember } from "../../backend/interfaces/member";
import { OrganizationsService } from "../organizations/organizations.service";
import { IOrganization } from "../../backend/interfaces";
import { MemberService } from "./member.service";
import Config from "../../async/config";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";

@Service("member-business-rules.service")
export class MemberBusinessRulesService extends BaseService {
  private organizationService: OrganizationsService;
  private memberService: MemberService;
  private organizationACLService: OrganizationACLService;

  private createMemberOnSignupQueue: Queue;

  constructor() {
    super();

    this.organizationService = new OrganizationsService();
    this.memberService = new MemberService();
    this.organizationACLService = new OrganizationACLService();

    this.createMemberOnSignupQueue = Config.initQueue("createMemberOnSignup");
  }

  /**
   * @summary Create member upon signup
   *
   * @param {Types.ObjectId | string} organizationId
   * @param {Types.ObjectId | string} userId
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async createMemberOnSignup(
    organizationId: Types.ObjectId | string,
    userId: Types.ObjectId | string
  ): Promise<void> {
    try {
      // Make sure organization already exists
      const organization: IOrganization | null = await this.organizationService.findOne(
        { _id: organizationId },
        { _id: 1 }
      );
      if (!organization) {
        throw Failure.BadRequest("organization not found");
      }

      const memberBody: IMember = <IMember>{ user: userId, organization: organizationId, isBanned: false };
      await this.memberService.create(memberBody);
    } catch (error) {
      this.logger?.error(error, "Error while adding member to organization on signup");
    }
  }

  /**
   * @summary Create member upon signup
   *
   * @param {Types.ObjectId | string} organizationId
   * @param {Types.ObjectId | string} userId
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async createMemberOnSignupAsync(
    organizationId: Types.ObjectId | string,
    userId: Types.ObjectId | string
  ): Promise<void> {
    await this.createMemberOnSignupQueue.add({ organizationId, userId });
  }

  /**
   * @summary Update a particular member, only Captain, Owner & eFuse Admins are allowed
   *
   * @param {Types.ObjectId | string} currentUserId
   * @param {Types.ObjectId | string} organizationId
   * @param {Types.ObjectId | string} memberId
   * @param {Partial<IMember>} memberBody
   *
   * @returns {Promise<IMember>}
   */
  public async updateMember(
    currentUserId: Types.ObjectId | string,
    organizationId: Types.ObjectId | string,
    memberId: Types.ObjectId | string,
    memberBody: Partial<IMember>
  ): Promise<IMember> {
    // Make sure user has access
    await this.organizationACLService.authorizedToAccess(currentUserId, organizationId, OrganizationAccessTypes.ANY);

    const member = await this.memberService.update({ _id: memberId }, memberBody);
    return member;
  }

  /**
   * Async Queue Processors
   */
  @Trace({ service: "member-business-rules.service", name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const memberBusinessRulesService = new MemberBusinessRulesService();

    memberBusinessRulesService.createMemberOnSignupQueue
      .process((job: { data: { organizationId: string; userId: string } }) => {
        const { organizationId, userId } = job.data;
        return memberBusinessRulesService.createMemberOnSignup(organizationId, userId);
      })
      .catch((error) =>
        memberBusinessRulesService.logger?.error(error, "Error while processing createMemberOnSignupQueue")
      );
  }
}
