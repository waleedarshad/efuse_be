import { Failure } from "@efuse/contracts";
import { LeagueBracketType } from "@efuse/entities";
import moment from "moment";
import { ILeagueBracket, ILeagueEvent, ILeagueRound } from "../../backend/interfaces";

import { ObjectId } from "../../backend/types";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { LeagueRoundService } from "./league-round.service";

@Service("league-round-generation.service")
export class LeagueRoundGenerationService extends BaseService {
  private roundService: LeagueRoundService;

  constructor() {
    super();

    this.roundService = new LeagueRoundService();
  }

  @Trace()
  public async generateRoundsForEvent(
    bracket: ILeagueBracket,
    numberOfTeams: number,
    event: ILeagueEvent
  ): Promise<ILeagueRound[]> {
    try {
      const rounds = await this.roundService.find({ bracket: bracket._id }, {}, { lean: true });

      if (rounds && rounds.length > 0) {
        return rounds;
      }

      switch (bracket.type) {
        case LeagueBracketType.ROUND_ROBIN:
          return this.createRoundsForRoundRobin(bracket, numberOfTeams, event);
        case LeagueBracketType.SINGLE_ELIM:
          return this.createRoundsForSingleElim(bracket, numberOfTeams, event);
        default:
          throw Failure.NotImplemented("Rounds unable to be generated do to unimplemented bracket type", {
            bracket
          });
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async createRounds(
    roundsToGenerate: number,
    bracketId: ObjectId,
    event: ILeagueEvent
  ): Promise<ILeagueRound[]> {
    try {
      const roundPromises: Promise<ILeagueRound>[] = [];

      for (let i = 1; i <= roundsToGenerate; i++) {
        const round = this.roundService.create(<ILeagueRound>{
          name: `Round ${i}`,
          bracket: bracketId,
          value: i,
          startDate: moment(event.startDate)
            .add(i - 1, "w")
            .toDate()
        });

        roundPromises.push(round);
      }

      const createdRounds = await Promise.all(roundPromises);

      return createdRounds;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  //number of teams is capped at 32, if we extend this or allow for an unlimited amount
  //for the user to enter we will need to extend this object to allow it
  private async nearestPowerOf2(numberOfTeams) {
    const numberOfRounds = { 2: 1, 4: 2, 8: 3, 16: 4, 32: 5 };

    //math.pow gives us the power of 2^X, math.ceil gives us the nearest whole number of power 2
    const correctNumberOfTeams = Math.pow(2, Math.ceil(Math.log2(numberOfTeams) / Math.log2(2)));
    return numberOfRounds[correctNumberOfTeams];
  }

  /**
   *
   * @param bracket
   * @param numberOfTeams
   * @param event
   * @returns
   */
  private async createRoundsForSingleElim(
    bracket: ILeagueBracket,
    numberOfTeams: number,
    event: ILeagueEvent
  ): Promise<ILeagueRound[]> {
    try {
      const numberOfRoundsToCreate = await this.nearestPowerOf2(numberOfTeams);

      return this.createRounds(numberOfRoundsToCreate, bracket._id, event);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * For Round Robin
   *
   * Number of Rounds:
   * Even Teams - (n Teams - 1)
   * Odd Teams  - (n Teams) Note: One Team will get a bye when teams are odd.
   */
  private async createRoundsForRoundRobin(
    bracket: ILeagueBracket,
    numberOfTeams: number,
    event: ILeagueEvent
  ): Promise<ILeagueRound[]> {
    try {
      if (numberOfTeams === 0) {
        return [];
      }

      const teamNumberEven = numberOfTeams % 2 === 0;

      const roundsToGenerate: number = teamNumberEven ? numberOfTeams - 1 : numberOfTeams;

      return this.createRounds(roundsToGenerate, bracket._id, event);
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
