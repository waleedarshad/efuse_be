import { Failure } from "@efuse/contracts";
import { ILeagueMatch, ILeagueScoreboard, ILeagueScoreboardWithPool } from "../../backend/interfaces";
import { ObjectId } from "../../backend/types";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { LeagueBracketService } from "./league-bracket.service";
import { LeagueMatchService } from "./league-match.service";
import { LeaguePoolService } from "./league-pool.service";
import { LeagueRoundService } from "./league-round.service";

@Service("league-scoreboard.service")
export class LeagueScoreboardService extends BaseService {
  private bracketService: LeagueBracketService;
  private matchService: LeagueMatchService;
  private poolService: LeaguePoolService;
  private roundService: LeagueRoundService;

  constructor() {
    super();

    this.bracketService = new LeagueBracketService();
    this.matchService = new LeagueMatchService();
    this.poolService = new LeaguePoolService();
    this.roundService = new LeagueRoundService();
  }

  @Trace()
  public async getScoreboardsWithPoolForEvent(eventId: ObjectId): Promise<ILeagueScoreboardWithPool[]> {
    try {
      const foundPools = await this.poolService.find(
        { event: eventId },
        { _id: true, teams: true },
        { lean: true, sort: { _id: "asc" } }
      );

      if (!foundPools || foundPools.length === 0) {
        return [];
      }

      const scoreBoardsWithPool = await Promise.all(
        foundPools.map((pool) => this.getScoreboardWithPool(pool._id, pool.teams))
      );

      return scoreBoardsWithPool;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async getScoreboardForPool(poolId: ObjectId, poolTeams: ObjectId[]): Promise<ILeagueScoreboard[]> {
    try {
      const foundMatches = await this.getMatchesForPool(poolId);

      const scoreBoard: ILeagueScoreboard[] = poolTeams.map((team) => <ILeagueScoreboard>{ team, wins: 0, losses: 0 });

      // this increases the count for wins and losses
      // It will only try to count wins and losses if the match has a winner
      foundMatches.forEach((match) => {
        const matchTeams = match.teams;
        const matchWinner = match.winner;

        if (matchWinner) {
          scoreBoard.forEach((board) => {
            const foundTeam = matchTeams.find((team) => team.equals(board.team));
            if (foundTeam && foundTeam.equals(matchWinner)) {
              board.wins += 1;
            } else if (foundTeam) {
              board.losses += 1;
            }
          });
        }
      });

      return this.sortScoreboardByWinsThenLosses(scoreBoard);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async getMatchesForPool(poolId: ObjectId): Promise<ILeagueMatch[]> {
    const foundBracket = await this.bracketService.findOne({ pool: poolId }, { _id: true }, { lean: true });

    if (!foundBracket) {
      throw Failure.UnprocessableEntity("Bracket does not exist for pool", { poolId });
    }

    const foundRounds = await this.roundService.find({ bracket: foundBracket._id }, { _id: true }, { lean: true });
    const roundIds = foundRounds.map((round) => round._id);

    const foundMatches = await this.matchService.find(
      { round: { $in: roundIds }, winner: { $exists: true } },
      { teams: true, winner: true },
      { lean: true }
    );

    return foundMatches;
  }

  @Trace()
  private async getScoreboardWithPool(poolId: ObjectId, poolTeams: ObjectId[]): Promise<ILeagueScoreboardWithPool> {
    try {
      const generatedScoreboard = await this.getScoreboardForPool(poolId, poolTeams);

      return <ILeagueScoreboardWithPool>{ pool: poolId, scoreboard: generatedScoreboard };
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private sortScoreboardByWinsThenLosses(scoreBoard: ILeagueScoreboard[]): ILeagueScoreboard[] {
    scoreBoard.sort((a, b) => {
      const winComparison = a.wins - b.wins;

      if (winComparison < 0) {
        return 1;
      }
      if (winComparison > 0) {
        return -1;
      }

      const loseComparison = a.losses - b.losses;

      if (loseComparison > 0) {
        return -1;
      }
      if (loseComparison < 0) {
        return 1;
      }

      return 0;
    });

    return scoreBoard;
  }
}
