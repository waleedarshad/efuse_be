import { Failure } from "@efuse/contracts";
import { LeagueBracketType, LeagueMatchState } from "@efuse/entities";
import { cloneDeep } from "lodash";
import { ILeagueEvent, ILeagueMatch, ILeagueRound } from "../../backend/interfaces";

import { ObjectId } from "../../backend/types";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { ShuffleUtil } from "../utils/shuffle.util";
import { LeagueMatchService } from "./league-match.service";

@Service("league-match-making.service")
export class LeagueMatchMakingService extends BaseService {
  private matchService: LeagueMatchService;

  constructor() {
    super();

    this.matchService = new LeagueMatchService();
  }

  @Trace()
  public async determineBracketTypeAndCreateMatchesForRounds(
    rounds: ILeagueRound[],
    teams: ObjectId[],
    bracketType: LeagueBracketType,
    event: ILeagueEvent
  ): Promise<ILeagueMatch[][] | void> {
    try {
      const roundIds = rounds.map((round) => round._id);
      const matchesExist = await this.matchService.exists({ round: { $in: roundIds } });

      if (matchesExist) {
        return;
      }

      switch (bracketType) {
        case LeagueBracketType.ROUND_ROBIN:
          return this.shuffleAndShiftTeamsForRoundsInRoundRobin(rounds, teams, event);
        case LeagueBracketType.SINGLE_ELIM:
          return this.generateSingleElimMatchups(rounds, teams);
        default:
          throw Failure.BadRequest("Could not determine league bracket type", {
            rounds,
            teams,
            bracketType
          });
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async createMatch(roundId: ObjectId, roundStartDate: Date, teamMatchUp: ObjectId[]): Promise<ILeagueMatch> {
    const foundNull = teamMatchUp.findIndex((team) => !team || team == null);
    const matchState = foundNull < 0 ? LeagueMatchState.ACTIVE : LeagueMatchState.BYE;

    const match = <ILeagueMatch>{
      startTime: roundStartDate,
      round: roundId,
      teams: teamMatchUp,
      state: matchState
    };

    await this.matchService.create(match);

    return match;
  }

  @Trace()
  private async createPendingMatch(
    roundId: ObjectId,
    roundStartDate: Date,
    teamMatchUp: ObjectId[]
  ): Promise<ILeagueMatch> {
    const match = <ILeagueMatch>{
      startTime: roundStartDate,
      round: roundId,
      teams: teamMatchUp,
      state: LeagueMatchState.PENDING
    };

    await this.matchService.create(match);

    return match;
  }

  @Trace()
  private async createMatches(
    roundId: ObjectId,
    roundStartDate: Date,
    matchUps: ObjectId[][]
  ): Promise<ILeagueMatch[]> {
    let matches: ILeagueMatch[] = [];

    try {
      matches = await Promise.all(matchUps.map((matchUp) => this.createMatch(roundId, roundStartDate, matchUp)));
    } catch (error) {
      throw this.handleError(error);
    }

    return matches;
  }

  @Trace()
  private async createPendingMatches(
    roundId: ObjectId,
    roundStartDate: Date,
    numberOfMatches: number
  ): Promise<ILeagueMatch[]> {
    let matchNumber = numberOfMatches;
    const matchUps: any[][] = [];
    let matches: ILeagueMatch[] = [];

    // this will allow use to generate the correct number of empty matchups for each round
    while (matchNumber > 0) {
      const matchUp = [];

      matchUps.push(matchUp);
      matchNumber--;
    }

    try {
      matches = await Promise.all(matchUps.map((matchUp) => this.createPendingMatch(roundId, roundStartDate, matchUp)));
    } catch (error) {
      throw this.handleError(error);
    }

    return matches;
  }

  /**
   * If teams are odd then one of the matchups will have a null team. We will use this as part of a bye
   */
  @Trace()
  private generateMatchUps(teams: ObjectId[]): ObjectId[][] {
    const clonedTeams = cloneDeep(teams);
    const matchUps: any[][] = [];

    while (clonedTeams.length > 0) {
      const firstTeam: ObjectId | undefined = clonedTeams.shift();
      const lastTeam: ObjectId | undefined = clonedTeams.pop();

      const matchUp = [firstTeam, lastTeam];

      matchUps.push(matchUp);
    }

    return matchUps;
  }

  @Trace()
  private async generateSingleElimMatchups(rounds: ILeagueRound[], teams: ObjectId[]): Promise<ILeagueMatch[][]> {
    // we shuffle the teams to keep it random for now. seeds might come later so this would go away
    const shuffledTeams = ShuffleUtil.knuthShuffle<ObjectId>(cloneDeep(teams));
    const matchPromises: Promise<ILeagueMatch[]>[] = [];
    let matchesPerRound = teams.length / 2;

    // cycle through each round and for the first round create matchups and matches
    rounds.forEach((round, index) => {
      let matchPromise;
      if (index === 0) {
        const matchUps = this.generateMatchUps(shuffledTeams);
        matchPromise = this.createMatches(round._id, round.startDate, matchUps);
      } else {
        //generate matches for later rounds with empty matchups
        matchPromise = this.createPendingMatches(round._id, round.startDate, matchesPerRound);
      }
      matchPromises.push(matchPromise);

      // set number of matches for next round
      matchesPerRound /= 2;
    });

    try {
      return await Promise.all(matchPromises);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async shuffleAndShiftTeamsForRoundsInRoundRobin(
    rounds: ILeagueRound[],
    teams: ObjectId[],
    event: ILeagueEvent
  ): Promise<void> {
    const shuffledTeams = ShuffleUtil.knuthShuffle<ObjectId>(cloneDeep(teams));

    const evenNumberOfTeams = shuffledTeams.length % 2 === 0;

    const matchPromises: Promise<ILeagueMatch[]>[] = [];

    // we will cycle through each round
    rounds.forEach((round) => {
      // for each round we will take the shuffled teams and then create an array of matchups
      const matchUps = this.generateMatchUps(shuffledTeams);

      // have to do slightly different things depending on even or odd number of teams
      // EVEN - we leave the first element in the array alone and move the others around
      // ODD - we shift the first team in the array and add it to the end
      const teamToAddToEnd = evenNumberOfTeams ? shuffledTeams.splice(1, 1) : [<ObjectId>shuffledTeams.shift()];
      shuffledTeams.push(...teamToAddToEnd);

      const matchPromise = this.createMatches(round._id, round.startDate, matchUps);
      matchPromises.push(matchPromise);
    });

    try {
      await Promise.all(matchPromises);
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
