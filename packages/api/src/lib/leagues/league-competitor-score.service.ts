import { ILeagueCompetitorScore } from "../../backend/interfaces";

import { LeagueCompetitorScore } from "../../backend/models/league";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ObjectId } from "../../backend/types";
import { Failure } from "@efuse/contracts";
import { TeamService } from "../team";
import { OwnerType } from "@efuse/entities";
import { LeagueGameScoreService } from "./league-game-score.service";

@Service("league-competitor-score.service")
export class LeagueCompetitorScoreService extends BaseModelService<ILeagueCompetitorScore> {
  private teamService: TeamService;
  private gameScoreService: LeagueGameScoreService;

  constructor() {
    super(LeagueCompetitorScore);

    this.teamService = new TeamService();
    this.gameScoreService = new LeagueGameScoreService();
  }

  @Trace()
  public async findOwner(competitorScoreId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const score = await this.findOne({ _id: competitorScoreId });

    if (!score) {
      throw Failure.UnprocessableEntity("Invalid id", competitorScoreId);
    }

    return this.gameScoreService.findOwner(score.gameScore);
  }
}
