import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";
import { OwnerType } from "@efuse/entities";

import { ILeaguePool } from "../../backend/interfaces";
import { LeaguePool } from "../../backend/models/league";
import { ObjectId } from "../../backend/types";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ShuffleUtil } from "../utils/shuffle.util";
import { LeagueEventService } from "./league-event.service";

@Service("league-pool.service")
export class LeaguePoolService extends BaseModelService<ILeaguePool> {
  private eventService: LeagueEventService;

  constructor() {
    super(LeaguePool);

    this.eventService = new LeagueEventService();
  }

  @Trace()
  public async generateLeagueTeamPools(teamIds: Types.ObjectId[], poolIds: Types.ObjectId[]): Promise<ILeaguePool[]> {
    const numberOfPools = poolIds.length;

    try {
      const areTeamsEven = teamIds.length % 2 === 0;
      if (!areTeamsEven) {
        throw Failure.NotAcceptable("There must be an even number of teams", {
          numberOfTeams: teamIds.length
        });
      }

      const shuffledTeams = ShuffleUtil.knuthShuffle<Types.ObjectId>(teamIds);
      const teamPools: Types.ObjectId[][] = [];

      for (let count = 0; count < numberOfPools; count++) {
        teamPools.push([]);
      }

      let count = 0;
      while (shuffledTeams.length > 0) {
        const removedTeams = shuffledTeams.splice(0, 2);
        teamPools[count % numberOfPools].push(...removedTeams);
        count += 1;
      }

      const updatedPools = await Promise.all(
        poolIds.map((poolId, index) => {
          const teamPool = teamPools[index];

          return this.updateOne({ _id: poolId }, { teams: teamPool }, { lean: true, new: true });
        })
      );

      return updatedPools;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createNumberOfPoolsForEvent(numberOfPools: number, eventId: ObjectId): Promise<Types.ObjectId[]> {
    try {
      let poolIds: Types.ObjectId[] = [];

      if (numberOfPools && numberOfPools > 0) {
        const poolPromises: Promise<ILeaguePool>[] = [];

        for (let i = 1; i <= numberOfPools; i++) {
          const pool = this.create(<ILeaguePool>{
            name: `Pool ${i}`,
            event: eventId
          });

          poolPromises.push(pool);
        }

        const pools = await Promise.all(poolPromises);
        poolIds = pools.map((pool) => pool._id);
      } else {
        const pool = await this.create(<ILeaguePool>{
          name: `Pool 1`,
          event: eventId
        });

        poolIds.push(pool._id);
      }

      return poolIds;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findOwner(poolId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const pool = await this.findOne({ _id: poolId });

    if (!pool) {
      throw Failure.BadRequest("Invalid id", poolId);
    }

    return this.eventService.findOwner(pool.event);
  }
}
