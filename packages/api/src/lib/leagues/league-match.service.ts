import { ILeagueMatch } from "../../backend/interfaces";

import { LeagueMatch } from "../../backend/models/league";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ObjectId } from "../../backend/types";
import { LeagueRoundService } from "./league-round.service";
import { Failure } from "@efuse/contracts";
import { OwnerType } from "@efuse/entities";

@Service("league-match.service")
export class LeagueMatchService extends BaseModelService<ILeagueMatch> {
  private roundService: LeagueRoundService;

  constructor() {
    super(LeagueMatch);

    this.roundService = new LeagueRoundService();
  }

  @Trace()
  public async findOwner(matchId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const match = await this.findOne({ _id: matchId });

    if (!match) {
      throw Failure.BadRequest("Invalid id", matchId);
    }

    return this.roundService.findOwner(match.round);
  }
}
