import { OwnerType } from "@efuse/entities";
import { Failure } from "@efuse/contracts";

import { ILeagueEvent } from "../../backend/interfaces";
import { LeagueEvent } from "../../backend/models/league";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ObjectId } from "../../backend/types";
import { LeagueService } from "./league.service";

@Service("league-event.service")
export class LeagueEventService extends BaseModelService<ILeagueEvent> {
  private leagueService: LeagueService;

  constructor() {
    super(LeagueEvent);

    this.leagueService = new LeagueService();
  }

  @Trace()
  public async findOwner(eventId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const event = await this.findOne({ _id: eventId });

    if (!event) {
      throw Failure.UnprocessableEntity("Event does not exist", { event });
    }

    const league = await this.leagueService.findOne({ _id: event.league });

    if (!league) {
      throw Failure.UnprocessableEntity("League does not exist", { league });
    }

    return { owner: league.owner, ownerType: league.ownerType };
  }
}
