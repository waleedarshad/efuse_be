import { ILeagueRound } from "../../backend/interfaces";
import { LeagueRound } from "../../backend/models/league";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ObjectId } from "../../backend/types";
import { Failure } from "@efuse/contracts";
import { OwnerType } from "@efuse/entities";
import { LeagueBracketService } from "./league-bracket.service";

@Service("league-round.service")
export class LeagueRoundService extends BaseModelService<ILeagueRound> {
  private bracketService: LeagueBracketService;

  constructor() {
    super(LeagueRound);
    this.bracketService = new LeagueBracketService();
  }

  @Trace()
  public async findOwner(roundId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const round = await this.findOne({ _id: roundId });

    if (!round) {
      throw Failure.BadRequest("Invalid id", roundId);
    }

    return this.bracketService.findOwner(round.bracket);
  }
}
