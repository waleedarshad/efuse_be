import { ILeagueGameScore } from "../../backend/interfaces";

import { LeagueGameScore } from "../../backend/models/league";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ObjectId } from "../../backend/types";
import { LeagueMatchService } from "./league-match.service";
import { Failure } from "@efuse/contracts";
import { LeagueScoreCreatorType, OwnerType } from "@efuse/entities";
import { TeamService } from "../team";

@Service("league-game-score.service")
export class LeagueGameScoreService extends BaseModelService<ILeagueGameScore> {
  private matchService: LeagueMatchService;
  private teamService: TeamService;

  constructor() {
    super(LeagueGameScore);
    this.matchService = new LeagueMatchService();
    this.teamService = new TeamService();
  }

  @Trace()
  public async findOwner(gameScoreId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const gameScore = await this.findOne({ _id: gameScoreId });

    if (!gameScore) {
      throw Failure.BadRequest("Invalid id", gameScoreId);
    }

    return this.matchService.findOwner(gameScore.match);
  }

  @Trace()
  public async findOwnerOfCreator(
    creator?: ObjectId,
    type?: LeagueScoreCreatorType
  ): Promise<{ owner?: ObjectId; ownerType?: OwnerType }> {
    if (type === LeagueScoreCreatorType.ORGANIZATIONS) {
      return { owner: creator, ownerType: OwnerType.ORGANIZATIONS };
    }

    if (type === LeagueScoreCreatorType.USERS) {
      return { owner: creator, ownerType: OwnerType.USERS };
    }

    if (type === LeagueScoreCreatorType.TEAMS) {
      const team = await this.teamService.findOne({ _id: creator }, {}, { lean: true });
      if (!team) {
        throw Failure.UnprocessableEntity("Invalid creator", creator);
      }
      return { owner: team.owner, ownerType: team.ownerType };
    }

    throw Failure.ExpectationFailed("Could not determine creator type", parent);
  }
}
