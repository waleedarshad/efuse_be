import { LeagueMatchMakingService } from "./league-match-making.service";
import { LeagueBracketType } from "@efuse/entities";
import { normalRound, secondRound, thirdRound } from "./test-utils/mock-league-round";
import { returnMockRoundRobinEvent, returnMockSingleEliminationEvent } from "./test-utils/mock-league-event";
import { normalMatch } from "./test-utils/mock-league-match";
import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";

// hardcoding id values so outcomes are more predictable
const teams = [
  Types.ObjectId("507f1f77bcf86cd799439012"),
  Types.ObjectId("507f1f77bcf86cd799439019"),
  Types.ObjectId("507f1f77bcf86cd799439013"),
  Types.ObjectId("507f1f77bcf86cd799439014"),
  Types.ObjectId("507f1f77bcf86cd799439015"),
  Types.ObjectId("507f1f77bcf86cd799439016"),
  Types.ObjectId("507f1f77bcf86cd799439017"),
  Types.ObjectId("507f1f77bcf86cd799439018")
];
const match = normalMatch(teams);

jest.mock("./league-match.service", () => ({
  LeagueMatchService: jest.fn().mockImplementation(() => ({
    exists: jest.fn().mockResolvedValue(false),
    create: jest.fn().mockResolvedValue(match)
  }))
}));

describe("leauge-match-making.service", () => {
  let subject: LeagueMatchMakingService;

  const round1 = normalRound();
  const round2 = secondRound();
  const round3 = thirdRound();
  const leagueEvent = returnMockRoundRobinEvent();
  const singleEliminationEvent = returnMockSingleEliminationEvent();
  const evenRounds = [round1, round2];
  const rounds = [round1, round2, round3];

  beforeEach(() => {
    subject = new LeagueMatchMakingService();
  });

  it("returns error if leagueBracketType is not round roubin", async () => {
    const bracketType = LeagueBracketType.POINT_RACE;

    const errorBadRequest = Failure.BadRequest("Could not determine league bracket type");
    let thrownError;

    try {
      await subject.determineBracketTypeAndCreateMatchesForRounds(rounds, teams, bracketType, leagueEvent);
    } catch (error) {
      thrownError = error;
    }

    expect(thrownError).toEqual(errorBadRequest);
  });

  it("returns correct number of pending/active matches for all rounds for single elimination event with 8 teams", async () => {
    const bracketType = LeagueBracketType.SINGLE_ELIM;
    const matches = await subject.determineBracketTypeAndCreateMatchesForRounds(
      rounds,
      teams,
      bracketType,
      singleEliminationEvent
    );

    expect(matches[0]).toHaveLength(4);
    expect(matches[1]).toHaveLength(2);
    expect(matches[1][0].state).toBe("pending");
    expect(matches[2]).toHaveLength(1);
    expect(matches[2][0].state).toBe("pending");
  });

  it("returns correct number of matches for round one for single elimination event with odd number of teams and sets bye for second match", async () => {
    const oddTeams = [
      Types.ObjectId("507f1f77bcf86cd799439012"),
      Types.ObjectId("507f1f77bcf86cd799439019"),
      Types.ObjectId("507f1f77bcf86cd799439013")
    ];

    const bracketType = LeagueBracketType.SINGLE_ELIM;
    const matches = await subject.determineBracketTypeAndCreateMatchesForRounds(
      evenRounds,
      oddTeams,
      bracketType,
      singleEliminationEvent
    );

    expect(matches[0]).toHaveLength(2);
    expect(matches[0][1].state).toBe("bye");
  });
});
