import { InviteCodeType, OwnerType, LeagueJoinStatus } from "@efuse/entities";
import { FilterQuery } from "mongoose";
import { IEfusePermission, IInviteCode, ILeague, ILeagueWithJoinStatus } from "../../backend/interfaces";

import { League } from "../../backend/models/league";
import { ObjectId } from "../../backend/types";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { EFusePermissionsService } from "../permissions/efuse-permissions.service";
import { InviteCodeService } from "../invite-code/invite-code.service";
import { TeamService } from "../team";
import { LeaguePolicyManager } from "../permissions/manager";

@Service("league.service")
export class LeagueService extends BaseModelService<ILeague> {
  private inviteCodeService: InviteCodeService;
  private permissionsService: EFusePermissionsService;
  private leaguePolicyManager: LeaguePolicyManager;
  private teamService: TeamService;

  constructor() {
    super(League);

    this.inviteCodeService = new InviteCodeService();
    this.permissionsService = new EFusePermissionsService();
    this.leaguePolicyManager = new LeaguePolicyManager();
    this.teamService = new TeamService();
  }

  @Trace()
  public async create(item: ILeague): Promise<ILeague> {
    try {
      const result: ILeague = await super.create(item);

      await this.createLeagueInvites(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createMany(items: ILeague[]): Promise<ILeague[]> {
    try {
      const result: ILeague[] = await super.createMany(items);

      const invitesPromises = items.map((league) => this.createLeagueInvites(league));
      await Promise.all(invitesPromises);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async giveOrgLeagueJoinPermissions(
    joiningOrganizationId: ObjectId,
    hostOrganizationId: ObjectId
  ): Promise<boolean> {
    // TODO: Is there any reason to wrap this? If we don't need to do any additional things here in
    // the future we can just get rid of this function and directly call the policy manager.
    // return await this.leaguePolicyManager.addJoinedOrgPolicy(joiningOrganizationId, hostOrganizationId);

    // TODO (Permissions): Temporarily replacing to get things up and running in a post-Casbin world.
    try {
      const policy = <IEfusePermission>(
        this.leaguePolicyManager.builder.joinOrgPolicy(joiningOrganizationId, hostOrganizationId)
      );
      const policyExists = await this.permissionsService.exists(<FilterQuery<IEfusePermission>>policy);

      if (!policyExists) {
        const savedPermission = await this.permissionsService.create(policy);
        return !!savedPermission;
      }

      return true;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async canOrgJoinOrgsLeagues(joiningOrganizationId: ObjectId, hostOrganizationId: ObjectId): Promise<boolean> {
    // TODO: Is there any reason to wrap this? If we don't need to do any additional things here in
    // the future we can just get rid of this function and directly call the policy manager.
    // return await this.leaguePolicyManager.hasJoinedOrgPolicy(joiningOrganizationId, hostOrganizationId);

    // TODO (Permissions): Temporarily replacing to get things up and running in a post-Casbin world.
    try {
      const policy = <IEfusePermission>(
        this.leaguePolicyManager.builder.joinOrgPolicy(joiningOrganizationId, hostOrganizationId)
      );
      const policyExists = await this.permissionsService.exists(<FilterQuery<IEfusePermission>>policy);

      return policyExists;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async canOrgJoinAnyLeagues(organizationId: ObjectId): Promise<boolean> {
    // TODO (Permissions): Temporarily replacing to get things up and running in a post-Casbin world.
    try {
      const policy = <IEfusePermission>this.leaguePolicyManager.builder.joinedOrgsPolicy(organizationId);
      const policyExists = await this.permissionsService.exists(<FilterQuery<IEfusePermission>>policy);

      return policyExists;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async canOrgCreateLeagues(organizationId: ObjectId): Promise<boolean> {
    // TODO (Permissions): Temporarily replacing to get things up and running in a post-Casbin world.
    try {
      const policy = <IEfusePermission>this.leaguePolicyManager.builder.createLeaguePolicy(organizationId);
      const policyExists = await this.permissionsService.exists(<FilterQuery<IEfusePermission>>policy);

      return policyExists;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async createLeagueInvites(league: ILeague): Promise<boolean> {
    const inviteCode: Partial<IInviteCode> = { type: InviteCodeType.ORGANIZATION_LEAGUES, active: true };
    if (league.ownerType === OwnerType.ORGANIZATIONS) {
      inviteCode.organization = league.owner;
    } else if (league.ownerType === OwnerType.USERS) {
      inviteCode.user = league.owner;
    }

    // look for existing organization invite code
    let allOrganizationLeaguesInviteCode = await this.inviteCodeService.findOne(
      {
        type: InviteCodeType.ORGANIZATION_LEAGUES,
        organization: inviteCode.organization,
        user: inviteCode.user
      },
      {},
      { lean: true }
    );

    // only create the org leagues invite if it doesn't exist
    if (!allOrganizationLeaguesInviteCode) {
      allOrganizationLeaguesInviteCode = await this.inviteCodeService.create(inviteCode);
    }

    return allOrganizationLeaguesInviteCode != null;
  }

  @Trace()
  public async getJoinedOrPendingLeagues(
    organizationId: string,
    joinStatus: LeagueJoinStatus
  ): Promise<ILeagueWithJoinStatus[]> {
    // get the ids of teams associated with the current organization
    const teamsOwned = await this.teamService.find({ owner: organizationId }, {}, { lean: true });
    const teamIds = teamsOwned.map((team) => team._id);
    let pendingLeagues: ILeague[] = [];

    const joinedLeagues = await this.find(
      {
        teams: { $in: teamIds }
      },
      {},
      { lean: true }
    );

    // exclude the already joined leagues if trying to get only those that are pending
    if (joinStatus === LeagueJoinStatus.PENDING || joinStatus === LeagueJoinStatus.JOINED_AND_PENDING) {
      pendingLeagues = await this.getPendingLeagues(organizationId, joinedLeagues);
    }

    let leagues = [...this.convertLeaguesToLeaguesWithStatus(pendingLeagues, LeagueJoinStatus.PENDING)];

    if (joinStatus === LeagueJoinStatus.JOINED || joinStatus === LeagueJoinStatus.JOINED_AND_PENDING) {
      leagues = [...this.convertLeaguesToLeaguesWithStatus(joinedLeagues, LeagueJoinStatus.JOINED), ...leagues];
    }

    return leagues;
  }

  @Trace()
  private convertLeaguesToLeaguesWithStatus(leagues: ILeague[], status: LeagueJoinStatus): ILeagueWithJoinStatus[] {
    return leagues.map((league) => {
      return <ILeagueWithJoinStatus>{
        league: league,
        joinStatus: status
      };
    });
  }

  @Trace()
  private async getPendingLeagues(organizationId: string, joinedLeagues: ILeague[]): Promise<ILeague[]> {
    // get all leagues from the orgs of the joined leagues
    const policies = await this.getJoinedLeaguePolicies(organizationId);
    const joinedOrgIds = policies.map((policy) => policy.tenant!);

    const query: FilterQuery<ILeague> = {
      owner: { $in: joinedOrgIds }
    };

    // get league ids of those that the organization has already joined
    const joinedLeagueIds = joinedLeagues.map((league) => league._id);

    if (joinedLeagueIds?.length > 0) {
      query._id = { $nin: joinedLeagueIds };
    }

    // grabs all the leagues of each org that has invited us to join a league
    const leagues = await this.find(query, {}, { lean: true });

    return leagues;
  }

  @Trace()
  private async getJoinedLeaguePolicies(organizationId: string): Promise<IEfusePermission[]> {
    // TODO (Permissions): Temporarily replacing to get things up and running in a post-Casbin world.
    try {
      const policy = <IEfusePermission>this.leaguePolicyManager.builder.joinedOrgsPolicy(organizationId);
      const policies = await this.permissionsService.find(<FilterQuery<IEfusePermission>>policy);

      return policies;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
