import { mock } from "jest-mock-extended";
import { ILeague } from "../../../backend/interfaces";
import { Types } from "mongoose";
import { OwnerType } from "@efuse/entities";

export const mockLeague = () => {
  const league = mock<ILeague>();
  league._id = Types.ObjectId("507f1f77bcf86cd799439079");

  return league;
};

export const customLeague = (args: { ownerType?: OwnerType; name?: string }) => {
  const { ownerType = OwnerType.ORGANIZATIONS, name = "Custom League" } = args;

  const league = mock<ILeague>();
  league._id = Types.ObjectId();
  league.owner = Types.ObjectId();
  league.ownerType = ownerType;
  league.name = name;

  return league;
};
