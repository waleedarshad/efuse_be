import { mock } from "jest-mock-extended";
import { Types } from "mongoose";
import { ILeagueMatch } from "../../../backend/interfaces";
import { LeagueMatchState } from "@efuse/entities";

export const normalMatch = (teams): ILeagueMatch => {
  const matchEvent = mock<ILeagueMatch>();
  matchEvent.round = Types.ObjectId("507f1f77bcf86cd799439015");
  matchEvent.teams = teams;
  matchEvent.state = LeagueMatchState.ACTIVE;
  matchEvent.startTime = new Date(2021, 10, 13);

  return matchEvent;
};
