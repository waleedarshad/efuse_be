import { mock } from "jest-mock-extended";
import { ILeagueEvent } from "../../../backend/interfaces";
import { LeagueBracketType, LeagueEventState, LeagueEventTimingMode } from "@efuse/entities";
import { Types } from "mongoose";

export const returnMockRoundRobinEvent = (): ILeagueEvent => {
  const leagueEvent = mock<ILeagueEvent>();
  leagueEvent._id = Types.ObjectId("507f1f77bcf86cd799439011");
  leagueEvent.bracketType = LeagueBracketType.ROUND_ROBIN;
  leagueEvent.description = "this is description";
  leagueEvent.details = "details";
  leagueEvent.gamesPerMatch = 1;
  leagueEvent.league = Types.ObjectId();
  leagueEvent.maxTeams = 8;
  leagueEvent.name = "League Name";
  leagueEvent.rules = "the rules";
  leagueEvent.startDate = new Date(2021, 10, 13);
  leagueEvent.state = LeagueEventState.ACTIVE;
  leagueEvent.teams = [Types.ObjectId("507f1f77bcf86cd799439012")];
  leagueEvent.timingMode = LeagueEventTimingMode.WEEKLY;

  return leagueEvent;
};

export const returnMockSingleEliminationEvent = (): ILeagueEvent => {
  const leagueEvent = mock<ILeagueEvent>();
  leagueEvent._id = Types.ObjectId("507f1f77bcf86cd799439019");
  leagueEvent.bracketType = LeagueBracketType.SINGLE_ELIM;
  leagueEvent.description = "this is description";
  leagueEvent.details = "details";
  leagueEvent.gamesPerMatch = 1;
  leagueEvent.league = Types.ObjectId();
  leagueEvent.maxTeams = 8;
  leagueEvent.name = "League Name";
  leagueEvent.rules = "the rules";
  leagueEvent.startDate = new Date(2021, 10, 13);
  leagueEvent.state = LeagueEventState.ACTIVE;
  leagueEvent.teams = [Types.ObjectId("507f1f77bcf86cd799439012"), Types.ObjectId("507f1f77bcf86cd799439019")];
  leagueEvent.timingMode = LeagueEventTimingMode.WEEKLY;

  return leagueEvent;
};

export const noTeamsEvent = (): ILeagueEvent => {
  const leagueEvent = mock<ILeagueEvent>();
  leagueEvent.bracketType = LeagueBracketType.ROUND_ROBIN;
  leagueEvent.name = "League Name";
  leagueEvent.league = Types.ObjectId();
  leagueEvent.description = "this is description";
  leagueEvent.gamesPerMatch = 1;
  leagueEvent.startDate = new Date(2021, 10, 13);
  leagueEvent.state = LeagueEventState.ACTIVE;
  leagueEvent.timingMode = LeagueEventTimingMode.WEEKLY;
  leagueEvent.maxTeams = 8;
  leagueEvent.details = "details";
  leagueEvent.rules = "the rules";

  return leagueEvent;
};
