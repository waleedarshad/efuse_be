import { mock } from "jest-mock-extended";
import { Types } from "mongoose";
import { ILeagueRound } from "../../../backend/interfaces";

export const normalRound = (): ILeagueRound => {
  const round = mock<ILeagueRound>();
  round._id = Types.ObjectId("507f1f77bcf86cd799439016");
  round.name = "First Round Name";
  round.value = 1;
  round.startDate = new Date(2021, 10, 13);

  return round;
};

export const secondRound = (): ILeagueRound => {
  const round = mock<ILeagueRound>();
  round._id = Types.ObjectId("507f1f77bcf86cd799439017");
  round.name = "Second Round Name";
  round.value = 2;
  round.startDate = new Date(2021, 10, 13);

  return round;
};

export const thirdRound = (): ILeagueRound => {
  const round = mock<ILeagueRound>();
  round._id = Types.ObjectId("507f1f77bcf86cd799439018");
  round.name = "Third Round Name";
  round.value = 3;
  round.startDate = new Date(2021, 10, 13);

  return round;
};
