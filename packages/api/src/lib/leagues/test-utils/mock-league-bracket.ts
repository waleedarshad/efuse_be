import { mock } from "jest-mock-extended";
import { ILeagueBracket } from "../../../backend/interfaces";
import { LeagueBracketType } from "@efuse/entities";
import { Types } from "mongoose";

export const roundRobinBracket = (): ILeagueBracket => {
  const bracket = mock<ILeagueBracket>();
  bracket._id = Types.ObjectId("507f1f77bcf86cd799439013");
  bracket.name = "Bracket Name";
  bracket.type = LeagueBracketType.ROUND_ROBIN;
  bracket.pool = Types.ObjectId();

  return bracket;
};

export const singleElimBracket = (): ILeagueBracket => {
  const bracket = mock<ILeagueBracket>();
  bracket._id = Types.ObjectId("507f1f77bcf86cd799439017");
  bracket.name = "Single Elimination Bracket";
  bracket.type = LeagueBracketType.SINGLE_ELIM;
  bracket.pool = Types.ObjectId();

  return bracket;
};

export const pointRaceBracket = (): ILeagueBracket => {
  const bracket = mock<ILeagueBracket>();
  bracket._id = Types.ObjectId("507f1f77bcf86cd799439014");
  bracket.name = "Bracket Name";
  bracket.type = LeagueBracketType.POINT_RACE;
  bracket.pool = Types.ObjectId();

  return bracket;
};

export const customBracket = (args: {
  bracketId?: Types.ObjectId;
  bracketName?: string;
  bracketType?: LeagueBracketType;
  poolId?: Types.ObjectId;
}): ILeagueBracket => {
  const {
    bracketId = Types.ObjectId("507f1f77bcf86cd799439018"),
    bracketName = "Custom Bracket",
    bracketType = LeagueBracketType.ROUND_ROBIN,
    poolId = Types.ObjectId()
  } = args;

  const bracket = mock<ILeagueBracket>();
  bracket._id = bracketId;
  bracket.name = bracketName;
  bracket.type = bracketType;
  bracket.pool = poolId;

  return bracket;
};
