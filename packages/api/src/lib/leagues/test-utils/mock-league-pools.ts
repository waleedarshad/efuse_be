import { mock } from "jest-mock-extended";
import { ILeaguePool } from "../../../backend/interfaces";
import { Types } from "mongoose";

export const leaguePool1 = (eventId): ILeaguePool => {
  const pool = mock<ILeaguePool>();
  pool._id = Types.ObjectId();
  pool.name = "pool 1";
  pool.teams = [Types.ObjectId(), Types.ObjectId()];
  pool.event = eventId;

  return pool;
};

export const leaguePool2 = (eventId): ILeaguePool => {
  const pool = mock<ILeaguePool>();
  pool._id = Types.ObjectId();
  pool.name = "pool 2";
  pool.teams = [Types.ObjectId(), Types.ObjectId()];
  pool.event = eventId;

  return pool;
};

export const leaguePool3 = (eventId): ILeaguePool => {
  const pool = mock<ILeaguePool>();
  pool._id = Types.ObjectId();
  pool.name = "pool 3";
  pool.teams = [Types.ObjectId(), Types.ObjectId()];
  pool.event = eventId;

  return pool;
};
