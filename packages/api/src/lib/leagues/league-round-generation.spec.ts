import { LeagueRoundGenerationService } from "./league-round-generation.service";
import { returnMockRoundRobinEvent, returnMockSingleEliminationEvent } from "./test-utils/mock-league-event";
import { roundRobinBracket, pointRaceBracket, singleElimBracket } from "./test-utils/mock-league-bracket";
import { Failure } from "@efuse/contracts";
import { ILeagueRound } from "../../backend/interfaces";
import moment from "moment";

jest.mock("./league-round.service", () => ({
  LeagueRoundService: jest.fn().mockImplementation(() => ({
    create: jest.fn((round): ILeagueRound => round),
    find: jest.fn().mockResolvedValue(false)
  }))
}));

describe("league-round-generation.service", () => {
  let subject: LeagueRoundGenerationService;

  const event = returnMockRoundRobinEvent();
  const numberOfTeams = event.maxTeams / 2;

  beforeEach(() => {
    subject = new LeagueRoundGenerationService();
  });

  it("returns error if leagueBracketType is not round robin", async () => {
    const errorBadRequest = Failure.BadRequest("Rounds unable to be generated do to unimplemented bracket type");
    let thrownError;

    const bracket = pointRaceBracket();

    try {
      await subject.generateRoundsForEvent(bracket, numberOfTeams, event);
    } catch (error) {
      thrownError = error;
    }

    expect(thrownError).toEqual(errorBadRequest);
  });

  it("returns error if round start dates are not a week apart", async () => {
    const bracket = roundRobinBracket();

    const roundOneStart = event.startDate;
    const roundTwoStart = moment(roundOneStart).add(1, "w").toDate();
    const roundThreeStart = moment(roundTwoStart).add(1, "w").toDate();

    const result = await subject.generateRoundsForEvent(bracket, numberOfTeams, event);

    expect(result[0].startDate).toEqual(roundOneStart);
    expect(result[1].startDate).toEqual(roundTwoStart);
    expect(result[2].startDate).toEqual(roundThreeStart);
  });

  it("returns one round if createRoundsForSingleElim has two teams", async () => {
    const event = returnMockSingleEliminationEvent();
    const bracket = singleElimBracket();

    const result = await subject.generateRoundsForEvent(bracket, 2, event);

    expect(result.length).toEqual(1);
  });

  it("returns two rounds if createRoundsForSingleElim has four teams", async () => {
    const event = returnMockSingleEliminationEvent();
    const bracket = singleElimBracket();

    const result = await subject.generateRoundsForEvent(bracket, 4, event);

    expect(result.length).toEqual(2);
  });

  it("returns three rounds if createRoundsForSingleElim has eight teams", async () => {
    const event = returnMockSingleEliminationEvent();
    const bracket = singleElimBracket();

    const result = await subject.generateRoundsForEvent(bracket, 8, event);

    expect(result.length).toEqual(3);
  });

  it("returns two rounds if createRoundsForSingleElim has 3 teams", async () => {
    const event = returnMockSingleEliminationEvent();
    const bracket = singleElimBracket();

    const result = await subject.generateRoundsForEvent(bracket, 3, event);

    expect(result.length).toEqual(2);
  });

  it("returns 5 rounds if createRoundsForSingleElim has 17 teams", async () => {
    const event = returnMockSingleEliminationEvent();
    const bracket = singleElimBracket();

    const result = await subject.generateRoundsForEvent(bracket, 17, event);

    expect(result.length).toEqual(5);
  });
});
