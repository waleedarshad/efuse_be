import { Failure } from "@efuse/contracts";
import { LeagueBracketType, OwnerType } from "@efuse/entities";
import { Types } from "mongoose";
import { ILeagueBracket } from "../../backend/interfaces";

import { LeagueBracket } from "../../backend/models/league";
import { ObjectId } from "../../backend/types";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { LeaguePoolService } from "./league-pool.service";

@Service("league-bracket.service")
export class LeagueBracketService extends BaseModelService<ILeagueBracket> {
  private poolService: LeaguePoolService;

  constructor() {
    super(LeagueBracket);
    this.poolService = new LeaguePoolService();
  }

  @Trace()
  public async createBracketsForPools(
    poolIds: ObjectId[],
    bracketType: LeagueBracketType,
    name?: string
  ): Promise<Types.ObjectId[]> {
    try {
      const bracketIds = await Promise.all(
        poolIds.map(async (poolId) => {
          const poolHasBracket = await this.exists({ pool: poolId });

          if (poolHasBracket) {
            throw Failure.NotAcceptable("Cannot create bracket for a pool that already has a bracket", {
              poolIds,
              bracketType,
              name
            });
          }

          const bracketToSave = <ILeagueBracket>{
            name: name || "New Bracket",
            pool: poolId,
            type: bracketType
          };

          const savedBracket = await this.create(bracketToSave);

          return savedBracket._id;
        })
      );

      return bracketIds;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findOwner(bracketId: ObjectId): Promise<{ owner: ObjectId; ownerType: OwnerType }> {
    const bracket = await this.findOne({ _id: bracketId });

    if (!bracket) {
      throw Failure.BadRequest("Invalid id", bracketId);
    }

    return this.poolService.findOwner(bracket.pool);
  }
}
