import { Queue } from "bull";

import { ISession } from "../../backend/interfaces";
import { Session } from "../../backend/models/session";
import { ObjectId } from "../../backend/types";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import Config from "../../async/config";

const SERVICE = "session.service";

@Service(SERVICE)
export class SessionService extends BaseModelService<ISession> {
  private revokeSessionQueue: Queue;
  private revokeSingleSessionQueue: Queue;

  constructor() {
    super(Session);

    this.revokeSessionQueue = Config.initQueue("revokeSession");
    this.revokeSingleSessionQueue = Config.initQueue("revokeSingleSession");
  }

  /**
   * @summary Revoke session
   *
   * @param {ObjectId} userId - Id of user
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async revokeSession(userId: ObjectId): Promise<void> {
    try {
      const sessions = await this.find({
        user: userId,
        used: false,
        active: true
      });
      if (!sessions || sessions.length === 0) {
        this.logger?.info({ userId }, "No active sessions found!");
      }
      await this.updateMany(
        { _id: { $in: sessions.map((session) => session.id) } },
        { active: false, revokedAt: new Date(Date.now()) }
      );
      this.logger?.info("All sessions have been revoked");
    } catch (error) {
      this.logger?.error(error, "Error while revoking session");
    }
  }

  /**
   * @summary Revoke a single session
   *
   * @param {string} refreshToken - Refresh token of that session
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async revokeSingleSession(refreshToken: string): Promise<void> {
    try {
      const session = await this.findOne({
        refreshToken: refreshToken,
        used: false,
        active: true
      });
      if (session) {
        await this.updateOne({ _id: session._id }, { active: false, revokedAt: new Date(Date.now()) });
      }
    } catch (error) {
      this.logger?.error(error, "Error while revoking a single session");
    }
  }

  /**
   * @summary Revoke session using bull queue
   *
   * @param {ObjectId} userId - Id of user
   *
   * @returns {void}
   */
  @Trace()
  public revokeSessionAsync(userId: ObjectId): void {
    this.revokeSessionQueue
      .add({ userId })
      .catch((error) => this.logger?.error(error, "Error while adding to queue revokeSessionQueue"));
  }

  /**
   * @summary Revoke a single session using bull queue
   *
   * @param {string} refreshToken - Refresh token of that session
   *
   * @returns {void}
   */
  @Trace()
  public revokeSingleSessionAsync(refreshToken: string): void {
    this.revokeSingleSessionQueue
      .add({ refreshToken })
      .catch((error) => this.logger?.error(error, "Error while adding to queue revokeSessionQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const sessionService = new SessionService();

    // revokeSession
    sessionService.revokeSessionQueue
      .process((job: { data: { userId: ObjectId } }) => {
        return sessionService.revokeSession(job.data.userId);
      })
      .catch((error) => sessionService.logger?.error(error, "Error while processing revokeSessionQueue"));

    // revokeSingleSessionQueue
    sessionService.revokeSingleSessionQueue
      .process((job: { data: { refreshToken: string } }) => {
        return sessionService.revokeSingleSession(job.data.refreshToken);
      })
      .catch((error) => sessionService.logger?.error(error, "Error while processing revokeSingleSession"));
  }

  @Trace()
  public async create(sessionBody: Partial<ISession>): Promise<ISession> {
    const doc = <ISession>sessionBody;
    const session = await Session.create(doc);
    return session;
  }
}
