import { Model } from "mongoose";
import moment from "moment";
import { DocumentType } from "@typegoose/typegoose";

import { BaseService } from "../base";
import { User } from "../../backend/models/User";
import { HypeModel } from "../../backend/models/hype/hype.model";
import { Comment } from "../../backend/models/comment";
import { Feed } from "../../backend/models/Feed";
import { HomeFeed } from "../../backend/models/HomeFeed";
import { LoungeFeed } from "../../backend/models/LoungeFeed";
import { Member } from "../../backend/models/member";
import { Organization } from "../../backend/models/Organization";
import { OrganizationFollower } from "../../backend/models/organization-follower";
import { OrganizationRequest } from "../../backend/models/organization-request";
import { GDPRAudit, GDPRAuditModel } from "../../backend/models/gdpr-audit.model";
import { Service, Trace } from "../decorators";

@Service("gdpr-audit.service")
export class GDPRAuditService extends BaseService {
  private $gdprModel: Model<DocumentType<GDPRAudit>> = GDPRAuditModel;

  /**
   * @summary CronJob to remove all the soft deleted records which have been deleted for 7 or more days
   */
  @Trace({ service: "gdpr-audit.service", name: "DeleteSoftDeletedRecords" })
  public static async DeleteSoftDeletedRecords(): Promise<void> {
    const gdprAuditService = new GDPRAuditService();

    // Seven days from now in past
    const timestamp = moment().subtract(7, "d").toDate();

    // Fetch records which have been deleted for 7 says or more
    const query = { deleted: true, deletedAt: { $lte: timestamp } };

    // Using Promise.all as it will try to resolve all promised concurrently
    const deletedRecords = await Promise.all([
      // deletedRecords[0] = user
      User.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from User")),

      // deletedRecords[1] = hypes
      HypeModel.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from Hype")),

      // deletedRecords[2] = comments
      Comment.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from Comment")),

      // deletedRecords[3] = feeds
      Feed.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from Feed")),

      // deletedRecords[4] = homeFeeds
      HomeFeed.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from HomeFeed")),

      // deletedRecords[5] = loungeFeeds
      LoungeFeed.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from LoungeFeed")),

      // deletedRecords[6] = organizations
      Organization.deleteMany(query)
        .exec()
        .catch((error) =>
          gdprAuditService.logger?.error(error, "Error deleting soft deleted records from Organization")
        ),

      // deletedRecords[7] = member
      Member.deleteMany(query)
        .exec()
        .catch((error) => gdprAuditService.logger?.error(error, "Error deleting soft deleted records from Member")),

      // deletedRecords[8] = organizationFollower
      OrganizationFollower.deleteMany(query)
        .exec()
        .catch((error) =>
          gdprAuditService.logger?.error(error, "Error deleting soft deleted records from OrganizationFollower")
        ),

      // deletedRecords[9] = organizationRecords
      OrganizationRequest.deleteMany(query)
        .exec()
        .catch((error) =>
          gdprAuditService.logger?.error(error, "Error deleting soft deleted records from OrganizationRequest")
        )
    ]);

    await gdprAuditService.$gdprModel.create({
      users: deletedRecords[0]?.deletedCount || 0,
      hypes: deletedRecords[1]?.deletedCount || 0,
      comments: deletedRecords[2]?.deletedCount || 0,
      feeds: deletedRecords[3]?.deletedCount || 0,
      homeFeeds: deletedRecords[4]?.deletedCount || 0,
      loungeFeeds: deletedRecords[5]?.deletedCount || 0,
      organizations: deletedRecords[6]?.deletedCount || 0,
      members: deletedRecords[7]?.deletedCount || 0,
      organizationFollowers: deletedRecords[8]?.deletedCount || 0,
      organizationRequests: deletedRecords[9]?.deletedCount || 0
    });
  }
}
