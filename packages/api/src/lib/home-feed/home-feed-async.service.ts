import { Queue } from "bull";
import { Document, PaginateModel } from "mongoose";
import mongooseDelete from "mongoose-delete";
import { HypeEnum } from "@efuse/entities";

import { Service, Trace } from "../decorators";
import { ObjectId } from "../../backend/types";
import { HomeFeed } from "../../backend/models/HomeFeed";
import Config from "../../async/config";
import { Feed } from "../../backend/models/Feed";
import { LoungeFeed } from "../../backend/models/LoungeFeed";
import { IHomeFeed } from "../../backend/interfaces";
import { ILoungeFeed } from "../../backend/interfaces/lounge-feed";
import { BaseService } from "../base";
import { FriendBaseService } from "../friend/friend-base.service";
import { OrganizationFollowerBaseService } from "../organizations/organization-follower-base.service";
import { HypeService } from "../hype/hype.service";
import { Comment } from "../../backend/models/comment";

const SERVICE = "home-feed-async.service";

type Model<T extends Document> = PaginateModel<T> & mongooseDelete.SoftDeleteModel<T>;

@Service(SERVICE)
export class HomeFeedAsyncService extends BaseService {
  private friendBaseService;
  private organizationFollowerBaseService;
  private hypeService;
  private readonly homeFeed: Model<IHomeFeed> = <Model<IHomeFeed>>HomeFeed;
  private readonly loungeFeed: Model<ILoungeFeed> = <Model<ILoungeFeed>>LoungeFeed;
  private updateFollowersHomeFeedQueue: Queue;
  private removeHomeFeedOnUserBlockQueue: Queue;
  private removeHomeFeedOnUserBlockeeQueue: Queue;
  private removeFeedQueue: Queue;
  private addFolloweesFeedToFollowerOnFollowQueue: Queue;

  constructor() {
    super();

    this.friendBaseService = new FriendBaseService();
    this.organizationFollowerBaseService = new OrganizationFollowerBaseService();
    this.hypeService = new HypeService();
    this.updateFollowersHomeFeedQueue = Config.initQueue("updateFollowersHomeFeed");
    this.removeHomeFeedOnUserBlockQueue = Config.initQueue("removeHomeFeedOnUserBlock");
    this.removeHomeFeedOnUserBlockeeQueue = Config.initQueue("removeHomeFeedOnUserBlockee");
    this.removeFeedQueue = Config.initQueue("removeFeed");
    this.addFolloweesFeedToFollowerOnFollowQueue = Config.initQueue("addFolloweesFeedToFollowerOnFollow");
  }

  /**
   * @summary Updated followers homefeed
   *
   * @param {ObjectId} feedId - Id of the feed
   * @param {ObjectId} authorId - Id of the feed author
   * @param {String} timelineableType - Whether it is a users post or organization
   * @return {Promise<void>}
   */
  @Trace()
  public async updateFollowersHomeFeed(feedId: ObjectId, authorId: ObjectId, timelineableType: string): Promise<void> {
    try {
      let friends;

      if (timelineableType === "organizations") {
        friends = await this.organizationFollowerBaseService.find({ followee: authorId }, {}, { lean: true });
      } else {
        friends = await this.friendBaseService.find({ followee: authorId }, {}, { lean: true });
      }

      if (!friends || friends.length === 0) {
        return;
      }

      const homeFeedOperations = friends.map((friend) => {
        const query = {
          user: friend.follower,
          feed: feedId,
          timelineable: authorId,
          timelineableType
        };
        return {
          // using updateOne so that we don't create duplicates
          updateOne: {
            filter: query,
            update: { ...query, updatedAt: Date.now() }, // adding updatedAt as bulk operation won't trigger mongoose middlewares
            upsert: true, // create new object if it is not already there
            setDefaultsOnInsert: true, // this would set the schema defaults
            ordered: false // this would make sure all queries are processed, otherwise mongoose stop processing on first error
          }
        };
      });
      const response = await HomeFeed.bulkWrite(homeFeedOperations);
      this.logger?.info(
        {
          upserted: response.upsertedCount,
          updated: response.modifiedCount,
          writeErrors: response.result.writeErrors
        },
        "Post has been added to all followers timeline"
      );
    } catch (error) {
      this.logger?.error(error, "Error adding post to home feed");
    }
  }

  @Trace()
  public updateFollowersHomeFeedAsync(feedId: ObjectId, authorId: ObjectId, timelineableType: string): any {
    this.updateFollowersHomeFeedQueue
      .add({
        feedId,
        authorId,
        timelineableType
      })
      .catch((error) => this.logger?.error(error, "Error while adding to queue updateFollowersHomeFeedQueue"));
  }

  /**
   * @summary Remove the user post from the blockee homefeed
   *
   * @param {ObjectId} blockerId - Id of the user who is blocking another user
   * @param {ObjectId} blockeeId - Id of the user who is being block by other user
   * @return {Promise<any>}
   */
  @Trace()
  public async removeHomeFeedOnUserBlock(blockeeId: ObjectId, blockerId: ObjectId): Promise<any> {
    try {
      const userFeeds = await Feed.find({ user: blockeeId }).select("_id").lean();
      if (!userFeeds || userFeeds.length === 0) {
        return;
      }
      return await Promise.all(
        userFeeds.map(async (feed) => {
          try {
            return await this.homeFeed.delete({ feed: feed._id, user: blockerId });
          } catch (error) {
            this.logger?.error(error, "Error removing feed");
          }
        })
      );
    } catch (error) {
      this.logger?.error(error, "Error removing feed");
    }
  }

  @Trace()
  public removeHomeFeedOnUserBlockAsync(blockeeId: ObjectId, blockerId: ObjectId): any {
    this.removeHomeFeedOnUserBlockQueue
      .add({
        blockeeId,
        blockerId
      })
      .catch((error) => this.logger?.error(error, "Error while adding to queue updateFollowersHomeFeedQueue"));
  }

  /**
   * @summary Remove the user post from the blocker homefeed
   *
   * @param {ObjectId} blockerId - Id of the user who is blocking another user
   * @param {ObjectId} blockeeId - Id of the user who is being block by other user
   * @return {Promise<any>}
   */
  @Trace()
  public async removeHomeFeedOnUserBlockee(blockeeId: ObjectId, blockerId: ObjectId): Promise<any> {
    try {
      const userFeeds = await Feed.find({ user: blockerId }).select("_id").lean();
      if (!userFeeds || userFeeds.length === 0) {
        return;
      }
      return await Promise.all(
        userFeeds.map(async (feed) => {
          try {
            return await this.homeFeed.delete({ feed: feed._id, user: blockeeId });
          } catch (error) {
            this.logger?.error(error, "Error removing feed");
          }
        })
      );
    } catch (error) {
      this.logger?.error(error, "Error removing feed");
    }
  }

  @Trace()
  public removeHomeFeedOnUserBlockeeAsync(blockeeId: ObjectId, blockerId: ObjectId): any {
    this.removeHomeFeedOnUserBlockeeQueue
      .add({ blockeeId, blockerId })
      .catch((error) => this.logger?.error(error, "Error while adding to queue removeHomeFeedOnUserBlockeeQueue"));
  }

  /**
   * @summary Delete a feed and its related likes and comments
   *
   * @param {ObjectId} feedId - Id of the feed to delete
   *
   * @return {Promise<any>}
   */
  @Trace()
  public async removeFeed(feedId: ObjectId): Promise<void> {
    try {
      await this.homeFeed.delete({ feed: feedId });
      await this.hypeService.deleteOne({ objId: feedId, objType: HypeEnum.FEED });
      await Comment.delete({ commentable: feedId, commentableType: "feeds" });

      await this.loungeFeed.delete({ feed: feedId });
    } catch (error) {
      this.logger?.error(error, "Error removing deleted feed");
    }
  }

  @Trace()
  public removeFeedAsync(feedId: ObjectId) {
    this.removeFeedQueue
      .add({ feedId })
      .catch((error) => this.logger?.error(error, "Error while adding to queue removeFeedQueue"));
  }

  /**
   * @summary Add the users feed to followees feed when that user is followed by another user
   *
   * @param {ObjectId} followeeId - Id of the user who is being followed by other user
   * @param {ObjectId} followerId - Id of the user who is following another user
   * @return {Promise<any>}
   */
  @Trace()
  public async addFolloweesFeedToFollowerOnFollow(followeeId: ObjectId, followerId: ObjectId): Promise<any> {
    try {
      // Adding only those feed which are not deleted
      const userFeeds = await Feed.find(
        {
          user: followeeId,
          deleted: { $ne: true }
        },
        {},
        { autopopulate: false }
      )
        .select("_id createdAt")
        .lean();
      if (!userFeeds || userFeeds.length === 0) {
        this.logger?.info({ followeeId, followerId }, "Feed not found");
      }
      return await Promise.all(
        userFeeds.map(async (feed) => {
          try {
            const checkFeed = await this.homeFeed.find({
              feed: feed._id,
              user: followerId
            });
            if (checkFeed.length === 0) {
              await this.homeFeed.create({
                user: followerId,
                feed: feed._id,
                timelineable: followeeId,
                timelineableType: "users",
                createdAt: feed.createdAt
              });
            }
          } catch (error) {
            this.logger?.error(error, "Error adding followees feed to follower");
          }
        })
      );
    } catch (error) {
      this.logger?.error(error, "Error adding followees feed to follower");
    }
  }

  @Trace()
  public addFolloweesFeedToFollowerOnFollowAsync(followeeId: ObjectId, followerId: ObjectId): void {
    this.addFolloweesFeedToFollowerOnFollowQueue
      .add({
        followeeId,
        followerId
      })
      .catch((error) =>
        this.logger?.error(error, "Error while adding to queue addFolloweesFeedToFollowerOnFollowQueue")
      );
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const homeFeedAsyncService = new HomeFeedAsyncService();

    // updateFollowersHomeFeed
    homeFeedAsyncService.updateFollowersHomeFeedQueue
      .process((job: { data: { feedId: ObjectId; authorId: ObjectId; timelineableType: string } }) => {
        const { feedId, authorId, timelineableType } = job.data;
        return homeFeedAsyncService.updateFollowersHomeFeed(feedId, authorId, timelineableType);
      })
      .catch((error) => homeFeedAsyncService.logger?.error(error, "Error while processing updateFollowersHomeFeed"));

    // removeHomeFeedOnUserBlock
    homeFeedAsyncService.removeHomeFeedOnUserBlockQueue
      .process((job: { data: { blockeeId: ObjectId; blockerId: ObjectId } }) => {
        const { blockeeId, blockerId } = job.data;
        return homeFeedAsyncService.removeHomeFeedOnUserBlock(blockeeId, blockerId);
      })
      .catch((error) => homeFeedAsyncService.logger?.error(error, "Error while processing removeHomeFeedOnUserBlock"));

    // removeHomeFeedOnUserBlockee
    homeFeedAsyncService.removeHomeFeedOnUserBlockeeQueue
      .process((job: { data: { blockeeId: ObjectId; blockerId: ObjectId } }) => {
        const { blockeeId, blockerId } = job.data;
        return homeFeedAsyncService.removeHomeFeedOnUserBlockee(blockeeId, blockerId);
      })
      .catch((error) =>
        homeFeedAsyncService.logger?.error(error, "Error while processing removeHomeFeedOnUserBlockee")
      );

    // removeFeed
    homeFeedAsyncService.removeFeedQueue
      .process((job: { data: { feedId: ObjectId } }) => {
        const { feedId } = job.data;
        return homeFeedAsyncService.removeFeed(feedId);
      })
      .catch((error) => homeFeedAsyncService.logger?.error(error, "Error while processing removeFeed"));

    // addFolloweesFeedToFollowerOnFollowQueue
    homeFeedAsyncService.addFolloweesFeedToFollowerOnFollowQueue
      .process((job: { data: { followeeId: ObjectId; followerId: ObjectId } }) => {
        const { followeeId, followerId } = job.data;
        return homeFeedAsyncService.addFolloweesFeedToFollowerOnFollow(followeeId, followerId);
      })
      .catch((error) =>
        homeFeedAsyncService.logger?.error(error, "Error while processing addFolloweesFeedToFollowerOnFollow")
      );
  }
}
