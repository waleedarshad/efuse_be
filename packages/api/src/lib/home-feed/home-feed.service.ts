import { Failure } from "@efuse/contracts";
import { isEmpty } from "lodash";
import { Model, Types, PaginateResult, FilterQuery, QueryOptions, PaginateOptions, PaginateModel } from "mongoose";

import { BaseService } from "../base/base.service";
import { formatHomeFeedPosts } from "../../backend/helpers/feed.helper";
import { IFeedList } from "../../backend/interfaces/feed/feed";
import { IHomeFeed } from "../../backend/interfaces/home-feed";
import { IPostFeedItem } from "../../backend/interfaces/feed/post-feed-item";
import { Service, Trace } from "../decorators";
import { HomeFeed } from "../../backend/models/HomeFeed";
import ViewsLib from "../views";
import { PaginationHelper } from "../../graphql/helpers/pagination.helper";
@Service("home-feed.service")
export class HomeFeedService extends BaseService {
  private readonly homeFeeds: Model<IHomeFeed> & PaginateModel<IHomeFeed> = HomeFeed;

  @Trace()
  public async create(homeFeedParams: IHomeFeed): Promise<IHomeFeed> {
    if (!homeFeedParams) {
      throw Failure.BadRequest("required params are not found");
    }

    const doc = homeFeedParams;
    const homeFeed = await this.homeFeeds.create(doc);
    return homeFeed;
  }

  /**
   * Retrieve a user's feed that should display on their profile.
   *
   * @param currentUserId
   * @param userId
   * @param page
   * @param limit
   */
  @Trace()
  public async getUserFeed(
    currentUserId: string | Types.ObjectId,
    userId: string | Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    const homeFeed = await this.aggregateUserFeed(currentUserId, userId, page, limit);

    const feeds = homeFeed.docs.map((feed: IHomeFeed) => {
      const formattedFeed = feed ? formatHomeFeedPosts(feed, currentUserId) : null;
      return formattedFeed;
    });

    const results: (IPostFeedItem | null)[] = await Promise.all(feeds);

    // remove empty docs
    const filterDocs = results.filter((doc) => !isEmpty(doc));

    const extendHomeFeed: { [k: string]: any } = { ...homeFeed };

    extendHomeFeed.docs = results;

    ViewsLib.incrementFeedViews(filterDocs.map((doc) => doc && doc.feedId)).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    return <PaginateResult<IFeedList>>extendHomeFeed;
  }

  /**
   * Retrieve org's posts to display on it's profile.
   *
   * @param currentUserId
   * @param feedOwners
   * @param orgId
   * @param page
   * @param limit
   */
  @Trace()
  public async getOrganizationHomeFeed(
    currentUserId: string | Types.ObjectId,
    feedOwners: string[] | Types.ObjectId[],
    orgId: string | Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    const homeFeed = await this.aggregateOrganizationFeed(currentUserId, feedOwners, orgId, page, limit);

    const feeds = homeFeed.docs.map((feed: IHomeFeed) => {
      const formattedFeed = feed ? formatHomeFeedPosts(feed, currentUserId) : null;
      return formattedFeed;
    });

    const results: (IPostFeedItem | null)[] = await Promise.all(feeds);

    // remove empty docs
    const filterDocs = results.filter((doc) => !isEmpty(doc));

    const extendHomeFeed: { [k: string]: any } = { ...homeFeed };

    extendHomeFeed.docs = filterDocs;

    ViewsLib.incrementFeedViews(filterDocs.map((doc) => doc && doc.feedId)).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    return <PaginateResult<IFeedList>>extendHomeFeed;
  }

  /**
   * The home feed is what we now call the "following" feed
   *
   * @param currentUserId
   * @param page
   * @param limit
   */
  @Trace()
  public async getFollowingFeed(
    currentUserId: string | Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    const homeFeed = await this.paginatedHomeFeed(currentUserId, page, limit);

    const feeds = homeFeed.docs.map((feed: IHomeFeed) => {
      const formattedFeed = feed ? formatHomeFeedPosts(feed, currentUserId) : null!;
      return formattedFeed;
    });

    const results: (IPostFeedItem | null)[] = await Promise.all(feeds);

    const extendHomeFeed: { [k: string]: any } = { ...homeFeed };

    // remove empty docs
    const filterDocs = results.filter((doc) => !isEmpty(doc));

    extendHomeFeed.docs = filterDocs;

    ViewsLib.incrementFeedViews(filterDocs.map((doc) => doc && doc.feedId)).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    return <PaginateResult<IFeedList>>extendHomeFeed;
  }

  @Trace()
  public async getHomeFeedPost(id: string, currentUserId: string): Promise<IPostFeedItem> {
    const feed = await this.homeFeeds.findOne({ _id: id }, {}, { lean: true });

    if (!feed) {
      throw Failure.NotFound("Could not find home feed.");
    }

    const formattedFeed = await formatHomeFeedPosts(feed, currentUserId);

    if (!formattedFeed) {
      throw Failure.InternalServerError("Unable to format feed", feed);
    }

    ViewsLib.incrementFeedViews([id]).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    return formattedFeed;
  }

  @Trace()
  public async paginatedHomeFeed(
    userId: string | Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IHomeFeed>> {
    const paginationHelper = new PaginationHelper();
    const { page: pageParam, limit: limitParam } = paginationHelper.validatePaginationParams(page, limit);

    const paginatedFeed = await HomeFeed.paginate(
      {
        user: new Types.ObjectId(userId),
        deleted: { $ne: true }
      },
      {
        page: pageParam,
        limit: limitParam,
        populate: [],
        lean: true,
        sort: { createdAt: -1 }
      }
    );
    return paginatedFeed;
  }

  @Trace()
  public async aggregateOrganizationFeed(
    currentUserId: string | Types.ObjectId,
    feedOwners: string[] | Types.ObjectId[],
    orgId: string | Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IHomeFeed>> {
    const aggregateValue = [
      {
        $match: {
          user: { $in: feedOwners },
          timelineableType: "organizations",
          timelineable: orgId,
          original: true,
          deleted: { $ne: true }
        }
      },
      {
        $lookup: {
          from: "feeds",
          localField: "feed",
          foreignField: "_id",
          as: "postFeed"
        }
      },
      {
        $unwind: {
          path: "$postFeed"
        }
      },
      { $match: { postFeed: { $ne: null } } },
      {
        $match: {
          "postFeed.deleted": { $ne: true },
          "postFeed.reports.reportedBy": { $ne: new Types.ObjectId(currentUserId) }
        }
      }
    ];

    // create an aggregate promise that retrieves the appropriate posts

    const feedAggregate = HomeFeed.aggregate(aggregateValue);

    // paginate the aggregate's results
    // todo: investigate & fix
    // @ts-ignore
    const homeFeed = await HomeFeed.aggregatePaginate(feedAggregate, {
      page,
      limit,
      populate: [],
      lean: true,
      sort: { createdAt: -1 }
    });

    return <PaginateResult<IHomeFeed>>homeFeed;
  }

  @Trace()
  public async aggregateUserFeed(
    currentUserId: string | Types.ObjectId,
    userId: string | Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IHomeFeed>> {
    const aggregateValue = [
      {
        $match: {
          user: new Types.ObjectId(userId),
          timelineableType: "users",
          timelineable: new Types.ObjectId(userId),
          deleted: { $ne: true }
        }
      },
      {
        $lookup: {
          from: "feeds",
          localField: "feed",
          foreignField: "_id",
          as: "postFeed"
        }
      },
      {
        $unwind: {
          path: "$postFeed"
        }
      },
      {
        $match: {
          "postFeed.user": new Types.ObjectId(userId),
          "postFeed.deleted": { $ne: true },
          "postFeed.reports.reportedBy": { $ne: new Types.ObjectId(currentUserId) }
        }
      }
    ];

    // create an aggregate promise that retrieves the appropriate posts

    const userPostsAggregate = HomeFeed.aggregate(aggregateValue);

    // paginate the aggregate's results
    // todo: investigate & fix
    // @ts-ignore
    const userPosts = await HomeFeed.aggregatePaginate(userPostsAggregate, {
      page,
      limit,
      populate: [],
      lean: true,
      sort: { createdAt: -1 }
    });

    return <PaginateResult<IHomeFeed>>userPosts;
  }

  public async findOne(filter: FilterQuery<IHomeFeed>): Promise<IHomeFeed | null>;
  public async findOne(filter: FilterQuery<IHomeFeed>, projection: any): Promise<IHomeFeed | null>;
  public async findOne(
    filter: FilterQuery<IHomeFeed>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IHomeFeed | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IHomeFeed>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IHomeFeed | null> {
    const homeFeed = await this.homeFeeds.findOne(filter, projection, options);
    return homeFeed;
  }

  /**
   * @summary Get paginated homeFeed
   *
   * @param {FilterQuery<IHomeFeed>} filter
   * @param {PaginateOptions} options
   *
   * @returns {Promise<PaginateResult<IHomeFeed>>}
   */
  @Trace()
  public async getPaginatedHomeFeed(
    filter: FilterQuery<IHomeFeed>,
    options: PaginateOptions
  ): Promise<PaginateResult<IHomeFeed>> {
    const homeFeed = await this.homeFeeds.paginate(filter, options);

    return homeFeed;
  }

  public async find(filter: FilterQuery<IHomeFeed>): Promise<IHomeFeed[]>;
  public async find(filter: FilterQuery<IHomeFeed>, projection: any): Promise<IHomeFeed[]>;
  public async find(
    filter: FilterQuery<IHomeFeed>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IHomeFeed[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IHomeFeed>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IHomeFeed[]> {
    const homeFeed = await this.homeFeeds.find(filter, projection, options);
    return homeFeed;
  }
}
