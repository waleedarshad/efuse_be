import mongoose from "mongoose";
import { ILogger, Logger } from "@efuse/logger";
import cachegoose from "cachegoose";

import { MONGO_DB_URL, MONGO_DB_POOL_SIZE } from "../../config";

export class MongodbService {
  private static _mongoose: mongoose.Mongoose;
  private static _logger: ILogger = Logger.create({ name: "mongodb.service" });

  private constructor() {}

  public static async initialize(): Promise<mongoose.Mongoose> {
    if (this._mongoose) {
      return this._mongoose;
    }

    this._logger.info("Initializing MongoDB connection");

    const start = new Date().getTime();

    this._mongoose = await mongoose.connect(MONGO_DB_URL, {
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      poolSize: <number>MONGO_DB_POOL_SIZE
    });

    const end = new Date().getTime();

    this._logger.info(`MongoDB Connected in ${(end - start) / 1000}s`);

    cachegoose(this._mongoose);

    return this._mongoose;
  }

  public static get mongoose(): mongoose.Mongoose {
    return this._mongoose;
  }

  public static get connection(): mongoose.Connection {
    return this._mongoose.connection;
  }
}
