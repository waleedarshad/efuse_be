import { Report } from "../../backend/models/report.model";
import { ReportInput } from "../../graphql/schema/report/report.input";
import { ReportService } from "./report.service";
import { report_data } from "./test-utils/mock-report";

jest.mock("../slack");
jest.mock("./report.service", () => ({
  ReportService: jest.fn().mockImplementation(() => ({
    createReport: jest.fn((report): Report => report),
    Slack: {
      sendMessage: jest.fn().mockReturnValue(true)
    }
  }))
}));

describe("report.service", () => {
  let subject: ReportService;
  let data = {} as ReportInput;

  beforeEach(() => {
    subject = new ReportService();
    data = report_data();
  });

  it("it should report comment", async () => {
    const report = await subject.createReport(data);
    expect(report).toEqual(data);
  });
});
