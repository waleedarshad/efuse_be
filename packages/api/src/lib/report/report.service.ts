import { DocumentType } from "@typegoose/typegoose";
import { BaseModelService } from "../base/base-model.service";
import { Service } from "../decorators";
import { Report, ReportModel } from "../../backend/models/report.model";
import { ReportInput } from "../../../src/graphql/schema/report/report.input";
@Service("report.service")
export class ReportService extends BaseModelService<DocumentType<Report>> {
  private $reportModel = ReportModel;
  constructor() {
    super(ReportModel);
  }

  /**
   * @summary Override create method to pass along object instead of DocumentType
   * @param body Report object
   * @returns Promise<Report>
   */
  public async createReport(body: ReportInput): Promise<Report> {
    const report = await this.$reportModel.create(body);
    return report;
  }
}
