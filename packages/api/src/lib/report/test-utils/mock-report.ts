import { mock } from "jest-mock-extended";
import { Types } from "mongoose";
import { ReportEnum } from "@efuse/entities";
import { ReportInput } from "packages/api/src/graphql/schema/report/report.input";

export const report_data = (): ReportInput => {
  const reportData = mock<ReportInput>();

  reportData.reportItem = "61764e845235bd00401c9fc2";
  reportData.reportkind = ReportEnum.COMMENT;
  reportData.user = Types.ObjectId("615169a6b870e2003d0812f4");
  reportData.reportReason = "Testing!";

  return reportData;
};
