import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions, Types, UpdateQuery, UpdateWriteOpResult } from "mongoose";

import { BaseService } from "./base/base.service";
import { IOAuthCredential } from "../backend/interfaces";
import { OAuthCredential, OAuthCredentialModel } from "../backend/models/oauth-credential";
import { Service, Trace } from "./decorators";

@Service("oauth-credential.service")
export class OAuthCredentialService extends BaseService {
  protected model: OAuthCredentialModel<IOAuthCredential> = OAuthCredential;

  @Trace()
  public async create(item: IOAuthCredential): Promise<IOAuthCredential> {
    try {
      const result: IOAuthCredential = await this.model.create(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createMany(items: IOAuthCredential[]): Promise<IOAuthCredential[]> {
    try {
      const result: IOAuthCredential[] = await this.model.insertMany(items);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async delete(id: Types.ObjectId | string): Promise<boolean> {
    if (!this.isValidObjectId(id)) {
      throw Failure.BadRequest("Invalid id", id);
    }

    try {
      const deleted = await this.model.findByIdAndDelete(id);

      return deleted != null;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async deleteByOwnerAndService(owner: string, service: string): Promise<void> {
    await this.deleteOne({ owner, service });
  }

  @Trace()
  public async deleteOne(
    filter?: FilterQuery<IOAuthCredential> | undefined,
    options?: QueryOptions | undefined
  ): Promise<boolean> {
    try {
      const deleted = await this.model.deleteOne(filter, options);

      return deleted != null;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async exists(filter: FilterQuery<IOAuthCredential>): Promise<boolean> {
    try {
      const match = await this.model.exists(filter);

      return match;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async find(
    filter: FilterQuery<IOAuthCredential>,
    projection?: any,
    options?: QueryOptions
  ): Promise<IOAuthCredential[]> {
    try {
      const item: IOAuthCredential[] = await this.model.find(filter, projection, options);

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findOne(
    filter: FilterQuery<IOAuthCredential>,
    projection?: any,
    options?: QueryOptions
  ): Promise<IOAuthCredential | null> {
    try {
      const item: IOAuthCredential | null = await this.model.findOne(filter, projection, options);

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async getConnectionStatus(owner: string, service: string): Promise<boolean> {
    const oauthCredential = await this.findOne({ owner, service });

    return oauthCredential !== undefined && oauthCredential !== null;
  }

  @Trace()
  public async updateOne(
    condition: FilterQuery<IOAuthCredential>,
    body: UpdateQuery<IOAuthCredential>,
    options?: QueryOptions
  ): Promise<IOAuthCredential> {
    if (!body) {
      throw Failure.BadRequest("Body is missing for update");
    }

    try {
      const item: IOAuthCredential | null = await this.model.findOneAndUpdate(condition, body, options);

      if (!item) {
        throw Failure.UnprocessableEntity("No such item available for update", { condition, body, options });
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async updateMany(
    condition: FilterQuery<IOAuthCredential>,
    body: UpdateQuery<IOAuthCredential>,
    options?: QueryOptions
  ): Promise<IOAuthCredential[]> {
    if (!body) {
      throw Failure.BadRequest("Body is missing for update");
    }

    try {
      const update: UpdateWriteOpResult = await this.model.updateMany(condition, body, options);

      if (update.nModified < 1) {
        throw Failure.UnprocessableEntity("No such items available for update", { condition, body, options });
      }

      const items: IOAuthCredential[] = await this.find(condition, {}, options);

      return items;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
