export class CrossPostPlatforms {
  public static readonly TWITTER = "twitter";
  public static readonly DISCORD = "discord";
  public static readonly FACEBOOK = "facebook";
}
