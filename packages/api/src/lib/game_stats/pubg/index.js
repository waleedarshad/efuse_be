const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/game_stats/pubg/index.js" });

const { User } = require("../../../backend/models/User");
const { Pubg } = require("../../../backend/models/Pubg");

const PUBGService = require("./service");
const PubgAsync = require("../../../async/stats/pubg");
const { io } = require("../../../backend/config/get-io-object");

/**
 * @summary Sync pubg stats with efuse account
 *
 * @param {Object} user - User information i.e id, name, email etc
 * @returns {Promise<any>} undefined
 */
const syncPubgStats = async (user) => {
  try {
    const logMsg = `[${user.email}]:`;
    const { platforms } = user.pubg;

    const tempStats = [];

    for (const pubgCreds of platforms) {
      try {
        logger.info(`${logMsg} Fetching stats for platform ${pubgCreds.platform}`);

        // todo: investigate
        // @ts-ignore

        const initPubg = new PUBGService(pubgCreds.platform, pubgCreds.username, pubgCreds.accountId);

        const pubgService = await initPubg.getSeasonStats();

        if (!pubgService.success) {
          logger.info(`${logMsg} Stats not found for platform ${pubgCreds.platform}`);
          continue;
        }

        pubgCreds.accountId = pubgService.accountId;

        tempStats.push({ ...pubgService.stats, platform: pubgCreds.platform });
      } catch (error) {
        logger.info({ error, pubgCreds }, `${logMsg} Error fetching stats`);
      }
    }

    const stats = tempStats.filter((stat) => stat);

    if (!stats || stats.length === 0) {
      logger.info(`${logMsg} Stats not found`);
      return;
    }

    try {
      const pubgStats = await Pubg.insertMany(stats);

      if (!pubgStats || pubgStats.length === 0) {
        logger.info(`${logMsg} Got Stats but none Updated`);
        return;
      }

      await Pubg.deleteMany({ _id: { $in: user.pubg.stats } }).exec();

      const updatedUser = await User.findOneAndUpdate(
        { _id: user._id },
        { pubg: { stats: pubgStats.map((stat) => stat._id), platforms } },
        // todo: investigate
        // @ts-ignore
        { new: true, select: "_id pubg email" }
      ).lean();

      // Need to test once testing accounts available
      // todo: investigate
      // @ts-ignore
      io.to(updatedUser._id).emit("UPDATE_ACCOUNT_STATS", {
        accountName: "pubg",
        stats: {
          // todo: investigate
          // @ts-ignore
          platforms: updatedUser.pubg.platforms,
          stats: pubgStats
        }
      });

      logger.info(`${logMsg} Updated stats`);
    } catch (err) {
      logger.error(err, "Error while updating");
    }
  } catch (error) {
    logger.error(error, "Error syncing pubg stats");
  }
};

const syncPubgStatsAsync = async (user) => {
  return await PubgAsync.syncPubgStatsQueue.add({ user });
};

/**
 * @summary Add all users in queue to sync pubg stats
 *
 * @returns {Promise<undefined>} undefined
 */
const syncPubgStatsCron = async () => {
  try {
    const users = await User.find(
      {
        // todo: investigate
        // @ts-ignore
        pubg: { $ne: null },
        "pubg.platforms": { $ne: [] }
      },
      {},
      { autopopulate: false }
    )
      .select("_id pubg email")
      .lean();
    if (!users || users.length === 0) {
      logger.info("No users found!");
      return;
    }
    logger.info(`Total users found to sync stats ${users.length}`);
    await Promise.all(
      users.map(async (user) => {
        return await syncPubgStatsAsync(user);
      })
    );
    logger.info("All jobs added to queue syncPubgStats");
    return;
  } catch (error) {
    logger.error(error, "Error iterating users to sync pubg stats");
    return;
  }
};

module.exports.syncPubgStats = syncPubgStats;
module.exports.syncPubgStatsAsync = syncPubgStatsAsync;
module.exports.syncPubgStatsCron = syncPubgStatsCron;
