const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/game_stats/pubg/service.js" });
const axiosLib = require("axios");

const { PUBG_API_KEY } = require("@efuse/key-store");

// todo: investigate & fix
// @ts-ignore
const axios = axiosLib.create({
  headers: {
    Accept: "application/vnd.api+json",
    Authorization: `Bearer ${PUBG_API_KEY}`
  }
});

/**
 * @summary Set platform, user name and account id to get stats for pubg
 *
 * @param {String} platform - Name of the platform
 * @param {String} username - Username
 * @param {String} accountId - Account id of the user
 */
function PUBGService(platform, username, accountId) {
  this.platform = platform;
  this.username = username;
  this.accountId = accountId;
  axios.defaults.baseURL = `https://api.pubg.com/shards/${this.platform}`;
}

PUBGService.prototype = {
  // todo: investigate
  // @ts-ignore
  async getSeasonStats() {
    const endpoint = "/players";

    try {
      if (!this.accountId) {
        const response = await axios.get(`${endpoint}?filter[playerNames]=${this.username}`);
        const { data, errors } = response.data;

        if (errors) {
          throw new Error(`PUBG reported errors: ${errors}`);
        }

        if (!data || (data && data.length === 0)) {
          logger.info(`No PUBG players found matching criteria: ${this.username}`);
        } else {
          this.accountId = data[0].id;
        }
      }

      const response = await axios.get(`${endpoint}/${this.accountId}/seasons/lifetime`);
      const { data, errors } = response.data;

      if (errors) {
        throw new Error(`PUBG reported errors: ${errors}`);
      }

      if (!data || (data && data.length === 0)) {
        logger.info(`No PUBG stats found for accountId: ${this.accountId}`);
      } else {
        this.stats = data;
        this.success = true;
      }

      return this;
    } catch (error) {
      if ([429, 404].includes(error?.response?.status)) {
        logger.info({ error }, error.message);

        this.success = false;

        return this;
      }

      logger.error(
        { accountId: this.accountId, username: this.username },
        `An error occurred while sending request to the PUBG endpoint '${endpoint}'): ${error.message}`
      );
    }
  }
};

module.exports.PUBGService = PUBGService;
