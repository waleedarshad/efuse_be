import { Trace } from "@efuse/decorators";
import { IInviteCode } from "../../backend/interfaces/invite-code";
import { BaseModelService } from "../base";
import { Service } from "../decorators";
import { InviteCode, InviteCodeModel } from "../../backend/models/invite-code";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { InviteCodeType } from "@efuse/entities";

@Service("invite-code.service")
export class InviteCodeService extends BaseModelService<IInviteCode> {
  private inviteCode: InviteCodeModel<IInviteCode>;

  constructor() {
    super(InviteCode);

    this.inviteCode = InviteCode;
  }

  @Trace()
  public async create(item: Partial<IInviteCode>): Promise<IInviteCode> {
    try {
      if (!item.code) {
        item.code = this.generateCode(item.type);
      }

      const result: IInviteCode = await this.inviteCode.create(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createMany(items: Partial<IInviteCode>[]): Promise<IInviteCode[]> {
    try {
      const itemsWithCodes = items.map((ic) => {
        if (!ic.code) {
          ic.code = this.generateCode(ic.type);
        }
        return ic;
      });

      const result: IInviteCode[] = await this.inviteCode.insertMany(itemsWithCodes);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public generateCode(type: InviteCodeType | undefined, length = 8): string {
    return this.getStartCharacters(type) + GeneralHelper.randomString(length, "123456789ABCDEFGHIJKLMNPQRSTUVWXYZ");
  }

  private getStartCharacters(type: InviteCodeType | undefined): string {
    switch (type) {
      case InviteCodeType.LEAGUE:
        return "LG";
      case InviteCodeType.LEAGUE_EVENT:
        return "EV";
      case InviteCodeType.ORGANIZATION:
        return "OR";
      case InviteCodeType.ORGANIZATION_LEAGUES:
        return "OL";
      case InviteCodeType.ORGANIZATION_MEMBER_INVITE:
        return "OM";
      case InviteCodeType.USER:
        return "US";
      default:
        return "";
    }
  }
}
