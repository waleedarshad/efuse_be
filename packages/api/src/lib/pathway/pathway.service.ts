import { FilterQuery, QueryOptions } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

import { PathwayModel, Pathway } from "../../backend/models/pathway";
import { IPathway } from "../../backend/interfaces/pathway";

@Service("pathway.service")
export class PathwayService extends BaseService {
  private readonly pathways: PathwayModel<IPathway> = Pathway;

  /**
   * @summary Get all active pathways
   *
   * @return {Promise<IPathway[]>}
   */
  @Trace()
  public async getActive(): Promise<IPathway[]> {
    const pathways: IPathway[] = await this.pathways.find({ active: true }).sort({ createdAt: -1 }).lean();
    return pathways;
  }

  public async findOne(filter: FilterQuery<IPathway>): Promise<IPathway | null>;
  public async findOne(filter: FilterQuery<IPathway>, projection: any): Promise<IPathway | null>;
  public async findOne(
    filter: FilterQuery<IPathway>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IPathway | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IPathway>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IPathway | null> {
    const pathway = await this.pathways.findOne(filter, projection, options);
    return pathway;
  }
}
