import { BULLET_TRAIN_EMAIL, BULLET_TRAIN_ENVIRONMENT, BULLET_TRAIN_PASSWORD } from "@efuse/key-store";
import * as crypto from "crypto";
import axios, { AxiosInstance, AxiosResponse } from "axios";
import { ILogger, Logger } from "@efuse/logger";

import {
  IFlagsmithFlag,
  IFlagsmithIdentity,
  IFlagsmithIdentityWithTraits,
  IFlagsmithSegment,
  IFlagsmithFeaturestate,
  IFlagsmithFeaturestateResult
} from "../../backend/interfaces/flagsmith";
import { FeatureFlagAsyncService } from "./feature-flag-async.service";
import * as RedisHelper from "../redis/helpers";
import { Service, Trace } from "../decorators";

const logger: ILogger = Logger.create();

const API_URL = "https://api.flagsmith.com/api/v1/";
const FLAG_CACHE_EXPIRATION = 60;
const HASH_ALGORITHM = "sha256";
const HASH_ENCODING = "base64";
const IDENTITY_CACHE_EXPIRATION = 86400;
const INVALID_KEY = "<INVALID_KEY>";
const MISSING_HASH = "<MISSING>";

const REDIS_KEYS = {
  flags: "feature_flags:flags",
  hash: "feature_flags:hash_values",
  identities: "feature_flags:identities",
  segments: "feature_flags:segments"
};

const ROUTES = {
  auth: "/auth/login/",
  environments: "/environments/",
  featurestates: "/featurestates/",
  flags: "/flags/",
  identities: "/identities/",
  segments: "/segments/"
};

const AsyncFeatureFlags = new FeatureFlagAsyncService();

@Service("feature-flag.service")
export class FeatureFlagService {
  private static authorizedHttp: AxiosInstance | undefined;
  private static apiKey = BULLET_TRAIN_ENVIRONMENT;
  private static credentials = { email: BULLET_TRAIN_EMAIL, password: BULLET_TRAIN_PASSWORD };
  private static http = axios.create({
    baseURL: API_URL,
    headers: { "X-Environment-Key": FeatureFlagService.apiKey },
    timeout: 5000
  });

  @Trace()
  public static async getAllFlags(): Promise<IFlagsmithFlag[]> {
    const cache = await this.checkCache<IFlagsmithFlag[]>(REDIS_KEYS.flags);

    if (!cache) {
      logger.warn("Global feature flags not found in redis.  Refreshing! (NOTE:  This should rarely happen)");
      AsyncFeatureFlags.refreshGlobalFeatureFlags();
      return [];
    }

    return cache;
  }

  @Trace()
  public static async getAllFlagsForSegment(): Promise<IFlagsmithSegment[]> {
    const cache = await this.checkCache<IFlagsmithSegment[]>(REDIS_KEYS.segments);

    if (!cache) {
      try {
        const response = await this.http.get<IFlagsmithSegment[]>(ROUTES.segments);

        await RedisHelper.setAsync(REDIS_KEYS.segments, JSON.stringify(response.data), 86400);
        return response.data;
      } catch (error: unknown) {
        logger.error({ error }, `Unable to hit bullet train ${ROUTES.segments} endpoint`);
      }
    }

    return <IFlagsmithSegment[]>cache;
  }

  @Trace()
  public static async getAllFlagsForUser(userId: string): Promise<IFlagsmithIdentityWithTraits> {
    /**
     * Asynchronously refresh flags for user, Using setTimeout here
     * so the refresh does not show up in the web requests in APM
     */
    AsyncFeatureFlags.refreshUserFeatureFlagsAsync(userId);

    const hash = await this.checkCache<string>(`${REDIS_KEYS.identities}:${userId}`);
    const cache = await this.checkCache<IFlagsmithIdentityWithTraits>(`${REDIS_KEYS.hash}:${hash || MISSING_HASH}`);

    /**
     * If no results exist, let's asynchronously refresh the user's
     * feature flags and return defaults
     */
    if (!cache) {
      logger.info(`[${userId}] Feature flags not found in redis.  Refreshing flags for user and returning defaults.`);
      const flags = await this.getAllFlags();
      return { identifier: userId, flags, traits: [] };
    }

    return cache;
  }

  @Trace()
  public static async getFlag(userId: string, name: string): Promise<IFlagsmithFlag | undefined> {
    let flag: IFlagsmithFlag | undefined;

    try {
      const items = await this.getUserFlags(userId);

      if (!items) {
        return flag;
      }

      flag = items.flags.find((item) => item.feature.name === name);

      /**
       * We have this option in Flagsmith "Hide disabled flags from SDKs" Once it is turned on it will not return the disabled flags
       * for the user. So in this case a flag might well be empty. To cater it if we didn't found out corresponding flag we will lookup for
       * it in the global flags before throwing out an error
       */
      if (!flag) {
        const flags: IFlagsmithFlag[] = await this.refreshGlobalFeatureFlags();
        flag = flags.find((item) => item.feature.name === name);
      }
    } catch (error: unknown) {
      logger.error({ error }, "unable to retrieve flag");
    }

    return flag;
  }

  @Trace()
  public static async hasFeature(key: string, userId?: string): Promise<boolean> {
    const userIdProvided = userId !== null && userId !== undefined;
    const flags = userIdProvided && userId ? await this.getAllFlagsForUser(userId) : await this.getAllFlags();

    return this.checkFeatureEnabled(key, flags);
  }

  @Trace()
  public static async refreshGlobalFeatureFlags(): Promise<IFlagsmithFlag[]> {
    logger.info("Refreshing global feature flags!");

    try {
      const response = await this.http.get<IFlagsmithFlag[]>(ROUTES.flags);
      const { data: flags } = response;

      await RedisHelper.setAsync(REDIS_KEYS.flags, JSON.stringify(flags), 86400);

      return flags;
    } catch (error: unknown) {
      logger.error({ error }, "Unable to hit bullet train /flags/ endpoint");

      return [];
    }
  }

  @Trace()
  public static async refreshUserFeatureFlags(userId: string): Promise<IFlagsmithIdentityWithTraits | undefined> {
    const key = `${REDIS_KEYS.identities}:${userId}`;
    const refreshKey = `${key}-refresh`;

    const stillValid = await this.checkCache<boolean>(refreshKey);

    if (stillValid) {
      return;
    }

    const cache = await this.checkCache<string>(key);

    try {
      const response = await this.http.get<IFlagsmithIdentityWithTraits>(`${ROUTES.identities}?identifier=${userId}`);
      const { data } = response;

      const value = JSON.stringify(data);
      const hash = crypto.createHash(HASH_ALGORITHM).update(value).digest(HASH_ENCODING);

      if (hash !== cache) {
        await this.setCache(key, hash, IDENTITY_CACHE_EXPIRATION);
        await this.setCache(`${REDIS_KEYS.hash}:${hash}`, value, IDENTITY_CACHE_EXPIRATION);
        await this.setCache(refreshKey, true, FLAG_CACHE_EXPIRATION);
      }

      return data;
    } catch (error: unknown) {
      logger.error({ error }, `Unable to hit bullet train ${ROUTES.identities} endpoint`);
      return { flags: [], traits: [] };
    }
  }

  @Trace()
  public static async toggleFlag(userId: string, key: string, enabled: boolean): Promise<boolean> {
    let toggled = false;

    if (!userId === undefined || !key === undefined || !enabled === undefined) {
      logger.error({ userId, key, enabled }, "toggleFlag() received invalid input");
      return toggled;
    }

    let flag: IFlagsmithFlag | undefined;
    let identity: IFlagsmithIdentity | undefined;

    try {
      flag = await this.getFlag(userId, key);
      identity = await this.getIdentity(userId);
    } catch (error: unknown) {
      logger.error({ error }, "failed while toggling feature flag");
    }

    if (!flag || !identity) {
      logger.error({ received: { flag, identity } }, `unable to toggle feature flag: ${key}`);
      return toggled;
    }

    let created = false;
    let updated = false;

    try {
      const { feature } = flag;
      const featureKey = feature.id?.toString() || INVALID_KEY;
      const identityKey = identity.id?.toString() || INVALID_KEY;

      if (!featureKey || !identityKey) {
        logger.error("failed to interrogate feature and identity keys");
      }

      const featureState = await this.getFeatureStateByIdentityAndFlag(identityKey, feature.id);

      if (featureState) {
        const flagKey = featureState.id?.toString() || INVALID_KEY;

        updated = await this.updateFeatureState(identityKey, flagKey, enabled);
      } else {
        created = await this.createFeatureState(identityKey, featureKey, enabled);
      }
    } catch (error: unknown) {
      logger.error({ error }, `an error occurred creating feature state: ${key}`);
    }

    toggled = created || updated;

    return toggled;
  }

  @Trace()
  public static async getIdentityFeatureStates(identityKey: string): Promise<IFlagsmithFeaturestateResult[] | []> {
    let featureStates: IFlagsmithFeaturestateResult[] = [];

    try {
      const http = await this.getAuthorizedHttpClient();

      const response = await http.get<IFlagsmithFeaturestate>(
        `${ROUTES.environments}${this.apiKey}${ROUTES.identities}/${identityKey}${ROUTES.featurestates}`
      );

      if (response) {
        const { results } = response.data;

        featureStates = results;
      }
    } catch (error: unknown) {
      logger.error({ error }, "unable to retrieve feature states for identity");
    }

    return featureStates;
  }

  @Trace()
  public static async getFeatureStateByIdentityAndFlag(
    identityKey: string,
    featureId: number
  ): Promise<IFlagsmithFeaturestateResult | null> {
    const featureStates: IFlagsmithFeaturestateResult[] = await this.getIdentityFeatureStates(identityKey);

    const state = featureStates.find((featureState) => featureState.feature === featureId) || null;

    return state;
  }

  @Trace()
  private static async authenticate(): Promise<string | null> {
    let pair: { key: string | null } = { key: null };

    try {
      const response: AxiosResponse<{ key: string | null }> = await this.http.post(ROUTES.auth, this.credentials);
      const { data } = response;

      pair = data;
    } catch (error: unknown) {
      logger.error({ error }, "unable to authenticate with flagsmith");
    }

    return pair.key;
  }

  @Trace()
  private static async checkCache<T>(key: string): Promise<T | null> {
    const cache = (await RedisHelper.getAsync(key)) as string;

    if (!cache) {
      return null;
    }

    let result: T | null;
    try {
      result = JSON.parse(cache) as T;
    } catch {
      result = (<unknown>cache) as T;
    }

    return result;
  }

  @Trace()
  private static checkFeatureEnabled(
    key: string,
    incomingFlags: IFlagsmithFlag[] | IFlagsmithIdentityWithTraits
  ): boolean {
    const flags: IFlagsmithFlag[] = Array.isArray(incomingFlags) ? incomingFlags : incomingFlags.flags;
    const flag: IFlagsmithFlag | undefined = flags.find((item: IFlagsmithFlag) => item.feature.name === key);

    return flag ? flag.enabled : false;
  }

  @Trace()
  private static async createFeatureState(identityKey: string, featureKey: string, enabled: boolean): Promise<boolean> {
    logger.debug("attempting to create...");

    let created = false;

    const body = { feature: featureKey, enabled };
    const route = `${ROUTES.environments}${this.apiKey}${ROUTES.identities}${identityKey}${ROUTES.featurestates}`;

    try {
      const http = await this.getAuthorizedHttpClient();
      const response: AxiosResponse<{ enabled: boolean }> = await http.post(route, body);
      const { data } = response;

      created = this.compareBooleans(data.enabled, enabled);
    } catch (error: unknown) {
      logger.info({ error }, "an error occurred creating featurestate");
    }

    return created;
  }

  @Trace()
  private static async getAuthorizedHttpClient(): Promise<AxiosInstance> {
    if (this.authorizedHttp !== undefined) {
      return new Promise((resolve) => {
        resolve(<AxiosInstance>this.authorizedHttp);
      });
    }

    /**
     * Authenticate, bail on failure
     */
    let token: string | null;

    try {
      token = await this.authenticate();

      if (!token) {
        throw new Error("missing token");
      }
    } catch (error: unknown) {
      const msg = "failed authentication";
      logger.error({ error }, msg);
      throw new Error(msg);
    }

    this.authorizedHttp = axios.create({
      baseURL: API_URL,
      headers: { Authorization: `Token ${token}` },
      timeout: 5000
    });

    return this.authorizedHttp;
  }

  @Trace()
  private static async getIdentities(userId: string): Promise<IFlagsmithIdentity[]> {
    let result: IFlagsmithIdentity[] = [];

    try {
      const http = await this.getAuthorizedHttpClient();
      const response: AxiosResponse<{
        results: IFlagsmithIdentity[];
        count: number;
        nextPage: string | null;
        prevPage: string | null;
      }> = await http.get(`${ROUTES.environments}${this.apiKey}${ROUTES.identities}?q=${userId}`);

      if (response) {
        const { data } = response;
        const { results } = data;

        result = results;
      }
    } catch (error: unknown) {
      logger.error({ error }, "unable to retrieve identities");
    }

    return result;
  }

  @Trace()
  private static async getIdentity(userId: string): Promise<IFlagsmithIdentity | undefined> {
    const identities = await this.getIdentities(userId);

    return identities.find((item) => item.identifier === userId);
  }

  @Trace()
  private static async getUserFlags(userId: string): Promise<IFlagsmithIdentityWithTraits | undefined> {
    let result: IFlagsmithIdentityWithTraits | undefined;

    try {
      const response = await this.http.get<IFlagsmithIdentityWithTraits>(`${ROUTES.identities}?identifier=${userId}`);

      if (response) {
        const { data } = response;

        result = data;
      }
    } catch (error: unknown) {
      logger.error({ error }, "unable to retrieve identities");
    }

    return result;
  }

  @Trace()
  private static async setCache(key: string, value: boolean | string, expiration: number): Promise<void> {
    await RedisHelper.setAsync(key, value, expiration);
  }

  @Trace()
  private static async updateFeatureState(identityKey: string, flagKey: string, enabled: boolean): Promise<boolean> {
    logger.debug("attempting to update...");

    let updated = false;

    const body = { id: flagKey, enabled };
    const route = `${ROUTES.environments}${this.apiKey}${ROUTES.identities}${identityKey}${ROUTES.featurestates}${flagKey}/`;

    try {
      const http = await this.getAuthorizedHttpClient();
      const response: AxiosResponse<{ enabled: boolean }> = await http.put(route, body);

      const { data } = response;

      updated = this.compareBooleans(data.enabled, enabled);
    } catch (error: unknown) {
      logger.info({ error }, "an error occurred updating featurestate");
    }

    return updated;
  }

  @Trace()
  private static compareBooleans(first: boolean, second: boolean): boolean {
    let result = false;

    try {
      const value1 = JSON.parse(String(first)) as boolean;
      const value2 = JSON.parse(String(second)) as boolean;
      result = value1 === value2;
    } catch (error) {
      logger.debug(error, "Error while comparing two booleans");
    }

    return result;
  }
}
