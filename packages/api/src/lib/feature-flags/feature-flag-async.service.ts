import Config from "../../async/config";
import { Queue, Job } from "bull";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { FeatureFlagService } from "./feature-flag.service";

interface UserFeatureFlagQueueParams {
  user_id: string;
}

@Service("feature-flag-async.service")
export class FeatureFlagAsyncService extends BaseService {
  private $refreshGlobalFeatureFlagsQueue: Queue;
  private $refreshUserFeatureFlagsAsyncQueue: Queue;

  constructor() {
    super();

    this.$refreshGlobalFeatureFlagsQueue = Config.initQueue("refreshGlobalFeatureFlags");
    this.$refreshUserFeatureFlagsAsyncQueue = Config.initQueue("refreshUserFeatureFlagsAsync");
  }

  /**
   * @summary Refresh global Feature flags through bull queue
   */
  @Trace()
  public refreshGlobalFeatureFlags() {
    void this.$refreshGlobalFeatureFlagsQueue.add({});
  }

  /**
   * @summary Refresh Feature flags for a user through bull queue
   *
   * @param user_id User ID
   */
  @Trace()
  public refreshUserFeatureFlagsAsync(user_id: string) {
    void this.$refreshUserFeatureFlagsAsyncQueue.add({ user_id });
  }

  /**
   * @summary Async Processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "feature-flag-async.service" })
  public static InitAsyncProcessors() {
    const featureFlagAsyncService = new FeatureFlagAsyncService();

    void featureFlagAsyncService.$refreshGlobalFeatureFlagsQueue.process((_job) =>
      FeatureFlagService.refreshGlobalFeatureFlags()
    );

    void featureFlagAsyncService.$refreshUserFeatureFlagsAsyncQueue.process((job: Job<UserFeatureFlagQueueParams>) =>
      FeatureFlagService.refreshUserFeatureFlags(job.data.user_id)
    );
  }
}
