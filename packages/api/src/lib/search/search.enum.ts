export enum OpportunitySort {
  popular = "popular",
  recent = "recent"
}

export enum SearchLimit {
  DEFAULT_SEARCH_LIMIT = 10,
  MAX_SEARCH_LIMIT = 25
}
