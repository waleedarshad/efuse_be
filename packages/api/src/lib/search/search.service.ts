import { Aggregate, Model, AggregatePaginateOptions, AggregatePaginateResult, Types } from "mongoose";

import { UserStatus } from "@efuse/entities";
import { BaseService } from "../base/base.service";
import { IOrganization, ILearningArticle } from "../../backend/interfaces";
import { Kinds } from "../kinds";
import { Organization } from "../../backend/models/Organization";
import { SEARCH } from "../metrics";
import { Service, Trace } from "../decorators";
import { Opportunity } from "../../backend/models/Opportunity";
import { User } from "../../backend/models/User";
import { IOpportunity, IPopulatedOpportunity } from "../../backend/interfaces/opportunity";
import { OpportunityLookupService } from "../opportunities/opportunity-lookup.service";
import { OpportunitySort } from "./search.enum";
import { IUser } from "../../backend/interfaces/user";
import { BlockedUsersService } from "../blocked-users/blocked-users.service";
import { LearningArticleService } from "../learningArticles/learning-article.service";

/**
 *  Service that encapsulates search functionality.
 */
@Service("search.service")
export class SearchService extends BaseService {
  private readonly organizations: Model<IOrganization> = <Model<IOrganization>>Organization;
  private readonly users: Model<IUser> = User;
  private readonly opportunities: Model<IOpportunity> = Opportunity;

  private opportunityLookupService: OpportunityLookupService;
  private blockedUserService: BlockedUsersService;
  private learningArticleService: LearningArticleService;

  constructor() {
    super();

    this.opportunityLookupService = new OpportunityLookupService();
    this.blockedUserService = new BlockedUsersService();
    this.learningArticleService = new LearningArticleService();
  }

  /**
   * Main search method.
   *
   * @param term
   * @param kinds
   * @param currentUserId
   * @param limit
   */
  @Trace()
  public async search(
    terms: string[],
    currentUserId: Types.ObjectId | string,
    kinds: string[] = [],
    limit = 10
  ): Promise<any[]> {
    const tags = [`kinds:${(kinds.length > 0 ? kinds.sort() : ["all"]).join(", ")}`];
    this.logger?.debug(`searching for "${terms.join(", ")}" in ${tags.join(", ")} entries `);

    this.recordMetric(SEARCH.START, tags);

    let entities: any[] = [];

    try {
      const onlyOrgs: boolean = kinds.length === 1 && kinds.includes(Kinds.ORG);
      const onlyUsers: boolean = kinds.length === 1 && kinds.includes(Kinds.USER);

      if (onlyOrgs) {
        entities = await this.searchForOrganizations(terms, limit);
      } else if (onlyUsers) {
        entities = await this.searchForUsers(terms, limit, currentUserId);
      } else {
        const users = await this.searchForUsers(terms, limit, currentUserId);
        const orgs = await this.searchForOrganizations(terms, limit);

        entities = users.concat(orgs);
      }
    } catch (error) {
      this.recordMetric(SEARCH.ERROR, tags);

      throw this.handleError(error, "Error performing search");
    }

    this.recordMetric(SEARCH.FINISH, tags);

    return entities;
  }

  /**
   * Search for organizations.
   *
   * @private
   * @param {string} term
   * @param {number} [limit=10]
   * @param {boolean} [applyProjection=true]
   * @param {any[]} aggregationSteps
   *
   * @memberof SearchService
   */
  @Trace()
  private searchForOrganizations(
    terms: string[],
    limit = 10,
    applyProjection = true,
    aggregationSteps: any[] = []
  ): Promise<Aggregate<IOrganization[], any>> {
    this.recordMetric(SEARCH.SEARCH_FOR_ORG.START);

    try {
      let docs = this.organizations
        .aggregate<IOrganization>([
          {
            $search: {
              autocomplete: {
                path: "name",
                query: terms,
                score: { boost: { value: 2 } }
              }
            }
          },

          ...aggregationSteps
        ])
        .limit(limit)
        .sort({ verified: -1, score: { $meta: "textScore" } });

      if (applyProjection) {
        docs = docs.project({
          f_id: { $toString: "$_id" },
          name: true,
          profilepicture: "$profileImage.url",
          profilePicture: { url: "$profileImage.url" },
          score: { $meta: "searchScore" },
          type: Kinds.ORG,
          url: {
            $concat: ["/organizations/show?id=", { $toString: "$_id" }]
          },
          username: { $ifNull: ["$shortName", ""] },
          verified: "$verified.status"
        });
      }

      this.recordMetric(SEARCH.SEARCH_FOR_ORG.FINISH);

      return <never>docs;
    } catch (error) {
      this.recordMetric(SEARCH.SEARCH_FOR_ORG.ERROR);

      throw this.handleError(error, "Error performing organization search");
    }
  }

  /**
   * Search for users.
   *
   * @private
   * @param terms
   * @param {number} [limit=10]
   * @param {Types.ObjectId | string} currentUserId
   * @param {boolean} [applyProjection=true]
   * @param {any[]} aggregationSteps
   *
   * @memberof SearchService
   */
  @Trace()
  private async searchForUsers(
    terms: string[],
    limit = 10,
    currentUserId: Types.ObjectId | string,
    applyProjection = true,
    aggregationSteps: any[] = []
  ): Promise<Array<any>> {
    this.recordMetric(SEARCH.SEARCH_FOR_USER.START);

    const hiddenUsers = currentUserId
      ? await this.blockedUserService.getBlockedOrBlockedByUsers(String(currentUserId))
      : [];

    try {
      let docs = this.users
        .aggregate<typeof User>([
          {
            $search: {
              compound: {
                must: {
                  text: {
                    path: "status",
                    query: UserStatus.ACTIVE
                  }
                },
                should: [
                  { autocomplete: { path: "name", query: terms } },
                  {
                    autocomplete: {
                      path: "username",
                      query: terms,
                      score: { boost: { value: 2 } }
                    }
                  },
                  {
                    text: {
                      path: "username",
                      query: terms,
                      score: { boost: { value: 3 } }
                    }
                  }
                ],
                minimumShouldMatch: 1
              }
            }
          },
          { $match: { _id: { $nin: hiddenUsers.map((it) => Types.ObjectId(it)) } } },

          ...aggregationSteps
        ])
        .limit(limit)
        .sort({ verified: -1, score: { $meta: "textScore" } });

      if (applyProjection) {
        docs = docs.project({
          f_id: { $toString: "$_id" },
          name: "$name",
          profilepicture: "$profilePicture.url",
          profilePicture: { url: "$profilePicture.url" },
          score: { $meta: "searchScore" },
          type: Kinds.USER,
          url: { $concat: ["/u/", "$username"] },
          username: true,
          verified: true
        });
      }

      this.recordMetric(SEARCH.SEARCH_FOR_USER.FINISH);

      return <never>docs;
    } catch (error) {
      this.recordMetric(SEARCH.SEARCH_FOR_USER.ERROR);

      throw this.handleError(error, "Error performing user search");
    }
  }

  public async searchForOpportunities(
    page: number,
    limit: number,
    currentUserId?: Types.ObjectId | string,
    term?: string,
    location?: string,
    opportunityTypes?: string[],
    subTypes?: string[],
    publishStatuses?: string[],
    userId?: Types.ObjectId,
    organizationId?: Types.ObjectId,
    organizationsIds?: Types.ObjectId[],
    sortBy: string | undefined = OpportunitySort.recent,
    opportunityIds?: Types.ObjectId[]
  ): Promise<AggregatePaginateResult<IOpportunity>> {
    const aggregateSteps: any[] = [];
    const textSearchAggregateSteps: any[] = [];

    // Filter specific opportunities, e.g applied opportunities
    if (opportunityIds) {
      aggregateSteps.push({ $match: { _id: { $in: opportunityIds } } });
    }

    // Include organizations
    if (userId && organizationsIds && organizationsIds.length > 0) {
      aggregateSteps.push({
        $match: { $or: [{ user: userId }, { organization: { $in: organizationsIds } }] }
      });
    } else {
      // Match by userId
      if (userId) {
        aggregateSteps.push({ $match: { user: userId } });
      }

      // Match by organizationId
      if (organizationId) {
        aggregateSteps.push({ $match: { organization: organizationId } });
      }
    }

    // build search term array
    if (term && term?.length > 0) {
      textSearchAggregateSteps.push(
        ...[
          {
            text: {
              query: term,
              path: "title",
              fuzzy: { maxEdits: 1 },
              score: { boost: { value: 2 } }
            }
          },
          {
            text: {
              query: term,
              path: "description",
              fuzzy: {}
            }
          },
          {
            text: {
              query: term,
              path: "shortName"
            }
          }
        ]
      );
    }

    // build location search array
    if (location && location?.length > 0) {
      textSearchAggregateSteps.push({
        text: {
          query: location,
          path: "location",
          fuzzy: { maxEdits: 1 },
          score: { boost: { value: 1.5 } }
        }
      });
    }

    if (textSearchAggregateSteps.length > 0) {
      aggregateSteps.push({
        $search: {
          index: "default",
          compound: {
            should: textSearchAggregateSteps
          }
        }
      });
    }

    // add type match
    if (opportunityTypes && opportunityTypes.length > 0) {
      aggregateSteps.push({
        $match: {
          opportunityType: { $in: opportunityTypes }
        }
      });
    }

    // add subtype match
    if (subTypes && subTypes.length > 0) {
      aggregateSteps.push({
        $match: {
          subType: { $in: subTypes }
        }
      });
    }

    // add publishStatus matching
    if (publishStatuses && publishStatuses.length > 0) {
      aggregateSteps.push({
        $match: {
          publishStatus: { $in: publishStatuses }
        }
      });
    }

    // limit to visible opportunities
    aggregateSteps.push({
      $match: {
        status: "visible"
      }
    });

    // Most recent
    let sort: Record<string, number> = { createdAt: -1 };

    if (sortBy === OpportunitySort.popular) {
      sort = { "metadata.rank": -1 };
    }

    const opportunitiesAggregate = Opportunity.aggregate<IOpportunity>(aggregateSteps);

    const paginateOptions: AggregatePaginateOptions = {
      page,
      limit,
      sort,
      lean: true
    };
    const paginatedOpportunities = await Opportunity.aggregatePaginate(opportunitiesAggregate, paginateOptions);

    // Fetch associated objects
    const populatedOpportunities: IPopulatedOpportunity[] =
      await this.opportunityLookupService.fetchAndAppendAssociatedObjects(paginatedOpportunities.docs, currentUserId);

    paginatedOpportunities.docs = populatedOpportunities;

    return paginatedOpportunities;
  }

  /**
   * @summary Search user
   *
   * @param {string[]} term
   * @param {number} [limit=10]
   * @param {Types.ObjectId | string} currentUserId
   * @memberof SearchService
   *
   * @returns {Promise<IUser[]>}
   *
   */
  public async searchUsersGraphQL(
    terms: string[],
    limit = 10,
    currentUserId: Types.ObjectId | string
  ): Promise<IUser[]> {
    try {
      const aggregationsSteps: any[] = [{ $addFields: { score: { $meta: "searchScore" } } }];
      const users: IUser[] = (await this.searchForUsers(
        terms,
        limit,
        currentUserId,
        false,
        aggregationsSteps
      )) as IUser[];
      return users;
    } catch (error) {
      throw this.handleError(error, "Error performing search");
    }
  }

  /**
   * @summary Search Organizations
   *
   * @param {string[]} term
   * @param {number} [limit=10]
   * @memberof SearchService
   *
   * @returns {Promise<IUser[]>}
   *
   */
  public async searchOrganizationsGraphQL(terms: string[], limit = 10): Promise<IOrganization[]> {
    try {
      const aggregationsSteps: any[] = [{ $addFields: { score: { $meta: "searchScore" } } }];
      const organizations: IOrganization[] = await this.searchForOrganizations(terms, limit, false, aggregationsSteps);
      return organizations;
    } catch (error) {
      throw this.handleError(error, "Error performing search");
    }
  }

  /**
   * @summary Search Opportunities
   *
   * @param {string[]} term
   * @param {number} [limit=10]
   * @memberof SearchService
   *
   * @returns {Promise<IOpportunity[]>}
   *
   */
  public async searchOpportunitiesGraphQL(terms: string[], limit = 10): Promise<IOpportunity[]> {
    /*
      MongoDB Atlas Search Configuration JSON

      {
       "mappings": {
        "dynamic": false,
        "fields": {
          "shortName": {
            "foldDiacritics": true,
            "maxGrams": 15,
            "minGrams": 2,
            "tokenization": "nGram",
            "type": "autocomplete"
          },
          "title": {
            "foldDiacritics": true,
            "maxGrams": 15,
            "minGrams": 2,
            "tokenization": "nGram",
            "type": "autocomplete"
          }
        }
      }
    }
    */
    try {
      const opportunities = await this.opportunities
        .aggregate<IOpportunity>([
          {
            $search: {
              index: "search-ngram",
              autocomplete: {
                path: "title",
                query: terms,
                score: { boost: { value: 2 } }
              }
            }
          }
        ])
        .limit(limit)
        .sort({ verified: -1, score: { $meta: "textScore" } });

      return opportunities;
    } catch (error) {
      throw this.handleError(error, "Error performing search");
    }
  }

  /**
   * @summary Search Learning Articles
   *
   * @param {string[]} term
   * @param {number} [limit=10]
   * @memberof SearchService
   *
   * @returns {Promise<ILearningArticle[]>}
   *
   */
  @Trace()
  public async searchLearningArticlesGraphQL(terms: string, limit = 10, userId?: string): Promise<ILearningArticle[]> {
    try {
      const learningArticles: ILearningArticle[] = await this.learningArticleService.search(terms, limit, userId);

      return learningArticles;
    } catch (error) {
      throw this.handleError(error, "Error performing search");
    }
  }
}
