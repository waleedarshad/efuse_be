import { ISearch } from "../../backend/interfaces";

export class SearchHelper {
  /**
   * @summary Sanitize a given string or array of string to be DB compatible
   *
   * @param {string | string[]} incoming
   *
   * @returns {string[]}
   */
  public sanitizeToArray(incoming: string | string[]): string[] {
    let results: string[] = [];

    if (!incoming) {
      return results;
    }

    if (Array.isArray(incoming)) {
      incoming.forEach((term: string) => {
        if (term.indexOf(" ") >= 1) {
          const split = term.split(" ");
          results.push(...split);
        } else {
          results.push(term);
        }
      });
    } else if (incoming.indexOf(" ") >= 1) {
      const split = incoming.split(" ");
      results.push(...split);
    } else {
      results.push(incoming);
    }

    /**
     * Incoming requests with unescaped symbols used as breaks (i.e. '+', '%20') will result in empty array
     * elements, which the database does not like.
     */
    results = results
      .map((element) => element.replace("%20", " ")) // convert escape to space
      .map((element) => element.replace(/\s/g, "")) // regex space to empty
      .filter((element) => element !== undefined && element !== null && element !== "");

    return results;
  }

  /**
   * @summary Sort entities to return verified first & then by score
   *
   * @param {any[]} entities
   * @param {number} limit
   *
   * @returns {any[]}
   */
  public sortEntities(entities: ISearch[] | any[], limit: number): ISearch[] | any[] {
    return entities.sort((a, b) => b.verified - a.verified || b.score - a.score).slice(0, limit);
  }

  /**
   * @summary Sort entities to return verified first & then by score
   *
   * @param {any[]} entities
   * @param {number} limit
   *
   * @returns {any[]}
   */
  public sortEntitiesWhenVerifiedIsNested(entities: ISearch[] | any[], limit: number): ISearch[] | any[] {
    return entities
      .sort((a, b) => {
        const bVerified: any = b.verified?.status || false;

        const aVerified: any = a.verified?.status || false;

        return bVerified - aVerified || b.score - a.score;
      })
      .slice(0, limit);
  }
}
