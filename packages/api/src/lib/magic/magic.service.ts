import { Magic, MagicUserMetadata } from "@magic-sdk/admin";
import { MAGIC_SECRET_KEY } from "@efuse/key-store";
import { Failure } from "@efuse/contracts";
import { UserStatus } from "@efuse/entities";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { AuthenticationMsgsEnum } from "../auth/auth.enum";
import { UserService } from "../users/user.service";
import { UserFields } from "../users/user-fields";
import { IUser, IUserAgent, IMagicLoginResponse } from "../../backend/interfaces";
import { IIpInfo } from "../../backend/interfaces/ip-info";
import { TokenService } from "../users/token.service";
import WhitelistHelper from "../../backend/helpers/whitelist_helper";

@Service("magic.service")
export class MagicService extends BaseService {
  private $magic: Magic;

  private $userService: UserService;
  private $tokenService: TokenService;

  constructor() {
    super();

    this.$magic = new Magic(MAGIC_SECRET_KEY);

    this.$userService = new UserService();
    this.$tokenService = new TokenService();
  }

  /**
   * @summary Login user using Magic Link
   *
   * @param {string} authorizationHeader
   * @param {string | undefined} platform
   * @param {IUserAgent | undefined} userAgent
   * @param {IIpInfo} location
   *
   * @returns {Promise<IMagicLoginResponse>}
   */
  @Trace()
  public async login(
    authorizationHeader: string,
    platform: string | undefined,
    userAgent: IUserAgent | undefined,
    location: IIpInfo
  ): Promise<IMagicLoginResponse> {
    const didToken = this.$magic.utils.parseAuthorizationHeader(authorizationHeader);

    // Only process if we have DID
    if (!didToken) {
      this.logger?.error("DID Token not found in Authorization Header");
      throw Failure.BadRequest(AuthenticationMsgsEnum.INVALID_LOGIN);
    }

    // Make sure platform is present
    if (!platform) {
      this.logger?.error("Platform not found");
      throw Failure.BadRequest(AuthenticationMsgsEnum.INVALID_LOGIN);
    }

    /**
     * The validate function will return if the specified DID token is authentic and not expired. If the token is forged
     * or otherwise invalid, the function will throw a descriptive error. Here using try/catch to return the Generic Error message
     */
    try {
      this.$magic.token.validate(didToken);
    } catch (error) {
      this.logger?.error(error, "error validating DID token");
      throw Failure.BadRequest(AuthenticationMsgsEnum.INVALID_LOGIN);
    }

    // Get user's information
    const userMetadata: MagicUserMetadata = await this.$magic.users.getMetadataByToken(didToken);

    // Make sure magic returns us email
    if (!userMetadata.email) {
      this.logger?.error("Email not returned by magic");
      throw Failure.BadRequest(AuthenticationMsgsEnum.INVALID_LOGIN);
    }

    // Lookup for user in DB
    const user: IUser | null = await this.$userService.findOne(
      { email: userMetadata.email },
      UserFields.JWT_USER_FIELDS,
      { lean: true }
    );

    // Make sure user exists
    if (!user) {
      this.logger?.error("User not found in DB");
      throw Failure.UnprocessableEntity(AuthenticationMsgsEnum.INVALID_LOGIN);
    }

    // Make sure user is active
    if (user.status === UserStatus.BLOCKED) {
      throw Failure.Forbidden(AuthenticationMsgsEnum.USER_BLOCKED);
    }

    // Generate bearerToken, refreshToken & create session
    const { token, refreshToken } = await this.$tokenService.createSession(user._id, userAgent, location);

    // Scrub user's fields
    const userPayload = WhitelistHelper.userPayload(user);

    // todo: investigate & fix
    // @ts-ignore
    return { user: userPayload, token, refreshToken, message: AuthenticationMsgsEnum.SUCCESS };
  }
}
