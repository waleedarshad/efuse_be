export enum WebhookType {
  flagged = "message.flagged",
  newMessage = "message.new"
}
