import { STREAMCHAT_API_KEY, STREAMCHAT_API_SECRET } from "@efuse/key-store";
import { Queue } from "bull";
import { StreamChat } from "stream-chat";
import { Types } from "mongoose";

import { BaseService } from "../base/base.service";
import { IStreamchatMessageFlagWebhook } from "../../backend/interfaces/streamchat";
import { sendEmail } from "../email";
import { Service, Trace } from "../decorators";
import { Slack } from "../slack";
import Config from "../../async/config";

const FLAGGED_SLACK_CHANNEL = "#community-carl";
const MODERATION_LINK = "https://dashboard.getstream.io/dashboard/v2/organization/41846/app/66627/chat/moderation";
const PRIVACY_EMAIL = "privacy@efuse.io";
const STREAMCHAT_ADMIN_USER = "efuse-ban-admin";
const TEMPLATE_ID = "d-d92d1cb614c943d4824d3e9f1995eaaa";

@Service("streamchat.service")
export class StreamChatService extends BaseService {
  private messageFlaggedWebhookHandlerQueue: Queue;
  private streamchatInstance: StreamChat;

  constructor() {
    super();

    this.streamchatInstance = new StreamChat(STREAMCHAT_API_KEY, STREAMCHAT_API_SECRET);

    this.messageFlaggedWebhookHandlerQueue = Config.initQueue("messageFlaggedWebhookHandler");
  }

  /**
   * Marks the user as banned in Streamchat.
   *
   * @param {string} userId
   * @return {Promise<boolean>}
   *
   * @memberof StreamChatService
   */
  @Trace()
  public async ban(userId: string): Promise<boolean> {
    await this.streamchatInstance.banUser(userId, {
      banned_by_id: STREAMCHAT_ADMIN_USER,
      target_user_id: userId
    });

    return true;
  }

  /**
   * Deactivates a user in streamchat (which does not mean banned === true)
   * which also marks existing messages as deleted.
   *
   * @param {string} userId
   * @return {Promise<boolean>}
   *
   * @memberof StreamChatService
   */
  @Trace()
  public async deactivate(userId: string): Promise<boolean> {
    await this.streamchatInstance.deactivateUser(userId, {
      mark_messages_deleted: true
    });

    return true;
  }

  /**
   * Reverts a call to `deactivate`, including
   * restoring previous messages.
   *
   * @param {string} userId
   * @return {Promise<boolean>}
   *
   * @memberof StreamChatService
   */
  @Trace()
  public async reactivate(userId: string): Promise<boolean> {
    await this.streamchatInstance.reactivateUser(userId, {
      restore_messages: true
    });

    return true;
  }

  /**
   * Reverts a call to `ban`.
   *
   * @param {string} userId
   * @return {Promise<boolean>}
   *
   * @memberof StreamChatService
   */
  @Trace()
  public async unban(userId: string): Promise<boolean> {
    await this.streamchatInstance.unbanUser(userId, {
      target_user_id: userId
    });

    return true;
  }

  /**
   * @summary Handle message flagged webhook
   *
   * @param {IStreamchatMessageFlagWebhook} body
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async messageFlaggedWebhookHandler(body: IStreamchatMessageFlagWebhook): Promise<void> {
    try {
      const { message, user, channel_id } = body;
      const offender = message.user;
      const reporter = user;

      const slackNotificationMessage = `New message flagged by user.\nPlease investigate with moderation tool ${MODERATION_LINK} \nMessage: ${message.text} \nMessage ID: ${message.id} \nChannel ID: ${channel_id} \nReporter: ${reporter.name}, ID: ${reporter.id} \nOffender: ${offender?.name}, ID: ${offender?.id}`;

      const emailData = {
        subject: "Message flagged by user",
        body: `<div style="padding:10px 20px; font-size:16px; color:#a9b5c4; line-height:22px;">
        A user has flagged a message. Please click on the link below to investigate with moderation tool
        <div style="text-align:center; margin-top:10px;">
          <a href='${MODERATION_LINK}' style='color:#fff;'>
            Moderation Tool
          </a>
        </div>
        <br />
        <div style="margin-top:10px;">
          Below are some extra details
          <ul style="padding:10px 20px; font-size:16px; color:#a9b5c4; line-height:22px;">
            <li>
              <strong>Message:</strong> <small>${message.text}</small>
            </li>
            <li>
              <strong>Message ID:</strong> <small>${message.id}</small>
            </li>
            <li>
              <strong>Channel ID:</strong> <small>${channel_id}</small>
            </li>
            <li>
              <strong>Reporter Name:</strong> <small>${reporter.name}</small>
            </li>
            <li>
              <strong>Reporter ID:</strong> <small>${reporter.id}</small>
            </li>
            <li>
              <strong>Offender Name:</strong> <small>${offender?.name}</small>
            </li>
            <li>
              <strong>Offender ID:</strong> <small>${offender?.id}</small>
            </li>
          </ul>
        </div>
      </div>`
      };

      // Resolve Promises concurrently
      await Promise.all([
        Slack.sendMessage(slackNotificationMessage, FLAGGED_SLACK_CHANNEL),
        sendEmail(PRIVACY_EMAIL, TEMPLATE_ID, emailData)
      ]);
    } catch (error) {
      this.logger?.error(error, "Error sending notification messageFlaggedWebhookHandler");
    }
  }

  /**
   * @summary Handle message flagged webhook asynchronously using bull queue
   *
   * @param {IStreamchatMessageFlagWebhook} body
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async messageFlaggedWebhookHandlerAsync(body: IStreamchatMessageFlagWebhook): Promise<void> {
    await this.messageFlaggedWebhookHandlerQueue.add({ body });
  }

  /**
   * @summary Async Processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "streamchat.service" })
  public static InitAsyncProcessors(): void {
    const streamchatService = new StreamChatService();

    streamchatService.messageFlaggedWebhookHandlerQueue
      .process((job: { data: { body: IStreamchatMessageFlagWebhook } }) => {
        return streamchatService.messageFlaggedWebhookHandler(job.data.body);
      })
      .catch((error) =>
        streamchatService.logger?.error(error, "Error while processing messageFlaggedWebhookHandlerQueue")
      );
  }

  /**
   * @summary Update stream chat user
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async updateStreamChatUser(id: Types.ObjectId | string, name: string, image: string): Promise<void> {
    await this.streamchatInstance.upsertUsers([
      {
        id: id.toString(),
        name: name,
        image: image
      }
    ]);

    this.streamchatInstance
      .disconnect()
      .catch((error) => this.logger?.error(error, "Error resolving streamchatInstance.disconnect promise"));
    this.logger?.info("StreamChat user info updated");
  }
}
