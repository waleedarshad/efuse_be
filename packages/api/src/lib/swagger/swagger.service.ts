import { BaseService } from "../base/base.service";
import { Service } from "../decorators";
import { Roles } from "../users/roles";
import { UserService } from "../users/user.service";

type IAuthorizerCallback = (err: any, authenticated?: boolean) => void;

@Service("swagger.service")
export class SwaggerService extends BaseService {
  private userService: UserService;

  constructor() {
    super();

    this.userService = new UserService();
  }

  /**
   * @summary Authorize user before accessing swagger docs
   *
   * @param {string} email
   * @param {string} password
   * @param {IAuthorizerCallback} cb
   *
   * @returns {Promise<void>}
   */
  public authorizer = async (email: string, password: string, cb: IAuthorizerCallback): Promise<void> => {
    try {
      const user = await this.userService.findOne({ email }, { _id: 1, password: 1, email: 1, roles: 1, status: 1 });

      if (!user) {
        this.logger?.error("User not found");
        return cb(null, false);
      }

      /**
       * Adding this import on top of file was some how throwing circular dependency warning around
       * getUserById method. To avoid it i've imported it here.
       */

      // eslint-disable-next-line @typescript-eslint/no-var-requires
      const { matchPassword } = require("../users");

      const matched = await matchPassword(user._id, password);
      if (!matched) {
        this.logger?.error("Password not matched");
        return cb(null, false);
      }

      if (!user.roles.includes(Roles.ADMIN) && !user.roles.includes(Roles.EMPLOYEE)) {
        this.logger?.error("User is neither admin nor employee");
        return cb(null, false);
      }

      if (user.status === "Blocked") {
        this.logger?.error("User is Blocked");
        return cb(null, false);
      }

      return cb(null, true);
    } catch (error) {
      this.logger?.error(error, "Something went wrong");
      cb(null, false);
    }
  };
}
