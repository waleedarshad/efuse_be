const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();

const { User } = require("../../backend/models/User");
const { EgencyBrand } = require("../../backend/models/EgencyBrand");
const { EgencyCampaign } = require("../../backend/models/EgencyCampaign");
const { EgencyOffer } = require("../../backend/models/EgencyOffer");

/**
 * @summary Create a new egency brand
 *
 * @param {Object} eGencyBrandParams - Data for egency brand
 * @return {Promise<any>} eGencyBrand - Newly created egency brand
 */
const createEgencyBrand = async (eGencyBrandParams) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyBrand = await new EgencyBrand(eGencyBrandParams).save();
      return resolve(eGencyBrand);
    } catch (error) {
      logger.error(eGencyBrandParams, "Error creating eGencyBrand");
      return reject(error);
    }
  });

/**
 * @summary Get egency brand by id
 *
 * @param {ObjectId} id - Id of egency brand
 * @return {Promise<any>} eGencyBrand
 */
const getEgencyBrandById = async (id) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyBrand = await validateEgencyPresence(id);
      await lookupAssociatedUser(eGencyBrand);
      return resolve(eGencyBrand);
    } catch (error) {
      logger.error(error, `Error fetching eGencyBrand ${id}`);
      return reject(error);
    }
  });

/**
 * @summary Check whether the egency brand exists or not
 *
 * @param {ObjectId} id - Id of egency brand
 * @return {Promise<any>} eGencyBrand
 */
const validateEgencyPresence = async (id) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyBrand = await EgencyBrand.findOne({
        _id: id
      }).lean();
      if (!eGencyBrand) {
        logger.info(`${id} eGency not found!`);
        return reject({ status: 422, message: "eGency not found!" });
      }
      return resolve(eGencyBrand);
    } catch (error) {
      logger.error(error, `Error fetching eGencyBrand ${id}`);
      return reject(error);
    }
  });

/**
 * @summary Get the user associated with egency
 *
 * @param {Object} object - egency object
 * @return {Promise<any>} object - with user info
 */
const lookupAssociatedUser = async (object) => {
  object.associcatedUser = await User.findOne({ _id: object.creator })
    .select("_id name profilePicture username")
    .lean();
  return object;
};

/**
 * @summary Create egency campaign
 *
 * @param {Object} eGencyCampaignParams - egency campaign parameters
 * @return {Promise<any>} eGencyCampaign
 */
const createEgencyCampaign = async (eGencyCampaignParams) =>
  new Promise(async (resolve, reject) => {
    try {
      await validateEgencyPresence(eGencyCampaignParams.brand);
      const eGencyCampaign = await new EgencyCampaign(eGencyCampaignParams).save();
      return resolve(eGencyCampaign);
    } catch (error) {
      logger.error(eGencyCampaignParams, "Error creating eGencyCampaign");
      return reject(error);
    }
  });

/**
 * @summary Create egency campaign offer
 *
 * @param {Object} eGencyCampaignOfferParams - egency campaign offer parameters
 * @return {Promise<any>} eGencyCampaignOffer
 */

const createEgencyCampaignOffer = async (eGencyCampaignOfferParams) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyCampaign = await validateEgencyCampaignPresence({
        _id: eGencyCampaignOfferParams.campaign
      });
      const user = await User.findOne({ _id: eGencyCampaignOfferParams.user }).select("_id").lean();
      if (!user) {
        logger.info(`${eGencyCampaignOfferParams.user} User not found!`);
        return reject({ status: 422, message: "User not found!" });
      }
      const existingCampaignOffer = await EgencyOffer.findOne({
        user: user._id,
        campaign: eGencyCampaign._id
      }).lean();
      if (existingCampaignOffer) {
        return reject({
          status: 400,
          message: "Campaign has already been offered to this User"
        });
      }
      const eGencyCampaignOffer = await new EgencyOffer(eGencyCampaignOfferParams).save();
      return resolve(eGencyCampaignOffer);
    } catch (error) {
      logger.error(eGencyCampaignOfferParams, "Error creating eGency Offer");
      return reject(error);
    }
  });

/**
 * @summary Update egency campaign status
 *
 * @param {ObjectId} userId - Id of user
 * @param {ObjectId} campaignOfferId - Id of egency campaign offer
 * @param {String} status - New status
 * @return {Promise<any>} eGencyCampaignOffer
 */
const updateCampaignStatus = async (userId, campaignOfferId, status) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyCampaignOffer = await EgencyOffer.findOne({
        user: userId,
        _id: campaignOfferId
      });
      if (!eGencyCampaignOffer) {
        logger.info(`user: ${userId}, campaign: ${campaignOfferId} Campaign offer not found!`);
        return reject({ status: 422, message: "Campaign offer not found!" });
      }
      eGencyCampaignOffer.status = status;
      await eGencyCampaignOffer.save();
      return resolve(eGencyCampaignOffer);
    } catch (error) {
      logger.error({ userId, campaignOfferId, status }, "Error updating Campaign offer status");
      return reject(error);
    }
  });

/**
 * @summary Get campaign by brand id
 *
 * @param {ObjectId} eGencyBrandId - Id of egency brand
 * @param {ObjectId} eGencyCampaignId - Id of egency campaign
 * @return {Promise<any>} eGencyCampaign
 */

const getCampaignByBrand = async (eGencyBrandId, eGencyCampaignId) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyCampaign = await validateEgencyCampaignPresence({
        _id: eGencyCampaignId,
        brand: eGencyBrandId
      });
      await lookupAssociatedUser(eGencyCampaign);
      return resolve(eGencyCampaign);
    } catch (error) {
      logger.error(error, `Error fetching eGency Campaign BrandId: ${eGencyBrandId} CampaignId: ${eGencyCampaignId}`);
      return reject(error);
    }
  });

/**
 * @summary Check whether the egency campaign exists or not
 *
 * @param {Object} query - Id of egency campaign { _id : ObjectId }
 * @return {Promise<any>} eGencyCampaign
 */
const validateEgencyCampaignPresence = async (query) =>
  new Promise(async (resolve, reject) => {
    try {
      const eGencyCampaign = await EgencyCampaign.findOne(query).lean();
      if (!eGencyCampaign) {
        logger.info(query, "eGency Campaign not found!");
        return reject({ status: 422, message: "eGency Campaign not found!" });
      }
      return resolve(eGencyCampaign);
    } catch (error) {
      logger.error(error, `Error fetching eGency Campaign ${query}`);
      return reject(error);
    }
  });

module.exports.createEgencyBrand = createEgencyBrand;
module.exports.createEgencyCampaign = createEgencyCampaign;
module.exports.createEgencyCampaignOffer = createEgencyCampaignOffer;
module.exports.getCampaignByBrand = getCampaignByBrand;
module.exports.getEgencyBrandById = getEgencyBrandById;
module.exports.updateCampaignStatus = updateCampaignStatus;
module.exports.validateEgencyCampaignPresence = validateEgencyCampaignPresence;
