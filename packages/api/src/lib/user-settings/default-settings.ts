import { MediaProfileEnum } from "@efuse/entities";
export class DefaultSettings {
  static get defaults() {
    return {
      email: {
        chatNotifications: {
          newMessage: true
        },
        postNotifications: {
          newHype: true,
          newComment: true,
          newShare: true
        },
        followNotifications: {
          newUserFollow: true
        },
        opportunityNotifications: {
          newApplicant: true
        },
        summaryNotifications: {
          weeklySummary: true
        }
      },
      push: {
        chatNotifications: {
          newMessage: true
        },
        postNotifications: {
          newHype: true,
          newComment: true,
          newShare: true
        },
        followNotifications: {
          newUserFollow: true
        },
        opportunityNotifications: {
          newApplicant: true
        }
      }
    };
  }

  static get defaultsMediaSettings() {
    return {
      profile: MediaProfileEnum.HD,
      mute: true,
      autoplay: true
    };
  }
}
