import { Failure } from "@efuse/contracts";
import { QueryOptions, Types, LeanDocument } from "mongoose";
import { isEmpty } from "lodash";

import { Service, Trace } from "../decorators";
import { DocumentType } from "@typegoose/typegoose";
import { UserSettings, UserSettingsModel } from "../../backend/models/user-settings/user-settings.model";
import { BaseModelService } from "../base";
import { MediaProfileEnum } from "@efuse/entities";
import { ObjectId } from "../../backend/types";
@Service("user-settings")
export class UserSettingsService extends BaseModelService<DocumentType<UserSettings>> {
  private readonly $userSettingsModel = UserSettingsModel;

  constructor() {
    super(UserSettingsModel);
  }
  /**
   * @summary Retrieve user settings object or create if it doesn't exist
   *
   * @param {ObjectId} userId
   * @param {unknown} projection
   * @param {unknown} options
   *
   * @returns {Promise<DocumentType<UserSettings>>}
   */
  @Trace()
  public async get(userId: ObjectId, projection?: any, options?: QueryOptions): Promise<DocumentType<UserSettings>> {
    const userSettings = await this.findOne({ user: userId }, projection, options);

    // Return when user settings are found
    if (userSettings) {
      return userSettings;
    }

    // If it reaches here then it means user settings doesn't exist. go ahead & create new
    const doc = <DocumentType<UserSettings>>{ user: userId };
    const newUserSettings = await this.create(doc);

    return newUserSettings;
  }

  /**
   * @summary Create new user settings object
   *
   * @param {DocumentType<UserSettings>} userSettingsBody
   *
   * @returns {Promise<DocumentType<UserSettings>>}
   */
  @Trace()
  public async create(userSettingsBody: DocumentType<UserSettings>): Promise<DocumentType<UserSettings>> {
    if (isEmpty(userSettingsBody)) {
      throw Failure.BadRequest("user settings body not found");
    }

    const newUserSettings = await super.create(userSettingsBody);
    return newUserSettings;
  }

  /**
   * @summary Update user settings for a particular user
   *
   * @param {Types.ObjectId} userId
   * @param {Partial<UserSettings>} userSettingsBody
   * @param {unknown} projection
   *
   * @returns {Promise<DocumentType<UserSettings>>}
   */
  @Trace()
  public async update(
    userId: Types.ObjectId,
    userSettingsBody: Partial<UserSettings>,
    projection?: any
  ): Promise<LeanDocument<DocumentType<UserSettings>>> {
    const doc: Partial<UserSettings> = userSettingsBody;

    // Validate media profile enums & also default to hd
    if (doc.media) {
      const mediaProfiles = Object.values(MediaProfileEnum);
      const { media } = doc;
      const { profile } = media;

      // Validate profile
      if (!profile || !mediaProfiles.includes(profile)) {
        doc.media.profile = MediaProfileEnum.HD;
      }

      // Make sure mute is not undefined
      if (media.mute === undefined) {
        media.mute = true;
      }

      // Make sure autoplay is not undefined
      if (media.autoplay === undefined) {
        media.autoplay = true;
      }
    }

    const userSettings = await this.$userSettingsModel
      .findOneAndUpdate({ user: userId }, { ...doc }, { new: true, projection })
      .lean();

    if (!userSettings) {
      throw Failure.BadRequest("user settings not found!");
    }

    return userSettings;
  }
}
