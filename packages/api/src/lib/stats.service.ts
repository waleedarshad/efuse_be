import { uniq, chain, sumBy, map, countBy, sortBy, forEach } from "lodash";
import { StatsEnum } from "@efuse/entities";

import { ApplicantEntityTypeEnum } from "./applicants/applicant.enum";
import { Service, Trace } from "./decorators";
import { MONGO_PREFERENCE_TAGS } from "../backend/config/constants";
import { EFuseAnalyticsUtil } from "./analytics.util";
import { BaseService } from "./base";
import { Applicant } from "../backend/models/applicant";
import { Comment } from "../backend/models/comment";
import { DiscordStatModel } from "../backend/models/discord-stat/discord-stat.model";
import { HypeModel } from "../backend/models/hype/hype.model";
import { Member } from "../backend/models/member";
import { Notification } from "../backend/models/Notification";
import { Organization } from "../backend/models/Organization";
import { Opportunity } from "../backend/models/Opportunity";
import { OrganizationFollower } from "../backend/models/organization-follower";
import { Feed } from "../backend/models/Feed";
import { User } from "../backend/models/User";
import { Friend } from "../backend/models/friend";

const SERVICE = "stats.service";

@Service(SERVICE)
export class StatsService extends BaseService {
  constructor() {
    super();
  }

  /**
   * @summary Get total followers in efuse
   */
  @Trace()
  private async getTotalFollowers(): Promise<void> {
    const result = await Friend.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_Followers", result);
  }

  /**
   * @summary Get total users in efuse
   */
  @Trace()
  private async getTotalUsers(): Promise<void> {
    const result = await User.find({ status: "Active" }).countDocuments().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users", result);
  }

  /**
   * @summary Get total likes in efuse
   */
  @Trace()
  private async getTotalLikes(): Promise<void> {
    const result = await HypeModel.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_likes", result);
  }

  /**
   * @summary Get total comments in efuse
   */
  @Trace()
  private async getTotalComments(): Promise<void> {
    const result = await Comment.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_comments", result);
  }

  /**
   * @summary Get total posts in efuse
   */
  @Trace()
  private async getTotalPosts(): Promise<void> {
    const result = await Feed.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_posts", result);
  }

  /**
   * @summary Get discord server stats
   */
  @Trace()
  private async getDiscordServerStats(): Promise<void> {
    const result = await DiscordStatModel.findOne().read("secondary", MONGO_PREFERENCE_TAGS).sort({ createdAt: -1 });

    if (!result) {
      return;
    }

    EFuseAnalyticsUtil.incrementGauge("total_discord.users", result.total_users);
    EFuseAnalyticsUtil.incrementGauge("total_discord.servers", result.total_servers);
  }

  /**
   * @summary Get total applicants in efuse
   */
  @Trace()
  private async getTotalApplicants(): Promise<void> {
    const totalApplicants = await Applicant.find(
      {},
      { entity: 1, user: 1, entityType: 1 },
      { populate: "entity user", lean: true }
    ).read("secondary", MONGO_PREFERENCE_TAGS);

    const applicantsByEntity = (applicants, groupByField) => {
      return chain(applicants)
        .groupBy((x) => (x.entity || {})[groupByField])
        .map((value, key) => {
          const ids = uniq(value.filter((x) => x.user).map((x) => x.user._id));
          return { key, value: ids };
        })
        .filter((x) => (x.value || []).length > 7) // Filter out opportunities with less than 5 applicants.  Probably used for testing.
        .sortBy((obj) => {
          return obj.value.length;
        })
        .reverse()
        .value();
    };

    const applicantsByOpportunity = applicantsByEntity(
      totalApplicants.filter((applicant) => applicant.entityType === ApplicantEntityTypeEnum.OPPORTUNITY),
      "title"
    );

    const applicantsByERena = applicantsByEntity(
      totalApplicants.filter((applicant) => applicant.entityType === ApplicantEntityTypeEnum.ERENA_TOURNAMENT),
      "tournamentName"
    );

    const totalApplicantsByOpportunities = sumBy(applicantsByOpportunity, (x) => x.value.length);
    const totalApplicantsByErena = sumBy(applicantsByERena, (x) => x.value.length);

    const totalApplied = totalApplicantsByOpportunities + totalApplicantsByErena;
    const totalAppliedOpportunities = applicantsByOpportunity.length;
    const totalAppliedTournaments = applicantsByERena.length;

    const topOpportunities = applicantsByOpportunity.slice(0, 10);
    this.logger?.debug(`Total Applied Opportunities: ${totalAppliedOpportunities}`);
    this.logger?.debug(`Total Applicants Applicants: ${totalApplied}`);
    this.logger?.debug(`Avg Applicant per Opportunity: ${totalApplied / totalAppliedOpportunities}`);

    EFuseAnalyticsUtil.incrementGauge("total_applicants", totalApplied);
    EFuseAnalyticsUtil.incrementGauge("total_opportunities.internal", totalAppliedOpportunities);

    topOpportunities.slice(0, 10).forEach((opportunity) => {
      this.logger?.debug(`${opportunity.key} ${opportunity.value.length}`);
      if (opportunity && opportunity.key && opportunity.value) {
        EFuseAnalyticsUtil.incrementGauge("total_applicants.byopportunity", opportunity.value.length, [
          `opportunity:${opportunity.key}`
        ]);
      }
    });

    const topTournaments = applicantsByERena.slice(0, 10);
    this.logger?.debug(`Total Applied Tournaments: ${totalAppliedTournaments}`);
    this.logger?.debug(`Avg Applicant per Tournament: ${totalApplied / totalAppliedTournaments}`);

    topTournaments.slice(0, 10).forEach((tournament) => {
      this.logger?.debug(`${tournament.key} ${tournament.value.length}`);
      EFuseAnalyticsUtil.incrementGauge("total_applicants.bytournament", tournament.value.length, [
        `tournament:${tournament.key}`
      ]);
    });
  }

  /**
   * @summary Get total opportunities in efuse
   */
  @Trace()
  private async getTotalOpportunities(): Promise<void> {
    const totalOpportunities = await Opportunity.find({
      status: "visible",
      publishStatus: "open"
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("open_opportunities", totalOpportunities);

    const openOppsHitmarker = await Opportunity.find({
      external_source: "hitmarker",
      status: "visible",
      publishStatus: "open"
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("open_opportunities.hitmarker", openOppsHitmarker);

    const openOppsInternal = await Opportunity.find({
      external: false,
      status: "visible",
      publishStatus: "open"
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("open_opportunities.internal", openOppsInternal);
  }

  /**
   * @summary Get total Notifications
   */
  @Trace()
  private async getTotalNotifications(): Promise<void> {
    const result = await Notification.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_notifications", result);
  }

  /**
   * @summary Get total organizations in efuse
   */
  @Trace()
  private async getTotalOrganizations(): Promise<void> {
    const result = await Organization.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_organizations", result);
  }

  /**
   * @summary Get total organization members
   */
  @Trace()
  private async getTotalOrgMembers(): Promise<void> {
    const result = await Member.estimatedDocumentCount().read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_org_members", result);
  }

  /**
   * @summary Get total number of user with twitch account in efuse
   */
  @Trace()
  private async getTotalUsersWithTwitchAccount(): Promise<void> {
    const result = await User.find({
      twitchVerified: true
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users_with_twitch_accounts", result);
  }

  /**
   * @summary Get total number of user with discord account in efuse
   */
  @Trace()
  private async getTotalUsersWithDiscordAccount(): Promise<void> {
    const result = await User.find({
      discordVerified: true
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users_with_discord_accounts", result);
  }

  /**
   * @summary Get total number of user with twitter account in efuse
   */
  @Trace()
  private async getTotalUsersWithTwitterAccount(): Promise<void> {
    const result = await User.find({
      twitterVerified: true
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users_with_twitter_accounts", result);
  }

  private sum = (values: any): number => values.reduce((total, x) => Number(total) + Number(x));

  /**
   * @summary Get total number of twitter followers
   */
  @Trace()
  private async getTotalTwitterFollowers(): Promise<void> {
    const result = await User.find(
      {
        twitterVerified: true
      },
      "-_id twitterfollowersCount"
    ).read("secondary", MONGO_PREFERENCE_TAGS);

    // Pull out all twitter followers
    const values = result.map((x) => x.twitterfollowersCount).filter((x = 0) => x > 0);

    EFuseAnalyticsUtil.incrementGauge("twitter.total_followers", this.sum(values));
  }

  /**
   * @summary Get total number of user with google account in efuse
   */
  @Trace()
  private async getTotalUsersWithGoogleAccount(): Promise<void> {
    const result = await User.find({
      googleVerified: true
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users_with_google_accounts", result);
  }

  /**
   * @summary Get total number of user with linkedin account in efuse
   */
  @Trace()
  private async getTotalUsersWithLinkedinAccount(): Promise<void> {
    const result = await User.find({
      linkedinVerified: true
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users_with_linkedin_accounts", result);
  }

  /**
   * @summary Get total number of user with facebook account in efuse
   */
  @Trace()
  private async getTotalUsersWithFacebookAccount(): Promise<void> {
    const result = await User.find({
      facebookVerified: true
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("total_users_with_facebook_accounts", result);
  }

  /**
   * @summary Get total number of organizations by type in efuse
   */
  @Trace()
  private async getTotalOrganizationsByType(): Promise<void> {
    const orgs = await Organization.find({}, { organizationType: 1, name: 1 }).read("secondary", MONGO_PREFERENCE_TAGS);

    const results = countBy(orgs.map((x) => x.organizationType));

    forEach(results, (value, key) => {
      EFuseAnalyticsUtil.incrementGauge("total_organizations.bytype", value, [`orgType:${key}`]);
    });

    const orgFollowers = (
      await OrganizationFollower.find({}, { followee: 1 }).lean().read("secondary", MONGO_PREFERENCE_TAGS)
    ).map((x) => x.followee);

    const orgFollowersCount = map(countBy(orgFollowers), (value, key) => {
      return { _id: key, followers: value };
    });

    const orgFollowersSorted = sortBy(orgFollowersCount, ["followers"]);

    // Build a map of org._id => org.name so we can look up highest followed orgs
    const orgLookup = {};
    orgs.forEach((x) => {
      orgLookup[x._id.toString()] = x.name;
    });

    orgFollowersSorted
      .reverse()
      .slice(0, 10)
      .forEach((x) => {
        const name = orgLookup[x._id];
        const value = x.followers;

        EFuseAnalyticsUtil.incrementGauge("total_organizations.byname", value, [`orgName:${name}`]);
      });
  }

  /**
   * @summary Get users portfolio stats
   */
  @Trace()
  private async getUserPortfolioStats(): Promise<void> {
    const profilePicture = await User.find({
      "profilePicture.filename": {
        $ne: "mindblown_white.png"
      }
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("user_profile_image", profilePicture);

    // Header Image
    const headerImage = await User.find({
      "headerImage.filename": {
        $ne: "no_image.jpg"
      }
    })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);
    EFuseAnalyticsUtil.incrementGauge("user_header_image", headerImage);

    // BIO
    const bio = await User.find({ bio: { $ne: "" } })
      .countDocuments()
      .read("secondary", MONGO_PREFERENCE_TAGS);

    EFuseAnalyticsUtil.incrementGauge("user_bio", bio);
  }

  @Trace({ name: StatsEnum.COLLECT_STATS_CRON, service: SERVICE })
  public static async collectStatsCron(): Promise<void> {
    const statsService = new StatsService();

    statsService
      .getTotalFollowers()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalFollowers promise"));
    statsService
      .getTotalUsers()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsers promise"));
    statsService
      .getTotalLikes()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalLikes promise"));
    statsService
      .getTotalComments()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalComments promise"));
    statsService
      .getTotalPosts()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalPosts promise"));
    statsService
      .getTotalNotifications()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalNotifications promise"));
    statsService
      .getTotalOrganizations()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalOrganizations promise"));
    statsService
      .getTotalUsersWithTwitchAccount()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsersWithTwitchAccount promise"));
    statsService
      .getTotalUsersWithDiscordAccount()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsersWithDiscordAccount promise"));
    statsService
      .getTotalUsersWithTwitterAccount()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsersWithTwitterAccount promise"));
    statsService
      .getTotalUsersWithGoogleAccount()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsersWithGoogleAccount promise"));
    statsService
      .getTotalUsersWithFacebookAccount()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsersWithFacebookAccount promise"));
    statsService
      .getTotalUsersWithLinkedinAccount()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalUsersWithLinkedinAccount promise"));
    statsService
      .getTotalTwitterFollowers()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalTwitterFollowers promise"));
    statsService
      .getTotalOrgMembers()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalOrgMembers promise"));
    statsService
      .getTotalOrganizationsByType()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalOrganizationsByType promise"));
    statsService
      .getTotalOpportunities()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalOpportunities promise"));
    statsService
      .getTotalApplicants()
      .catch((error) => statsService.logger?.error(error, "Error resolving getTotalApplicants promise"));
    statsService
      .getDiscordServerStats()
      .catch((error) => statsService.logger?.error(error, "Error resolving getDiscordServerStats promise"));
    statsService
      .getUserPortfolioStats()
      .catch((error) => statsService.logger?.error(error, "Error resolving getUserPortfolioStats promise"));
  }
}
