import { Types } from "mongoose";

import { Service, Trace } from "../decorators";
import { BaseModelService } from "../base";
import { IFriend } from "../../backend/interfaces/friend";
import { Friend } from "../../backend/models/friend";

@Service("friend-base.service")
export class FriendBaseService extends BaseModelService<IFriend> {
  constructor() {
    super(Friend);
  }

  /**
   * @summary Get followings by userId. This query uses cache
   *
   * @param {Types.ObjectId | string} userId
   * @param {number} [cache=60]
   *
   * @returns {Promise<IFriend[]>>}
   */
  @Trace()
  public async getFollowings(userId: Types.ObjectId | string, cache = 60): Promise<IFriend[]> {
    const friends = await Friend.find({ follower: userId }).lean().cache(cache);

    return friends;
  }
}
