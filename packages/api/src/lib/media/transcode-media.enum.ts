export enum SNSMessageType {
  subscriptionConfirmation = "SubscriptionConfirmation",
  notification = "Notification"
}

export enum SNSMessageDetailType {
  mediaConvertState = "MediaConvert Job State Change"
}

export enum MediaConvertJobStatus {
  complete = "COMPLETE",
  error = "ERROR",
  progress = "PROGRESSING"
}

export enum ImageQualities {
  sd = 25,
  hd = 50,
  uhd = 75
}

export enum MediaFeatureFlags {
  enableVideoTranscoding = "enable_video_transcoding",
  enableCloudflareStream = "enable_cloudflare_stream",
  enableEfuseWatermark = "enable_efuse_watermark"
}

export enum S3MediaDirectory {
  upload = "uploads/media",
  transcodeImage = "media/images",
  transcodeVideo = "media/video"
}

// Below values are for 8k
export enum AllowedImageDimensions {
  width = 7680,
  height = 4320
}

export enum S3ERenaDirectory {
  upload = "uploads/erena"
}
