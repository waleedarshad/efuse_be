import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions } from "mongoose";

import { ITranscodeMedia } from "../../backend/interfaces";
import { TranscodeMedia, TranscodeMediaModel } from "../../backend/models/transcode-media";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("transcode-media.service")
export class TranscodeMediaService extends BaseService {
  private transcodeMediaModel: TranscodeMediaModel<ITranscodeMedia> = TranscodeMedia;

  /**
   * @summary Create multiple transcode media objects using insertMany
   *
   * @param {ITranscodeMedia[]} transcodeMediaDocs
   *
   * @returns {Promise<ITranscodeMedia[]>}
   */
  @Trace()
  public async createMultiple(transcodeMediaDocs: ITranscodeMedia[]): Promise<ITranscodeMedia[]> {
    const createdTranscodeMediaDocs = await this.transcodeMediaModel.insertMany(transcodeMediaDocs);
    return createdTranscodeMediaDocs;
  }

  public async findOne(filter: FilterQuery<ITranscodeMedia>): Promise<ITranscodeMedia | null>;
  public async findOne(filter: FilterQuery<ITranscodeMedia>, projection: any): Promise<ITranscodeMedia | null>;
  public async findOne(
    filter: FilterQuery<ITranscodeMedia>,
    projection: any,
    options: QueryOptions
  ): Promise<ITranscodeMedia | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<ITranscodeMedia>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ITranscodeMedia | null> {
    const transcodeMedia = await this.transcodeMediaModel.findOne(filter, projection, options);
    return transcodeMedia;
  }

  /**
   * @summary Update a particular transcode media object
   *
   * @param {FilterQuery<ITranscodeMedia>} filter
   * @param {Partial<ITranscodeMedia>} transcodeMediaBody
   *
   * @returns {Promise<ITranscodeMedia>}
   */
  @Trace()
  public async update(
    filter: FilterQuery<ITranscodeMedia>,
    transcodeMediaBody: Partial<ITranscodeMedia>
  ): Promise<ITranscodeMedia> {
    // Make sure params are passed
    if (!transcodeMediaBody) {
      throw Failure.BadRequest("transcodeMediaBody is missing");
    }

    // Update the corresponding object as per condition
    const updatedTranscodeMedia: ITranscodeMedia | null = await this.transcodeMediaModel
      .findOneAndUpdate(filter, { ...transcodeMediaBody }, { new: true })
      .lean();

    // Make sure transcodeMedia object exists
    if (!updatedTranscodeMedia) {
      throw Failure.UnprocessableEntity("transcodeMedia not found");
    }

    return updatedTranscodeMedia;
  }

  public async find(filter: FilterQuery<ITranscodeMedia>): Promise<ITranscodeMedia[]>;
  public async find(filter: FilterQuery<ITranscodeMedia>, projection: any): Promise<ITranscodeMedia[]>;
  public async find(
    filter: FilterQuery<ITranscodeMedia>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<ITranscodeMedia[]>;
  @Trace()
  public async find(
    filter: FilterQuery<ITranscodeMedia>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ITranscodeMedia[]> {
    const transcodeMedia = await this.transcodeMediaModel.find(filter, projection, options);
    return transcodeMedia;
  }
}
