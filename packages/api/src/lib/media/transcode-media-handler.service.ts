import { Failure } from "@efuse/contracts";
import SNS from "aws-sdk/clients/sns";
import { isEmpty } from "lodash";
import { Types } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { AWS_DEFAULT_REGION, AWS_S3_BUCKET } from "@efuse/key-store";
import { ITranscodeMedia, ITranscodeMediaStatus, ITranscodeMediaDetail } from "../../backend/interfaces";
import { TranscodeMediaService } from "./transcode-media.service";
import { IMedia } from "../../backend/interfaces/media";
import { MediaService } from "./media.service";
import { S3_URL } from "../aws/s3.service";
import { MediaConvertJobStatus, S3MediaDirectory } from "./transcode-media.enum";

@Service("transcode-media-handler.service")
export class TranscodeMediaHandlerService extends BaseService {
  private sns: SNS;

  private transcodeMediaService: TranscodeMediaService;
  private mediaService: MediaService;

  constructor() {
    super();

    this.sns = new SNS({ region: AWS_DEFAULT_REGION });

    this.transcodeMediaService = new TranscodeMediaService();
    this.mediaService = new MediaService();
  }

  /**
   * @summary Confirm HTTPS endpoint subscription for SNS
   *
   * @param {string} topicARN
   * @param {string} token
   *
   * @links https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SNS.html#confirmSubscription-property
   *
   * @returns {Promise<SNS.Types.ConfirmSubscriptionResponse>}
   */
  @Trace()
  public async subscribeEndpoint(topicARN: string, token: string): Promise<SNS.Types.ConfirmSubscriptionResponse> {
    const params: SNS.Types.ConfirmSubscriptionInput = <SNS.Types.ConfirmSubscriptionInput>{
      TopicArn: topicARN,
      Token: token
    };

    const response: SNS.Types.ConfirmSubscriptionResponse = await this.sns.confirmSubscription(params).promise();
    return response;
  }

  /**
   * @summary Receive SNS notification & create the corresponding transcoded media objects
   *
   * @param {ITranscodeMediaStatus} message
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async transcodeStatusNotification(message: ITranscodeMediaStatus): Promise<void> {
    try {
      // Make sure message exists
      if (isEmpty(message)) {
        throw Failure.BadRequest("message is empty");
      }

      const { detail } = message;

      // Lookup for corresponding TranscodeMedia object
      const transcodeMedia: ITranscodeMedia | null = await this.transcodeMediaService.findOne({ jobId: detail.jobId });

      // Make sure transcodeMedia object exists
      if (!transcodeMedia) {
        throw Failure.UnprocessableEntity(
          `Received a SNS message with JobId: ${detail.jobId} which doesn't exist in TranscodeMedia collection`
        );
      }

      // Check if transcoding resulted in an error
      await (detail.status === MediaConvertJobStatus.error
        ? this.updateStatus(transcodeMedia, {
            status: detail.status,
            error: { code: detail.errorCode, message: detail.errorMessage }
          })
        : this.storeTranscodedMedia(transcodeMedia, detail));
    } catch (error) {
      this.logger?.error(error, "Error while creating media objects from SNS notification");
    }
  }

  /**
   * @summary Store the transcoded media into subsequent media objects
   *
   * @param {ITranscodeMedia} transcodeMedia
   * @param {ITranscodeMediaDetail} detail
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async storeTranscodedMedia(transcodeMedia: ITranscodeMedia, detail: ITranscodeMediaDetail): Promise<void> {
    // Fetch original media
    const originalMedia: IMedia | null = await this.mediaService.findOne({ _id: transcodeMedia.originalMedia });

    // Make sure media object exists
    if (!originalMedia) {
      throw Failure.UnprocessableEntity(
        `Corresponding media object not found against ID: ${String(transcodeMedia.originalMedia)}`
      );
    }

    // Build IMedia[]
    const mediaToBeCreated: IMedia[] = [];

    // Loop through each output group to get output file URL
    for (const output of detail.outputGroupDetails) {
      const { outputDetails } = output;

      // Build Media Object
      for (const outputDetail of outputDetails) {
        // Build file URL
        const outputURL = outputDetail.outputFilePaths[0];
        const fileKey = outputURL.split(`s3://${AWS_S3_BUCKET}/`)[1];
        const filename = fileKey.split(`${S3MediaDirectory.transcodeVideo}/`)[1];
        const fileURL = `${S3_URL}/${fileKey}`;

        // Fetch profile
        const splitURL = outputURL.split(".");
        const profile = splitURL[splitURL.length - 2];

        // Media object to be created
        const mediaObject: IMedia = <IMedia>{
          file: {
            filename,
            contentType: originalMedia.file.contentType,
            url: fileURL
          },
          profile,
          mediaable: originalMedia.mediaable,
          mediaableType: originalMedia.mediaableType,
          user: originalMedia.user
        };

        // Push to array
        mediaToBeCreated.push(mediaObject);
      }
    }
    // Create new Media Objects
    const mediaObjects: IMedia[] = await this.mediaService.createMediaObjects(mediaToBeCreated);

    // Mark transcodeMedia status as COMPLETE
    const newMediaIds: Types.ObjectId[] = mediaObjects.map((mo: IMedia): Types.ObjectId => <Types.ObjectId>mo._id);
    await this.updateStatus(transcodeMedia, { status: detail.status, transcodedMedia: newMediaIds });
  }

  /**
   * @summary Update transcodeMedia status
   *
   * @param {ITranscodeMedia} transcodeMedia
   * @param {string} status
   * @param {Types.ObjectId[]} newMediaObjectIds
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async updateStatus(
    transcodeMedia: ITranscodeMedia,
    fieldsToBeUpdated: Partial<ITranscodeMedia>
  ): Promise<void> {
    await this.transcodeMediaService.update({ _id: transcodeMedia._id }, { processed: true, ...fieldsToBeUpdated });
  }
}
