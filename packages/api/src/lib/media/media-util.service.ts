import { imageSize as sizeOf } from "image-size";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { AllowedImageDimensions } from "./transcode-media.enum";

@Service("media-util.service")
export class MediaUtilService extends BaseService {
  @Trace()
  public areDimensionsValid(input: string | Buffer): boolean {
    const dimensions = sizeOf(input);

    if (!dimensions.width || !dimensions.height) {
      return false;
    }

    return AllowedImageDimensions.width >= dimensions.width && AllowedImageDimensions.height >= dimensions.height;
  }
}
