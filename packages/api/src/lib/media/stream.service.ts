import axiosLib, { AxiosInstance, AxiosResponse } from "axios";
import { CLOUDFLARE_API_KEY, CLOUDFLARE_ACCOUNT_ID, BACKEND_DOMAIN } from "@efuse/key-store";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("Stream.service")
export class StreamService extends BaseService {
  private $axios: AxiosInstance;

  constructor() {
    super();

    this.$axios = axiosLib.create({
      baseURL: `https://api.cloudflare.com/client/v4/accounts/${CLOUDFLARE_ACCOUNT_ID}/stream`,
      headers: {
        Authorization: `Bearer ${CLOUDFLARE_API_KEY}`
      }
    });
  }

  /**
   * Upload videos to cloudflare stream
   *
   * @param videoUrl Url of the video to upload
   * @param options Any other valid stream options. Refer https://api.cloudflare.com/#stream-videos-upload-a-video-from-a-url
   */
  @Trace()
  public async uploadToStream(videoUrl: string, options: object = {}): Promise<string> {
    try {
      const notificationData = { notificationUrl: `${BACKEND_DOMAIN}/api/__webhook__/cf-stream-transcode` };

      await this.$axios.put(`/webhook`, notificationData);

      const data = { url: videoUrl, ...options };

      const response: AxiosResponse = await this.$axios.post(`/copy`, data);

      return <string>response?.data?.result?.uid;
    } catch (error) {
      throw this.handleError(error, "Error uploading to stream");
    }
  }
}
