import { Failure } from "@efuse/contracts";
import MediaConvert from "aws-sdk/clients/mediaconvert";
import { DeleteObjectOutput, HeadObjectOutput } from "aws-sdk/clients/s3";
import { Queue } from "bull";
import imagemin from "imagemin";
import imageminPngQuant from "imagemin-pngquant";
import imageminMozjpeg from "imagemin-mozjpeg";
import { isEmpty, difference, flatten } from "lodash";
import { Types, PaginateResult, QueryOptions, FilterQuery } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

import { Media, MediaModel } from "../../backend/models/media";
import { IMedia, IMediaUpdate, IResponse, IAssociatedMedia } from "../../backend/interfaces/media";
import { IFeedMedia } from "../../backend/interfaces/feed/feed-item";
import {
  AWS_DEFAULT_REGION,
  AWS_S3_BUCKET,
  CLOUDFLARE_STREAM_WATERMARK,
  MEDIA_CONVERT_ENDPOINT,
  MEDIA_CONVERT_JOB_TEMPLATE,
  MEDIA_CONVERT_QUEUE,
  MEDIA_CONVERT_ROLE
} from "@efuse/key-store";
import { MediaProfileEnum } from "@efuse/entities";
import { TranscodeMediaService } from "./transcode-media.service";
import { ITranscodeMedia } from "../../backend/interfaces";
import { MEDIA_CONVERT_JOB, TRANSCODE_VIDEO, TRANSCODE_IMAGE } from "../metrics";
import Config from "../../async/config";
import { MediaConvertJobStatus, MediaFeatureFlags, ImageQualities, S3MediaDirectory } from "./transcode-media.enum";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";

import { S3Service, S3ContentTypes, S3_URL } from "../aws/s3.service";
import { StreamService } from "./stream.service";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { UserSettingsService } from "../user-settings/user-settings";
import { MediaUtilService } from "./media-util.service";
import { ObjectId } from "../../backend/types";

const OUTPUT_DESTINATION = `s3://${AWS_S3_BUCKET}/media/video/`;

type MediaUpdate = Pick<IMediaUpdate, "mediaable" | "file" | "user" | "mediaableType" | "updatedAt">;

@Service("media.service")
export class MediaService extends BaseService {
  private mediaConvert: MediaConvert;

  private readonly media: MediaModel<IMedia> = Media;
  private transcodeMediaService: TranscodeMediaService;
  private userSettingsService: UserSettingsService;
  private mediaUtilService: MediaUtilService;
  private streamService: StreamService;

  private transcodeVideoQueue: Queue;
  private transcodeImageQueue: Queue;
  private createTranscodedImagesQueue: Queue;
  private storeMediaFileSizeQueue: Queue;
  private uploadToStreamQueue: Queue;

  private s3: S3Service;

  constructor() {
    super();

    this.mediaConvert = new MediaConvert({
      endpoint: MEDIA_CONVERT_ENDPOINT,
      region: AWS_DEFAULT_REGION
    });

    this.transcodeMediaService = new TranscodeMediaService();
    this.userSettingsService = new UserSettingsService();
    this.mediaUtilService = new MediaUtilService();
    this.streamService = new StreamService();

    this.transcodeVideoQueue = Config.initQueue("transcodeVideo");

    this.transcodeImageQueue = Config.initQueue("transcodeImage");

    this.createTranscodedImagesQueue = Config.initQueue("createTranscodedImages");

    this.storeMediaFileSizeQueue = Config.initQueue("storeMediaFileSize");

    this.uploadToStreamQueue = Config.initQueue("uploadToStream");

    this.s3 = new S3Service();
  }

  public async findOne(filter: FilterQuery<IMedia>): Promise<IMedia | null>;
  public async findOne(filter: FilterQuery<IMedia>, projection: any): Promise<IMedia | null>;
  public async findOne(
    filter: FilterQuery<IMedia>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IMedia | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IMedia>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IMedia | null> {
    const media = await this.media.findOne(filter, projection, options);
    return media;
  }

  /**
   * @summary Get all media for a given post
   *
   * @param {Types.ObjectId | string} mediaId
   *
   * @returns {Promise<IMedia>}
   */
  @Trace()
  public async getMediaByPostId(postId: Types.ObjectId): Promise<IMedia[]> {
    return this.find({ mediaable: postId });
  }

  /**
   * @summary Update a particular media object
   *
   * @param {Types.ObjectId | string} mediaId
   * @param {MediaUpdate} mediaBody
   *
   * @returns {Promise<IMedia>}
   */
  @Trace()
  public async update(mediaId: Types.ObjectId | string, mediaBody: MediaUpdate): Promise<IMedia> {
    // Make sure ID is present
    if (!mediaId) {
      throw Failure.BadRequest("media ID not found");
    }

    // Make sure ID is valid
    if (!Types.ObjectId.isValid(mediaId)) {
      throw Failure.BadRequest("invalid media ID");
    }

    // Make sure params are passed
    if (!mediaBody) {
      throw Failure.BadRequest("params are not found");
    }

    const updatedMedia: IMedia | null = await this.media
      .findOneAndUpdate({ _id: mediaId }, { ...mediaBody }, { new: true })
      .lean();

    if (!updatedMedia) {
      throw Failure.UnprocessableEntity("Requested media object not found");
    }

    return updatedMedia;
  }

  @Trace()
  public async updateMany(mediaIds: Types.ObjectId[] | string[], mediaBody: MediaUpdate): Promise<IResponse> {
    if (!mediaIds) {
      throw Failure.BadRequest("media ID not found");
    }

    // Make sure params are passed
    if (!mediaBody) {
      throw Failure.BadRequest("params are not found");
    }

    const updatedMedia: IResponse = await this.media.updateMany({ _id: { $in: mediaIds } }, { $set: mediaBody });
    if (!updatedMedia.ok) {
      throw Failure.UnprocessableEntity("Unable to create Media");
    }
    return updatedMedia;
  }

  @Trace()
  public async create(media: IMedia): Promise<IMedia> {
    if (!media) {
      throw Failure.BadRequest("params are not found");
    }

    const doc = <IMedia>media;
    const newMedia = await this.media.create(doc);

    // We will get profile only incase of transcoded media
    if (isEmpty(newMedia.profile)) {
      const { contentType, filename } = newMedia.file;
      // TranscodeVideo when contentType includes video
      if (contentType?.includes("video")) {
        if (await this.videoTranscodingEnabled()) {
          await this.transcodeVideoAsync([newMedia]);
        }
        if (await this.streamUploadEnabled()) {
          const options: any = {
            meta: { name: filename }
          };
          if (await this.watermarkEnabled()) {
            options.watermark = {
              uid: `${CLOUDFLARE_STREAM_WATERMARK}`
            };
          }
          await this.uploadToStreamAsync([newMedia], options);
        }
      } else if (this.shouldTranscodeImage(contentType)) {
        await this.createTranscodedImagesAsync([newMedia]);
      }
    }

    // Fetch & store media size
    await this.storeMediaFileSizeAsync([newMedia]);

    return newMedia;
  }

  @Trace()
  public async createMediaObjects(mediaFiles: IMedia[]): Promise<IMedia[]> {
    try {
      const mediaObjects = await this.media.insertMany(mediaFiles);

      // Fetch that media where profile is empty & contentType includes video
      const videosToBeTranscoded: IMedia[] = mediaObjects.filter(
        (media) => isEmpty(media.profile) && media.file.contentType?.includes("video")
      );

      // Fetch that media where profile is empty & contentType is image/jpg or image/png
      const imagesToBeTranscoded: IMedia[] = mediaObjects.filter(
        (media) =>
          isEmpty(media.profile) &&
          (media.file.contentType === S3ContentTypes.PNG || media.file.contentType === S3ContentTypes.JPEG)
      );

      // Add to async queue for transcoding
      if (videosToBeTranscoded.length > 0 && (await this.videoTranscodingEnabled())) {
        await this.transcodeVideoAsync(videosToBeTranscoded);
      }

      // Add images to async queue for transcoding
      if (imagesToBeTranscoded.length > 0) {
        await this.createTranscodedImagesAsync(imagesToBeTranscoded);
      }

      // Fetch & store media size
      await this.storeMediaFileSizeAsync(mediaObjects);

      return mediaObjects;
    } catch {
      throw Failure.BadRequest("Unable to create media objects");
    }
  }

  /**
   * @summary Create TranscodeMedia objects and initialize the transcoding process
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async createTranscodedImages(mediaObjects: IMedia[]): Promise<void> {
    try {
      const transcodedMedia: ITranscodeMedia[] = [];

      for (const media of mediaObjects) {
        const transcodeMedia: ITranscodeMedia = <ITranscodeMedia>{
          originalMedia: media._id,
          processed: false,
          status: MediaConvertJobStatus.progress
        };

        transcodedMedia.push(transcodeMedia);
      }

      await this.transcodeMediaService.createMultiple(transcodedMedia);

      await this.transcodeImagesAsync(mediaObjects);
    } catch (error) {
      this.logger?.error({ error });
    }
  }

  /**
   * @summary Use imagemin and transcode images
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async transcodeImages(mediaObjects: IMedia[]): Promise<void> {
    try {
      this.recordMetric(TRANSCODE_IMAGE.START);
      const versions: string[] = Object.keys(MediaProfileEnum).filter((profile) => profile !== MediaProfileEnum.HLS); // Ignore hls version as it is for videos only

      for (const media of mediaObjects) {
        const transcodedMediaObject = await this.transcodeMediaService.findOne({ originalMedia: media._id }); // Fetch corressponding transcoded media object

        if (!transcodedMediaObject) {
          throw Failure.UnprocessableEntity(
            `Corresponding transcoded media object not found against ID: ${String(media._id)}`
          );
        }

        try {
          const { filename } = media.file;
          const downloadFilePath = `${S3MediaDirectory.upload}/${<string>filename}`;
          const uploadFilePath = `${S3MediaDirectory.transcodeImage}/${<string>filename}`;
          const fileType = media.file.contentType === S3ContentTypes.JPEG ? S3ContentTypes.JPEG : S3ContentTypes.PNG;
          const readableFileType = fileType === S3ContentTypes.JPEG ? "jpg" : "png";

          const { Body } = await this.s3.downloadMediaFromS3(downloadFilePath); // Download Image from S3
          const data = <Buffer>Body;

          // Making sure image falls in supported dimensions
          const validDimensions: boolean = this.mediaUtilService.areDimensionsValid(data);

          if (!validDimensions) {
            throw Failure.BadRequest("Image dimensions are not supported");
          }

          const transcodedImages: Buffer[] = await Promise.all(
            fileType === S3ContentTypes.JPEG
              ? versions.map((version) =>
                  imagemin.buffer(data, { plugins: [imageminMozjpeg({ quality: ImageQualities[version] })] })
                )
              : versions.map((version) =>
                  imagemin.buffer(data, {
                    plugins: [
                      imageminPngQuant({
                        quality: [ImageQualities[version] / 100, ImageQualities[version] / 100 + 0.1]
                      })
                    ]
                  })
                )
          );

          const destinations: string[] = versions.map((version) =>
            GeneralHelper.changeExtension(uploadFilePath, `.${version}.${readableFileType}`)
          );

          await Promise.all(
            versions.map((_, index) => this.s3.uploadMediaToS3(transcodedImages[index], destinations[index], fileType))
          );

          const originalMedia = await this.media.findOne({ _id: media._id }); // Fetch media from database

          // Make sure media object exists
          if (!originalMedia) {
            throw Failure.UnprocessableEntity(`Corresponding media object not found against ID: ${String(media._id)}`);
          }

          const transcodedMediaFiles: IMedia[] = versions.map(
            (profile, index) =>
              <IMedia>{
                file: {
                  // todo: investigate & fix
                  // @ts-ignore
                  filename: GeneralHelper.changeExtension(filename, `.${profile}.${readableFileType}`),
                  contentType: fileType,
                  url: `${S3_URL}/${destinations[index]}`
                },
                user: originalMedia.user,
                mediaable: originalMedia.mediaable,
                mediaableType: originalMedia.mediaableType,
                fileOriginalUrl: `${S3_URL}/${destinations[index]}`,
                profile
              }
          );

          const newMedia = await this.createMediaObjects(transcodedMediaFiles); // Create corresponding media objects
          const transcodedMediaIDs: Types.ObjectId[] = newMedia.map(
            (_transcodedMedia): Types.ObjectId => <Types.ObjectId>_transcodedMedia._id
          );

          await this.transcodeMediaService.update(
            { _id: transcodedMediaObject._id },
            { processed: true, transcodedMedia: transcodedMediaIDs, status: MediaConvertJobStatus.complete }
          ); // Update transcode media object to have the reference to newly created media versions
          this.recordMetric(TRANSCODE_IMAGE.FINISH);
        } catch (error) {
          this.recordMetric(TRANSCODE_IMAGE.ERROR);

          await this.transcodeMediaService.update(
            {
              _id: transcodedMediaObject._id
            },
            {
              status: MediaConvertJobStatus.error,
              error: {
                message: error.message || ""
              }
            }
          );

          this.logger?.error({ error, media }, "Error while transcoding media object");
        }
      }
    } catch (error) {
      this.logger?.error(error, "Error while transcoding images");
    }
  }

  /**
   * @summary Create MediaConvert jobs to transcode video
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async transcodeVideo(mediaObjects: IMedia[]): Promise<void> {
    try {
      this.recordMetric(TRANSCODE_VIDEO.START);

      const transcodeMediaObjects: ITranscodeMedia[] = [];

      // Iterate through media objects & create job to transcode
      for (const media of mediaObjects) {
        this.recordMetric(MEDIA_CONVERT_JOB.START);

        try {
          const params = {
            JobTemplate: MEDIA_CONVERT_JOB_TEMPLATE,
            Queue: MEDIA_CONVERT_QUEUE,
            Role: MEDIA_CONVERT_ROLE,
            Settings: {
              Inputs: [{ FileInput: media.fileOriginalUrl }],
              OutputGroups: [{ OutputGroupSettings: { FileGroupSettings: { Destination: OUTPUT_DESTINATION } } }]
            }
          };

          const response = await this.mediaConvert.createJob(params).promise();

          const transcodeMedia: ITranscodeMedia = <ITranscodeMedia>{
            jobId: response.Job?.Id,

            originalMedia: media._id,
            processed: false
          };

          transcodeMediaObjects.push(transcodeMedia);

          this.recordMetric(MEDIA_CONVERT_JOB.FINISH);
        } catch (error) {
          this.recordMetric(MEDIA_CONVERT_JOB.ERROR);
          this.logger?.error(error, "Error while creating media convert job");
        }
      }

      // Create TranscodeMedia objects
      await this.transcodeMediaService.createMultiple(transcodeMediaObjects);
      this.recordMetric(TRANSCODE_VIDEO.FINISH);
    } catch (error) {
      this.recordMetric(TRANSCODE_VIDEO.ERROR);
      this.logger?.error(error, "Error while transcoding");
    }
  }

  /**
   * @summary Create MediaConvert jobs to transcode video using bull queue
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async transcodeVideoAsync(mediaObjects: IMedia[]): Promise<void> {
    await this.transcodeVideoQueue.add({ mediaObjects });
  }

  /**
   * @summary Create transcode images job using bull queue
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  public async transcodeImagesAsync(mediaObjects: IMedia[]): Promise<void> {
    await this.transcodeImageQueue.add({ mediaObjects });
  }

  /**
   * @summary Initialize transcoding of images using bull queue
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  public async createTranscodedImagesAsync(mediaObjects: IMedia[]): Promise<void> {
    await this.createTranscodedImagesQueue.add({ mediaObjects });
  }

  public async find(filter: FilterQuery<IMedia>): Promise<IMedia[]>;
  public async find(filter: FilterQuery<IMedia>, projection: any): Promise<IMedia[]>;
  public async find(
    filter: FilterQuery<IMedia>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IMedia[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IMedia>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IMedia[]> {
    const media = await this.media.find(filter, projection, options);
    return media;
  }

  /**
   * @summary Get the associatedMedia based on the user's desired media profile
   *
   * @param {Types.ObjectId | string} userId - User who is requesting data, mostly currentUser
   * @param {Types.ObjectId | string} mediable
   * @param {string} mediableType
   *
   * @returns {Promise<IAssociatedMedia>}
   */
  @Trace()
  public async getAssociatedMedia(
    userId: ObjectId,
    mediable: Types.ObjectId,
    mediableType: string
  ): Promise<IAssociatedMedia> {
    // Get the original media
    const originalMediaObjects = mediable
      ? await this.find({
          mediaable: mediable,
          mediaableType: mediableType,

          profile: <any>{ $eq: null }
        })
      : [];

    // Proceed only when we get originalMediaObjects
    if (isEmpty(originalMediaObjects)) {
      return { media: undefined, medias: [] };
    }

    // Get transcoded media
    const originalMediaIds: string[] = originalMediaObjects.map((m) => String(m._id));
    const transcodedMediaObject: ITranscodeMedia[] = await this.getTranscodeMediaObjects(originalMediaIds);

    // Proceed only when we get transcoded media
    if (isEmpty(transcodedMediaObject)) {
      return { media: originalMediaObjects[0], medias: originalMediaObjects };
    }

    // Make sure we have version for each original media
    const originalMediaIdsInTranscodeMediaObject = transcodedMediaObject.map((t) => String(t.originalMedia));
    const mediaIdsWithNoVersions = difference(originalMediaIds, originalMediaIdsInTranscodeMediaObject);

    // Retrieve user's desired media
    const userMediaSetting = await this.userSettingsService.get(userId, { media: 1 });
    const desiredProfile = userMediaSetting.media?.profile || MediaProfileEnum.HD;

    const transcodedMediaIds = flatten(transcodedMediaObject.map((t) => t.transcodedMedia));
    const hlsMediaIds = transcodedMediaObject.map((ob) => ob.hlsMedia).filter((ob) => ob !== undefined);

    // Return hls version of media if it is available
    const desiredMedia: IMedia[] = await (isEmpty(hlsMediaIds)
      ? this.find({ _id: { $in: transcodedMediaIds }, profile: desiredProfile })
      : this.find({ _id: { $in: hlsMediaIds }, profile: MediaProfileEnum.HLS }));

    // Return the desiredMedia when each media has transcoded versions
    if (mediaIdsWithNoVersions.length === 0) {
      return { media: desiredMedia[0], medias: desiredMedia };
    }

    // Build media array to be returned
    const mediaToBeReturned: IMedia[] = [
      ...desiredMedia,
      ...originalMediaObjects.filter((m) => mediaIdsWithNoVersions.includes(String(m._id)))
    ];
    return { media: mediaToBeReturned[0], medias: mediaToBeReturned };
  }

  /**
   * @summary Get file size & store in DB
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeMediaFileSize(mediaObjects: IMedia[]): Promise<void> {
    try {
      // Build S3 file keys
      const fileKeys = this.buildFileKeys(mediaObjects);

      // Build promises
      const promises: Promise<HeadObjectOutput | null>[] = fileKeys.map<Promise<HeadObjectOutput | null>>(
        (key: string): Promise<HeadObjectOutput | null> =>
          this.s3.getMetadataWithoutFileDownload(key).catch((): null => null)
      );

      // Resolve promises concurrently
      const resolvedPromises = await Promise.all(promises);

      // Build media objects to be updated
      const bulkOperation: any[] = mediaObjects
        .map((media: IMedia, index: number) => {
          const fileMetadata: HeadObjectOutput | null = resolvedPromises[index];

          // Don't update when no promise is found
          if (!fileMetadata || !fileMetadata.ContentLength) {
            return;
          }

          // ordered: this would make sure all queries are processed, otherwise mongoose stops processing on first error
          return {
            updateOne: { filter: { _id: media._id }, update: { size: fileMetadata.ContentLength }, ordered: false }
          };
        })
        .filter((op) => !isEmpty(op));

      await this.media.bulkWrite(bulkOperation);
    } catch (error) {
      this.logger?.error(error, "Error while storing MediaObject Size");
    }
  }

  /**
   * @summary Get file size & store asynchronously
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async storeMediaFileSizeAsync(mediaObjects: IMedia[]): Promise<void> {
    await this.storeMediaFileSizeQueue.add({ mediaObjects });
  }

  /**
   * Async Processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "media.service" })
  public static InitAsyncProcessors(): void {
    const mediaService = new MediaService();

    mediaService.transcodeVideoQueue
      .process((job: { data: { mediaObjects: IMedia[] } }) => {
        return mediaService.transcodeVideo(job.data.mediaObjects);
      })
      .catch((error) => mediaService.logger?.error(error, "Error while processing transcodeVideoQueue"));

    mediaService.transcodeImageQueue
      .process((job: { data: { mediaObjects: IMedia[] } }) => {
        return mediaService.transcodeImages(job.data.mediaObjects);
      })
      .catch((error) => mediaService.logger?.error(error, "Error while processing transcodeImageQueue"));

    mediaService.createTranscodedImagesQueue
      .process((job: { data: { mediaObjects: IMedia[] } }) => {
        return mediaService.createTranscodedImages(job.data.mediaObjects);
      })
      .catch((error) => mediaService.logger?.error(error, "Error while processing createTranscodedImagesQueue"));

    mediaService.storeMediaFileSizeQueue
      .process((job: { data: { mediaObjects: IMedia[] } }) => {
        return mediaService.storeMediaFileSize(job.data.mediaObjects);
      })
      .catch((error) => mediaService.logger?.error(error, "Error while processing storeMediaFileSizeQueue"));

    mediaService.uploadToStreamQueue
      .process((job: { data: { mediaObjects: IMedia[]; options: object } }) => {
        return mediaService.uploadToStream(job.data.mediaObjects, job.data.options);
      })
      .catch((error) => mediaService.logger?.error(error, "Error while processing uploadToStreamQueue"));
  }

  /**
   * @summary Delete media from DB & s3
   *
   * @param {FilterQuery<IMedia>} condition
   * @param {string[]} s3FileKeys
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async delete(filter: FilterQuery<IMedia>, s3FileKeys: string[]): Promise<void> {
    // First delete the objects from DB
    await this.media.deleteMany(filter);

    /**
     * if for some reason we are unable to delete media from s3 or we have a wrong key then
     * it should not break the code
     */
    try {
      if (!isEmpty(s3FileKeys)) {
        const deleteMediaFromS3Promises: Promise<DeleteObjectOutput>[] = s3FileKeys.map<Promise<DeleteObjectOutput>>(
          (key): Promise<DeleteObjectOutput> => this.s3.deleteMediaFromS3(key)
        );
        // Once records are deleted from DB then doing cleanup in S3
        await Promise.all(deleteMediaFromS3Promises);
      }
    } catch {
      this.logger?.debug({ s3FileKeys }, "Unable to delete media from s3");
    }
  }

  /**
   * @summary Update media for a particular mediaable item
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} mediaable
   * @param {string} mediaableType
   * @param {IFeedMedia} mediaObjectsBody
   *
   * @returns {Promise<undefined | void>}
   */
  @Trace()
  public async updateMedia(
    userId: Types.ObjectId | string,
    mediaable: Types.ObjectId | string,
    mediaableType: string,
    mediaObjectsBody: IFeedMedia[]
  ): Promise<undefined | void> {
    const mediaObjectDB = await this.find({ mediaable, mediaableType });

    // When No media exists | Simply exit
    if (isEmpty(mediaObjectsBody) && isEmpty(mediaObjectDB)) {
      return;
    }

    // When media exists & user opted to delete all media
    if (isEmpty(mediaObjectsBody)) {
      const fileKeys = this.buildFileKeys(mediaObjectDB);
      await this.delete({ mediaable, mediaableType }, fileKeys);
      return;
    }

    // Validate Media
    this.validateMedia(mediaObjectsBody);

    // If it reaches here then user has valid media
    const mediaToBeCreated: IFeedMedia[] = mediaObjectsBody.filter((mo) => isEmpty(mo.id));
    const formattedMediaObjects: IMedia[] = this.formatMediaBodyToMatchSchema(
      userId,
      mediaable,
      mediaableType,
      mediaToBeCreated
    );

    // No media exists in DB go ahead & create
    if (isEmpty(mediaObjectDB)) {
      await this.createMediaObjects(formattedMediaObjects);
      return;
    }

    const originalMediaObjects: IMedia[] = mediaObjectDB.filter((mo) => isEmpty(mo.profile));
    const originalMediaIds: string[] = originalMediaObjects.map((m) => String(m._id));
    const transcodedMediaObjects: ITranscodeMedia[] = await this.getTranscodeMediaObjects(originalMediaIds);

    // Build Media to be retained
    const mediaToBeRetained: string[] = this.getMediaIdsToBeRetained(
      mediaObjectsBody,
      originalMediaObjects,
      transcodedMediaObjects
    );

    // Media to be Deleted
    let mediaToBeDeletedIds = difference(originalMediaIds, mediaToBeRetained);

    // Only proceed when we have some media to be deleted
    if (mediaToBeDeletedIds.length > 0) {
      // Pull in corresponding transcoded media ids to be deleted
      transcodedMediaObjects.forEach((tmo) => {
        if (mediaToBeDeletedIds.includes(String(tmo.originalMedia))) {
          const transcodedMediaIds: string[] = tmo.transcodedMedia.map((id) => String(id));
          mediaToBeDeletedIds = [...mediaToBeDeletedIds, ...transcodedMediaIds];
        }
      });

      // Now we have ids get all the media object that are to be deleted
      const mediaObjectsToBeDeleted = mediaObjectDB.filter((mo) => mediaToBeDeletedIds.includes(String(mo._id)));

      // When media exists & user opted to delete all media
      const fileKeys = this.buildFileKeys(mediaObjectsToBeDeleted);
      await this.delete({ _id: { $in: mediaToBeDeletedIds } }, fileKeys);
    }

    // Create any media we have
    if (formattedMediaObjects.length > 0) {
      await this.createMediaObjects(formattedMediaObjects);
    }
  }

  /**
   * @summary Build S3 fileKeys from a collection of mediaObjects
   *
   * @param {IMedia[]} mediaObjects
   *
   * @returns {string[]}
   */
  @Trace()
  public buildFileKeys(mediaObjects: IMedia[]): string[] {
    const fileKeys: string[] = [];

    mediaObjects
      .filter((ob) => ob.profile !== MediaProfileEnum.HLS) // Ignore hls media objects as they already have size when created
      .forEach((media) => {
        const { file, profile } = media;
        const { filename, contentType } = file;
        const transcoded = !isEmpty(profile);

        if (filename && contentType) {
          const fileKey = contentType.includes("video")
            ? this.getVideoFileKey(transcoded, filename)
            : this.getImageFileKey(transcoded, filename);

          fileKeys.push(fileKey);
        }
      });

    return fileKeys;
  }

  /**
   * @summary Get paginated feed media of a particular user
   *
   * @param {Types.ObjectId | string} userId
   * @param {number} page
   * @param {number} limit
   *
   * @returns {Promise<PaginateResult<IMedia>>}
   */
  @Trace()
  public async getPaginatedFeedMediaOfUser(
    userId: Types.ObjectId | string,
    page = 1,
    limit = 10
  ): Promise<PaginateResult<IMedia>> {
    // Make sure we have a userId
    if (!userId) {
      throw Failure.BadRequest("userId is required");
    }

    // Make sure userId is valid
    if (!this.isValidObjectId(userId)) {
      throw Failure.BadRequest("Invalid userId");
    }

    // Make sure page param is valid
    let pageParam = page;
    if (pageParam <= 0 || Number.isNaN(page)) {
      pageParam = 1;
    }

    // Make sure page limit is valid
    let limitParam = limit;
    if (limitParam <= 0) {
      limitParam = 10;
    }

    // Must not return more than 30
    if (limitParam > 30 || Number.isNaN(limit)) {
      limitParam = 30;
    }

    // Get paginated collection
    const paginatedMedia: PaginateResult<IMedia> = await this.media.paginate(
      { user: userId, mediaableType: "feeds", profile: { $exists: false } },
      { page: pageParam, limit: limitParam, lean: true, sort: { createdAt: -1 } }
    );

    return paginatedMedia;
  }

  /**
   * @summary Check whether video transcoding is enabled or not
   *
   * @returns {Promise<boolean>}
   */
  @Trace()
  private async videoTranscodingEnabled(): Promise<boolean> {
    const enabled = await FeatureFlagService.hasFeature(MediaFeatureFlags.enableVideoTranscoding);
    return enabled;
  }

  /**
   * @summary Get S3 fileKey for Image
   *
   * @param {boolean} transcoded
   * @param {string} filename
   *
   * @returns {string}
   */
  @Trace()
  private getImageFileKey(transcoded: boolean, filename: string): string {
    const fileKey = transcoded ? `${S3MediaDirectory.transcodeImage}/${filename}` : this.getUploadedMediaKey(filename);
    return fileKey;
  }

  /**
   * @summary Get S3 fileKey for Video
   *
   * @param {boolean} transcoded
   * @param {string} filename
   *
   * @returns {string}
   */
  @Trace()
  private getVideoFileKey(transcoded: boolean, filename: string): string {
    const fileKey = transcoded ? `${S3MediaDirectory.transcodeVideo}/${filename}` : this.getUploadedMediaKey(filename);
    return fileKey;
  }

  /**
   * @summary Get S3 fileKey for Upload Media
   *
   * @param {boolean} transcoded
   * @param {string} filename
   *
   * @returns {string}
   */
  @Trace()
  private getUploadedMediaKey(filename: string): string {
    return `${S3MediaDirectory.upload}/${filename}`;
  }

  /**
   * @summary Format media objects to match DB schema
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} mediaable
   * @param {string} mediaableType
   * @param {IFeedMedia[]} mediaObjects
   *
   * @returns {IMedia[]}
   */
  @Trace()
  private formatMediaBodyToMatchSchema(
    userId: Types.ObjectId | string,
    mediaable: Types.ObjectId | string,
    mediaableType: string,
    mediaObjects: IFeedMedia[]
  ): IMedia[] {
    const formattedMedia: IMedia[] = mediaObjects.map(
      (media: IFeedMedia): IMedia =>
        <IMedia>{
          file: { filename: media.filename, contentType: media.contentType, url: media.url },
          user: userId,
          mediaable,
          mediaableType
        }
    );

    return formattedMedia;
  }

  /**
   * @summary Validate media object & throw exception where possible
   *
   * @param {IFeedMedia} mediaObjects
   *
   * @return {void}
   */
  @Trace()
  private validateMedia(mediaObjects: IFeedMedia[]): void {
    if (mediaObjects.length > 4) {
      throw Failure.BadRequest("You are only allowed to post 1 video or 4 images per post");
    }

    const videoMedia: IFeedMedia[] = mediaObjects.filter((mo) => mo.contentType?.includes("video"));
    if (videoMedia.length > 1) {
      throw Failure.BadRequest("You are not allowed to post more than 1 video per post");
    }

    const imageMedia: IFeedMedia[] = mediaObjects.filter((mo) => mo.contentType?.includes("image"));
    if (imageMedia.length > 4) {
      throw Failure.BadRequest("You are not allowed to post more than 4 images per post");
    }

    if (videoMedia.length > 0 && imageMedia.length > 0) {
      throw Failure.BadRequest("You are not allowed to post both image & video");
    }
  }

  /**
   * @summary Get transcodeMedia objects given mediaIds
   *
   * @param {string[]} originalMediaIds
   *
   * @returns {Promise<ITranscodeMedia[]>}
   */
  @Trace()
  private async getTranscodeMediaObjects(originalMediaIds: string[]): Promise<ITranscodeMedia[]> {
    const transcodedMediaObjects: ITranscodeMedia[] = await this.transcodeMediaService.find(
      {
        originalMedia: { $in: originalMediaIds },
        status: MediaConvertJobStatus.complete,
        processed: true,
        hlsMedia: undefined // TODO: Remove this to display HLS versions of videos ( Akhil )
      },
      {},
      { lean: true }
    );

    return transcodedMediaObjects;
  }

  /**
   * @summary Get array media ids which are to be retained in DB upon update
   *
   * @param {IFeedMedia[]} mediaObjectsBody
   * @param {IMedia[]} originalMediaObjects
   * @param {ITranscodeMedia[]} transcodedMediaObjects
   *
   * @returns {string[]}
   */
  @Trace()
  private getMediaIdsToBeRetained(
    mediaObjectsBody: IFeedMedia[],
    originalMediaObjects: IMedia[],
    transcodedMediaObjects: ITranscodeMedia[]
  ): string[] {
    const mediaToBeRetained: string[] = [];
    const existingMediaObjects: IFeedMedia[] = mediaObjectsBody.filter((mo) => !isEmpty(mo.id));

    // Loop through each media object passed & get the corresponding media object in DB
    existingMediaObjects.forEach((media: IFeedMedia) => {
      // Find if we we have received original media object from FE
      const correspondingOriginalMedia: IMedia | undefined = originalMediaObjects.find(
        (mo: IMedia) => String(mo._id) === String(media.id)
      );

      if (correspondingOriginalMedia) {
        mediaToBeRetained.push(String(correspondingOriginalMedia._id));
      } else {
        // FE has passed a transcoded media object
        const transcodedMedia: ITranscodeMedia | undefined = transcodedMediaObjects.find((tmo) => {
          const transcodedMediaIds: string[] = tmo.transcodedMedia.map((id) => String(id));
          return transcodedMediaIds.includes(media.id);
        });

        // Once transcodeMedia is found push the original media ID
        if (transcodedMedia) {
          mediaToBeRetained.push(String(transcodedMedia.originalMedia));
        }
      }
    });

    return mediaToBeRetained;
  }

  /**
   * @summary Initiate media upload process to cloudflare stream
   *
   * @param {IMedia[]} mediaObjects
   * @param {object} options
   */
  @Trace()
  private async uploadToStream(mediaObjects: IMedia[], options: object = {}): Promise<void> {
    try {
      const transcodeMediaObjects: ITranscodeMedia[] = [];

      // Iterate through media objects & upload to stream.
      for (const media of mediaObjects) {
        // Start upload to stream
        const uid = await this.streamService.uploadToStream(media.fileOriginalUrl, options);

        const transcodeMedia: ITranscodeMedia = <ITranscodeMedia>{
          jobId: uid,

          originalMedia: media._id,
          processed: false
        };
        transcodeMediaObjects.push(transcodeMedia);
      }

      // Create TranscodeMedia objects
      await this.transcodeMediaService.createMultiple(transcodeMediaObjects);
    } catch (error) {
      this.logger?.error(error, "Error while uploading to stream.");
    }
  }

  /**
   * @summary Check whether uploading to cloudflare stream is enabled or not
   *
   * @returns {Promise<boolean>}
   */
  @Trace()
  private async streamUploadEnabled(): Promise<boolean> {
    const enabled = await FeatureFlagService.hasFeature(MediaFeatureFlags.enableCloudflareStream);
    return enabled;
  }

  /**
   * @summary Check whether watermark should be included or not for cloudflare stream uploads
   *
   * @returns {Promise<boolean>}
   */
  @Trace()
  private async watermarkEnabled(): Promise<boolean> {
    const enabled = await FeatureFlagService.hasFeature(MediaFeatureFlags.enableEfuseWatermark);
    return enabled;
  }

  /**
   * Create uploading to stream job using bull queue
   *
   * @param {IMedia[]} mediaObjects
   * @param {object} options
   */
  @Trace()
  public async uploadToStreamAsync(mediaObjects: IMedia[], options: object = {}): Promise<void> {
    await this.uploadToStreamQueue.add({ mediaObjects, options });
  }

  /**
   * @summary Check whether media can be transcoded as image
   *
   * @param {string} contentType
   *
   * @returns {boolean}
   */
  private shouldTranscodeImage(contentType): boolean {
    return (
      contentType?.includes("image") && (contentType === S3ContentTypes.PNG || contentType === S3ContentTypes.JPEG)
    );
  }
}
