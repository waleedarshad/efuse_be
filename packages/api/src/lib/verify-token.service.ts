import { Service, Trace } from "./decorators";
import { BaseService } from "./base/base.service";
import jwt, { GetPublicKeyOrSecret, JwtPayload } from "jsonwebtoken";
import { AuthenticationError } from "apollo-server-express";
import jwksClient from "jwks-rsa";
import { UserStatus } from "@efuse/entities";
import { isEmpty } from "lodash";

import { Session } from "../backend/models/session";
import { User } from "../backend/models/User";
import { IUser } from "../backend/interfaces/user";
import { EfuseAuthEnum } from "./auth/auth.enum";
import { UserFields } from "./users/user-fields";

import {
  AUTH0_EFUSE_PROFILE_CLAIM,
  AUTH0_INTERNAL_AUDIENCE,
  AUTH0_INTERNAL_DOMAIN,
  AUTH0_INTERNAL_ISSUER_URL,
  JWT_KEY,
  JWT_MAX_AGE
} from "@efuse/key-store";

const ERROR_MESSAGE = "Unauthorized";

@Service("verify-token.service")
export class VerifyToken extends BaseService {
  @Trace()
  public async validate(token: string): Promise<IUser> {
    try {
      let user: IUser | undefined;

      const decodedToken = jwt.decode(token, { complete: true });

      if (!decodedToken) {
        throw new AuthenticationError(ERROR_MESSAGE);
      }

      const decodedPayload = decodedToken.payload;

      // Validate Auth0 Token
      if (decodedPayload.iss === AUTH0_INTERNAL_ISSUER_URL) {
        user = await this.validateAuth0TokenAndGetUser(token);
      }

      // Validate eFuse token
      if (decodedPayload.iss === EfuseAuthEnum.ISS) {
        user = await this.validateEfuseTokenAndGetUser(token);
      }

      if (isEmpty(user)) {
        this.logger?.info("User not found!");
        throw new AuthenticationError(ERROR_MESSAGE);
      }

      return user as IUser;
    } catch (error) {
      this.logger?.info({ stack: error.stack, message: error.message, error }, "Error while validating jwt-token");
      throw new AuthenticationError(ERROR_MESSAGE);
    }
  }

  @Trace()
  public async validateAuth0TokenAndGetUser(token: string): Promise<IUser> {
    const payload = await this.verifyAuth0Token(token);

    const { userId } = <{ userId: string | undefined }>payload[AUTH0_EFUSE_PROFILE_CLAIM];

    if (!userId) {
      this.logger?.info("userId not found! | Auth0");
      throw new AuthenticationError(ERROR_MESSAGE);
    }

    const user = await this.getUser(userId);

    return user;
  }

  @Trace()
  public verifyAuth0Token(token: string): Promise<JwtPayload> {
    return new Promise((resolve, reject) => {
      const audience = AUTH0_INTERNAL_AUDIENCE.split(",");

      const client = jwksClient({ jwksUri: `https://${AUTH0_INTERNAL_DOMAIN}/.well-known/jwks.json` });

      const getKey: GetPublicKeyOrSecret = (header, callback) => {
        client.getSigningKey(header.kid, (_err, key) => {
          const signingKey = key.getPublicKey();
          callback(null, signingKey);
        });
      };

      jwt.verify(token, getKey, { algorithms: ["RS256"], issuer: AUTH0_INTERNAL_ISSUER_URL }, (error, decoded) => {
        if (error || !decoded) {
          return reject(error);
        }

        return resolve(decoded);
      });
    });
  }

  @Trace()
  public async validateEfuseTokenAndGetUser(token: string): Promise<IUser> {
    const payload = jwt.verify(token, JWT_KEY, {
      algorithms: ["HS256"],
      issuer: EfuseAuthEnum.ISS,
      audience: EfuseAuthEnum.AUD,
      maxAge: JWT_MAX_AGE
    }) as JwtPayload;

    const { sub, jti } = payload;

    if (!sub || !jti) {
      this.logger?.info("Jwt sub or jti is undefined.");
      throw new AuthenticationError(ERROR_MESSAGE);
    }

    const session = await Session.findOne({ _id: jti, active: true }).lean();

    if (!session) {
      this.logger?.info("Session not found or session is revoked");
      throw new AuthenticationError(ERROR_MESSAGE);
    }

    const user = await this.getUser(sub);

    return user;
  }

  @Trace()
  public async getUser(userId: string): Promise<IUser> {
    const user: IUser | null = await User.findById(userId).cache(10).select(UserFields.JWT_USER_FIELDS);

    if (!user) {
      this.logger?.info("User not found!");
      throw new AuthenticationError(ERROR_MESSAGE);
    }

    if (user.status === UserStatus.BLOCKED) {
      this.logger?.info("User is Blocked");
      throw new AuthenticationError(ERROR_MESSAGE);
    }

    return user;
  }
}
