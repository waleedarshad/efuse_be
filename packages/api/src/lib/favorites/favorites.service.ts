import { Failure } from "@efuse/contracts";
import { Types, PaginateResult } from "mongoose";
import { IFavorite } from "../../backend/interfaces/favorite";
import { Favorite, FavoriteModel } from "../../backend/models/favorite";

import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

type ObjectId = Types.ObjectId | string;

@Service("favorites.service")
export class FavoritesService extends BaseModelService<IFavorite> {
  private readonly favorites: FavoriteModel<IFavorite> = Favorite;
  private readonly failure = Failure;

  constructor() {
    super(Favorite);
  }

  // need to do more checks for create than what is done in baseModelService
  @Trace()
  public async create(item: IFavorite | Partial<IFavorite>): Promise<IFavorite> {
    try {
      this.checkForRelatedDocError(<ObjectId>item.relatedDoc);
      this.checkForOwnerError(<ObjectId>item.owner);
      this.checkForIDError(<ObjectId>item.creator, "creator ID is malformed");

      const result: IFavorite = await this.favorites.create(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async getPaginatedFavorites(owner: ObjectId, page = 1, limit = 10): Promise<PaginateResult<IFavorite>> {
    this.checkForOwnerError(owner);

    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    if (limitParam > 30) {
      limitParam = 30;
    }

    const notes = await this.favorites.paginate(
      { owner },
      {
        page: pageParam,
        limit: limitParam,
        sort: { createdAt: -1 },
        lean: true
      }
    );

    return notes;
  }

  @Trace()
  private checkForRelatedDocError(relatedDoc: Types.ObjectId | string): void {
    this.checkForIDError(relatedDoc, "relatedDoc ID is malformed");
  }

  @Trace()
  private checkForOwnerError(owner: Types.ObjectId | string): void {
    this.checkForIDError(owner, "owner ID is malformed");
  }

  @Trace()
  private checkForIDError(objectId: Types.ObjectId | string, errorMessage: string): void {
    if (!this.isValidObjectId(objectId)) {
      throw this.failure.BadRequest(errorMessage);
    }
  }
}
