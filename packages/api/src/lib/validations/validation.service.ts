import { isEmpty } from "lodash";
import { Failure } from "@efuse/contracts";
import { SlugTypeEnum } from "@efuse/entities";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { OpportunityBaseService } from "../opportunities/opportunity-base.service";
import { OrganizationBaseService } from "../organizations/organization-base.service";
import { LearningArticleBaseService } from "../learningArticles/learning-article-base.service";
import { ERenaTournamentService } from "../erena/erena-tournament.service";
import { IValidationResponse } from "../../backend/interfaces";

const SLUG_ALREADY_TAKEN = "This URL already exists. Please use a different URL.";

@Service("validation.service")
export class ValidationService extends BaseService {
  private $opportunityBaseService: OpportunityBaseService;
  private $organizationBaseService: OrganizationBaseService;
  private $learningArticleBaseService: LearningArticleBaseService;
  private $erenaTournamentService: ERenaTournamentService;

  constructor() {
    super();

    this.$opportunityBaseService = new OpportunityBaseService();
    this.$organizationBaseService = new OrganizationBaseService();
    this.$learningArticleBaseService = new LearningArticleBaseService();
    this.$erenaTournamentService = new ERenaTournamentService();
  }

  /**
   * @summary Validate slug for the given slugType
   *
   * @param {string} slug
   * @param {string} slugType
   *
   * @returns {Promise<IValidationResponse>}
   */
  @Trace()
  public async validateSlug(slug: string, slugType: string): Promise<IValidationResponse> {
    let error = "";

    // Make sure slugType is provided
    if (!slugType) {
      throw Failure.BadRequest("slugType is required");
    }

    // Make sure slug is provided
    if (!slug) {
      error = "Slug is required";
    }

    // Make sure slug format is valid
    const regex = /^[\dA-Za-z-]{1,63}$/;

    if (!regex.test(slug)) {
      error = "Invalid characters. Only letters, numbers and hyphens are allowed.";
    } else {
      // Make sure slug is unique
      switch (slugType) {
        case SlugTypeEnum.OPPORTUNITY:
          // Validate Slug presence for Opportunity
          if (await this.$opportunityBaseService.findOne({ shortName: slug }, { _id: 1 }, { lean: true })) {
            error = SLUG_ALREADY_TAKEN;
          }

          break;

        case SlugTypeEnum.ORGANIZATION:
          // Validate Slug presence for Organization
          if (await this.$organizationBaseService.findOne({ shortName: slug }, { _id: 1 }, { lean: true })) {
            error = SLUG_ALREADY_TAKEN;
          }

          break;

        case SlugTypeEnum.LEARNING_ARTICLE:
          // Validate Slug presence for Learning Articles
          if (await this.$learningArticleBaseService.findOne({ slug }, { _id: 1 }, { lean: true })) {
            error = SLUG_ALREADY_TAKEN;
          }

          break;

        case SlugTypeEnum.ERENA_TOURNAMENT:
          // Validate Slug presence for ERenaTournament
          if (await this.$erenaTournamentService.findOne({ slug }, { _id: 1 }, { lean: true })) {
            error = SLUG_ALREADY_TAKEN;
          }

          break;

        default:
          throw Failure.NotImplemented("Unhandled scenario");
      }
    }

    return { error, valid: isEmpty(error) };
  }
}
