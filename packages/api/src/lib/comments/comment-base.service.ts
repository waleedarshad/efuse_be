import {
  FilterQuery,
  PaginateOptions,
  PaginateResult,
  CreateQuery,
  UpdateQuery,
  LeanDocument,
  QueryOptions
} from "mongoose";
import { Failure } from "@efuse/contracts";

import { IComment } from "../../backend/interfaces/comment";
import { CommentModel, Comment } from "../../backend/models/comment";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ObjectId, DeleteResult } from "../../backend/types";

@Service("comment-base.service")
export class CommentBaseService extends BaseService {
  // todo: investigate & fix
  // @ts-ignore
  protected $comments: CommentModel<IComment> = Comment;

  /**
   * @summary Get paginated comments
   *
   * @param {FilterQuery<IComment>} filter
   * @param {PaginateOptions} options
   *
   * @returns {Promise<PaginateResult<IComment>>}
   */
  @Trace()
  public async getPaginatedComments(
    filter: FilterQuery<IComment>,
    options: PaginateOptions
  ): Promise<PaginateResult<IComment>> {
    const comments = await this.$comments.paginate(filter, options);

    return comments;
  }

  /**
   * @summary Create a new comment
   *
   * @param {Partial<IComment>} body
   *
   * @returns {Promise<IComment>}
   */
  @Trace()
  public async create(body: Partial<IComment>): Promise<IComment> {
    const doc = <CreateQuery<IComment>>body;

    const comment = await this.$comments.create(doc);

    comment.depopulate("user");

    return comment;
  }

  /**
   * @summary Update a particular comment
   *
   * @param {UpdateQuery<IComment>} filter
   * @param {FilterQuery<IComment>} body
   *
   * @returns {Promise<IComment>}
   */
  @Trace()
  public async update(filter: FilterQuery<IComment>, body: UpdateQuery<IComment>): Promise<LeanDocument<IComment>> {
    const comment = await this.$comments.findOneAndUpdate(filter, body, { new: true, autopopulate: false }).lean();

    if (!comment) {
      throw Failure.UnprocessableEntity("Comment not found");
    }

    return comment;
  }

  public async findOne(filter: FilterQuery<IComment>): Promise<IComment | null>;
  public async findOne(filter: FilterQuery<IComment>, projection: any): Promise<IComment | null>;
  public async findOne(
    filter: FilterQuery<IComment>,
    projection: any,
    options: QueryOptions | null
  ): Promise<IComment | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IComment>,
    projection?: any,
    options?: QueryOptions | null
  ): Promise<IComment | null> {
    const comment = await this.$comments.findOne(filter, projection, options);
    return comment;
  }

  public async find(filter: FilterQuery<IComment>): Promise<IComment[]>;
  public async find(filter: FilterQuery<IComment>, projection: any): Promise<IComment[]>;
  public async find(filter: FilterQuery<IComment>, projection: any, options: QueryOptions): Promise<IComment[]>;
  @Trace()
  public async find(filter: FilterQuery<IComment>, projection?: any, options?: QueryOptions): Promise<IComment[]> {
    const comment = await this.$comments.find(filter, projection, options);
    return comment;
  }

  /**
   * @summary Finds the given document, deletes it, and returns the document as it was before deletion
   *
   * @param {FilterQuery<IComment>} filter
   * @param {QueryOptions | null} options
   *
   * @returns {Promise<IComment | null>}
   */
  @Trace()
  public async findOneAndDelete(
    filter: FilterQuery<IComment>,
    options?: QueryOptions | null
  ): Promise<IComment | null> {
    const comment = await this.$comments.findOneAndDelete(filter, options);

    return comment;
  }

  @Trace()
  public async performSoftDelete(userId: ObjectId): Promise<DeleteResult> {
    const result: DeleteResult = await this.$comments.delete({ user: userId });

    return result;
  }

  @Trace()
  public async performSoftUndelete(userId: ObjectId): Promise<DeleteResult> {
    const result: DeleteResult = await this.$comments.restore({ user: userId });

    return result;
  }
}
