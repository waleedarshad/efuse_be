export enum CommentTypeEnum {
  FEED = "feeds"
}

export enum CommentPaginationLimitEnum {
  DEFAULT = 5,
  MAX = 15
}

export enum ThreadPaginationLimitEnum {
  DEFAULT = 2,
  MAX = 15
}
