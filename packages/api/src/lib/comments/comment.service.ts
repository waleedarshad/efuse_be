import { Failure } from "@efuse/contracts";
import { isEmpty } from "lodash";
import { Types, LeanDocument } from "mongoose";
import sanitizeHtml from "sanitize-html";
import { StreakName } from "@efuse/entities";

import { IComment, ICommentBody, IUser, TokenLedgerAction, TokenLedgerKind } from "../../backend/interfaces";
import { IFeed } from "../../backend/interfaces/feed";
import { IHomeFeed } from "../../backend/interfaces/home-feed";
import { IMedia } from "../../backend/interfaces/media";
import { Service, Trace } from "../decorators";
import { FeedService } from "../feeds/feed.service";
import { CommentBaseService } from "./comment-base.service";
import { BlockedUsersService } from "../blocked-users/blocked-users.service";
import { GiphyService } from "../giphy/giphy.service";
import { CommentTypeEnum } from "./comment.enum";
import { MediaService } from "../media/media.service";
import { MediaTypeEnum } from "../media/media.enum";
import { HomeFeedService } from "../home-feed/home-feed.service";
import { BrazeService } from "../braze/braze.service";
import { BrazeCampaignName } from "../braze/braze.enum";
import { CommentHooksService } from "../hooks/comment-hooks.service";
import { StreaksService } from "../streaks/streaks.service";
import { TokenLedgerService } from "../token-ledger";
import { Giphy } from "../../backend/models/giphy/giphy.model";
import { Roles } from "../../lib/users/roles";
import { ObjectId } from "../../backend/types";

@Service("comment.service")
export class CommentService extends CommentBaseService {
  private $feedService: FeedService;
  private $blockedUsersService: BlockedUsersService;
  private $giphyService: GiphyService;
  private $mediaService: MediaService;
  private $homeFeedService: HomeFeedService;
  private $brazeService: BrazeService;
  private $streakService: StreaksService;
  private $tokenLedgerService: TokenLedgerService;
  private $commentHooksService: CommentHooksService;

  constructor() {
    super();

    this.$feedService = new FeedService();
    this.$blockedUsersService = new BlockedUsersService();
    this.$giphyService = new GiphyService();
    this.$mediaService = new MediaService();
    this.$homeFeedService = new HomeFeedService();
    this.$brazeService = new BrazeService();
    this.$streakService = new StreaksService();
    this.$tokenLedgerService = new TokenLedgerService();
    this.$commentHooksService = new CommentHooksService();
  }

  /**
   * @summary Create a new feed comment
   *
   * @param {IUser} user
   * @param {ICommentBody} body
   *
   * @returns {Promise<IComment>}
   */
  @Trace()
  public async createFeedComment(user: IUser, body: ICommentBody): Promise<IComment> {
    const userId: Types.ObjectId | string = user._id;

    // Make sure body is present
    this.validateBody(body);

    // Make sure platform is present
    if (!body.platform) {
      throw Failure.BadRequest("platform is missing");
    }

    // Make sure feedId is present
    if (!body.feedId) {
      throw Failure.BadRequest("feedId is missing");
    }

    // Make sure feedId is valid objectID
    if (!this.isValidObjectId(body.feedId)) {
      throw Failure.BadRequest("Invalid feedId");
    }

    // Make sure feed exists
    const feed = await this.$feedService.findOne({ _id: body.feedId }, {}, { autopopulate: false, lean: true });

    if (!feed) {
      throw Failure.UnprocessableEntity("Unable to find post to add comment.");
    }

    // don't allow comment if the posting user is blocked by the post's creator
    const isUserBlocked = await this.$blockedUsersService.isUserBlocked(String(userId), String(feed.user));

    if (isUserBlocked) {
      this.logger?.info({ user: userId, feed }, "User attempted to comment on a user's post that blocked them.");

      throw Failure.BadRequest("Unable to comment on post.");
    }

    // Sanitize content
    const sanitizedContent: string | undefined = this.sanitizedContent(body.content);

    const giphyId: Types.ObjectId | string | undefined = await this.storeGiphy(body.giphy);

    // Create new comment
    const comment = await this.create({
      user: userId,
      content: sanitizedContent,
      commentable: body.feedId,
      commentableType: CommentTypeEnum.FEED,
      mentions: body.mentions,
      parent: body.parent,
      platform: body.platform,
      giphy: giphyId
    });

    // Create media
    if (body.file && body.file.url) {
      await this.$mediaService.create(<IMedia>{
        file: body.file,
        mediaable: comment._id as string,
        mediaableType: MediaTypeEnum.COMMENT,
        user: userId
      });
    }

    // Increment comments count
    await this.$feedService.update({ _id: body.feedId }, { $inc: { comments: 1 } }, { new: true });

    // Dispatch notifications
    await this.dispatchNotification(user, comment, feed);

    // Hype Score
    await this.$commentHooksService.newPostComment(comment);

    // Update streak
    await this.$streakService.checkStreak(String(user._id), StreakName.DAILY_COMMENT);
    await this.$streakService.checkStreak(String(user._id), StreakName.DAILY_USAGE, "Post Comment");

    // credit user for commenting
    await this.$tokenLedgerService.createEntry(TokenLedgerAction.comment, TokenLedgerKind.credit, user._id);

    return comment;
  }

  /**
   * @summary Dispatch all notifications on the basis of the comment
   *
   * @param {IUser} user
   * @param {IComment} comment
   * @param {IFeed} feed
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async dispatchNotification(user: IUser, comment: IComment, feed: IFeed): Promise<void> {
    const homeFeed: IHomeFeed | null = await this.$homeFeedService.findOne(
      { feed: String(feed._id) },
      {},
      { lean: true }
    );

    if (!homeFeed) {
      throw Failure.UnprocessableEntity("Home feed not found");
    }

    const { mentions } = comment;

    if (mentions && mentions.length > 0) {
      // sender is the person that is commenting
      const senderId = user._id;

      // send one notification for each mention in comment. Do not send if it's an org, or if you're mentioning yourself.
      mentions.forEach(async (mention) => {
        // receiver is the person being mentioned
        const receiverId = mention.id;
        if (mention.type !== "org" && String(senderId) !== String(receiverId) && homeFeed) {
          this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.COMMENT_MENTION, {
            trigger_properties: {
              sender_user_id: senderId,
              sender_username: user.username,
              sender_name: user.name,
              comment_id: String(comment._id),
              feed_id: String(homeFeed._id)
            },
            recipients: [{ external_user_id: String(receiverId) }]
          });
        }
      });
    }

    // check if this was a reply to another comment
    if (comment.parent) {
      const parentComment = await this.findOne({ _id: comment.parent }, {}, { lean: true });

      // check if parent comment exists
      if (parentComment && String(user._id) !== String(parentComment.user)) {
        // notify the comment author (not the post author)
        this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.COMMENT_REPLY, {
          trigger_properties: {
            commentreply_username: user.username,
            commentreply_user_id: String(user._id),
            feed_id: String(homeFeed._id),
            comment_id: String(comment._id)
          },
          recipients: [{ external_user_id: String(parentComment.user) }]
        });
      }
    }

    // prevent all notifications below if you're commenting on your own post
    if (String(user._id) === String(feed.user)) {
      this.logger?.info(
        "Skipping notification as sender and receiver are the same. User is commenting on their own post. No notificaiton needed."
      );
      return;
    }

    // Normal comment notification sent to post author
    this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.COMMENT, {
      trigger_properties: {
        commented_username: user.username,
        commented_user_id: String(user._id),
        feed_id: String(homeFeed._id),
        comment_id: String(comment._id)
      },
      recipients: [{ external_user_id: String(feed.user) }]
    });

    // currently disabled in braze
    // [user name] has commented on a post you've hyped!
    // this is NOT the notification for hyping a comment
    // todo: need to move the hypedPostCommentNotification() logic into comment or hype service. We plan to remove /lib/notifications/dispatcher.js
    this.$brazeService.sendCampaignMessageAsync(BrazeCampaignName.COMMENT_ON_POST_YOU_HYPED, {
      trigger_properties: {
        comment_username: user.username,
        comment_user_id: user._id,
        feed_id: String(homeFeed._id),
        comment_id: String(comment._id)
      },
      recipients: [{ external_user_id: String(feed.user) }]
    });
  }

  /**
   * @summary Update a particular feed comment
   *
   * @param {Types.ObjectId | string} commentId
   * @param {Types.ObjectId | string} userId
   * @param {Partial<ICommentBody>} body
   *
   * @returns {Promise<LeanDocument<IComment>>}
   */
  @Trace()
  public async updateFeedComment(
    commentId: Types.ObjectId | string,
    userId: Types.ObjectId | string,
    body: Partial<ICommentBody>
  ): Promise<LeanDocument<IComment>> {
    // Make sure commentId is valid
    if (!this.isValidObjectId(commentId)) {
      throw Failure.BadRequest("Invalid commentId");
    }

    // Make sure required params are passed
    this.validateBody(body);

    // Sanitize content
    const sanitizedContent: string | undefined = this.sanitizedContent(body.content);

    const giphyId: Types.ObjectId | string | undefined = await this.storeGiphy(body.giphy);

    const comment = await this.update(
      { user: userId, _id: commentId, commentableType: CommentTypeEnum.FEED },
      { $set: { content: sanitizedContent, mentions: body.mentions, giphy: giphyId } }
    );

    // Handle media edit
    if (body.file && body.file.url) {
      const query = { mediaable: commentId, user: userId, mediaableType: MediaTypeEnum.COMMENT };

      // Fetch media to be deleted
      const mediaObjects = await this.$mediaService.find(query);

      // Build S3 fileKeys
      const fileKeys = this.$mediaService.buildFileKeys(mediaObjects);

      // Delete all associated media
      await this.$mediaService.delete(query, fileKeys);

      // Upload new media
      await this.$mediaService.create(<IMedia>{
        file: body.file,
        mediaable: comment._id as string,
        mediaableType: MediaTypeEnum.COMMENT,
        user: userId
      });
    }

    return comment;
  }

  /**
   * @summary Delete a particular comment & it's child
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} commentId
   *
   * @returns {Promise<IComment>}
   */
  @Trace()
  public async deleteComment(user: IUser, commentId: ObjectId): Promise<IComment> {
    // Make sure commentId is valid
    if (!this.isValidObjectId(commentId)) {
      throw Failure.BadRequest("Invalid commentId");
    }

    // Delete the comment
    const deletedComment = await this.findOneAndDelete({ _id: commentId }, { lean: true, autopopulate: false });

    // Make sure comment exists
    if (!deletedComment) {
      throw Failure.UnprocessableEntity("Comment not found");
    }

    // Delete all the child comments
    const { deletedCount } = await this.$comments.deleteMany({ parent: commentId });

    // Build count to be decremented
    const totalCommentsDeleted = (deletedCount ? deletedCount + 1 : 1) * -1;

    // Decrement Feed Comments count
    await this.$feedService.update({ _id: deletedComment.commentable }, { $inc: { comments: totalCommentsDeleted } });

    return deletedComment;
  }

  /**
   * @summary Store Giphy in DB & return giphy objectId
   *
   * @param {Giphy} giphyBody
   *
   * @returns {Promise<Types.ObjectId | string | undefined>}
   */
  @Trace()
  private async storeGiphy(giphyBody: Giphy | undefined): Promise<Types.ObjectId | string | undefined> {
    let giphyId: Types.ObjectId | string | undefined;

    // Add Giphy
    if (giphyBody) {
      const giphy = await this.$giphyService.createOrUpdate(giphyBody);

      giphyId = giphy._id as string;
    }

    return giphyId;
  }

  /**
   * @summary Validate body params to make sure all required params are passed
   *
   * @param {Partial<ICommentBody>} body
   */
  private validateBody(body: Partial<ICommentBody>): void {
    // Make sure body is present
    if (isEmpty(body) || (isEmpty(body.content) && isEmpty(body.giphy) && isEmpty(body.file))) {
      throw Failure.BadRequest("body is missing");
    }
  }

  /**
   * @summary Sanitize content to strip tags
   *
   * @param {string | undefined} content
   *
   * @returns {string  undefined}
   */
  private sanitizedContent(content: string | undefined): string | undefined {
    let sanitizedContent: string | undefined;

    if (content) {
      sanitizedContent = sanitizeHtml(content, { allowedTags: [], allowedAttributes: {} }) as string;
    }

    return sanitizedContent;
  }

  /**
   * @summary Validate Comment
   *
   * @param {Types.ObjectId | string} commentId
   *
   * @returns {Promise<IComment>}
   */
  @Trace()
  public async validateComment(commentId: ObjectId): Promise<IComment> {
    // Make sure commentID is valid
    if (!this.isValidObjectId(commentId)) {
      throw Failure.BadRequest("Invalid commentId");
    }

    // Find out comment
    const comment = await this.findOne({ _id: commentId }, {}, { autopopulate: false, lean: true });

    // Make sure comment exists
    if (!comment) {
      throw Failure.UnprocessableEntity("Comment not found");
    }

    return comment;
  }

  /**
   * @summary Validate Comment
   *
   * @param {Types.ObjectId | string} commentId
   * * @param {Types.ObjectId | string} user
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async validatePermission(user: IUser, commentId: ObjectId): Promise<void> {
    const userId = user._id;
    const commentToBeDeleted = await this.validateComment(commentId);

    // Find the feed
    const feed = await this.$feedService.findOne(
      { _id: commentToBeDeleted.commentable },
      {},
      { lean: true, autopopulate: false }
    );

    // Make sure feed exists
    if (!feed) {
      throw Failure.BadRequest("Post not found");
    }

    // Make sure user is either owner of feed or comment
    if (
      String(feed.user) !== String(userId) &&
      String(commentToBeDeleted.user) !== String(userId) &&
      !user.roles.includes(Roles.ADMIN)
    ) {
      throw Failure.Forbidden("You are not allowed to delete this comment");
    }
  }
}
