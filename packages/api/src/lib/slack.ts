import { WebClient } from "@slack/web-api";
import { BaseService } from "./base/base.service";

import { Service, Trace } from "./decorators";

const TOKEN = "xoxp-552912212464-741147022181-1080183057140-8159e6e629ed2c57c4f1cc044faf45f9";

@Service("slack")
export class Slack extends BaseService {
  /**
   * @summary Send message to slack channel
   *
   * @param {String} message - Message text to send to slack
   * @param {String} channel - Name of channel to send message to
   */
  @Trace()
  public static async sendMessage(message: string, channel: string): Promise<void> {
    const client = new WebClient(TOKEN);

    await client.chat.postMessage({ channel, text: message });
  }
}
