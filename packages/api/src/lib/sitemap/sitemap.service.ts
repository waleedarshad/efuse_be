import { DOMAIN } from "@efuse/key-store";
import { concat } from "lodash";
import { SitemapStream, streamToPromise } from "sitemap";

import { BaseService } from "../base/base.service";
import { LearningArticleService } from "../learningArticles/learning-article.service";
import { getVisibleOpportunityUrls } from "../opportunities";
import { ISitemapUrl } from "../../backend/interfaces/sitemap-url";
import { S3ContentTypes, S3Service } from "../aws/s3.service";
import { Service, Trace } from "../decorators";

const S3_FILE_KEY = "sitemap/sitemap.xml";

@Service("sitemap.service")
export class SitemapService extends BaseService {
  private $s3Service: S3Service;
  private $learningArticleService: LearningArticleService;

  constructor() {
    super();

    this.$s3Service = new S3Service();
    this.$learningArticleService = new LearningArticleService();
  }

  /**
   * @summary Get Article URLs used in other methods
   *
   * @return {Promise<ISitemapUrl[]>} an array of sitemap urls to be added to the sitemap
   */
  @Trace()
  private async getUrls(): Promise<ISitemapUrl[]> {
    const staticUrls = [
      { url: "/", changefreq: "monthly", priority: 0.5 },
      { url: "/signup", changefreq: "monthly", priority: 0.5 },
      { url: "/login", changefreq: "monthly", priority: 0.5 },
      { url: "/news", changefreq: "monthly", priority: 0.5 }
    ];

    // ADD Dynamic urls here
    const articleUrls = <ISitemapUrl[]>await this.$learningArticleService.getArticleUrls();
    const opportunityUrls = <ISitemapUrl[]>await getVisibleOpportunityUrls();

    const urls = concat(staticUrls, articleUrls, opportunityUrls);

    return urls;
  }

  /**
   * @summary Generate sitemap
   *
   * @return {Promise<string>}
   */
  @Trace()
  private async generateSitemap(): Promise<string> {
    try {
      const smStream = new SitemapStream({ hostname: DOMAIN });
      const arrayOfSitemapItems = await this.getUrls();

      arrayOfSitemapItems.forEach((item) => smStream.write(item));

      const data = streamToPromise(smStream).then((stream) => stream.toString());
      smStream.end();

      return data;
    } catch (error) {
      throw this.handleError(error, "Unable to generate sitemap.");
    }
  }

  /**
   * @summary Set sitemap
   *
   * @return {Promise<string>} the updated sitemap
   */
  @Trace()
  public async setSitemap(): Promise<void> {
    try {
      const sitemap = await this.generateSitemap();

      await this.$s3Service.uploadMediaToS3(sitemap, S3_FILE_KEY, S3ContentTypes.XML);
    } catch (error) {
      throw this.handleError(error, "Unable to upload sitemap to S3");
    }
  }

  /**
   * @summary Cron to set sitemap
   *
   * @return {Promise<string>} the updated sitemap
   */
  @Trace({ name: "SyncSitemapCron", service: "sitemap.service" })
  public static async SyncSitemapCron(): Promise<void> {
    const sitemapService = new SitemapService();
    await sitemapService.setSitemap();
  }
}
