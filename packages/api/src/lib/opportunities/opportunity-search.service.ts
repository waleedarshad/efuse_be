import { LeanDocument } from "mongoose";
import { OwnerKind } from "@efuse/entities";
import { Queue } from "bull";
import moment from "moment";

import { AlgoliaIndicesEnum } from "../algolia/algolia.enum";
import { AlgoliaService } from "../algolia/algolia.service";
import { Service, Trace } from "../decorators";
import { OpportunityBaseService } from "./opportunity-base.service";
import { IAlgoliaFormattedOwner, IOpportunity, IPopulateOpportunityOwner } from "../../backend/interfaces";
import Config from "../../async/config";
import { UserService } from "../users/user.service";
import { OrganizationBaseService } from "../organizations/organization-base.service";
import { UserFields } from "../users/user-fields";
import { OpportunityStatusEnum } from "./opportunity-status.enum";
import { ObjectId } from "../../backend/types";

const SERVICE = "opportunity-search.service";

@Service(SERVICE)
export class OpportunitySearchService extends OpportunityBaseService {
  public $index = `${process.env.NODE_ENV}_${AlgoliaIndicesEnum.OPPORTUNITIES}`;

  private $syncOpportunitiesWithAlgoliaQueue: Queue;

  private $algoliaService: AlgoliaService;
  private $userService: UserService;
  private $organizationBaseService: OrganizationBaseService;

  constructor() {
    super();

    this.$syncOpportunitiesWithAlgoliaQueue = Config.initQueue("syncOpportunitiesWithAlgolia");

    this.$algoliaService = new AlgoliaService();
    this.$userService = new UserService();
    this.$organizationBaseService = new OrganizationBaseService();
  }

  /**
   * @summary Search Opportunities using AlgoliaSearch
   *
   * @param {string} query
   * @param {number} limit
   * @param {string} userId
   *
   * @returns {Promise<IOpportunity[]>}
   */
  @Trace()
  public async search(query: string, limit: number, userId?: string): Promise<IOpportunity[]> {
    const results = await this.$algoliaService.search<IPopulateOpportunityOwner>(this.$index, query, limit, userId);

    // Make sure to depopulate author so that it can be resolved by graphql
    const opportunities: IOpportunity[] = results.map(
      (result: IPopulateOpportunityOwner) =>
        <IOpportunity>{
          ...result,
          user: result.user?._id || "",
          organization: (result.organization?._id as string) || ""
        }
    );

    return opportunities;
  }

  /**
   * @summary Populate owner & sync with algolia
   *
   * @param {LeanDocument<IOpportunity>} opportunity
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncWithAlgolia(opportunity: LeanDocument<IOpportunity>): Promise<void> {
    try {
      // Remove opportunity from algolia if it is not visible & open
      if (opportunity.status !== OpportunityStatusEnum.VISIBLE) {
        this.logger?.info({ _id: opportunity._id }, "Removing opportunity from algolia");

        await this.deleteFromAlgolia(opportunity._id);

        // Short circuit
        return;
      }

      // Fetch the user
      const user = await this.$userService.findOne(
        { _id: opportunity.user },
        UserFields.ALGOLIA_FIELDS_TO_BE_SELECTED,
        { lean: true }
      );

      // Fetch the organization
      const organization = await this.$organizationBaseService.findOne(
        { _id: opportunity.organization },
        {},
        { lean: true }
      );

      // Fetch owner
      const ownerType = organization ? OwnerKind.ORGANIZATION : OwnerKind.USER;
      const owner = ownerType === OwnerKind.ORGANIZATION ? organization : user;

      // Format author object
      const formattedOwner: IAlgoliaFormattedOwner | null = this.$algoliaService.formatOwner(owner, ownerType);

      // Build object with formatted author
      const opportunityToBeSynced: Record<string, unknown> = {
        ...opportunity,
        user: user ? this.$algoliaService.buildUserObject(user) : null,
        organization,
        author: formattedOwner || {}
      };

      // Converting Dates for unix for algolia sorting
      if (opportunity.dueDate) {
        opportunityToBeSynced.dueDateUnix = moment(opportunity.dueDate).unix();
      }

      if (opportunity.startDate) {
        opportunityToBeSynced.startDateUnix = moment(opportunity.startDate).unix();
      }

      // Sync with algolia
      await this.$algoliaService.syncObject(this.$index, opportunityToBeSynced);

      this.logger?.info({ _id: opportunity._id }, "Synced opportunity with algolia");
    } catch (error) {
      this.logger?.error(error, "Error while syncing Opportunity with algolia");
    }
  }

  /**
   * @summary Populate owner & sync with algolia using bull queue
   *
   * @param {LeanDocument<IOpportunity>} opportunity
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncWithAlgoliaAsync(opportunity: LeanDocument<IOpportunity>): void {
    this.$syncOpportunitiesWithAlgoliaQueue
      .add({ opportunity })
      .catch((error) => this.logger?.error(error, "Error while adding to queue syncOpportunitiesWithAlgoliaQueue"));
  }

  /**
   * @summary Remove from algolia
   *
   * @param {ObjectId} opportunityId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async deleteFromAlgolia(opportunityId: ObjectId): Promise<void> {
    this.logger?.info({ opportunityId }, "Removing opportunity from algolia");

    await this.$algoliaService.deleteObject(this.$index, <string>opportunityId);
  }

  /**
   * @summary Remove from algolia asynchronously using bull queue
   *
   * @param {ObjectId} opportunityId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public deleteFromAlgoliaAsync(opportunityId: ObjectId): void {
    this.logger?.info({ opportunityId }, "Removing opportunity from algolia");

    this.$algoliaService.deleteObjectAsync(this.$index, <string>opportunityId);
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const opportunitySearchService = new OpportunitySearchService();

    // syncOpportunitiesWithAlgoliaQueue
    opportunitySearchService.$syncOpportunitiesWithAlgoliaQueue
      .process((job: { data: { opportunity: LeanDocument<IOpportunity> } }) => {
        return opportunitySearchService.syncWithAlgolia(job.data.opportunity);
      })
      .catch((error) =>
        opportunitySearchService.logger?.error(error, "Error while processing syncOpportunitiesWithAlgoliaQueue")
      );
  }
}
