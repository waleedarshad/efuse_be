import { FilterQuery, QueryOptions, Types } from "mongoose";
import { Failure } from "@efuse/contracts";

import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { Opportunity, OpportunityModel } from "../../backend/models/Opportunity";
import { IOpportunity } from "../../backend/interfaces/opportunity";

@Service("opportunity-base.service")
export class OpportunityBaseService extends BaseService {
  protected readonly opportunities: OpportunityModel<IOpportunity> = Opportunity;

  public async findOne(filter: FilterQuery<IOpportunity>): Promise<IOpportunity | null>;
  public async findOne(filter: FilterQuery<IOpportunity>, projection: any): Promise<IOpportunity | null>;
  public async findOne(
    filter: FilterQuery<IOpportunity>,
    projection: any,
    options: QueryOptions
  ): Promise<IOpportunity | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IOpportunity>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunity | null> {
    const opportunity = await this.opportunities.findOne(filter, projection, options);
    return opportunity;
  }

  public async find(filter: FilterQuery<IOpportunity>): Promise<IOpportunity[]>;
  public async find(filter: FilterQuery<IOpportunity>, projection?: any): Promise<IOpportunity[]>;
  public async find(
    filter: FilterQuery<IOpportunity>,
    projection: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunity[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IOpportunity>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunity[]> {
    const opportunities = await this.opportunities.find(filter, projection, options);
    return opportunities;
  }

  /**
   * @summary Update a particular opportunity
   *
   * @param {Types.ObjectId | string} opportunityId
   * @param {Partial<IOpportunity>} opportunityBody
   *
   * @returns {Promise<IOpportunity>}
   */
  @Trace()
  public async update(
    opportunityId: Types.ObjectId | string,
    opportunityBody: Partial<IOpportunity>
  ): Promise<IOpportunity> {
    // Make sure ID is present
    if (!opportunityId) {
      throw Failure.BadRequest("opportunity ID is not found");
    }

    // Make sure ID is valid
    if (!Types.ObjectId.isValid(opportunityId)) {
      throw Failure.BadRequest("invalid opportunity ID");
    }

    // Make sure params are passed
    if (!opportunityBody) {
      throw Failure.BadRequest("params are not found");
    }

    /**
     * Make sure to only update shortName when shortName is passed. The opportunityBody type is Partial. So there is every chance of shortName not being part of body.
     * In that case it will try to set it to null, which will eventually throw a MongoError: E11000 duplicate key error
     */
    if (opportunityBody.shortName) {
      opportunityBody.shortName = opportunityBody.shortName?.toLowerCase();
    }

    const updatedOpportunity: IOpportunity | null = await this.opportunities
      .findOneAndUpdate({ _id: opportunityId }, { ...opportunityBody }, { new: true })
      .lean();

    // Make sure opportunity exists
    if (!updatedOpportunity) {
      throw Failure.UnprocessableEntity("Requested opportunity doesn't exists");
    }

    return updatedOpportunity;
  }
}
