import { FilterQuery, QueryOptions, Types } from "mongoose";
import { isEmpty } from "lodash";

import { IOpportunityRequirements } from "../../backend/interfaces";
import { OpportunityRequirement, OpportunityRequirementModel } from "../../backend/models/opportunity-requirement";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("opportunity-requirement-base.service")
export class OpportunityRequirementBaseService extends BaseService {
  protected $opportunityRequirements: OpportunityRequirementModel<IOpportunityRequirements> = OpportunityRequirement;

  public async find(filter: FilterQuery<IOpportunityRequirements>): Promise<IOpportunityRequirements[]>;
  public async find(
    filter: FilterQuery<IOpportunityRequirements>,
    projection?: any
  ): Promise<IOpportunityRequirements[]>;
  public async find(
    filter: FilterQuery<IOpportunityRequirements>,
    projection: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunityRequirements[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IOpportunityRequirements>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunityRequirements[]> {
    const opportunityRequirements = await this.$opportunityRequirements.find(filter, projection, options);
    return opportunityRequirements;
  }

  /**
   * @summary Get opportunity requirements by IDS
   *
   * @param {Array<Types.ObjectId | string>} ids
   *
   * @returns {Promise<IOpportunityRequirements[]>}
   */
  @Trace()
  public async findByIds(ids: Array<Types.ObjectId | string>): Promise<IOpportunityRequirements[]> {
    // Make sure we have ids to filter
    if (isEmpty(ids)) {
      return [];
    }

    // Making sure to avoid empty string
    const requirementIds = ids.filter((id) => this.isValidObjectId(id));

    const opportunityRequirements = await this.find(
      { _id: { $in: requirementIds }, isActive: true },
      {},
      { lean: true }
    );

    return opportunityRequirements;
  }
}
