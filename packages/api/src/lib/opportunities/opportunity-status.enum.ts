export enum OpportunityStatusEnum {
  HIDDEN = "hidden",
  VISIBLE = "visible"
}
