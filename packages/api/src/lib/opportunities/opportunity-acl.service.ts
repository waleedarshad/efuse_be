import { Service, Trace } from "../decorators";
import { Roles } from "../users/roles";
import { OpportunityBaseService } from "./opportunity-base.service";
import { IOpportunity, IUser } from "../../backend/interfaces";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";

@Service("opportunity-acl.service")
export class OpportunityACLService extends OpportunityBaseService {
  private $organizationACLService: OrganizationACLService;

  constructor() {
    super();

    this.$organizationACLService = new OrganizationACLService();
  }

  /**
   * @summary Verify Opportunity Ownership
   *
   * @param {IUser} currentUser
   * @param {IOpportunity} opportunity
   *
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async verifyOwnership(
    currentUser: IUser,
    opportunity: IOpportunity,
    organizationId: string
  ): Promise<boolean> {
    // Allow if user is eFuse admin
    if (currentUser.roles?.includes(Roles.ADMIN)) {
      this.logger!.info("User is eFuse admin | Access Granted");
      return true;
    }

    // If user is opportunity owner
    if (String(opportunity.user) === String(currentUser._id)) {
      this.logger!.info("User is opportunity Author | Access Granted");
      return true;
    }

    // If opportunity is created as an organization
    if (organizationId) {
      await this.$organizationACLService.authorizedToAccess(currentUser, organizationId, OrganizationAccessTypes.ANY);
    }

    return true;
  }
}
