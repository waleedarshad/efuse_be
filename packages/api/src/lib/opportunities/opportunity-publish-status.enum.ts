export enum OpportunityPublishStatusEnum {
  OPEN = "open",
  CLOSED = "closed"
}
