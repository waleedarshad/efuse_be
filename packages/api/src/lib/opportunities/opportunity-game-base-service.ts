import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions, Types } from "mongoose";

import { IOpportunityGame } from "../../backend/interfaces";
import { OpportunityGame, OpportunityGameModel } from "../../backend/models/opportunity-game";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("opportunity-game-base.service")
export class OpportunityGameBaseService extends BaseService {
  protected readonly $opportunityGames: OpportunityGameModel<IOpportunityGame> = OpportunityGame;

  /**
   * @summary Update a particular opportunity game object
   *
   * @param {FilterQuery<IOpportunityGame>} condition
   * @param {Partial<IOpportunityGame>} body
   * @param {QueryOptions | null | undefined} options
   *
   * @returns {Promise<IOpportunityGame>}
   */
  @Trace()
  public async update(
    condition: FilterQuery<IOpportunityGame>,
    body: Partial<IOpportunityGame>,
    options: QueryOptions | null | undefined
  ): Promise<IOpportunityGame> {
    if (!body) {
      throw Failure.BadRequest("body is missing");
    }

    // Update the corresponding object as per condition
    const opportunityGame: IOpportunityGame | null = await this.$opportunityGames.findOneAndUpdate(
      condition,
      { ...body },
      options
    );

    // Make sure object is present
    if (!opportunityGame) {
      throw Failure.UnprocessableEntity("OpportunityGame not found");
    }

    return opportunityGame;
  }

  /**
   * @summary Delete a particular opportunity game object
   *
   * @param {Types.ObjectId | string} id
   *
   * @returns {Promise<IOpportunityGame | null>}
   */
  @Trace()
  public async findByIdAndDelete(
    id: Types.ObjectId | string,
    options?: QueryOptions | null
  ): Promise<IOpportunityGame | null> {
    const item: IOpportunityGame | null = await this.$opportunityGames.findByIdAndRemove(id, options);

    return item;
  }

  public async findOne(condition: FilterQuery<IOpportunityGame>): Promise<IOpportunityGame | null>;
  public async findOne(
    condition: FilterQuery<IOpportunityGame>,
    projection: QueryOptions | null | undefined
  ): Promise<IOpportunityGame | null>;
  public async findOne(
    condition: FilterQuery<IOpportunityGame>,
    projection: unknown,
    options: QueryOptions | null | undefined
  ): Promise<IOpportunityGame | null>;
  @Trace()
  public async findOne(
    condition: FilterQuery<IOpportunityGame>,
    projection?: unknown,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunityGame | null> {
    const opportunityGame = await this.$opportunityGames.findOne(condition, projection, options);
    return opportunityGame;
  }

  /**
   * @summary Get a Count of number of documents
   *
   * @param {FilterQuery<IOpportunityGame>} filter
   *
   * @returns {Promise<number>}
   */
  public async countDocuments(filter?: FilterQuery<IOpportunityGame>): Promise<number> {
    let count = 0;

    count = await (filter ? this.$opportunityGames.countDocuments(filter) : this.$opportunityGames.countDocuments());

    return count;
  }
}
