import { isEmpty, round } from "lodash";
import { Types } from "mongoose";
import Bull from "bull";

import { BaseService } from "../base/base.service";
import { OpportunityService } from "./opportunity.service";
import { ApplicantBaseService } from "../applicants/applicant-base.service";

import { Service, Trace } from "../decorators";

import { ConstantsService } from "../constants";
import * as Config from "../../async/config";
import { IOpportunity } from "../../backend/interfaces/opportunity";
import { RankingUtil } from "../utils/ranking.util";

interface IWeight {
  boostWeight: number;
  verifiedWeight: number;
  promotedWeight: number;
  applicantWeight: number;
  gravity: number;
  timeBucket: number;
  internalWeight: number;
  externalWeight: number;
}

@Service("opportunity-rank.service")
export class OpportunityRankService extends BaseService {
  private opportunityService: OpportunityService;
  private applicantBaseService: ApplicantBaseService;
  private constantsService: ConstantsService;

  private setObjectRankQueue: Bull.Queue<any>;

  constructor() {
    super();

    this.opportunityService = new OpportunityService();
    this.applicantBaseService = new ApplicantBaseService();
    this.constantsService = new ConstantsService();

    this.setObjectRankQueue = Config.initQueue("setOpportunityRank");
  }

  private async rankFormula(
    boosted: boolean,
    verified: boolean,
    createdAt: Date,
    applicantsCount: number,
    external: boolean
  ): Promise<number> {
    const weights = (await this.constantsService.getVariables("OPPORTUNITY_RANK_ALGORITHM")) as IWeight;

    // Make sure weights are available
    if (isEmpty(weights)) {
      this.logger!.info("Constants are not found, Setting 0 as rank");
      return 0;
    }

    // boost weight
    const boostWeight = boosted ? weights.boostWeight : 0;

    // verified weight
    const verifiedWeight = verified ? weights.verifiedWeight : 0;

    // Setting internal eFuse opportunities & external opportunities to have a default weight
    const defaultWeight = external ? weights.externalWeight : weights.internalWeight;

    // Calculate rank
    const rankValue = round(
      (defaultWeight + (applicantsCount * weights.applicantWeight + boostWeight + verifiedWeight)) /
        (RankingUtil.getTimeConstraint(createdAt, weights.timeBucket) + 2) ** weights.gravity,
      2
    );
    return Number.isNaN(rankValue) ? 0 : rankValue;
  }

  /**
   * @summary Set rank of an opportunity
   *
   * @param {Types.ObjectId} opportunityId
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async calculateRank(opportunityIdOrObject: Types.ObjectId | string | IOpportunity): Promise<number> {
    let opportunity: IOpportunity | null = opportunityIdOrObject as IOpportunity;

    // Fetch the opportunity if ID is passed
    if (opportunityIdOrObject instanceof Types.ObjectId || typeof opportunityIdOrObject === "string") {
      opportunity = await this.opportunityService.findOne(
        { _id: opportunityIdOrObject },
        {
          _id: 1,
          "metadata.rank": 1,
          "metadata.boosted": 1,
          verified: 1,
          promoted: 1,
          createdAt: 1,
          publishStatus: 1,
          status: 1,
          external: 1
        }
      );
    }

    // Return 0 rank if opportunity is not found
    if (!opportunity) {
      this.logger!.info({ opportunityIdOrObject }, "Opportunity not found! Might be deleted");
      return 0;
    }

    // if opportunity is not open or not visible set rank to 0
    if (opportunity.publishStatus !== "open" || opportunity.status !== "visible") {
      return 0;
    }

    // Get the applicants count
    const applicantsCount = await this.applicantBaseService.countDocuments({ opportunity: opportunity._id as string });

    // Calculate rank
    const rankValue = await this.rankFormula(
      opportunity.metadata?.boosted,
      opportunity.verified.status,
      opportunity.createdAt as Date,
      applicantsCount,
      opportunity.external
    );

    return rankValue;
  }

  @Trace()
  public async setObjectRank(opportunityId: Types.ObjectId | string): Promise<void> {
    const rankValue = await this.calculateRank(opportunityId);
    await this.opportunityService.update(opportunityId, { "metadata.rank": rankValue });
  }

  /**
   * @summary Set rank of an opportunity using bull queue
   *
   * @param {Types.ObjectId} opportunityId
   *
   */
  @Trace()
  public setObjectRankAsync(opportunityId: Types.ObjectId | string): void {
    this.setObjectRankQueue
      .add({ opportunityId })
      .catch((error) => this.logger?.error(error, "Error adding job to queue setObjectRank"));
  }

  /**
   * @summary Async Queue processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "opportunity-rank.service" })
  public static InitAsyncProcessors(): void {
    const opportunityRankService = new OpportunityRankService();

    opportunityRankService.setObjectRankQueue
      .process((job: { data: { opportunityId: Types.ObjectId | string } }) => {
        const { opportunityId } = job.data;
        return opportunityRankService.setObjectRank(opportunityId);
      })
      .catch((error) => {
        opportunityRankService.logger?.error(error, "Error while processing queue setObjectRankQueue");
      });
  }

  /**
   * @summary Cron job to refresh opportunity rank
   */
  @Trace({ name: "RefreshOpportunityRankCron", service: "opportunity-rank.service" })
  public static async RefreshOpportunityRankCron(): Promise<void> {
    const opportunityRankService = new OpportunityRankService();

    // Rank all opportunities
    const opportunities: IOpportunity[] = await opportunityRankService.opportunityService.getAllOpenAndVisible(
      {},
      { _id: 1 },
      { sort: { createdAt: -1 } }
    );

    for (const opportunity of opportunities) {
      opportunityRankService.setObjectRankAsync(opportunity._id);
    }
  }
}
