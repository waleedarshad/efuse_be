import { Types } from "mongoose";

import { IOpportunityGame } from "../../backend/interfaces/opportunity/opportunity-game";
import { GameBaseService } from "../game/game-base.service";
import { Service, Trace } from "../decorators";
import { OpportunityGameBaseService } from "./opportunity-game-base-service";
import { IGame, IPopulatedOpportunityGame } from "../../backend/interfaces";

@Service("opportunity-game.service")
export class OpportunityGameService extends OpportunityGameBaseService {
  private $gameBaseService: GameBaseService;

  constructor() {
    super();

    this.$gameBaseService = new GameBaseService();
  }

  /**
   * @summary Associate game with opportunity
   *
   * @param {Types.ObjectId | string} opportunityId
   * @param {Types.ObjectId | string} gameId
   *
   * @returns {Promise<IOpportunityGame | null>}
   */
  @Trace()
  public async associateGame(
    opportunityId: Types.ObjectId | string,
    gameId: Types.ObjectId | string
  ): Promise<IOpportunityGame | null> {
    // Make sure gameId is valid & exists in our DB
    const gameParam = await this.$gameBaseService.validateGame(gameId);

    let opportunityGame: IOpportunityGame | null = null;

    // Update existing game or create new one
    if (gameParam) {
      opportunityGame = await this.update(
        { opportunity: opportunityId },
        { opportunity: opportunityId, game: gameParam, updatedAt: new Date() },
        { new: true, lean: true, setDefaultsOnInsert: true, upsert: true }
      );
    } else {
      //  Delete a particular opportunity game object if one exists
      opportunityGame = await this.findOne({ opportunity: opportunityId });

      if (!opportunityGame?._id) {
        return null;
      }

      await this.findByIdAndDelete(opportunityGame?._id);
    }

    return opportunityGame;
  }

  /**
   * @summary Get game associated with the opportunity
   *
   * @param {Types.ObjectId | string} opportunityId
   *
   * @returns {Promise<IGame | null>}
   */
  @Trace()
  public async getAssociatedGame(opportunityId: Types.ObjectId | string): Promise<IGame | null> {
    const opportunityGame = (<unknown>(
      await this.findOne({ opportunity: opportunityId }, {}, { populate: "game" })
    )) as IPopulatedOpportunityGame | null;

    if (opportunityGame?.game) {
      return opportunityGame.game;
    }

    return null;
  }
}
