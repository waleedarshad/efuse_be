const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { isEmpty } = require("lodash");
const createError = require("http-errors");

const { GeneralHelper } = require("../../backend/helpers/general.helper");
const { allowedUserFields } = require("../../backend/helpers/whitelist_helper");

const { DOMAIN } = require("@efuse/key-store");
const { User } = require("../../backend/models/User");
const { Organization } = require("../../backend/models/Organization");
const { Opportunity } = require("../../backend/models/Opportunity");
const { OrganizationACLService } = require("../organizations/organization-acl.service");
const { OrganizationsService } = require("../organizations/organizations.service");
const { ApplicantBaseService } = require("../applicants/applicant-base.service");
const { ValidateRequirementsService } = require("../applicants/validate-requirements.service");
const { OpportunityGameService } = require("./opportunity-game.service");
const { ApplicantEntityTypeEnum } = require("../applicants/applicant.enum");
const { OpportunityRequirementBaseService } = require("./opportunity-requirement-base.service");
const { OrganizationUserRoleEnum } = require("../permissions/enums/organization-user-role.enum");
const { ConstantsService } = require("../constants");

const constantsService = new ConstantsService();
const applicantBaseService = new ApplicantBaseService();
const opportunityRequirementBaseService = new OpportunityRequirementBaseService();
const validateRequirementsService = new ValidateRequirementsService();
const organizationACLService = new OrganizationACLService();
const organizationsService = new OrganizationsService();

/**
 * @summary Method to lookup for all associated objects for an opportunity
 *
 * @param {Object} opportunity - The opportunity object to apply lookup against
 * @param {Object} currentUser - User who is requesting this opportunity
 *
 * @returns {Promise<any>} Opportunity Object
 */
const buildAggregate = async (opportunity, currentUser = null) => {
  const aggregateOpportunityObject = opportunity;

  aggregateOpportunityObject.applicantRequirements = {
    canApply: true,
    requirementsMet: [],
    requirementsNotMet: []
  };

  const opportunityGameService = new OpportunityGameService();

  aggregateOpportunityObject.associatedGame = await opportunityGameService.getAssociatedGame(opportunity._id);

  const { organization, user, game, _id } = aggregateOpportunityObject;
  if (currentUser) {
    const userId = GeneralHelper.getObjectId(currentUser._id);

    // Lookup for current user's applicant object
    const applicant = await applicantBaseService.findOne(
      {
        entity: GeneralHelper.getObjectId(_id),
        entityType: ApplicantEntityTypeEnum.OPPORTUNITY,
        user: userId
      },
      {},
      { lean: true }
    );

    if (applicant) {
      aggregateOpportunityObject.applicant = applicant;
    }

    aggregateOpportunityObject.applicantRequirements = await validateRequirementsService.validateByRequirementIds(
      currentUser.id,
      opportunity.requirements
    );
  }
  // Applicants Lookup for displaying avatar
  await applicantBaseService.getAssociatedApplicantsAvatar(aggregateOpportunityObject);

  // Organization Lookup
  if (organization) {
    const orgId = organization._id || organization;

    const associatedOrganization = await Organization.findOne({
      _id: GeneralHelper.getObjectId(orgId)
    })
      .lean()
      .select(["name", "profileImage", "verified", "user", "captains"]);

    if (associatedOrganization) {
      aggregateOpportunityObject.associatedOrganization = associatedOrganization;
    }
  }

  // User Lookup
  aggregateOpportunityObject.associatedUser = await User.findOne({
    _id: GeneralHelper.getObjectId(user)
  })
    .lean()
    .select(["name", "profilePicture", "username"]);

  // Opportunity Requirements
  const { requirements } = aggregateOpportunityObject;
  if (requirements && requirements.length > 0) {
    try {
      const fetchedRequirements = await opportunityRequirementBaseService.findByIds(
        aggregateOpportunityObject.requirements
      );

      aggregateOpportunityObject.requirements = fetchedRequirements;
    } catch (error) {
      logger.error({ message: error.message }, "Error while populating opportunities requirements");
    }
  }
  return aggregateOpportunityObject;
};

/**
 * @summary Mark an opportunity as verified
 *
 * @param {Object} opportunity - opportunity to be verified
 * @param {Object} currentUser - current user object
 *
 * @returns {Promise<boolean>} true/false
 */
const markOpportunityAsVerified = async (opportunity, currentUser) => {
  // Check if it is owned by organization
  if (opportunity.organization) {
    // Fetch organization
    await organizationACLService.canPerformAction(currentUser, opportunity.organization, [
      OrganizationUserRoleEnum.CAPTAIN,
      OrganizationUserRoleEnum.OWNER
    ]);

    const organization = await organizationsService.getOrganizationById(opportunity.organization);

    // return true when organization is verified
    return organization?.verified?.status === true;
  }

  // return true when user is owner & is verified
  if (String(opportunity.user) === String(currentUser._id)) {
    return currentUser.verified;
  }

  // Default return false
  return false;
};

/**
 * @summary Get recent active opportunities of a particular type
 * @param {String} type - type of opportunities
 * @param {Number} limit
 *
 * @return Opportunity Promise
 */
const getRecentActiveOpportunitiesByType = (type, limit) => {
  return Opportunity.find({
    opportunityType: type,
    $and: [
      {
        $or: [{ publishStatus: "open" }, { publishStatus: { $exists: false } }]
      },
      { $or: [{ status: "visible" }, { status: { $exists: false } }] }
    ]
  })
    .sort({ createdAt: -1 })
    .limit(limit)
    .lean();
};

/**
 * @summary Get recent 10 opportunities from each type
 * @param {Object} currentUser - current user object
 *
 * @returns {Promise<any>} {jobs, events, scholarships, teamOpenings}
 */
const recentOpportunities = async (currentUser) => {
  // Get constants
  const variables = await constantsService.getVariables("OPPORTUNITY_TYPE");

  // Make sure variables are present
  if (isEmpty(variables)) {
    throw createError(422, "OPPORTUNITY_TYPE constant are not found");
  }

  // Default limit
  const limit = 10;

  // Using Promise.all as it will try to resolve all promises concurrently
  const opportunities = await Promise.all([
    // opportunities[0] = Scholarship
    getRecentActiveOpportunitiesByType(variables.scholarship, limit),

    // opportunities[1] = Event
    getRecentActiveOpportunitiesByType(variables.event, limit),

    // opportunities[2] = Team Opening
    getRecentActiveOpportunitiesByType(variables.teamOpening, limit),

    // opportunities[3] = Job
    getRecentActiveOpportunitiesByType(variables.job, limit)
  ]);

  // Build scholarship aggregate
  const scholarshipPromises = [];

  for (const scholarship of opportunities[0]) {
    scholarshipPromises.push(buildAggregate(scholarship, currentUser));
  }
  // Resolving all promises concurrently
  const scholarships = await Promise.all(scholarshipPromises);

  // Build event aggregate
  const eventPromises = [];

  for (const event of opportunities[1]) {
    eventPromises.push(buildAggregate(event, currentUser));
  }
  // Resolving all promises concurrently
  const events = await Promise.all(eventPromises);

  // Build team opending aggregate
  const teamOpeningPromises = [];

  for (const teamOpening of opportunities[2]) {
    teamOpeningPromises.push(buildAggregate(teamOpening, currentUser));
  }
  // Resolving all promises concurrently
  const teamOpenings = await Promise.all(teamOpeningPromises);

  // Build job aggregate
  const jobPromises = [];

  for (const job of opportunities[3]) {
    jobPromises.push(buildAggregate(job, currentUser));
  }
  // Resolving all promises concurrently
  const jobs = await Promise.all(jobPromises);

  return { scholarships, events, teamOpenings, jobs };
};

/**
 * @summary Return validation error response object
 *
 * @param {string} error - Error message
 *
 * @return {object} {isValid: Boolean, error: string}
 */
const hasError = (error) => ({ isValid: isEmpty(error), error });

/**
 * @summary Validate the format of shortName
 *
 * @param {string} shortName - shortName to be validated
 *
 * @return {object} {isValid: Boolean, error: string}
 */
const shortNameFormatError = (shortName) => {
  let error = "";
  if (isEmpty(shortName)) {
    error = "shortName is required";
  } else if (shortName.length < 3 || shortName.length > 24) {
    error = "shortName must be 3-24 character alphanumeric and hyphen characters only";
  } else if (!/^[\dA-Za-z-]+$/.test(shortName)) {
    error = "shortName must be alphanumeric and hyphen characters only";
  }
  return hasError(error);
};

/**
 * @summary Validate uniqueness of shortName
 *
 * @param {string} shortName - shortName to be validated
 *
 * @returns {Promise<any>} {isValid: Boolean, error: string}
 */
const shortNameUniquenessError = async (shortName) => {
  let error = "";
  const existingShortName = await Opportunity.findOne({ shortName }).lean().select("_id shortName");

  if (existingShortName) {
    error = "Custom URL already in use. Please enter a unique url slug.";
  }

  return hasError(error);
};

// TODO: when this is converted to typescript use the ISitemapUrl for the return type
const getVisibleOpportunityUrls = async () => {
  try {
    const aggregate = [
      {
        $match: {
          $and: [{ shortName: { $ne: null } }, { $or: [{ status: "visible" }, { status: { $exists: false } }] }]
        }
      },
      {
        $project: { url: { $concat: [DOMAIN, "/o/", "$shortName"] } }
      }
    ];
    const opportunities = await Opportunity.aggregate(aggregate);

    return opportunities.map((url) => ({ url: url.url, changefreq: "daily", priority: 0.1 }));
  } catch (error) {
    logger.error(error, "Error: generating Learning urls");

    return [];
  }
};

module.exports.buildAggregate = buildAggregate;
module.exports.getVisibleOpportunityUrls = getVisibleOpportunityUrls;
module.exports.markOpportunityAsVerified = markOpportunityAsVerified;
module.exports.recentOpportunities = recentOpportunities;
module.exports.shortNameFormatError = shortNameFormatError;
module.exports.shortNameUniquenessError = shortNameUniquenessError;
