import { Types } from "mongoose";
import { isEmpty } from "lodash";

import { ApplicantService } from "../applicants/applicant.service";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { IApplicant } from "../../backend/interfaces/applicant";
import { File } from "../../backend/interfaces/file";
import { IUser } from "../../backend/interfaces/user";
import {
  IOpportunity,
  IOpportunityAssociatedObjects,
  IPopulatedOpportunity,
  IOpportunityRequirementsResponse
} from "../../backend/interfaces";
import { OrganizationsService } from "../organizations/organizations.service";
import { UserService } from "../users/user.service";
import WhitelistHelper from "../../backend/helpers/whitelist_helper";
import { ValidateRequirementsService } from "../applicants/validate-requirements.service";
import { OpportunityGameService } from "./opportunity-game.service";
import { ApplicantEntityTypeEnum } from "../applicants/applicant.enum";

@Service("opportunity-lookup.service")
export class OpportunityLookupService extends BaseService {
  private applicantService: ApplicantService;
  private organizationsService: OrganizationsService;
  private userService: UserService;
  private opportunityGameService: OpportunityGameService;
  private validateRequirementsService: ValidateRequirementsService;

  constructor() {
    super();

    this.applicantService = new ApplicantService();
    this.organizationsService = new OrganizationsService();
    this.userService = new UserService();
    this.opportunityGameService = new OpportunityGameService();
    this.validateRequirementsService = new ValidateRequirementsService();
  }

  /**
   * @summary Get most recent 6 applicant avatars for opportunity
   *
   * @param {Types.ObjectId | string} opportunityId
   *
   * @returns {Promise<File[]>}
   */
  @Trace()
  public async getApplicantAvatars(opportunityId: Types.ObjectId | string): Promise<File[]> {
    const applicants: IApplicant[] = await this.applicantService.find(
      { entity: opportunityId, entityType: ApplicantEntityTypeEnum.OPPORTUNITY },
      { user: 1 },
      // todo: investigate how to correctly use `populate`
      <never>{ populate: { path: "user", select: "profilePicture" }, lean: true, limit: 6, sort: { createdAt: -1 } }
    );

    const avatars: File[] = applicants
      .map((applicant): File | undefined => {
        const user: IUser = applicant.user as IUser;
        return user?.profilePicture;
      })
      .filter((avatar) => !isEmpty(avatar)) as File[];

    return avatars;
  }

  /**
   * Get opportunity applicant
   *
   * @param {Types.ObjectId | string} opportunityId
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IApplicant>}
   */
  @Trace()
  public async getApplicant(
    opportunityId: Types.ObjectId | string,
    userId: Types.ObjectId | string
  ): Promise<IApplicant | null> {
    const applicant: IApplicant | null = await this.applicantService.findOne(
      { entity: opportunityId, entityType: ApplicantEntityTypeEnum.OPPORTUNITY, user: userId },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false }
    );

    return applicant;
  }

  /**
   * @summary Get associated objects of an opportunity
   *
   * @param {IOpportunity} opportunity
   *
   * @returns {Promise<IOpportunityAssociatedObjects>}
   */
  @Trace()
  public async getAssociatedObjects(
    opportunity: IOpportunity,
    userId?: Types.ObjectId | string | null
  ): Promise<IOpportunityAssociatedObjects> {
    const resolvedPromises = await Promise.all([
      // If current user is an applicant on opportunity, fetch the applicant object
      userId
        ? this.getApplicant(opportunity._id, userId).catch((error) => {
            this.logger?.error(error, "Error while fetching opportunity applicant");
            return null;
          })
        : null,

      // Get avatars of the applicants of opportunity
      this.getApplicantAvatars(opportunity._id).catch((error) => {
        this.logger?.error(error, "Error while fetching opportunity applicant's avatars");
        return [];
      }),

      // Get organization if opportunity is created on behalf of it
      opportunity.organization
        ? this.organizationsService.findOne({ _id: opportunity.organization }).catch((error) => {
            this.logger?.error(error, "Error while fetching opportunity's organization");
            return null;
          })
        : null,

      // Get user who created the post
      opportunity.user
        ? this.userService
            .findOne({ _id: opportunity.user }, { ...WhitelistHelper.associatedUserFields().$project })
            .catch((error) => {
              this.logger?.error(error, "Error while fetching opportunity's user");
              return null;
            })
        : null,

      // Get applicant requirements
      <IOpportunityRequirementsResponse>(
        (opportunity.user
          ? this.validateRequirementsService.validateByRequirementIds(opportunity.user, opportunity.requirements)
          : { canApply: true, requirementsMet: [], requirementsNotMet: [] })
      ),

      // Get associated game
      this.opportunityGameService.getAssociatedGame(opportunity._id)
    ]);

    return {
      applicant: resolvedPromises[0],
      applicantAvatars: resolvedPromises[1] as File[] | never[],
      associatedOrganization: resolvedPromises[2],
      associatedUser: resolvedPromises[3],
      applicantRequirements: resolvedPromises[4],
      associatedGame: resolvedPromises[5]
    };
  }

  /**
   * @summary Append all the associated objects to opportunity
   *
   * @param {IOpportunity} opportunity
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IPopulatedOpportunity>}
   */
  @Trace()
  public async buildOpportunity(
    opportunity: IOpportunity,
    userId?: Types.ObjectId | string
  ): Promise<IPopulatedOpportunity> {
    const populatedOpportunity: IPopulatedOpportunity = <IPopulatedOpportunity>{ ...opportunity };

    // Get associated objects
    const associatedObjects: IOpportunityAssociatedObjects = await this.getAssociatedObjects(opportunity, userId);

    // Append applicant object
    populatedOpportunity.applicant = associatedObjects.applicant;

    // Append applicant avatars
    populatedOpportunity.applicantAvatars = associatedObjects.applicantAvatars;

    // Append organization
    populatedOpportunity.associatedOrganization = associatedObjects.associatedOrganization;

    // Append user
    populatedOpportunity.associatedUser = associatedObjects.associatedUser;

    // Append applicant requirements
    populatedOpportunity.applicantRequirements = associatedObjects.applicantRequirements;

    // Append game object
    populatedOpportunity.associatedGame = associatedObjects.associatedGame;

    return populatedOpportunity;
  }

  /**
   * @summary Iterate through opportunities, get associated objects & returned fully populated opportunity collection
   *
   * @param {IOpportunity[]} opportunities
   * @param {Types.ObjectId | string} userId
   *
   * @returns {Promise<IPopulatedOpportunity[]>}
   */
  @Trace()
  public async fetchAndAppendAssociatedObjects(
    opportunities: IOpportunity[],
    userId?: Types.ObjectId | string
  ): Promise<IPopulatedOpportunity[]> {
    // Construct an array of promises to build opportunity object
    const promises: Promise<IPopulatedOpportunity | null>[] = opportunities.map(
      (opportunity: IOpportunity): Promise<IPopulatedOpportunity | null> =>
        this.buildOpportunity(opportunity, userId).catch((error) => {
          this.logger?.error(error, "Error while resolving buildOpportunity");
          return null;
        })
    );

    // Resolve promises concurrently
    const resolvedPromises: (IPopulatedOpportunity | null)[] = await Promise.all(promises);

    // Neglect any opportunity object which is null
    const populatedOpportunities: IPopulatedOpportunity[] = resolvedPromises.filter(
      (populatedOpportunity: IPopulatedOpportunity | null) => !isEmpty(populatedOpportunity)
    ) as IPopulatedOpportunity[];

    return populatedOpportunities;
  }
}
