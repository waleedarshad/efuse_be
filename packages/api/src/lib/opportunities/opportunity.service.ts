import { Failure } from "@efuse/contracts";
import { FilterQuery, LeanDocument, QueryOptions, Types } from "mongoose";
import { ChangeStreamOperationTypeEnum } from "@efuse/entities";

import { IOpportunity } from "../../backend/interfaces/opportunity";
import { Service, Trace } from "../decorators";
import { Roles } from "../users/roles";
import { buildAggregate } from ".";
import { OpportunityBaseService } from "./opportunity-base.service";
import { IMongodbWatcherResponse } from "../../backend/interfaces";
import { OpportunitySearchService } from "./opportunity-search.service";
import { OpportunityStatusEnum } from "./opportunity-status.enum";
import { OpportunityPublishStatusEnum } from "./opportunity-publish-status.enum";

const SERVICE = "opportunity.service";

@Service(SERVICE)
export class OpportunityService extends OpportunityBaseService {
  private openAndVisibleQuery: FilterQuery<IOpportunity> = {
    publishStatus: OpportunityPublishStatusEnum.OPEN,
    status: OpportunityStatusEnum.VISIBLE
  };

  private $opportunitySearchService: OpportunitySearchService;

  constructor() {
    super();

    this.$opportunitySearchService = new OpportunitySearchService();
  }

  @Trace()
  public async closeOpportunity(opportunityId: Types.ObjectId | string): Promise<void> {
    if (!opportunityId) {
      this.logger?.debug("Opportunity Id is empty. Exiting");
      return;
    }

    await this.update(opportunityId, {
      status: OpportunityStatusEnum.HIDDEN,
      publishStatus: OpportunityPublishStatusEnum.CLOSED
    });
  }

  /**
   * @summary Lookup and populate opportunity with buildAggregate
   *
   * @param {Types.ObjectId | string} opportunityId
   *
   * @return {Promise<IOpportunity | null>}
   */
  @Trace()
  public async lookupOpportunity(opportunityId: Types.ObjectId | string): Promise<IOpportunity | null> {
    // Return if opportunityId is not found
    if (!opportunityId) {
      return null;
    }

    const opportunity = await this.findOne({ _id: opportunityId }, {}, { lean: true });

    // Return if opportunity if not found
    if (!opportunity) {
      return null;
    }

    // Build aggregate & return opportunity

    const aggregatedOpportunity: IOpportunity = await buildAggregate(opportunity);

    return aggregatedOpportunity;
  }

  @Trace()
  public async getAllOpenAndVisible(
    conditions?: FilterQuery<IOpportunity>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IOpportunity[]> {
    let query: FilterQuery<IOpportunity> = this.openAndVisibleQuery;
    if (conditions) {
      query = { ...query, ...conditions };
    }
    const opportunities: IOpportunity[] = await this.find(query, projection, options);
    return opportunities;
  }

  @Trace()
  public async toggleBoost(
    currentUserRoles: string[] | null | undefined,
    opportunityId: Types.ObjectId | string,
    boosted: boolean
  ): Promise<void> {
    // Make sure user is admin
    if (!currentUserRoles?.includes(Roles.ADMIN)) {
      throw Failure.Forbidden("You are not authorized to perform this action!");
    }

    // Validate opportunityId
    if (!Types.ObjectId.isValid(opportunityId)) {
      throw Failure.BadRequest("invalid opportunity ID");
    }

    // Update opportunity with boosted attribute
    await this.update(opportunityId, { "metadata.boosted": boosted });
  }

  /**
   * @summary Opportunity Watcher to read change streams
   *
   * @param {IMongodbWatcherResponse<LeanDocument<IOpportunity>>} data
   *
   * @returns {void}
   */
  @Trace({ service: SERVICE, name: "Watcher" })
  public static Watcher(data: IMongodbWatcherResponse<LeanDocument<IOpportunity>>): void {
    const opportunityService = new OpportunityService();

    try {
      if (data.operationType === ChangeStreamOperationTypeEnum.DELETE) {
        // Remove from algolia
        opportunityService.$opportunitySearchService.deleteFromAlgoliaAsync(data?.documentKey?._id);
      } else {
        // Sync Data with algolia
        opportunityService.$opportunitySearchService.syncWithAlgoliaAsync(data.fullDocument);
      }
    } catch (error) {
      opportunityService.logger?.error(error, "Error reading data in opportunity watcher");
    }
  }

  /**
   * @summary Get opportunity by id or shortName
   *
   * @param {Types.ObjectId | string | undefined} idOrShortName
   *
   * @returns {Promise<Types.ObjectId | undefined>}
   */
  @Trace()
  public async getOpportunityByIdOrShortName(
    idOrShortName: Types.ObjectId | string | undefined
  ): Promise<IOpportunity | undefined> {
    if (!idOrShortName) {
      return;
    }
    const isValidId = this.isValidObjectId(idOrShortName);

    // Lookup for opportunity by shortName
    const opportunity = await (isValidId
      ? this.findOne({ _id: idOrShortName })
      : this.findOne({ shortName: idOrShortName as string }));

    if (!opportunity) {
      throw Failure.UnprocessableEntity("Opportunity not found");
    }

    return opportunity;
  }
}
