import { Queue, Job } from "bull";
import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import moment from "moment";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import * as Config from "../../async/config";
import { OpportunityService } from "./opportunity.service";
import { OPPORTUNITIES } from "../metrics";

const OPPORTUNITY_BASE_QUEUE = "opportunity-async-queue";
const OPPORTUNITY_ASYNC_SERVICE = "opportunity-async.service";

enum OpportunityQueue {
  CLOSE_OPPORTUNITY_QUEUE = "close-opportunity-queue"
}

enum OpportunityQueueFunctions {
  CLOSE_OPPORTUNITIES_BASED_ON_DUE_DATE = "closeOpportunititesBasedOnDueDate"
}

interface CloseOpportunityJobParams {
  funcName: OpportunityQueueFunctions;
  params: {
    opportunityId: Types.ObjectId;
  };
}

type OpportunityJob = Job<CloseOpportunityJobParams>;

@Service(OPPORTUNITY_ASYNC_SERVICE)
export class OpportunityAsyncService extends BaseService {
  private static oppQueue: Map<string, Queue> = new Map<string, Queue>();

  private opportunityService: OpportunityService;

  public constructor() {
    super();

    this.opportunityService = new OpportunityService();
  }

  @Trace({ name: "CloseOpportunitiesByDueDateCron", service: OPPORTUNITY_ASYNC_SERVICE })
  public static async CloseOpportunitiesByDueDateCron(): Promise<void> {
    const asyncService = new OpportunityAsyncService();
    try {
      asyncService.recordMetric(OPPORTUNITIES.CLOSE_OPPORTUNITIES_BY_DUE_DATE.START);

      await asyncService.findAndQueueOpportunitiesForClosure();

      asyncService.recordMetric(OPPORTUNITIES.CLOSE_OPPORTUNITIES_BY_DUE_DATE.FINISH);
    } catch (error) {
      asyncService.recordMetric(OPPORTUNITIES.CLOSE_OPPORTUNITIES_BY_DUE_DATE.ERROR);

      asyncService.logger?.error(error);

      throw asyncService.handleError(error, "Could not finish finding and queuing opportunities for closure {cron}");
    }
  }

  @Trace({ name: "InitAsyncProcessors", service: OPPORTUNITY_ASYNC_SERVICE })
  public static InitAsyncProcessors(): void {
    const service = new OpportunityAsyncService();
    const oppQueueList: string[] = Object.values(OpportunityQueue);

    oppQueueList.forEach((queueName) => {
      const newQueue = OpportunityAsyncService.OpportunityBaseQueue(queueName);
      service.logger?.info(`Created Opportunity Queue: ${newQueue.name}`);
    });
  }

  @Trace({ name: "handleQueueJob", service: OPPORTUNITY_ASYNC_SERVICE })
  private static handleQueueJob(job: OpportunityJob): Promise<any> {
    const opportunityService = new OpportunityService();

    const funcName = job?.data?.funcName;

    switch (funcName) {
      case OpportunityQueueFunctions.CLOSE_OPPORTUNITIES_BASED_ON_DUE_DATE: {
        const opportunityId = job?.data?.params?.opportunityId;

        return opportunityService.closeOpportunity(opportunityId);
      }
      default:
        throw Failure.InternalServerError(`No queue service function named ${job?.data?.funcName}`, {
          data: job?.data
        });
    }
  }

  @Trace({ name: "OpportunityBaseQueue", service: OPPORTUNITY_ASYNC_SERVICE })
  private static OpportunityBaseQueue(oppQueueName: string): Queue {
    const queueName = `${OPPORTUNITY_BASE_QUEUE}:${oppQueueName.toLowerCase()}`;

    if (!OpportunityAsyncService.oppQueue[queueName]) {
      // attempt to get existing queue before creating a new one

      const foundOppQueue: Queue = Config.getQueue(queueName);

      if (foundOppQueue) {
        OpportunityAsyncService.oppQueue[queueName] = foundOppQueue;
      } else {
        // create a new queue

        OpportunityAsyncService.oppQueue[queueName] = Config.initQueue(queueName);

        OpportunityAsyncService.oppQueue[queueName].process((job: OpportunityJob) => this.handleQueueJob(job));
      }
    }

    return <Queue>OpportunityAsyncService.oppQueue[queueName];
  }

  @Trace()
  public async closeOpportunitiesQueue(opportunityId: Types.ObjectId): Promise<void> {
    const data: CloseOpportunityJobParams = {
      funcName: OpportunityQueueFunctions.CLOSE_OPPORTUNITIES_BASED_ON_DUE_DATE,
      params: {
        opportunityId
      }
    };

    try {
      const job = await OpportunityAsyncService.OpportunityBaseQueue(OpportunityQueue.CLOSE_OPPORTUNITY_QUEUE).add(
        data
      );

      return <void>await job.finished();
    } catch (error) {
      this.logger?.error({ createdData: data, error });
      throw this.handleError(error, "Function: closeOpportunitiesQueue");
    }
  }

  @Trace()
  public async findAndQueueOpportunitiesForClosure(): Promise<void> {
    try {
      const foundOpportunityIds = await this.getOpportunityIdsForClosure();

      if (!foundOpportunityIds || foundOpportunityIds.length === 0) {
        this.logger?.debug(foundOpportunityIds, "Opportunities is either empty or null");
        return;
      }

      this.logger?.info(`Attempting to queue ${foundOpportunityIds.length} opportuntities`);

      await Promise.allSettled(foundOpportunityIds.map((id) => this.closeOpportunitiesQueue(id)));

      this.logger?.info(`Finished queuing ${foundOpportunityIds.length} opportuntities`);
    } catch (error) {
      this.logger?.error(error);
      throw this.handleError(error, "Function: findAndCloseOpportunitiesOnDueDate");
    }
  }

  @Trace()
  private async getOpportunityIdsForClosure(): Promise<Types.ObjectId[]> {
    // this gets the current date and then sets its time to the end of the day
    // it is then used to get the date of the previous day

    const cutOff = moment().utc().subtract(1, "d").endOf("day").toDate();

    try {
      const opportunities = await this.opportunityService.getAllOpenAndVisible(
        {
          dueDate: { $lte: cutOff }
        },
        { _id: 1 },
        { lean: true }
      );

      if (!opportunities || opportunities.length === 0) {
        this.logger?.debug({ opportunities, cutOff }, "No opportunities found to close");
        return [];
      }

      // shouldn't be necessary to check for nulls, but doesn't hurt
      const opportunityIds = opportunities.map((opp) => <Types.ObjectId>opp._id).filter((id) => id !== null);

      return opportunityIds;
    } catch (error) {
      this.logger?.error(error);
      throw this.handleError(error, "Function: getOpportunitiesForClosure");
    }
  }
}
