const { StreamChat } = require("stream-chat");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { initQueue } = require("../../async/config");

const { getUserById } = require("../users");

const setUserChatToken = async (userId) => {
  const user = await getUserById(userId, { disableCache: true });

  logger.info({ user }, "Beginning setUserChatToken for user");

  // todo: investigate
  // @ts-ignore
  if (user.streamChatToken) {
    logger.warn({ user }, "User already has a streamChatToken");
    return;
  }
  const client = new StreamChat("mqw84w3ayrt3", "x3egat397chbeqq2jr9hcyg5f85cm9he8cmyzv8as5zcumwtvm2cu4gurc5bb3cy");

  // todo: investigate
  // @ts-ignore
  const streamChatToken = client.createToken(user.id.toString());

  // todo: investigate
  // @ts-ignore
  user.streamChatToken = streamChatToken;

  // todo: investigate
  // @ts-ignore
  const updatedUser = await user.save({ validateBeforeSave: false });
  await client.updateUsers([
    {
      // todo: investigate
      // @ts-ignore
      id: user.id.toString(),

      // todo: investigate
      // @ts-ignore
      name: user.name,

      // todo: investigate
      // @ts-ignore
      image: user.profilePicture.url
    }
  ]);
  client.disconnect().catch((error) => logger.error(error, "Error resolving client.disconnect promise"));
  logger.info({ user }, "Created streamChatToken for user");
  return updatedUser;
};

const setUserChatTokenAsyncQueue = initQueue("setUserChatToken", {
  limiter: { max: 5, duration: 1000 }
});

const setUserChatTokenAsync = async (userId) => {
  setUserChatTokenAsyncQueue.add(userId);
};

const initAsyncProcessors = () => {
  setUserChatTokenAsyncQueue
    .process((job) => setUserChatToken(job.data))
    .catch((error) => logger.error(error, "Error resolving setUserChatTokenAsyncQueue queue"));
};

module.exports.initAsyncProcessors = initAsyncProcessors;
module.exports.setUserChatToken = setUserChatToken;
module.exports.setUserChatTokenAsync = setUserChatTokenAsync;
