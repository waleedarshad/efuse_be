import axios, { AxiosInstance } from "axios";
import { ConfigIniParser } from "config-ini-parser";
import { RedisRecommendationsEnum, PublishStatus } from "@efuse/entities";
import { Queue } from "bull";

import { Service, Trace } from "../decorators";
import { LearningArticleService } from "../learningArticles/learning-article.service";
import { UserService } from "../users/user.service";
import { OpportunityBaseService } from "../opportunities/opportunity-base.service";
import { OrganizationBaseService } from "../organizations/organization-base.service";
import { RedisHelper } from "../../backend/helpers/redis.helper";
import WhitelistHelper from "../../backend/helpers/whitelist_helper";
import { ILearningArticle, IOpportunity, IOrganization, IUser } from "../../backend/interfaces";
import { BaseService } from "../base/base.service";
import Config from "../../async/config";

enum ResponseStatus {
  SUCCESS = "SUCCESS"
}

const API_URL = "https://servedbyadbutler.com/adserve/";
const AD_LEARNING_ROUTE = ";ID=178881;size=0x0;setID=436029;type=json;click=CLICK_MACRO_PLACEHOLDER";
const AD_OPPORTUNITIES_ROUTE = ";ID=178881;size=0x0;setID=436323;type=json;click=CLICK_MACRO_PLACEHOLDER";
const AD_ORGANIZATIONS_ROUTE = ";ID=178881;size=0x0;setID=436559;type=json;click=CLICK_MACRO_PLACEHOLDER";
const AD_USERS_ROUTE = ";ID=178881;size=0x0;setID=436681;type=json;click=CLICK_MACRO_PLACEHOLDER";

const SERVICE = "promoted.service";

@Service(SERVICE)
export class PromotedService extends BaseService {
  private learningArticleService: LearningArticleService;
  private userService: UserService;
  private opportunityBaseService: OpportunityBaseService;
  private organizationBaseService: OrganizationBaseService;
  private learningRecommendationsQueue: Queue;
  private opportunitiesRecommendationsQueue: Queue;
  private organizationsRecommendationsQueue: Queue;
  private usersRecommendationsQueue: Queue;
  protected http: AxiosInstance;

  constructor() {
    super();

    this.learningArticleService = new LearningArticleService();
    this.userService = new UserService();
    this.opportunityBaseService = new OpportunityBaseService();
    this.organizationBaseService = new OrganizationBaseService();
    this.http = axios.create({
      baseURL: API_URL
    });
    this.learningRecommendationsQueue = Config.initQueue("learningRecommendations");
    this.opportunitiesRecommendationsQueue = Config.initQueue("opportunitiesRecommendations");
    this.organizationsRecommendationsQueue = Config.initQueue("organizationsRecommendations");
    this.usersRecommendationsQueue = Config.initQueue("usersRecommendations");
  }

  /**
   * Convert array structure to object
   * @param {*} incoming
   */
  public static arrayToObject = (incoming) => {
    const result = {};

    incoming.forEach((variable) => {
      const [key, value] = variable;

      result[key] = value;
    });

    return result;
  };

  /**
   * @summary Get the number of promoted articles requested by the user
   *
   * @param {Number} amount - The amount of the articles requested
   * @returns {Promise<any>} articles
   */
  @Trace()
  public async getPromotedLearningArticles(amount: number): Promise<any> {
    try {
      // get all active promoted learning article ads from ad butler JSON API
      const adButlerResponse = await this.http.get(AD_LEARNING_ROUTE);

      const promotedArticles: ILearningArticle[] = [];
      let nonPromotedArticles: ILearningArticle[] = [];

      // Status will only be "SUCCESS" if there are active ads returned by Ad Butler API
      if (adButlerResponse.data.status === ResponseStatus.SUCCESS) {
        // use INI format parser to convert Ad Butler payload to usable data
        const delimiter = "\r\n";
        const parser = new ConfigIniParser(delimiter);
        parser.parse(adButlerResponse.data.placements.placement_1.body);

        const variables = parser.items("variables");
        const finalVariables = PromotedService.arrayToObject(variables);

        // Ad Butler response must contain _id
        if (finalVariables["_id"]) {
          const article = await this.learningArticleService.findOne({ _id: finalVariables["_id"] }, {}, { lean: true });

          // mark article as promoted, add Ad Butler redirect url
          if (article) {
            article.promoted = true;
            article.promoted_url = adButlerResponse.data.placements.placement_1.redirect_url;
            promotedArticles.push(article);
          }
        }
      }

      // only fetch non-promoted articles if amount needed in greater than 0
      if (amount - promotedArticles.length > 0) {
        // get most recent articles from db
        nonPromotedArticles = await this.learningArticleService.find(
          {
            status: PublishStatus.PUBLISHED,
            isActive: true
          },
          {},
          { sort: { createdAt: -1 }, limit: amount - promotedArticles.length, lean: true }
        );

        // mark all articles as not promoted
        nonPromotedArticles = nonPromotedArticles.map((article) => {
          article.promoted = false;
          return article;
        });
      }

      //shuffle in random order
      nonPromotedArticles = nonPromotedArticles.sort(() => Math.random() - 0.5);

      // combine promoted and non-promoted learning articles into single array
      const articles = [...promotedArticles, ...nonPromotedArticles];

      // get full article with full author, category, and link
      const articlesWithAssociatedObjects = await articles.map(async (article) => {
        return this.learningArticleService.generateFullArticle(article);
      });

      // wait for .map() promises to resolve before returning
      Promise.all(articlesWithAssociatedObjects)
        .then((results) => {
          RedisHelper.instance()
            .setAsync(RedisRecommendationsEnum.LEARNING, JSON.stringify(results))
            .catch((error) => this.logger?.error(error, "Error resolving setAsync promise"));
        })
        .catch((error) => this.logger?.error(error, "Error resolving all promises"));
    } catch (error) {
      this.logger?.error(error, " Error in getLearningRecommendations() - recommendations_controller.js");
    }
  }

  /**
   * Get  the number of promoted opportunities requested by the user
   *
   * @param {Number} amount - The amount of the opportunities requested
   * @returns {Promise<any>}  opportunities
   */
  public async getPromotedOpportunities(amount: number): Promise<any> {
    try {
      // get all active promoted opportunities ads from ad butler JSON API
      const adButlerResponse = await this.http.get(AD_OPPORTUNITIES_ROUTE);

      const promotedOpportunities: IOpportunity[] = [];
      let nonPromotedOpportunities: IOpportunity[] = [];

      // Status will only be "SUCCESS" if there are active ads returned by Ad Butler API
      if (adButlerResponse.data.status === ResponseStatus.SUCCESS) {
        // use INI format parser to convert Ad Butler payload to usable data
        const delimiter = "\r\n";
        const parser = new ConfigIniParser(delimiter);
        parser.parse(adButlerResponse.data.placements.placement_1.body);

        const variables = parser.items("variables");
        const finalVariables = PromotedService.arrayToObject(variables);

        // Ad Butler response must contain _id
        if (finalVariables["_id"]) {
          const opportunity = await this.opportunityBaseService.findOne(
            { _id: finalVariables["_id"] },
            {},
            { lean: true }
          );

          // if it found an opportunity, mark it as promoted, add Ad Butler redirect url
          if (opportunity) {
            opportunity.promoted = true;
            opportunity.promoted_url = adButlerResponse.data.placements.placement_1.redirect_url;
            promotedOpportunities.push(opportunity);
          }
        }
      }

      // only fetch non-promoted opportunities if amount needed in greater than 0
      if (amount - promotedOpportunities.length > 0) {
        // get most recent opportunities from db
        nonPromotedOpportunities = await this.opportunityBaseService.find(
          {
            publishStatus: PublishStatus.OPEN,
            status: PublishStatus.VISIBLE
          },
          {},
          { sort: { createdAt: -1 }, limit: amount - promotedOpportunities.length, lean: true }
        );

        // mark all opportunities as not promoted
        nonPromotedOpportunities = nonPromotedOpportunities.map((opportunity) => {
          opportunity.promoted = false;
          return opportunity;
        });
      }

      //shuffle in random order
      nonPromotedOpportunities = nonPromotedOpportunities.sort(() => Math.random() - 0.5);

      // combine promoted and non-promoted opportunities into single array
      const opportunities = [...promotedOpportunities, ...nonPromotedOpportunities];
      RedisHelper.instance()
        .setAsync(RedisRecommendationsEnum.OPPORTUNITIES, JSON.stringify(opportunities))
        .catch((error) => this.logger?.error(error, "Error resolving setAsync promise"));
    } catch (error) {
      this.logger?.error(error, "500 Error in getPromotedOpportunities - promoted.js");
    }
  }

  /**
   * Get  the number of promoted organization requested by the user
   *
   * @param {Number} amount - The amount of the organization requested
   * @returns {Promise<any>}  organizations
   */
  public async getPromotedOrganizations(amount: number): Promise<any> {
    try {
      // get all active promoted organization ads from ad butler JSON API
      const adButlerResponse = await this.http.get(AD_ORGANIZATIONS_ROUTE);

      const promotedOrganizations: IOrganization[] = [];
      let nonPromotedOrganizations: IOrganization[] = [];

      // Status will only be "SUCCESS" if there are active ads returned by Ad Butler API
      if (adButlerResponse.data.status === ResponseStatus.SUCCESS) {
        // use INI format parser to convert Ad Butler payload to usable data
        const delimiter = "\r\n";
        const parser = new ConfigIniParser(delimiter);
        parser.parse(adButlerResponse.data.placements.placement_1.body);

        const variables = parser.items("variables");
        const finalVariables = PromotedService.arrayToObject(variables);

        // Ad Butler response must contain _id
        if (finalVariables["_id"]) {
          const organization = await this.organizationBaseService.findOne(
            { _id: finalVariables["_id"] },
            {},
            { lean: true }
          );

          // if it found an organization, mark it as promoted, add Ad Butler redirect url
          if (organization) {
            organization.promoted = true;
            organization.promoted_url = adButlerResponse.data.placements.placement_1.redirect_url;
            promotedOrganizations.push(organization);
          }
        }
      }

      // only fetch non-promoted organizations if amount needed in greater than 0
      if (amount - promotedOrganizations.length > 0) {
        // get most recent organizations from db
        nonPromotedOrganizations = await this.organizationBaseService.find(
          {
            publishStatus: PublishStatus.VISIBLE
          },
          {},
          { sort: { createdAt: -1 }, limit: amount - promotedOrganizations.length, lean: true }
        );

        // mark all organizations as not promoted
        nonPromotedOrganizations = nonPromotedOrganizations.map((organization) => {
          organization.promoted = false;
          return organization;
        });
      }

      //shuffle in random order
      nonPromotedOrganizations = nonPromotedOrganizations.sort(() => Math.random() - 0.5);

      // combine promoted and non-promoted organization into single array
      const organizations = [...promotedOrganizations, ...nonPromotedOrganizations];
      RedisHelper.instance()
        .setAsync(RedisRecommendationsEnum.ORGANIZATIONS, JSON.stringify(organizations))
        .catch((error) => this.logger?.error(error, "Error resolving setAsync promise"));
    } catch (error) {
      this.logger?.error(error, "500 Error in getPromotedOrganizations - promoted.js");
    }
  }

  /**
   * Get  the number of promoted users requested by the user
   *
   * @param {Number} amount - The amount of the users requested
   * @returns {Promise<any>}  users
   */
  public async getPromotedUsers(amount: number): Promise<any> {
    try {
      // get all active promoted users ads from ad butler JSON API
      const adButlerResponse = await this.http.get(AD_USERS_ROUTE);

      const promotedUsers: IUser[] = [];
      let nonPromotedUsers: IUser[] = [];

      // Status will only be "SUCCESS" if there are active ads returned by Ad Butler API
      if (adButlerResponse.data.status === ResponseStatus.SUCCESS) {
        // use INI format parser to convert Ad Butler payload to usable data
        const delimiter = "\r\n";
        const parser = new ConfigIniParser(delimiter);
        parser.parse(adButlerResponse.data.placements.placement_1.body);

        const variables = parser.items("variables");
        const finalVariables = PromotedService.arrayToObject(variables);

        // Ad Butler response must contain _id
        if (finalVariables["_id"]) {
          let user = await this.userService.findOne({ _id: finalVariables["_id"] }, {}, { lean: true });

          // if it found an user, mark it as promoted, add Ad Butler redirect url
          if (user) {
            // @ts-ignore
            const formattedUser: IUser = WhitelistHelper.userPayload(user);
            user = formattedUser;
            user.promoted = true;
            user.promoted_url = adButlerResponse.data.placements.placement_1.redirect_url;
            promotedUsers.push(user);
          }
        }
      }

      // only fetch non-promoted users if amount needed in greater than 0
      if (amount - promotedUsers.length > 0) {
        // get most recent users from db
        nonPromotedUsers = await this.userService.find(
          {},
          {},
          { sort: { created: -1 }, limit: amount - promotedUsers.length, lean: true }
        );

        // mark all users as not promoted
        nonPromotedUsers = nonPromotedUsers.map((user) => {
          // @ts-ignore
          user = WhitelistHelper.userPayload(user);
          user.promoted = false;

          return user;
        });
      }

      // shuffle in random order
      nonPromotedUsers = nonPromotedUsers.sort(() => Math.random() - 0.5);

      // combine promoted and non-promoted users into single array
      const users = [...promotedUsers, ...nonPromotedUsers];
      RedisHelper.instance()
        .setAsync(RedisRecommendationsEnum.USERS, JSON.stringify(users))
        .catch((error) => this.logger?.error(error, "Error resolving setAsync promise"));
    } catch (error) {
      this.logger?.error(error, "500 Error in getUsersRecommendations() - recommendations_controller.js");
    }
  }

  /**
   * @summary get promoted/recommended learning articles using bull queue
   *
   * @param {number} amount
   *
   * @returns {void}
   */
  @Trace()
  public getPromotedLearningArticlesAsync(amount: number): void {
    this.learningRecommendationsQueue
      .add({ amount })
      .catch((error) => this.logger?.error(error, "Error while adding to queue learningRecommendationsQueue"));
  }

  /**
   * @summary get promoted/recommended opportunities using bull queue
   *
   * @param {number} amount
   *
   * @returns {void}
   */
  @Trace()
  public getPromotedOpportunitiesAsync(amount: number): void {
    this.opportunitiesRecommendationsQueue
      .add({ amount })
      .catch((error) => this.logger?.error(error, "Error while adding to queue learningRecommendationsQueue"));
  }

  /**
   * @summary get promoted/recommended organizations using bull queue
   *
   * @param {number} amount
   *
   * @returns {void}
   */
  @Trace()
  public getPromotedOrganizationsAsync(amount: number): void {
    this.organizationsRecommendationsQueue
      .add({ amount })
      .catch((error) => this.logger?.error(error, "Error while adding to queue learningRecommendationsQueue"));
  }

  /**
   * @summary get promoted/recommended users using bull queue
   *
   * @param {number} amount
   *
   * @returns {void}
   */
  @Trace()
  public getPromotedUsersAsync(amount: number): void {
    this.usersRecommendationsQueue
      .add({ amount })
      .catch((error) => this.logger?.error(error, "Error while adding to queue learningRecommendationsQueue"));
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const promotedService = new PromotedService();

    // getPromotedLearningArticles
    promotedService.learningRecommendationsQueue
      .process((job: { data: { amount: number } }) => {
        return promotedService.getPromotedLearningArticles(job.data.amount);
      })
      .catch((error) => promotedService.logger?.error(error, "Error while processing getPromotedLearningArticles"));

    // getPromotedOpportunities
    promotedService.opportunitiesRecommendationsQueue
      .process((job: { data: { amount: number } }) => {
        return promotedService.getPromotedOpportunities(job.data.amount);
      })
      .catch((error) => promotedService.logger?.error(error, "Error while processing getPromotedOpportunities"));

    // getPromotedOrganizations
    promotedService.organizationsRecommendationsQueue
      .process((job: { data: { amount: number } }) => {
        return promotedService.getPromotedOrganizations(job.data.amount);
      })
      .catch((error) => promotedService.logger?.error(error, "Error while processing getPromotedOrganizations"));

    // getPromotedUsers
    promotedService.usersRecommendationsQueue
      .process((job: { data: { amount: number } }) => {
        return promotedService.getPromotedUsers(job.data.amount);
      })
      .catch((error) => promotedService.logger?.error(error, "Error while processing getPromotedUsers"));
  }
}
