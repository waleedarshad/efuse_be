import { Failure } from "@efuse/contracts";
import { Types } from "mongoose";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ERenaPlayerService } from "./erena-player.service";
import { ERenaTeamService } from "./erena-team.service";
import {
  IERenaPlayer,
  IERenaPlayerPopulated,
  IERenaResponse,
  IERenaTeam,
  IERenaTeamPopulated,
  IERenaTournament,
  IERenaTournamentPopulated,
  IUpdatePlayerStatsParams
} from "../../backend/interfaces/erena";
import { ERenaTournamentService } from "./erena-tournament.service";

import { EFuseAnalyticsUtil } from "../analytics.util";
import { ERenaBracketService } from "./erena-bracket.service";
import { IOpportunity } from "../../backend/interfaces/opportunity";
import { ERenaScoringKinds, ERenaTournamentStatus, ERenaType } from "../../backend/interfaces/erena/erena-enums";

const STAT_NAME = "Points";

@Service("erena.service")
export class ERenaService extends BaseService {
  private erenaTeamService: ERenaTeamService;
  private erenaPlayerService: ERenaPlayerService;
  private erenaTournamentService: ERenaTournamentService;
  private erenaBracketService: ERenaBracketService;

  constructor() {
    super();

    this.erenaTeamService = new ERenaTeamService();
    this.erenaPlayerService = new ERenaPlayerService();
    this.erenaTournamentService = new ERenaTournamentService();
    this.erenaBracketService = new ERenaBracketService();
  }

  /**
   * @deprecated Use ERenaLeaderboardService.getPlayerLeaderboard
   * @summary Get players leaderboard
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaPlayer[] | []>}
   */
  @Trace()
  public async getPlayerLeaderboard(tournamentIdOrSlug: string, namespace: string): Promise<IERenaPlayer[] | []> {
    const tournament = await this.erenaTournamentService.getByIdOrSlug(tournamentIdOrSlug, namespace);
    if (!tournament) {
      return [];
    }

    const players = await this.erenaPlayerService.find(
      { tournament: tournament._id, name: { $not: /.*Placement Points.*/ } },
      {},
      { sort: { isActive: -1, "stats.kills": -1, "stats.value": -1 }, lean: true }
    );

    const response: IERenaPlayer[] = this.ranked(players) as IERenaPlayer[];
    return response;
  }

  /**
   * @deprecated Use ERenaLeaderboardService.getTeamLeaderboard
   * @summary Get teams leaderboard
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTeamPopulated[] | []>}
   */
  @Trace()
  public async getTeamLeaderboard(tournamentIdOrSlug: string, namespace: string): Promise<IERenaTeamPopulated[] | []> {
    const teams = await this.getTeamsForTournament(tournamentIdOrSlug, namespace);
    if (teams.length === 0) {
      return [];
    }

    const promises: Promise<IERenaTeamPopulated>[] = (teams as IERenaTeam[]).map(async (incoming: IERenaTeam) => {
      const team: IERenaTeamPopulated = incoming;

      team.players = await this.erenaPlayerService.find({ team: team._id });
      team.teamName = team.name;

      const scores = team.players?.map((player) => (player.stats?.value ?? player.stats?.kills) || 0);
      const teamScore = scores.reduce((a, b) => a + b, 0);

      team.stats = {
        kills: teamScore,
        value: teamScore,
        name: "Points"
      };

      return team;
    });

    const response: IERenaTeamPopulated[] = await Promise.all(promises);
    return this.ranked(response) as IERenaTeamPopulated[];
  }

  /**
   * @deprecated use ERenaTeamService.getPlayersByTeam
   * @summary Get players by team ID
   *
   * @param {Types.ObjectId | string} teamId
   *
   * @return {Promise<IERenaPlayer[]>}
   */
  @Trace()
  public async getPlayersByTeamId(teamId: Types.ObjectId | string): Promise<IERenaPlayer[]> {
    // Make sure it's a valid teamId
    if (!Types.ObjectId.isValid(teamId)) {
      throw Failure.BadRequest("invalid teamId");
    }

    // Make sure team exists
    const team = await this.erenaTeamService.findOne({ _id: teamId }, { _id: 1 });
    if (!team) {
      throw Failure.UnprocessableEntity("Team not found");
    }

    // Get & return players

    const erenaPlayers = await this.erenaPlayerService.find({ team: team._id });
    return erenaPlayers;
  }

  /**
   * @deprecated moved to ERenaPlayerService.postPlayer logic
   * @summary Create player
   *
   * @param {Types.ObjectId | string} teamId
   * @param {string} name
   * @param {Types.ObjectId | string} userId
   * @param {string} type
   * @param {Types.ObjectId | string} playerUserId
   *
   * @return {Promise<IERenaPlayer>}
   */
  @Trace()
  public async createPlayer(
    tournamentIdOrSlug: string,
    teamId: Types.ObjectId | string,
    name: string,
    userId: Types.ObjectId,
    type: string = ERenaType.whiteLabel,
    playerUserId?: Types.ObjectId
  ): Promise<IERenaPlayer> {
    // Make sure user is owner of tournament
    const tournament = await this.erenaTournamentService.verifyPermissionAndRetrieve(userId, tournamentIdOrSlug);

    // Get team
    const team = await this.erenaTeamService.findById(teamId);

    // Create player
    const erenaPlayer: IERenaPlayer = await this.erenaPlayerService.create({
      team: team._id,
      name,

      tournament: tournament._id,
      type,
      user: playerUserId
    });
    return erenaPlayer;
  }

  /**
   * @deprecated Use ERenaTeamService.deleteTeamAndPlayers
   * @summary Delete a particular team by ID
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} teamId
   *
   * @return {Promise<IERenaResponse>}
   */
  @Trace()
  public async deleteTeam(
    tournamentIdOrSlug: string,
    userId: Types.ObjectId | string,
    teamId: Types.ObjectId | string
  ): Promise<IERenaResponse> {
    // Make sure user is owner of the team
    const tournament = await this.erenaTournamentService.verifyPermissionAndRetrieve(userId, tournamentIdOrSlug);

    const deletedTeam: IERenaResponse = await this.erenaTeamService.deleteDeprecated(tournament._id, teamId);

    // Cleanup players when team deletion is success
    if (deletedTeam.result === "success") {
      const deletedPlayer: IERenaResponse = await this.erenaPlayerService.deleteMany({ team: teamId });
      return deletedPlayer;
    }

    return deletedTeam;
  }

  /**
   * @deprecated logic moved to ERenaPlayerController.deletePlayer
   * @summary Delete a particular player by ID
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} playerId
   *
   * @return {Promise<IERenaResponse>}
   */
  @Trace()
  public async deletePlayer(
    tournamentIdOrSlug: string,
    userId: Types.ObjectId | string,
    playerId: Types.ObjectId | string
  ): Promise<IERenaResponse> {
    // Make sure user is owner of the team
    const tournament = await this.erenaTournamentService.verifyPermissionAndRetrieve(userId, tournamentIdOrSlug);

    if (!Types.ObjectId.isValid(playerId)) {
      throw Failure.BadRequest("invalid playerId");
    }

    const player: IERenaPlayer | null = await this.erenaPlayerService.findOne({
      _id: playerId,

      tournament: tournament._id
    });

    if (!player) {
      throw Failure.UnprocessableEntity("Player not found");
    }

    const team: IERenaTeam | null = await this.erenaTeamService.findOne({ _id: player.team });

    const response = await this.erenaPlayerService.delete(playerId);

    // If successfully deleted, go ahead & recalculate team score
    if (response.result === "success" && team) {
      await this.updateTeamStats(team.name, team.isActive, team._id, tournamentIdOrSlug);
    }
    return response;
  }

  /**
   * @deprecated use ERenaTeamService.findTeamsByTournament
   * @summary Get teams for tournament
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTeam[] | []>}
   */
  @Trace()
  public async getTeamsForTournament(tournamentIdOrSlug: string, namespace: string): Promise<IERenaTeam[] | []> {
    const tournament = await this.erenaTournamentService.getByIdOrSlug(tournamentIdOrSlug, namespace);
    if (!tournament) {
      return [];
    }

    const erenaTeams = await this.erenaTeamService.find(
      { tournament: tournament._id },
      {},
      { sort: { isActive: -1, "stats.kills": -1, "stats.value": -1 }, lean: true }
    );
    return erenaTeams;
  }

  /**
   * @deprecated Logic moved to ERenaPlayerPlayerController.patchPlayer
   * NOTE: the new version does not update player stats, just the player object
   *
   * @summary Update player stats
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} playerId
   * @param {Types.ObjectId | string} teamId
   * @param {IUpdatePlayerStatsParams} playerParams
   * @param {Types.ObjectId | string} currentUserId
   *
   * @return {<IERenaPlayerPopulated>}
   */
  @Trace()
  public async updatePlayerStats(
    tournamentIdOrSlug: string,
    playerId: Types.ObjectId | string,
    teamId: Types.ObjectId | string,
    playerParams: IUpdatePlayerStatsParams,
    currentUserId: Types.ObjectId | string
  ): Promise<IERenaPlayerPopulated> {
    const totalKills = playerParams.total_kills ? playerParams.total_kills : 0;
    const { status, name, userId } = playerParams;

    // Make sure only eFuse admin or owner is allowed to update
    const tournament = await this.erenaTournamentService.verifyPermissionAndRetrieve(currentUserId, tournamentIdOrSlug);

    // Make sure team exists
    const team = await this.erenaTeamService.findById(teamId);

    // Build query to update players
    const playerUpdateQuery: Partial<IERenaPlayer> = <Partial<IERenaPlayer>>{
      tournament: tournament._id,
      name,

      team: team._id,
      isActive: status === ERenaTournamentStatus.active,
      stats: { kills: Number(totalKills), value: Number(totalKills), name: tournament.statName || STAT_NAME }
    };

    const updatedPlayer: IERenaPlayer = await this.erenaPlayerService.update(playerId, playerUpdateQuery);

    // spavel: There is a race condition here if multiple scores are updated in very quick succession.
    // update the team's stats so they're in sync
    await this.updateTeamStats(team.name, team.isActive, teamId, tournamentIdOrSlug);

    EFuseAnalyticsUtil.incrementMetric("erena.players.sync", 1, [
      `tournamentName:${tournament.tournamentName}`,
      `teamName:${team.name}`
    ]);

    const player: IERenaPlayerPopulated = <IERenaPlayerPopulated>{ ...updatedPlayer, team };

    if (userId) {
      player.user = userId;
    }

    return player;
  }

  /**
   * @deprecated We are no longer storing stats on the teams object
   * Retrieve scores using the ERenaScoresService
   *
   * @summary Update teams stats
   *
   * @param {IERenaScoreQueueData} queueData
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async updateTeamStats(
    name: string,
    isActive: boolean,
    teamId: string | Types.ObjectId,
    tournamentIdOrSlug: string
  ): Promise<void> {
    // Make sure tournament exists
    const tournament = await this.erenaTournamentService.getByIdOrSlug(tournamentIdOrSlug);
    if (!tournament) {
      throw Failure.UnprocessableEntity("Team not found");
    }

    const identifier = `${tournament.tournamentId} / ${tournament.tournamentName}`;
    const players: IERenaPlayer[] = await this.getPlayersByTeamId(teamId);

    // Calculate score
    let score = 0;
    players.forEach((player: IERenaPlayer) => {
      score += player?.stats?.kills || 0;
    });

    // Update team
    const updated = await this.erenaTeamService.findOneAndUpdate(
      { _id: teamId },
      {
        tournament: tournament._id,
        name,
        isActive,
        stats: { kills: Number(score), value: Number(score), name: tournament.statName || STAT_NAME }
      },
      { upsert: true }
    );

    EFuseAnalyticsUtil.incrementMetric("erena.team.sync", 1, [
      `tournamentName:${tournament.tournamentName}`,
      `teamName:${name}`
    ]);

    if (!updated) {
      this.logger?.info(`[${identifier}] Inserting new team into the database: ${name}`);
    }
  }

  /**
   * @deprecated logic moved to ERenaTournamentController.postTourname nt
   * @summary Create Tournament
   *
   * @param {IERenaTournament} tournamentParams
   *
   * @return {Promise<IERenaTournament>}
   */
  @Trace()
  public async createTournament(tournamentParams: IERenaTournament): Promise<IERenaTournament> {
    const tournament = await this.erenaTournamentService.create(tournamentParams);

    // Create bracket
    if (tournament.bracketType === ERenaScoringKinds.bracket) {
      this.erenaBracketService.createAsync(tournament.slug, tournamentParams.totalTeams);
    }

    return tournament;
  }

  /**
   * @deprecated logic moved to ERenaTournamentService.getTournamentWithStats
   * @summary Get tournament stats
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTournamentPopulated | null>}
   */
  @Trace()
  public async getTournamentStats(
    tournamentIdOrSlug: string,
    namespace: string
  ): Promise<IERenaTournamentPopulated | null> {
    const tournament: IERenaTournament | null = await this.erenaTournamentService.getByIdOrSlug(
      tournamentIdOrSlug,
      namespace
    );

    if (!tournament) {
      return null;
    }

    const populatedTournament: IERenaTournamentPopulated = <IERenaTournamentPopulated>(
      (<unknown>{ ...tournament.toObject() })
    );

    // Get teams
    const teams = await this.getTeamsForTournament(tournamentIdOrSlug, namespace);

    // Get team stats
    const teamStats: IERenaTeamPopulated[] = await Promise.all(
      (teams as IERenaTeamPopulated[]).map(async (incoming) => {
        const team = incoming;
        team.players = await this.getPlayersByTeamId(team._id);
        return team;
      })
    );

    populatedTournament.teamStats = teamStats;

    // Fetch Bracket
    if (populatedTournament.bracketType === ERenaScoringKinds.bracket) {
      const bracket = await this.erenaBracketService.findByTournament(tournament._id);
      if (bracket) {
        populatedTournament.bracket = bracket;
      }
    }

    // Lookup for opportunity
    const opportunity: IOpportunity | null = await this.erenaTournamentService.lookupOpportunity(
      tournament.opportunity
    );
    if (opportunity) {
      populatedTournament.opportunity = opportunity;
    }

    return populatedTournament;
  }

  /**
   * Deprecated - use ERenaTeamController.postTeam
   * @summary Create a new eRena Team
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {string} name
   * @param {string} type
   *
   * @return {Promise<IERenaTeamPopulated>}
   */
  @Trace()
  public async createTeam(
    tournamentIdOrSlug: string,
    userId: Types.ObjectId | string,
    name: string,
    type = ERenaType.whiteLabel
  ): Promise<IERenaTeamPopulated> {
    // Make sure user is owner
    const tournament = await this.erenaTournamentService.verifyPermissionAndRetrieve(userId, tournamentIdOrSlug);

    const team = await this.erenaTeamService.createDeprecated(tournament._id, name, type);
    return team;
  }

  /**
   * Deprecated - use ERenaTeamService.update
   * @summary Update a particular eRena Team
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} erenaMatchId
   * @param {string} name
   * @param {string} type
   *
   * @return {Promise<IERenaTeamPopulated>}
   */
  @Trace()
  public async updateTeam(
    tournamentIdOrSlug: string,
    userId: Types.ObjectId | string,
    erenaTeamId: Types.ObjectId | string,
    name: string,
    type: string
  ): Promise<IERenaTeam> {
    // Make sure user is owner
    await this.erenaTournamentService.verifyPermissionAndRetrieve(userId, tournamentIdOrSlug);

    const team = await this.erenaTeamService.update(erenaTeamId, { name, type });
    return team;
  }

  /**
   * @summary Sort players or teams on the basis of their stats
   *
   * @param {(IERenaPlayer | IERenaTeamPopulated)[]} data
   *
   * @return {(IERenaPlayer | IERenaTeamPopulated)[]}
   */
  @Trace()
  private ranked(data: (IERenaPlayer | IERenaTeamPopulated)[]): (IERenaPlayer | IERenaTeamPopulated)[] {
    return data.map((incoming: IERenaPlayer | IERenaTeamPopulated, index: number) => {
      const item = incoming;
      if (index > 0) {
        // Get our previous list item
        const prevItem = data[index - 1];

        if ((prevItem?.stats?.kills || prevItem?.stats?.value) === (item?.stats?.kills || item?.stats?.value)) {
          // Same score = same rank
          item.rank = prevItem.rank;
        } else {
          // Not the same score, give em the current iterated index + 1
          item.rank = index + 1;
        }
      } else {
        // First item takes the rank 1 spot
        item.rank = 1;
      }
      return item;
    });
  }
}
