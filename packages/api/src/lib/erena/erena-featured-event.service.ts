import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ErenaFeaturedEvent, ERenaFeaturedEventModel } from "../../backend/models/erena/erena-featured-event";
import { IERenaFeaturedEvent, IERenaTournament } from "../../backend/interfaces/erena";
import { ERenaTournamentService } from "./erena-tournament.service";

@Service("erena-featured-event.service")
export class ErenaFeaturedEventService extends BaseService {
  private readonly model: ERenaFeaturedEventModel<IERenaFeaturedEvent> = ErenaFeaturedEvent;

  private readonly tournamentService: ERenaTournamentService;

  public constructor() {
    super();

    this.tournamentService = new ERenaTournamentService();
  }

  /**
   * Get eRena Featured Event
   *
   * @returns {Promise<IERenaTournament>} The featured event object with id
   *
   * @memberof ERenaFeaturedEventService
   */
  @Trace()
  public async getFeaturedEvent(): Promise<null | IERenaTournament> {
    try {
      const flaggedEvent = await this.model.findOne({ active: true });

      if (!flaggedEvent) {
        return null;
      }

      const featuredEvent: IERenaTournament | null = await this.tournamentService.findOne({
        _id: flaggedEvent.tournament
      });

      if (!featuredEvent) {
        return null;
      }

      return featuredEvent;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
