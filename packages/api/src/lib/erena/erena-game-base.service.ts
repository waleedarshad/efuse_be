import { FilterQuery, QueryOptions } from "mongoose";
import { Failure } from "@efuse/contracts";

import { IERenaGame } from "../../backend/interfaces";
import { ERenaGame, ERenaGameModel } from "../../backend/models/erena";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("erena-game-base.service")
export class ERenaGameBaseService extends BaseService {
  protected readonly $erenaGames: ERenaGameModel<IERenaGame> = ERenaGame;

  public async findOne(condition: FilterQuery<IERenaGame>): Promise<IERenaGame | null>;
  public async findOne(condition: FilterQuery<IERenaGame>, projection: unknown): Promise<IERenaGame | null>;
  public async findOne(
    condition: FilterQuery<IERenaGame>,
    projection: unknown,
    options: QueryOptions
  ): Promise<IERenaGame | null>;
  @Trace()
  public async findOne(
    condition: FilterQuery<IERenaGame>,
    projection?: unknown,
    options?: QueryOptions
  ): Promise<IERenaGame | null> {
    const erenaGame = await this.$erenaGames.findOne(condition, projection, <any>options);
    return erenaGame;
  }

  /**
   * @summary Update a particular erena game object
   *
   * @param {FilterQuery<IERenaGame>} condition
   * @param {Partial<IERenaGame>} body
   * @param {unknown} options
   *
   * @returns {Promise<IERenaGame>}
   */
  @Trace()
  public async update(
    condition: FilterQuery<IERenaGame>,
    body: Partial<IERenaGame>,
    options: QueryOptions
  ): Promise<IERenaGame> {
    if (!body) {
      throw Failure.BadRequest("body is missing");
    }

    // Update the corresponding object as per condition
    const erenaGame: IERenaGame | null = await this.$erenaGames.findByIdAndUpdate(condition, { ...body }, options);

    // Make sure object is present
    if (!erenaGame) {
      throw Failure.UnprocessableEntity("ERenaGame not found");
    }

    return erenaGame;
  }
}
