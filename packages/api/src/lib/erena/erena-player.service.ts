import { Failure } from "@efuse/contracts";
import { Types, QueryOptions, FilterQuery } from "mongoose";

import { IERenaLeaderboardPlayer, IERenaPlayer, IERenaResponse } from "../../backend/interfaces/erena";
import { ERenaType } from "../../backend/interfaces/erena/erena-enums";
import { IERenaScore } from "../../backend/interfaces/erena/erena-score";
import { ERenaPlayer, ERenaPlayerModel } from "../../backend/models/erena";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ERenaScoreService } from "./erena-score.service";
import { TwitchAccountInfoService } from "../twitch-account-info.service";
import { UserService } from "../users/user.service";

@Service("erena-player.service")
export class ERenaPlayerService extends BaseService {
  private readonly erenaPlayers: ERenaPlayerModel<IERenaPlayer> = ERenaPlayer;

  private erenaScoreService: ERenaScoreService;
  private twitchAccountInfoService: TwitchAccountInfoService;
  private userService: UserService;

  public constructor() {
    super();

    this.erenaScoreService = new ERenaScoreService();
    this.twitchAccountInfoService = new TwitchAccountInfoService();
    this.userService = new UserService();
  }

  public async find(filter: FilterQuery<IERenaPlayer>): Promise<IERenaPlayer[]>;
  public async find(filter: FilterQuery<IERenaPlayer>, projection: any): Promise<IERenaPlayer[]>;
  public async find(
    filter: FilterQuery<IERenaPlayer>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaPlayer[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IERenaPlayer>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaPlayer[]> {
    const erenaPlayers = await this.erenaPlayers.find(filter, projection, options);
    return erenaPlayers;
  }

  /**
   * @summary Get player by ID
   *
   * @param {Types.ObjectId | string} playerId
   *
   * @return {Promise<ERenaTeam>}
   */
  @Trace()
  public async findById(playerId: Types.ObjectId | string): Promise<IERenaPlayer> {
    // Make sure teamId exists
    if (!playerId) {
      throw Failure.BadRequest("player id not found");
    }

    // Make sure it is a valid mongoose ID
    if (!Types.ObjectId.isValid(playerId)) {
      throw Failure.BadRequest("invalid teamId");
    }

    const player = await this.findOne({ _id: playerId });

    if (!player) {
      throw Failure.UnprocessableEntity("Player not found");
    }

    return player;
  }

  /**
   * @summary Get erena players by team
   *
   * @param {Types.ObjectId | string} team
   *
   * @return {Promise<IERenaPlayer[]>}
   */
  @Trace()
  public async getByTeam(team: Types.ObjectId | string): Promise<IERenaPlayer[]> {
    if (!Types.ObjectId.isValid(team)) {
      throw Failure.BadRequest("invalid teamId");
    }

    const erenaPlayers = await this.find({ team }, {}, { lean: true });
    return erenaPlayers;
  }

  /**
   * @summary Get erena players by tournament
   *
   * @param {Types.ObjectId | string} team
   *
   * @return {Promise<IERenaPlayer[]>}
   */
  @Trace()
  public async getByTournament(tournament: Types.ObjectId | string): Promise<IERenaPlayer[]> {
    if (!Types.ObjectId.isValid(tournament)) {
      throw Failure.BadRequest("invalid tournament");
    }

    const erenaPlayers = await this.find(
      { tournament },
      { _id: 1, name: 1, isActive: 1, team: 1, user: 1, tournament: 1, __v: 1 },
      { lean: true }
    );

    const populatedPlayers = await this.populatePlayerProfiles(erenaPlayers);
    return populatedPlayers;
  }

  /**
   * @summary Create erena player
   *
   * @param {Types.ObjectId | string} teamId
   * @param {string} name
   * @param {Types.ObjectId | string} tournament
   * @param {string} type
   * @param {Types.ObjectId | string} playerUserId used to link player in tournament to an eFuse user
   *
   * @return {Promise<IERenaPlayer>}
   */
  @Trace()
  public async create(player: Partial<IERenaPlayer>): Promise<IERenaPlayer> {
    if (!player.team || !Types.ObjectId.isValid(player.team)) {
      throw Failure.BadRequest("Invalid Team");
    }

    if (!player.tournament || !Types.ObjectId.isValid(player.tournament)) {
      throw Failure.BadRequest("invalid tournamentId");
    }

    if (player.user && !Types.ObjectId.isValid(<string>player.user)) {
      throw Failure.BadRequest("Invalid player id");
    }

    if (!player.type) {
      player.type = ERenaType.whiteLabel;
    }

    if (player.isActive === undefined) {
      player.isActive = true;
    }

    const doc = <IERenaPlayer>player;

    const erenaPlayer = await this.erenaPlayers.create(doc);
    return erenaPlayer;
  }

  /**
   * @summary Delete a particular player by ID
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {Types.ObjectId | string} playerId
   *
   * @return {Promise<IERenaResponse>}
   */
  @Trace()
  public async delete(playerId: Types.ObjectId | string): Promise<IERenaResponse> {
    // Make sure ID is valid
    if (!Types.ObjectId.isValid(playerId)) {
      throw Failure.BadRequest("invalid playerId");
    }

    const deleted = await this.erenaPlayers.deleteOne({ _id: playerId });
    return deleted.deletedCount ? { result: "success" } : { result: "failure" };
  }

  /**
   * @summary Delete players that match a particular condition
   *
   * @param {FilterQuery<IERenaPlayer>} filter
   * @param {ModelOptions} options
   *
   * @return {Promise<IERenaResponse>}
   */
  @Trace()
  public async deleteMany(filter: FilterQuery<IERenaPlayer>): Promise<IERenaResponse> {
    const deletedPlayers = await this.erenaPlayers.deleteMany(filter);
    return deletedPlayers.deletedCount ? { result: "success" } : { result: "failure" };
  }

  /**
   * @summary Update particular player
   *
   * @param {Types.ObjectId | string} erenaPlayerId
   * @param {Partial<IERenaPlayer>} erenaPlayerBody
   *
   * @return {Promise<IERenaPlayer>}
   */
  @Trace()
  public async update(
    erenaPlayerId: Types.ObjectId | string,
    erenaPlayerBody: Partial<IERenaPlayer>
  ): Promise<IERenaPlayer> {
    // Make sure ID is present
    if (!erenaPlayerId) {
      throw Failure.BadRequest("playerId is required");
    }

    // Make sure it is valid mongoose ID
    if (!Types.ObjectId.isValid(erenaPlayerId)) {
      throw Failure.BadRequest("invalid playerId");
    }

    // Make sure erenaPlayerBody is present
    if (!erenaPlayerBody) {
      throw Failure.BadRequest("body is required");
    }

    // update player
    const player: IERenaPlayer | null = await this.erenaPlayers
      .findOneAndUpdate({ _id: erenaPlayerId }, { ...erenaPlayerBody }, { new: true })
      .lean();

    // Make sure player exists
    if (!player) {
      throw Failure.UnprocessableEntity(`Unable to find player with id [${String(erenaPlayerId)}]`);
    }

    return player;
  }

  public async findOne(filter: FilterQuery<IERenaPlayer>): Promise<IERenaPlayer | null>;
  public async findOne(filter: FilterQuery<IERenaPlayer>, projection: any): Promise<IERenaPlayer | null>;
  public async findOne(
    filter: FilterQuery<IERenaPlayer>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaPlayer | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaPlayer>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaPlayer | null> {
    const player = await this.erenaPlayers.findOne(filter, projection, options);
    return player;
  }

  /**
   * Lookup and set player scores on an array of IERenaPlayers.
   *
   * @param players array of IERenaPlayer objects to add scores to.
   *
   * @returns Promise<IERenaLeaderboardPlayer[]>
   */
  @Trace()
  public async populatePlayerScores(players: IERenaPlayer[]): Promise<IERenaLeaderboardPlayer[]> {
    const playerIds = players.map((player) => <Types.ObjectId>player._id);
    // get all scores for the tournament
    const playerScores = await this.erenaScoreService.find({ ownerId: { $in: playerIds } }, {}, { lean: true });

    // map scores to player
    const playersWithScore = players.map((player: IERenaPlayer) => {
      const returnPlayer = <IERenaLeaderboardPlayer>player;
      // get all scores for the player
      const rawScores = playerScores.filter((s: IERenaScore) => s.ownerId.equals(player._id)).map((s) => s.score);

      returnPlayer.score = rawScores?.length > 0 ? rawScores.reduce((a, b) => a + b) : 0;

      return returnPlayer;
    });

    return playersWithScore;
  }

  private async populatePlayerProfiles(players: IERenaPlayer[]): Promise<IERenaPlayer[]> {
    const userIds = players.map((player) => <Types.ObjectId>player.user);

    const playerProfiles = await this.userService.find(
      { _id: { $in: userIds } },
      { _id: 1, name: 1, username: 1, twitchUserName: 1 },
      { lean: true }
    );
    const twitchAccountInfos = await this.twitchAccountInfoService.find(
      { _owner: { $in: userIds } },
      { owner: 1, username: 1 },
      { lean: true }
    );

    const populatedPlayers = players.map((player) => {
      if (!player.user) {
        return player;
      }

      const profile = playerProfiles.find((pp) => pp._id.toString() === player.user.toString());
      const twitchAccountInfo = twitchAccountInfos.find((tai) => tai.owner === player.user.toString());

      if (profile) {
        profile.twitchUserName = profile.twitchUserName ?? twitchAccountInfo?.username;
        player.user = profile;
      }

      return player;
    });

    return populatedPlayers;
  }
}
