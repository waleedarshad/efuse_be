import { Failure } from "@efuse/contracts";
import { cloneDeep } from "lodash";
import { Types, QueryOptions, FilterQuery } from "mongoose";
import { ERenaScoringKinds } from "../../backend/interfaces/erena/erena-enums";

import { IERenaScore } from "../../backend/interfaces/erena/erena-score";
import { ERenaScore, ERenaScoreModel } from "../../backend/models/erena/erena-score";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ERenaSharedService } from "./erena-shared.service";

const erenaSharedService = new ERenaSharedService();

@Service("erena-score.service")
export class ERenaScoreService extends BaseService {
  private readonly model: ERenaScoreModel<IERenaScore> = ERenaScore;

  /**
   * Create Score
   *
   * @param {IERenaScore} item The score object
   *
   * @returns {Promise<IERenaScore>} The created score object with id
   *
   * @memberof ERenaScoreService
   */
  @Trace()
  public async create(item: IERenaScore): Promise<IERenaScore> {
    try {
      const localItem = cloneDeep(item);
      if (!localItem) {
        throw Failure.BadRequest("Unable to create item, missing 'item'");
      }

      // Make sure only eFuse admin or owner is allowed to update
      // const tournament = await this.erenaTournamentService.verifyOwner(currentUserId, tournamentIdOrSlug);

      const validationErrors = this.validateIncomingFields(item);
      if (validationErrors.length > 0) {
        throw Failure.BadRequest(validationErrors.join(" | "));
      }

      const doc = <IERenaScore>localItem;
      const result: IERenaScore = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *  Find a score based on the its ID.
   *
   * @param {string} id - The score id
   *
   * @returns {Promise<IERenaScore} The eRena score corresponding to the ID
   *
   * @memberof ERenaScoreService
   */
  @Trace()
  public async findById(id: string | Types.ObjectId): Promise<IERenaScore> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to find item, missing 'id'");
      }

      const identifier: Types.ObjectId = typeof id === "string" ? Types.ObjectId(id) : id;

      const item: IERenaScore | null = await this.model.findById(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *  Find a score based on the its ID.
   *
   * @param {string} id - The score id
   *
   * @returns {Promise<IERenaScore} The eRena score corresponding to the ID
   *
   * @memberof ERenaScoreService
   */

  public async find(filter: FilterQuery<IERenaScore>): Promise<IERenaScore[]>;
  public async find(filter: FilterQuery<IERenaScore>, projection: any): Promise<IERenaScore[]>;
  public async find(
    filter: FilterQuery<IERenaScore>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaScore[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IERenaScore>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaScore[]> {
    const erenaScores = await this.model.find(filter, projection, options);
    return erenaScores;
  }

  public async findOne(filter: FilterQuery<IERenaScore>): Promise<IERenaScore | null>;
  public async findOne(filter: FilterQuery<IERenaScore>, projection: any): Promise<IERenaScore | null>;
  public async findOne(
    filter: FilterQuery<IERenaScore>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaScore | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaScore>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaScore | null> {
    const score = await this.model.findOne(filter, projection, options);
    return score;
  }

  /**
   * Delete a score
   *
   * @param {Promise<IERenaScore>} id The id of the score to delete
   *
   * @return {Promise<IERenaScore>} The deleted score
   *
   * @memberof ERenaScoreService
   */
  @Trace()
  public async delete(id: string): Promise<IERenaScore> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to create item, missing 'id'");
      }

      const identifier: Types.ObjectId = Types.ObjectId(id);
      const item: IERenaScore | null = await this.model.findByIdAndRemove(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Update existing score
   *
   * @param {string} id The id of the score to be updated
   * @param {IERenaScore} item The desired score content
   *
   * @returns {Promise<IERenaScore>} The updated score
   *
   * @memberof ERenaScoreService
   */
  @Trace()
  public async update(id: string, item: IERenaScore): Promise<IERenaScore> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to update item, missing 'id'");
      }

      if (!item) {
        throw Failure.BadRequest("Unable to update item, missing 'item'");
      }

      const identifier: Types.ObjectId = Types.ObjectId(id);
      const updated: IERenaScore = <IERenaScore>await this.model.findByIdAndUpdate(identifier, item, {
        new: true,
        lean: true
      });

      if (!updated) {
        throw Failure.NotFound("Unable to update requested item.");
      }

      return updated;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *
   * @param {IERenaScore[]} scores An array of all scores for a tournament
   * @returns {Promise<IERenaScore[]>} The updates scores
   *
   * @memberof ERenaScoreService
   *
   */
  @Trace()
  public async bulkCreateScores(tournamentId: string, body: IERenaScore[]): Promise<Promise<IERenaScore>[]> {
    const scores = body.map(async (item) => {
      const tournament = await erenaSharedService.getTournamentByIdOrSlug(tournamentId);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      item.tournament = Types.ObjectId(tournamentId);
      item.erenaKind =
        tournament.bracketType === ERenaScoringKinds.bracket ? ERenaScoringKinds.bracket : ERenaScoringKinds.pointRace;

      return this.create(item);
    });

    return scores;
  }

  /**
   *
   * @param {IERenaScore[]} scores An array of all scores for a tournament
   * @returns {Promise<IERenaScore[]>} The updates scores
   *
   * @memberof ERenaScoreService
   *
   */
  @Trace()
  public async bulkUpdateScores(tournamentId: string, body: IERenaScore[]): Promise<Promise<IERenaScore>[]> {
    // do a find where id = the player id and tournament id = tournament id
    const scores = body.map(async (item) => {
      // find the entry in the database so we can grab the id
      const dbScore: IERenaScore | null = await this.findOne({ tournament: tournamentId, ownerId: item.ownerId });
      if (!dbScore) {
        throw Failure.BadRequest("Unable to find ERena Score.");
      } else {
        dbScore.score = Number(item.score);
        return this.update(dbScore._id, dbScore);
      }
    });

    return scores;
  }

  /**
   * Checks the passed entity to make sure it has all the required fields for creation
   * @param entity The entity the verify
   *
   * @returns string[] Array of errors
   */
  public validateIncomingFields(entity: IERenaScore): string[] {
    const errors: string[] = [];
    if (!entity.erenaKind) {
      errors.push("Unable to create item, missing 'eRenaKind'");
    }

    if (!entity.ownerId) {
      errors.push("Unable to create item, missing 'ownerId'");
    }

    if (!entity.ownerKind) {
      errors.push("Unable to create item, missing 'ownerKind'");
    }

    if (entity.score === null || entity.score === undefined) {
      errors.push("Unable to create item, missing 'score'");
    }

    return errors;
  }
}
