import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ERenaFeaturedVideo, ERenaFeaturedVideoModel } from "../../backend/models/erena/erena-featured-video";
import { IERenaFeaturedVideo } from "../../backend/interfaces/erena";

@Service("erena-featured-video.service")
export class ERenaFeaturedVideoService extends BaseService {
  private readonly model: ERenaFeaturedVideoModel<IERenaFeaturedVideo> = ERenaFeaturedVideo;

  public constructor() {
    super();
  }

  /**
   * Get eRena Featured Videos
   *
   * @returns {Promise<IERenaFeaturedVideo[]>} The featured videos
   *
   * @memberof ERenaFeaturedVideoService
   */
  @Trace()
  public async getFeaturedVideos(): Promise<IERenaFeaturedVideo[]> {
    try {
      const featuredVideos = await this.model.find({}, null, { sort: { order: 1 } });

      if (!featuredVideos) {
        return [];
      }

      return featuredVideos;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
