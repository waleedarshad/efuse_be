import { Failure } from "@efuse/contracts";
import { findIndex, isEmpty } from "lodash";
import { Types, isValidObjectId, QueryOptions, FilterQuery } from "mongoose";

import { IERenaMatch, IERenaMatchCreateParams, IERenaMatchMultiCreateParams } from "../../backend/interfaces/erena";
import { File } from "../../backend/interfaces/file";
import { ERenaMatch, ERenaMatchModel } from "../../backend/models/erena";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ERenaRoundService } from "./erena-round.service";
import { ERenaSharedService } from "./erena-shared.service";
import { ERenaTeamService } from "./erena-team.service";

@Service("erena-match.service")
export class ERenaMatchService extends BaseService {
  private readonly erenaMatch: ERenaMatchModel<IERenaMatch> = ERenaMatch;

  private erenaSharedService: ERenaSharedService;
  private erenaTeamService: ERenaTeamService;
  private erenaRoundService: ERenaRoundService;

  constructor() {
    super();

    this.erenaSharedService = new ERenaSharedService();
    this.erenaTeamService = new ERenaTeamService();
    this.erenaRoundService = new ERenaRoundService();
  }

  public async findOne(filter: FilterQuery<IERenaMatch>): Promise<IERenaMatch | null>;
  public async findOne(filter: FilterQuery<IERenaMatch>, projection: any): Promise<IERenaMatch | null>;
  public async findOne(
    filter: FilterQuery<IERenaMatch>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaMatch | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaMatch>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaMatch | null> {
    const erenaMatch = await this.erenaMatch.findOne(filter, projection, options);
    return erenaMatch;
  }

  public async find(filter: FilterQuery<IERenaMatch>): Promise<IERenaMatch[]>;
  public async find(filter: FilterQuery<IERenaMatch>, projection: any): Promise<IERenaMatch[]>;
  public async find(
    filter: FilterQuery<IERenaMatch>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaMatch[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IERenaMatch>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaMatch[]> {
    const erenaMatch = await this.erenaMatch.find(filter, projection, options);
    return erenaMatch;
  }

  /**
   *  Find a match based on the its ID.
   *
   * @param {string} id - The match id
   *
   * @returns {Promise<IERenaMatch} The eRena round corresponding to the ID
   *
   * @memberof ERenaMatchService
   */
  @Trace()
  public async findById(id: string | Types.ObjectId): Promise<IERenaMatch> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to find item, missing 'id'");
      }

      const identifier: Types.ObjectId = typeof id === "string" ? Types.ObjectId(id) : id;

      const item: IERenaMatch | null = await this.erenaMatch.findById(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Retrieve matches by tournament id or slug
   *
   * @param {Types.ObjectId | string} tournamentIdOrSlug id or slug of the tournament
   * @param {Types.ObjectId | string} roundId - ID of the round to filter to
   *
   * @returns {Promise<IERenaMatch[]>}
   *
   * @memberof ERenaMatchService
   */
  public async findByTournament(tournamentIdOrSlug: string, roundId: string): Promise<IERenaMatch[]> {
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
    if (!tournament) {
      return [];
    }

    const query: FilterQuery<IERenaMatch> = { tournament: tournament._id };

    if (roundId && this.isValidObjectId(roundId)) {
      // pull round and get matches
      const round = await this.erenaRoundService.findById(roundId);

      const matchIds = round.matches.map((match) => match.match);
      query._id = matchIds;
    } else if (roundId) {
      return [];
    }

    const erenaMatches = await this.find(query, {}, { lean: true });

    return erenaMatches;
  }

  /**
   * @summary Create eRena Match
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   * @param {IERenaMatchCreateParams} matchParams
   *
   * @return {Promise<IERenaMatch>}
   *
   * @memberof ERenaMatchService
   */
  @Trace()
  public async createMatch(
    userId: Types.ObjectId | string,
    tournamentIdOrSlug: string,
    matchParams: IERenaMatchCreateParams
  ): Promise<IERenaMatch> {
    // Make sure user is owner of tournament
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

    if (!tournament) {
      throw Failure.NotFound("Unable to find tournament.");
    }

    // Build teamIds array
    const teamIds = [matchParams.team1, matchParams.team2];

    // Fetch the teams
    const teams = await this.erenaTeamService.find(
      { _id: { $in: teamIds }, tournament: tournament._id },
      { _id: 1 },
      { lean: true }
    );

    // Verify presence of teams
    if (teams.length !== 2) {
      throw Failure.UnprocessableEntity("Teams are not found");
    }

    // Create Match
    const erenaMatch: IERenaMatch = await this.create(tournament._id, matchParams);
    return erenaMatch;
  }

  /**
   * @summary Create multiple matches
   *
   * @param {IERenaMatchMultiCreateParams[]} matchDocs
   *
   * @return {Promise<IERenaMatch[]>}
   *
   * @memberof ERenaMatchService
   */
  @Trace()
  public async createMultiple(matchDocs: IERenaMatchMultiCreateParams[]): Promise<IERenaMatch[]> {
    // todo: investigate, why is the <never> necessary?
    const erenaMatch = <IERenaMatch[]>(<unknown>await this.erenaMatch.insertMany(<never>matchDocs));
    return erenaMatch;
  }

  /**
   * @summary Update a particular round
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {Types.ObjectId | string} matchId - ID of match to be updated
   * @param {Partial<IErenaMatch>} matchParams
   *
   * @return {Promise<IERenaRound>}
   *
   * @memberof ERenaMatchService
   */
  public async update(
    tournamentId: Types.ObjectId | string,
    matchId: Types.ObjectId | string,
    matchParams: Partial<IERenaMatch>
  ): Promise<IERenaMatch> {
    if (!matchParams) {
      throw Failure.BadRequest("params are not found");
    }

    const updatedMatch: IERenaMatch | null = await this.erenaMatch
      .findOneAndUpdate({ _id: matchId, tournament: tournamentId }, { ...matchParams }, { new: true })
      .lean();

    if (!updatedMatch) {
      throw Failure.UnprocessableEntity("Round not found");
    }

    return updatedMatch;
  }

  /**
   * @summary Upload score screenshots for teams
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} matchId
   * @param {File} teamOneImage
   * @param {File} teamTwoImage
   *
   * @return {Promise<IERenaMatch>}
   *
   * @memberof ERenaMatchService
   */
  public async uploadScreenshot(
    userId: Types.ObjectId | string,
    matchId: Types.ObjectId | string,
    teamOneImage: File,
    teamTwoImage: File
  ): Promise<IERenaMatch> {
    if (!isValidObjectId(matchId)) {
      throw Failure.BadRequest("invalid matchID");
    }

    // Find match
    const match = await this.findOne(
      { _id: matchId },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false }
    );

    if (!match) {
      throw Failure.UnprocessableEntity("Match not found");
    }

    // Make sure user is owner
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(
      (<Types.ObjectId>match?.tournament).toHexString()
    );

    if (!tournament) {
      throw Failure.UnprocessableEntity("[Upload Screenshot] Tournament not found", {
        tournament: (<Types.ObjectId>match?.tournament).toHexString()
      });
    }

    if (!isEmpty(teamOneImage)) {
      match.teamOneImage = teamOneImage;
    }

    if (!isEmpty(teamTwoImage)) {
      match.teamTwoImage = teamTwoImage;
    }

    const updatedMatch = <IERenaMatch>await this.erenaMatch.findOneAndUpdate({ _id: match._id }, match, { new: true });

    return updatedMatch;
  }

  /**
   * @summary Create eRena Match
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {IERenaMatchCreateParams} matchParams
   *
   * @return {Promise<IERenaMatch>}
   *
   * @memberof ERenaMatchService
   */
  @Trace()
  private async create(
    tournamentId: Types.ObjectId | string,
    matchParams: IERenaMatchCreateParams
  ): Promise<IERenaMatch> {
    const doc: Partial<IERenaMatch> = <Partial<IERenaMatch>>{
      tournament: tournamentId,
      matchDate: matchParams.matchDate,
      teams: [{ team: matchParams.team1 }, { team: matchParams.team2 }]
    };
    const erenaMatch = await this.erenaMatch.create(doc);
    return erenaMatch;
  }

  /**
   * @summary Update a particular match
   *
   * @param {ObjectId} userId - ID of currentUser
   * @param {String} tournamentIdOrSlug - ID or slug of tournament
   * @param {ObjectId} matchId - ID of match
   * @param {Object} matchParams - {matchDate, winner}
   *
   * @return IErenaMatch, IERenaMatch - currentMatch, nextMatch
   *
   * @memberof ERenaMatchService
   */
  public async updateMatch(
    matchId: Types.ObjectId | string,
    matchParams: IERenaMatch
  ): Promise<{ currentMatch: IERenaMatch; nextMatch?: IERenaMatch }> {
    // Verify matchId
    if (!Types.ObjectId.isValid(matchId)) {
      throw Failure.BadRequest("Invalid match id");
    }

    // Find match
    const match = await this.findById(matchId);

    // Verify presence
    if (!match) {
      throw Failure.UnprocessableEntity("Match not found!");
    }

    // Make sure user is owner
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(
      (<Types.ObjectId>match?.tournament).toHexString()
    );

    if (!tournament) {
      throw Failure.NotFound("Unable to find tournament.");
    }

    // Destruct params
    const { teams, winner, matchDate } = matchParams;

    // Set matchDate
    if (matchDate) {
      match.matchDate = matchDate;
    }
    if (teams) {
      match.teams = teams;
    }

    // Verify winner is in teams array & Set winner
    if (winner) {
      // Make sure winner is a valid objectId
      if (!Types.ObjectId.isValid(winner as string)) {
        throw Failure.BadRequest("winner ID is malformed");
      }

      // Make sure winner belongs to this match
      if (!match.teams.find((t) => String(t.team) === String(winner))) {
        throw Failure.BadRequest("Invalid winner ID");
      }

      // Set winner
      match.winner = winner;
    }

    // Save match
    await this.erenaMatch.update({ _id: match._id }, match);

    // return response if winner is not there
    if (!winner) {
      return { currentMatch: match };
    }

    // Advance winner to next round's match
    // First fetch current round
    const round = await this.erenaRoundService.findOne(
      { "matches.match": match._id, tournament: tournament._id },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false, lean: true }
    );

    // Verify round's presence
    if (!round) {
      // Unsettling winner
      match.winner = undefined;
      await this.erenaMatch.update({ _id: match._id }, match);

      // throw error
      throw Failure.UnprocessableEntity("Round not found!");
    }

    /**
     * Formula to get next match number
     *
     * n = currentIndex / 2
     * result = currentMatchNumber + numberOfMatchesInRound - n
     *
     * Example for position 8:
     * -> 8/2 (round down for odd numbers) = 4
     * -> 8+8-4 = 12
     */

    // Find index of current match
    const currentMatchIndex = findIndex(
      <{ match: IERenaMatch }[]>round.matches,
      (obj) => String(obj.match) === String(match._id)
    );

    // Find next match number
    const { matchNumber } = match;
    if (matchNumber) {
      const totalRoundMatches = round.matches.length;
      let nextMatchNumber = totalRoundMatches + matchNumber - Math.floor(currentMatchIndex / 2);
      const isEvenMatchNumber = matchNumber % 2 === 0;

      // when match number is even we should subtract 1 to get next matchNumber
      if (isEvenMatchNumber) {
        nextMatchNumber -= 1;
      }

      // Fetch next match
      const nextMatch = await this.findOne(
        {
          matchNumber: nextMatchNumber,
          tournament: tournament._id
        },
        {},
        // todo: investigate correct usage, is this from a plugin?
        <never>{ autopopulate: false }
      );

      // Build teamIndex in teams array, where team is to placed
      const teamIndex = isEvenMatchNumber ? 1 : 0;

      // Check if nextMatch is not found
      if (!nextMatch) {
        throw Failure.BadRequest("Winner is set but unable to advance to next round");
      }

      // Advance team to next round & save
      nextMatch.teams[teamIndex].team = winner;
      await this.erenaMatch.update({ _id: nextMatch._id }, nextMatch);
      // Return response
      return { nextMatch, currentMatch: match };
    }

    return { nextMatch: undefined, currentMatch: match };
  }
}
