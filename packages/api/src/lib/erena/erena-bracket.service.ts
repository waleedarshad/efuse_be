import { Failure } from "@efuse/contracts";
import { Types, isValidObjectId, QueryOptions, FilterQuery } from "mongoose";
import { findIndex, isEmpty } from "lodash";
import Bull from "bull";

import {
  IERenaBracket,
  IERenaMatch,
  IERenaRound,
  IERenaRoundCreateParams,
  IERenaTeam,
  IERenaTournament,
  IERenaMatchMultiCreateParams
} from "../../backend/interfaces/erena";
import { ERenaBracket, ERenaBracketModel } from "../../backend/models/erena";
import { Service, Trace } from "../decorators";
import { ERenaMatchService } from "./erena-match.service";
import { ERenaRoundService } from "./erena-round.service";
import Config from "../../async/config";
import { ConstantsService } from "../constants";
import { BaseService } from "../base/base.service";
import { ERenaTeamService } from "./erena-team.service";
import { File } from "../../backend/interfaces/file";
import { ERenaSharedService } from "./erena-shared.service";
import { ERenaScoringKinds, ERenaType } from "../../backend/interfaces/erena/erena-enums";

@Service("erena-bracket.service")
export class ERenaBracketService extends BaseService {
  private createBracketQueue: Bull.Queue<any>;
  private erenaMatchService: ERenaMatchService;
  private erenaRoundService: ERenaRoundService;
  private erenaSharedService: ERenaSharedService;
  private erenaTeamService: ERenaTeamService;
  private constantsService: ConstantsService;

  private readonly erenaBracket: ERenaBracketModel<IERenaBracket> = ERenaBracket;

  constructor() {
    super();

    this.erenaMatchService = new ERenaMatchService();
    this.erenaRoundService = new ERenaRoundService();
    this.erenaTeamService = new ERenaTeamService();
    this.erenaSharedService = new ERenaSharedService();
    this.constantsService = new ConstantsService();

    this.createBracketQueue = Config.initQueue("createBracket");
  }

  /**
   * @summary Create eRena Round
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   * @param {IERenaRoundCreateParams} roundParams
   *
   * @return {Promise<IERenaRound>}
   */
  @Trace()
  public async createRound(tournamentIdOrSlug: string, roundParams: IERenaRoundCreateParams): Promise<IERenaRound> {
    // Make sure user is owner of tournament
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

    if (!tournament) {
      throw Failure.UnprocessableEntity("Unable to find tournament");
    }

    // Makes sure valid match ids are passed
    const matches = await this.erenaMatchService.find(
      { _id: { $in: roundParams.matches }, tournament: tournament._id },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false }
    );

    // Make sure all matches exist in DB
    if (roundParams?.matches && matches?.length !== roundParams.matches.length) {
      throw Failure.UnprocessableEntity("Matches not found!");
    }

    // Create Round
    const erenaRound: IERenaRound = await this.erenaRoundService.create(tournament._id, roundParams);
    return erenaRound;
  }

  public async findOne(filter: FilterQuery<IERenaBracket>): Promise<IERenaBracket | null>;
  public async findOne(filter: FilterQuery<IERenaBracket>, projection: any): Promise<IERenaBracket | null>;
  public async findOne(
    filter: FilterQuery<IERenaBracket>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaBracket | null>;
  @Trace()
  public async findOne(
    filter?: FilterQuery<IERenaBracket>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaBracket | null> {
    const erenaBracket = await this.erenaBracket.findOne(filter, projection, options);
    return erenaBracket;
  }

  /**
   *  Find a bracket based on the its ID.
   *
   * @param {string} id - The bracket id
   *
   * @returns {Promise<IERenaBracket} The eRena bracket corresponding to the ID
   *
   * @memberof ERenaBracketService
   */
  @Trace()
  public async findById(id: string | Types.ObjectId): Promise<IERenaBracket> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to find item, missing 'id'");
      }

      const identifier: Types.ObjectId = typeof id === "string" ? Types.ObjectId(id) : id;

      const item: IERenaBracket | null = await this.erenaBracket.findById(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Get bracket for a particular tournament
   *
   * @param {Types.ObjectId | string} tournamentId - ID of the tournament
   *
   * @return {Promise<IErenaBracket | null>} ERenaBracket
   */
  @Trace()
  public async findByTournament(tournamentId: Types.ObjectId | string): Promise<IERenaBracket | null> {
    const erenaBracket = await this.findOne({ tournament: tournamentId });
    return erenaBracket;
  }

  /**
   * @summary Automatically create bracket for the event
   *
   * @param {string} tournamentIdOrSlug - tournamentId of slug of the tournament
   * @param {number} totalTeams - Number of teams participating in the tournament
   *
   * @return {Promise<void>}
   */
  @Trace()
  public async create(tournamentIdOrSlug: string, noOfTeams: number): Promise<void> {
    try {
      const totalTeams = Number(noOfTeams);

      // Fetch constants

      const variables: { teams: number[] } = await this.constantsService.getVariables("ERENA_BRACKET");

      // Make sure constants are present
      if (!variables.teams) {
        throw Failure.UnprocessableEntity("Teams constant is not found in feature ERENA_BRACKET");
      }

      // Make sure totalTeams are present in array of teams
      if (!variables.teams.includes(totalTeams)) {
        throw Failure.BadRequest("Invalid number of totalTeams found", { totalTeams, requiredTeams: variables.teams });
      }

      // Fetch tournament
      const tournament: IERenaTournament | null = await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(
        undefined,
        tournamentIdOrSlug
      );

      // Check tournament presence
      if (!tournament) {
        throw Failure.UnprocessableEntity("[Create Bracket] Tournament not found", { tournamentIdOrSlug });
      }

      // Check if bracket type is bracket
      if (tournament.bracketType !== ERenaScoringKinds.bracket) {
        throw Failure.BadRequest("bracketType is not bracket. Finish job", { tournamentIdOrSlug });
      }

      // Defaults
      let teamNumber = 0;
      const teamsToBeInserted: Partial<IERenaTeam>[] = [];

      // Create teams
      while (teamNumber !== totalTeams) {
        // Increment team number
        teamNumber += 1;

        // Push new team
        teamsToBeInserted.push({
          name: `Team ${teamNumber}`,
          type: ERenaType.efuse,
          isActive: true,

          tournament: tournament._id
        });
      }

      // Creating teams
      await this.erenaTeamService.createMultiple(teamsToBeInserted);

      // Defaults
      let matchesPerRound = totalTeams / 2;
      let roundNumber = 0;
      let matchNumber = 0;
      const roundsToBeInserted: Partial<IERenaRound>[] = [];

      // Generate rounds
      while (matchesPerRound >= 1) {
        // Round Defaults
        const teamsPerRound = matchesPerRound * 2;
        // const matches = [];

        // Increment roundNumber
        roundNumber += 1;

        // Add placeholder teams
        const placeholderTeams = new Array(teamsPerRound).fill({});

        const matchesToBeSaved: IERenaMatchMultiCreateParams[] = [];

        // Generate matches
        while (placeholderTeams.length !== 0) {
          // Increment matchNumber
          matchNumber += 1;

          // Push match
          matchesToBeSaved.push({
            matchNumber,

            tournament: tournament._id,
            teams: placeholderTeams.splice(0, 2) as unknown[]
          });
        }

        // Create matches

        const createdMatches: IERenaMatch[] = await this.erenaMatchService.createMultiple(matchesToBeSaved);

        const matches: Array<{ match: Types.ObjectId }> = createdMatches.map((m) => ({ match: m._id }));

        this.logger?.info(`Creating round ${roundNumber}`);

        // Push round
        roundsToBeInserted.push({
          roundTitle: `Round ${roundNumber}`,
          roundNumber,

          tournament: tournament._id,
          matches
        });
        // Build matches for next round
        matchesPerRound /= 2;
      }

      // Last match increments
      matchNumber += 1;
      roundNumber += 1;

      // Create Champion Match
      const championMatch: IERenaMatch = (
        await this.erenaMatchService.createMultiple([
          { matchNumber, tournament: tournament._id, teams: [{}] as unknown[] }
        ])
      )[0];

      this.logger?.info(`Creating round ${roundNumber}`);

      // Push Champion round
      roundsToBeInserted.push({
        roundTitle: `Round ${roundNumber}`,
        roundNumber,

        tournament: tournament._id,

        matches: [{ match: championMatch._id }]
      });

      // Create Rounds
      const createdRounds: IERenaRound[] = await this.erenaRoundService.createMultiple(roundsToBeInserted);

      const rounds: Array<{ round: Types.ObjectId }> = createdRounds.map((r) => ({ round: r._id }));

      this.logger?.info("Creating Bracket");

      const doc = <IERenaBracket>{
        totalTeams,
        totalRounds: rounds.length,

        tournament: tournament._id,
        totalMatches: matchNumber - 1,
        rounds
      };
      await this.erenaBracket.create(doc);
    } catch (error) {
      this.logger?.error(error, "Error while generating bracket");
    }
  }

  /**
   * @deprecated no longer doing this asynchronously
   * @summary Automatically create bracket for the event asynchronously
   *
   * @param {string} tournamentIdOrSlug - tournamentId of slug of the tournament
   * @param {number} totalTeams - Number of teams participating in the tournament
   *
   * @return {void}
   */
  @Trace()
  public createAsync(tournamentIdOrSlug: string, totalTeams: number): void {
    this.createBracketQueue.add({ tournamentIdOrSlug, totalTeams }).catch((error) => {
      this.logger?.error(error);
    });
  }

  /**
   * @deprecated no longer using async for this.
   * Async processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "erena-bracket.service" })
  public static InitAsyncProcessors(): void {
    const erenaBracketService = new ERenaBracketService();

    erenaBracketService.createBracketQueue
      .process((job: { data: { tournamentIdOrSlug: string; totalTeams: number } }) => {
        const { tournamentIdOrSlug, totalTeams } = job.data;

        erenaBracketService.create(tournamentIdOrSlug, totalTeams).catch((error) => {
          erenaBracketService.logger?.error(error, "Error while processing createBracketQueue");
        });
      })
      .catch((error) => {
        erenaBracketService.logger?.error(error, "Error creating bracket queue");
      });
  }

  /**
   * @deprecated use ERenaMatchService.updateMatch
   * @summary Update a particular match
   *
   * @param {ObjectId} userId - ID of currentUser
   * @param {String} tournamentIdOrSlug - ID or slug of tournament
   * @param {ObjectId} matchId - ID of match
   * @param {Object} matchParams - {matchDate, winner}
   *
   * @return {Object} - ERenaMatch
   */
  public async updateMatch(
    userId: Types.ObjectId | string,
    tournamentIdOrSlug: string,
    matchId: Types.ObjectId | string,
    matchParams: IERenaMatch
  ): Promise<{ currentMatch: IERenaMatch; bracket: IERenaBracket | null; nextMatch?: IERenaMatch }> {
    // Make sure user is owner
    const tournament = await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(userId, tournamentIdOrSlug);

    // Verify matchId
    if (!Types.ObjectId.isValid(matchId)) {
      throw Failure.BadRequest("Invalid matchId");
    }

    // Find match
    const match = await this.erenaMatchService.findOne(
      { _id: matchId },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false }
    );

    // Verify presence
    if (!match) {
      throw Failure.UnprocessableEntity("Match not found!");
    }

    // Destruct params
    const { winner, matchDate, teamOneScore, teamTwoScore, addTeam, addTeamIndex } = matchParams;

    // Set matchDate
    if (matchDate) {
      match.matchDate = matchDate;
    }
    if (teamOneScore) {
      match.teamOneScore = teamOneScore;
    }
    if (teamTwoScore) {
      match.teamTwoScore = teamTwoScore;
    }

    // addTeam and addTeamIndex are used to select the teams for the first round
    if (addTeam && addTeamIndex !== undefined && addTeamIndex >= 0) {
      match.teams[addTeamIndex].team = addTeam;
    }

    // Verify winner is in teams array & Set winner
    if (winner) {
      // Make sure winner is a valid objectId
      if (!Types.ObjectId.isValid(winner as string)) {
        throw Failure.BadRequest("winner ID is malformed");
      }

      // Make sure winner belongs to this match
      if (!match.teams.find((t) => String(t.team) === String(winner))) {
        throw Failure.BadRequest("Invalid winner ID");
      }

      // Set winner
      match.winner = winner;
    }

    // Save match
    await match.save();

    // return response if winner is not there
    if (!winner) {
      const bracket: IERenaBracket | null = await this.findByTournament(tournament._id);
      return { currentMatch: match, bracket };
    }

    // Advance winner to next round's match
    // First fetch current round
    const round = await this.erenaRoundService.findOne(
      { "matches.match": match._id, tournament: tournament._id },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false, lean: true }
    );

    // Verify round's presence
    if (!round) {
      // Unsettling winner
      match.winner = undefined;
      await match.save();

      // throw error
      throw Failure.UnprocessableEntity("Round not found!");
    }

    /**
     * Formula to get next match number
     *
     * n = currentIndex / 2
     * result = currentMatchNumber + numberOfMatchesInRound - n
     *
     * Example for position 8:
     * -> 8/2 (round down for odd numbers) = 4
     * -> 8+8-4 = 12
     */

    // Find index of current match
    const currentMatchIndex = findIndex(<never>round.matches, (obj: any) => String(obj.match) === String(match._id));

    // Find next match number
    const { matchNumber } = match;
    const totalRoundMatches = round.matches.length;
    let nextMatchNumber = totalRoundMatches + matchNumber - Math.floor(currentMatchIndex / 2);
    const isEvenMatchNumber = matchNumber % 2 === 0;

    // when match number is even we should subtract 1 to get next matchNumber
    if (isEvenMatchNumber) {
      nextMatchNumber -= 1;
    }

    // Fetch next match
    const nextMatch = await this.erenaMatchService.findOne(
      {
        matchNumber: nextMatchNumber,

        tournament: tournament._id
      },
      {},
      // todo: investigate correct usage, is this from a plugin?
      <never>{ autopopulate: false }
    );

    // Build teamIndex in teams array, where team is to placed
    const teamIndex = isEvenMatchNumber ? 1 : 0;

    // Check if nextMatch is not found
    if (!nextMatch) {
      throw Failure.BadRequest("Winner is set but unable to advance to next round");
    }

    // Advance team to next round & save
    nextMatch.teams[teamIndex].team = winner;
    await nextMatch.save();

    const bracket = await this.findByTournament(tournament._id);
    // Return response
    return { nextMatch, currentMatch: match, bracket };
  }

  /**
   * @deprecated Use `ERenaMatchService.uploadScreenshot (note, does not )
   * @summary Upload score screenshots for teams
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} matchId
   * @param {File} teamOneImage
   * @param {File} teamTwoImage
   *
   * @return {Promise<{match: IERenaMatch, bracket: IERenaBracket}>}
   */
  public async uploadScreenshot(
    userId: Types.ObjectId | string,
    tournamentIdOrSlug: string,
    matchId: Types.ObjectId | string,
    teamOneImage: File,
    teamTwoImage: File
  ): Promise<{ match: IERenaMatch; bracket: IERenaBracket }> {
    if (!isValidObjectId(matchId)) {
      throw Failure.BadRequest("invalid matchID");
    }

    // Make sure user is owner
    const tournament: IERenaTournament = await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(
      userId,
      tournamentIdOrSlug
    );

    const imageParams: any = {};

    if (!isEmpty(teamOneImage)) {
      imageParams.teamOneImage = teamOneImage;
    }

    if (!isEmpty(teamTwoImage)) {
      imageParams.teamTwoImage = teamTwoImage;
    }

    // Find match
    const eRenaMatch: IERenaMatch = await this.erenaMatchService.update(tournament._id, matchId, imageParams);

    if (!eRenaMatch) {
      throw Failure.InternalServerError("Match not found");
    }

    const bracket: IERenaBracket = <IERenaBracket>await this.findByTournament(tournament._id);

    return { match: eRenaMatch, bracket };
  }
}
