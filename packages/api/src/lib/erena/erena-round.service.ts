import { Failure } from "@efuse/contracts";
import { QueryOptions, FilterQuery, Types } from "mongoose";

import { IERenaRound, IERenaRoundCreateParams } from "../../backend/interfaces/erena";
import { ERenaRound, ERenaRoundModel } from "../../backend/models/erena";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { ERenaSharedService } from "./erena-shared.service";

@Service("erena-round.service")
export class ERenaRoundService extends BaseService {
  private readonly erenaRound: ERenaRoundModel<IERenaRound> = ERenaRound;

  private erenaSharedService: ERenaSharedService;

  constructor() {
    super();

    this.erenaSharedService = new ERenaSharedService();
  }

  /**
   *  Find a round based on the its ID.
   *
   * @param {string} id - The round id
   *
   * @returns {Promise<IERenaRound} The eRena round corresponding to the ID
   *
   * @memberof ERenaRoundService
   */
  @Trace()
  public async findById(id: string | Types.ObjectId): Promise<IERenaRound> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to find item, missing 'id'");
      }

      const identifier: Types.ObjectId = typeof id === "string" ? Types.ObjectId(id) : id;

      const item: IERenaRound | null = await this.erenaRound.findById(identifier).lean();

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public async find(filter: FilterQuery<IERenaRound>): Promise<IERenaRound[]>;
  public async find(filter: FilterQuery<IERenaRound>, projection: any): Promise<IERenaRound[]>;
  public async find(
    filter: FilterQuery<IERenaRound>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaRound[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IERenaRound>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaRound[]> {
    const erenaRounds = await this.erenaRound.find(filter, projection, options);
    return erenaRounds;
  }

  /**
   * @summary Update a particular round
   *
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} roundId - ID of round to be updated
   * @param {IERenaRound} roundParams - {roundTitle, roundDate, matches}
   *
   * @return {Promise<IERenaRound>}
   */
  public async updateRound(
    userId: Types.ObjectId | string,
    roundId: Types.ObjectId | string,
    roundParams: IERenaRoundCreateParams
  ): Promise<IERenaRound> {
    if (!Types.ObjectId.isValid(roundId)) {
      throw Failure.BadRequest("Invalid round id");
    }

    const round = await this.findById(roundId);

    // Verify presence
    if (!round) {
      throw Failure.UnprocessableEntity("Round not found!");
    }

    // Make sure user is owner of tournament
    await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(userId, round.tournament.toHexString());

    const doc: IERenaRound = <IERenaRound>{
      roundDate: roundParams.roundDate ? roundParams.roundDate : round.roundDate,
      roundTitle: roundParams.roundTitle ? roundParams.roundTitle : round.roundTitle,
      roundNumber: roundParams.roundNumber ? roundParams.roundNumber : round.roundNumber,
      matches: roundParams.matches ? roundParams.matches.map((m) => ({ match: Types.ObjectId(m) })) : round.matches
    };

    // Update round
    const erenaRound: IERenaRound = await this.update(roundId, doc);
    return erenaRound;
  }

  /**
   * @summary Create eRena Round
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {IERenaRoundCreateParams} roundParams
   *
   * @return {Promise<IERenaRound>}
   */
  @Trace()
  public async create(
    tournamentId: Types.ObjectId | string,
    roundParams: IERenaRoundCreateParams
  ): Promise<IERenaRound> {
    const doc = <IERenaRound>(<unknown>{
      roundDate: roundParams.roundDate,
      roundTitle: roundParams.roundTitle,
      roundNumber: roundParams.roundNumber,
      tournament: tournamentId,
      matches: <{ match: string }[]>roundParams?.matches?.map((m) => ({ match: m }))
    });

    const round = await this.erenaRound.create(doc);
    return round;
  }

  /**
   * @summary Create multiple rounds
   *
   * @param {Partial<IERenaRound>[]} roundDocs
   *
   * @return {Promise<IERenaRound[]>}
   */
  @Trace()
  public async createMultiple(roundDocs: Partial<IERenaRound>[]): Promise<IERenaRound[]> {
    // todo: investigate, why is the <never> necessary?
    const erenaRounds = <IERenaRound[]>(<unknown>await this.erenaRound.insertMany(<never>roundDocs));
    return erenaRounds;
  }

  public async findOne(filter: FilterQuery<IERenaRound>): Promise<IERenaRound | null>;
  public async findOne(filter: FilterQuery<IERenaRound>, projection: any): Promise<IERenaRound | null>;
  public async findOne(
    filter: FilterQuery<IERenaRound>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaRound | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaRound>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaRound | null> {
    const erenaRound = await this.erenaRound.findOne(filter, projection, options);
    return erenaRound;
  }

  /**
   * @summary Update a particular round
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {Types.ObjectId | string} roundId - ID of round to be updated
   * @param {IERenaRound} roundParams - {roundTitle, roundDate, matches}
   *
   * @return {Promise<IERenaRound>}
   */
  private async update(roundId: Types.ObjectId | string, roundParams: Partial<IERenaRound>): Promise<IERenaRound> {
    if (!roundParams) {
      throw Failure.BadRequest("params are not found");
    }

    const updatedRound: IERenaRound | null = await this.erenaRound
      .findOneAndUpdate({ _id: roundId }, { ...roundParams }, { new: true })
      .lean();

    if (!updatedRound) {
      throw Failure.UnprocessableEntity("Round not found");
    }

    return updatedRound;
  }
}
