import { IERenaTournamentPopulated } from "../../backend/interfaces";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { ERENA } from "../metrics";
import { ERenaTournamentService } from "./erena-tournament.service";

const ERENA_CRON_SERVICE = "erena-cron.service";

@Service(ERENA_CRON_SERVICE)
export class ErenaCronService extends BaseService {
  @Trace({ name: "UpdateErenaEventStatusCron", service: ERENA_CRON_SERVICE })
  public static async UpdateErenaEventStatusCron(): Promise<void> {
    const cronService = new ErenaCronService();
    const erenaTournamentService = new ERenaTournamentService();
    const tournamentPromises: Promise<IERenaTournamentPopulated>[] = [];

    try {
      cronService.recordMetric(ERENA.UPDATE_ERENA_EVENT_STATUS_CRON.START);
      cronService.logger?.info("Starting cron task for updating eRena event statuses.");

      const eventsToActivate = await erenaTournamentService.getEventsToActivate();
      const eventsToFinish = await erenaTournamentService.getEventsToFinish();
      const events = [...eventsToActivate, ...eventsToFinish];

      if (!events || events.length <= 0) {
        cronService.logger?.info("No events found. Exiting.");
        return;
      }

      cronService.logger?.info(`Found ${events.length} events. Processing.`);

      events.forEach((event) => {
        tournamentPromises.push(erenaTournamentService.moveToNextStatus(event));
      });

      await Promise.all(tournamentPromises);

      cronService.logger?.info("Finished cron task for updating eRena event statuses.");
      cronService.recordMetric(ERENA.UPDATE_ERENA_EVENT_STATUS_CRON.FINISH);
    } catch (error) {
      cronService.recordMetric(ERENA.UPDATE_ERENA_EVENT_STATUS_CRON.ERROR);

      throw cronService.handleError(error, "Unable to update eRena event statuses.");
    }
  }
}
