import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

import { ERenaTournamentService } from "./erena-tournament.service";
import {
  IERenaLeaderboardPlayer,
  IERenaLeaderboardTeam,
  IERenaPlayer,
  IERenaTeam
} from "../../backend/interfaces/erena";
import { ERenaPlayerService } from "./erena-player.service";
import { ERenaScoreService } from "./erena-score.service";
import { IERenaScore } from "../../backend/interfaces/erena/erena-score";
import { ERenaTeamService } from "./erena-team.service";
import { ERenaScoreOwnerKinds } from "../../backend/interfaces/erena/erena-enums";

@Service("erena-leaderboard.service")
export class ERenaLeaderboardService extends BaseService {
  private erenaTournamentService: ERenaTournamentService;
  private erenaPlayerService: ERenaPlayerService;
  private erenaScoreService: ERenaScoreService;
  private erenaTeamService: ERenaTeamService;

  constructor() {
    super();

    this.erenaTournamentService = new ERenaTournamentService();
    this.erenaPlayerService = new ERenaPlayerService();
    this.erenaScoreService = new ERenaScoreService();
    this.erenaTeamService = new ERenaTeamService();
  }

  /**
   * @summary Get players leaderboard
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaPlayer[] | []>}
   */
  @Trace()
  public async getPlayerLeaderboard(
    tournamentIdOrSlug: string,
    namespace: string,
    includePlacement: boolean
  ): Promise<IERenaLeaderboardPlayer[] | []> {
    const tournament = await this.erenaTournamentService.getByIdOrSlug(tournamentIdOrSlug, namespace);
    if (!tournament) {
      return [];
    }

    // get players
    let players: IERenaPlayer[] = await this.erenaPlayerService.getByTournament(tournament._id);

    if (!includePlacement) {
      players = players.filter((player) => player.name !== "Team Placement Points");
    }

    const filter = { tournament: tournament?._id, ownerKind: ERenaScoreOwnerKinds.player };

    if (!includePlacement) {
      filter["name"] = { $not: /.*Placement Points.*/ };
    }

    // get scores
    const scores = await this.erenaScoreService.find(filter, {}, { lean: true });

    // map scores to player
    const playersWithScore = players.map((player: IERenaPlayer) => {
      const returnPlayer = <IERenaLeaderboardPlayer>player;
      // get all scores for the player
      const playerScores = scores.filter((s: IERenaScore) => s.ownerId.equals(player._id)).map((s) => s.score);

      returnPlayer.score = playerScores?.length > 0 ? playerScores.reduce((a, b) => a + b) : 0;

      return returnPlayer;
    });

    const response: IERenaLeaderboardPlayer[] = this.ranked(playersWithScore) as IERenaLeaderboardPlayer[];
    return response;
  }

  /**
   * @summary Get teams leaderboard
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTeamPopulated[] | []>}
   */
  @Trace()
  public async getTeamLeaderboard(
    tournamentIdOrSlug: string,
    namespace: string
  ): Promise<IERenaLeaderboardTeam[] | []> {
    const tournament = await this.erenaTournamentService.getByIdOrSlug(tournamentIdOrSlug, namespace);
    if (!tournament) {
      return [];
    }

    const teams: IERenaTeam[] = await this.erenaTeamService.findTeamsByTournament(tournament._id, namespace);
    if (teams.length === 0) {
      return [];
    }

    const teamScores = await this.erenaScoreService.find(
      { tournament: tournament?._id, ownerKind: ERenaScoreOwnerKinds.team },
      {},
      { lean: true }
    );

    // get the individual player scores
    const playerScores = await this.getPlayerLeaderboard(tournamentIdOrSlug, namespace, true);

    // assign the players to their team
    const leaderboardTeams = teams.map((t: IERenaTeam) => {
      const team: IERenaLeaderboardTeam = <IERenaLeaderboardTeam>t;

      const teamScore = teamScores.find((s: IERenaScore) => s.ownerId.equals(team._id));

      team.score = teamScore ? teamScore.score : 0;

      if (playerScores?.length > 0) {
        team.players = playerScores.filter((player: IERenaLeaderboardPlayer) => player.team.equals(team._id));

        // calculate total team score
        const teamPlayerScores = team.players.map((player: IERenaLeaderboardPlayer) => player.score);
        if (teamScores) {
          team.score += teamPlayerScores.reduce((a, b) => a + b, 0);
        }
      }

      return team;
    });

    // rank and return the team
    return this.ranked(leaderboardTeams) as IERenaLeaderboardTeam[];
  }

  /**
   * @summary Sort players or teams on the basis of their stats
   *
   * @param {(IERenaLeaderboardPlayer | IERenaLeaderboardTeam)[]} data
   *
   * @return {(IERenaLeaderboardPlayer | IERenaLeaderboardTeam)[]}
   */
  @Trace()
  private ranked(
    data: (IERenaLeaderboardPlayer | IERenaLeaderboardTeam)[]
  ): (IERenaLeaderboardPlayer | IERenaLeaderboardTeam)[] {
    return data.map((incoming: IERenaLeaderboardPlayer | IERenaLeaderboardTeam, index: number) => {
      const item = incoming;
      if (index > 0) {
        // Get our previous list item
        const prevItem = data[index - 1];

        if (prevItem.score === item.score) {
          // Same score = same rank
          item.rank = prevItem.rank;
        } else {
          // Not the same score, give em the current iterated index + 1
          item.rank = index + 1;
        }
      } else {
        // First item takes the rank 1 spot
        item.rank = 1;
      }
      return item;
    });
  }
}
