import { Failure } from "@efuse/contracts";
import { Types, QueryOptions, FilterQuery } from "mongoose";

import {
  IERenaLeaderboardPlayer,
  IERenaLeaderboardTeam,
  IERenaPlayer,
  IERenaResponse,
  IERenaTeam,
  IERenaTeamPopulated
} from "../../backend/interfaces/erena";
import { ERenaType } from "../../backend/interfaces/erena/erena-enums";
import { IERenaScore } from "../../backend/interfaces/erena/erena-score";
import { ERenaTeam, ERenaTeamModel } from "../../backend/models/erena";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { ERenaPlayerService } from "./erena-player.service";
import { ERenaScoreService } from "./erena-score.service";
import { ERenaSharedService } from "./erena-shared.service";

@Service("erena-team.service")
export class ERenaTeamService extends BaseModelService<IERenaTeam> {
  private readonly erenaTeams: ERenaTeamModel<IERenaTeam> = ERenaTeam;

  private erenaSharedService: ERenaSharedService;
  private erenaPlayerService: ERenaPlayerService;
  private erenaScoreService: ERenaScoreService;

  public static ProjectValues = { _id: 1, name: 1, isActive: 1, tournament: 1, __v: 1 };

  constructor() {
    super(ERenaTeam);

    this.erenaSharedService = new ERenaSharedService();
    this.erenaPlayerService = new ERenaPlayerService();
    this.erenaScoreService = new ERenaScoreService();
  }

  /**
   * @summary Get team by ID
   *
   * @param {Types.ObjectId | string} teamId
   *
   * @return {Promise<ERenaTeam>}
   */
  @Trace()
  public async findById(teamId: Types.ObjectId | string, lean = false): Promise<IERenaTeam> {
    // Make sure teamId exists
    if (!teamId) {
      throw Failure.BadRequest("teamId not found");
    }

    // Make sure it is a valid mongoose ID
    if (!Types.ObjectId.isValid(teamId)) {
      throw Failure.BadRequest("invalid teamId");
    }

    const team = await this.findOne({ _id: teamId }, {}, { lean });

    if (!team) {
      throw Failure.UnprocessableEntity("Team not found");
    }

    return team;
  }

  /**
   * @summary Get teams for tournament
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTeam[] | []>}
   */
  @Trace()
  public async findTeamsByTournament(tournamentIdOrSlug: string, namespace?: string): Promise<IERenaTeam[]> {
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug, namespace);
    if (!tournament) {
      return [];
    }

    const query: FilterQuery<IERenaTeam> = { tournament: tournament._id };

    const erenaTeams = await this.find(query, ERenaTeamService.ProjectValues, { lean: true });

    return erenaTeams;
  }

  /**
   * @summary Delete a particular team by ID
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {Types.ObjectId | string} teamId
   *
   * @return {Promise<IERenaTeam>}
   */
  @Trace()
  public async deleteTeamAndPlayers(
    userId: Types.ObjectId | string,
    teamId: Types.ObjectId | string
  ): Promise<IERenaTeam> {
    const team = await this.findById(teamId);

    if (!team) {
      throw Failure.BadRequest("Could not find team");
    }

    // Make sure user is owner of the team
    await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(userId, team.tournament.toHexString());

    const deletedTeam: IERenaTeam = await this.deleteTeam(teamId);

    // Cleanup players when team deletion is success
    if (deletedTeam) {
      await this.erenaPlayerService.deleteMany({ team: teamId });
    }

    return deletedTeam;
  }

  /**
   * @summary Delete a particular team by ID
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {Types.ObjectId | string} teamId
   *
   * @return {Promise<IERenaTeam>}
   */
  @Trace()
  private async deleteTeam(teamId: Types.ObjectId | string): Promise<IERenaTeam> {
    // Make sure teamId is valid
    if (!Types.ObjectId.isValid(teamId)) {
      throw Failure.BadRequest("invalid teamId");
    }

    const team = await this.findById(teamId);

    if (!team) {
      throw Failure.BadRequest("Unable to find team.");
    }

    const deletedTeam = await team.deleteOne();

    return deletedTeam;
  }

  public async findOne(filter: FilterQuery<IERenaTeam>): Promise<IERenaTeam | null>;
  public async findOne(filter: FilterQuery<IERenaTeam>, projection: any): Promise<IERenaTeam | null>;
  public async findOne(
    filter: FilterQuery<IERenaTeam>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaTeam | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaTeam>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaTeam | null> {
    const team = await this.erenaTeams.findOne(filter, projection, options);
    return team;
  }

  public async find(filter: FilterQuery<IERenaTeam>): Promise<IERenaTeam[]>;
  public async find(filter: FilterQuery<IERenaTeam>, projection: any): Promise<IERenaTeam[]>;
  public async find(
    filter: FilterQuery<IERenaTeam>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaTeam[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IERenaTeam>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaTeam[]> {
    const erenaTeams = await this.erenaTeams.find(filter, projection, options);
    return erenaTeams;
  }

  /**
   * @summary Create a team
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} name
   *
   * @return {Promise<IERenaTeam>}
   */
  @Trace()
  public async create(team: IERenaTeam): Promise<IERenaTeam> {
    // Make sure tournament ID is present
    if (!team.tournament) {
      throw Failure.BadRequest("tournament is required");
    }

    // Make sure name is present
    if (!team.name) {
      throw Failure.BadRequest("name is required");
    }

    const doc = <IERenaTeam>{
      name: team.name,
      type: team.type ? team.type : ERenaType.whiteLabel,
      isActive: true,
      tournament: team.tournament
    };

    const newTeam = await this.erenaTeams.create(doc);

    return newTeam;
  }

  /**
   * @summary Update particular team
   *
   * @param {Types.ObjectId | string} erenaTeamId
   * @param {Partial<IERenaTeam>} erenaTeamBody
   *
   * @return {Promise<IERenaTeam>}
   */
  @Trace()
  public async update(erenaTeamId: Types.ObjectId | string, erenaTeamBody: Partial<IERenaTeam>): Promise<IERenaTeam> {
    // Make sure ID is present
    if (!erenaTeamId) {
      throw Failure.BadRequest("teamId is required");
    }

    // Make sure it is valid mongoose ID
    if (!Types.ObjectId.isValid(erenaTeamId)) {
      throw Failure.BadRequest("invalid teamId");
    }

    // Make sure erenaTeamBody is present
    if (!erenaTeamBody) {
      throw Failure.BadRequest("body is required");
    }

    // update team
    const team: IERenaTeam | null = await this.erenaTeams
      .findOneAndUpdate({ _id: erenaTeamId }, { ...erenaTeamBody }, { new: true })
      .lean();

    // Make sure team exists
    if (!team) {
      throw Failure.UnprocessableEntity(`Unable to find team with id [${String(erenaTeamId)}]`);
    }

    return team;
  }

  /**
   * @summary find a particular team & update it
   *
   * @param {FilterQuery<IERenaTeam>} conditions
   * @param {Partial<IERenaTeam>} erenaTeamBody
   * @param {QueryOptions} options
   *
   * @return {Promise<IERenaTeam | null>}
   */
  public async findOneAndUpdate(
    filter: FilterQuery<IERenaTeam>,
    erenaTeamBody: Partial<IERenaTeam>,
    options: QueryOptions
  ): Promise<IERenaTeam | null> {
    const erenaTeam = await this.erenaTeams.findOneAndUpdate(filter, { ...erenaTeamBody }, options);
    return erenaTeam;
  }

  /**
   * @summary Create multiple teams
   *
   * @param {Partial<IERenaTeam>[]} teamDocs
   *
   * @return {Promise<IERenaTeam[]>}
   */
  @Trace()
  public async createMultiple(teamDocs: Partial<IERenaTeam>[]): Promise<IERenaTeam[]> {
    // todo: investigate, why is the <never> necessary?
    const erenaTeams = <IERenaTeam[]>(<unknown>await this.erenaTeams.insertMany(<never>teamDocs));
    return erenaTeams;
  }

  @Trace()
  public async populateTeamsWithPlayers(erenaTeams: IERenaLeaderboardTeam[]): Promise<IERenaLeaderboardTeam[]> {
    if (!erenaTeams || erenaTeams.length === 0) {
      return [];
    }
    const tournamentId = erenaTeams[0].tournament;

    const allPlayers = await this.erenaPlayerService.getByTournament(tournamentId);

    const populatedTeams = erenaTeams.map((team: IERenaTeam) => {
      const popTeam = <IERenaLeaderboardTeam>team;
      popTeam.players = <IERenaLeaderboardPlayer[]>allPlayers.filter((player) => player.team.equals(team._id));

      return popTeam;
    });

    return populatedTeams;
  }

  @Trace()
  public async populateTeamsWithScores(
    erenaTeams: IERenaLeaderboardTeam[],
    includePlayers: boolean
  ): Promise<IERenaLeaderboardTeam[]> {
    if (!erenaTeams || erenaTeams.length === 0) {
      return [];
    }
    const tournamentId = erenaTeams[0].tournament;

    // get all scores for the tournament
    const scoreQuery: FilterQuery<IERenaScore> = { tournament: tournamentId };
    const tournamentScores = await this.erenaScoreService.find(scoreQuery, {}, { lean: true });

    // get all players
    const players: IERenaPlayer[] = await this.erenaPlayerService.getByTournament(tournamentId);

    // map scores to player
    const playersWithScore = players.map((player: IERenaPlayer) => {
      const returnPlayer = <IERenaLeaderboardPlayer>player;
      // get all scores for the player
      const playerScores = tournamentScores
        .filter((s: IERenaScore) => s.ownerId.equals(player._id))
        .map((s) => s.score);

      returnPlayer.score = playerScores?.length > 0 ? playerScores.reduce((a, b) => a + b) : 0;

      return returnPlayer;
    });

    // map scores to teams
    const teamsWithScore = erenaTeams.map((team: IERenaLeaderboardTeam) => {
      const localTeam = team;

      // get the team's score
      const teamScore = tournamentScores.find((s: IERenaScore) => s.ownerId.equals(team._id));
      localTeam.score = teamScore?.score ? teamScore.score : 0;

      // filter player scores to just the players on the team
      const teamPlayerScores = playersWithScore.filter((player) => player.team.equals(team._id));
      if (includePlayers) {
        localTeam.players = teamPlayerScores;
      }

      // sum up the player scores
      const rawScoreValues = teamPlayerScores.map((s) => s.score);

      localTeam.summedPlayerScore = rawScoreValues?.length > 0 ? rawScoreValues.reduce((a, b) => a + b) : 0;

      return localTeam;
    });

    return teamsWithScore;
  }

  /**
   * @deprecated use `delete`
   * @summary Delete a particular team by ID
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {Types.ObjectId | string} teamId
   *
   * @return {Promise<IERenaResponse>}
   */
  @Trace()
  public async deleteDeprecated(
    tournamentId: Types.ObjectId | string,
    teamId: Types.ObjectId | string
  ): Promise<IERenaResponse> {
    // Make sure teamId is valid
    if (!Types.ObjectId.isValid(teamId)) {
      throw Failure.BadRequest("invalid teamId");
    }

    const deletedTeam = await this.erenaTeams.deleteOne({ _id: teamId, tournament: tournamentId });
    return deletedTeam.deletedCount ? { result: "success" } : { result: "failure" };
  }

  /**
   * @deprecated use create
   * @summary Create a team
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} name
   * @param {string} type
   *
   * @return {Promise<IERenaTeamPopulated>}
   */
  @Trace()
  public async createDeprecated(
    tournamentId: Types.ObjectId | string,
    name: string,
    type: string = ERenaType.whiteLabel
  ): Promise<IERenaTeamPopulated> {
    // Make sure tournament ID is present
    if (!tournamentId) {
      throw Failure.BadRequest("tournamentId is not found");
    }

    // Make sure name is present
    if (!name) {
      throw Failure.BadRequest("name is required");
    }

    const doc = <IERenaTeam>{
      name,
      type,
      isActive: true,
      tournament: tournamentId
    };

    const newTeam = await this.erenaTeams.create(doc);

    const populatedTeam: IERenaTeamPopulated = <IERenaTeamPopulated>{ ...newTeam.toObject(), players: [] };
    return populatedTeam;
  }
}
