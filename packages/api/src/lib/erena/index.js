/**
 * NOTE: THIS SERVICE IS DEPRECATED IN FAVOR OF erena.service
 */

const _ = require("lodash");
const Airtable = require("airtable");
const { Logger } = require("@efuse/logger");
const parentLogger = Logger.create();

const { EFuseAnalyticsUtil } = require("../analytics.util");
const { ERenaTournament, ERenaPlayer, ERenaTeam } = require("../../backend/models/erena");

const logger = parentLogger.child({ lib: "erena" });

Airtable.configure({ endpointUrl: "https://api.airtable.com", apiKey: "keyof8oaF4Twsqhlb" });

// Global eRena Events table
// https://airtable.com/tblhnwi6PkAMXKfsU/viwHQNqcNNgaH4z3Q?blocks=hide
const GLOBAL_ERENA_AIRTABLE_ID = "appoywzylKIdmh9q1";

const { ERenaService } = require("./erena.service");
const erenaService = new ERenaService();

function getTournaments() {
  return ERenaTournament.find().cache(5);
}

const getAirtableDatav2 = async (baseName, viewName, baseId) => {
  try {
    const base = Airtable.base(baseId);
    const results = await base(baseName).select({ view: viewName }).all();

    return results;
  } catch (error) {
    logger.error(`[${baseId} / ${baseName} / ${viewName}] Error getting results from base: ${error.message}`);
    return [];
  }
};

const getTeamByAirtableId = (airtableId) => {
  return ERenaTeam.findOne({ airtableId });
};

const ranked = (data) => {
  return data.map((incoming, i) => {
    const item = incoming;

    if (i > 0) {
      // Get our previous list item
      const prevItem = data[i - 1];

      if ((prevItem.stats.kills || prevItem.stats.value) === (item.stats.kills || item.stats.value)) {
        // Same score = same rank
        item.rank = prevItem.rank;
      } else {
        // Not the same score, give em the current iterated index + 1

        item.rank = i + 1;
      }
    } else {
      // First item takes the rank 1 spot
      item.rank = 1;
    }
    return item;
  });
};

const getAirtables = async () => {
  const records = await getAirtableDatav2("eRena Events", "Grid view", GLOBAL_ERENA_AIRTABLE_ID);

  // todo: investigate
  // @ts-ignore
  let appIds = records.map((record) => {
    if (record.get("Status") === "Active") {
      return record.get("Airtable Base #");
    }
  });

  appIds = appIds.filter((appId) => {
    return appId;
  });

  return appIds;
};

const getAirtableTournaments = async () => {
  const records = await getAirtableDatav2("eRena Events", "Grid view", GLOBAL_ERENA_AIRTABLE_ID);
  if (records.length === 0) {
    const msg = "Error hitting airtable API";
    logger.error(msg);
    throw new Error(msg);
  }
  return records;
};

const getTeams = async () => {
  return ERenaTeam.find();
};

const pruneRemovedPlayers = async () => {
  logger.info("Pruning players that are not assigned teams/tournaments");

  const players = await ERenaPlayer.find();

  if (players !== null && players !== undefined) {
    players.forEach(async (player) => {
      if (player.type && player.type === "EFUSE") {
        logger.info("Skipped");
      } else if (!player.tournament) {
        logger.info("No tournament found for player.  Removing");

        player.delete();

        EFuseAnalyticsUtil.incrementMetric("erena.players.delete", 1, [
          // todo: investigate
          // @ts-ignore
          `tournamentName:${player.tournament.tournamentName}`
        ]);
      } else {
        const tournament = await ERenaTournament.findOne({ _id: player.tournament }).cache(5);

        try {
          const base = Airtable.base(tournament.airtableBaseId);
          // todo: investigate
          // @ts-ignore
          await base("Players").find(player.airtableId);
        } catch {
          logger.info({ player }, "Player not found in airtable. Removing.");

          player.delete();

          const name = tournament ? tournament.tournamentName : "invalid-tournament";
          EFuseAnalyticsUtil.incrementMetric("erena.players.delete", 1, [`tournamentName:${name}`]);
        }
      }
    });
  }
};

const pruneTeams = async () => {
  const teams = await getTeams();
  teams.forEach(async (team) => {
    if (team.type && team.type === "EFUSE") {
      return;
    }
    if (!team.tournament) {
      logger.info("No tournament found for team.  Skipping");
    }
    const tournament = await ERenaTournament.findOne({ _id: team.tournament });
    if (!tournament) {
      return;
    }
    // todo: investigate
    // @ts-ignore
    const base = Airtable.base(tournament.airtableBaseId);
    try {
      // todo: investigate
      // @ts-ignore
      const result = await base("Teams").find(team.airtableId);
    } catch {
      logger.info(`[${tournament.tournamentName}/${team.name}] Team not found in airtable. Removing.`);
      EFuseAnalyticsUtil.incrementMetric("erena.team.delete", 1, [`tournamentName:${tournament.tournamentName}`]);
      team.delete();
    }
  });
};

const pruneTournaments = async () => {
  const mongoDBTournaments = await getTournaments();
  const airtableTournaments = await getAirtableTournaments();
  if (airtableTournaments.length === 0) {
    logger.warn("No events returned from airtable.  Not proceeding.");
    return;
  }
  mongoDBTournaments.forEach((tournament) => {
    if (tournament.type && tournament.type === "EFUSE") {
      return;
    }

    const airtable = airtableTournaments.filter((x) => {
      return x.get("Airtable Base #") === tournament.airtableBaseId;
    });

    if (airtable.length === 0) {
      logger.info(
        `[${tournament._id} / ${tournament.airtableBaseId}] Airtable record ID doesnt exist in global table.  Removing tournament from MongoDB`
      );
      EFuseAnalyticsUtil.incrementMetric("erena.tournament.delete", 1, [
        `airtableId:${tournament.airtableBaseId}`,
        `tournamentId:${tournament.tournamentId}`
      ]);
      tournament.delete();
    }
  });
};

const syncPlayersFromAirtable = async () => {
  const airtables = await getAirtables();
  airtables.forEach(async (airtableBaseId) => {
    const records = await getAirtableDatav2("Players", "Player Leaderboard", airtableBaseId);
    const promises = records.map(updatePlayerStats);
    const results = await Promise.all(promises);
    // todo: investigate
    // @ts-ignore
    const players = results.filter((x) => x).map((result) => result.name);

    return results;
  });
};

const syncTeamsFromAirtable = async () => {
  const airtables = await getAirtables();

  airtables.forEach(async (airtableBaseId) => {
    const teams = await getAirtableDatav2("Teams", "Team Leaderboard", airtableBaseId);
    const promises = teams.map(updateTeamStats);
    const results = await Promise.all(promises);
    const filtered = results.filter((x) => x);
  });
};

const syncTournamentsFromAirtable = async () => {
  const records = await getAirtableDatav2("eRena Events", "Grid view", GLOBAL_ERENA_AIRTABLE_ID);

  try {
    records.forEach(async (record) => {
      const baseId = record.get("Airtable Base #");
      const tournamentId = record.get("Tournament ID");
      const name = record.get("Name");

      let localLogger = parentLogger;
      if (!tournamentId) {
        logger.warn(`[${baseId} / ${name}] Missing Tournament ID in global table.  Skipping.`);
        return;
      }

      if (!baseId) {
        logger.warn(`[${name}] Missing tournament Airtable Base ID.  Skipping.`);
        return;
      }
      // todo: investigate
      // @ts-ignore
      let tournament = await ERenaTournament.findOne({ $or: [{ airtableBaseId: baseId }, { tournamentId }] }).cache(5);

      if (!tournament) {
        logger.info(`Tournament not found: ${baseId}`);
        logger.info(`Creating new tournament with airtableBaseId: ${baseId}`);

        tournament = new ERenaTournament({ airtableBaseId: baseId, tournamentId });

        EFuseAnalyticsUtil.incrementMetric("erena.tournament.create", 1, [
          `airtableBaseId:${baseId}`,
          `tournamentName:${name}`
        ]);
      }

      localLogger = parentLogger.child({
        airtableBaseId: tournament.airtableBaseId,
        tournamentId: tournament.tournamentId,
        "tournament.lastAirtableModificationTime": tournament.lastAirtableModificationTime,
        lastAirtableModificationTime: record.get("lastAirtableModificationTime")
      });

      if (tournament.lastAirtableModificationTime === record.get("lastAirtableModificationTime")) {
        return;
      }

      const extensionNamespace = record.get("Extension Namespace");

      tournament.status = record.get("Status");
      tournament.slug = record.get("URL Slug");
      tournament.extensionLinkoutUrl = record.get("Extension Linkout URL");
      tournament.landingPageUrl = record.get("Extension Linkout URL");
      tournament.brandLogoUrl = record.get("Brand Logo URL");
      tournament.twitchChannel = record.get("Twitch Channel");
      tournament.imageUrl1 = record.get("Advertisement 1 URL");
      tournament.imageUrl2 = record.get("Advertisement 2 URL");
      tournament.imageUrl3 = record.get("Advertisement 3 URL");
      tournament.imageUrl4 = record.get("Advertisement 4 URL");
      tournament.imageUrl5 = record.get("Advertisement 5 URL");
      tournament.imageUrl6 = record.get("Advertisement 6 URL");
      tournament.imageLink1 = record.get("Advertisement 1 Link");
      tournament.imageLink2 = record.get("Advertisement 2 Link");
      tournament.imageLink3 = record.get("Advertisement 3 Link");
      tournament.imageLink4 = record.get("Advertisement 4 Link");
      tournament.imageLink5 = record.get("Advertisement 5 Link");
      tournament.imageLink6 = record.get("Advertisement 6 Link");
      tournament.rulesMarkdown = record.get("Rules");
      tournament.websiteUrl = record.get("Website URL");
      tournament.organization = record.get("eFuse Organization ID");
      tournament.tournamentDescription = record.get("Tournament Description");
      tournament.challongeLeaderboard = record.get("challongeLeaderboard");
      tournament.primaryColor = record.get("primaryColor");
      tournament.secondaryColor = record.get("secondaryColor");
      tournament.backgroundImageUrl = record.get("backgroundImageUrl");
      tournament.lastAirtableModificationTime = record.get("lastAirtableModificationTime");
      tournament.extensionPointer = record.get("Extension Pointer") || false;
      // todo: investigate
      // @ts-ignore
      tournament.extensionNamespace = extensionNamespace ? extensionNamespace.toLowerCase() : "erena";
      tournament.airtableId = record.id;
      tournament.airtableBaseId = baseId;
      tournament.tournamentId = tournamentId;
      tournament.tournamentName = name;
      tournament.totalTeams = record.get("# Teams");
      tournament.totalRounds = record.get("# Rounds");
      tournament.statName = record.get("Stat Name");

      logger.info({ tournament }, "Finished syncing tournament from global airtable");

      EFuseAnalyticsUtil.incrementMetric("erena.tournament.sync", 1, [`tournamentName:${tournamentId}`]);

      tournament.save();
    });
  } catch (error) {
    logger.error("An error syncing tournaments from airtable", error);
  }
};

const updatePlayerRanks = async (tournamentId) => {
  const teams = ["keempark-wzw-week4", "fandom-legends-week1"];
  const promises = teams.map(async (team) => {
    // todo: investigate
    // @ts-ignore
    const players = await erenaService.getPlayerLeaderboard(team);
    const response = ranked(players);
    response.forEach((player) => player.save());
    return response;
  });
  const resolved = await Promise.all(promises);

  return [];
};

const updatePlayerStats = async (record) => {
  const name = record.get("Player");
  const status = record.get("Status");
  const totalKills = record.get("Total Kills");

  const airtableBaseId = record._table._base._id;
  const tournament = await ERenaTournament.findOne({ airtableBaseId }).cache(5);
  if (!tournament) {
    logger.info({ airtableBaseId, name, record }, "Tournament not found.  Skipping");
    return;
  }
  const teamId = record.get("Team")[0];
  const team = await getTeamByAirtableId(teamId);

  // todo: investigate
  // @ts-ignore
  const player = await ERenaPlayer.findOneAndUpdate(
    { airtableId: record.id },
    {
      airtableId: record.id,
      airtableBaseId,
      tournament,
      name,
      team,
      isActive: status === "Active",
      stats: { kills: Number(totalKills), value: Number(totalKills), name: tournament.statName || "Points" }
    },
    { upsert: true }
  );

  EFuseAnalyticsUtil.incrementMetric("erena.players.sync", 1, [
    `tournamentName:${tournament.tournamentName}`,

    // todo: investigate
    // @ts-ignore
    `teamName:${team.name}`
  ]);

  return player;
};

const updateTeamStats = async (record) => {
  const recordName = record.get("Name");
  const name = record.get("Leaderboard Name");
  const kills = record.get("Kills");
  const status = record.get("Status");

  const airtableBaseId = record._table._base._id;
  const tournament = await ERenaTournament.findOne({ airtableBaseId }).cache(5);

  if (_.isEmpty(tournament)) {
    logger.info(`[${airtableBaseId} / ${name}] Could not find tournament with airtableBaseId.  Skipping.`);
    return;
  }

  const identifier = `${tournament.tournamentId} / ${tournament.tournamentName}`;
  const updated = await ERenaTeam.findOneAndUpdate(
    { airtableId: record.id },
    {
      airtableId: record.id,
      tournament,
      name,
      isActive: status === "Active",
      lastAirtableModificationTime: record.get("lastAirtableModificationTime"),
      stats: { kills: Number(kills), value: Number(kills), name: tournament.statName || "Points" }
    },
    { upsert: true }
  );

  EFuseAnalyticsUtil.incrementMetric("erena.team.sync", 1, [
    `tournamentName:${tournament.tournamentName}`,
    `teamName:${recordName}`
  ]);

  if (updated === null) {
    logger.info(`[${identifier}] Inserting new team into the database: ${recordName}`);
  } else {
    return name;
  }
};

module.exports.getAirtableDatav2 = getAirtableDatav2;
module.exports.getAirtables = getAirtables;
module.exports.getAirtableTournaments = getAirtableTournaments;
module.exports.getTeamByAirtableId = getTeamByAirtableId;
module.exports.getTeams = getTeams;
module.exports.pruneRemovedPlayers = pruneRemovedPlayers;
module.exports.pruneTeams = pruneTeams;
module.exports.pruneTournaments = pruneTournaments;
module.exports.ranked = ranked;
module.exports.syncPlayersFromAirtable = syncPlayersFromAirtable;
module.exports.syncTeamsFromAirtable = syncTeamsFromAirtable;
module.exports.syncTournamentsFromAirtable = syncTournamentsFromAirtable;
module.exports.updatePlayerRanks = updatePlayerRanks;
module.exports.updatePlayerStats = updatePlayerStats;
module.exports.updateTeamStats = updateTeamStats;
