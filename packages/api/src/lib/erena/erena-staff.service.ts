import { Failure } from "@efuse/contracts";
import { cloneDeep } from "lodash";
import { Types, QueryOptions, FilterQuery } from "mongoose";

import { ERenaRoles } from "../../backend/interfaces/erena/erena-enums";
import { IERenaStaff } from "../../backend/interfaces/erena/erena-staff";
import { ERenaStaff, ERenaStaffModel } from "../../backend/models/erena/erena-staff";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("erena-staff.service")
export class ERenaStaffService extends BaseService {
  private readonly model: ERenaStaffModel<IERenaStaff> = ERenaStaff;

  /**
   * Create Staff
   *
   * @param {IERenaStaff} item The staff object
   *
   * @returns {Promise<IERenaStaff>} The created staff object with id
   *
   * @memberof ERenaStaffService
   */
  @Trace()
  public async create(item: Partial<IERenaStaff>): Promise<IERenaStaff> {
    try {
      const localItem = cloneDeep(item);
      if (!localItem) {
        throw Failure.BadRequest("Unable to create item, missing 'item'");
      }

      const validationErrors = this.validateIncomingFields(<IERenaStaff>item);
      if (validationErrors.length > 0) {
        throw Failure.BadRequest(validationErrors.join(" | "));
      }

      const doc = <IERenaStaff>localItem;
      const result: IERenaStaff = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *  Find a staff based on the its ID.
   *
   * @param {string} id - The staff id
   *
   * @returns {Promise<IERenaStaff} The eRena staff corresponding to the ID
   *
   * @memberof ERenaStaffService
   */
  @Trace()
  public async findById(id: string | Types.ObjectId): Promise<IERenaStaff> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to find item, missing 'id'");
      }

      const identifier: Types.ObjectId = typeof id === "string" ? Types.ObjectId(id) : id;

      const item: IERenaStaff | null = await this.model.findById(identifier);

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   *  Find staff using query
   *
   * @param {FilterQuery<IERenaStaff>} conditions: The search query
   *
   * @returns {Promise<IERenaStaff} The eRena staff corresponding to the ID
   *
   * @memberof ERenaStaffService
   */
  public async find(filter: FilterQuery<IERenaStaff>): Promise<IERenaStaff[]>;
  public async find(filter: FilterQuery<IERenaStaff>, projection: any): Promise<IERenaStaff[]>;
  public async find(
    filter: FilterQuery<IERenaStaff>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaStaff[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IERenaStaff>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaStaff[]> {
    const erenaStaffs = await this.model.find(filter, projection, options);
    return erenaStaffs;
  }

  public async findOne(filter: FilterQuery<IERenaStaff>): Promise<IERenaStaff | null>;
  public async findOne(filter: FilterQuery<IERenaStaff>, projection: any): Promise<IERenaStaff | null>;
  public async findOne(
    filter: FilterQuery<IERenaStaff>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaStaff | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaStaff>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaStaff | null> {
    const staff = await this.model.findOne(filter, projection, options);
    return staff;
  }

  /**
   * Delete a staff
   *
   * @param {Promise<IERenaStaff>} id The id of the staff to delete
   *
   * @return {Promise<IERenaStaff>} The deleted staff
   *
   * @memberof ERenaStaffService
   */
  @Trace()
  public async delete(id: string): Promise<IERenaStaff> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to create item, missing 'id'");
      }

      const identifier: Types.ObjectId = Types.ObjectId(id);
      const item: IERenaStaff | null = await this.model.findByIdAndRemove(identifier);

      if (!item) {
        throw Failure.NotFound("Unable to find requested item");
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Update existing staff
   *
   * @param {string} id The id of the staff to be updated
   * @param {IERenaStaff} item The desired staff content
   *
   * @returns {Promise<IERenaStaff>} The updated staff
   *
   * @memberof ERenaStaffService
   */
  @Trace()
  public async update(id: string, item: Partial<IERenaStaff>): Promise<IERenaStaff> {
    try {
      if (!id) {
        throw Failure.BadRequest("Unable to update item, missing 'id'");
      }

      if (!item) {
        throw Failure.BadRequest("Unable to update item, missing 'item'");
      }

      if (!item.role || !Object.values(ERenaRoles).find((role) => role === item.role)) {
        throw Failure.BadRequest("Invalid role");
      }

      const identifier: Types.ObjectId = Types.ObjectId(id);
      const updated: IERenaStaff = <IERenaStaff>await this.model.findByIdAndUpdate(identifier, item, {
        new: true
      });

      if (!updated) {
        throw Failure.NotFound("Unable to update requested item.");
      }

      return updated;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Checks the passed entity to make sure it has all the required fields for creation
   * @param entity The entity the verify
   *
   * @returns string[] Array of errors
   */
  public validateIncomingFields(entity: IERenaStaff): string[] {
    const errors: string[] = [];
    if (!entity.user) {
      errors.push("Unable to create item, missing 'user'");
    }

    if (!entity.tournament) {
      errors.push("Unable to create item, missing 'tournament'");
    }

    if (!entity.role || !Object.values(ERenaRoles).find((role) => role === entity.role)) {
      errors.push("Unable to create item, missing 'role'");
    }

    return errors;
  }
}
