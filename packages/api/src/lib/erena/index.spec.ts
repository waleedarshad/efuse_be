it.todo("should have a test");

export {};

// ERenaTournament mocks for this query:
// ERenaTournament.findOne(query).cache(5);
// const ERenaTournament = require("../../../backend/models/ERenaTournament");

// jest.mock("../../backend/models/ERenaTournament", () => {
//   return {
//     ERenaTournament: {
//       findOne: jest.fn(() => {
//         return {
//           cache: jest.fn(() => "ERenaTournament")
//         };
//       })
//     }
//   };
// });

// jest.mock("../../async/config");
// // let erenaLib = require("./index.js");

// todo: reenable after refactor
// test("Test getTournamentByID regular tournament ID", async () => {
//   let fakeTournamentId = "someTournamentId";
//   let result = await erenaLib.getTournamentByID(fakeTournamentId);

//   // Make sure we returned a consistent tournament object as returned above
//   expect(result).toBe("ERenaTournament");

//   // Validte we sent proper query to ERenaTournament.findOne(query)
//   expect(ERenaTournament.ERenaTournament.findOne.mock.calls[0]).toMatchObject([
//     { $or: [{ tournamentId: fakeTournamentId }, { slug: fakeTournamentId }] }
//   ]);
// });

// todo: reenable after refactor
// test("Test getTournamentByID with __current__ tournament ID", async () => {
//   let fakeTournamentId = "someTournamentId";
//   let result = await erenaLib.getTournamentByID("__current__");

//   // Make sure we returned a consistent tournament object as returned above
//   expect(result).toBe("ERenaTournament");

//   // Validte we sent proper query to ERenaTournament.findOne(query)
//   expect(ERenaTournament.ERenaTournament.findOne.mock.calls[0]).toMatchObject([
//     { extensionNamespace: "erena", extensionPointer: true }
//   ]);
// });
