import { Types } from "mongoose";

import { Service, Trace } from "../decorators";
import { ERenaGameBaseService } from "./erena-game-base.service";
import { IERenaGame, IGame, IPopulatedERenaGame } from "../../backend/interfaces";
import { GameBaseService } from "../game/game-base.service";

@Service("erena-game.service")
export class ERenaGameService extends ERenaGameBaseService {
  private $gameBaseService: GameBaseService;

  constructor() {
    super();

    this.$gameBaseService = new GameBaseService();
  }

  /**
   * @summary Associate game with erena tournament
   *
   * @param {Types.ObjectId | string} tournamentId
   * @param {Types.ObjectId | string} gameId
   *
   * @returns {Promise<IERenaGame | null>}
   */
  @Trace()
  public async associateGame(
    tournamentId: Types.ObjectId | string,
    gameId: Types.ObjectId | string
  ): Promise<IERenaGame | null> {
    // Make sure gameId is valid & exists in our DB
    const gameParam = await this.$gameBaseService.validateGame(gameId);

    let erenaGame: IERenaGame | null = null;

    // Update existing game or create new one
    if (gameParam) {
      erenaGame = await this.update(
        { tournament: tournamentId },
        { tournament: tournamentId, game: gameParam, updatedAt: new Date() },
        { new: true, lean: true, setDefaultsOnInsert: true, upsert: true }
      );
    }

    return erenaGame;
  }

  /**
   * @summary Get game associated with the erena tournament
   *
   * @param {Types.ObjectId | string} tournamentId
   *
   * @returns {Promise<IGame | null>}
   */
  @Trace()
  public async getAssociatedGame(tournamentId: Types.ObjectId | string): Promise<IGame | null> {
    const erenaGame = (<unknown>(
      await this.findOne({ tournament: tournamentId }, {}, { populate: "game" })
    )) as IPopulatedERenaGame | null;

    if (erenaGame?.game) {
      return erenaGame.game;
    }

    return null;
  }
}
