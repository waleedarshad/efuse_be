import { Failure } from "@efuse/contracts";
import { FilterQuery, Types } from "mongoose";

import { IERenaTournament } from "../../backend/interfaces/erena";
import { ERenaRoles, ERenaTournamentOwnerKinds } from "../../backend/interfaces/erena/erena-enums";
import { IERenaStaff } from "../../backend/interfaces/erena/erena-staff";
import { ERenaTournament, ERenaTournamentModel } from "../../backend/models/erena";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { UserService } from "../users/user.service";
import { ERenaStaffService } from "./erena-staff.service";

/**
 * The eRena Share service should have no peer eRena service dependencies.
 * This service is meant to be imported into any of the erena services and provide
 * common functionality to prevent circular dependencies.
 */
@Service("erena-shared.service")
export class ERenaSharedService extends BaseService {
  private readonly erenaTournaments: ERenaTournamentModel<IERenaTournament> = ERenaTournament;

  private userService: UserService;
  private organizationACLService: OrganizationACLService;
  private eRenaStaffService: ERenaStaffService;

  constructor() {
    super();

    this.userService = new UserService();
    this.organizationACLService = new OrganizationACLService();
    this.eRenaStaffService = new ERenaStaffService();
  }

  /**
   * @summary Make sure user is owner of the tournament. Will automatically throw error if user does not have permission
   * @note This does not account for staff. Use the `isUserStaff` method first to check for staff members.
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   *
   * @returns {Promise<IERenaTournament>} The tournament that was validated
   */
  @Trace()
  public async verifyTournamentOwnershipAndRetrieve(
    userId: Types.ObjectId | string | undefined,
    tournamentIdOrSlug: string
  ): Promise<IERenaTournament> {
    // Fetch tournament
    const tournament = await this.getTournamentByIdOrSlug(tournamentIdOrSlug);

    // Verify presence
    if (!tournament) {
      throw Failure.UnprocessableEntity("[Verify and Retrieve] Tournament not found", { tournamentIdOrSlug });
    }

    if (!userId) {
      // return tournament if a user isn't passed, this is used in cron jobs
      return tournament;
    }

    const allowed = await this.verifyTournamentOwnership(userId, tournament);

    if (allowed) {
      return tournament;
    }

    throw Failure.Forbidden("You are not authorized to perform this action");
  }

  /**
   * @summary Make sure user is owner of the tournament.
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   *
   * @returns {Promise<IERenaTournament>} The tournament that was validated
   */
  @Trace()
  public async verifyTournamentOwnership(
    userId: Types.ObjectId | string | undefined,
    tournament: IERenaTournament
  ): Promise<boolean> {
    if (!tournament.ownerType) {
      this.logger?.info("ownerType is empty. Might be from airtable, Access Granted");

      return true;
    }

    // Fetching user object
    const user = await this.userService.findOne({ _id: userId }, { _id: 1, roles: 1 }, { lean: true });

    // Verify presence
    if (!user) {
      throw Failure.UnprocessableEntity("User not found!");
    }

    // check the staff collection
    if (await this.isUserStaff(<string | Types.ObjectId>userId, tournament._id, ERenaRoles.owner)) {
      return true;
    }

    // fallbacks, these are old checks for tournaments before staff was added
    // if the user is an admin, just allow it
    if (user.roles.includes("admin")) {
      this.logger?.info("currentUser is eFuse admin, Access Granted");

      return true;
    }

    // Destructure to get ownerType & owner
    const { owner, ownerType } = tournament;

    // when tournament owner is a user see if the current user is the owner
    if (ownerType === ERenaTournamentOwnerKinds.user && String(owner) === String(userId)) {
      return true;
    }

    // when tournament owner is org, see if the user has permission on the org
    if (ownerType === ERenaTournamentOwnerKinds.org) {
      // `authorizedToAccess` throws errors if the user doesn't have permissions. Catch it and return false
      try {
        if (await this.organizationACLService.authorizedToAccess(user, owner, OrganizationAccessTypes.ANY)) {
          return true;
        }
      } catch (error) {
        this.logger?.warn(error, "User is not authorized to access erena event.");
      }
    }

    return false;
  }

  /**
   * Check if the user is a staff member of specified tournament
   *
   * @param userId
   * @param role optional property to check for a specific role.
   */
  public async isUserStaff(
    userId: string | Types.ObjectId,
    tournamentId: string | Types.ObjectId,
    role?: ERenaRoles | undefined
  ): Promise<boolean> {
    const findQuery: FilterQuery<IERenaStaff> = { user: userId, tournament: tournamentId };

    if (role) {
      findQuery.role = role;
    }

    const staff = await this.eRenaStaffService.find(findQuery, {}, { lean: true });

    return staff && staff.length > 0;
  }

  /**
   * @summary Get tournament by ID or slug
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTournament | null>}
   */
  @Trace()
  public async getTournamentByIdOrSlug(
    tournamentIdOrSlug: string,
    namespace = "erena"
  ): Promise<IERenaTournament | null> {
    // Make sure tournament ID is present
    if (!tournamentIdOrSlug) {
      throw Failure.BadRequest("tournamentIdOrSlug is not found");
    }

    let query: FilterQuery<IERenaTournament> = {
      $or: [{ tournamentId: tournamentIdOrSlug }, { slug: tournamentIdOrSlug }]
    };

    if (this.isValidObjectId(tournamentIdOrSlug)) {
      query = {
        $or: [{ _id: tournamentIdOrSlug }, { tournamentId: tournamentIdOrSlug }, { slug: tournamentIdOrSlug }]
      };
    }

    if (tournamentIdOrSlug === "__current__") {
      query = { extensionPointer: true, extensionNamespace: namespace.toLowerCase() };
    }

    const tournament: IERenaTournament = await this.erenaTournaments.findOne(query).cache(5);

    if (tournament && !tournament.landingPageUrl) {
      tournament.landingPageUrl = `https://efuse.gg/e/${tournament.slug}?leaderboard=true`;
    }

    return tournament;
  }

  /**
   * @summary Verify user Permission on tournament
   *
   * @param {string | Types.ObjectId} userId
   * @param {string | Types.ObjectId} tournament
   */
  @Trace()
  public async isAuthorizedUser(
    userId: string | Types.ObjectId,
    tournament: IERenaTournament,
    role?: ERenaRoles | undefined
  ): Promise<boolean> {
    if (
      !(await this.isUserStaff(userId, tournament._id, role)) &&
      !(await this.verifyTournamentOwnership(userId, tournament))
    ) {
      throw Failure.Forbidden("You are not authorized to perform this action.");
    }
    return true;
  }
}
