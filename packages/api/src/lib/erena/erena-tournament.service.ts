import { Failure } from "@efuse/contracts";
import { isEmpty } from "lodash";
import { FilterQuery, isValidObjectId, PaginateResult, QueryOptions, Types } from "mongoose";
import { BaseService } from "../base/base.service";
import { buildAggregate } from "../opportunities";
import { OpportunityService } from "../opportunities/opportunity.service";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";
import { Service, Trace } from "../decorators";
import { UserService } from "../users/user.service";
import { ERenaTournament, ERenaTournamentModel } from "../../backend/models/erena";
import {
  IERenaResponse,
  IERenaTournament,
  IERenaTournamentFileUploader,
  IERenaTournamentPopulated
} from "../../backend/interfaces/erena";
import { IOpportunity } from "../../backend/interfaces/opportunity";
import { File } from "../../backend/interfaces/file";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { IUser } from "../../backend/interfaces/user";
import { ERenaSharedService } from "./erena-shared.service";
import { ERenaBracketService } from "./erena-bracket.service";
import {
  ERenaRoles,
  ERenaScoringKinds,
  ERenaTournamentOwnerKinds,
  ERenaTournamentStatus
} from "../../backend/interfaces/erena/erena-enums";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { ERenaStaffService } from "./erena-staff.service";
import { maxAndMinFieldFilterBuilder } from "../../backend/helpers/query.helper";
import { S3ERenaDirectory } from "../media/transcode-media.enum";

import { S3Service } from "../aws/s3.service";
@Service("erena-tournament.service")
export class ERenaTournamentService extends BaseService {
  private readonly erenaTournaments: ERenaTournamentModel<IERenaTournament> = ERenaTournament;

  private userService: UserService;
  private bracketService: ERenaBracketService;
  private opportunityService: OpportunityService;
  private erenaSharedService: ERenaSharedService;
  private organizationACLService: OrganizationACLService;
  private erenaStaffService: ERenaStaffService;
  private s3: S3Service;
  constructor() {
    super();

    this.userService = new UserService();
    this.opportunityService = new OpportunityService();
    this.erenaSharedService = new ERenaSharedService();
    this.bracketService = new ERenaBracketService();
    this.organizationACLService = new OrganizationACLService();
    this.erenaStaffService = new ERenaStaffService();
    this.s3 = new S3Service();
  }

  public async findOne(filter: FilterQuery<IERenaTournament>): Promise<IERenaTournament | null>;
  public async findOne(filter: FilterQuery<IERenaTournament>, projection: any): Promise<IERenaTournament | null>;
  public async findOne(
    filter: FilterQuery<IERenaTournament>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IERenaTournament | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IERenaTournament>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IERenaTournament | null> {
    const tournament = await this.erenaTournaments.findOne(filter, projection, options);
    return tournament;
  }

  /**
   * @summary Get tournament by ID
   *
   * @param {string} tournamentIdOrSlug
   * @param {string} namespace
   *
   * @return {Promise<IERenaTournament | null>}
   */
  @Trace()
  public async getByIdOrSlug(tournamentIdOrSlug: string, namespace = "erena"): Promise<IERenaTournament | null> {
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug, namespace);

    return tournament;
  }

  /**
   * @summary Get all tournaments
   *
   * @return {Promise<IERenaTournament[]>}
   */
  @Trace()
  public async getAll(
    page: number,
    limit: number,
    status?: ERenaTournamentStatus[],
    sortBy?: string,
    sortDirection?: number,
    startDateMin?: number,
    startDateMax?: number,
    endDateMin?: number,
    endDateMax?: number
  ): Promise<PaginateResult<IERenaTournament>> {
    if (sortDirection && sortDirection !== 1 && sortDirection !== -1) {
      throw Failure.BadRequest("Invalid sortDirection provided");
    }

    // Make sure page param is positive
    let pageParam = page;
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    let statusParam = status;
    if (!statusParam) {
      statusParam = [
        ERenaTournamentStatus.active,
        ERenaTournamentStatus.deleted,
        ERenaTournamentStatus.finished,
        ERenaTournamentStatus.scheduled
      ];
    }

    const fieldToSortBy = sortBy || "createdAt";
    const defaultSortDirection = -1;
    const sortDirectionParam = sortDirection || defaultSortDirection;

    const sortByParam = { [fieldToSortBy]: sortDirectionParam };

    let queries = {
      status: statusParam
    };

    // todo: investigate & fix
    // @ts-ignore
    const startDateMinParam: Date = GeneralHelper.convertUnixTimestampToISOString(startDateMin);

    // todo: investigate & fix
    // @ts-ignore
    const startDateMaxParam: Date = GeneralHelper.convertUnixTimestampToISOString(startDateMax);

    if (startDateMinParam || startDateMaxParam) {
      // @ts-ignore
      queries = { startDatetime: maxAndMinFieldFilterBuilder(startDateMinParam, startDateMaxParam), ...queries };
    }

    // todo: investigate & fix
    // @ts-ignore
    const endDateMinParam: Date = GeneralHelper.convertUnixTimestampToISOString(endDateMin);

    // todo: investigate & fix
    // @ts-ignore
    const endDateMaxParam: Date = GeneralHelper.convertUnixTimestampToISOString(endDateMax);

    if (endDateMinParam || endDateMaxParam) {
      // @ts-ignore
      queries = { endDatetime: maxAndMinFieldFilterBuilder(endDateMinParam, endDateMaxParam), ...queries };
    }

    const tournaments = await this.erenaTournaments.paginate(queries, {
      page: pageParam,
      limit: limitParam,
      sort: sortByParam,
      lean: true,
      populate: {
        path: "opportunity"
      }
    });

    return tournaments;
  }

  /**
   * @summary Get live or future tournaments
   *
   * @return {Promise<IERenaTournament | null>}
   */
  @Trace()
  public async getLiveAndScheduledEvents(
    page: number,
    limit: number,
    sortBy?: string,
    sortDirection?: number
  ): Promise<PaginateResult<IERenaTournament | null>> {
    if (sortDirection && sortDirection !== 1 && sortDirection !== -1) {
      throw Failure.BadRequest("Invalid sortDirection provided");
    }

    // Make sure page param is positive
    let pageParam = page;
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    const fieldToSortBy = sortBy || "createdAt";
    const defaultSortDirection = -1;
    const sortDirectionParam = sortDirection || defaultSortDirection;

    const sortByParam = { [fieldToSortBy]: sortDirectionParam };

    const currentTime = new Date(Date.now()).toISOString();

    const queries = {
      status: [ERenaTournamentStatus.active, ERenaTournamentStatus.scheduled],
      endDatetime: { $gte: currentTime }
    };

    const tournaments = await this.erenaTournaments.paginate(<never>queries, {
      page: pageParam,
      limit: limitParam,
      sort: sortByParam,
      lean: true,
      populate: {
        path: "opportunity"
      }
    });

    return tournaments;
  }

  /**
   * @summary Update particular tournament
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {Partial<IERenaTournament>} tournamentBody
   *
   * @return {Promise<IERenaTournamentPopulated>}
   */
  @Trace()
  public async update(
    tournamentIdOrSlug: string,
    tournamentBody: Partial<IERenaTournament>,
    files?: IERenaTournamentFileUploader
  ): Promise<IERenaTournamentPopulated> {
    const tournament = await this.getByIdOrSlug(tournamentIdOrSlug);
    if (!tournament) {
      throw Failure.NotFound("Unable to find tournament");
    }

    const updateValues = tournamentBody;

    if (files?.brandLogoUrl) {
      const logoImage: File = GeneralHelper.buildFileObject(files.brandLogoUrl);
      if (logoImage.url) {
        updateValues.brandLogoUrl = logoImage.url;
      }
    }

    if (files?.backgroundImageUrl) {
      const backgroundImage: File = GeneralHelper.buildFileObject(files.backgroundImageUrl);
      if (backgroundImage.url) {
        updateValues.backgroundImageUrl = backgroundImage.url;
      }
    }

    if (
      tournament?.brandLogoUrl &&
      updateValues?.brandLogoUrl === "" &&
      tournament?.brandLogoUrl !== updateValues?.brandLogoUrl
    ) {
      // Remove image from S3
      await this.deleteTournamentMedia(tournament?.brandLogoUrl);
    }

    if (
      tournament?.backgroundImageUrl &&
      updateValues?.backgroundImageUrl === "" &&
      tournament?.backgroundImageUrl !== updateValues?.backgroundImageUrl
    ) {
      // Remove image from S3
      await this.deleteTournamentMedia(tournament.backgroundImageUrl);
    }

    // if deselecting an opportunity, this is needed to handle it
    if (updateValues != null && updateValues.opportunity != null && !Types.ObjectId.isValid(updateValues.opportunity)) {
      updateValues.opportunity = undefined;
    }

    // Update tournament
    const updatedTournament: IERenaTournament | null = await this.erenaTournaments
      .findOneAndUpdate({ _id: <Types.ObjectId>tournament._id }, updateValues, { new: true })
      .lean();

    if (!updatedTournament) {
      throw Failure.UnprocessableEntity("[Tournament Update] Tournament not found!", {
        id: <Types.ObjectId>tournament._id
      });
    }

    const populatedTournament: IERenaTournamentPopulated = await this.fetchAssociatedObjects(updatedTournament);
    return populatedTournament;
  }

  /**
   * @summary Add or remove AD
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   * @param {{url: string}} image
   * @param {string} link
   * @param {number} index
   *
   * @return {Promise<IERenaTournamentPopulated>}
   */
  @Trace()
  public async addOrRemoveAd(
    tournament: IERenaTournament,
    userId: Types.ObjectId | string,
    index: number,
    required: boolean,
    imageURL?: string,
    link?: string
  ): Promise<IERenaTournamentPopulated> {
    if (required && (!imageURL || !link)) {
      throw Failure.BadRequest("imageURL & link are required");
    }

    const imageIndex = Number(index);
    if (imageIndex < 1 || imageIndex > 6 || Number.isNaN(imageIndex)) {
      throw Failure.BadRequest("Ad index must be between 1 & 6");
    }

    // Build Query
    const tournamentUpdateQuery: Partial<IERenaTournament> = <Partial<IERenaTournament>>{
      [`imageUrl${index}`]: imageURL,
      [`imageLink${index}`]: link
    };

    if (tournament[`imageUrl${index}`] && tournament[`imageUrl${index}`] !== imageURL) {
      // Remove image from S3

      const filename = tournament[`imageUrl${index}`].split("/").pop();
      const fileKey = `${S3ERenaDirectory.upload}/${filename}`;
      await this.s3.deleteMediaFromS3(fileKey);
    }

    const tournamentIdOrSlug = (<Types.ObjectId>tournament._id).toHexString();
    // Update & return
    const updatedTournament = await this.update(tournamentIdOrSlug, tournamentUpdateQuery);
    return updatedTournament;
  }

  /**
   * @summary Create tournament
   *
   * @param {IERenaTournament} tournamentParams
   *
   * @return {Promise<IERenaTournament>}
   */
  @Trace()
  public async create(
    tournamentParams: IERenaTournament,
    currentUserId?: string | Types.ObjectId
  ): Promise<IERenaTournament> {
    if (!tournamentParams) {
      throw Failure.BadRequest("Tournament params are not found");
    }

    const doc = tournamentParams;
    const tournament = await this.erenaTournaments.create(doc);

    if (currentUserId) {
      // create owner
      await this.erenaStaffService.create({
        user: currentUserId,
        tournament: <Types.ObjectId>tournament._id,
        role: ERenaRoles.owner
      });
    }

    // Create bracket, if necessary before returning tournament.
    // This guarantees there isn't a delay in tournament creation and the ability to edit the players/bracket
    if (tournament.bracketType === ERenaScoringKinds.bracket) {
      await this.bracketService.create(tournament.slug, tournamentParams.totalTeams);
    }

    return tournament;
  }

  @Trace()
  public async validateTournament(
    tournament: IERenaTournament,
    user: IUser
  ): Promise<{ errors: string[]; isValid: boolean }> {
    const errors: string[] = [];

    if (isEmpty(tournament.slug)) errors.push("Tournament slug is required.");

    if (isEmpty(tournament.tournamentId)) errors.push("Tournament id is required.");

    if (isEmpty(tournament.tournamentName)) errors.push("Tournament name is required.");

    if (isEmpty(tournament.tournamentDescription)) errors.push("Tournament description is required.");

    if (isEmpty(tournament.startDatetime)) errors.push("Tournament start date and time are required.");

    const slugCount = await ERenaTournament.find({ slug: tournament.slug });
    if (slugCount.length > 0) errors.push("Slug already taken.");

    const idCount = await ERenaTournament.find({
      tournamentId: tournament.tournamentId
    });
    if (idCount.length > 0) errors.push("Tournament id already taken.");

    if (tournament.ownerType === "organizations") {
      try {
        const result = await this.organizationACLService.authorizedToAccess(
          user,
          tournament.owner,
          OrganizationAccessTypes.ANY
        );
        if (!result.organization || !result.user) {
          errors.push("You are not authorized to perform this action!");
        }
      } catch (error) {
        errors.push(error);
      }
    } else if (tournament.ownerType === "users" && !user._id.equals(tournament.owner)) {
      errors.push("You are not authorized to perform this action!");
    } else if (!tournament.ownerType) {
      errors.push("Tournament ownerType is required.");
    }

    // Validate Opportunity param
    if (!isEmpty(tournament.opportunity)) {
      // Make sure opportunity ID is valid
      if (!isValidObjectId(tournament.opportunity)) {
        errors.push("Invalid opportunity ID");
      } else {
        // Make sure opportunity exists in our DB
        const opportunity = await this.opportunityService.findOne({
          _id: tournament.opportunity
        });

        if (!opportunity) {
          errors.push("Opportunity not found");
        } else if (opportunity.publishStatus !== "open" || opportunity.status !== "visible") {
          errors.push("Make sure opportunity is open & visible");
        }
      }
    }

    return {
      errors,
      isValid: isEmpty(errors)
    };
  }

  /**
   * @summary Append all associated objects
   *
   * @param {IERenaTournament} tournament
   *
   * @return {Promise<IERenaTournamentPopulated>}
   */
  @Trace()
  private async fetchAssociatedObjects(tournament: IERenaTournament): Promise<IERenaTournamentPopulated> {
    const populatedTournament: IERenaTournamentPopulated = <IERenaTournamentPopulated>{
      ...tournament,
      opportunity: undefined
    };

    if (tournament.opportunity) {
      // Lookup for opportunity
      const opportunity: IOpportunity | null = await this.opportunityService.lookupOpportunity(tournament.opportunity);
      if (opportunity) {
        populatedTournament.opportunity = opportunity;
      }
    }

    return populatedTournament;
  }

  /**
   * @deprecated Use the `update` method and set the status to "Deleted"
   * @summary Mark a particular tournament as deleted
   *
   * @param {string} tournamentIdOrSlug
   * @param {Types.ObjectId | string} userId
   *
   * @return {Promise<IERenaResponse>}
   */
  @Trace()
  public async markAsDeleted(tournamentIdOrSlug: string, userId: Types.ObjectId | string): Promise<IERenaResponse> {
    // Make sure user is admin or owner of the tournament
    const tournament = await this.verifyPermissionAndRetrieve(userId, tournamentIdOrSlug);
    tournament.status = ERenaTournamentStatus.deleted;
    await tournament.save();
    return { result: "Tournament deleted successfully" };
  }

  @Trace()
  public async moveToNextStatus(tournament: IERenaTournament): Promise<IERenaTournamentPopulated> {
    const tournamentBody: Partial<IERenaTournament> = {};

    if (!tournament) {
      throw Failure.BadRequest("Tournament was not valid.");
    }

    switch (tournament.status) {
      case ERenaTournamentStatus.active:
        tournamentBody.status = ERenaTournamentStatus.finished;
        break;
      case ERenaTournamentStatus.scheduled:
        tournamentBody.status = ERenaTournamentStatus.active;
        break;
      default:
      // Do nothing.
    }

    // This is going to cause a call to the DB just to retrieve the object we already have. We're
    // trading what should be a small performance hit in exchange for not duplicating code.
    //
    // TODO: We might want to look into eventually refactoring both functions to split out the code
    // that would be shared so we are neither duplicating code nor taking performance hits.
    return this.update(tournament._id, tournamentBody);
  }

  @Trace()
  public async getEventsToActivate(): Promise<IERenaTournament[]> {
    const currentTime = new Date();

    const queries = {
      status: [ERenaTournamentStatus.scheduled],
      startDatetime: { $lte: currentTime }
    };

    const tournaments = await this.erenaTournaments.find(queries, {}, { lean: true });

    return tournaments;
  }

  @Trace()
  public async getEventsToFinish(): Promise<IERenaTournament[]> {
    const currentTime = new Date();

    const queries = {
      status: [ERenaTournamentStatus.active],
      endDatetime: { $lte: currentTime }
    };

    const tournaments = await this.erenaTournaments.find(queries, {}, { lean: true });

    return tournaments;
  }

  /**
   * @deprecated Use OpportunityService.lookupOpportunity
   *
   * @summary Lookup for associated opportunity
   *
   * @param {Types.ObjectId | string} opportunityId
   *
   * @return {Promise<IOpportunity | null>}
   */
  @Trace()
  public async lookupOpportunity(opportunityId: Types.ObjectId | string): Promise<IOpportunity | null> {
    // Return if opportunityId is not found
    if (!opportunityId) {
      return null;
    }

    const opportunity = await this.opportunityService.findOne({ _id: opportunityId }, {}, { lean: true });

    // Return if opportunity if not found
    if (!opportunity) {
      return null;
    }

    // Build aggregate & return opportunity
    const aggregatedOpportunity = <IOpportunity>await buildAggregate(opportunity);
    return aggregatedOpportunity;
  }

  /**
   * @deprecated Use ERenaSharedService.verifyTournamentPermissionAndRetrieve
   * @summary Make sure user is owner of the tournament
   *
   * @param {Types.ObjectId | string} userId
   * @param {string} tournamentIdOrSlug
   *
   * @return {Promise<IERenaTournament>}
   */
  @Trace()
  public async verifyPermissionAndRetrieve(
    userId: Types.ObjectId | string,
    tournamentIdOrSlug: string
  ): Promise<IERenaTournament> {
    // Fetch tournament
    const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

    // Verify presence
    if (!tournament) {
      throw Failure.UnprocessableEntity("[Tournament Verify] Tournament not found", { tournamentIdOrSlug });
    }

    if (!tournament.ownerType) {
      this.logger?.info("ownerType is empty. Might be from airtable, Access Granted");
      return tournament;
    }

    // Fetching user object
    const user = await this.userService.findOne({ _id: userId }, { _id: 1, roles: 1 }, { lean: true });

    // Verify presence
    if (!user) {
      throw Failure.UnprocessableEntity("User not found!");
    }

    // Make sure to allow eFuse admin
    if (user.roles.includes("admin")) {
      this.logger?.info("currentUser is eFuse admin, Access Granted");
      return tournament;
    }

    // Destructure to get ownerType & owner
    const { owner, ownerType } = tournament;

    // When tournament owner is a user
    if (ownerType === ERenaTournamentOwnerKinds.user) {
      // Verify owner
      if (String(owner) !== String(userId)) {
        throw Failure.Forbidden("You are not authorized to perform this action");
      }
    }

    if (ownerType === "organizations") {
      await this.organizationACLService.authorizedToAccess(user, owner, OrganizationAccessTypes.ANY);
    }

    return tournament;
  }

  public static getOwnerType(tournament: IERenaTournament): ERenaTournamentOwnerKinds {
    let { ownerType } = tournament;

    ownerType = ownerType || (tournament.organization ? ERenaTournamentOwnerKinds.org : ERenaTournamentOwnerKinds.user);

    return ownerType;
  }

  /**
   * @summary Delete tournament media
   * @param url
   * @returns void
   *
   */
  @Trace()
  private async deleteTournamentMedia(url: string): Promise<void> {
    const filename = url.split("/").pop();
    const fileKey = `${S3ERenaDirectory.upload}/${filename}`;
    try {
      await this.s3.deleteMediaFromS3(fileKey);
    } catch {
      this.logger?.debug({ fileKey }, "Unable to delete media from s3");
    }
  }
}
