import { Failure } from "@efuse/contracts";
import { OwnerType } from "@efuse/entities";
import { Types } from "mongoose";
import { BaseService } from "../base";
import { Service } from "../decorators";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { OrganizationsService } from "../organizations/organizations.service";

type ObjectId = string | Types.ObjectId;

@Service("owner-permission.service")
export class OwnerPermissionService extends BaseService {
  private organizationAclService: OrganizationACLService;
  private organizationsService: OrganizationsService;

  constructor() {
    super();

    this.organizationAclService = new OrganizationACLService();
    this.organizationsService = new OrganizationsService();
  }

  /**
   * Checks if the user is allowed to perform actions for the entity or throw an error.
   */
  public async userAllowedToPerformActions(
    currentUserId: ObjectId,
    owner?: ObjectId,
    ownerType?: OwnerType,
    orgAccessType?: string[]
  ): Promise<void> {
    try {
      if (!ownerType || !owner) {
        throw Failure.BadRequest("Missing owner");
      }

      if (ownerType === OwnerType.ORGANIZATIONS) {
        await this.organizationAclService.authorizedToAccess(
          currentUserId,
          owner,
          orgAccessType ?? OrganizationAccessTypes.ANY
        );
      }

      if (ownerType === OwnerType.USERS && owner !== currentUserId) {
        throw Failure.Forbidden("User does not have permission to perform this action.");
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Checks if the user is allowed to perform actions for the any entity or throw an error.
   */
  public async userAllowedToPerformActionsForAny(
    currentUserId: ObjectId,
    owners: { owner?: ObjectId; ownerType?: OwnerType }[]
  ): Promise<void> {
    let foundAllowedOwner = false;
    for (const owner of owners) {
      try {
        await this.userAllowedToPerformActions(currentUserId, owner.owner, owner.ownerType);
        foundAllowedOwner = true;
      } catch {
        //noop
      }
    }

    if (!foundAllowedOwner) {
      throw this.handleError(Failure.Forbidden("User does not have permission to perform this action."));
    }
  }

  /**
   * Checks if the user is allowed to create a league for the entity or throw an error.
   */
  public async userAllowedToCreateLeagues(
    currentUserId: ObjectId,
    owner: ObjectId,
    ownerType: OwnerType
  ): Promise<void> {
    try {
      let canCreate = false;
      if (ownerType === OwnerType.USERS) {
        canCreate = await this.organizationsService.canUserCreateLeagues(currentUserId);
      }

      if (ownerType === OwnerType.ORGANIZATIONS) {
        canCreate = await this.organizationsService.canUserCreateLeaguesForOrg(currentUserId, owner);
      }

      if (!canCreate) {
        throw Failure.Forbidden("User does not have permission to perform this action.");
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
