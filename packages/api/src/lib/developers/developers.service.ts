import { DocumentType } from "@typegoose/typegoose";
import { DeveloperModel, Developer } from "../../backend/models/developer.model";
import { BaseModelService } from "../base";
import { Service } from "../decorators";

@Service("developers.service")
export class DeveloperService extends BaseModelService<DocumentType<Developer>> {
  constructor() {
    super(DeveloperModel);
  }
}
