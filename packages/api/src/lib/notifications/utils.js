const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { FeatureFlagService } = require("../../lib/feature-flags/feature-flag.service");
const { UserSettingsService } = require("../user-settings/user-settings");

const userSettingsService = new UserSettingsService();

/**
 * @summary Check whether permission is enabled or not
 *
 * @param {string} permission
 * @param {Object} notificationSettings
 *
 * @returns {boolean}
 */
const permissionEnabled = (permission, notificationSettings) => {
  // Fetching value from nested object i.e postNotifications.newHype

  return permission.split(".").reduce((accumulator, value) => accumulator[value], notificationSettings);
};

const getNotificationTarget = async (user, permission) => {
  let target = "";

  // if there is no permission we want to send to all
  // this would be for high signal notifications like tournament staff
  if (!permission) {
    return "all";
  }

  // Wrapping in try/catch to avoid unnecessary errors due to old settings stored in user collection
  try {
    // Get user's settings
    const userSettings = await userSettingsService.get(user._id, { notifications: 1 });
    const notificationSettings = userSettings.notifications;

    const emailEnabled = permissionEnabled(permission, notificationSettings.email);
    const pushEnabled = permissionEnabled(permission, notificationSettings.push);

    // Set Target
    if (emailEnabled && pushEnabled) {
      target = "all";
    } else if (emailEnabled) {
      target = "web";
    } else if (pushEnabled) {
      target = "mobile";
    }
  } catch (error) {
    logger.error(error, "Error checking notification permissions");
  }

  return target;
};

const emailNotificationsEnabled = async () => {
  const enableEmailNotifications = await FeatureFlagService.hasFeature("enable_email_notifications");
  if (!enableEmailNotifications) {
    logger.info("Email notification is disabled using enable_email_notifications flag in BT");
  }
  return enableEmailNotifications;
};

module.exports.emailNotificationsEnabled = emailNotificationsEnabled;
module.exports.getNotificationTarget = getNotificationTarget;
module.exports.permissionEnabled = permissionEnabled;
