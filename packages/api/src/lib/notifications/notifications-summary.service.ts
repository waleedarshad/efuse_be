import { isEmpty } from "lodash";
import { Model, Types, QueryCursor } from "mongoose";
import moment from "moment";
import { Queue } from "bull";

import { MONGO_PREFERENCE_TAGS } from "../../backend/config/constants";
import { BaseService } from "../base/base.service";
import { Comment } from "../../backend/models/comment";
import { EFuseAnalyticsUtil } from "../analytics.util";
import { Feed } from "../../backend/models/Feed";
import { Friend } from "../../backend/models/friend";
import { GeneralHelper } from "../../backend/helpers/general.helper";
import { HypeModel } from "../../backend/models/hype/hype.model";
import { IComment } from "../../backend/interfaces/comment";
import { IFeed } from "../../backend/interfaces/feed";
import { IFriend } from "../../backend/interfaces/friend";
import { IUser } from "../../backend/interfaces/user";
import { IUserStats } from "../../backend/interfaces/user-stats";
import { sendEmail } from "../email";
import { Service, Trace } from "../decorators";
import { UserSettings, UserSettingsModel } from "../../backend/models/user-settings/user-settings.model";

import Config from "../../async/config";
import UserLib from "../users";
import { HypeEnum } from "@efuse/entities";

interface ISummaryEmailData {
  totalComments: number;
  totalHypes: number;
  totalEngagements: number;
  totalPostViews: number;
  totalHypeScore: number;
  currentStreak: number;
  totalFollowers: number;
}

interface IGroupedUserSettings {
  _id: Types.ObjectId;
}

@Service("notifications-summary.service")
export class NotificationsSummaryService extends BaseService {
  protected readonly $userSettings = UserSettingsModel;
  private feeds: Model<IFeed> = Feed;
  private comments: Model<IComment> = Comment;
  private hypes = HypeModel;
  private friends: Model<IFriend> = Friend;

  private dispatchSummaryEmailQueue: Queue;

  constructor() {
    super();

    this.dispatchSummaryEmailQueue = Config.initQueue("dispatchSummaryEmail") as Queue;
  }

  /**
   * @summary Cronjob that adds all the users to queue who have opted for weekly summary
   */
  @Trace({ name: "WeeklySummaryCron", service: "notifications-summary.service" })
  public static async WeeklySummaryCron(): Promise<void> {
    const service = new NotificationsSummaryService();
    try {
      // Get those users who have opted-in for weekly Summary notification

      const userSettingsCursor: QueryCursor<UserSettings> = service.$userSettings
        .aggregate([
          { $match: { "notifications.email.summaryNotifications.weeklySummary": true } },
          { $group: { _id: "$user" } }
        ])
        .cursor({ batchSize: 100 })
        .exec();

      let totalUsers = 0;

      // Loop through each user & add to queue to dispatch notification
      for (
        let userSetting: IGroupedUserSettings = (await userSettingsCursor.next()) as IGroupedUserSettings;
        userSetting !== null;
        userSetting = (await userSettingsCursor.next()) as IGroupedUserSettings
      ) {
        // Add to queue to dispatch notification
        service.logger?.info(`Adding user ${String(userSetting._id)} to queue`);
        await service.dispatchSummaryEmailAsync(userSetting._id);
        totalUsers += 1;
      }

      EFuseAnalyticsUtil.incrementGauge("weeklySummary.cron.usersAddedToQueue", totalUsers);
      service.logger?.info(`Added ${totalUsers} users to queue to dispatch weekly summary`);
      EFuseAnalyticsUtil.incrementMetric("weeklySummary.cron.success");
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("weeklySummary.cron.failure");
      service.logger?.error(error, "Error while queing up weekly notification summary");
    }
  }

  /**
   * @summary Lookup user, feeds, comments, likes & send weekly summary email
   *
   * @param {ObjectId} userId - userId whose stats are to be fetched
   */
  @Trace()
  public async dispatchSummaryEmail(userId: Types.ObjectId): Promise<void | undefined> {
    try {
      // Lookup User
      // todo: investigate & fix
      // @ts-ignore
      const user = (await UserLib.getUserById(userId, {
        populate: ["stats"],
        dailyStreak: true,
        projection: { email: 1 }
      })) as IUser;

      // Check user presence
      if (!user) {
        this.logger?.info({ userId }, "User not found, May be user is deleted");
        return;
      }

      // Check user is active
      if (user.status === "Blocked") {
        this.logger?.info({ userId }, "User has been blocked");
        return;
      }

      // Create 1 week window
      const today = new Date();
      const weekAgo: Date = moment().subtract(7, "d").startOf("d").toDate();

      // Timestamp window to make sure we only get one week result
      const timestampWindow = [{ createdAt: { $gte: weekAgo } }, { createdAt: { $lt: today } }];

      // Lookup feeds & comments Using Promise.all to resolve promises concurrently
      const resolvedPromises = await Promise.all([
        // resolvedPromises[0] = Feed
        this.feeds
          .find({
            user: user._id
          })
          .select("_id")
          .lean()
          .read("secondary", MONGO_PREFERENCE_TAGS),

        // resolvedPromises[1] = Comment
        this.comments
          .find({
            user: user._id
          })
          .select("_id")
          .lean()
          .read("secondary", MONGO_PREFERENCE_TAGS)
      ]);

      // User's Feeds
      const feed = resolvedPromises[0] as IFeed[];
      const feedIds = feed.map((f) => <Types.ObjectId>f._id);

      // User's comments
      const comments = resolvedPromises[1] as IComment[];
      const commentIds = comments.map((c) => <Types.ObjectId>c._id);

      // Check if user has no feeds & comments
      if (feed.length === 0 && comments.length === 0) {
        this.logger?.info({ userId }, "No activity found for user");
        return;
      }

      // Convert userId to objectId
      const userObjectId: Types.ObjectId = GeneralHelper.getObjectId(user._id);

      // Get all the comments, hypes & hypes received by user
      const resolvedCountPromises = await Promise.all([
        // resolvedCountPromises[0] = totalComments
        this.comments
          .find({
            user: { $ne: userObjectId },
            commentable: { $in: feedIds },
            commentableType: "feeds",
            $and: timestampWindow
          })
          .countDocuments()
          .read("secondary", MONGO_PREFERENCE_TAGS),

        // resolvedCountPromises[1] = totalCommentLikes,

        this.hypes
          .find({
            user: { $ne: userObjectId },
            objId: { $in: commentIds },
            objType: HypeEnum.COMMENT,
            $and: timestampWindow
          })
          .countDocuments()
          .read("secondary", MONGO_PREFERENCE_TAGS),

        // resolvedCountPromises[2] = totalHypesCount
        this.hypes
          .aggregate<{ _id: null; totalHypes: number }>([
            {
              $match: { $and: timestampWindow, user: { $ne: userObjectId }, objType: "feeds", objId: { $in: feedIds } }
            },
            { $group: { _id: null, totalHypes: { $sum: "$hypeCount" } } }
          ])
          .read("secondary", MONGO_PREFERENCE_TAGS),

        // resolvedCountPromises[3] = totalFollowers
        this.friends
          .find({ followee: userObjectId, $and: timestampWindow })
          .countDocuments()
          .read("secondary", MONGO_PREFERENCE_TAGS)
      ]);

      // Get Total comments against feed
      const totalComments = resolvedCountPromises[0];

      // Get total likes received on comments
      let totalHypes = resolvedCountPromises[1];

      // Get total hypes received on posts
      const feedHypesReceived = resolvedCountPromises[2];
      if (feedHypesReceived.length > 0) {
        totalHypes += feedHypesReceived[0].totalHypes;
      }

      // Get totalFollowers
      const totalFollowers = resolvedCountPromises[3];

      // Check if user has no hypes & no comments
      if (totalHypes === 0 && totalComments === 0 && totalFollowers === 0) {
        this.logger?.info({ userId }, "No activity found for user");
        return;
      }

      // Build streak
      let activeStreakDuration = 0;
      if (!isEmpty(user.streaks.daily)) {
        activeStreakDuration = user.streaks.daily.streakDuration;
      }

      // Get userStats
      const userStats = user.stats as IUserStats;

      // Build Email Data
      const emailData: ISummaryEmailData = {
        totalComments, // Total comments received by user weekAgo
        totalHypes, // Total hypes received by user weekAgo
        totalEngagements: userStats?.totalEngagements || 0, // Total lifetime engagements
        totalPostViews: userStats?.postViews || 0, // Total lifetime postViews
        totalHypeScore: userStats?.hypeScore || 0, // Total lifetime hypeScore
        currentStreak: activeStreakDuration, // Current active streak duration
        totalFollowers // Total followers in last week
      };

      this.logger?.info({ email: user.email, emailData }, "Sending weekly notification summary email to user");

      // Send Email
      await sendEmail(user.email, "d-a7548268278d4af3bd48db3ce6b463bc", emailData);
      EFuseAnalyticsUtil.incrementMetric("weekly.job.success");
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("weeklySummary.job.failure");
      this.logger?.error(error, `Error while dispatching summary email to user ${String(userId)}`);
    }
  }

  /**
   * @summary Asynchronously Lookup user, feeds, comments, hypes & send weekly summary email using Bull Queue
   *
   * @param {ObjectId} userId - User's ID to whom summary email is to be sent
   */
  @Trace()
  public async dispatchSummaryEmailAsync(userId: Types.ObjectId): Promise<void> {
    await this.dispatchSummaryEmailQueue.add({ userId });
  }

  /**
   * @summary Async processor initializers
   */
  @Trace({ name: "InitAsyncProcessors", service: "notifications-summary.service" })
  public static InitAsyncProcessors(): void {
    const notificationsSummaryService = new NotificationsSummaryService();

    notificationsSummaryService.dispatchSummaryEmailQueue
      .process((job: { data: { userId: unknown } }) =>
        notificationsSummaryService.dispatchSummaryEmail(job.data.userId as Types.ObjectId)
      )
      .catch((error) => {
        notificationsSummaryService.logger?.error(error, "Error while processing queue dispatchSummaryEmailQueue");
      });
  }
}
