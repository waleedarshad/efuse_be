const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/notifications/rate-limit.js" });
const moment = require("moment");
const { ObjectId } = require("mongodb");
const { NotificationLog } = require("../../backend/models/NotificationLog");

/**
 * @summary Apply rate limit to notification
 *
 * @param {ObjectId} userId - ID of recipient
 * @param {String} type - i.e push, in-app or email
 * @param {Number} allowedLimit - Number of notifications a user can receiver
 * @param {Number} interval - Duration e.g 1 or 2
 * @param {String} intervalUnit - Unit to create window e.g 'm' for minutes, 'h' for hours, 's' for seconds
 * @return {Promise<any>} e.g true of false
 */
const rateLimit = async (userId, type, allowedLimit = 1, interval = 1, intervalUnit = "m") => {
  return new Promise(async (resolve, reject) => {
    try {
      // todo: fix
      // @ts-ignore
      const duration = new Date(moment().subtract(interval, intervalUnit));
      const logs = await NotificationLog.find({
        user: userId,
        createdAt: { $gt: duration },
        type,
        status: "SENT"
      })
        .select("_id")
        .lean();
      if (!logs) {
        return resolve(true);
      }
      return resolve(logs.length < allowedLimit);
    } catch (error) {
      logger.error({ message: error.message, stack: error.stack }, "Error while fetching notification log");
      // Incase of DB error, still sending the notification.
      return resolve(true);
    }
  });
};

module.exports.rateLimit = rateLimit;
