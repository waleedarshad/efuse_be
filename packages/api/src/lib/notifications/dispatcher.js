const { Failure } = require("@efuse/contracts");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../../backend/models/User");
const { HomeFeed } = require("../../backend/models/HomeFeed");
const { Comment } = require("../../backend/models/comment");
const { Feed } = require("../../backend/models/Feed");
const { HypeModel } = require("../../backend/models/hype/hype.model");
const { Opportunity } = require("../../backend/models/Opportunity");
const { GeneralHelper } = require("../../backend/helpers/general.helper");
const NotificationUtils = require("./utils");
const NotificationLib = require(".");
const Config = require("../../async/config");
const { HypeEnum } = require("@efuse/entities");

// Async Queues
const dispatchSingleNotificationQueue = Config.initQueue("dispatchSingleNotification");
const commentReplyNotificationQueue = Config.initQueue("commentReplyNotification");
const hypedPostCommentNotificationQueue = Config.initQueue("hypedPostCommentNotification");
const applicantNotificationQueue = Config.initQueue("applicantNotification");
const dispatchChatNotificationQueue = Config.initQueue("dispatchChatNotification");

const userFields = (permission) => `_id ${permission} ${permission}Mobile online`;

const dispatchSingleNotification = async (notification, permission, appendTimeline, feedId) => {
  try {
    const { receiver } = notification;
    let receiverId = receiver;
    if (receiver._id) {
      receiverId = receiver._id;
    }
    const user = await User.findOne({ _id: receiverId }).select(userFields(permission)).lean();

    if (!user) {
      throw Failure.InternalServerError(`Unable to find user to notify: ${receiver}`, {
        notification,
        permission,
        appendTimeline,
        feedId
      });
    }

    const target = await NotificationUtils.getNotificationTarget(user, permission);

    if (!target) {
      logger.info({ user, permission }, "Skipping notification as permission is not enabled by user");
      return;
    }

    const newNotification = { ...notification, target };
    if (appendTimeline) {
      const homeFeed = await HomeFeed.findOne({ feed: feedId }, {}, { autopopulate: false }).lean();
      if (homeFeed) {
        newNotification.timeline = homeFeed._id;
      }
    }

    return await NotificationLib.createNotification(newNotification);
  } catch (error) {
    logger.error(error, "Error while dispatching notification");
  }
};

const dispatchSingleNotificationAsync = (notification, permission, appendTimeline, feedId) => {
  return dispatchSingleNotificationQueue.add({ notification, permission, appendTimeline, feedId });
};

const commentReplyNotification = async (notification, permission, parent, feedId) => {
  try {
    const localNotification = notification;
    const comment = await Comment.findOne({ _id: parent }, {}, { autopopulate: false }).lean();

    if (!comment) {
      logger.info({ comment: parent }, "Comment not found!");
      return;
    }

    const user = await User.findOne({ _id: comment.user }).select(userFields(permission)).lean();

    // if a user replies on his own thread
    if (!user || String(localNotification.sender) === String(user._id)) {
      logger.info({ comment: parent, user }, "user replied to his own thread");
      return;
    }

    const receiverId = user._id;
    const feed = await Feed.findOne({ _id: feedId }, {}, { autopopulate: false }).lean();

    // if a user who is the author of the post starts a thread on his own post then some one comments on that thread
    if (!feed || String(feed.user) === String(receiverId)) {
      logger.info({ feed, user }, "User is the owner of the post");
      return;
    }

    localNotification.receiver = receiverId;

    const target = await NotificationUtils.getNotificationTarget(user, permission);
    if (!target) {
      logger.info({ user, permission }, "Permission not enabled by user");
      return;
    }

    const homeFeed = await HomeFeed.findOne(
      {
        user: feed.user,
        feed: feed._id
      },
      {},
      { autopopulate: false }
    ).lean();

    if (!homeFeed) {
      logger.info({ user: feed.user, feed: feed._id }, "HomeFeed not found!");
      return;
    }

    return await NotificationLib.createNotification({
      ...localNotification,
      timeline: homeFeed._id,
      target
    });
  } catch (error) {
    logger.error(error, "Error sending comment reply notification");
  }
};

const commentReplyNotificationAsync = (notification, permission, parent, feedId) => {
  return commentReplyNotificationQueue.add({ notification, permission, parent, feedId });
};

const hypedPostCommentNotification = async (notification, permission, feedId) => {
  try {
    const localNotification = notification;
    const { notifyable } = localNotification;
    const dontNotifyUsers = [];
    const comment = await Comment.findOne({ _id: notifyable }, {}, { autopopulate: false }).lean();

    if (!comment) {
      logger.info(localNotification, "Comment not found!");
      return;
    }

    // Skip comment creator
    dontNotifyUsers.push(GeneralHelper.getObjectId(comment.user));
    if (comment.parent) {
      // Skip thread owner | Thread owner receives commentReplyNotification
      const parentComment = await Comment.findOne({ _id: comment.parent }, {}, { autopopulate: false }).lean();

      if (parentComment) {
        dontNotifyUsers.push(GeneralHelper.getObjectId(parentComment.user));
      }
    }

    // Skip Feed Owner | Feed owner receiver Someone commented on your post notification
    const feed = await Feed.findOne({ _id: feedId }, {}, { autopopulate: false }).lean();
    if (!feed) {
      logger.info(feedId, "Feed not found!");
      return;
    }

    dontNotifyUsers.push(GeneralHelper.getObjectId(feed.user));

    const homeFeed = await HomeFeed.findOne(
      {
        feed: feedId,
        user: feed.user
      },
      {},
      { autopopulate: false }
    ).lean();

    if (!homeFeed) {
      logger.info({ feed: feedId, user: feed.user }, "HomeFeed not found!");
      return;
    }

    localNotification.timeline = homeFeed._id;

    const likes = await HypeModel.find(
      {
        objId: feedId,
        objType: HypeEnum.FEED,
        user: { $nin: dontNotifyUsers }
      },
      {},
      { autopopulate: false }
    ).lean();

    if (!likes || likes.length === 0) {
      logger.info(
        {
          objId: feedId,
          objType: HypeEnum.FEED,
          user: { $nin: dontNotifyUsers }
        },
        "Hypes not found!"
      );
      return;
    }

    logger.info(likes.length, "Total Hypes Found");

    const response = await Promise.all(
      likes.map(async (like) => {
        const user = await User.findOne({ _id: like.user }).select(userFields(permission)).lean();
        const target = await NotificationUtils.getNotificationTarget(user, permission);
        // todo: investigate
        // @ts-ignore
        const responseObject = { user: user._id };

        if (!target) {
          responseObject.permission = false;
          return responseObject;
        }

        try {
          await NotificationLib.createNotification({
            ...localNotification,
            // todo: investigate
            // @ts-ignore
            receiver: user._id,
            target
          });

          responseObject.success = true;

          return responseObject;
        } catch (error) {
          responseObject.success = false;
          responseObject.error = error;
          logger.error(responseObject, "Error Sending hypedPostCommentNotification");

          return responseObject;
        }
      })
    );

    logger.info(response, "Sent hypedPostCommentNotification to users");

    return;
  } catch (error) {
    logger.error(error, "Error sending hyped post comment notification");
  }
};

const hypedPostCommentNotificationAsync = (notification, permission, feedId) => {
  return hypedPostCommentNotificationQueue.add({
    notification,
    permission,
    feedId
  });
};

const applicantNotification = async (notification) => {
  try {
    const { notifyable } = notification;
    const permission = "opportunityNotifications.newApplicant";
    const opportunity = await Opportunity.findOne({ _id: notifyable }, {}, { autopopulate: false }).lean();

    if (!opportunity) {
      logger.info({ notification }, "Opportunity not found");
      return;
    }

    const user = await User.findOne({ _id: opportunity.user }).select("_id").lean();

    if (!user) {
      logger.info({ user: opportunity.user }, "User not found!");
      return;
    }

    const target = await NotificationUtils.getNotificationTarget(user, permission);

    if (!target) {
      logger.info({ user, permission }, "Permission not enabled by user");
      return;
    }

    // TODO MIGRATE EMAIL TO BE DISPATCHED INSTEAD OF LAMBDA
    return await NotificationLib.createNotification({
      ...notification,
      receiver: user._id,
      target
    });
  } catch (error) {
    logger.error(error, "Error sending notification to applicant");
  }
};

const applicantNotificationAsync = (notification) => {
  return applicantNotificationQueue.add({ notification });
};

const dispatchChatNotification = async (notification, permission) => {
  try {
    const { receiver } = notification;
    const user = User.findOne({ _id: receiver }).select(userFields(permission)).lean();
    const target = await NotificationUtils.getNotificationTarget(user, permission);

    if (!target) {
      logger.info({ user, permission }, "Permission not enabled by user");
      return;
    }

    // todo: investigate
    // @ts-ignore
    if (!user.online) {
      logger.info({ user }, "User is offline");
      return;
    }

    return await NotificationLib.createNotification({
      ...notification,
      target
    });
  } catch (error) {
    logger.error(error, "Error sending chat notification");
  }
};

const dispatchChatNotificationAsync = (notification, permission) => {
  return dispatchChatNotificationQueue.add({ notification, permission });
};

// Async Queue Processors

const initProcessors = () => {
  dispatchSingleNotificationQueue.process((job) => {
    const { notification, permission, appendTimeline, feedId } = job.data;

    return dispatchSingleNotification(notification, permission, appendTimeline, feedId);
  });

  commentReplyNotificationQueue.process((job) => {
    const { notification, permission, parent, feedId } = job.data;

    return commentReplyNotification(notification, permission, parent, feedId);
  });

  hypedPostCommentNotificationQueue.process((job) => {
    const { notification, permission, feedId } = job.data;

    return hypedPostCommentNotification(notification, permission, feedId);
  });

  applicantNotificationQueue.process((job) => {
    const { notification } = job.data;

    return applicantNotification(notification);
  });

  dispatchChatNotificationQueue.process((job) => {
    const { notification, permission } = job.data;

    return dispatchChatNotification(notification, permission);
  });
};

module.exports.applicantNotification = applicantNotification;
module.exports.applicantNotificationAsync = applicantNotificationAsync;
module.exports.commentReplyNotification = commentReplyNotification;
module.exports.commentReplyNotificationAsync = commentReplyNotificationAsync;
module.exports.dispatchChatNotification = dispatchChatNotification;
module.exports.dispatchChatNotificationAsync = dispatchChatNotificationAsync;
module.exports.dispatchSingleNotification = dispatchSingleNotification;
module.exports.dispatchSingleNotificationAsync = dispatchSingleNotificationAsync;
module.exports.hypedPostCommentNotification = hypedPostCommentNotification;
module.exports.hypedPostCommentNotificationAsync = hypedPostCommentNotificationAsync;
module.exports.initProcessors = initProcessors;
