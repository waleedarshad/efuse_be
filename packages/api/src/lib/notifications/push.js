const OneSignal = require("onesignal-node");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { uniq } = require("lodash");

const { io } = require("../../backend/config/get-io-object");

const { ONESIGNAL_API_KEY, ONESIGNAL_APP_ID } = require("@efuse/key-store");

const NotificationLogLib = require("./log");
const NotificationRateLimitLib = require("./rate-limit");

const sendNotification = async (userId, title, message, type) => {
  await io.to(userId).emit("PUSH_NOTIFICATIONS", { title, message, type });
};

const sendPushNotification = async (userIds = [], content = "", opts = {}) => {
  return new Promise(async (resolve, reject) => {
    if (!ONESIGNAL_API_KEY || !ONESIGNAL_APP_ID) {
      logger.fatal(
        "You must set the ONESIGNAL_API_KEY and ONESIGNAL_APP_ID environment variable to send push notification"
      );
      return reject("Keys are not configured");
    }
    const uniqUserIds = uniq(userIds.map((id) => id.toString()));
    // Applying rate limit
    const shouldReceiveNotification = [];
    for (const userId of uniqUserIds) {
      const sendNotification = await NotificationRateLimitLib.rateLimit(userId, "push");
      if (sendNotification) {
        shouldReceiveNotification.push(userId);
      }
    }
    const shouldNotReceiveNotification = uniqUserIds.filter((id) => !shouldReceiveNotification.includes(id));

    try {
      const client = new OneSignal.Client(ONESIGNAL_APP_ID, ONESIGNAL_API_KEY);
      let response = {};
      if (shouldReceiveNotification.length > 0) {
        response = await client.createNotification({
          contents: { en: content },
          include_external_user_ids: shouldReceiveNotification,
          ...opts
        });
        await createNotificationLog(shouldReceiveNotification, content, opts, "SENT");
      }
      if (shouldNotReceiveNotification.length > 0) {
        await createNotificationLog(shouldNotReceiveNotification, content, opts, "RATE_LIMIT_REACHED");
        response.rateLimitReached = shouldNotReceiveNotification;
      }
      return resolve(response);
    } catch (error) {
      await createNotificationLog(shouldReceiveNotification, content, opts, "ERROR", error);
      if (error instanceof OneSignal.HTTPError) {
        // When status code of HTTP response is not 2xx, HTTPError is thrown.
        logger.error(error, "Error sending push notificaiton");
        return reject(error);
      }
      logger.error(error, "General Error sending push notificaiton");
      return reject(error);
    }
  });
};

const createNotificationLog = async (userIds, content, opts, status, error = {}) => {
  const errorObject = error ? { message: error.message, stack: error.stack } : null;
  const notificationObject = opts.data ? opts.data : null;
  const notificationId = notificationObject ? notificationObject._id : null;
  for (const userId of userIds) {
    await NotificationLogLib.createLog(userId, "push", content, notificationId, status, errorObject);
  }
};

module.exports.createNotificationLog = createNotificationLog;
module.exports.sendNotification = sendNotification;
module.exports.sendPushNotification = sendPushNotification;
