const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/notifications/email.js" });
const sgMail = require("@sendgrid/mail");
const { SENDGRID_API_KEY } = require("@efuse/key-store");
const Config = require("../../async/config.js");

const emailNotificationsAsync = Config.initQueue("sendNotificationEmail");
const NotificationLogLib = require("./log");
const NotificationRateLimit = require("./rate-limit");

const { UserService } = require("../users/user.service");

const userService = new UserService();

const createNotificationLog = async (userId, content, status, error = null) => {
  return await NotificationLogLib.createLog(userId, "email", content, null, status, error);
};

const sendNotificationEmail = async (receiver, emailData) => {
  try {
    let logStatus = "RATE_LIMIT_REACHED";

    const sendNotification = await NotificationRateLimit.rateLimit(receiver, "email");
    if (sendNotification) {
      const receiveUser = await userService.findOne({ _id: receiver }, { email: 1 });
      sgMail.setApiKey(SENDGRID_API_KEY);

      await sgMail.send({
        templateId: "d-d92d1cb614c943d4824d3e9f1995eaaa",
        from: { name: "eFuse", email: "notifications@efuse.gg" },
        // todo: investigate
        // @ts-ignore
        to: receiveUser.email,
        dynamicTemplateData: emailData
      });
      logStatus = "SENT";
    }
    await createNotificationLog(receiver, emailData.subject, logStatus);
  } catch (error) {
    logger.error({ message: error.message }, "Error sending email notification");
    await createNotificationLog(receiver, emailData.subject, "ERROR", {
      message: error.message,
      stack: error.stack
    });
  }
};

const sendNotificationEmailAsync = (receiver, emailData) => {
  return emailNotificationsAsync.add({ receiver, emailData });
};

const initAsyncProcessors = () => {
  emailNotificationsAsync.process(async (job) => {
    await sendNotificationEmail(job.data.receiver, job.data.emailData);
  });
};

module.exports.createNotificationLog = createNotificationLog;
module.exports.initAsyncProcessors = initAsyncProcessors;
module.exports.sendNotificationEmailAsync = sendNotificationEmailAsync;
module.exports.sendNotificationEmail = sendNotificationEmail;
