const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Notification } = require("../../backend/models/Notification");
const { NotificationLog } = require("../../backend/models/NotificationLog");
const { Media } = require("../../backend/models/media");
const { HomeFeed } = require("../../backend/models/HomeFeed");

const { DOMAIN } = require("@efuse/key-store");
const { io } = require("../../backend/config/get-io-object");

const PushLib = require("./push");
const { Braze } = require("@efuse/braze-rest-client");

const rescueNil = (prop, key, rescueWith = "") => {
  try {
    return prop[key];
  } catch {
    return rescueWith;
  }
};

const getMediaUrl = async (mediaable) => {
  try {
    const media = await Media.findOne({
      mediaable,
      mediaableType: "feeds"
    });

    // todo: investigate
    // @ts-ignore
    if (!media || !media.file || !media.file.contentType.startsWith("image")) {
      return "";
    }

    return media.file.url;
  } catch (error) {
    logger.error(error, "Error fetching media to construct media url for push notification");
    return "";
  }
};

const buildUrl = (notification) => {
  const dontOpen = "/?_osp=do_not_open";
  try {
    const { notifyable, notifyableType, timeline } = notification;
    const follower = rescueNil(notifyable, "follower");
    const organization = rescueNil(notifyable, "organization");
    const notifyableId = rescueNil(notifyable, "_id");
    const timelineUrl = timeline ? `/home?feedId=${timeline}` : dontOpen;
    const tournament = rescueNil(notifyable, "slug");
    let url = "";

    switch (notifyableType) {
      case "friends":
        url = follower ? `/users/posts?id=${follower}` : dontOpen;
        break;
      case "hypes":
      case "likes":
      case "comments":
        url = timelineUrl;
        break;
      case "homeFeeds":
        url = notifyableId ? `/home?feedId=${notifyableId}` : dontOpen;
        break;
      case "opportunities":
        url = notifyableId ? `/opportunities/show/${notifyableId}` : dontOpen;
        break;
      case "organizationInvitations":
        url = organization ? `/organizations/show?id=${organization}` : dontOpen;
        break;
      case "erenatournaments":
        url = tournament ? `/e/${tournament}/manage?scoring=true` : dontOpen;
        break;
      default:
        url = dontOpen;
        break;
    }

    return `${DOMAIN}${url}`;
  } catch (error) {
    logger.warn(error, "Error Building Push Notification Url");
    return `${DOMAIN}${dontOpen}`;
  }
};

const sendPushNotification = async (notification, mediaUrl) => {
  const { receiver, content } = notification;
  const notificationContent = content.replace(/<[^>]+>/g, "");
  try {
    const opts = {
      data: notification,
      big_picture: mediaUrl,
      ios_attachments: { id: mediaUrl },
      web_url: buildUrl(notification)
    };

    // create notification groups
    if (notification.notifyableType === "hypes") {
      const groupId = `${notification.notifyableType}-${notification.timeline}`;
      opts.android_group = groupId;
      opts.thread_id = groupId;
    }

    await PushLib.sendPushNotification([receiver], notificationContent, opts);
  } catch (error) {
    logger.error(error, "General | Error sending push notification");
  }
};

const createNotification = async (notification) => {
  try {
    // Array of notifyableType that have been migrated to Braze.
    const DO_NOT_SEND_TYPES = ["friends", "hypes", "comments", "erenatournaments", "opportunities"];

    if (DO_NOT_SEND_TYPES.includes(notification.notifyableType)) {
      logger.info(
        { notification },
        "Skipping OneSignal push notification because notification has been migrated to Braze"
      );
      return;
    }

    const createdNotification = await new Notification(notification).save();
    const populatedNotification = await createdNotification
      .populate({ path: "sender", select: "_id" })
      .populate({
        path: "notifyable",
        select: "_id follower organization slug"
      })
      .execPopulate();

    await new NotificationLog({
      user: populatedNotification.receiver,
      type: "in-app",
      content: populatedNotification.content,
      notification: populatedNotification._id
    }).save();

    let mediaUrl = "";

    if (notification.notifyableType === "homeFeeds") {
      const homeFeed = await HomeFeed.findOne({
        _id: notification.notifyable
      });

      if (homeFeed) {
        // todo: investigate
        // @ts-ignore
        mediaUrl = await getMediaUrl(homeFeed.feed);
      }
    } else if (notification.notifyableType === "feeds") {
      // todo: investigate
      // @ts-ignore
      mediaUrl = await getMediaUrl(notification.notifyable);
    }
    await sendPushNotification(populatedNotification, mediaUrl);

    await io.to(populatedNotification.receiver).emit("BUMP_NOTIFICATION_COUNT", {});

    return;
  } catch (error) {
    logger.error(error, "Error creating notification");
  }
};

module.exports.buildUrl = buildUrl;
module.exports.createNotification = createNotification;
module.exports.getMediaUrl = getMediaUrl;
module.exports.rescueNil = rescueNil;
module.exports.sendPushNotification = sendPushNotification;
