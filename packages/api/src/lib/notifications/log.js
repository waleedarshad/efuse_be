const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create({ name: "lib/notifications/log.js" });

const { NotificationLog } = require("../../backend/models/NotificationLog");

/**
 * @summary Create notification log
 *
 * @param {ObjectId} userId - recipient of notification
 * @param {String} type - i.e push, in-app or email
 * @param {String} content - content of the notification
 * @param {ObjectId | null} notificationId - ID of the notification if available
 * @param {string} status - i.e RATE_LIMIT_REACHED, SENT or ERROR
 * @param {any} error - object
 */
const createLog = async (userId, type, content = "", notificationId = null, status = "SUCCESS", error = null) => {
  try {
    await new NotificationLog({
      user: userId,
      type,
      notification: notificationId,
      content,
      status,
      error
    }).save({ validateBeforeSave: false });
  } catch (error) {
    logger.error({ message: error.message, stack: error.stack }, "Error while creating notification log");
  }
};

module.exports.createLog = createLog;
