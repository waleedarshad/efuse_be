import { Failure, LogLevel } from "@efuse/contracts";
import { ILogger } from "@efuse/logger";
import { StatsD } from "hot-shots";
import { Types } from "mongoose";

/**
 * Base service, helps provide instrumentation
 */
export class BaseService {
  protected logger: ILogger | undefined;
  protected name: string | undefined;
  protected stats: StatsD | undefined;

  /**
   * @summary Check whether provide object id is valid or invalid
   *
   * @param {Types.ObjectId | string} objectId - mongoDB id to be checked
   *
   * @return {boolean} - Return true if valid otherwise false
   */
  public isValidObjectId(objectId: Types.ObjectId | string): boolean {
    try {
      // This will return true for any 12 or 24 chars string
      let isValid = Types.ObjectId.isValid(objectId);

      /**
       * Valid ids do not change when casted to an ObjectId but a string that gets a false valid
       * will change when casted to an objectId
       */
      if (isValid) {
        const castedObjectId = new Types.ObjectId(objectId);
        isValid = String(objectId) === String(castedObjectId);
      }

      return isValid;
    } catch (error) {
      this.logger?.error(error, "Error while validating ObjectId");
      return false;
    }
  }

  /**
   * Calls `incrementMetric` under the service name and sets
   * the `resource.name` to the relevant service name
   *
   * @param functionName
   * @param event
   * @param tags
   */
  public recordMetric(event: string, tags: string[] = []): void {
    const prepend: string = this.name ? `${this.name}.` : "";
    if (this.stats) {
      this.stats.increment(`${prepend}${event}`, tags);
    }
  }

  protected handleError(error: Error, message?: string): Error {
    const httpError: Failure = <Failure>error;
    const internalMessage = httpError.message || message || "there was an error";

    switch (httpError.level) {
      case LogLevel.info: {
        this.logger?.info(error, internalMessage);
        break;
      }
      case LogLevel.warn: {
        this.logger?.warn(error, internalMessage);
        break;
      }
      case LogLevel.error: {
        this.logger?.error(error, internalMessage);
        break;
      }
      default: {
        this.logger?.error(error, internalMessage);
        break;
      }
    }

    return httpError.statusCode ? error : Failure.InternalServerError(internalMessage);
  }
}
