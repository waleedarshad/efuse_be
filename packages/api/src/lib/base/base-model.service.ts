import { Failure } from "@efuse/contracts";
import { isObject } from "lodash";
import {
  Document,
  FilterQuery,
  Model,
  PaginateResult,
  PaginateModel,
  PaginateOptions,
  QueryOptions,
  Types,
  UpdateQuery,
  UpdateWriteOpResult,
  Aggregate
} from "mongoose";

import { Trace } from "../decorators";
import { BaseService } from "./base.service";

/**
 * Base Model Service extends the functionality of base service by providing basic CRUD (A.K.A Create Read Update Delete)
 * functionality when a model and type are passed in.
 */
export class BaseModelService<T extends Document> extends BaseService {
  private baseModel: Model<T>;

  constructor(model: Model<T>) {
    super();

    this.baseModel = model;
  }

  /**
   * Calls the underlying Mongoose aggregate function.
   * @param pipeline The aggregate pipeline stages in array form, as defined by MongoDB.
   * @returns An array of aggregated objects.
   */
  @Trace()
  public async aggregate(pipeline: any[]): Promise<Aggregate<T[], any>> {
    try {
      return this.baseModel.aggregate(pipeline);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * Retrieves an aggregate object to perform additional operation on. Use this if you want to be
   * able to call the clean, chainable Mongoose wrapper methods instead of building a raw MongoDB
   * aggregate pipeline array.
   * @returns An aggregate object.
   */
  @Trace()
  public getAggregate(): Aggregate<T[], any> {
    return this.baseModel.aggregate();
  }

  /**
   * USE WITH CAUTION
   *
   * This is not recommended if getting a count for everything in a large collection. This is because it will scan every document in the collection without indexes.
   * If this were used on "valorantplayermatchstats" collection, it would have very poor performance since that has over 13 million documents.
   *
   * If getting count for everything in a collection then "estimatedCountDocuments" would be a better option.
   *
   */
  @Trace()
  public async countDocuments(filter: FilterQuery<T>): Promise<number> {
    try {
      const count = await this.baseModel.countDocuments(filter);

      return count;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async create(item: T): Promise<T> {
    try {
      const result: T = await this.baseModel.create(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createMany(items: T[]): Promise<T[]> {
    try {
      const result: T[] = await this.baseModel.insertMany(items);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async delete(id: Types.ObjectId | string): Promise<boolean> {
    if (!this.isValidObjectId(id)) {
      throw Failure.BadRequest("Invalid id", id);
    }

    try {
      const deleted = await this.baseModel.findByIdAndDelete(id);

      return deleted != null;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * USE WITH CAUTION
   */
  @Trace()
  public async deleteMany(filter: FilterQuery<T>, options?: QueryOptions): Promise<boolean> {
    try {
      if (!isObject(filter)) {
        throw Failure.BadRequest("Invalid filter", filter);
      }

      const deleted = await this.baseModel.deleteMany(filter, options);

      return deleted.deletedCount != null && deleted.deletedCount > 0;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async deleteOne(filter: FilterQuery<T>, options?: QueryOptions): Promise<boolean> {
    try {
      if (!isObject(filter)) {
        throw Failure.BadRequest("Invalid filter", filter);
      }

      const deleted = await this.baseModel.deleteOne(filter, options);

      return deleted != null;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findOneAndDelete(filter: FilterQuery<T>, options?: QueryOptions): Promise<T | null> {
    try {
      if (!isObject(filter)) {
        throw Failure.BadRequest("Invalid filter", filter);
      }

      const deletedObject = await this.baseModel.findOneAndDelete(filter, options);

      return deletedObject;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async exists(filter: FilterQuery<T>): Promise<boolean> {
    try {
      const match = await this.baseModel.exists(filter);

      return match;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async find(filter: FilterQuery<T>, projection?: any, options?: QueryOptions): Promise<T[]> {
    try {
      const item: T[] = await this.baseModel.find(filter, projection, options);

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findOne(filter: FilterQuery<T>, projection?: any, options?: QueryOptions): Promise<T | null> {
    try {
      const item: T | null = await this.baseModel.findOne(filter, projection, options);

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async findPaginated(filter: FilterQuery<T>, options: PaginateOptions): Promise<PaginateResult<T>> {
    // Shallow clone. Shallow is fine since we're not changing reference object properties.
    const constrainedOptions = { ...options };

    // Make sure the page param is positive.
    if (!constrainedOptions.page || constrainedOptions.page < 1) {
      constrainedOptions.page = 1;
    }

    // Make sure the limit is positive.
    if (!constrainedOptions.limit || constrainedOptions.limit < 1) {
      constrainedOptions.limit = 1;
    }

    try {
      const paginatedItems = await (<PaginateModel<T>>this.baseModel).paginate(filter, options);

      return paginatedItems;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async updateOne(condition: FilterQuery<T>, body: UpdateQuery<T>, options?: QueryOptions): Promise<T> {
    if (!body) {
      throw Failure.BadRequest("Body is missing for update");
    }

    try {
      const item: T | null = await this.baseModel.findOneAndUpdate(condition, body, options);

      if (!item) {
        throw Failure.UnprocessableEntity("No such item available for update", { condition, body, options });
      }

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async updateMany(condition: FilterQuery<T>, body: UpdateQuery<T>, options?: QueryOptions): Promise<T[]> {
    if (!body) {
      throw Failure.BadRequest("Body is missing for update");
    }

    try {
      const update: UpdateWriteOpResult = await this.baseModel.updateMany(condition, body, options);

      if (update.nModified < 1) {
        throw Failure.UnprocessableEntity("No such items available for update", { condition, body, options });
      }

      const items: T[] = await this.find(condition, {}, options);

      return items;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
