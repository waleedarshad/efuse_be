import { Service, Trace } from "@efuse/decorators";

import { BaseModelService } from "./base";
import { ITwitchAccountInfo } from "../backend/interfaces";
import { TwitchAccountInfo } from "../backend/models/twitch-account-info";

@Service("twitch-account-info.service")
export class TwitchAccountInfoService extends BaseModelService<ITwitchAccountInfo> {
  constructor() {
    super(TwitchAccountInfo);
  }

  @Trace()
  public async deleteByOwner(owner: string): Promise<void> {
    await this.deleteOne({ owner });
  }
}
