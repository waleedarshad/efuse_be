const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create();
const feed = require("../feeds/index");

/**
 * @summary Called the method to save hashtags
 *
 * @param {ObjectId} postId - Id of the post
 */
const saveHashtags = async (postId) => {
  try {
    logger.info({ func: "saveHashtags", postId }, "In saveHashtags hook");
    return await feed.saveHashtagsAsync(postId);
  } catch (error) {
    logger.error({ error }, "Error in saveHashtags hook ");
    throw error;
  }
};

module.exports.saveHashtags = saveHashtags;
