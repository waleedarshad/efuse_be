import { IFollowee, IFollower } from "../../backend/interfaces";
import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { initFomoNotifyFollowers } from "../fomo/index";

@Service("user-hooks.service")
export class UserHooksService extends BaseService {
  constructor() {
    super();
  }

  /**
   * @summary Calls the method to send notification
   *
   * @param {IFollower} follower - follower information i.e id and name
   * @param {IFollowee} followee - followee information i,e id
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async newUserFollowsUser(follower: IFollower, followee: IFollowee): Promise<void> {
    try {
      return await initFomoNotifyFollowers(follower, followee);
    } catch (error) {
      this.logger?.error(error);
    }
  }
}
