const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { createLivestream, endLivestream } = require("../livestream");

const { Slack } = require("../slack");

const SLACK_CHANNEL = "#livestreams";

/**
 * @summary User making online livestream
 *
 * @param {Object} user - User info i.e name, id, email etc
 * @param {String} platform - Platform of the user's livestream
 * @param {String} title - Title of the stream
 */
const userWentOnline = (user, platform, title) => {
  try {
    // TODO: send out a push notification to subscribers when user goes live
    createLivestream(user._id, platform, title).catch((error) =>
      logger.error(error, "Error resolving createLivestream promise")
    );

    logger.info({ username: user.username, platform }, "User just went online!");
    Slack.sendMessage(`[@${user.username}] User just went online on ${platform}!`, SLACK_CHANNEL).catch((error) =>
      logger.error(error, "Error resolving sendMessage promise")
    );
  } catch (error) {
    logger.error({ error }, "Error in the userWentOnline hook");
  }
};

/**
 * @summary User ending online livestream
 *
 * @param {Object} user - User info i.e name, id, email etc
 * @param {String} platform - Platform of the user's livestream
 */
const userWentOffline = (user, platform) => {
  try {
    logger.info({ user: user.username }, `User just went offline on ${platform}`);
    Slack.sendMessage(`[@${user.username}] User just went offline on ${platform}!`, SLACK_CHANNEL).catch((error) =>
      logger.error(error, "Error resolving sendMessage promise")
    );
    endLivestream(user._id, platform).catch((error) => logger.error(error, "Error resolving endLivestream promise"));
  } catch (error) {
    logger.error({ username: user.username, platform, error }, "Error in the userWentOffline hook");
  }
};

module.exports.userWentOffline = userWentOffline;
module.exports.userWentOnline = userWentOnline;
