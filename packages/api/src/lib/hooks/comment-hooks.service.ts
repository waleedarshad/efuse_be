import { BaseService } from "../base";
import { Service, Trace } from "../decorators";
import { HypeAsyncService } from "../hype/hype-async.service";
import { FeedService } from "../feeds/feed.service";
import { IComment } from "../../backend/interfaces";

@Service("comment-hooks.service")
export class CommentHooksService extends BaseService {
  private hypeAsyncService: HypeAsyncService;
  private feedService: FeedService;

  constructor() {
    super();

    this.hypeAsyncService = new HypeAsyncService();
    this.feedService = new FeedService();
  }

  /**
   * @summary Add a new comment on the post
   *
   * @param {IComment} comment - Comment to add against the post
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async newPostComment(comment: IComment): Promise<void> {
    try {
      const feedPost = await this.feedService.findOne({ _id: comment.commentable }, {}, { lean: true });

      if (feedPost?.user) {
        await this.hypeAsyncService.updateHypeScoreAsync(feedPost.user);
      }
    } catch (error) {
      this.logger?.error({ error }, "Error in newPostComment hook ");
    }
  }
}
