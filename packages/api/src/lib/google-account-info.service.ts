import { Service, Trace } from "@efuse/decorators";

import { BaseModelService } from "./base";
import { IGoogleAccountInfo } from "../backend/interfaces";
import { GoogleAccountInfo } from "../backend/models/google-account-info";

@Service("google-account-info.service")
export class GoogleAccountInfoService extends BaseModelService<IGoogleAccountInfo> {
  constructor() {
    super(GoogleAccountInfo);
  }

  @Trace()
  public async deleteByOwner(owner: string): Promise<void> {
    await this.deleteOne({ owner });
  }
}
