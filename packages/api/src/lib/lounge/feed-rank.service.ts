import { isEmpty, round } from "lodash";
import { Types, Model } from "mongoose";
import Bull from "bull";

import { Service, Trace } from "../decorators";
import { RankService } from "../rank/rank.service";

import { ConstantsService } from "../constants";
import Config from "../../async/config";

import { ILoungeFeed } from "../../backend/interfaces/lounge-feed";

import { LoungeFeed } from "../../backend/models/LoungeFeed";
import { MediaService } from "../media/media.service";
import { RankingUtil } from "../utils/ranking.util";
import { Feed } from "../../backend/models/Feed";
import { IFeed } from "../../backend/interfaces/feed";
import { HypeService } from "../hype/hype.service";

interface IWeights {
  imageWeight: number;
  boostWeight: number;
  verifiedWeight: number;
  hypeWeight: number;
  commentWeight: number;
  videoWeight: number;
  timeBucket: number;
  gravity: number;
}

@Service("feed-rank.service")
export class FeedRankService extends RankService {
  private readonly feed: Model<IFeed> = Feed;
  private readonly loungeFeed: Model<ILoungeFeed> = LoungeFeed;

  private mediaService: MediaService;
  private hypeService: HypeService;
  private constantsService: ConstantsService;

  private calculateAndStoreFeedRankQueue: Bull.Queue<any>;

  constructor() {
    super("feeds");

    this.mediaService = new MediaService();
    this.hypeService = new HypeService();
    this.constantsService = new ConstantsService();

    this.calculateAndStoreFeedRankQueue = Config.initQueue("calculateAndStoreFeedRank");
  }

  /**
   * @summary Calculate the feed score
   *
   * @param {Date} createdAt - Date on which feed is created
   * @param {Number} totalHypes - Total hypes received by post
   * @param {Number} totalComments - Total comments received by post
   * @param {String} mediaContentType - Content type of media if it is attached
   * @param {Boolean} verified - Author is verified or not
   * @param {Boolean} boosted - Post is boosted or not
   *
   * @return {Number} Feed score
   */
  @Trace()
  public async feedRankFormula(
    createdAt: Date,
    totalHypes = 0,
    totalComments = 0,
    mediaContentType = "",
    verified = false,
    boosted = false
  ): Promise<number> {
    let weights: IWeights = await this.constantsService.getVariables("FEED_RANK_ALGORITHM");

    // Fallback to defaults when weights are not found
    if (isEmpty(weights)) {
      this.logger!.info("Constants are not found, Fallback to default");
      weights = {
        gravity: 1.8,
        videoWeight: 5,
        imageWeight: 2.5,
        hypeWeight: 1,
        commentWeight: 1,
        timeBucket: 1,
        verifiedWeight: 1,
        boostWeight: 10
      };
    }

    // Building media constraint
    let mediaWeight = 0;
    if (mediaContentType.includes("video")) {
      mediaWeight = weights.videoWeight;
    } else if (mediaContentType.includes("image")) {
      mediaWeight = weights.imageWeight;
    }

    // Build boost weight
    let boostWeight = 0;
    if (boosted) {
      boostWeight = weights.boostWeight;
    }

    // Build verified weight
    let verifiedWeight = 0;
    if (verified) {
      verifiedWeight = weights.verifiedWeight;
    }

    // Calculate feed rank
    const calculatedRank = round(
      (weights.hypeWeight * totalHypes +
        weights.commentWeight * totalComments +
        mediaWeight +
        boostWeight +
        verifiedWeight +
        1) /
        (RankingUtil.getTimeConstraint(createdAt, weights.timeBucket) + 2) ** weights.gravity,
      2
    );
    // Return rank
    return Number.isNaN(calculatedRank) ? 0 : calculatedRank;
  }

  /**
   * @summary Add or edit Feed in redis sorted set
   *
   * @param {ObjectId} feedId - ID of the target feed
   * @param {Object} lounge - LoungeFeed object
   *
   * @return {Promise<number | undefined>}
   */
  @Trace()
  public async calculateFeedRank(
    feedId: Types.ObjectId,
    loungeFeedObject?: ILoungeFeed | null | undefined
  ): Promise<number | undefined | void> {
    try {
      let lounge = loungeFeedObject;

      // Get the targeted Feed
      const feed = await this.feed.findOne(
        { _id: feedId },
        { _id: 1, comments: 1, likes: 1, createdAt: 1, "metadata.boosted": 1 },
        { lean: true }
      );

      // Verify feed presence
      if (!feed) {
        this.logger!.info({ feedId }, "Feed not found! Might be deleted");
        // Removing feed
        await this.removeObject(feedId);
        return;
      }

      // If lounge is not passed as param, Goahead & fetch it
      if (!lounge) {
        // Get corresponding loungeFeed
        lounge = await this.loungeFeed.findOne({ feed: feedId }).select("userVerified").lean();

        if (!lounge) {
          this.logger!.info({ feedId }, "Lounge not found! Might be deleted");
          // Removing feed
          await this.removeObject(feedId);
          return;
        }
      }

      // Get associated Media
      const media = await this.mediaService.findOne({ mediaableType: "feeds", mediaable: feedId });

      // Set media content type
      let mediaContentType = "";
      if (media && media.file.contentType) {
        mediaContentType = media.file.contentType;
      }

      // Get hypes count

      const hypesCount = await this.hypeService.getPostHypeCount(feedId);

      // in IFeed inteface we have set createdAt as optional, due to which need to tell
      // compiler that we are not passing empty date
      let createdAt = new Date();
      if (feed.createdAt) {
        createdAt = feed.createdAt;
      }

      // Calculate feed rank
      const feedRank = await this.feedRankFormula(
        createdAt,
        hypesCount,
        feed.comments,
        mediaContentType,
        lounge.userVerified,
        feed.metadata?.boosted
      );
      return feedRank;
    } catch (error) {
      this.logger!.error(error, "Error while adding feed to sorted set");
    }
  }

  /**
   * @summary Update feed rank
   *
   * @param {Types.ObjectId} feedId
   */
  @Trace()
  public async calculateAndStoreFeedRank(feedId: Types.ObjectId): Promise<void> {
    const feedRank = await this.calculateFeedRank(feedId);

    if (feedRank !== undefined) {
      await this.setObjectRank(feedId, feedRank);
    }
  }

  /**
   * @summary Update feed rank async
   *
   * @param {Types.ObjectId} feedId
   */
  @Trace()
  public async calculateAndStoreFeedRankAsync(feedId: Types.ObjectId): Promise<void> {
    await this.calculateAndStoreFeedRankQueue.add({ feedId });
  }

  /**
   * @summary Async Queue processors
   */
  @Trace({ name: "InitAsyncProcessors", service: "feed-rank.service" })
  public static InitAsyncProcessors(): void {
    const feedRankService = new FeedRankService();

    feedRankService.calculateAndStoreFeedRankQueue
      .process((job: { data: { feedId: unknown } }) => {
        const { feedId } = job.data;
        return feedRankService.calculateAndStoreFeedRank(feedId as Types.ObjectId);
      })
      .catch((error) => {
        feedRankService.logger?.error(error, "Error while processing queue calculateAndStoreFeedRankQueue");
      });
  }

  /**
   * @summary Cron job to update feed rank
   */
  @Trace({ name: "RefreshFeedRankCron", service: "feed-rank.service" })
  public static async RefreshFeedRankCron(): Promise<void> {
    const feedRankService = new FeedRankService();

    // Get ranks for those feeds which are created in last 72 hours
    const ranks = await feedRankService.getRanksInAWindow(72);

    const refreshRanksPromises: Promise<void>[] = [];

    for (const rank of ranks) {
      refreshRanksPromises.push(feedRankService.calculateAndStoreFeedRank(rank.document));
    }

    // Resolving promises concurrenlty
    await Promise.all(refreshRanksPromises);
  }
}
