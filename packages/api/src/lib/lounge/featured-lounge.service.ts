import { Types, PaginateResult } from "mongoose";
import { isEmpty, shuffle } from "lodash";

import { ILoungeAggregate } from "../../backend/interfaces/lounge-feed";
import { IRank } from "../../backend/interfaces/rank";
import { Service, Trace } from "../decorators";
import { buildAssociatedFeed } from "../feeds";
import { LoungeFeedService } from "./lounge-feed.service";
import { RankService } from "../rank/rank.service";
import ViewsLib from "../views";
import { formatLoungeFeedPosts } from "../../backend/helpers/feed.helper";
import { IPostFeedItem } from "../../backend/interfaces/feed/post-feed-item";

@Service("featured-lounge.service")
export class FeaturedLoungeService extends RankService {
  private loungeFeedService: LoungeFeedService;

  constructor() {
    super("feeds");

    this.loungeFeedService = new LoungeFeedService();
  }

  /**
   * @summary Get a fully populated paginated LoungeFeed
   *
   * @param {Types.ObjectId} userId
   * @param {string[]} currentUserRoles
   * @param {number} page
   * @param {number} limit
   *
   * @return {Promise<PaginateResult<ILoungeAggregate>>}
   */
  @Trace()
  public async getPaginatedLoungeFeed(
    userId: Types.ObjectId,
    currentUserRoles: string[],
    page: number,
    limit: number
  ): Promise<PaginateResult<ILoungeAggregate>> {
    // timestamp to get post from last 3 days
    const timestamp: Date = this.moment().subtract(3, "d").toDate();

    // Get paginated ranked feeds
    const rankedFeed = await this.getRanked(page, limit, { createdAt: { $gte: timestamp } });

    // Extract feedIds array
    const feedIds = rankedFeed.docs.map((rank) => rank.document);

    // Get corresponding lounge feed
    const loungeFeed = await this.loungeFeedService.find({ feed: { $in: feedIds } }, {}, { lean: true });

    // Lookup associated objects
    const aggregatedLoungeFeedPromises: Promise<ILoungeAggregate>[] = [];

    for (const lounge of loungeFeed) {
      // todo: investigate
      // @ts-ignore
      aggregatedLoungeFeedPromises.push(buildAssociatedFeed(lounge, userId) as Promise<ILoungeAggregate>);
    }

    // Resolving promises concurrently
    const aggregatedLoungeFeed = (await Promise.all(aggregatedLoungeFeedPromises)).filter((lounge) => !isEmpty(lounge));

    // Adding feedRank for admin
    if (currentUserRoles.includes("admin")) {
      for (const lounge of aggregatedLoungeFeed) {
        const feedRank = rankedFeed.docs.find((rank) => String(rank.document) === String(lounge.feed));
        lounge.associatedFeed.metadata.feedRank = feedRank?.rankValue || 0;
      }
    }

    // Increment views
    ViewsLib.incrementFeedViews(feedIds).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    return {
      docs: shuffle(aggregatedLoungeFeed),
      hasNextPage: rankedFeed.hasNextPage,
      hasPrevPage: rankedFeed.hasPrevPage,
      limit: rankedFeed.limit,
      page: rankedFeed.page,
      nextPage: rankedFeed.nextPage,
      pagingCounter: rankedFeed.pagingCounter,
      prevPage: rankedFeed.prevPage,
      totalDocs: rankedFeed.totalDocs,
      totalPages: rankedFeed.totalPages
    };
  }

  /**
   * Retrieve the lounge feed in ranked order (randomized) and formatted for simplified feeds
   *
   * @param userId
   * @param currentUserRoles
   * @param page
   * @param limit
   */
  @Trace()
  public async getPaginatedRankLoungeFeedFormatted(
    userId: Types.ObjectId,
    currentUserRoles: string[],
    page: number,
    limit: number
  ): Promise<PaginateResult<IPostFeedItem>> {
    const rankedFeed = await this.getPaginatedFeaturedPosts(page, limit);

    // Extract feedIds array
    const feedIds = rankedFeed.docs.map((rank) => rank.document);

    // Get corresponding lounge feed
    const loungeFeed = await this.loungeFeedService.find({ feed: { $in: feedIds } }, {}, { lean: true });

    // Format feed objects
    const feeds = loungeFeed.map((feed) => {
      const formattedFeed = <IPostFeedItem>(feed ? formatLoungeFeedPosts(feed, userId) : {});
      return formattedFeed;
    });

    const results = await Promise.all(feeds);
    // Removing empty docs
    const docs = results.filter((doc) => !isEmpty(doc));

    // Adding feedRank for admin
    if (currentUserRoles.includes("admin")) {
      for (const lounge of docs) {
        const feedRank = rankedFeed.docs.find((rank) => String(rank.document) === String(lounge.feedId));

        lounge.feedRank = feedRank?.rankValue || 0;
      }
    }

    // Increment views
    ViewsLib.incrementFeedViews(feedIds).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    const paginatedLoungeFeed: PaginateResult<IPostFeedItem> = {
      docs: shuffle(docs),
      hasNextPage: rankedFeed.hasNextPage,
      hasPrevPage: rankedFeed.hasPrevPage,
      limit: rankedFeed.limit,
      page: rankedFeed.page,
      nextPage: rankedFeed.nextPage,
      pagingCounter: rankedFeed.pagingCounter,
      prevPage: rankedFeed.prevPage,
      totalDocs: rankedFeed.totalDocs,
      totalPages: rankedFeed.totalPages
    };

    return paginatedLoungeFeed;
  }

  /**
   * @summary Get a paginated list of featured posts
   *
   * @param {number} page
   * @param {number} limit
   *
   * @return {Promise<PaginateResult<IRank>>}
   */
  @Trace()
  public async getPaginatedFeaturedPosts(page: number, limit: number): Promise<PaginateResult<IRank>> {
    // timestamp to get post from last 3 days
    const timestamp: Date = this.moment().subtract(3, "d").toDate();

    // Get paginated ranked feeds
    const rankedFeed = await this.getRanked(page, limit, { createdAt: { $gte: timestamp } });
    return rankedFeed;
  }
}
