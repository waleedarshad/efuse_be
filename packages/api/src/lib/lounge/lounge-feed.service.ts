import { Failure } from "@efuse/contracts";
import { Types, PaginateResult } from "mongoose";

import { isEmpty } from "lodash";
import { BaseModelService } from "../base";
import { ILoungeFeed } from "../../backend/interfaces/lounge-feed";
import { Service, Trace } from "../decorators";
import { LoungeFeed } from "../../backend/models/LoungeFeed";
import { IFeedList } from "../../backend/interfaces/feed/feed";
import { formatLoungeFeedPosts } from "../../backend/helpers/feed.helper";
import { IPostFeedItem } from "../../backend/interfaces/feed/post-feed-item";
import { FeedService } from "../feeds/feed.service";
import { IFeed } from "../../backend/interfaces/feed";
import { BlockedUsersService } from "../blocked-users/blocked-users.service";
import { IRank } from "../../backend/interfaces/rank";
import { PaginationHelper } from "../../graphql/helpers/pagination.helper";
import { ObjectId } from "../../backend/types";

import FeedLib from "../feeds";
import LoungeAsync from "../../async/core/lounge";
import ViewsLib from "../views";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";
import { FlagsmithEnum, SegmentEventEnum } from "@efuse/entities";
import { UserService } from "../users/user.service";
import moment from "moment";
import { EFuseSegmentAnalyticsUtil } from "../efuse-segment-analytics/efuse-segment-analytics.util";
import { FeedRankService } from "./feed-rank.service";
import { IUser } from "../../backend/interfaces";

@Service("lounge-feed.service")
export class LoungeFeedService extends BaseModelService<ILoungeFeed> {
  private blockedUsersService: BlockedUsersService;
  private userService: UserService;

  constructor() {
    super(LoungeFeed);

    this.blockedUsersService = new BlockedUsersService();
    this.userService = new UserService();
  }

  /**
   * @summary Add Rank to feed
   * @param featuredFeed
   * @param feedRanks
   *
   */
  public addRankToFeed(featuredFeed: IFeed[], feedRanks: IRank[]): IFeed[] {
    return featuredFeed.map((feed) => {
      const feedRank = feedRanks.find((rank) => String(rank.document) === String(feed._id));
      feed.feedRank = feedRank?.rankValue || 0;
      return feed;
    });
  }

  /**
   * @summary Method to cleanup user's created lounge feed
   *
   * @param {ObjectId} userId - ID of user whose lounge feed is to be deleted
   */
  @Trace()
  public async cleanupUsersLoungeFeed(userId: ObjectId): Promise<void> {
    try {
      await this.deleteMany({ user: userId }, { lean: true });
      this.logger?.info("Correspoding lounge feeds have been deleted");
    } catch (error) {
      throw this.handleError(error, "Error while cleaning up user's lounge feed");
    }
  }

  /**
   * @summary Async method to add job to queue to cleanup user's created lounge feed
   *
   * @param {ObjectId} userId - ID of user whose lounge feed is to be deleted
   */
  @Trace()
  public async cleanupUsersLoungeFeedAsync(userId: ObjectId): Promise<void> {
    try {
      return LoungeAsync.cleanupUsersLoungeFeedQueue.add({ userId });
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Remove blocked user feed
   *
   * @param feedIds
   * @param userId
   * @returns {IFeed[]}
   */
  @Trace()
  public async filterFeedByConstraints(feedIds: Types.ObjectId[], userId: string): Promise<IFeed[]> {
    // TODO: instantiating here to avoid a circle dependency. We might look into doing something different here
    const feedService = new FeedService();
    const featuredFeed: IFeed[] = await feedService.find({ _id: { $in: feedIds } }, {}, { lean: true });
    const blockedUsers = await this.blockedUsersService.getBlockedOrBlockedByUsers(userId);

    const adjustedFeed = featuredFeed.map((feed) => {
      let wasPostReportedByUser = false;

      if (feed.reports) {
        wasPostReportedByUser =
          feed.reports?.filter((report) => report.reportedBy?.toHexString() === userId).length > 0;
      }

      if (blockedUsers.includes(feed.user.toHexString()) || wasPostReportedByUser) {
        return null;
      }

      return feed;
    });

    const finalizedFeed = adjustedFeed.filter((feed) => feed != null);

    // because of the filter there aren't any nulls, but the IDE can have trouble figuring that out
    return <IFeed[]>finalizedFeed;
  }

  /**
   * @summary Get a LoungeFeed object by id
   *
   * @param {ObjectId} id - ID of the LoungeFeed
   * @param {ObjectId} userId - ID of the user who is requesting feed
   * @return {Promise<any>} LoungeFeed Object with associated records
   */
  @Trace()
  public async getLoungeById(id: ObjectId, userId: ObjectId): Promise<ILoungeFeed> {
    if (!this.isValidObjectId(id)) {
      throw Failure.BadRequest("ID is malformed", { id });
    }

    const loungeFeed = await this.findOne({ _id: id }, {}, { lean: true });

    if (!loungeFeed) {
      throw Failure.UnprocessableEntity("Post not found!", { id, loungeFeed });
    }

    // TODO: <any> is temporary fix until FeedLib is converted to typescript
    const adjustedLoungeFeed = await FeedLib.buildAssociatedFeed(loungeFeed, <any>userId);

    if (!adjustedLoungeFeed) {
      throw Failure.BadRequest("Post not found!");
    }

    return adjustedLoungeFeed;
  }

  @Trace()
  public async getLoungeFeedPost(id: string, currentUserId: string): Promise<IPostFeedItem> {
    const feed = await this.findOne({ _id: id }, {}, { lean: true });
    if (!feed) {
      throw Failure.NotFound("Could not find lounge feed.");
    }

    const formattedFeed = await formatLoungeFeedPosts(feed, currentUserId);

    if (!formattedFeed) {
      throw Failure.InternalServerError("Unable to format feed", feed);
    }

    ViewsLib.incrementFeedViews([id]).catch((error) => {
      this.logger?.warn(error, "Error while incrementing Feed view count");
    });

    return formattedFeed;
  }

  /**
   * @summary Get paginated collection of lounge by query
   *
   * TODO: type should be made into enum
   *
   * @param {String} type - featured, gold, verified or organization
   * @param {ObjectId} userId - User ID who is requesting feed
   * @param {Number} page
   * @param {Number} limit - Number of records per page
   *
   * @return {Promise<any>} Lounge Feed collection with pagination info {docs: [], paginationInfo}
   */
  @Trace()
  public async getPaginatedLoungeFeed(
    type: string,
    userId: ObjectId,
    page = 1,
    limit = 20
  ): Promise<PaginateResult<ILoungeFeed>> {
    const enableGoldFeature = await FeatureFlagService.hasFeature("gold_feature_flags", String(userId));

    const query = {
      published: { $ne: false },
      ...(type === "verified" ? { userVerified: true } : {}),
      ...(type === "gold" && enableGoldFeature ? { gold: true } : {}),
      ...(type === "organization" ? { timelineableType: "organizations" } : {})
    };

    if (limit > 20) {
      limit = 20;
    }

    const paginatedLoungeFeed = await this.findPaginated(
      { ...query, deleted: { $ne: true } },
      {
        page,
        limit,
        lean: true,
        populate: [],
        sort: { createdAt: -1 },
        // todo: investigate
        // @ts-ignore
        forceCountFn: true
      }
    );

    // TODO: <any> is temporary fix until Feedlib is converted to TS
    const associatedObjects = await Promise.all(
      paginatedLoungeFeed.docs.map((loungeFeed) => FeedLib.buildAssociatedFeed(loungeFeed, <any>userId))
    );

    paginatedLoungeFeed.docs = associatedObjects.filter((doc) => !isEmpty(doc));

    return paginatedLoungeFeed;
  }

  /**
   * @summary Get verified lounge feed
   *
   * @param {string | Types.ObjectId} currentUserId
   * @param {number} page page number
   * @param {number} limit limit page size
   *
   * @return {PaginateResult<IFeedList>} return paginated feed
   */
  @Trace()
  public async getVerifiedLoungeFeed(
    currentUserId: Types.ObjectId,
    page: number,
    limit: number
  ): Promise<PaginateResult<IFeedList>> {
    // Make sure page param is positive
    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    // Fetching paginated collection
    const paginatedLoungeFeed = await this.findPaginated(
      {
        published: { $ne: false },
        userVerified: true,
        deleted: { $ne: true }
      },
      {
        page,
        limit,
        lean: true,
        populate: [],
        sort: { createdAt: -1 },
        // todo: investigate & fix
        // @ts-ignore
        forceCountFn: true
      }
    );

    return this.convertPaginatedLoungeFeedToPaginatedFeedList(paginatedLoungeFeed, currentUserId);
  }

  /**
   * @summary Verified feed for graphql
   *
   * @param currentUserId
   * @param page
   * @param limit
   */
  @Trace()
  public async getVerifiedFeed(page: number, limit: number): Promise<PaginateResult<ILoungeFeed>> {
    const paginationHelper = new PaginationHelper();
    const { page: pageParam, limit: limitParam } = paginationHelper.validatePaginationParams(page, limit);

    // Fetching paginated collection
    const paginatedLoungeFeed = await this.findPaginated(
      {
        published: { $ne: false },
        userVerified: true,
        deleted: { $ne: true }
      },
      {
        page: pageParam,
        limit: limitParam,
        lean: true,
        populate: [],
        sort: { createdAt: -1 },
        // todo: investigate & fix
        // @ts-ignore
        forceCountFn: true
      }
    );

    return paginatedLoungeFeed;
  }

  @Trace()
  public async updateLoungeFeedAsync(
    userId: ObjectId,
    feedId: ObjectId,
    timelineable: ObjectId,
    timelineableType: string,
    verified: boolean
  ): Promise<void> {
    return LoungeAsync.updateLoungeFeedQueue.add({
      userId,
      feedId,
      timelineable,
      timelineableType,
      verified
    });
  }

  @Trace()
  public async updateLoungeFeed(
    userId: ObjectId,
    feedId: ObjectId,
    timelineable: ObjectId,
    timelineableType: string,
    verified: boolean
  ): Promise<void> {
    try {
      const existingFeed = await this.exists({
        user: userId,
        feed: feedId,
        timelineableType: timelineableType,
        timelineable: timelineable
      });

      if (existingFeed) {
        this.logger?.warn("Post has already been added to lounge feed. Ending request!");
        return;
      }

      const user = await this.userService.findOne(
        { _id: userId },
        { goldSubscriber: true, createdAt: true },
        { lean: true }
      );

      if (!user) {
        this.logger?.warn(`User not found: ${userId}`);
        return;
      }

      /**
       * Disabling new user's post getting added to lounge when user is created in last 24 hours
       * Controlled by flag disable_new_user_lounge_post_creation
       */
      const disableNewUserLoungePostCreation = await FeatureFlagService.hasFeature(
        FlagsmithEnum.DISABLE_NEW_USER_LOUNGE_POST_CREATION
      );
      if (disableNewUserLoungePostCreation) {
        // investigate
        // @ts-ignore
        const createdAt = moment(user.createdAt);
        const createdAtDifference = moment().diff(createdAt, "h");

        if (createdAtDifference <= 24) {
          this.logger?.info(
            { createdAt, userId, createdAtDifference },
            "Preventing user's post to be added in lounge as user's createdAt <= 24"
          );
          return;
        }
      }

      // Added logic that user can not have more than 2 featured post
      const date = moment().subtract(1, "h").toDate();
      const feedsCount = await this.countDocuments({
        user: userId,
        createdAt: { $gt: date }
      });

      this.logger?.info({ post: feedsCount }, `Post created during [${date}] window`);

      if (feedsCount >= 2) {
        this.logger?.warn("User can not have more than 2 feature post within an hour");
        return;
      }

      await this.createLoungeFeedFromUpdate(user, feedId, timelineable, timelineableType, verified);
      await this.boostUsersFirstPost(userId, feedId);

      // Adding feed to redis sorted sets once lounge feed is created
      // TODO: Types.ObjectId(feedId.toString()) is a workaround. I know its weird, but something like it is currently necessary until feed rank service is updated
      const feedRankService = new FeedRankService();
      await feedRankService.calculateAndStoreFeedRankAsync(Types.ObjectId(feedId.toString()));
    } catch (error) {
      this.handleError(error, "Error adding post to lounge feed");
    }
  }

  @Trace()
  private async boostUsersFirstPost(userId: ObjectId, feedId: ObjectId): Promise<void> {
    try {
      const finishedPromises = await Promise.all([
        this.countDocuments({ user: userId }),
        FeatureFlagService.hasFeature(FlagsmithEnum.EXP_BE_BOOSTED_FIRST_POST, String(userId))
      ]);

      // finished promise values will be based on input array
      const loungeCount = finishedPromises[0];
      const expBoostFirstPost = finishedPromises[1];

      if (loungeCount === 1) {
        // Boost first post when flag is enabled
        if (expBoostFirstPost) {
          // Set the boost boolean on feed
          // TODO: avoiding cirlce dependency. This is temporary. We will need to investigate a better solution
          const feedService = new FeedService();
          const feed = await feedService.update(
            { _id: feedId },
            { "metadata.boosted": true },
            { lean: true, new: true }
          );

          if (!feed) {
            throw Failure.UnprocessableEntity("Feed not found");
          }
        }

        // Segment Tracking
        // TODO: <string>userId is temporary fix until AnalyticsUtil is updated
        EFuseSegmentAnalyticsUtil.track({ userId: <string>userId }, SegmentEventEnum.EXPERIMENT_STARTED, {
          "Experiment name": FlagsmithEnum.EXP_BE_BOOSTED_FIRST_POST,
          "Variant name": expBoostFirstPost ? "boosted" : "not_boosted"
        });
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * TODO: This is a workaround for how we are doing feeds. The IFeedlist needs a slight rework, so this handles it for now here.
   */
  @Trace()
  private async convertPaginatedLoungeFeedToPaginatedFeedList(
    paginatedLoungeFeed: PaginateResult<ILoungeFeed>,
    currentUserId: ObjectId
  ): Promise<PaginateResult<IFeedList>> {
    try {
      // Format verified feed objects
      const feeds = paginatedLoungeFeed.docs.map((feed) => feed && formatLoungeFeedPosts(feed, currentUserId));

      const results = await Promise.all(feeds);

      // Removing empty docs
      const postFeed = results.filter((feedItem) => !isEmpty(feedItem)) as IPostFeedItem[];

      ViewsLib.incrementFeedViews(postFeed.map((postItem: IPostFeedItem) => postItem.feedId)).catch((error) => {
        this.logger?.warn(error, "Error while incrementing Feed view count");
      });

      const { hasNextPage, hasPrevPage, pagingCounter, totalDocs, totalPages, meta, nextPage, prevPage } =
        paginatedLoungeFeed;

      return {
        docs: <any>postFeed,
        limit: paginatedLoungeFeed.limit,
        page: paginatedLoungeFeed.page,
        totalDocs,
        hasNextPage,
        hasPrevPage,
        pagingCounter,
        totalPages,
        meta,
        nextPage,
        prevPage
      };
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async createLoungeFeedFromUpdate(
    user: IUser,
    feedId: ObjectId,
    timelineable: ObjectId,
    timelineableType: string,
    verified: boolean
  ): Promise<void> {
    try {
      const loungeFeed = <ILoungeFeed>{
        user: user._id,
        feed: feedId,
        timelineableType,
        timelineable,
        userVerified: verified || false,
        gold: user.goldSubscriber || false
      };

      await this.create(loungeFeed);

      this.logger?.info("Added new post to lounge feed");
    } catch (error) {
      this.handleError(error);
    }
  }
}
