import { PaginateResult, Types, FilterQuery } from "mongoose";
import moment from "moment";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

import { Rank, RankModel } from "../../backend/models/rank";
import { IRank } from "../../backend/interfaces/rank";

type RankQuery = FilterQuery<
  Pick<IRank, "_id" | "rankValue" | "document" | "documentType" | "createdAt" | "updatedAt">
>;

@Service("rank.service")
export class RankService extends BaseService {
  private refreshInterval: number;
  private maxObjects: number;
  private documentType: string;

  protected readonly moment = moment;

  private readonly MONGO_PREFERENCE_TAGS: { nodeType: string }[] = [{ nodeType: "ANALYTICS" }];

  private ranks: RankModel<IRank> = Rank;

  constructor(documentType: string, refreshInterval = 0, maxObjects = 1000) {
    super();
    this.documentType = documentType;
    this.refreshInterval = refreshInterval;
    this.maxObjects = maxObjects;
  }

  /**
   * @summary Get paginated collection of ranked objects
   *
   * @param {number} page
   * @param {number} limit - number of records per page
   *
   * @return {Promise<PaginateResult<IRank>>}
   */
  @Trace()
  public async getRanked(page = 1, limit = 10, conditions: RankQuery = {}): Promise<PaginateResult<IRank>> {
    let pageNumber = Number(page);
    if (pageNumber < 0) {
      pageNumber = 1;
    }

    let recordsPerPage = Number(limit);
    if (recordsPerPage < 0) {
      recordsPerPage = 10;
    }
    if (recordsPerPage > 25) {
      recordsPerPage = 25;
    }

    const paginatedRank = await this.ranks.paginate(
      <FilterQuery<IRank>>{ documentType: this.documentType, ...conditions },
      {
        page: pageNumber,
        limit: recordsPerPage,
        sort: { rankValue: -1 },
        lean: true,
        select: "_id rankValue document documentType updatedAt",
        read: { pref: "secondary", tags: this.MONGO_PREFERENCE_TAGS }
      }
    );
    return paginatedRank;
  }

  /**
   * @summary Create a new object to be ranked
   *
   * @param {Types.ObjectId} objectId
   *
   * @return {Promise<IRank>}
   */
  @Trace()
  public async addObject(objectId: Types.ObjectId, value: number): Promise<void> {
    const doc = <IRank>{ document: objectId, documentType: this.documentType, rankValue: value };
    await this.ranks.create(doc);
  }

  /**
   * @summary Get rank value for a particular object
   *
   * @param {Types.ObjectId} objectId
   *
   * @return {Promise<Number>}
   */
  @Trace()
  public async getObjectRank(objectId: Types.ObjectId): Promise<number> {
    const rankObject = await this.ranks
      .findOne({ documentType: this.documentType, document: objectId })
      .select("_id rankValue")
      .lean()
      .read("secondary", this.MONGO_PREFERENCE_TAGS);
    return rankObject?.rankValue || 0;
  }

  /**
   * @summary Update the object rank
   *
   * @param {Types.ObjectId} objectId
   * @param {number} value
   *
   * @return {Promise<IRank | null>}
   */
  @Trace()
  public async setObjectRank(objectId: Types.ObjectId, value: number): Promise<void> {
    await this.ranks
      .findOneAndUpdate(
        {
          documentType: this.documentType,
          document: objectId
        },
        { documentType: this.documentType, document: objectId, rankValue: value, updatedAt: new Date() },
        { new: true, upsert: true, setDefaultsOnInsert: true }
      )
      .read("secondary", this.MONGO_PREFERENCE_TAGS);
  }

  /**
   * @summary Remove a particular object from rank collection
   *
   * @param {Types.ObjectId} objectId
   */
  @Trace()
  public async removeObject(objectId: Types.ObjectId): Promise<void> {
    await this.ranks
      .findOneAndRemove({ documentType: this.documentType, document: objectId })
      .read("secondary", this.MONGO_PREFERENCE_TAGS);
  }

  /**
   * @summary Refresh ranks for a particular object
   *
   * @param {Types.ObjectId} objectId
   */
  @Trace()
  public async refreshObjectRank(objectId: Types.ObjectId): Promise<void> {
    // Get the rank
    const rankValue = await this.getObjectRank(objectId);

    // Update rank
    await this.setObjectRank(objectId, rankValue);
  }

  /**
   * @summary Get ranks which are created in a particular interval
   *
   * @param {number} hours
   *
   * @return
   */
  @Trace()
  public async getRanksInAWindow(hours: number): Promise<IRank[]> {
    // Timestamp for last x hours
    const timestamp: Date = this.moment().subtract(hours, "h").toDate();

    this.logger!.info(`Fetching ${this.documentType} ranks where createdAt >= ${String(timestamp)}`);
    const ranks = await this.ranks.find({
      rankValue: { $gt: 0 },
      documentType: this.documentType,
      createdAt: { $gte: timestamp }
    });
    return ranks;
  }
}
