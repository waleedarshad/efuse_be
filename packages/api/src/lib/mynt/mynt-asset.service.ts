import { FilterQuery, QueryOptions } from "mongoose";
import { cloneDeep } from "lodash";
import { Failure } from "@efuse/contracts";
import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { MyntAsset, MyntAssetModel } from "../../backend/models/mynt/mynt-asset.model";
import { IMyntAsset } from "../../backend/interfaces/mynt";

@Service("mynt-asset.service")
export class MyntAssetService extends BaseService {
  private readonly myntAsset: MyntAssetModel<IMyntAsset> = MyntAsset;

  public async findOne(filter: FilterQuery<IMyntAsset>): Promise<IMyntAsset | null>;
  public async findOne(filter: FilterQuery<IMyntAsset>, projection: any): Promise<IMyntAsset | null>;
  public async findOne(
    filter: FilterQuery<IMyntAsset>,
    projection: any,
    options: QueryOptions
  ): Promise<IMyntAsset | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IMyntAsset>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IMyntAsset | null> {
    const myntAsset = await this.myntAsset.findOne(filter, projection, options);
    return myntAsset;
  }

  public async find(filter: FilterQuery<IMyntAsset>): Promise<IMyntAsset[]>;
  public async find(filter: FilterQuery<IMyntAsset>, projection: any): Promise<IMyntAsset[]>;
  public async find(filter: FilterQuery<IMyntAsset>, projection: any, options: QueryOptions): Promise<IMyntAsset[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IMyntAsset>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IMyntAsset[]> {
    const myntAssets = await this.myntAsset.find(filter, projection, options);

    return myntAssets;
  }

  @Trace()
  public async create(item: IMyntAsset): Promise<IMyntAsset> {
    try {
      const localItem = cloneDeep(item);
      if (!localItem) {
        throw Failure.BadRequest("Unable to create item, missing 'item'");
      }

      const doc = <IMyntAsset>localItem;
      const result: IMyntAsset = await this.myntAsset.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createOrUpdate(item: IMyntAsset): Promise<IMyntAsset> {
    try {
      const existingItem = await this.findOne({ tokenId: item.tokenId });

      if (existingItem) {
        const result = <IMyntAsset>(
          (<unknown>await this.myntAsset.updateOne({ _id: existingItem._id }, item, { new: true }))
        );

        return result;
      }

      return await this.create(item);
    } catch (error) {
      console.error(error);
      throw this.handleError(error);
    }
  }
}
