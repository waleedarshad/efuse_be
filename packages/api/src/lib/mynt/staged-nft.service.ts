import { FilterQuery, QueryOptions } from "mongoose";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { IStagedNFT } from "../../backend/interfaces/mynt/staged/staged-nft";
import { StagedNFT, StagedNFTModel } from "../../backend/models/mynt/staged-nft";

@Service("nft-asset.service")
export class StagedNFTService extends BaseService {
  private readonly stagedNFT: StagedNFTModel<IStagedNFT> = StagedNFT;

  public async findOne(filter: FilterQuery<IStagedNFT>): Promise<IStagedNFT | null>;
  public async findOne(filter: FilterQuery<IStagedNFT>, projection: any): Promise<IStagedNFT | null>;
  public async findOne(
    filter: FilterQuery<IStagedNFT>,
    projection: any,
    options: QueryOptions
  ): Promise<IStagedNFT | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IStagedNFT>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IStagedNFT | null> {
    const nft = await this.stagedNFT.findOne(filter, projection, options);
    return nft;
  }

  public async find(filter: FilterQuery<IStagedNFT>): Promise<IStagedNFT[]>;
  public async find(filter: FilterQuery<IStagedNFT>, projection: any): Promise<IStagedNFT[]>;
  public async find(filter: FilterQuery<IStagedNFT>, projection: any, options: QueryOptions): Promise<IStagedNFT[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IStagedNFT>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IStagedNFT[]> {
    const nfts = await this.stagedNFT.find(filter, projection, options);

    return nfts;
  }

  @Trace()
  public async create(doc: IStagedNFT): Promise<IStagedNFT> {
    return this.stagedNFT.create(doc);
  }

  public async findOneAndUpdate(conditions: FilterQuery<IStagedNFT>, update: any): Promise<IStagedNFT | null>;
  public async findOneAndUpdate(
    conditions: FilterQuery<IStagedNFT>,
    update: any,
    options?: QueryOptions | null | undefined
  ): Promise<IStagedNFT | null>;
  @Trace()
  public async findOneAndUpdate(
    conditions: FilterQuery<IStagedNFT>,
    update: any,
    options?: QueryOptions | null | undefined
  ): Promise<IStagedNFT | null> {
    const nft = await this.stagedNFT.findOneAndUpdate(conditions, update, options);

    return nft;
  }
}
