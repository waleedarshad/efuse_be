import { Service, Trace } from "../decorators";
import { BaseService } from "../base/base.service";
import { MyntAssetService } from "./mynt-asset.service";
import { OPEN_SEA_EFUSE_COLLECTION } from "@efuse/key-store";
import { OpenSeaApiService } from "../open-sea/open-sea-api.service";
import { IMyntAsset } from "../../backend/interfaces/mynt";

@Service("mynt-asset-async.service")
export class MyntAssetAsyncService extends BaseService {
  @Trace({ name: "SyncMyntAssetsCron", service: "mynt-asset-async.service" })
  public static async SyncMyntAssetsCron(): Promise<void> {
    const myntAssetAsyncService = new MyntAssetAsyncService();
    const openSeaApiService = new OpenSeaApiService();

    const assets: IMyntAsset[] = [];

    try {
      let processing = true;
      let offset = 0;
      while (processing) {
        const newAssets = await openSeaApiService.getOpenSeaAssets({
          collection: OPEN_SEA_EFUSE_COLLECTION,
          limit: "50",
          offset: String(offset)
        });
        assets.push(...newAssets);

        if (newAssets.length < 50) {
          processing = false;
        } else {
          offset += 1;
        }
      }

      const creationPromises = MyntAssetAsyncService.generateAssetCreationPromises(assets);

      Promise.allSettled(creationPromises)
        .then((results) => {
          myntAssetAsyncService.logger?.info(`Successfully saved assets for opensea collection: ${openSeaApiService}`);

          return results;
        })
        .catch((error) => {
          myntAssetAsyncService.logger?.error(
            error,
            `Failed to save assets for opensea collection ${openSeaApiService}`
          );
        });
    } catch (error) {
      throw myntAssetAsyncService.handleError(error, "Unable to sync open sea assets");
    }
  }

  @Trace({ name: "generateAssetCreationPromises", service: "mynt-asset-async.service" })
  private static generateAssetCreationPromises(assets: IMyntAsset[]): Promise<any>[] {
    const myntAssetService: MyntAssetService = new MyntAssetService();

    const promises = assets.map((asset) => myntAssetService.createOrUpdate(asset));

    return promises;
  }
}
