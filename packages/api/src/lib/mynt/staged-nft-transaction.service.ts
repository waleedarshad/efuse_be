import { FilterQuery, QueryOptions } from "mongoose";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { StagedNFTTransaction, StagedNFTTransactionModel } from "../../backend/models/mynt/staged-nft-transaction";
import { IStagedNFTTransaction } from "../../backend/interfaces/mynt/staged/staged-nft-transaction";

@Service("nft-transactions.service")
export class StagedNFTTransactionService extends BaseService {
  private readonly stagedNFTTransaction: StagedNFTTransactionModel<IStagedNFTTransaction> = StagedNFTTransaction;

  public async findOne(filter: FilterQuery<IStagedNFTTransaction>): Promise<IStagedNFTTransaction | null>;
  public async findOne(
    filter: FilterQuery<IStagedNFTTransaction>,
    projection: any
  ): Promise<IStagedNFTTransaction | null>;
  public async findOne(
    filter: FilterQuery<IStagedNFTTransaction>,
    projection: any,
    options: QueryOptions
  ): Promise<IStagedNFTTransaction | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IStagedNFTTransaction>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IStagedNFTTransaction | null> {
    const nft = await this.stagedNFTTransaction.findOne(filter, projection, options);

    return nft;
  }

  public async find(filter: FilterQuery<IStagedNFTTransaction>): Promise<IStagedNFTTransaction[]>;
  public async find(filter: FilterQuery<IStagedNFTTransaction>, projection: any): Promise<IStagedNFTTransaction[]>;
  public async find(
    filter: FilterQuery<IStagedNFTTransaction>,
    projection: any,
    options: QueryOptions
  ): Promise<IStagedNFTTransaction[]>;
  @Trace()
  public async find(
    filter: FilterQuery<IStagedNFTTransaction>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IStagedNFTTransaction[]> {
    const nfts = await this.stagedNFTTransaction.find(filter, projection, options);

    return nfts;
  }

  @Trace()
  public async create(doc: IStagedNFTTransaction): Promise<IStagedNFTTransaction> {
    return this.stagedNFTTransaction.create(doc);
  }
}
