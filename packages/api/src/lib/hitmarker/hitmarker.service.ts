import axiosLib from "axios";
import { Logger } from "@efuse/logger";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { Async } from "../decorators/async";
import { Opportunity } from "../../backend/models/Opportunity";
import { OpportunityRankService } from "../opportunities/opportunity-rank.service";

const logger = Logger.create();

const axios = axiosLib.create({
  baseURL: "https://hitmarker.net/A6um6C3k!9",
  headers: { Authorization: "Bearer a6q7BUb5Dkx-OULdEnVjXvU01lTc1RG4" }
});

const opportunityRankService = new OpportunityRankService();
/**
 * Hitmarker Service
 *
 * @example
 * const HitmarkerService = require('lib/hitmarker/hitmarker.service')
 *
 *
 */
@Service("hitmarker.service")
export class HitmarkerService extends BaseService {
  @Async()
  static async syncHitmarkerJobsAsync() {
    HitmarkerService.syncHitmarkerJobs().catch((error) =>
      logger.error(error, "Error resolving syncHitmarkerJobs promise")
    );
  }

  @Async()
  static async pruneHitmarkerJobsAsync() {
    HitmarkerService.pruneHitmarkerJobs().catch((error) =>
      logger.error(error, "Error resolving pruneHitmarkerJobs promise")
    );
  }

  @Trace({ service: "hitmarker.service", name: "pruneHitmarkerJobs" })
  static async pruneHitmarkerJobs() {
    try {
      const response = await axios.get("");
      const jobs = response.data.data;
      const externalJobIds = jobs.map((job) => String(job.id));

      logger.info(`Total external hitmarker jobs: ${externalJobIds.length}`);

      const internalJobIds = await Opportunity.find({
        external_source: "hitmarker",
        publishStatus: "open"
      })
        .select("external_id")
        .lean();

      logger.info(`Total internal hitmarker jobs: ${internalJobIds.length}`);
      logger.info("Iterating over jobs and closing");

      // Only remove hitmarker jobs that aren't in the external dump
      const jobIdsToRemove = internalJobIds.filter((job) => {
        return !externalJobIds.includes(job.external_id);
      });

      logger.info(`Total jobs to be closed: ${jobIdsToRemove.length}`);
      for (const job of jobIdsToRemove) {
        await Opportunity.updateOne(
          { _id: job._id },
          {
            publishStatus: "closed",
            status: "hidden"
          }
        );
      }
    } catch (error) {
      logger.error(error, "Error pruning hitmarker jobs");
      return;
    }
  }

  @Trace({ service: "hitmarker.service", name: "syncHitmarkerJobs" })
  static async syncHitmarkerJobs() {
    try {
      const response = await axios.get("");
      const jobs = response.data.data;
      logger.info(`Total # of External Jobs: ${jobs.length}`);
      for (const job of jobs) {
        const opportunity = {
          external: true,
          external_url: job.application.link,
          external_source: "hitmarker",
          external_id: job.id,
          title: job.title,
          link: job.application.link,
          location: job.location,
          description: job.description,
          experience_level: job.level,
          "image.url": job.company.logo,
          opportunityType: "Job",
          subType: "Full-Time",
          company_name: job.company.title,
          randomSeed: Math.random(),
          publishStatus: "open",
          status: "visible",
          shortName: `hm-${job.id}`
        };
        try {
          const description = `${job.id} - ${job.title}`;
          const existingOpportunity = await Opportunity.findOne({
            external_source: "hitmarker",
            external_id: job.id
          });
          if (existingOpportunity) {
            if (existingOpportunity.status !== "visible" || existingOpportunity.publishStatus !== "open") {
              await Opportunity.findOneAndUpdate({ _id: existingOpportunity._id }, { $set: opportunity });
              logger.info(`Job updated: ${description}`);
              opportunityRankService.setObjectRankAsync(existingOpportunity._id);

              continue;
            }
          } else {
            // No opportunity currently exists.  Create a new one.
            const newOpportunity = await new Opportunity(opportunity).save();
            logger.info(`Job created: ${description}`);
            opportunityRankService.setObjectRankAsync(newOpportunity._id);
          }
        } catch (error) {
          logger.error({ message: error.message, job }, "Error while updating job");
        }
      }

      logger.info("Hitmarker Jobs Updated");
      return;
    } catch (error) {
      logger.error(error, "Error updating hitmarker jobs");
      return;
    }
  }
}
