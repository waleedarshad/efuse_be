const { Livestream } = require("../../backend/models/LiveStream");

const { getUserById } = require("../users");

const moment = require("moment");
const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "lib/livestream" });
const { sampleSize } = require("lodash");

const getDayAgo = (hoursOld = 24) => {
  const now = new Date();
  const dayAgo = moment(now).subtract(hoursOld, "h");
  return dayAgo;
};

const getActiveLivestreams = async (hoursOld = 24) => {
  const ls = await Livestream.find({
    endedAt: null,
    startedAt: { $gt: getDayAgo(hoursOld) }
  }).sort({ createdAt: -1 });

  return ls;
};

const getRecentLivestream = async (userId, platform) => {
  const user = await getUserById(userId);

  const ls = await Livestream.findOne({
    user,
    platform,
    endedAt: null,
    startedAt: { $gt: getDayAgo() }
  });
  return ls || null;
};

const endLivestream = async (userId, platform) => {
  const ls = await getRecentLivestream(userId, platform);
  const user = getUserById(userId);
  if (!ls) {
    logger.warn({ user, platform, func: "endLivestream" }, "No recent livestream found for user.  Nothing to do");
  } else {
    logger.info({ user, platform, func: "endLivestream", ls }, "Ending livestream for user!");
    ls.endedAt = new Date();
    await ls.save();
  }
};

const createLivestream = async (userId, platform, title) => {
  let ls = await getRecentLivestream(userId, platform);
  const user = await getUserById(userId);
  if (ls) {
    logger.warn(
      { user, platform, func: "createLivestream", ls },
      "Active livestream already found for user.  Not creating a new one."
    );
  } else {
    logger.info({ user, platform, func: "createLivestream" }, "Creating new livestream!");
    ls = new Livestream({ user, platform, streamTitle: title });
    await ls.save();
    logger.info({ ls }, "Livestream created");
  }
};

const getCarouselLiveStreams = () => {
  return new Promise(async (resolve, reject) => {
    try {
      // Fetching all active in last 24 hours
      const liveStreams = await Livestream.find({
        thumbnailUrl: { $ne: null },
        endedAt: null,
        startedAt: { $gt: getDayAgo() }
      })
        .sort({ createdAt: -1 })
        .lean();

      // Fetching only 10 random streams
      const randomStreams = sampleSize(liveStreams, 10);
      const results = [];
      for (const liveStream of randomStreams) {
        await liveStreamAggregate(liveStream);
        results.push({ type: "live", data: liveStream });
      }
      return resolve(results);
    } catch (error) {
      logger.error({ message: error.message }, "Error while fetching live streams for carousel");
      return reject(error);
    }
  });
};

const liveStreamAggregate = async (liveStream) => {
  liveStream.thumbnailUrl = liveStream.thumbnailUrl.replace("{width}", 1920).replace("{height}", 1080);
  liveStream.associatedUser = await getUserById(liveStream.user, {
    populate: ["twitch"],
    disableCache: true
  });
  return liveStream;
};

module.exports.createLivestream = createLivestream;
module.exports.endLivestream = endLivestream;
module.exports.getActiveLivestreams = getActiveLivestreams;
module.exports.getCarouselLiveStreams = getCarouselLiveStreams;
module.exports.getRecentLivestream = getRecentLivestream;
