import { IAimlabProfile } from "@efuse/contracts";
import { Service, Trace } from "@efuse/decorators";
import { OAuthServiceKind } from "@efuse/entities";
import { STATE_SPACE_CLIENT_ID, STATE_SPACE_CLIENT_SECRET } from "@efuse/key-store";
import { Nullable } from "@efuse/types";
import axios from "axios";
import { Types } from "mongoose";
import superagent from "superagent";

import { AimlabHistoricalStatsDto, AimlabProfileDto } from "../../backend/models/aimlab";
import { AimlabHistoricalStatsService } from "./aimlab-historical-stats.service";
import { AimlabStatsService } from "./aimlab-stats.service";
import { BaseService } from "../base/base.service";
import { IOAuthCredential } from "../../backend/interfaces";
import { OAuthCredentialService } from "../oauth-credential.service";

interface ITokenFlowResponse {
  access_token: string;
  expires_in: number;
  id_token: string;
  refresh_token: string;
  scope: string;
  token_type: string;
}

const AUTH_ENDPOINT = "https://auth.aimlab.gg/oauth"; // todo: move to @efuse/key-store
const QUERY_ENDPOINT = "https://api.aimlab.gg/graphql"; // todo: move to @efuse/key-store

@Service("statespace.service")
export class StatespaceService extends BaseService {
  private credentialService: OAuthCredentialService;
  private historicalService: AimlabHistoricalStatsService;
  private storageService: AimlabStatsService;

  constructor() {
    super();

    this.credentialService = new OAuthCredentialService();
    this.historicalService = new AimlabHistoricalStatsService();
    this.storageService = new AimlabStatsService();
  }

  @Trace()
  public async syncStats(user: string): Promise<Nullable<IAimlabProfile>> {
    let profile: Nullable<IAimlabProfile> = null;
    let credentials: Nullable<IOAuthCredential> = null;

    try {
      credentials = await this.updateCredentials(user);
    } catch (error) {
      this.logger!.error(error, `An error occurred updating Aimlab credentials for user: ${user}`);
      return null;
    }

    try {
      profile = await this.fetch(user, credentials.accessToken);

      if (!profile) {
        this.logger!.error(`No data was returned from Aimlab API for user: ${user}`);
        return null;
      }

      // add user
      profile.owner = new Types.ObjectId(user);
    } catch (error: unknown) {
      this.logger!.error(error, `An error occurred syncing Aimlab stats for user: ${user}`);
      return null;
    }

    try {
      const savedProfile = await this.storageService.upsert(<AimlabProfileDto>profile);

      /**
       * append profile to historical stats ledger for future analysis
       */
      await this.historicalService.create(<AimlabHistoricalStatsDto>profile);

      return savedProfile;
    } catch (error: unknown) {
      this.logger!.error(error, `An error occurred saving Aimlab stats for user: ${user}`);
      return null;
    }
  }

  @Trace()
  private async fetch(user: string, accessToken?: string): Promise<IAimlabProfile | null> {
    try {
      const response = await axios({
        headers: { Authorization: `Bearer ${accessToken}` },
        method: "POST",
        url: QUERY_ENDPOINT,
        data: {
          query: `
          query {
            aimlabProfile {
              ranking {
                rank {
                  displayName,
                  level,
                  maxSkill,
                  minSkill,
                  tier
                },
                skill
              },
              skillScores {
                name,
                score
              },
              taskStats {
                averageScore,
                playCount,
                task { slug },
                topScore
              },
              user {
                id,
                playfabID,
                plays {
                  createdAt,
                  endedAt,
                  mode,
                  performanceScores,
                  playId,
                  rawDataUrl,
                  score,
                  startedAt,
                  taskSlug,
                  version { marketing },
                  weaponName,
                  weaponType
                }
              },
              username
            }
          }
        `
        }
      });

      // get axios data object, if it exists
      const { data: outer } = response.data as { data: { aimlabProfile: IAimlabProfile } };
      if (!outer) {
        return null;
      }

      // get actual aimlab response
      const { aimlabProfile } = outer;
      if (!aimlabProfile) {
        return null;
      }

      return aimlabProfile;
    } catch (error: unknown) {
      this.logger!.error(error, `An error occurred fetching aimlab stats for user: ${user}`);
      return null;
    }
  }

  private async fetchCredentials(user: string, refreshToken: string): Promise<Nullable<ITokenFlowResponse>> {
    try {
      const response = await superagent
        .post(`${AUTH_ENDPOINT}/token`)
        .set("Content-Type", "application/x-www-form-urlencoded")
        .send({
          client_id: STATE_SPACE_CLIENT_ID,
          client_secret: STATE_SPACE_CLIENT_SECRET,
          grant_type: "refresh_token",
          refresh_token: refreshToken
        });

      return response.body as ITokenFlowResponse;
    } catch (error) {
      this.logger!.error(error, `An error occurred fetching updated credentials for user: ${user}`);
      return null;
    }
  }

  private async getStoredCredentials(user: string): Promise<Nullable<IOAuthCredential>> {
    const credentials: Nullable<IOAuthCredential> = <Nullable<IOAuthCredential>>(
      await this.credentialService.findOne({ owner: user, service: OAuthServiceKind.STATESPACE })
    );

    return credentials;
  }

  private async updateCredentials(user: string): Promise<IOAuthCredential> {
    const credentials: Nullable<IOAuthCredential> = await this.getStoredCredentials(user);
    if (!credentials || !credentials.accessToken || !credentials.refreshToken) {
      throw new Error("Unable to update credentials prior to Aimlab API fetch");
    }

    const tokenResponse = await this.fetchCredentials(user, credentials.refreshToken);
    if (!tokenResponse) {
      throw new Error("Invalid token response during Aimlab stat sync");
    }

    credentials.accessToken = tokenResponse.access_token;
    credentials.refreshToken = tokenResponse.refresh_token;

    return credentials;
  }
}
