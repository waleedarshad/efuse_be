import { Service, Trace } from "@efuse/decorators";
import {
  AimlabHistoricalStatsDto,
  AimlabHistoricalStatsModel,
  AimlabHistoricalStats
} from "../../backend/models/aimlab";

import { BaseService } from "../base/base.service";

@Service("aimlab-historical-stats.service")
export class AimlabHistoricalStatsService extends BaseService {
  private readonly model: AimlabHistoricalStatsModel<AimlabHistoricalStatsDto> = AimlabHistoricalStats;

  @Trace()
  public async create(item: AimlabHistoricalStatsDto): Promise<AimlabHistoricalStatsDto> {
    try {
      const doc = item;
      const result: AimlabHistoricalStatsDto = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
