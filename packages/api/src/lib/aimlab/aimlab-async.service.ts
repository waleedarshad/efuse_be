import { UserStatus } from "@efuse/entities";
import { Job, Queue } from "bull";
import { unitOfTime } from "moment";

import { AimlabCronType } from "./aimlab-cron-type.enum";
import { CronEntityKind, CronKindEnum } from "../track-cron/track-cron.enum";
import { IAimlabProfileCronTracker } from "../../backend/interfaces";
import { IUser } from "../../backend/interfaces/user";
import { StatespaceService } from "./statespace.service";
import { TrackCronService } from "../track-cron/track-cron.service";
import * as Config from "../../async/config";

const AIMLAB_STATS_QUEUE_NAME = "aimlab-stats-queue";
const INTERVAL = 24;
const LIMIT = 10;
const POPULATED_FIELDS = "_id deleted status";
const UNIT: unitOfTime.DurationConstructor = "h";

export interface ISyncAimlabStatsJobParams {
  accessToken?: string;
  refreshToken?: string;
  userId: string;
}

export class AimlabAsyncService extends TrackCronService<IAimlabProfileCronTracker> {
  private queueInstance!: Queue;
  private statespaceService: StatespaceService;

  constructor() {
    super(
      CronKindEnum.AIMLAB,
      Object.values(AimlabCronType),
      CronEntityKind.USER,
      POPULATED_FIELDS,
      LIMIT,
      INTERVAL,
      UNIT
    );

    this.statespaceService = new StatespaceService();
  }

  public static initAsyncProcessors(): void {
    const service = new AimlabAsyncService();

    service.asyncProcessors();
    service.logger!.info(`initialized aimlab async processors...`);
  }

  public static async refreshStatsCron(): Promise<void> {
    const service = new AimlabAsyncService();

    await service.executeCron(AimlabCronType.UPDATE, (user: IUser): void => {
      service
        .AimlabStatsQueue()
        .add({ userId: user._id }, { priority: 2 })
        .catch((error: unknown) => {
          service.logger!.error(<any>error, `An error occurred processing aimlab profile for user: ${user._id}`);
        });
    });
  }

  public AimlabStatsQueue(): Queue {
    if (!this.queueInstance) {
      const q = Config.getQueue(AIMLAB_STATS_QUEUE_NAME) as Queue;

      if (q) {
        this.queueInstance = q;
      } else {
        this.queueInstance = Config.initQueue(AIMLAB_STATS_QUEUE_NAME) as Queue;

        this.queueInstance
          .process((job: Job<ISyncAimlabStatsJobParams>) => {
            const { userId } = job.data;

            this.statespaceService
              .syncStats(userId)
              .then(() => {
                this.markCronAsSuccess(AimlabCronType.UPDATE, userId);
              })
              .catch((error: Error) => {
                this.markCronAsFailure(
                  AimlabCronType.UPDATE,
                  userId,
                  error.message || "An error occurred syncing aimlab stats..."
                );
              });
          })
          .catch((error: unknown) => {
            this.logger!.error(<any>error, "An error occurred processing queue: AimlabStatsQueue");
          });
      }
    }

    return this.queueInstance;
  }

  private async executeCron(methodName: string, cronProcessor: (user: IUser) => void): Promise<void> {
    try {
      const entries: IAimlabProfileCronTracker[] = await this.getEntities(methodName);

      if (entries.length === 0) {
        this.logger?.info(`Nothing available to process in trackCron for ${methodName}`);
        return;
      }

      for (const entry of entries) {
        const { entity: user } = entry;

        const valid: boolean = this.validate(user);
        if (!valid) {
          this.markCronAsFailure(methodName, user._id, "Unprocessable entity");
        } else {
          cronProcessor(user);
          this.markCronAsProcessing(methodName, user._id);
        }
      }
    } catch (error: unknown) {
      this.logger!.error(error, `An error occurred running cron ${methodName}`);
    }
  }

  private validate(user: IUser): boolean {
    if (!user) {
      this.logger?.info({ user }, "User not found");
      return false;
    }

    if (user.status === UserStatus.BLOCKED) {
      this.logger?.info({ user }, "User is blocked");
      return false;
    }

    if (user.deleted) {
      this.logger?.info({ user }, "User has been deleted");
      return false;
    }

    return true;
  }
}
