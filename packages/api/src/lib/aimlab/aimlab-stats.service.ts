import { Failure, IAimlabRank } from "@efuse/contracts";
import { Service, Trace } from "@efuse/decorators";
import { Nullable } from "@efuse/types";
import { FilterQuery, QueryOptions, UpdateQuery, UpdateWriteOpResult } from "mongoose";

import { AimlabProfile, AimlabProfileDto, AimlabProfileModel } from "../../backend/models/aimlab";
import { BaseService } from "../base/base.service";

const BADGE_IMAGE_BASE_URL = "/static/aimlab/badges";

@Service("aimlab-stats.service")
export class AimlabStatsService extends BaseService {
  private readonly model: AimlabProfileModel<AimlabProfileDto> = AimlabProfile;

  @Trace()
  public async create(item: AimlabProfileDto): Promise<AimlabProfileDto> {
    try {
      const doc = item;
      const result: AimlabProfileDto = await this.model.create(doc);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async createMany(items: AimlabProfileDto[]): Promise<AimlabProfileDto[]> {
    try {
      const result: AimlabProfileDto[] = await this.model.insertMany(items);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async deleteByOwner(owner: string): Promise<Nullable<AimlabProfileDto>> {
    try {
      if (!owner) {
        throw Failure.BadRequest("Unable to delete item, missing 'owner'");
      }

      const item: Nullable<AimlabProfileDto> = await this.model.findOneAndDelete({ owner }).exec();

      return item;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async distinct(field: string, filter?: FilterQuery<AimlabProfileDto>): Promise<any[]> {
    const match = await this.model.distinct(field, filter).exec();

    return match;
  }

  public async find(filter: FilterQuery<AimlabProfileDto>): Promise<AimlabProfileDto[]>;
  public async find(filter: FilterQuery<AimlabProfileDto>, projection: any): Promise<AimlabProfileDto[]>;
  public async find(
    filter: FilterQuery<AimlabProfileDto>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<AimlabProfileDto[]>;
  @Trace()
  public async find(
    filter: FilterQuery<AimlabProfileDto>,
    projection?: any | undefined,
    options?: QueryOptions | null | undefined
  ): Promise<AimlabProfileDto[]> {
    const match = await this.model.find(filter, projection, options).exec();
    return match;
  }

  public async findOne(filter: FilterQuery<AimlabProfileDto>): Promise<Nullable<AimlabProfileDto>>;
  public async findOne(filter: FilterQuery<AimlabProfileDto>, projection: any): Promise<Nullable<AimlabProfileDto>>;
  public async findOne(
    filter: FilterQuery<AimlabProfileDto>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<Nullable<AimlabProfileDto>>;
  @Trace()
  public async findOne(
    filter: FilterQuery<AimlabProfileDto>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<Nullable<AimlabProfileDto>> {
    const match = await this.model.findOne(filter, projection, options).exec();
    return match;
  }

  @Trace()
  public async updateOne(
    filter: FilterQuery<AimlabProfileDto>,
    update?: UpdateQuery<AimlabProfileDto>,
    options?: Nullable<QueryOptions>
  ): Promise<UpdateWriteOpResult> {
    const match = await this.model.updateOne(filter, update, options);

    return match;
  }

  @Trace()
  public async upsert(item: AimlabProfileDto): Promise<AimlabProfileDto> {
    let result: Nullable<AimlabProfileDto> = null;

    const exists = await this.model.exists({ owner: item.owner });

    if (!exists) {
      result = await this.create(item);
    } else {
      await this.model.findOneAndUpdate({ owner: item.owner }, { ...item, updatedAt: new Date() });

      result = await this.findOne({ owner: item.owner });
    }

    if (!result) {
      throw Failure.InternalServerError("Unable to upsert Aimlab stats");
    }

    return result;
  }

  /**
   * Aimlab actually provides what ranking the user currently has,
   * there is no need to do an explicit switch to generate the range
   * to image conversion, we can just use the level and tier to
   * create the appropriate image url.
   *
   * @param {IAimlabRank} rank
   * @return {*}  {string}
   * @memberof AimlabStatsService
   */
  public getBadgeImage(rank: IAimlabRank): string {
    const { level } = rank;
    const { tier } = rank;

    const transformedTier = tier.toString().toLowerCase();

    return `${BADGE_IMAGE_BASE_URL}/${transformedTier}_${level}.png`;
  }
}
