import { BaseService } from "./base/base.service";
import * as RedisHelper from "./redis/helpers";
import { Service, Trace } from "./decorators";

const DEFAULT_CACHE_EXPIRATION = 4 * 60 * 60; // four hours

@Service("redis-cache.service")
export class RedisCacheService extends BaseService {
  /**
   * Reads from caching layer
   *
   * @param {string} key
   * @return {*}  {Promise<string>}
   * @memberof UserService
   */
  @Trace()
  public readFromCache(key: string): Promise<string | null> {
    return RedisHelper.getAsync(key);
  }

  /**
   * Writes to caching layer
   *
   * @param {string} key
   * @param {string} value
   * @param {number} [expiration=DEFAULT_CACHE_EXPIRATION]
   * @return {Promise<void>}
   * @memberof UserService
   */
  @Trace()
  public writeToCache(key: string, value: string, expiration: number = DEFAULT_CACHE_EXPIRATION): Promise<void> {
    return new Promise<void>((resolve) => {
      RedisHelper.setAsync(key, value, expiration);
      resolve();
    });
  }
}
