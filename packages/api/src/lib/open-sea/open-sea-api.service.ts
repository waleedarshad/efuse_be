import axiosLib from "axios";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { OPEN_SEA_API_KEY } from "@efuse/key-store";
import { IMyntAsset } from "../../backend/interfaces/mynt";

const axios = axiosLib.create({
  baseURL: "https://api.opensea.io/api/v1",
  headers: { "X-API-KEY": OPEN_SEA_API_KEY }
});

export interface OpenSeaAssetQuery {
  token_ids?: string;
  owner?: string;
  asset_contract_address?: string;
  asset_contract_addresses?: string;
  order_by?: string;
  order_direction?: string;
  offset?: string;
  limit?: string; // Defaults to 20, capped at 50.
  collection?: string;
}

/**
 * The OpenseaApiService is intended to interact with
 * the external API provided by OpenSea.
 */

@Service("opensea-api.service")
export class OpenSeaApiService extends BaseService {
  /**
   * Retrieve the assets from opensea and return Mynt Asset.
   *
   * @param query - AssetQuery object
   *
   * @returns <IMyntAsset> assets object
   * @memberof OpenseaApiService
   */
  @Trace()
  public async getOpenSeaAssets(query: OpenSeaAssetQuery): Promise<IMyntAsset[]> {
    const response = await axios.get(`/assets`, { params: query });

    return this.translateResponseObjects(response.data.assets);
  }

  private translateResponseObjects(rawAssets: any[]): IMyntAsset[] {
    const translatedObjects = rawAssets.map((asset) => {
      const translatedTraits: any = {};
      for (const trait of asset.traits) {
        translatedTraits[trait.trait_type] = trait.value;
      }

      return <IMyntAsset>(<unknown>{
        openSeaId: asset.id,
        tokenId: asset.token_id,
        numSales: asset.num_sales,
        backgroundColor: asset.background_color,
        imageUrl: asset.image_url,
        imagePreviewUrl: asset.image_preview_url,
        imageUrlThumbnail: asset.image_thumbnail_url,
        imageUrlOriginal: asset.image_original_url,
        animationUrl: asset.animation_url,
        animationOriginalUrl: asset.animation_original_url,
        name: asset.name,
        description: asset.description,
        externalLink: asset.external_link,
        openSeaLink: asset.permalink,
        decimals: asset.decimals,
        owner: {
          user: {
            username: asset.owner.user.username
          },
          profileImageUrl: asset.owner.profile_img_url,
          address: asset.owner.address
        },
        assetContract: {
          name: asset.asset_contract.name,
          address: asset.asset_contract.address,
          type: asset.asset_contract.type,
          schemaName: asset.asset_contract.schema_name,
          sellerFeeBasisPoints: asset.asset_contract.seller_fee_basis_points,
          buyerFeeBasisPoints: asset.asset_contract.buyer_fee_basis_points,
          description: asset.asset_contract.description,
          tokenSymbol: asset.asset_contract.symbol,
          imageUrl: asset.asset_contract.image_url,
          stats: asset.asset_contract.stats,
          traitStats: asset.asset_contract.trait_stats,
          externalLink: asset.asset_contract.external_link,
          wikiLink: asset.asset_contract.wiki_link
        },
        collection: {
          name: asset.collection.name,
          slug: asset.collection.slug,
          editors: asset.collection.editors,
          hidden: asset.collection.hidden,
          featured: asset.collection.featured,
          createdDate: asset.collection.created_date,
          description: asset.collection.description,
          imageUrl: asset.collection.image_url,
          largeImageUrl: asset.collection.large_image_url,
          featuredImageUrl: asset.collection.featured_image_url,
          stats: asset.collection.stats,
          displayData: asset.collection.display_data,
          payoutAddress: asset.collection.payout_address,
          traitStats: asset.collection.trait_stats,
          externalLink: asset.collection.external_link,
          wikiLink: asset.collection.wiki_link
        },
        creator: {
          user: {
            username: asset.creator.user.username
          },
          profileImageUrl: asset.creator.profile_image_url,
          address: asset.creator.address
        },
        traits: translatedTraits
      });
    });

    return translatedObjects;
  }
}
