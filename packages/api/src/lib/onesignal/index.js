const axios = require("axios");

const { ONESIGNAL_API_KEY, ONESIGNAL_APP_ID } = require("@efuse/key-store");

// todo: investigate
// @ts-ignore
const onesignal = axios.create({
  baseURL: `https://onesignal.com/api/v1/apps/${ONESIGNAL_APP_ID}/`,
  headers: {
    Authorization: `Basic ${ONESIGNAL_API_KEY}`
  }
});

// Send an object (key-value) of tags to link to a onesignal user, if one exists.
const updateUserTags = async (userId, tags) => {
  const response = await onesignal.put(`users/${userId}`, { tags });

  return response;
};

module.exports.updateUserTags = updateUserTags;
