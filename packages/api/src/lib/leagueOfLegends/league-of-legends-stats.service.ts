import { FilterQuery, QueryOptions } from "mongoose";
import {
  LeagueOfLegendsStats,
  LeagueOfLegendsStatsModel
} from "../../backend/models/league-of-legends/LeagueOfLegendsStat";
import { ILeagueOfLegendsStats } from "../../backend/interfaces/leagueOfLegends";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("league-of-legends-stats.service")
export class LeagueOfLegendsStatsService extends BaseService {
  private model: LeagueOfLegendsStatsModel<ILeagueOfLegendsStats> = LeagueOfLegendsStats;

  public async findOne(filter: FilterQuery<ILeagueOfLegendsStats>): Promise<ILeagueOfLegendsStats | null>;
  public async findOne(
    filter: FilterQuery<ILeagueOfLegendsStats>,
    projection: any
  ): Promise<ILeagueOfLegendsStats | null>;
  public async findOne(
    filter: FilterQuery<ILeagueOfLegendsStats>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<ILeagueOfLegendsStats | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<ILeagueOfLegendsStats>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ILeagueOfLegendsStats | null> {
    const leagueStats = await this.model.findOne(filter, projection, options);

    return leagueStats;
  }

  public async find(filter: FilterQuery<ILeagueOfLegendsStats>): Promise<ILeagueOfLegendsStats[]>;
  public async find(filter: FilterQuery<ILeagueOfLegendsStats>, projection?: any): Promise<ILeagueOfLegendsStats[]>;
  public async find(
    filter: FilterQuery<ILeagueOfLegendsStats>,
    projection: any,
    options?: QueryOptions | null | undefined
  ): Promise<ILeagueOfLegendsStats[]>;
  @Trace()
  public async find(
    filter: FilterQuery<ILeagueOfLegendsStats>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ILeagueOfLegendsStats[]> {
    const leagueStats = await this.model.find(filter, projection, options);
    return leagueStats;
  }
}
