/**
 * @summary League of Legends queue types for league info
 */
export enum LeagueOfLegendsQueueType {
  rankedSolo5By5 = "RANKED_SOLO_5x5"
}
