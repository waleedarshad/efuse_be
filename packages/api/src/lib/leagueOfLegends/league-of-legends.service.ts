import { Failure } from "@efuse/contracts";
import axios, { AxiosResponse } from "axios";
import { Types } from "mongoose";
import { clone } from "lodash";

import {
  LeagueOfLegendsChampionData,
  LeagueOfLegendsChampionDataModel
} from "../../backend/models/league-of-legends/LeagueOfLegendsChampionData";
import {
  LeagueOfLegendsStats,
  LeagueOfLegendsStatsModel
} from "../../backend/models/league-of-legends/LeagueOfLegendsStat";
import {
  LeagueOfLegendsVersionData,
  LeagueOfLegendsVersionDataModel
} from "../../backend/models/league-of-legends/LeagueOfLegendsVersionData";
import {
  IChampionMasteryDTO,
  ILeagueEntryDTO,
  ILeagueOfLegendsStats,
  ISummonerDTO
} from "../../backend/interfaces/leagueOfLegends";
import { ILeagueOfLegendsChampionData } from "../../backend/interfaces/leagueOfLegends/league-of-legends-champion-data";
import { ILeagueOfLegendsVersionData } from "../../backend/interfaces/leagueOfLegends/league-of-legends-version-data";
import { IUser } from "../../backend/interfaces/user";
import { IUserLeagueOfLegendsInfo } from "../../backend/interfaces/user-league-of-legends-info";
import { User } from "../../backend/models/User";

import { BaseService } from "../base/base.service";
import { LEAGUE_OF_LEGENDS } from "../metrics";
import { S3ContentTypes, S3Service } from "../aws/s3.service";
import { Service, Trace } from "../decorators";

import { LeagueOfLegendsQueueService } from "./league-of-legends-api-queue.service";
import { LeagueOfLegendsRankTierMapper } from "./league-of-legends-rank-tier-mapper";
import { LeagueOfLegendsQueueType } from "./league-of-legends.enum";

import { io } from "../../backend/config/get-io-object";
import { LeagueOfLegendsRegion } from "./league-of-legends-region.enum";

const DEFAULT_REGION: string = LeagueOfLegendsRegion.NA1;
const ROOT_BUCKET = "static/league-of-legends";

@Service("league-of-legends.service")
export class LeagueOfLegendsService extends BaseService {
  private model: LeagueOfLegendsStatsModel<ILeagueOfLegendsStats> = LeagueOfLegendsStats;
  private versionModel: LeagueOfLegendsVersionDataModel<ILeagueOfLegendsVersionData> = LeagueOfLegendsVersionData;
  private championModel: LeagueOfLegendsChampionDataModel<ILeagueOfLegendsChampionData> = LeagueOfLegendsChampionData;

  private leagueOfLegendsQueueService: LeagueOfLegendsQueueService = new LeagueOfLegendsQueueService();
  private s3Service: S3Service = new S3Service();

  /**
   * A user can change their name in lol. This will take care of that
   *
   * There have also been issues where a users summoner id does not match up with their nickname. This will also fix that.
   */
  @Trace()
  public async checkUsersLeagueInfoAndCorrectIssues(userId: Types.ObjectId): Promise<void> {
    const user = <IUser>await User.findById(userId).select("leagueOfLegends");
    const leagueInfo = user?.leagueOfLegends;

    // leave because no info to check
    if (!leagueInfo) {
      return;
    }

    const { riotRegion, riotId, riotNickname } = leagueInfo;
    const region = riotRegion || DEFAULT_REGION;

    try {
      let foundRiotInfo: ISummonerDTO | null = null;

      if (riotId) {
        foundRiotInfo = await this.leagueOfLegendsQueueService.getSummonerByID(riotId, region);
      }

      const newLeagueInfo: IUserLeagueOfLegendsInfo = leagueInfo;

      if (foundRiotInfo) {
        // return because nothing wrong
        if (riotNickname && foundRiotInfo.name === riotNickname) {
          return;
        }
        newLeagueInfo.riotNickname = foundRiotInfo?.name;
      } else {
        let foundRiotId: string | null = null;

        if (riotNickname) {
          foundRiotId = await this.leagueOfLegendsQueueService.getSummonerIDByName(riotNickname, region);
        }

        // return because this user has some issues since they don't have riotNickName or riotId. put out error for user
        if (!foundRiotInfo) {
          this.logger?.error(user, "Users riotId and riotNickname do not correspond to any LOL data from Riot");
        }

        if (foundRiotId) {
          newLeagueInfo.riotId = foundRiotId;
        }
      }

      user.leagueOfLegends = newLeagueInfo;

      await user.save();
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Retrieve data for League of Legends including champion names, images, and current version info
   *
   * @param {boolean} force if set to true, the data fetch will happen even if it's not out of date
   *
   * @memberof LeagueOfLegendsService
   * @link https://developer.riotgames.com/docs/lol#data-dragon
   */
  @Trace()
  public async fetchLeagueOfLegendsData(): Promise<void> {
    const service: LeagueOfLegendsService = new LeagueOfLegendsService();
    service.recordMetric(LEAGUE_OF_LEGENDS.FETCH_LEAGUE_DATA_CRON.START);

    try {
      // get the currently saved version of League of Legends Data
      const savedVersion = await service.versionModel.findOne();

      // get the current version from League of Legends public file
      const versions: string[] = await axios
        .get("https://ddragon.leagueoflegends.com/api/versions.json")
        .then((value: AxiosResponse) => <string[]>value.data); // return the json from the response

      if (!versions || versions.length === 0) {
        throw Failure.InternalServerError("Unable to retrieve version from League of Legends");
      }

      const newVersion = versions[0];
      if (savedVersion?.version === newVersion) {
        service.logger?.info("League of Legends has not updated content. Exiting.");

        return;
      }

      service.logger?.info(
        `League of Legends has updated content. Old: ${
          savedVersion?.version ? savedVersion.version : "Undefined"
        }, New: ${versions[0]}`
      );

      // pull the champion data for the latest version

      const championData = await axios
        .get(`http://ddragon.leagueoflegends.com/cdn/${newVersion}/data/en_US/champion.json`)

        .then((value: AxiosResponse) => value.data);

      if (!championData) {
        throw Failure.InternalServerError("Unable to retrieve champion list from League of Legends");
      }

      const championList: ILeagueOfLegendsChampionData[] = LeagueOfLegendsService.convertDownloadedChampionData(
        championData.data
      );

      // attempt to process all the champions asynchronously, this should make it go faster
      const processedChampions: ILeagueOfLegendsChampionData[] = await Promise.all(
        championList.map((champion: ILeagueOfLegendsChampionData) => {
          return service.processDownloadedChampionData(champion);
        })
      );

      if (!processedChampions || processedChampions.length === 0) {
        throw Failure.InternalServerError("Failed to process champions, non were updated.");
      }

      if (savedVersion) {
        savedVersion.version = newVersion;

        await savedVersion.save();
      } else {
        const leagueVersion = <ILeagueOfLegendsVersionData>{ version: newVersion };

        await service.versionModel.create(leagueVersion);
      }

      service.recordMetric(LEAGUE_OF_LEGENDS.FETCH_LEAGUE_DATA_CRON.FINISH);
    } catch (error) {
      service.recordMetric(LEAGUE_OF_LEGENDS.FETCH_LEAGUE_DATA_CRON.ERROR);

      throw service.handleError(error, "Error fetching League of Legends Data (cron)");
    }
  }

  /**
   * Updates user's riot info to match the passed info
   *
   * @param userId
   * @param userLeagueOfLegendsInfo
   *
   * @return {IUserLeagueOfLegendsInfo} The updated data
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async updateRiotInfo(
    userId: string,
    userLeagueOfLegendsInfo: IUserLeagueOfLegendsInfo
  ): Promise<IUserLeagueOfLegendsInfo> {
    this.recordMetric(LEAGUE_OF_LEGENDS.UPDATE_RIOT_INFO.START);

    try {
      // Attempt to verify that the summoner exists.

      const verificationToken = Math.random().toString(36).slice(2, 8).toUpperCase();
      let retrievedUser = <IUser>await User.findOne({ _id: userId }).select("leagueOfLegends");

      if (
        !retrievedUser?.leagueOfLegends ||
        retrievedUser.leagueOfLegends.riotNickname !== userLeagueOfLegendsInfo.riotNickname ||
        retrievedUser.leagueOfLegends.riotRegion !== userLeagueOfLegendsInfo.riotRegion
      ) {
        const newLeagueInfo: IUserLeagueOfLegendsInfo = userLeagueOfLegendsInfo;

        // Get the summoner id, then verify
        const summonerId = await this.leagueOfLegendsQueueService.getSummonerIDByName(
          <string>newLeagueInfo.riotNickname,
          <string>newLeagueInfo.riotRegion
        );

        if (summonerId && summonerId === "") {
          throw Failure.BadRequest("Invalid summoner name");
        }

        newLeagueInfo.verified = false;
        newLeagueInfo.verificationToken = verificationToken;
        newLeagueInfo.riotId = summonerId;

        retrievedUser.leagueOfLegends = newLeagueInfo;

        retrievedUser = await retrievedUser.save({ validateBeforeSave: false });
      }

      if (!retrievedUser?.leagueOfLegends?.riotNickname) {
        throw Failure.InternalServerError("Unable to save riot info", { retrievedUser });
      }
      this.recordMetric(LEAGUE_OF_LEGENDS.UPDATE_RIOT_INFO.FINISH);

      return retrievedUser.leagueOfLegends;
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.UPDATE_RIOT_INFO.ERROR);

      if (error.error) {
        // league returns a json object in error

        const er = JSON.parse(error.error);

        const message =
          er?.stats?.message ||
          `[${<string>userLeagueOfLegendsInfo.riotNickname} | ${<string>(
            userLeagueOfLegendsInfo.riotRegion
          )}] Error getting summoner information. Please check the summoner name and region.`;
        throw this.handleError(Failure.BadRequest(message, error));
      } else {
        throw this.handleError(
          error,
          `[${<string>userLeagueOfLegendsInfo.riotNickname} | ${<string>(
            userLeagueOfLegendsInfo.riotRegion
          )}] Error getting summoner information. Please check the summoner name and region.`
        );
      }
    }
  }

  /**
   * Retrieve and save League of Legends Stats for specified user
   *
   * @param {Types.ObjectId} userID the user to sync stats for
   *
   * @return {Promise<IUser>} the updated user
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async syncUserLeagueOfLegendsStats(userId: Types.ObjectId): Promise<ILeagueOfLegendsStats | undefined> {
    try {
      // we will check league info to be sure nothing is wrong and then continue on
      await this.checkUsersLeagueInfoAndCorrectIssues(userId);

      let user = <IUser>await User.findById(userId).select("leagueOfLegends");

      this.recordMetric(LEAGUE_OF_LEGENDS.SYNC_USERS_LOL_STATS.START);

      if (!user?.leagueOfLegends) {
        this.logger?.warn("Failed to find user with League of Legends config. Exiting.", { user });
        this.recordMetric(LEAGUE_OF_LEGENDS.SYNC_USERS_LOL_STATS.ERROR);
        return;
      }

      if (!user.leagueOfLegends.riotNickname) {
        this.logger?.warn(`Riot nickname not set. League of Legends account has been unverified.`, { user });

        user.leagueOfLegends.verified = false;
        user = await user.save({ validateBeforeSave: false });

        this.recordMetric(LEAGUE_OF_LEGENDS.SYNC_USERS_LOL_STATS.ERROR);
        return;
      }

      let leagueStats: ILeagueOfLegendsStats | undefined;
      leagueStats = await this.getLeagueOfLegendsStats(
        user.leagueOfLegends?.riotNickname,
        user.leagueOfLegends?.riotRegion || DEFAULT_REGION,
        user.leagueOfLegends.riotId
      );

      // riot nickname or id is invalid
      if (!leagueStats) {
        user.leagueOfLegends.verified = false;
        user = await user.save({ validateBeforeSave: false });
        this.logger?.warn(
          `Invalid riot nickname: ${
            user?.leagueOfLegends?.riotNickname ? user.leagueOfLegends.riotNickname : ""
          }. League of Legends account has been unverified.`,
          { user }
        );
      } else {
        const riotId =
          user?.leagueOfLegends?.riotId ||
          leagueStats?.leagueInfo[0]?.summonerId ||
          leagueStats?.championInfo[0]?.summonerId;

        if (user.leagueOfLegends?.stats) {
          const statsDoc = await this.model.findById(user.leagueOfLegends.stats);

          if (statsDoc) {
            statsDoc.leagueInfo = leagueStats.leagueInfo;
            statsDoc.championInfo = leagueStats.championInfo;
            statsDoc.leaderboardRankTier = leagueStats.leaderboardRankTier;
            statsDoc.leaderboardRankTierScore = leagueStats.leaderboardRankTierScore;
            leagueStats = await statsDoc.save();
          }
        } else {
          const newStatsDoc = leagueStats;

          leagueStats = await this.model.create(newStatsDoc);
        }

        user.leagueOfLegends.stats = <Types.ObjectId>leagueStats?._id;
        user.leagueOfLegends.riotId = riotId;
        user = await user.save({ validateBeforeSave: false });
      }

      io.to(user._id).emit("UPDATE_ACCOUNT_STATS", {
        accountName: "leagueOfLegends",
        stats: user.leagueOfLegends
      });

      this.recordMetric(LEAGUE_OF_LEGENDS.SYNC_USERS_LOL_STATS.FINISH);

      return leagueStats;
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.SYNC_USERS_LOL_STATS.ERROR);
      throw this.handleError(error, `Error getting summoner info for user`);
    }
  }

  /**
   * Verify user's currently saved Riot info
   *
   * @param {Types.ObjectId} userID the user to sync stats for
   *
   * @return {Promise<boolean>} Indicates if the riot info was verified
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async verifyLeagueOfLegendsSummonerByUserId(userID: Types.ObjectId): Promise<boolean> {
    this.recordMetric(LEAGUE_OF_LEGENDS.VERIFY_SUMMONER_BY_USER_ID.START);

    try {
      const user = <IUser>await User.findOne({ _id: userID }).select("leagueOfLegends");

      // user's summoner name is already verified!
      if (user.verified) {
        return true;
      }

      if (!user.leagueOfLegends) {
        throw Failure.BadRequest("Missing user's League of Legends information.");
      }

      if (!user.leagueOfLegends.verificationToken) {
        throw Failure.BadRequest("Missing user's Riot verification token.");
      }

      if (!user.leagueOfLegends.riotNickname) {
        throw Failure.BadRequest("Missing user's Riot nickname.");
      }

      // attempt the verification
      const userId = await this.verifyLeagueOfLegendsSummoner(
        user.leagueOfLegends?.verificationToken,
        user.leagueOfLegends?.riotNickname,
        user.leagueOfLegends?.riotRegion
      );

      let isVerified = userId !== "";
      // update the user's verification status
      if (isVerified) {
        user.leagueOfLegends.verified = true;
        user.leagueOfLegends.riotId = userId;
        const updatedUser = await user.save({ validateBeforeSave: false });

        // reset on save to verify it was saved
        isVerified = <boolean>updatedUser.leagueOfLegends?.verified;

        // update the user's stats for the first time, asynchronously using promise.
        // if we need this to be done before we return the verification status, use await
        this.syncUserLeagueOfLegendsStats(updatedUser._id).catch((error) => {
          throw Failure.InternalServerError("Failed to retrieve user's League of Legends stats", error);
        });
      }

      this.recordMetric(LEAGUE_OF_LEGENDS.VERIFY_SUMMONER_BY_USER_ID.FINISH);

      return isVerified;
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.VERIFY_SUMMONER_BY_USER_ID.ERROR);
      throw this.handleError(error, `Error getting summoner info for user: ${userID}`);
    }
  }

  /**
   * Verify League of Legends summoner ID
   *
   * @param token Token retrieved from League of Legends console
   * @param summonerName The League of Legends summoner name to verify
   * @param region The region the user plays
   *
   * @return {Promise<string>} Summoner ID
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async verifyLeagueOfLegendsSummoner(
    token: string,
    summonerName: string,
    region = DEFAULT_REGION
  ): Promise<string> {
    this.recordMetric(LEAGUE_OF_LEGENDS.VERIFY_SUMMONER.START);

    try {
      if (!summonerName || summonerName === "") {
        throw Failure.BadRequest("Summoner name is missing.");
      }

      if (!token || token === "") {
        throw Failure.BadRequest("Token is missing.");
      }

      // Get the summoner id, then verify
      const summonerId = await this.leagueOfLegendsQueueService.getSummonerIDByName(summonerName, region);
      const result = await this.leagueOfLegendsQueueService.verifyLeagueOfLegendsCode(token, summonerId, region);

      this.recordMetric(LEAGUE_OF_LEGENDS.VERIFY_SUMMONER.FINISH);

      return result ? summonerId : "";
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.VERIFY_SUMMONER.ERROR);

      if (error.error) {
        // league returns a json object in error

        const er = JSON.parse(error.error);

        const message =
          er?.stats?.message ||
          `[${summonerName} | ${region}] Error getting summoner information. Please check the summoner name and region.`;

        throw this.handleError(Failure.BadRequest(message, error));
      } else {
        throw this.handleError(
          error,
          `[${summonerName}] Error getting summoner information. Please check the summoner name and region.`
        );
      }
    }
  }

  /**
   * Get a user's League of Legends stats
   *
   * @param {Types.ObjectId} userID the user to sync stats for
   *
   * @return {Promise<ILeagueOfLegendsStats>}
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async getUserLeagueOfLegendsStats(userID: Types.ObjectId): Promise<ILeagueOfLegendsStats> {
    this.recordMetric(LEAGUE_OF_LEGENDS.GET_USER_STATS.START);

    try {
      const user = <IUser>await User.findOne({ _id: userID }).select("leagueOfLegends");

      if (!user?.leagueOfLegends?.riotNickname) {
        throw Failure.NotFound("User does not have a riot nickname set");
      }

      if (!user?.leagueOfLegends?.stats) {
        throw Failure.NotFound("User does not have riot stats");
      }

      const statsDoc = await this.model.findById(user?.leagueOfLegends?.stats);

      if (!statsDoc) {
        throw Failure.NotFound("Unable to find user's League of Legends stats");
      }

      this.recordMetric(LEAGUE_OF_LEGENDS.GET_USER_STATS.FINISH);

      return statsDoc;
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.GET_USER_STATS.ERROR);

      throw this.handleError(error, `Error getting summoner info for user: ${userID}`);
    }
  }

  /**
   * Retrieve League of Legends stats for any summoner/region combination
   *
   * @param summonerName The League of Legends summoner name to verify
   * @param region The region the user plays
   *
   * @return {Promise<ILeagueOfLegendsStats>}
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async getLeagueOfLegendsStats(
    summonerName: string,
    riotRegion: string = DEFAULT_REGION,
    riotId?: string
  ): Promise<ILeagueOfLegendsStats | undefined> {
    this.recordMetric(LEAGUE_OF_LEGENDS.GET_LEAGUE_OF_LEGENDS_STATS.START);

    try {
      let riotSummonerId = riotId;

      if (!riotSummonerId) {
        const summonerId = await this.leagueOfLegendsQueueService.getSummonerIDByName(summonerName, riotRegion);

        // no summoner found
        if (!summonerId || summonerId === "") {
          return;
        }
        riotSummonerId = summonerId;
      }

      // exit if no id could be found
      if (!riotSummonerId) {
        this.logger?.warn(`No riot id could be found: ${summonerName} - ${riotRegion}`);
        return;
      }

      const leagueEntryDTOs = await this.leagueOfLegendsQueueService.getLeagueEntriesForSummoner(
        riotSummonerId,
        riotRegion
      );
      const championMasteryDTOs = await this.leagueOfLegendsQueueService.getChampionMasteriesForSummoner(
        riotSummonerId,
        riotRegion
      );

      let leaderboardRankTier = "";
      let leaderboardRankTierScore = 0;

      let mappedLeagueEntryDTos: ILeagueEntryDTO[] = [];

      // set image urls
      mappedLeagueEntryDTos = leagueEntryDTOs?.map((league) => {
        const mappedLeague = clone(league);

        mappedLeague.tierImage = `${ROOT_BUCKET}/ranked-emblems/${league.tier.toUpperCase()}.png`;

        if (mappedLeague.queueType === LeagueOfLegendsQueueType.rankedSolo5By5) {
          const { score, rankTier } = this.mapRankTierCombinationToScore(mappedLeague);
          leaderboardRankTier = rankTier;
          leaderboardRankTierScore = score;
        }

        return mappedLeague;
      });

      let mappedChampionMasteryDTOs: IChampionMasteryDTO[] = [];

      mappedChampionMasteryDTOs = await Promise.all(
        championMasteryDTOs?.map(async (champion) => {
          const championData = await this.championModel.findOne({ key: champion.championId.toString() }).lean();
          const mappedChampion = clone(champion);

          mappedChampion.championName = championData?.name;
          mappedChampion.loadingImage = championData?.loadingImage;
          mappedChampion.iconImage = championData?.iconImage;
          mappedChampion.splashImage = championData?.splashImage;

          return mappedChampion;
        })
      );

      this.recordMetric(LEAGUE_OF_LEGENDS.GET_LEAGUE_OF_LEGENDS_STATS.FINISH);

      return <ILeagueOfLegendsStats>{
        leagueInfo: mappedLeagueEntryDTos,
        championInfo: mappedChampionMasteryDTOs,
        leaderboardRankTier,
        leaderboardRankTierScore
      };
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.GET_LEAGUE_OF_LEGENDS_STATS.ERROR);

      throw this.handleError(
        error,
        `Error retrieving stats from League Of Legend API: ${summonerName} | ${riotRegion}`
      );
    }
  }

  /**
   * Process individual champion information from Riot.
   * This method does the downloading/uploading of the images and saving of the champion data in our database.
   *
   * @param champion The champion to process
   *
   * @return {ILeagueOfLegendsChampionData} the updated and processed data
   *
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public async processDownloadedChampionData(
    champion: ILeagueOfLegendsChampionData
  ): Promise<ILeagueOfLegendsChampionData> {
    this.recordMetric(LEAGUE_OF_LEGENDS.PROCESS_DOWNLOADED_CHAMPION_DATA.START);

    const rootBucket = `${ROOT_BUCKET}/champions`;
    try {
      const splashBucket = `${rootBucket}/${champion.key}_splash.jpg`;
      const splashSuccess = await this.s3Service.downloadMediaAndUploadToS3(
        champion.sourceSplashImage,
        splashBucket,
        S3ContentTypes.JPEG
      );

      const iconBucket = `${rootBucket}/${champion.key}_icon.jpg`;
      const iconSuccess = await this.s3Service.downloadMediaAndUploadToS3(
        champion.sourceIconImage,
        iconBucket,
        S3ContentTypes.JPEG
      );

      const loadingBucket = `${rootBucket}/${champion.key}_loading.jpg`;
      const loadingSuccess = await this.s3Service.downloadMediaAndUploadToS3(
        champion.sourceLoadingImage,
        loadingBucket,
        S3ContentTypes.JPEG
      );

      if (!splashSuccess || !iconSuccess || !loadingSuccess) {
        throw Failure.InternalServerError("Failed to move League of Legends images to S3", champion);
      }

      const existingChampion = await this.championModel.findOne({ key: champion.key });

      let savedChampion: ILeagueOfLegendsChampionData;
      if (existingChampion) {
        existingChampion.version = champion.version;
        existingChampion.name = champion.name;

        existingChampion.sourceIconImage = champion.sourceIconImage;
        existingChampion.sourceSplashImage = champion.sourceSplashImage;
        existingChampion.sourceLoadingImage = champion.sourceLoadingImage;

        existingChampion.splashImage = splashBucket;
        existingChampion.iconImage = iconBucket;
        existingChampion.loadingImage = loadingBucket;

        savedChampion = await existingChampion.save();
      } else {
        const newChampion = champion;
        newChampion.splashImage = splashBucket;
        newChampion.iconImage = iconBucket;
        newChampion.loadingImage = loadingBucket;

        savedChampion = await this.championModel.create(newChampion);
      }

      this.recordMetric(LEAGUE_OF_LEGENDS.PROCESS_DOWNLOADED_CHAMPION_DATA.FINISH);

      return savedChampion;
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.PROCESS_DOWNLOADED_CHAMPION_DATA.ERROR);
      throw this.handleError(error, "Error processing downloaded champion data");
    }
  }

  /**
   *  Convert Riot's raw champion data to a usable format (ILeagueOfLegendsChampionData[])
   *
   * @param championData The raw data from Riot's JSON file.
   *
   * @return {ILeagueOfLegendsChampionData[]}
   *
   * @link https://developer.riotgames.com/docs/lol#data-dragon
   * @memberof LeagueOfLegendsService
   */
  @Trace()
  public static convertDownloadedChampionData(championData: any): ILeagueOfLegendsChampionData[] {
    // map the champion data to ILeagueOfLegendsChampionData
    const championList: ILeagueOfLegendsChampionData[] = Object.entries(championData).map((value: [string, any]) => {
      const { key, id, version, image }: { key: string; id: string; version: string; image: { full: string } } =
        value[1];

      return <ILeagueOfLegendsChampionData>{
        key,
        name: id,
        version,
        sourceIconImage: `http://ddragon.leagueoflegends.com/cdn/${version}/img/champion/${image.full}`,
        sourceLoadingImage: `http://ddragon.leagueoflegends.com/cdn/img/champion/loading/${id}_0.jpg`,
        sourceSplashImage: `http://ddragon.leagueoflegends.com/cdn/img/champion/splash/${id}_0.jpg`,
        iconImage: "",
        loadingImage: "",
        splashImage: ""
      };
    });

    return championList;
  }

  /**
   * @summary Map Rank Tier Combination to a score
   *
   * @param {ILeagueEntryDTO} leagueInfo
   *
   * @return {}
   */
  @Trace()
  private mapRankTierCombinationToScore(leagueInfo: ILeagueEntryDTO): { score: number; rankTier: string } {
    let score = 0;
    let rankTier = "";
    const tier = leagueInfo.tier?.toUpperCase();
    const rank = leagueInfo.rank?.toUpperCase();
    const rankTierCombination = `${tier}_${rank}`.trim();

    const rankTierScore: number | undefined = <number | undefined>LeagueOfLegendsRankTierMapper[rankTierCombination];

    if (rankTierScore) {
      score = rankTierScore;
    }

    rankTier = rankTierCombination;

    return { score, rankTier };
  }
}
