export const LeagueOfLegendsRankTierMapper = {
  // IRON
  IRON_IV: 1,
  IRON_III: 2,
  IRON_II: 3,
  IRON_I: 4,
  IRON: 4,

  // BRONZE
  BRONZE_IV: 5,
  BRONZE_III: 6,
  BRONZE_II: 7,
  BRONZE_I: 8,
  BRONZE: 8,

  // SILVER
  SILVER_IV: 9,
  SILVER_III: 10,
  SILVER_II: 11,
  SILVER_I: 12,
  SILVER: 12,

  // GOLD
  GOLD_IV: 13,
  GOLD_III: 14,
  GOLD_II: 15,
  GOLD_I: 16,
  GOLD: 16,

  // PLATINUM
  PLATINUM_IV: 17,
  PLATINUM_III: 18,
  PLATINUM_II: 19,
  PLATINUM_I: 20,
  PLATINUM: 20,

  // DIAMOND
  DIAMOND_IV: 21,
  DIAMOND_III: 22,
  DIAMOND_II: 23,
  DIAMOND_I: 24,
  DIAMOND: 24,

  // MASTER
  MASTER_IV: 25,
  MASTER_III: 26,
  MASTER_II: 27,
  MASTER_I: 28,
  MASTER: 28,

  // GRANDMASTER
  GRANDMASTER_IV: 29,
  GRANDMASTER_III: 30,
  GRANDMASTER_II: 31,
  GRANDMASTER_I: 30,
  GRANDMASTER: 30,

  // CHALLENGER
  CHALLENGER_IV: 31,
  CHALLENGER_III: 32,
  CHALLENGER_II: 33,
  CHALLENGER_I: 34,
  CHALLENGER: 34
};
