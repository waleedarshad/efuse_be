import { Failure } from "@efuse/contracts";
import { Queue, Job } from "bull";

import * as Config from "../../async/config";
import { ILeagueEntryDTO, IChampionMasteryDTO, ISummonerDTO } from "../../backend/interfaces/leagueOfLegends";

import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { RiotLoLApiService } from "../riot-lol-api.service";
import { LeagueOfLegendsRegion } from "./league-of-legends-region.enum";

// LOL base rate limiter is 30,000 jobs per 10 minutes or ~ (50/s)
// Riot is very particular about their limits
// We are dropping them down by 1 to try to avoid this
// Reference here if adding new endpoints: https://developer.riotgames.com/app/486039/apis
const LOL_API_LIMITS = {
  BASE: {
    max: 49,
    duration: 1000 // 1 second
  },
  CHAMPION_MASTERIES: {
    max: 199,
    duration: 6000 // 6 seconds
  },
  ENTRIES: {
    max: 5,
    duration: 6000 // 6 seconds
  },
  SUMMONERS: {
    max: 159,
    duration: 6000 // 6 seconds
  },
  THIRD_PARTY: {
    max: 49,
    duration: 1000 // 1 second
  }
};

const BASE_QUEUE_NAME = "league-of-legends-api-calls";
const CHAMPION_MASTERY_QUEUE_NAME = "lol-champion-mastery-api-calls";
const LEAGUE_ENTRIES_QUEUE_NAME = "lol-entries-api-calls";
const SUMMONER_BY_NAME_QUEUE_NAME = "lol-summoner-by-name-api-calls";
const SUMMONER_BY_ID_QUEUE_NAME = "lol-summoner-by-id-api-calls";
const VERIFY_CODE_QUEUE_NAME = "lol-verify-code-api-calls";

const LOL_QUEUE_SERVICE = "league-of-legends-queue.service";

const enum QueueMethods {
  getChampionMasteriesForSummonerQueued = "getChampionMasteriesForSummonerQueued",
  getLeagueEntriesForSummonerQueued = "getLeagueEntriesForSummonerQueued",
  getSummonerIDByNameQueued = "getSummonerIDByNameQueued",
  getSummonerByIDQueued = "getSummonerByIDQueued",
  verifyLeagueOfLegendsCodeQueued = "verifyLeagueOfLegendsCodeQueued"
}

@Service(LOL_QUEUE_SERVICE)
export class LeagueOfLegendsQueueService extends BaseService {
  private static leagueQueue: Map<string, Queue> = new Map<string, Queue>();
  private static lolChampionMasteryQueue: Map<string, Queue> = new Map<string, Queue>();
  private static lolEntriesQueue: Map<string, Queue> = new Map<string, Queue>();
  private static lolSummonerQueue: Map<string, Queue> = new Map<string, Queue>();
  private static lolVerifyCodeQueue: Map<string, Queue> = new Map<string, Queue>();

  /**
   * Use `riotApi` getter not this member
   */
  private static $riotApi: RiotLoLApiService;
  private static get riotApi(): RiotLoLApiService {
    if (!LeagueOfLegendsQueueService.$riotApi) {
      LeagueOfLegendsQueueService.$riotApi = new RiotLoLApiService();
    }

    return LeagueOfLegendsQueueService.$riotApi;
  }

  @Trace({ name: "InitAsyncProcessors", service: LOL_QUEUE_SERVICE })
  public static InitAsyncProcessors(): void {
    const regions = Object.values(LeagueOfLegendsRegion);

    const service = new LeagueOfLegendsQueueService();
    regions.forEach((region) => {
      const newQueue = LeagueOfLegendsQueueService.LeagueBaseQueue(region);
      service.logger?.info(`Created LoL Queue: ${newQueue.name}`);
    });
  }

  /**
   * The LeagueQueue is a rate limited implementation of the Bull Queue used to execute ALL queries to the Riot API.
   * When one of the public function on the `LeagueOfLegendsQueueService` are called this queue is used to execute the
   * underlying api call.
   *
   * Note: Riot rate limits based on region, thus why we have separate queues
   *
   * @memberof LeagueOfLegendsQueueService
   */
  @Trace({ name: "LeagueQueue", service: LOL_QUEUE_SERVICE })
  private static async LeagueQueue(job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>): Promise<any> {
    const { region } = job.data.params;

    let localJob: Job;
    switch (job.data.funcName) {
      case QueueMethods.getChampionMasteriesForSummonerQueued:
        localJob = await LeagueOfLegendsQueueService.LOLChampionMasteriesForSummonerQueue(region).add(job.data);
        break;
      case QueueMethods.getLeagueEntriesForSummonerQueued:
        localJob = await LeagueOfLegendsQueueService.LOLEntriesForSummonerQueue(region).add(job.data);
        break;
      case QueueMethods.getSummonerIDByNameQueued:
        localJob = await LeagueOfLegendsQueueService.LOLSummonerIDByNameQueue(region).add(job.data);
        break;
      case QueueMethods.getSummonerByIDQueued:
        localJob = await LeagueOfLegendsQueueService.LOLSummonerByIDQueue(region).add(job.data);
        break;
      case QueueMethods.verifyLeagueOfLegendsCodeQueued:
        localJob = await LeagueOfLegendsQueueService.LOLVerifyCodeQueue(region).add(job.data);
        break;
      default:
        throw Failure.InternalServerError(`No queue service function named ${<string>job.data.funcName}`);
    }

    const result = await localJob.finished();

    return result;
  }

  /**
   * The LeagueBaseQueue is a rate limited implementation of the Bull Queue used to execute ALL queries to the Riot API.
   * When one of the public function on the `LeagueOfLegendsQueueService` are called this queue is used to execute the
   * underlying api call.
   *
   * Note: Riot rate limits based on region, thus why we have separate queues
   *
   * @memberof LeagueOfLegendsQueueService
   */
  @Trace({ name: "LeagueBaseQueue", service: LOL_QUEUE_SERVICE })
  private static LeagueBaseQueue(regionName: string): Queue {
    const queueName = `${BASE_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!LeagueOfLegendsQueueService.leagueQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const lolQueue: Queue = <Queue>Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsQueueService.leagueQueue[queueName] = lolQueue;
      } else {
        // create a new queue
        LeagueOfLegendsQueueService.leagueQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: LOL_API_LIMITS.BASE,
          defaultJobOptions: { removeOnComplete: true }
        });

        LeagueOfLegendsQueueService.leagueQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>) => LeagueOfLegendsQueueService.LeagueQueue(job)
        );
      }
    }

    return <Queue>LeagueOfLegendsQueueService.leagueQueue[queueName];
  }

  @Trace({ name: "LOLChampionMasteriesForSummonerQueue", service: LOL_QUEUE_SERVICE })
  private static LOLChampionMasteriesForSummonerQueue(regionName: string): Queue {
    const queueName = `${CHAMPION_MASTERY_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!LeagueOfLegendsQueueService.lolChampionMasteryQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const lolQueue: Queue = <Queue>Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsQueueService.lolChampionMasteryQueue[queueName] = lolQueue;
      } else {
        // create a new queue
        LeagueOfLegendsQueueService.lolChampionMasteryQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: LOL_API_LIMITS.CHAMPION_MASTERIES,
          defaultJobOptions: { removeOnComplete: true }
        });

        LeagueOfLegendsQueueService.lolChampionMasteryQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>) => {
            const { summonerId, region } = job.data.params;

            return LeagueOfLegendsQueueService.getChampionMasteriesForSummonerQueued(summonerId, region);
          }
        );
      }
    }

    return <Queue>LeagueOfLegendsQueueService.lolChampionMasteryQueue[queueName];
  }

  @Trace({ name: "LOLEntriesForSummonerQueue", service: LOL_QUEUE_SERVICE })
  private static LOLEntriesForSummonerQueue(regionName: string): Queue {
    const queueName = `${LEAGUE_ENTRIES_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!LeagueOfLegendsQueueService.lolEntriesQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const lolQueue: Queue = <Queue>Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsQueueService.lolEntriesQueue[queueName] = lolQueue;
      } else {
        // create a new queue
        LeagueOfLegendsQueueService.lolEntriesQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: LOL_API_LIMITS.ENTRIES,
          defaultJobOptions: { removeOnComplete: true }
        });

        LeagueOfLegendsQueueService.lolEntriesQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>) => {
            const { summonerId, region } = job.data.params;

            return LeagueOfLegendsQueueService.getLeagueEntriesForSummonerQueued(summonerId, region);
          }
        );
      }
    }

    return <Queue>LeagueOfLegendsQueueService.lolEntriesQueue[queueName];
  }

  @Trace({ name: "LOLSummonerIDByNameQueue", service: LOL_QUEUE_SERVICE })
  private static LOLSummonerIDByNameQueue(regionName: string): Queue {
    const queueName = `${SUMMONER_BY_NAME_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!LeagueOfLegendsQueueService.lolSummonerQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const lolQueue: Queue = <Queue>Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsQueueService.lolSummonerQueue[queueName] = lolQueue;
      } else {
        // create a new queue
        LeagueOfLegendsQueueService.lolSummonerQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: LOL_API_LIMITS.SUMMONERS,
          defaultJobOptions: { removeOnComplete: true }
        });

        LeagueOfLegendsQueueService.lolSummonerQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>) => {
            const { nickname, region } = job.data.params;

            return LeagueOfLegendsQueueService.getSummonerIDByNameQueued(nickname, region);
          }
        );
      }
    }

    return <Queue>LeagueOfLegendsQueueService.lolSummonerQueue[queueName];
  }

  @Trace({ name: "LOLSummonerByIDQueue", service: LOL_QUEUE_SERVICE })
  private static LOLSummonerByIDQueue(regionName: string): Queue {
    const queueName = `${SUMMONER_BY_ID_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!LeagueOfLegendsQueueService.lolSummonerQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const lolQueue: Queue = <Queue>Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsQueueService.lolSummonerQueue[queueName] = lolQueue;
      } else {
        // create a new queue
        LeagueOfLegendsQueueService.lolSummonerQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: LOL_API_LIMITS.SUMMONERS,
          defaultJobOptions: { removeOnComplete: true }
        });

        LeagueOfLegendsQueueService.lolSummonerQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>) => {
            const { summonerId, region } = job.data.params;

            return LeagueOfLegendsQueueService.getSummonerByIDQueued(summonerId, region);
          }
        );
      }
    }

    return <Queue>LeagueOfLegendsQueueService.lolSummonerQueue[queueName];
  }

  @Trace({ name: "LOLVerifyCodeQueue", service: LOL_QUEUE_SERVICE })
  private static LOLVerifyCodeQueue(regionName: string): Queue {
    const queueName = `${VERIFY_CODE_QUEUE_NAME}:${regionName.toLowerCase()}`;

    if (!LeagueOfLegendsQueueService.lolVerifyCodeQueue[queueName]) {
      // attempt to get existing queue before creating a new one
      const lolQueue: Queue = <Queue>Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsQueueService.lolVerifyCodeQueue[queueName] = lolQueue;
      } else {
        // create a new queue
        LeagueOfLegendsQueueService.lolVerifyCodeQueue[queueName] = <Queue>Config.initQueue(queueName, {
          limiter: LOL_API_LIMITS.THIRD_PARTY,
          defaultJobOptions: { removeOnComplete: true }
        });

        LeagueOfLegendsQueueService.lolVerifyCodeQueue[queueName].process(
          (job: Job<{ funcName: QueueMethods; params: LOLQueueParams }>) => {
            const { token, summonerId, region } = job.data.params;

            return LeagueOfLegendsQueueService.verifyLeagueOfLegendsCodeQueued(token, summonerId, region);
          }
        );
      }
    }

    return <Queue>LeagueOfLegendsQueueService.lolVerifyCodeQueue[queueName];
  }

  /**
   * Retrieve the a user's summoner ID based on their summoner nickname and region.
   * Note: this function creates a queue task in order to not exceed rate limits, it may be significantly delayed.
   *
   * @param nickname
   * @param region
   *
   * @returns <string> summoner id
   * @memberof LeagueOfLegendsQueueService
   */
  @Trace()
  public async getSummonerIDByName(nickname: string, region: string): Promise<string> {
    const data = {
      funcName: QueueMethods.getSummonerIDByNameQueued,
      params: {
        nickname,
        region
      }
    };

    const job = await LeagueOfLegendsQueueService.LeagueBaseQueue(region).add(data, { priority: 1 });

    const result = await job.finished();

    return <string>result;
  }

  /**
   * Retrieve the a user's summoner info based on their summoner id.
   * Note: this function creates a queue task in order to not exceed rate limits, it may be significantly delayed.
   *
   * @param summonerId
   * @param region
   *
   * @returns <string> summoner id
   * @memberof LeagueOfLegendsQueueService
   */
  @Trace()
  public async getSummonerByID(summonerId: string, region: string): Promise<ISummonerDTO | null> {
    const data = {
      funcName: QueueMethods.getSummonerByIDQueued,
      params: {
        summonerId,
        region
      }
    };

    const job = await LeagueOfLegendsQueueService.LeagueBaseQueue(region).add(data, { priority: 1 });

    const result = await job.finished();

    return <ISummonerDTO | null>result;
  }

  /**
   *
   * @param summonerId
   * @param region
   */
  @Trace()
  public async getLeagueEntriesForSummoner(summonerId: string, region: string): Promise<ILeagueEntryDTO[]> {
    const data = {
      funcName: QueueMethods.getLeagueEntriesForSummonerQueued,
      params: {
        summonerId,
        region
      }
    };

    const job = await LeagueOfLegendsQueueService.LeagueBaseQueue(region).add(data, { priority: 2 });

    return <ILeagueEntryDTO[]>await job.finished();
  }

  /**
   * Retrieve the champion mastery data for a specified summoner id
   * Note: this function creates a queue task in order to not exceed rate limits, it may be significantly delayed.
   *
   * @param summonerId
   * @param region
   *
   * @returns Promise<IChampionMasterDTO[]> Array of champion mastery data objects
   * @memberof LeagueOfLegendsQueueService
   */
  @Trace()
  public async getChampionMasteriesForSummoner(summonerId: string, region: string): Promise<IChampionMasteryDTO[]> {
    const data = {
      funcName: QueueMethods.getChampionMasteriesForSummonerQueued,
      params: {
        summonerId,
        region
      }
    };

    const job = await LeagueOfLegendsQueueService.LeagueBaseQueue(region).add(data, { priority: 2 });

    return <IChampionMasteryDTO[]>await job.finished();
  }

  /**
   * Verify a user's League of Legends account via the generated token
   * Note: this function creates a queue task in order to not exceed rate limits, it may be significantly delayed.
   *
   * @param summonerId
   * @param region
   *
   * @returns Promise<boolean> True if the token was successfully verified
   * @memberof LeagueOfLegendsQueueService
   */
  @Trace()
  public async verifyLeagueOfLegendsCode(token: string, summonerId: string, region: string): Promise<boolean> {
    const data = {
      funcName: QueueMethods.verifyLeagueOfLegendsCodeQueued,
      params: {
        token,
        summonerId,
        region
      }
    };

    const job = await LeagueOfLegendsQueueService.LeagueBaseQueue(region).add(data, { priority: 1 });

    return <boolean>await job.finished();
  }

  @Trace()
  private static async getSummonerIDByNameQueued(nickname: string, region: string): Promise<string> {
    const data = await LeagueOfLegendsQueueService.riotApi.fetchSummonerInfoByName(nickname, region);

    return data?.id ? data.id : "";
  }

  @Trace()
  private static async getSummonerByIDQueued(summonerId: string, region: string): Promise<ISummonerDTO | null> {
    const data = await LeagueOfLegendsQueueService.riotApi.fetchSummonerInfoByID(summonerId, region);

    return data;
  }

  @Trace()
  private static async getLeagueEntriesForSummonerQueued(
    summonerId: string,
    region: string
  ): Promise<ILeagueEntryDTO[]> {
    const results = await LeagueOfLegendsQueueService.riotApi.fetchLeagueEntriesForSummoner(summonerId, region);

    return results || [];
  }

  @Trace()
  private static async getChampionMasteriesForSummonerQueued(
    summonerId: string,
    region: string
  ): Promise<IChampionMasteryDTO[]> {
    const results = await LeagueOfLegendsQueueService.riotApi.fetchChampionMasteriesForSummoner(summonerId, region);

    return results ? results.splice(0, 3) : [];
  }

  @Trace()
  private static async verifyLeagueOfLegendsCodeQueued(
    token: string,
    summonerId: string,
    region: string
  ): Promise<boolean> {
    const result = await LeagueOfLegendsQueueService.riotApi.fetchThirdPartyCode(summonerId, region);

    return result === token;
  }
}

interface LOLQueueParams {
  token: string;
  summonerId: string;
  region: string;
  nickname: string;
}
