import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import { LEAGUE_OF_LEGENDS } from "../metrics";
import { LeagueOfLegendsAsyncService } from "./league-of-legends-async.service";
import { LeagueOfLegendsService } from "./league-of-legends.service";

const LOL_CRON_SERVICE = "leagueof-of-legends-cron.service";

@Service(LOL_CRON_SERVICE)
export class LeagueOfLegendsCronService extends BaseService {
  public constructor() {
    super();
  }

  /**
   * Users can change their ingame name and that will mess up some things.
   * This will check verified LOL users nicknames and update them.
   * It will also check riot id for issues.
   */
  @Trace({ name: "CheckUsersLOLInfoCron", service: LOL_CRON_SERVICE })
  public static async CheckUsersLOLInfoCron(): Promise<void> {
    const cronService = new LeagueOfLegendsCronService();
    const asyncService = new LeagueOfLegendsAsyncService();

    try {
      cronService.recordMetric(LEAGUE_OF_LEGENDS.CHECK_USER_LEAGUE_DATA_CRON.START);

      cronService.logger?.info(`Start queuing users to check League of Legends info {Cron}.`);

      await asyncService.getUsersAndQueueCheckLOLInfo();

      cronService.recordMetric(LEAGUE_OF_LEGENDS.CHECK_USER_LEAGUE_DATA_CRON.FINISH);

      cronService.logger?.info(`Finished queuing users to check League of Legends info {Cron}.`);
    } catch (error) {
      cronService.recordMetric(LEAGUE_OF_LEGENDS.CHECK_USER_LEAGUE_DATA_CRON.ERROR);

      throw cronService.handleError(error, "Unable to sync all league of legends stats.");
    }
  }

  /**
   * @summary Retrieve stats for users that have a Riot Nickname configured.
   * This static method is meant be used as a cron task.
   *
   * @return {Promise<IUser[]>}
   * @memberof LeagueOfLegendsService
   */
  @Trace({ name: "SyncAllLeagueOfLegendsStatsCron", service: LOL_CRON_SERVICE })
  public static async SyncAllLeagueOfLegendsStatsCron(): Promise<void> {
    const cronService = new LeagueOfLegendsCronService();
    const asyncService = new LeagueOfLegendsAsyncService();

    try {
      cronService.recordMetric(LEAGUE_OF_LEGENDS.SYNC_ALL_LOL_STATS_CRON.START);
      cronService.logger?.info(`Start Queuing All League of Legends users for stat refreshes {Cron}.`);

      await asyncService.getUsersAndQueueStatRefreshes();

      cronService.logger?.info(`Finished Queuing All League of Legends users for stat refreshes {Cron}.`);
      cronService.recordMetric(LEAGUE_OF_LEGENDS.SYNC_ALL_LOL_STATS_CRON.FINISH);
    } catch (error) {
      cronService.recordMetric(LEAGUE_OF_LEGENDS.SYNC_ALL_LOL_STATS_CRON.ERROR);

      throw cronService.handleError(error, "Unable to sync all league of legends stats.");
    }
  }

  /**
   * @link https://developer.riotgames.com/docs/lol#data-dragon
   */
  @Trace({ name: "FetchLeagueOfLegendsDataCron", service: LOL_CRON_SERVICE })
  public static async FetchLeagueOfLegendsDataCron(): Promise<void> {
    const cronService = new LeagueOfLegendsCronService();
    const leagueService = new LeagueOfLegendsService();

    try {
      cronService.logger?.info(`Start fetching League of Legends data {Cron}.`);

      await leagueService.fetchLeagueOfLegendsData();

      cronService.logger?.info(`Finished fetching League of Legends data {Cron}.`);
    } catch (error) {
      cronService.recordMetric(LEAGUE_OF_LEGENDS.FETCH_LEAGUE_DATA_CRON.ERROR);

      throw cronService.handleError(error, "Unable to sync all league of legends stats.");
    }
  }
}
