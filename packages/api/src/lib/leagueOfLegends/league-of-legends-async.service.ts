import { Queue } from "bull";
import { Model, Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { IUser } from "../../backend/interfaces";
import { User } from "../../backend/models/User";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";
import * as Config from "../../async/config";
import { LEAGUE_OF_LEGENDS } from "../metrics";
import { UserService } from "../users/user.service";
import { LeagueOfLegendsService } from "./league-of-legends.service";

const LOL_ASYNC_SERVICE = "league-of-legends-async.service";

const LOL_BASE_QUEUE = "league-of-legends-base-queue";

enum LOLQueues {
  CHECK_BASIC_USER_LOL_INFO_QUEUE = "check-basic-user-lol-info-queue",
  REFRESH_USER_STATS_QUEUE = "refresh-user-stats-queue"
}

enum LOLAsyncFunctions {
  SYNC_USERS_LEAGUE_OF_LEGENDS_STATS = "syncUserLeagueOfLegendsStats",
  CHECK_AND_FIX_USER_BASIC_LOL_INFO = "checkUserBasicLOLInfo"
}

export interface ILOLQueueParams {
  userId: Types.ObjectId;
}

@Service(LOL_ASYNC_SERVICE)
export class LeagueOfLegendsAsyncService extends BaseService {
  private static leagueQueue: Map<string, Queue> = new Map<string, Queue>();

  public constructor() {
    super();
  }

  @Trace({ name: "InitAsyncProcessors", service: LOL_ASYNC_SERVICE })
  public static InitAsyncProcessors(): void {
    const service = new LeagueOfLegendsAsyncService();
    const lolQueue: string[] = [LOLQueues.REFRESH_USER_STATS_QUEUE];

    lolQueue.forEach((queueName) => {
      const newQueue = LeagueOfLegendsAsyncService.LOLQueue(queueName);
      service.logger?.info(`Created LOL Queue: ${newQueue.name}`);
    });
  }

  /**
   * LOL Queue allows adding new lol functions for queue more easily
   */
  @Trace({ name: "LOLQueue", service: LOL_ASYNC_SERVICE })
  private static LOLQueue(queue: string): Queue {
    const lolService = new LeagueOfLegendsService();
    const queueName = `${LOL_BASE_QUEUE}:${queue.toLowerCase()}`;

    if (!LeagueOfLegendsAsyncService.leagueQueue[queueName]) {
      // attempt to get existing queue before creating a new one

      const lolQueue: Queue = Config.getQueue(queueName);

      if (lolQueue) {
        LeagueOfLegendsAsyncService.leagueQueue[queueName] = lolQueue;
      } else {
        // create a new queue

        LeagueOfLegendsAsyncService.leagueQueue[queueName] = Config.initQueue(queueName);

        LeagueOfLegendsAsyncService.leagueQueue[queueName].process((job) => {
          const userId = (<ILOLQueueParams>job?.data?.params)?.userId;

          switch (job.data.funcName) {
            case LOLAsyncFunctions.CHECK_AND_FIX_USER_BASIC_LOL_INFO:
              return lolService.checkUsersLeagueInfoAndCorrectIssues(userId);
            case LOLAsyncFunctions.SYNC_USERS_LEAGUE_OF_LEGENDS_STATS:
              return lolService.syncUserLeagueOfLegendsStats(userId);
            default:
              throw Failure.InternalServerError(`No queue service function named ${job?.data?.funcName}`, job?.data);
          }
        });
      }
    }

    return <Queue>LeagueOfLegendsAsyncService.leagueQueue[queueName];
  }

  @Trace()
  public async queueUserAndCheckBasicLOLInfo(userId: Types.ObjectId): Promise<void> {
    try {
      const data = {
        funcName: LOLAsyncFunctions.CHECK_AND_FIX_USER_BASIC_LOL_INFO,
        params: {
          userId
        }
      };

      await LeagueOfLegendsAsyncService.LOLQueue(LOLQueues.CHECK_BASIC_USER_LOL_INFO_QUEUE).add(data);
    } catch (error) {
      throw this.handleError(error, "Unable to check user league of legends stats.");
    }
  }

  @Trace()
  public async queueUserAndRefreshStatsQueue(userId: Types.ObjectId): Promise<void> {
    try {
      const data = {
        funcName: LOLAsyncFunctions.SYNC_USERS_LEAGUE_OF_LEGENDS_STATS,
        params: {
          userId
        }
      };

      await LeagueOfLegendsAsyncService.LOLQueue(LOLQueues.REFRESH_USER_STATS_QUEUE).add(data);
    } catch (error) {
      throw this.handleError(error, "Unable to sync user league of legends stats.");
    }
  }

  @Trace()
  public async getUsersAndQueueCheckLOLInfo(): Promise<void> {
    try {
      const userIdsToCheck: Types.ObjectId[] = await this.getVerifiedLOLUserIds();

      // exit if no users to update
      if (!userIdsToCheck || userIdsToCheck.length === 0) {
        this.logger?.warn(userIdsToCheck, "No users were found to do a LOL info check");
        return;
      }

      this.logger?.info(`Attempting to queue ${userIdsToCheck.length} users for LOL info check.`);

      await Promise.allSettled(userIdsToCheck.map((id) => this.queueUserAndCheckBasicLOLInfo(id)));

      this.logger?.info(`Created LoL check info jobs for ${userIdsToCheck.length} users.`);
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.CHECK_USER_LEAGUE_DATA_CRON.ERROR);
      throw this.handleError(error, "Unable to sync all league of legends stats.");
    }
  }

  @Trace()
  public async getUsersAndQueueStatRefreshes(): Promise<void> {
    try {
      const userIdsToUpdate: Types.ObjectId[] = await this.getVerifiedLOLUserIds();

      // exit if no users to update
      if (!userIdsToUpdate || userIdsToUpdate.length === 0) {
        this.logger?.warn(userIdsToUpdate, "No users were found to update LOL stats");
        return;
      }

      this.logger?.info(`Attempting to queue ${userIdsToUpdate.length} users for LOL stat refresh.`);

      await Promise.allSettled(userIdsToUpdate.map((id) => this.queueUserAndRefreshStatsQueue(id)));

      this.logger?.info(`Created LoL stats refresh jobs for ${userIdsToUpdate.length} users.`);
    } catch (error) {
      this.recordMetric(LEAGUE_OF_LEGENDS.SYNC_ALL_LOL_STATS_CRON.ERROR);
      throw this.handleError(error, "Unable to sync all league of legends stats.");
    }
  }

  @Trace()
  private async getVerifiedLOLUserIds(): Promise<Types.ObjectId[]> {
    try {
      const verifiedUsers: IUser[] = [];
      // get users without any league stats first.
      const usersWithoutLeagueStats = await this.getUsersWithNoLeagueStats();

      if (usersWithoutLeagueStats?.length > 0) {
        verifiedUsers.push(...usersWithoutLeagueStats);
      }

      const usersWithLeagueStats = await this.getUsersWithLeagueStats();

      if (usersWithLeagueStats?.length > 0) {
        verifiedUsers.push(...usersWithLeagueStats);
      }

      return verifiedUsers && verifiedUsers.length > 0 ? verifiedUsers.map((user) => user._id) : [];
    } catch (error) {
      throw this.handleError(error, "Unable to fetch verified LOL users");
    }
  }

  @Trace()
  private getUsersWithNoLeagueStats(): Promise<IUser[]> {
    const userService = new UserService();

    return userService.find(
      {
        "leagueOfLegends.riotNickname": { $ne: null },
        "leagueOfLegends.verified": true,
        "leagueOfLegends.stats": null
      },
      { _id: 1 },
      { lean: true }
    );
  }

  /**
   * Find the users with stats, order by least recently updated
   * TODO: adjust to remove lookup
   */
  @Trace()
  private async getUsersWithLeagueStats(): Promise<IUser[]> {
    const userModel: Model<IUser> = User;

    const foundUsers = await userModel
      .aggregate([
        {
          $match: {
            "leagueOfLegends.riotNickname": {
              $ne: null
            },
            "leagueOfLegends.verified": true,
            "leagueOfLegends.stats": {
              $ne: null
            }
          }
        },
        {
          $lookup: {
            from: "leagueoflegendsstats",
            localField: "leagueOfLegends.stats",
            foreignField: "_id",
            as: "lol_stats"
          }
        },
        {
          $unwind: {
            path: "$lol_stats",
            preserveNullAndEmptyArrays: true
          }
        },
        {
          $addFields: {
            lastUpdate: {
              $ifNull: ["$updatedAt", "$createdAt"]
            }
          }
        },
        {
          $sort: {
            lastUpdate: 1
          }
        }
      ])
      .project("_id");

    return <IUser[]>foundUsers;
  }
}
