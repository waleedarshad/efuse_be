export enum GameEnum {
  GAME_SERVICE = "game.service",
  SYNC_WITH_ALGOLIA_CRON = "SyncGamesWithAlgoliaCron"
}
