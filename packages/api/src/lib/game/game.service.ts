import { AlgoliaIndicesEnum } from "../algolia/algolia.enum";
import { AlgoliaService } from "../algolia/algolia.service";
import { Service, Trace } from "../decorators";
import { FeedGameBaseService } from "../feeds/feed-game-base.service";
import { OpportunityGameBaseService } from "../opportunities/opportunity-game-base-service";
import { OrganizationGameBaseService } from "../organizations/organization-game-base.service";
import { UserGameBaseService } from "../users/user-game-base.service";
import { GameBaseService } from "./game-base.service";
import { GameEnum } from "./game.enum";

@Service(GameEnum.GAME_SERVICE)
export class GameService extends GameBaseService {
  public $index = `${process.env.NODE_ENV}_${AlgoliaIndicesEnum.GAMES}`;

  private $opportunityGameBaseService: OpportunityGameBaseService;
  private $userGameBaseService: UserGameBaseService;
  private $organizationGameBaseService: OrganizationGameBaseService;
  private $feedGameBaseService: FeedGameBaseService;
  private $algoliaService: AlgoliaService;

  constructor() {
    super();

    this.$opportunityGameBaseService = new OpportunityGameBaseService();
    this.$userGameBaseService = new UserGameBaseService();
    this.$organizationGameBaseService = new OrganizationGameBaseService();
    this.$feedGameBaseService = new FeedGameBaseService();
    this.$algoliaService = new AlgoliaService();
  }

  @Trace({ name: GameEnum.SYNC_WITH_ALGOLIA_CRON, service: GameEnum.GAME_SERVICE })
  public static async SyncGamesWithAlgoliaCron(): Promise<void> {
    const gameService = new GameService();

    try {
      // Get games cursor
      const gameCursor = gameService.$games.find({}).lean().cursor();

      const gamesToBeSynced: Record<string, unknown>[] = [];

      // Iterate through games & get count of docs using games
      for (let game = await gameCursor.next(); game !== null; game = await gameCursor.next()) {
        const query = { game: game._id as string };

        // Get count of opportunities using this game
        const opportunityCount = await gameService.$opportunityGameBaseService.countDocuments(query);

        // Get count of users using this game
        const userCount = await gameService.$userGameBaseService.countDocuments(query);

        // Get count of organizations using this game
        const organizationCount = await gameService.$organizationGameBaseService.countDocuments(query);

        // Get count of feeds using this game
        const feedCount = await gameService.$feedGameBaseService.countDocuments(query);

        // Build total count
        const totalCount = opportunityCount + userCount + organizationCount + feedCount;

        // Build object & push to array
        gamesToBeSynced.push({ ...game, totalCount, opportunityCount, userCount, organizationCount, feedCount });
      }

      gameService.logger?.info("Syncing games with algolia");

      // Sync with algolia
      await gameService.$algoliaService.syncObjects(gameService.$index, gamesToBeSynced);

      gameService.logger?.info("All games are synced with algolia");
    } catch (error) {
      gameService.logger?.error(error, "Error syncing games with algolia");
    }
  }
}
