import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions, Types } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";
import { sortBy } from "lodash";

import { BaseService } from "../base/base.service";
import { GameModel, Game } from "../../backend/models/game.model";
import { Service, Trace } from "../decorators";
import { PipelineLeaderboardType } from "../leaderboards/pipeline-leaderboard-type.enum";

@Service("game-base.service")
export class GameBaseService extends BaseService {
  protected readonly $games = GameModel;

  public async findOne(filter: FilterQuery<DocumentType<Game>>): Promise<DocumentType<Game> | null>;
  public async findOne(filter: FilterQuery<DocumentType<Game>>, projection: any): Promise<DocumentType<Game> | null>;
  public async findOne(
    filter: FilterQuery<DocumentType<Game>>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<DocumentType<Game> | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<DocumentType<Game>>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<DocumentType<Game> | null> {
    const game = await this.$games.findOne(filter, projection, options);

    return game;
  }

  public async find(filter: FilterQuery<DocumentType<Game>>): Promise<DocumentType<Game>[]>;
  public async find(filter: FilterQuery<DocumentType<Game>>, projection: any): Promise<DocumentType<Game>[]>;
  public async find(
    filter: FilterQuery<DocumentType<Game>>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<DocumentType<Game>[]>;
  @Trace()
  public async find(
    filter: FilterQuery<DocumentType<Game>>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<DocumentType<Game>[]> {
    const games = await this.$games.find(filter, projection, options);
    return sortBy(games, "title");
  }

  /**
   * @summary Update a particular game object
   *
   * @param {QueryConditions} condition
   * @param {Partial<DocumentType<Game>>} gameBody
   *
   * @returns {Promise<DocumentType<Game>>}
   */
  @Trace()
  public async update(
    filter: FilterQuery<DocumentType<Game>>,
    gameBody: Partial<DocumentType<Game>>
  ): Promise<DocumentType<Game>> {
    // Make sure params are passed
    if (!gameBody) {
      throw Failure.BadRequest("gameBody is missing");
    }

    // Update the corresponding object as per condition
    const updatedGame: DocumentType<Game> | null = await this.$games
      .findOneAndUpdate(filter, { ...gameBody }, { new: true })
      .lean();

    // Make sure game object exists
    if (!updatedGame) {
      throw Failure.UnprocessableEntity("game not found");
    }

    return updatedGame;
  }

  /**
   * @summary Find game by ID
   *
   * @param {Types.ObjectId | string} gameId
   *
   * @returns {Promise<DocumentType<Game>>}
   */
  @Trace()
  public async findById(gameId: Types.ObjectId | string): Promise<DocumentType<Game>> {
    if (!gameId) {
      throw Failure.BadRequest("gameId param is missing");
    }

    if (!this.isValidObjectId(gameId)) {
      throw Failure.BadRequest("Invalid gameId");
    }

    const game: DocumentType<Game> | null = await this.findOne({ _id: gameId });

    if (!game) {
      throw Failure.UnprocessableEntity("Game not found");
    }

    return game;
  }

  /**
   * Find the game by its title.
   * @param gameTitle
   * @returns
   */
  @Trace()
  public async findByTitle(gameTitle: string): Promise<DocumentType<Game>> {
    if (!gameTitle) {
      throw Failure.BadRequest("gameTitle param is missing.");
    }

    const game: DocumentType<Game> | null = await this.findOne({ title: gameTitle });

    if (!game) {
      throw Failure.UnprocessableEntity("Game not found");
    }

    return game;
  }

  @Trace()
  public convertTypeToTitle(gameType: PipelineLeaderboardType): string {
    // TODO: It should be possible to automate this.
    switch (gameType) {
      case PipelineLeaderboardType.AIM_LAB:
        return "Aim Lab";
      case PipelineLeaderboardType.LEAGUE_OF_LEGENDS:
        return "League of Legends";
      case PipelineLeaderboardType.VALORANT:
        return "Valorant";
      default:
        throw Failure.UnprocessableEntity("Game type not found.");
    }
  }

  /**
   * @summary Validate gameId before storing in DB
   *
   * @param {Types.ObjectId | string | undefined} gameId
   *
   * @returns {Types.ObjectId | string}
   */
  @Trace()
  public async validateGame(gameId: Types.ObjectId | string | undefined): Promise<Types.ObjectId | undefined> {
    if (!gameId) {
      return;
    }

    await this.findById(gameId);

    return new Types.ObjectId(gameId);
  }
}
