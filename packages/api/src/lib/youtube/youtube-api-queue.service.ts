import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";
import { YOUTUBE_GOOGLE_ID, YOUTUBE_GOOGLE_SECRET } from "@efuse/key-store";
import axiosLib from "axios";
import { Queue } from "bull";
import fetch from "node-fetch";

import { BaseService } from "../base/base.service";
import { ISecure } from "../../backend/interfaces/secure";
import { YoutubeChannelResponse } from "../../backend/models/youtube-channel/youtube-channel-response.model";
import { OAuthCredentialService } from "../oauth-credential.service";
import { Service, Trace } from "../decorators";
import * as Config from "../../async/config";
import { Secure } from "../../backend/models/Secure";

const QUEUE_NAME = "youtube-api-calls";

@Service("youtube-api-queue.service")
export class YoutubeQueueService extends BaseService {
  private static youtubeQueue: Queue;

  /**
   * Initialize Youtube Queue
   *
   * @memeberof YoutubeQueueService
   */
  @Trace()
  private static get YoutubeQueue(): Queue {
    if (!YoutubeQueueService.youtubeQueue) {
      YoutubeQueueService.youtubeQueue = Config.initQueue(QUEUE_NAME);

      YoutubeQueueService.YoutubeQueue.process(async (job) => {
        const { token, refreshToken, owner, ownerKind } = job.data.params;

        switch (job.data.funcName) {
          case "getChannelDetails":
            return YoutubeQueueService.getYoutubeStatsQueued(token, refreshToken, owner, ownerKind);
          default:
            throw Failure.InternalServerError(`No queue service function named ${<string>job.data.funcName}`);
        }
      }).catch((error) => {
        const youtubeQueueService = new YoutubeQueueService();
        youtubeQueueService.logger?.error(error, "Error resolving YoutubeQueue promise");
      });
    }
    return YoutubeQueueService.youtubeQueue;
  }

  /**
   * Get youtube channel detail
   *
   * @param token
   *
   * @returns <YoutubeChannelResponse> youtube channels stats
   * @memberof YoutubeQueueService
   */
  @Trace()
  public async getChannelDetails(
    refreshToken: string,
    accessToken: string,
    owner: string,
    ownerKind: OwnerKind
  ): Promise<YoutubeChannelResponse> {
    const data = {
      funcName: "getChannelDetails",
      params: {
        token: accessToken,
        refreshToken,
        owner,
        ownerKind
      }
    };

    const job = await YoutubeQueueService.YoutubeQueue.add(data, { priority: 1 });
    const result = <YoutubeChannelResponse>await job.finished();

    return result;
  }

  /**
   * Retrieve Youtube channel stats.
   *
   * @param token
   *
   * @returns youtube channel stats
   * @memberof YoutubeQueueService
   */
  @Trace()
  private static async getYoutubeStatsQueued(
    token: string,
    refreshToken: string,
    owner: string,
    ownerKind: OwnerKind
  ): Promise<YoutubeChannelResponse | unknown> {
    try {
      const channelStats = await this.getChannelInfo(token);
      return channelStats;
    } catch (error) {
      if (error.response.status === 401) {
        // refresh token
        const service = new YoutubeQueueService();
        const accessToken = await service.getAccessToken(refreshToken);

        // TODO: remove this once users are moved to new clout cards
        if (ownerKind === OwnerKind.USER) {
          const secureObject = <ISecure>await Secure.findOne({
            user: owner
          }).exec();

          secureObject.google.accessToken = accessToken;
          secureObject.google.refreshToken = refreshToken;

          await secureObject.save();
        }

        // update the oauth credentials for this owner's google account
        const oauthCredentialService = new OAuthCredentialService();
        await oauthCredentialService.updateOne(
          { owner, ownerKind },
          {
            accessToken,
            refreshToken
          }
        );

        const channelStats = await this.getChannelInfo(token);
        return channelStats;
      }
      return <unknown>error;
    }
  }

  @Trace()
  private static async getChannelInfo(token: string): Promise<YoutubeChannelResponse> {
    const axios = axiosLib.create({
      baseURL: "https://youtube.googleapis.com",
      headers: { Authorization: `Bearer ${token}`, Accept: "application/json" }
    });

    const youtube = await axios.get(
      `/v3/channels?part=snippet%2CcontentDetails%2Cstatistics&mine=true&key=${YOUTUBE_GOOGLE_ID}`
    );
    return <YoutubeChannelResponse>youtube.data;
  }

  @Trace()
  public async getAccessToken(refreshToken: string): Promise<string> {
    const tokenDetails = await fetch("https://accounts.google.com/o/oauth2/token", {
      method: "POST",
      body: JSON.stringify({
        client_id: YOUTUBE_GOOGLE_ID,
        client_secret: YOUTUBE_GOOGLE_SECRET,
        refresh_token: refreshToken,
        grant_type: "refresh_token"
      })
    });

    const token = await tokenDetails.json();

    return token.access_token;
  }
}
