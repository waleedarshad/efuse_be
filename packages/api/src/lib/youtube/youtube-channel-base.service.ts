import { DocumentType } from "@typegoose/typegoose";
import { YoutubeChannelModel, YoutubeChannel } from "../../backend/models/youtube-channel/youtube-channel.model";
import { BaseModelService } from "../base";
import { Service } from "../decorators";

@Service("youtube-channel-base.service")
export class YoutubeChannelBaseService extends BaseModelService<DocumentType<YoutubeChannel>> {
  constructor() {
    super(YoutubeChannelModel);
  }
}
