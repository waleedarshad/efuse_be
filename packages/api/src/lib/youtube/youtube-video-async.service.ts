import { Types } from "mongoose";
import { Queue } from "bull";
import { AxiosResponse } from "axios";

import { OwnerKind } from "@efuse/entities";

import { Service, Trace } from "../decorators";
import { YoutubeAsyncService } from "./youtube-async.service";
import { YoutubeChannelBaseService } from "./youtube-channel-base.service";
import { YoutubeCronsEnum } from "./youtube.enum";
import Config from "../../async/config";
import { IOrganization, IUser, IYoutubePlaylistItem, IYoutubePlaylistResponse } from "../../backend/interfaces";
import { YoutubeVideoBaseService } from "./youtube-video-base.service";
import { YoutubeVideo } from "../../backend/models/youtube-video/youtube-video.model";

const SERVICE = "youtube-video-async.service";

@Service(SERVICE)
export class YoutubeVideoAsyncService extends YoutubeAsyncService {
  private $youtubeChannelBaseService: YoutubeChannelBaseService;
  private $youtubeVideoBaseService: YoutubeVideoBaseService;

  private $fetchYoutubeVideosQueue: Queue;
  private $storeYoutubeVideosQueue: Queue;
  private $syncYoutubeVideosQueue: Queue;

  constructor() {
    super();

    this.$youtubeChannelBaseService = new YoutubeChannelBaseService();
    this.$youtubeVideoBaseService = new YoutubeVideoBaseService();

    this.$fetchYoutubeVideosQueue = Config.initQueue("fetchYoutubeVideos", { limiter: { max: 5, duration: 1000 } });
    this.$storeYoutubeVideosQueue = Config.initQueue("storeYoutubeVideos");
    this.$syncYoutubeVideosQueue = Config.initQueue("syncYoutubeVideos", { limiter: { max: 5, duration: 1000 } });
  }

  /**
   * @summary Sync youtube videos for user/organization using bull queue
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {string | undefined} lastPaginationCursor
   *
   * @returns {Promise<void>}
   */
  public async syncYoutubeVideos(
    owner: Types.ObjectId | string,
    ownerKind: OwnerKind,
    lastPaginationCursor?: string
  ): Promise<void> {
    try {
      // Find youtube-channel object
      const youtubeChannel = await this.$youtubeChannelBaseService.findOne({ owner: String(owner), ownerKind });

      // Since we need channel ID to fetch playlist, Make sure it exists
      if (!youtubeChannel || !youtubeChannel.contentDetails?.relatedPlaylists?.uploads) {
        this.logger?.error({ owner, ownerKind }, "Youtube Channel not found");

        // Mark cron as failed
        this.markCronAsFailure(YoutubeCronsEnum.YOUTUBE_VIDEOS, owner, "Youtube Channel not found", ownerKind);

        // exit
        return;
      }

      // Fetch Videos & store in DB
      this.fetchYoutubeVideosAsync(
        owner,
        ownerKind,
        youtubeChannel.contentDetails.relatedPlaylists.uploads,
        lastPaginationCursor
      );
    } catch (error) {
      this.logger?.error(error, "Error syncing youtube videos");

      // Update cron
      this.markCronAsFailure(YoutubeCronsEnum.YOUTUBE_VIDEOS, owner, error.message, ownerKind);
    }
  }

  /**
   * @summary Sync youtube videos for user/organization using bull queue
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {string | undefined} lastPaginationCursor
   *
   * @returns {Promise<void>}
   */
  public syncYoutubeVideosAsync(
    owner: Types.ObjectId | string,
    ownerKind: OwnerKind,
    lastPaginationCursor?: string
  ): void {
    this.$syncYoutubeVideosQueue
      .add({ owner, ownerKind, lastPaginationCursor })
      .catch((error) => this.logger?.error(error, "Error adding jobs to queue syncYoutubeVideosQueue"));
  }

  /**
   * @summary Fetch videos from youtube using playlist API
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {string} youtubeChannelId
   * @param {string | undefined} lastPaginationCursor
   * @param {string | undefined} nextPageCursor
   *
   * @link https://developers.google.com/youtube/v3/docs/playlistItems/list
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async fetchYoutubeVideos(
    owner: Types.ObjectId | string,
    ownerKind: string,
    youtubeChannelId: string,
    lastPaginationCursor?: string,
    nextPageCursor?: string
  ): Promise<void> {
    try {
      // Only fetch page when we get a new next page cursor
      if (lastPaginationCursor && lastPaginationCursor === nextPageCursor) {
        this.logger?.info(
          { owner, ownerKind, youtubeChannelId, lastPaginationCursor, nextPageCursor },
          "Videos on this page has already been stored"
        );

        // Mark as success
        this.markCronAsSuccess(YoutubeCronsEnum.YOUTUBE_VIDEOS, owner, ownerKind);

        return;
      }

      const part = "snippet,contentDetails"; // Additional data to receive in response
      const maxResults = 50; // This is the max results that youtube returns per page

      // Build Query params
      let queryParams = `part=${part}&maxResults=${maxResults}&playlistId=${youtubeChannelId}`;

      // Append next page token
      if (nextPageCursor) {
        queryParams += `&pageToken=${nextPageCursor}`;
      }

      // Fetch Videos
      const response: AxiosResponse<IYoutubePlaylistResponse> = await this.$http.get(`/playlistItems?${queryParams}`);

      const { data } = response;

      // Make sure we have received videos
      if (data?.items?.length === 0) {
        this.logger?.info({ queryParams, owner, ownerKind }, "No videos found");
        return;
      }

      // Store Pagination Cursor
      if (data.nextPageToken && !data.prevPageToken) {
        this.track(YoutubeCronsEnum.YOUTUBE_VIDEOS, owner, { lastPaginationCursor: data.nextPageToken }, ownerKind);
      }

      // Store in DB
      this.storeYoutubeVideosAsync(owner, ownerKind, data.items);

      // Fetch next Page
      if (data.nextPageToken) {
        this.fetchYoutubeVideosAsync(owner, ownerKind, youtubeChannelId, lastPaginationCursor, data.nextPageToken);
      } else {
        // Mark as success
        this.markCronAsSuccess(YoutubeCronsEnum.YOUTUBE_VIDEOS, owner, ownerKind);
      }
    } catch (error) {
      this.logger?.error({ error, owner, ownerKind, youtubeChannelId }, "Error while fetching youtube videos");

      this.markCronAsFailure(YoutubeCronsEnum.YOUTUBE_VIDEOS, owner, error.message, ownerKind);
    }
  }

  /**
   * @summary Fetch videos from youtube using playlist API via bull queue
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {string} youtubeChannelId
   * @param {string | undefined} lastPaginationCursor
   * @param {string | undefined} nextPageCursor
   *
   * @returns {Promise<void>}
   */
  private fetchYoutubeVideosAsync(
    owner: Types.ObjectId | string,
    ownerKind: string,
    youtubeChannelId: string,
    lastPaginationCursor?: string,
    nextPageCursor?: string
  ): void {
    this.$fetchYoutubeVideosQueue
      .add({ owner, ownerKind, youtubeChannelId, lastPaginationCursor, nextPageCursor })
      .catch((error) => this.logger?.error(error, "Error adding youtube videos to queue fetchYoutubeVideosQueue"));
  }

  /**
   * @summary Store playlistItems in DB as YoutubeVideos
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {IYoutubePlaylistItem[]} youtubePlaylistItems
   *
   * @returns {Promise<void>}
   */
  @Trace()
  private async storeYoutubeVideos(
    owner: Types.ObjectId | string,
    ownerKind: string,
    youtubePlaylistItems: IYoutubePlaylistItem[]
  ): Promise<void> {
    try {
      // Make sure videos are present
      if (youtubePlaylistItems.length === 0) {
        this.logger?.info({ owner, ownerKind }, "Empty youtubePlaylistItems received");
        return;
      }

      // Format response to match DB schema
      const youtubeVideos = this.buildYoutubeVideoObject(owner, ownerKind, youtubePlaylistItems);

      // Save in DB
      await this.$youtubeVideoBaseService.bulkUpdate(youtubeVideos);
    } catch (error) {
      this.logger?.error({ error, owner, ownerKind }, "Error storing youtube videos in DB");
    }
  }

  /**
   * @summary Store playlistItems in DB as YoutubeVideos using bull queue
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {IYoutubePlaylistItem[]} youtubePlaylistItems
   *
   * @returns {Promise<void>}
   */
  private storeYoutubeVideosAsync(
    owner: Types.ObjectId | string,
    ownerKind: string,
    youtubePlaylistItems: IYoutubePlaylistItem[]
  ): void {
    this.$storeYoutubeVideosQueue
      .add({ owner, ownerKind, youtubePlaylistItems })
      .catch((error) => this.logger?.error(error, "Error adding youtube videos to queue storeYoutubeVideosQueue"));
  }

  /**
   * @summary Format playlistItem response to match DB schema
   *
   * @param {Types.ObjectId | string} owner
   * @param {string} ownerKind
   * @param {IYoutubePlaylistItem[]} youtubePlaylistItems
   *
   * @returns {Partial<YoutubeVideo>[]}
   */
  private buildYoutubeVideoObject(
    owner: Types.ObjectId | string,
    ownerKind: string,
    youtubePlaylistItems: IYoutubePlaylistItem[]
  ): Partial<YoutubeVideo>[] {
    const youtubeVideos: Partial<YoutubeVideo>[] = youtubePlaylistItems.map((playlistItem) => {
      const { snippet, contentDetails } = playlistItem;

      return <Partial<YoutubeVideo>>{
        owner,
        ownerKind,
        kind: playlistItem.kind,
        etag: playlistItem.etag,
        externalId: playlistItem.id,
        title: snippet?.title,
        description: snippet?.description,
        thumbnails: snippet?.thumbnails,
        playlistId: snippet?.playlistId,
        videoId: contentDetails?.videoId,
        videoPublishedAt: contentDetails?.videoPublishedAt
      };
    });

    return youtubeVideos;
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const youtubeVideoAsyncService = new YoutubeVideoAsyncService();

    // fetchYoutubeVideosQueue
    youtubeVideoAsyncService.$fetchYoutubeVideosQueue
      .process(
        (job: {
          data: {
            owner: Types.ObjectId | string;
            ownerKind: string;
            youtubeChannelId: string;
            lastPaginationCursor?: string;
            nextPageCursor?: string;
          };
        }) => {
          const { owner, ownerKind, youtubeChannelId, lastPaginationCursor, nextPageCursor } = job.data;

          return youtubeVideoAsyncService.fetchYoutubeVideos(
            owner,
            ownerKind,
            youtubeChannelId,
            lastPaginationCursor,
            nextPageCursor
          );
        }
      )
      .catch((error) =>
        youtubeVideoAsyncService.logger?.error(error, "Error while processing fetchYoutubeVideosQueue")
      );

    // storeYoutubeVideosQueue
    youtubeVideoAsyncService.$storeYoutubeVideosQueue
      .process(
        (job: {
          data: { owner: Types.ObjectId | string; ownerKind: string; youtubePlaylistItems: IYoutubePlaylistItem[] };
        }) => {
          const { owner, ownerKind, youtubePlaylistItems } = job.data;

          return youtubeVideoAsyncService.storeYoutubeVideos(owner, ownerKind, youtubePlaylistItems);
        }
      )
      .catch((error) =>
        youtubeVideoAsyncService.logger?.error(error, "Error while processing storeYoutubeVideosQueue")
      );

    // syncYoutubeVideosQueue
    youtubeVideoAsyncService.$syncYoutubeVideosQueue
      .process(
        (job: { data: { owner: Types.ObjectId | string; ownerKind: OwnerKind; lastPaginationCursor?: string } }) => {
          const { owner, ownerKind, lastPaginationCursor } = job.data;

          return youtubeVideoAsyncService.syncYoutubeVideos(owner, ownerKind, lastPaginationCursor);
        }
      )
      .catch((error) => youtubeVideoAsyncService.logger?.error(error, "Error while processing syncYoutubeVideosQueue"));
  }

  /**
   * @summary Cron job to sync user/organization youtube videos
   *
   * @return {Promise<void>}
   */
  @Trace({ service: SERVICE, name: YoutubeCronsEnum.YOUTUBE_VIDEOS })
  public static async SyncYoutubeVideosCron(): Promise<void> {
    const youtubeVideoAsyncService = new YoutubeVideoAsyncService();

    const cronProcessor = (entity: IUser | IOrganization, entityKind: string, lastPaginationCursor?: string) => {
      youtubeVideoAsyncService.syncYoutubeVideosAsync(entity._id, <OwnerKind>entityKind, lastPaginationCursor);
    };

    await youtubeVideoAsyncService.executeCron(YoutubeCronsEnum.YOUTUBE_VIDEOS, cronProcessor);
  }
}
