import { unitOfTime } from "moment";
import axios, { AxiosInstance } from "axios";
import { YOUTUBE_DATA_API_KEY } from "@efuse/key-store";

import { IOrganization, IPopulatedUserOrgTrackCron, IUser } from "../../backend/interfaces";
import { Service, Trace } from "../decorators";
import { TrackCronService } from "../track-cron/track-cron.service";
import { CronEntityKind, CronKindEnum } from "../track-cron/track-cron.enum";
import { YoutubeCronsEnum } from "./youtube.enum";

const POPULATED_FIELDS = "_id deleted status publishStatus";
const LIMIT = 10; // Number of users to process each time cron runs
const INTERVAL = 24;
const UNIT: unitOfTime.DurationConstructor = "h";

const BASE_API_URL = "https://www.googleapis.com/youtube/v3";

const SERVICE = "youtube-async.service";

@Service(SERVICE)
export class YoutubeAsyncService extends TrackCronService<IPopulatedUserOrgTrackCron> {
  protected $http: AxiosInstance;

  constructor() {
    super(
      CronKindEnum.YOUTUBE,
      Object.values(YoutubeCronsEnum),
      CronEntityKind.USER, // default entityKind
      POPULATED_FIELDS,
      LIMIT,
      INTERVAL,
      UNIT
    );

    this.$http = axios.create({ baseURL: BASE_API_URL, params: { key: YOUTUBE_DATA_API_KEY } });
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const youtubeAsyncService = new YoutubeAsyncService();

    // Track Cron async processors
    youtubeAsyncService.asyncProcessors();
  }

  /**
   * @summary Set Cron to run on the basis of TrackCron service
   *
   * @param {string} methodName
   * @param {(entity: IUser | IOrganization) => void} cronProcessor
   *
   * @returns {Promise<void>}
   */
  @Trace()
  protected async executeCron(
    methodName: string,
    cronProcessor: (entity: IUser | IOrganization, entityKind: string, lastPaginationCursor?: string) => void
  ): Promise<void> {
    try {
      const processableEntities: IPopulatedUserOrgTrackCron[] = await this.getEntities(methodName);

      if (processableEntities.length === 0) {
        this.logger?.info(`NO entity found in trackCron for ${methodName}`);
        return;
      }

      for (const processableEntity of processableEntities) {
        const { entity, entityKind } = processableEntity;

        // Make sure user is valid
        const isValid =
          entityKind === CronEntityKind.USER
            ? this.validateUserEntity(methodName, entity as IUser)
            : this.validateOrganizationEntity(methodName, entity as IOrganization);

        if (!isValid) {
          this.logger?.info({ processableEntity }, "Cron can't be processed");

          // Skip iteration
          continue;
        }

        // Fire cron processor
        cronProcessor(entity, entityKind, processableEntity.lastPaginationCursor);

        // Update status
        this.markCronAsProcessing(methodName, entity._id, entityKind);
      }
    } catch (error) {
      this.logger?.error(error, `Error running cron ${methodName}`);
    }
  }
}
