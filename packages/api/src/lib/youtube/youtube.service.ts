import { Failure } from "@efuse/contracts";
import { OAuthServiceKind, OwnerKind } from "@efuse/entities";
import { YOUTUBE_DATA_API_KEY } from "@efuse/key-store";
import { Types } from "mongoose";
import fetch from "node-fetch";
import { DocumentType } from "@typegoose/typegoose";
import { YoutubeChannelBaseService } from "./youtube-channel-base.service";
import { IOAuthCredential } from "../../backend/interfaces";
import { IUser } from "../../backend/interfaces/user";

import { OAuthCredentialService } from "../oauth-credential.service";
import { Secure } from "../../backend/models/Secure";
import { Service, Trace } from "../decorators";
import { User } from "../../backend/models/User";
import { YOUTUBE } from "../metrics";
import { YoutubeChannel } from "../../backend/models/youtube-channel/youtube-channel.model";
import { YoutubeChannelThumbnail, ContentDetail, Statistics } from "../../backend/models/youtube-channel/types";
import { YoutubeChannelResponse } from "../../backend/models/youtube-channel/youtube-channel-response.model";

import { YoutubeCronsEnum } from "./youtube.enum";
import { YoutubeQueueService } from "./youtube-api-queue.service";
import { ObjectId } from "../../backend/types";

@Service("youtube.service")
export class YoutubeService extends YoutubeChannelBaseService {
  private youtubeQueueService: YoutubeQueueService = new YoutubeQueueService();

  @Trace()
  public async getYoutubeStream(userId: Types.ObjectId | string): Promise<{
    youtubeLive: boolean;
    youtubeMediaID?: string;
    isLive: boolean;
  }> {
    try {
      const user: IUser | null = await User.findOne({ _id: userId });
      if (user?.youtubeChannelId) {
        const response = await fetch(
          `https://www.googleapis.com/youtube/v3/search?part=snippet&channelId=${user.youtubeChannelId}&type=video&eventType=live&key=${YOUTUBE_DATA_API_KEY}`
        );

        const responseData = await response.json();

        if (responseData?.items?.length > 0) {
          return {
            youtubeLive: true,

            youtubeMediaID: responseData.items[0].id.videoId,
            isLive: true
          };
        }
      }
      return { youtubeLive: false, isLive: false };
    } catch (error) {
      this.logger?.error(error, "Error while fetching youtube stream");
      throw this.handleError(Failure.BadRequest("Error while fetching youtube stream", error));
    }
  }

  @Trace()
  public async findByOwner(owner: string): Promise<DocumentType<YoutubeChannel> | null> {
    try {
      const youtubeChannel = await this.findOne({ owner }, {}, { lean: true });

      return youtubeChannel;
    } catch (error) {
      throw this.handleError(Failure.BadRequest("Error deleting youtube channel stats. Please try again", error));
    }
  }

  @Trace()
  public async saveChannelInformation(
    refreshToken: string,
    accessToken: string,
    owner: string,
    ownerKind: OwnerKind,
    user?: IUser
  ): Promise<YoutubeChannel> {
    // TODO: this needs updated to support an "owner" and an "ownerkind"
    // this will allow us to store information for organizations, much like we do for auth information
    try {
      const data: YoutubeChannelResponse = await this.youtubeQueueService.getChannelDetails(
        refreshToken,
        accessToken,
        owner,
        ownerKind
      );

      if (!data || !data.items || !data.items[0]) {
        throw Failure.BadRequest("Channel Information not found.");
      }

      const item = data.items[0];
      const itemDetail = item?.snippet;

      const object = <DocumentType<YoutubeChannel>>(<unknown>{
        owner,
        ownerKind,
        kind: item.kind,
        etag: item.etag,
        id: item.id, // this is the channel ID
        title: itemDetail?.title,
        description: itemDetail?.description,
        publishedAt: itemDetail?.publishedAt,
        thumbnails: itemDetail.thumbnails as YoutubeChannelThumbnail,
        contentDetails: item.contentDetails as ContentDetail,
        statistics: item?.statistics as Statistics
      });

      // attempt to find existing youtube channel info by owner/ownerKind
      let youtubeChannel = await this.updateOne(
        { owner, ownerKind },
        { $set: { ...object, updatedAt: new Date() } },
        {
          upsert: true,
          lean: true,
          new: true,
          setDefaultsOnInsert: true
        }
      );

      // TODO: [org-clout] remove this once we convert user clout cards
      // attempt to find by user's saved youtubeChannel (pre-oauthcredential)
      if (!youtubeChannel && user) {
        youtubeChannel = await this.updateOne(
          { _id: user?.youtubeChannel },
          { $set: { ...object, updatedAt: new Date() } },
          {
            upsert: true,
            lean: true,
            new: true,
            setDefaultsOnInsert: true
          }
        );
      }

      // create the youtube channel if we've failed to find an existing one
      if (!youtubeChannel) {
        youtubeChannel = await this.create(object);
      }

      // TODO: [org-clout] remove this once we convert all user clout cards
      if (user) {
        user.youtubeChannel = youtubeChannel._id;
        await user.save({ validateBeforeSave: false });
      }

      this.logger?.info("Youtube stats updated successfully");
      return youtubeChannel;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Remove associated channel
   *
   * @param {ObjectId} YoutubeChannelId
   * @returns {Promise}
   */
  public async removeAssociatedChannel(channelId?: ObjectId, ownerId?: string): Promise<void> {
    if (!channelId && !ownerId) {
      return;
    }
    try {
      // old functionality removes by channel id
      // new functionality removes by owner
      await this.deleteOne({
        $or: [{ _id: channelId }, { $and: [{ owner: { $ne: undefined } }, { owner: ownerId }] }]
      });
    } catch (error) {
      throw this.handleError(Failure.BadRequest("Error deleting youtube channel stats. Please try again", error));
    }
  }

  /**
   * Update youtube channel information periodically
   * This static method is meant be used as a cron task
   *
   * @memberof YoutubeService
   */
  @Trace({ name: YoutubeCronsEnum.YOUTUBE_STATS, service: "youtube.service" })
  public static async SyncAllYoutubeStatsCron(): Promise<void> {
    const youtubeService = new YoutubeService();
    youtubeService.recordMetric(YOUTUBE.SYNC_USERS_YOUTUBE_CHANNEL_STATS.START);

    try {
      // retrieve array of channels that need updated
      const youtubePromises = await YoutubeService.getYoutubeChannelUpdatePromises();

      // TODO: [org-clout] remove the user specific logic once all data is updated
      const youtubePromisesOld = await YoutubeService.getUserYoutubeUpdatePromises();
      youtubePromises.push(...youtubePromisesOld);

      // update 'em all
      await Promise.all(youtubePromises);
      youtubeService.recordMetric(YOUTUBE.SYNC_USERS_YOUTUBE_CHANNEL_STATS.FINISH);
    } catch (error) {
      youtubeService.recordMetric(YOUTUBE.SYNC_USERS_YOUTUBE_CHANNEL_STATS.ERROR);
      throw youtubeService.handleError(Failure.BadRequest("Unable to sync all youtube stats.", error));
    }
  }

  @Trace({ name: "getYoutubeChannelUpdatePromises", service: "youtube.service" })
  private static async getYoutubeChannelUpdatePromises(): Promise<Promise<any>[]> {
    const youtubeService = new YoutubeService();
    const oauthCredentialService = new OAuthCredentialService();

    const googleAccountConnections = await oauthCredentialService.find({ service: OAuthServiceKind.GOOGLE });

    const youtubePromises = googleAccountConnections
      .map((credential: IOAuthCredential) => {
        if (credential.refreshToken && credential.accessToken) {
          return youtubeService
            .saveChannelInformation(
              credential.refreshToken,
              credential.accessToken,
              credential.owner,
              OwnerKind[credential.ownerKind]
            )
            .catch((error) => {
              youtubeService.logger?.info("Error", error);
              throw youtubeService.handleError(error, `Failed to sync youtube stats for owner ${credential.owner}`);
            });
        }
        return null;
      })
      .filter((o) => o !== null);

    return <Promise<YoutubeChannel>[]>youtubePromises;
  }

  /**
   * @deprecated This will be removed once all clout cards are updated to new paths with OAuthEntity storage.
   */
  @Trace({ name: "getUserYoutubeUpdatePromises", service: "youtube.service" })
  private static async getUserYoutubeUpdatePromises(): Promise<Promise<any>[]> {
    const youtubeService = new YoutubeService();
    const users = await User.find({ youtubeChannel: { $ne: null || undefined } }).select("_id, youtubeChannel");

    // Get userIds
    const userIds = users.map((u) => u._id);
    // Get corresponding secure object to get google creds
    const secureObjects = await Secure.find({ user: { $in: userIds } });

    // Build promises
    const youtubePromises = <Promise<any>[]>secureObjects
      .map((secureObject) => {
        // Get user from above array of users

        const user = users.find((u) => String(u._id) === String(secureObject.user));
        if (user) {
          return youtubeService
            .saveChannelInformation(
              secureObject.google.refreshToken,
              secureObject.google.accessToken,
              user._id.toHexString(),
              OwnerKind.USER,
              user
            )
            .catch((error) => {
              youtubeService.logger?.info("Error", error);
              throw youtubeService.handleError(error, `Failed to sync youtube stats for user ${String(user._id)}`);
            });
        }
        return null;
      })
      .filter((p) => p);

    return youtubePromises;
  }
}
