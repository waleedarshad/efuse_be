export enum YoutubeCronsEnum {
  YOUTUBE_VIDEOS = "SyncYoutubeVideosCron",
  YOUTUBE_STATS = "SyncAllYoutubeStatsCron"
}

export enum YoutubeVideoPaginationLimit {
  DEFAULT = 10,
  MAX = 25
}
