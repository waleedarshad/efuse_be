import { Types } from "mongoose";

import { OwnerKind } from "@efuse/entities";
import { Failure } from "@efuse/contracts";

import { Service } from "../decorators";
import { YoutubeVideoBaseService } from "./youtube-video-base.service";
import { OrganizationACLService } from "../organizations/organization-acl.service";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";

@Service("youtube-video.service")
export class YoutubeVideoService extends YoutubeVideoBaseService {
  private $organizationACLService: OrganizationACLService;

  constructor() {
    super();

    this.$organizationACLService = new OrganizationACLService();
  }

  public async validateYoutubeOwnership(
    userId: Types.ObjectId | string,
    youtubeVideoId: Types.ObjectId | string
  ): Promise<void> {
    // Make sure it is a valid ObjectId
    if (!this.isValidObjectId(youtubeVideoId)) {
      throw Failure.BadRequest("Invalid youtubeVideo ID");
    }

    // Get existing video
    const youtubeVideo = await this.findOne({ _id: youtubeVideoId }, {}, { lean: true });

    // Make sure video is present
    if (!youtubeVideo) {
      throw Failure.UnprocessableEntity("Youtube Video not found");
    }

    //  Validate ownership
    if (youtubeVideo.ownerKind === OwnerKind.USER) {
      // Make sure current user is owner
      if (String(youtubeVideo.owner) !== String(userId)) {
        throw Failure.Forbidden("You are not owner of attached youtube video");
      }
    } else {
      // It means video belongs to an organization. Make sure user is owner or captain
      await this.$organizationACLService.authorizedToAccess(userId, youtubeVideo.owner, OrganizationAccessTypes.ANY);
    }
  }
}
