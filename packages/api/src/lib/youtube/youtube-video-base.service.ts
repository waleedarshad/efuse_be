import { FilterQuery, PaginateOptions, PaginateResult } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";

import { IBulkWriteResponse } from "../../backend/interfaces";
import { YoutubeVideo, YoutubeVideoModel } from "../../backend/models/youtube-video/youtube-video.model";
import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";

@Service("youtube-video-base.service")
export class YoutubeVideoBaseService extends BaseModelService<DocumentType<YoutubeVideo>> {
  protected readonly $youtubeVideos = YoutubeVideoModel;

  constructor() {
    super(YoutubeVideoModel);
  }

  /**
   * @summary Bulk update youtube videos
   *
   * @param {YoutubeVideo[]} youtubeVideos
   *
   * @returns {Promise<IBulkWriteResponse>}
   */
  @Trace()
  public async bulkUpdate(youtubeVideos: Partial<YoutubeVideo>[]): Promise<IBulkWriteResponse> {
    // Build bulk operations array
    const bulkOperations: any[] = youtubeVideos.map((youtubeVideo) => {
      // Query to make sure we don't duplicate objects
      const query: Record<string, unknown> = {
        owner: youtubeVideo.owner,
        ownerKind: youtubeVideo.ownerKind,
        externalId: youtubeVideo.externalId
      };

      // Return bulkOperation object to update or create a new object
      return {
        updateOne: {
          filter: query,
          update: { ...youtubeVideo, updatedAt: Date.now() },
          upsert: true, // create new object if it is not already there
          setDefaultsOnInsert: true, // this would set the schema defaults
          ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
        }
      };
    });

    // Execute bulkWrite
    const response = await this.$youtubeVideos.bulkWrite(bulkOperations);

    return {
      upserted: response.upsertedCount || 0,
      updated: response.upsertedCount || 0,
      modified: response.modifiedCount || 0,
      deleted: response.deletedCount || 0,
      matched: response.matchedCount || 0
    };
  }

  /**
   * @summary Get paginated youtube videos
   *
   * @param {FilterQuery<IComment>} filter
   * @param {PaginateOptions} options
   *
   * @returns {Promise<PaginateResult<YoutubeVideo>>}
   */
  @Trace()
  public async getPaginatedYoutubeVideos(
    filter: FilterQuery<DocumentType<YoutubeVideo>>,
    options: PaginateOptions
  ): Promise<PaginateResult<YoutubeVideo>> {
    const youtubeVideos = await this.$youtubeVideos.paginate(filter, options);

    return youtubeVideos;
  }
}
