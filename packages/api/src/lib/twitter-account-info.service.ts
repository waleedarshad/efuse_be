import { Service, Trace } from "@efuse/decorators";

import { BaseModelService } from "./base";
import { ITwitterAccountInfo } from "../backend/interfaces";
import { TwitterAccountInfo } from "../backend/models/twitter-account-info";

@Service("twitter-account-info.service")
export class TwitterAccountInfoService extends BaseModelService<ITwitterAccountInfo> {
  constructor() {
    super(TwitterAccountInfo);
  }

  @Trace()
  public async deleteByOwner(owner: string): Promise<void> {
    await this.deleteOne({ owner });
  }
}
