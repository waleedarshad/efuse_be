import { Failure } from "@efuse/contracts";
import { Types, PaginateResult } from "mongoose";

import { BaseService } from "../base";
import { INote } from "../../backend/interfaces/note";
import { Note, NoteModel } from "../../backend/models/note";
import { Service, Trace } from "../decorators";
import WhitelistHelper from "../../backend/helpers/whitelist_helper";

const ORGANIZATION_FIELDS = "_id name description website status verified profileImage headerImage user slug shortName";

/**
 * @deprecated - new service incoming
 */
@Service("notes.service")
export class NotesService extends BaseService {
  private readonly notes: NoteModel<INote> = Note;
  private relatedModel: string | undefined;
  private ownerType: string | undefined;
  protected readonly failure = Failure;

  constructor(ownerType: string, relatedModel: string) {
    super();

    this.relatedModel = relatedModel;
    this.ownerType = ownerType;
  }

  /**
   * @deprecated - new service incoming
   */
  @Trace()
  public async create(
    author: Types.ObjectId | string,
    owner: Types.ObjectId | string,
    content: string,
    relatedDoc: Types.ObjectId | string
  ): Promise<INote> {
    // Make sure author is valid
    this.throwAuthorError(author);

    // Make sure owner is valid
    this.throwOwnerError(owner);

    // Make sure content is present
    if (!content) {
      throw this.failure.BadRequest("content is required");
    }

    if (this.ownerType === undefined || this.relatedModel === undefined) {
      throw this.failure.ExpectationFailed(
        `When notes service is intitialized it must have owner type and related model to be able to use this function.
        Use "createNote" as an alternative.`
      );
    }

    // Create & return note object
    const doc = <INote>{
      author,
      content,
      relatedDoc,
      relatedModel: this.relatedModel,
      ownerType: this.ownerType,
      owner
    };
    const note = await this.notes.create(doc);
    return note;
  }

  /**
   * @deprecated - new service incoming
   */
  public async paginatedNotes(
    relatedDoc: Types.ObjectId | string,
    owner: Types.ObjectId | string,
    page: number,
    limit: number,
    author: Types.ObjectId | string
  ): Promise<PaginateResult<INote>>;
  /**
   * @deprecated - new service incoming
   */
  @Trace()
  public async paginatedNotes(
    relatedDoc: Types.ObjectId | string,
    owner: Types.ObjectId | string,
    page = 1,
    limit = 10
  ): Promise<PaginateResult<INote>> {
    // Make sure page param is positive
    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    // Make sure limit is not greater than 30
    if (limitParam > 30) {
      limitParam = 30;
    }

    // Make sure relatedDoc ID is valid
    this.throwRelatedDocError(relatedDoc);

    // Make sure owner ID is valid
    this.throwOwnerError(owner);

    // Get paginated notes
    const notes = await this.notes.paginate(
      // todo: investigate & fix
      // @ts-ignore
      { relatedDoc, relatedModel: this.relatedModel, owner, ownerType: this.ownerType },
      {
        page: pageParam,
        limit: limitParam,
        sort: { createdAt: -1 },
        populate: [
          { path: "author", select: WhitelistHelper.allowedUserFields },
          {
            path: "owner",
            select: this.ownerType === "users" ? WhitelistHelper.allowedUserFields : ORGANIZATION_FIELDS
          }
        ],
        lean: true
      }
    );

    return notes;
  }

  /**
   * @deprecated - new service incoming
   */
  @Trace()
  public async delete(noteId: Types.ObjectId | string): Promise<boolean> {
    // Make sure ID is valid
    if (!this.isValidObjectId(noteId)) {
      throw Failure.BadRequest("invalid noteId");
    }

    const deleted = await this.notes.deleteOne({ _id: noteId });

    return deleted?.deletedCount != null && deleted.deletedCount > 0;
  }

  /**
   * @deprecated - new service incoming
   * @summary Throw exception when relatedDoc ID is not valid
   */
  @Trace()
  protected throwRelatedDocError(relatedDoc: Types.ObjectId | string): void {
    this.throwIDError(relatedDoc, "relatedDoc ID is malformed");
  }

  /**
   * @deprecated - new service incoming
   * @summary Throw exception when author ID is not valid
   */
  @Trace()
  protected throwAuthorError(author: Types.ObjectId | string): void {
    this.throwIDError(author, "author ID is malformed");
  }

  /**
   * @deprecated - new service incoming
   * @summary Throw exception when owner ID is not valid
   */

  @Trace()
  protected throwOwnerError(owner: Types.ObjectId | string): void {
    this.throwIDError(owner, "owner ID is malformed");
  }

  /**
   * @deprecated - new service incoming
   */
  @Trace()
  private throwIDError(objectId: Types.ObjectId | string, errorMessage: string): void {
    if (!this.isValidObjectId(objectId)) {
      throw this.failure.BadRequest(errorMessage);
    }
  }
}
