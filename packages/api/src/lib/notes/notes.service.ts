import { Failure } from "@efuse/contracts";
import { Types, PaginateResult } from "mongoose";

import { NoteOwnerType } from "@efuse/entities";
import { BaseModelService } from "../base";
import { INote } from "../../backend/interfaces/note";
import { Note, NoteModel } from "../../backend/models/note";
import { Service, Trace } from "../decorators";
import WhitelistHelper from "../../backend/helpers/whitelist_helper";

// TODO: Remove once FE updated to support GraphQL queries
const ORGANIZATION_FIELDS = "_id name description website status verified profileImage headerImage user slug shortName";

@Service("notes.service")
export class NotesService extends BaseModelService<INote> {
  private readonly notes: NoteModel<INote> = Note;
  private readonly failure = Failure;

  constructor() {
    super(Note);
  }

  // need to do more checks for create than what is done in BaseModelService
  @Trace()
  public async create(item: INote): Promise<INote> {
    try {
      this.checkForRelatedDocError(item.relatedDoc);
      this.checkForOwnerError(item.owner);
      this.checkForIDError(item.author, "author ID is malformed");

      if (!item.content) {
        throw this.failure.BadRequest("Content is required");
      }

      const result: INote = await this.notes.create(item);

      return result;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @deprecated - use "getPaginatedNotes"
   * Temporary until FE is using GraphQL
   *
   */
  @Trace()
  public async getPopulatedPaginatedNotes(
    relatedDoc: Types.ObjectId | string,
    owner: Types.ObjectId | string,
    page = 1,
    limit = 10,
    ownerType: NoteOwnerType
  ): Promise<PaginateResult<INote>> {
    this.checkForRelatedDocError(relatedDoc);
    this.checkForOwnerError(owner);
    // Make sure page param is positive
    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    if (limitParam > 30) {
      limitParam = 30;
    }

    // Get paginated notes
    const notes = await this.notes.paginate(
      { relatedDoc, owner },
      {
        page: pageParam,
        limit: limitParam,
        sort: { createdAt: -1 },
        populate: [
          { path: "author", select: WhitelistHelper.allowedUserFields },
          {
            path: "owner",
            select: ownerType === NoteOwnerType.USERS ? WhitelistHelper.allowedUserFields : ORGANIZATION_FIELDS
          }
        ],
        lean: true
      }
    );

    return notes;
  }

  @Trace()
  public async getPaginatedNotes(
    relatedDoc: Types.ObjectId | string,
    owner: Types.ObjectId | string,
    page = 1,
    limit = 10
  ): Promise<PaginateResult<INote>> {
    this.checkForRelatedDocError(relatedDoc);
    this.checkForOwnerError(owner);

    let pageParam = Number(page);
    if (pageParam < 1) {
      pageParam = 1;
    }

    let limitParam = Number(limit);
    if (limitParam < 1) {
      limitParam = 1;
    }

    if (limitParam > 30) {
      limitParam = 30;
    }

    const notes = await this.notes.paginate(
      { relatedDoc, owner },
      {
        page: pageParam,
        limit: limitParam,
        sort: { createdAt: -1 },
        lean: true
      }
    );

    return notes;
  }

  @Trace()
  private checkForRelatedDocError(relatedDoc: Types.ObjectId | string): void {
    this.checkForIDError(relatedDoc, "relatedDoc ID is malformed");
  }

  @Trace()
  private checkForOwnerError(owner: Types.ObjectId | string): void {
    this.checkForIDError(owner, "owner ID is malformed");
  }

  @Trace()
  private checkForIDError(objectId: Types.ObjectId | string, errorMessage: string): void {
    if (!this.isValidObjectId(objectId)) {
      throw this.failure.BadRequest(errorMessage);
    }
  }
}
