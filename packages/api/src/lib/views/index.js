const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create({ name: "lib/views/index.js" });

const { Feed } = require("../../backend/models/Feed");
const { Opportunity } = require("../../backend/models/Opportunity");

/**
 * @summary Increment post views counter
 *
 * @param {Array} feedIds - Array of feed ids
 */
const incrementFeedViews = async (feedIds) => {
  try {
    await Feed.updateMany({ _id: { $in: feedIds } }, { $inc: { "metadata.views": 1 } });
  } catch (error) {
    logger.error(`[${feedIds}] Error while incrementing Feed count`, error);
  }
};

/**
 * @summary Increment opportunities views counter
 *
 * @param {Array} opportunityIds - Array of opprtunity ids
 */
const incrementOpportunityViews = async (opportunityIds) => {
  try {
    await Opportunity.updateMany({ _id: { $in: opportunityIds } }, { $inc: { "metadata.views": 1 } });
  } catch (error) {
    logger.error(`[${opportunityIds}] Error while incrementing Opportunity count`, error);
  }
};

module.exports.incrementFeedViews = incrementFeedViews;
module.exports.incrementOpportunityViews = incrementOpportunityViews;
