import { DocumentType } from "@typegoose/typegoose";
import { Failure } from "@efuse/contracts";
import { isEmpty } from "lodash";

import { Service, Trace } from "../decorators";
import { BaseModelService } from "../base";
import { ClippyCommand, ClippyCommandModel } from "../../backend/models/clippy/clippy-command.model";
import { ObjectId } from "../../backend/types";
import {
  ShoutoutCommand,
  MuteCommand,
  ReplayCommand,
  StopAllCommand,
  StopCommand,
  WatchCommand,
  UnmuteCommand,
  ResetCommand
} from "./commands";

@Service("clippy-commands.service")
export class ClippyCommandsService extends BaseModelService<DocumentType<ClippyCommand>> {
  constructor() {
    super(ClippyCommandModel);
  }

  /**
   * @summary Get existing clippy commands or generate new commands
   *
   * @param {ObjectId} clippyId
   *
   * @returns {Promise<DocumentType<ClippyCommand>[]>}
   */
  @Trace()
  public async findOrCreateCommands(clippyId: ObjectId): Promise<DocumentType<ClippyCommand>[]> {
    // Get existing commands
    const commands = await this.find({ clippy: clippyId }, {}, { lean: true });

    // If we get commands then return those
    if (commands.length > 0) {
      return commands;
    }

    // If it reaches here then generate new commands
    const commandsToBeCreated = [
      this.generateCommandDefaults(clippyId, UnmuteCommand.defaults),
      this.generateCommandDefaults(clippyId, MuteCommand.defaults),
      this.generateCommandDefaults(clippyId, ReplayCommand.defaults),
      this.generateCommandDefaults(clippyId, ResetCommand.defaults),
      this.generateCommandDefaults(clippyId, ShoutoutCommand.defaults),
      this.generateCommandDefaults(clippyId, StopAllCommand.defaults),
      this.generateCommandDefaults(clippyId, StopCommand.defaults),
      this.generateCommandDefaults(clippyId, WatchCommand.defaults)
    ];

    // Create multiple commands at once
    const newlyCreatedCommands = await this.createMany(commandsToBeCreated);

    // return newly created commands
    return newlyCreatedCommands;
  }

  /**
   * @summary Update a particular command
   *
   * @param {ObjectId} clippyId
   * @param {ObjectId} clippyCommandId
   * @param {Partial<ClippyCommand>} body
   *
   * @returns {Promise<DocumentType<ClippyCommand>>}
   */
  public async updateCommand(clippyId: ObjectId, body: Partial<ClippyCommand>): Promise<DocumentType<ClippyCommand>> {
    if (!body._id) {
      throw Failure.BadRequest("ID is required");
    }

    // Make sure to set only fields which are passed in body.
    const params: Record<string, unknown> = {};

    // Set command
    if (body.command) {
      params.command = body.command?.trim();

      // Find duplicates for the command
      const existingCommand = await this.find(
        { _id: { $ne: body._id }, clippy: clippyId, command: params.command as string },
        {},
        { lean: true }
      );

      // Make sure command is unique
      if (existingCommand.length > 0) {
        throw Failure.BadRequest("Command must be unique");
      }
    }

    // Set sortOrder
    if (body.sortOrder) {
      params.sortOrder = body.sortOrder;
    }

    // Set dateRange
    if (body.dateRange) {
      params.dateRange = body.dateRange;
    }

    // Set allowedRoles
    if (body.allowedRoles) {
      params.allowedRoles = body.allowedRoles;
    }

    // Set minimumViews
    if (body.minimumViews !== undefined && body.minimumViews >= 0) {
      params.minimumViews = body.minimumViews;
    }

    // Set queueClips
    if (body.queueClips !== undefined && body.queueClips !== null) {
      params.queueClips = body.queueClips;
    }

    // Make sure to have some params to update
    if (isEmpty(params)) {
      throw Failure.BadRequest("body is required");
    }

    // Update Commands
    const commands = await this.updateOne({ _id: body._id }, params, { lean: true, new: true });

    return commands;
  }

  /**
   * @summary Generate default command object
   *
   * @param {ObjectId} clippyId
   * @param {Record<string, unknown>} defaults
   *
   * @returns {DocumentType<ClippyCommand>}
   */
  private generateCommandDefaults(clippyId: ObjectId, defaults: Record<string, unknown>): DocumentType<ClippyCommand> {
    return <DocumentType<ClippyCommand>>{ clippy: clippyId, ...defaults };
  }
}
