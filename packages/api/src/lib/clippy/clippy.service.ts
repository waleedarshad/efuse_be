import { DocumentType } from "@typegoose/typegoose";
import { ClippySegmentEventEnum } from "@efuse/entities";

import { BaseModelService } from "../base";
import { Service, Trace } from "../decorators";
import { Clippy, ClippyModel } from "../../backend/models/clippy/clippy.model";
import { ObjectId } from "../../backend/types";
import { ClippyBurnedTokenService } from "./clippy-burned-token.service";
import { EFuseSegmentAnalyticsUtil } from "../efuse-segment-analytics/efuse-segment-analytics.util";
import { ClippyPermissionService } from "./clippy-permission.service";
import { ClippyRoleValueEnum } from "../permissions/enums";

@Service("clippy.service")
export class ClippyService extends BaseModelService<DocumentType<Clippy>> {
  private $clippyBurnedTokenService: ClippyBurnedTokenService;
  private $clippyPermissionService: ClippyPermissionService;

  constructor() {
    super(ClippyModel);

    this.$clippyBurnedTokenService = new ClippyBurnedTokenService();
    this.$clippyPermissionService = ClippyPermissionService.new();
  }

  /**
   * @summary Get clippy object for user or create a new one
   *
   * @param {ObjectId} userId
   *
   * @returns {Promise<DocumentType<Clippy>>}
   */
  @Trace()
  public async findOrCreate(userId: ObjectId): Promise<DocumentType<Clippy>> {
    try {
      // Find existing clippy object
      const clippy = await this.findOne({ user: userId }, {}, { lean: true });

      // Return if we find one
      if (clippy) {
        return clippy;
      }

      // If it reaches here, then user doesn't have an object. Let's create one
      const token = await this.$clippyBurnedTokenService.generateToken();

      // Using upsert of findOneAndUpdate to avoid any race condition
      const newClippy = await this.updateOne(
        { user: userId },
        { user: userId, token, updatedAt: new Date() },
        { upsert: true, new: true, lean: true, setDefaultsOnInsert: true }
      );

      // no need to wait for this to finish so adding a catch
      this.$clippyPermissionService
        .assignEntityClippyRole(userId, ClippyRoleValueEnum.CLIPPY_FREE)
        .catch((error) => this.handleError(error));

      // Tracking when the first time user's clippy config is setup
      EFuseSegmentAnalyticsUtil.track({ userId: String(userId) }, ClippySegmentEventEnum.USER_INITIALIZED);

      return newClippy;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
