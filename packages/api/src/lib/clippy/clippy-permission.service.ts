import { Failure } from "@efuse/contracts";
import { Service, Trace } from "@efuse/decorators";
import { Types } from "mongoose";
import { ObjectId } from "../../backend/types";
import { BaseService } from "../base";
import { ClippyRoleValueEnum, EntityRoleEntityKindEnum, PermissionActionsEnum } from "../permissions/enums";
import { ClippyResourceValueEnum } from "../permissions/enums/resource-enums";
import { IEntityRole, IPermissionsDatabaseService } from "../permissions/interfaces";
import { EFusePermissionsDatabaseService } from "../permissions/services/efuse-permissions-database.service";

@Service("clippy-permission.service")
export class ClippyPermissionService extends BaseService {
  private permissionDatabaseService: IPermissionsDatabaseService;

  constructor(efusePermissionDatabaseService: IPermissionsDatabaseService) {
    super();

    this.permissionDatabaseService = efusePermissionDatabaseService;
  }

  @Trace()
  public static new(): ClippyPermissionService {
    const permissionDatabaseService = EFusePermissionsDatabaseService.new();

    return new ClippyPermissionService(permissionDatabaseService);
  }

  /**
   * roleValue defaults to "clippyFree" if not set.
   * entityKind defaults to "users" if not set.
   */
  @Trace()
  public async assignEntityClippyRole(
    entityId: ObjectId,
    roleValue = ClippyRoleValueEnum.CLIPPY_FREE,
    entityKind = EntityRoleEntityKindEnum.USERS
  ): Promise<IEntityRole> {
    try {
      return this.permissionDatabaseService.assignRoleByRoleValue(roleValue, entityId, entityKind);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * entityKind defaults to "users" if not set.
   */
  @Trace()
  public async entityHasCommand(
    entityId: ObjectId,
    resourceValue: string | ClippyResourceValueEnum,
    entityKind = EntityRoleEntityKindEnum.USERS
  ): Promise<boolean> {
    try {
      return this.permissionDatabaseService.entityHasPermission(
        entityId,
        entityKind,
        resourceValue,
        PermissionActionsEnum.ACCESS
      );
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  public async entityHasRoles(entityId: ObjectId, roleValues: ClippyRoleValueEnum[]): Promise<boolean> {
    try {
      const foundRoles = await this.permissionDatabaseService.findRolesByRoleValues(roleValues);

      const roleIds: Types.ObjectId[] = foundRoles && foundRoles.length > 0 ? foundRoles.map((role) => role._id) : [];

      return this.permissionDatabaseService.hasRoles(entityId, roleIds);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * This is meant to be used with clippy endpoints
   *
   * With this as long as the user has one of the roles then they can access the endpoint.
   * Otherwise it will throw a forbidden error
   *
   * TODO: This will be updated or changed at some point to be more robust. Using roles keeps it quick and simple for now.
   */
  @Trace()
  public async userHasAccess(
    userId: ObjectId,
    roleValue: ClippyRoleValueEnum[],
    forbiddenMessage?: string
  ): Promise<void> {
    try {
      const hasRoles = await this.entityHasRoles(userId, roleValue);

      if (!hasRoles) {
        throw Failure.Forbidden(forbiddenMessage ?? "User does not have access for this clippy resource", {
          userId,
          roleValue,
          hasRoles
        });
      }
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
