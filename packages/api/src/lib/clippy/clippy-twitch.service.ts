import { DocumentType } from "@typegoose/typegoose";
import { Failure } from "@efuse/contracts";
import moment from "moment";
import { ClippyDateRangeEnum, ClippySegmentEventEnum, ClippySortOrderEnum } from "@efuse/entities";
import { random } from "lodash";

import { Service, Trace } from "../decorators";
import { TwitchBaseService } from "../twitch/twitch-base.service";
import {
  ITwitchClipBroadcasterQueryParams,
  ITwitchClipIdQueryParams,
  ITwitchClipPaginationResponse,
  ITwitchClipResponse
} from "../../backend/interfaces";
import { ClippyCommand } from "../../backend/models/clippy/clippy-command.model";
import { ObjectId } from "../../backend/types";
import { ClippyTwitchClip } from "../../backend/models/clippy/clippy-twitch-clip.model";
import { EFuseSegmentAnalyticsUtil } from "../efuse-segment-analytics/efuse-segment-analytics.util";

interface IDateRange {
  startDate: string;
  endDate: string;
}

@Service("clippy-twitch.service")
export class ClippyTwitchService extends TwitchBaseService {
  /**
   * @summary Fetch shoutout clip from twitch
   *
   * @param {ObjectId} userId
   * @param {string} channelName
   * @param {DocumentType<ClippyCommand>} clippyCommand
   *
   * @returns {Promise<ITwitchClipResponse | null>}
   */
  @Trace()
  public async getShoutoutClip(
    userId: ObjectId,
    channelName: string,
    clippyCommand: DocumentType<ClippyCommand>
  ): Promise<ClippyTwitchClip | null> {
    try {
      // Get Broadcaster ID
      const twitchUsers = await this.getUsers(userId, { login: `login=${channelName}` });

      // Make sure we get the user object
      if (twitchUsers.length === 0) {
        throw Failure.BadRequest("Channel not found");
      }

      const twitchUser = twitchUsers[0];

      // Fetch twitch clips
      const [twitchClips, queryParams] = await this.fetchTwitchClipsAndQueryParamsThenCycleDateRangesIfNecessary(
        userId,
        twitchUser.id,
        clippyCommand
      );

      // if we still don't receive a clip simply exit
      if (!twitchClips || twitchClips.data.length === 0) {
        this.logger?.info("No clips available against this channel");

        return null;
      }

      return this.returnRandomTwitchClipOrReturnFirstClip(clippyCommand, twitchClips, userId, queryParams);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Get a specific twitch clip by id
   *
   * @param {ObjectId} userId
   * @param {string} clipId
   *
   * @returns {Promise<ClippyTwitchClip | null>}
   */
  @Trace()
  public async getSpecificClip(userId: ObjectId, clipId: string): Promise<ClippyTwitchClip | null> {
    try {
      const queryParams: ITwitchClipIdQueryParams = { id: `id=${clipId}` };

      // Fetch twitch clip
      const { data } = await this.getClips(userId, queryParams);

      return data.length === 0 ? null : <ClippyTwitchClip>data[0];
    } catch (error) {
      throw this.handleError(error);
    }
  }

  private createQueryToGetTwitchClips(
    twitchUserId: string,
    clippyCommandSortOrder?: ClippySortOrderEnum,
    clippyDateRange?: ClippyDateRangeEnum
  ): ITwitchClipBroadcasterQueryParams {
    // Set broadcaster_id & first param
    const queryParams: ITwitchClipBroadcasterQueryParams = {
      broadcaster_id: `broadcaster_id=${twitchUserId}`,
      first: clippyCommandSortOrder === ClippySortOrderEnum.RANDOM ? 100 : 1
    };

    // Build date range on the basis of user's settings
    const dateRange = this.generateDateRange(clippyDateRange);

    // Set start & end date
    if (dateRange) {
      queryParams.started_at = dateRange.startDate;
      queryParams.ended_at = dateRange.endDate;
    }

    return queryParams;
  }

  /**
   * cycle through date ranges until clips are found or no clips at all available
   */
  @Trace()
  private async cycleDateRangesAndFetchTwitchClipsAndQueryParams(
    userId: ObjectId,
    twitchUserId: string,
    clippyCommand: DocumentType<ClippyCommand>,
    twitchClips: ITwitchClipPaginationResponse
  ): Promise<[ITwitchClipPaginationResponse, ITwitchClipBroadcasterQueryParams]> {
    try {
      let adjustedQueryParams: ITwitchClipBroadcasterQueryParams = <ITwitchClipBroadcasterQueryParams>{};
      let fallbackDateRange = clippyCommand.dateRange!;
      let fallbackTwitchClips: ITwitchClipPaginationResponse = JSON.parse(JSON.stringify(twitchClips));

      while (fallbackTwitchClips.data.length === 0 && fallbackDateRange !== ClippyDateRangeEnum.ALL) {
        fallbackDateRange = this.fallbackToNextDateRange(fallbackDateRange);
        this.logger?.info(`Clips not found | Falling back to "${fallbackDateRange}" date range to find a clip`);

        adjustedQueryParams = this.createQueryToGetTwitchClips(
          twitchUserId,
          clippyCommand.sortOrder,
          fallbackDateRange
        );

        // Get fallback clips
        fallbackTwitchClips = await this.getClips(userId, adjustedQueryParams);
      }

      return [fallbackTwitchClips, adjustedQueryParams];
    } catch (error) {
      throw this.handleError(error);
    }
  }

  /**
   * @summary Build date range
   *
   * @param {ClippyDateRangeEnum} range
   *
   * @returns {IDateRange}
   */
  private generateDateRange(range: ClippyDateRangeEnum | undefined): IDateRange | null {
    if (!range) {
      return null;
    }

    const endDate = moment.utc().format();

    switch (range) {
      case ClippyDateRangeEnum.DAY:
        // 24 Hours
        return {
          startDate: moment.utc().subtract(24, "h").format(),
          endDate
        };
      case ClippyDateRangeEnum.WEEK:
        // One Week
        return {
          startDate: moment.utc().subtract(1, "w").format(),
          endDate
        };
      case ClippyDateRangeEnum.MONTH:
        // One Month
        return {
          startDate: moment.utc().subtract(1, "M").format(),
          endDate
        };
      case ClippyDateRangeEnum.YEAR:
        // One Year
        return {
          startDate: moment.utc().subtract(1, "y").format(),
          endDate
        };
      case ClippyDateRangeEnum.ALL:
        // All time
        return null;
      default:
        throw Failure.ExpectationFailed("Date range not implemented");
    }
  }

  /**
   * This is meant to help when a user has no clips in a certain range
   * It will expand the date range and try again.
   */
  private fallbackToNextDateRange(currentRange: ClippyDateRangeEnum): ClippyDateRangeEnum {
    switch (currentRange) {
      case ClippyDateRangeEnum.DAY:
        return ClippyDateRangeEnum.WEEK;
      case ClippyDateRangeEnum.WEEK:
        return ClippyDateRangeEnum.MONTH;
      case ClippyDateRangeEnum.MONTH:
        return ClippyDateRangeEnum.YEAR;
      case ClippyDateRangeEnum.YEAR:
        return ClippyDateRangeEnum.ALL;
      default:
        throw Failure.ExpectationFailed("Date range not implemented");
    }
  }

  /**
   * This returns a tuple that contains any found twitch clips along with the query params that were used.
   */
  @Trace()
  private async fetchTwitchClipsAndQueryParamsThenCycleDateRangesIfNecessary(
    userId: ObjectId,
    twitchUserId: string,
    clippyCommand: DocumentType<ClippyCommand>
  ): Promise<[ITwitchClipPaginationResponse, ITwitchClipBroadcasterQueryParams]> {
    try {
      const queryParams: ITwitchClipBroadcasterQueryParams = this.createQueryToGetTwitchClips(
        twitchUserId,
        clippyCommand.sortOrder,
        clippyCommand.dateRange
      );

      const twitchClips = await this.getClips(userId, queryParams);

      if (twitchClips.data.length > 0) {
        return [twitchClips, queryParams];
      }

      // Get all time clips if no date range
      if (clippyCommand.dateRange == null) {
        const noDateRangeForQueryParams = this.createQueryToGetTwitchClips(
          twitchUserId,
          clippyCommand.sortOrder,
          ClippyDateRangeEnum.ALL
        );
        this.logger?.info("Clips not found | Removing date filters to find a clip");
        const foundAllTimeTwitchClips = await this.getClips(userId, noDateRangeForQueryParams);

        return [foundAllTimeTwitchClips, noDateRangeForQueryParams];
      }

      return this.cycleDateRangesAndFetchTwitchClipsAndQueryParams(userId, twitchUserId, clippyCommand, twitchClips);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async getTwitchClipsWithPagination(
    userId: ObjectId,
    twitchClips: ITwitchClipPaginationResponse,
    queryParams: ITwitchClipBroadcasterQueryParams
  ): Promise<ITwitchClipResponse[]> {
    try {
      const twitchClipsFromAllPages = twitchClips.data;
      let currentPage = 1;
      let paginateTwitchClips: ITwitchClipPaginationResponse = JSON.parse(JSON.stringify(twitchClips));
      const adjustedQueryParams: ITwitchClipBroadcasterQueryParams = JSON.parse(JSON.stringify(queryParams));

      // Loop through pages one by one & store result in array
      // Limit it to 5 pages else it could get thousands of clips
      // Unfortunately there isn't much we can do to improve the performance here because
      // we're limited to max 100 clips per page and we can't get the next page until we have the previous page.
      while (twitchClips.pagination.cursor && currentPage < 5) {
        this.logger?.info("Fetching clips from next page");

        // Set param to get next page
        adjustedQueryParams.after = twitchClips.pagination.cursor;

        // Get clips from next page
        paginateTwitchClips = await this.getClips(userId, adjustedQueryParams);

        // Update array
        twitchClipsFromAllPages.push(...paginateTwitchClips.data);

        // Update page
        currentPage += 1;
      }

      return twitchClipsFromAllPages;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async returnRandomTwitchClipOrReturnFirstClip(
    clippyCommand: DocumentType<ClippyCommand>,
    twitchClips: ITwitchClipPaginationResponse,
    userId: ObjectId,
    queryParams: ITwitchClipBroadcasterQueryParams
  ): Promise<ClippyTwitchClip> {
    try {
      if (clippyCommand.sortOrder === ClippySortOrderEnum.RANDOM) {
        return this.returnRandomTwitchClip(clippyCommand, userId, twitchClips, queryParams);
      }

      this.logger?.info("Returning the first clip we get back from twitch");

      // If it reaches here then return the very first clip we get back from twitch
      return <ClippyTwitchClip>twitchClips.data[0];
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async returnRandomTwitchClip(
    clippyCommand: DocumentType<ClippyCommand>,
    userId: ObjectId,
    twitchClips: ITwitchClipPaginationResponse,
    queryParams: ITwitchClipBroadcasterQueryParams
  ): Promise<ClippyTwitchClip> {
    try {
      const twitchClipsFromPagination = await this.getTwitchClipsWithPagination(userId, twitchClips, queryParams);

      // Tracking the total number of clips received from twitch
      EFuseSegmentAnalyticsUtil.track({ userId: String(userId) }, ClippySegmentEventEnum.CLIP_COUNT_RETRIEVED, {
        count: twitchClipsFromPagination.length
      });

      // Handle minimumViews filter
      if (clippyCommand.minimumViews && clippyCommand.minimumViews > 0) {
        this.logger?.info("Filter clips on the basis of minimumViews");

        // Filter clips to get clips where view_count > minimumViews
        const filteredClips = twitchClipsFromPagination.filter(
          (clip) => clip.view_count >= (clippyCommand.minimumViews as number)
        );

        // If we get clips where view_count > minimumViews then return the max
        if (filteredClips.length > 0) {
          this.logger?.info("Got clips on the basis of minimumViews | Returning a random clip");

          return <ClippyTwitchClip>filteredClips[random(filteredClips.length - 1)];
        }
      }

      this.logger?.info("Returning a random clip from a paginated collection of clips");

      // If it reaches here then it means no clips falls under threshold return some random clip
      return <ClippyTwitchClip>twitchClipsFromPagination[random(twitchClipsFromPagination.length - 1)];
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
