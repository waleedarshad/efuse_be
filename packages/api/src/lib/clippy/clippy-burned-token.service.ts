import { DocumentType } from "@typegoose/typegoose";
import { randomBytes } from "crypto";

import { Service, Trace } from "../decorators";
import { BaseModelService } from "../base";
import { ClippyBurnedToken, ClippyBurnedTokenModel } from "../../backend/models/clippy/clippy-burned-token.model";

@Service("clippy-burned-token.service")
export class ClippyBurnedTokenService extends BaseModelService<DocumentType<ClippyBurnedToken>> {
  constructor() {
    super(ClippyBurnedTokenModel);
  }

  /**
   * @summary Generate a unique 32 Characters long token for the user
   *
   * @returns {Promise<string | void>}
   */
  @Trace()
  public async generateToken(): Promise<string> {
    // Generating a 32 characters long string
    let token = randomBytes(16).toString("hex");

    // Make sure token is unique
    const existingToken = await this.findOne({ token }, {}, { lean: true });

    // If we don't find any existing object then it's a unique token
    if (!existingToken) {
      // Store it in DB, so that it can't be reused
      await this.create(<DocumentType<ClippyBurnedToken>>{ token });

      // Return the token
      return token;
    }

    // Re-generateToken: If it reaches here then token is already taken.
    token = await this.generateToken();

    return token;
  }
}
