import {
  ClippyCommandKindEnum,
  ClippySortOrderEnum,
  ClippyDateRangeEnum,
  ClippyAllowedRolesEnum,
  ClippyDefaultCommandEnum
} from "@efuse/entities";

export class ShoutoutCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.SHOUTOUT,
      command: ClippyDefaultCommandEnum.SHOUTOUT,
      sortOrder: ClippySortOrderEnum.TOP,
      dateRange: ClippyDateRangeEnum.DAY,
      allowedRoles: [ClippyAllowedRolesEnum.BROADCASTER, ClippyAllowedRolesEnum.MODERATOR],
      minimumViews: 0,
      queueClips: false
    };
  }
}
