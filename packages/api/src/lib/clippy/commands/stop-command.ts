import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class StopCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.STOP,
      command: ClippyDefaultCommandEnum.STOP
    };
  }
}
