import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class StopAllCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.STOP_ALL,
      command: ClippyDefaultCommandEnum.STOP_ALL
    };
  }
}
