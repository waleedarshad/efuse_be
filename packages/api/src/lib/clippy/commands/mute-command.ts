import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class MuteCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.MUTE,
      command: ClippyDefaultCommandEnum.MUTE
    };
  }
}
