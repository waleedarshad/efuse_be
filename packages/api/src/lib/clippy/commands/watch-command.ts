import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class WatchCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.WATCH,
      command: ClippyDefaultCommandEnum.WATCH
    };
  }
}
