import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class ReplayCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.REPLAY,
      command: ClippyDefaultCommandEnum.REPLAY
    };
  }
}
