import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class ResetCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.RESET,
      command: ClippyDefaultCommandEnum.RESET
    };
  }
}
