import { ClippyCommandKindEnum, ClippyDefaultCommandEnum } from "@efuse/entities";

export class UnmuteCommand {
  static get defaults() {
    return {
      kind: ClippyCommandKindEnum.UNMUTE,
      command: ClippyDefaultCommandEnum.UNMUTE
    };
  }
}
