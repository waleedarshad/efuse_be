import { Failure } from "@efuse/contracts";
import { Trace } from "@efuse/decorators";
import { InviteCodeType, InviteResourceType } from "@efuse/entities";
import { PaginateResult, QueryOptions, Types } from "mongoose";
import { IInviteCode } from "../../backend/interfaces";
import { ITeam } from "../../backend/interfaces/team/team";
import { Team, TeamModel } from "../../backend/models/team/team";
import { BaseModelService } from "../base";
import { Service } from "../decorators";
import { InviteCodeService } from "../invite-code/invite-code.service";
import { TeamMemberService } from "./team-member.service";

type ObjectId = string | Types.ObjectId;

@Service("team.service")
export class TeamService extends BaseModelService<ITeam> {
  private team: TeamModel<ITeam>;
  private inviteCodeService: InviteCodeService;

  constructor() {
    super(Team);

    this.team = Team;
    this.inviteCodeService = new InviteCodeService();
  }

  @Trace()
  public async create(item: ITeam): Promise<ITeam> {
    const team = await super.create(item);

    const inviteCode: Partial<IInviteCode> = {
      type: InviteCodeType.TEAM,
      resource: team._id,
      resourceType: InviteResourceType.TEAM,
      active: true
    };

    await this.inviteCodeService.create(inviteCode);

    return team;
  }

  @Trace()
  public async getPaginatedTeams(page?: number, limit?: number, owner?: ObjectId): Promise<PaginateResult<ITeam>> {
    // Make sure page param is positive
    let pageParam = page;
    if (!pageParam || pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (!limitParam || limitParam < 1) {
      limitParam = 10;
    }

    const leaderboard = await this.team.paginate(owner ? { owner } : {}, {
      page: pageParam,
      limit: limitParam,
      sort: { name: 1 },
      lean: true
    });

    return leaderboard;
  }

  @Trace()
  public async getTeamByTeamMember(
    teamMemberId: ObjectId,
    projection?: any,
    options?: QueryOptions
  ): Promise<ITeam | null> {
    const teamMemberService: TeamMemberService = new TeamMemberService();
    const leanOption: QueryOptions = { lean: true };
    const adjustedOutput = { team: 1 };

    try {
      const foundTeamMember = await teamMemberService.findOne({ _id: teamMemberId }, adjustedOutput, leanOption);

      if (!foundTeamMember) {
        throw Failure.NotFound("Team member not found", { teamMemberId });
      }

      const foundTeam = await this.findOne({ _id: foundTeamMember.team }, projection, options);

      return foundTeam;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  public;
}
