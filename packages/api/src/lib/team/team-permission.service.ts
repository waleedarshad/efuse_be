// TODO: Once teams for users are fully supported, convert this to using the generic OwnerPermissionService.
import { Failure } from "@efuse/contracts";
import { OwnerType } from "@efuse/entities";
import { Types } from "mongoose";
import { BaseService } from "../base";
import { Service } from "../decorators";
import { OrganizationAccessTypes } from "../organizations/organization-access-types";
import { OrganizationACLService } from "../organizations/organization-acl.service";

type ObjectId = string | Types.ObjectId;

@Service("team-permission.service")
export class TeamPermissionService extends BaseService {
  private organizationAclService: OrganizationACLService;

  constructor() {
    super();

    this.organizationAclService = new OrganizationACLService();
  }

  /**
   * Checks if user is allowed to perform actions for team. Otherwise throws error
   */
  public async userAllowedToPerformAction(
    currentUserId: ObjectId,
    owner: ObjectId,
    ownerType: OwnerType
  ): Promise<boolean> {
    try {
      const ownerIsOrganization: boolean = ownerType === OwnerType.ORGANIZATIONS;

      if (ownerIsOrganization) {
        // will throw error if current user does not have permission for action in organization
        await this.organizationAclService.authorizedToAccess(currentUserId, owner, OrganizationAccessTypes.ANY);

        return ownerIsOrganization;
      }

      // TEMPORARY until teams for users are fully supported
      // const ownerIsCurrentUser = owner === currentUserId && ownerType === OwnerType.USERS;

      // if (ownerIsCurrentUser) {
      //   return ownerIsCurrentUser;
      // }

      throw Failure.Forbidden("User does not have permission to perform this team action");
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
