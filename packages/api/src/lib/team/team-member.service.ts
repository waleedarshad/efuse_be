import { Failure } from "@efuse/contracts";
import { Trace } from "@efuse/decorators";
import { TeamMemberStatus, OwnerType } from "@efuse/entities";
import { PaginateResult, Types } from "mongoose";
import { IMember } from "../../backend/interfaces/member";
import { ITeamMember } from "../../backend/interfaces/team/team-member";
import { TeamMember, TeamMemberModel } from "../../backend/models/team/team-member";
import { BaseModelService } from "../base";
import { Service } from "../decorators";
import { MemberService } from "../member/member.service";
import { TeamService } from "./team.service";

type ObjectId = string | Types.ObjectId;

@Service("team-member.service")
export class TeamMemberService extends BaseModelService<ITeamMember> {
  private organizationMemberService: MemberService;
  private teamMember: TeamMemberModel<ITeamMember>;

  constructor() {
    super(TeamMember);

    this.organizationMemberService = new MemberService();
    this.teamMember = TeamMember;
  }

  @Trace()
  public async getPaginatedTeamMembers(
    teamId: ObjectId,
    status?: TeamMemberStatus,
    page?: number,
    limit?: number
  ): Promise<PaginateResult<ITeamMember>> {
    // Make sure page param is positive
    let pageParam = page;
    if (!pageParam || pageParam < 1) {
      pageParam = 1;
    }

    // Make sure limit is positive
    let limitParam = limit;
    if (!limitParam || limitParam < 1) {
      limitParam = 10;
    }

    const members = await this.teamMember.paginate(status ? { team: teamId, status } : { team: teamId }, {
      page: pageParam,
      limit: limitParam,
      sort: { user: 1 },
      lean: true
    });

    return members;
  }

  @Trace()
  public async deleteMembersFromTeam(teamId: ObjectId): Promise<boolean> {
    const teamMembers: ITeamMember[] = await this.find({ team: teamId }, { _id: 1 }, { lean: true });

    const teamMembersToDelete = teamMembers && teamMembers.length > 0;

    if (teamMembersToDelete) {
      await Promise.all(teamMembers.map((teamMember) => this.delete(teamMember._id)));
    }

    return teamMembersToDelete;
  }

  /**
   * This is meant strictly for organization teams.
   *
   * The user being added will need to be part of the Org if they are not already a part of it, unless
   * the "mustBeCurrentOrgMember" flag is used. If this is set to false then the user will be added as an Org member
   * if they are not already.
   */
  @Trace()
  public async addUserToOrganizationTeam(
    teamId: ObjectId,
    userIdToAddToTeam: ObjectId,
    role: string,
    mustBeCurrentOrgMember = true
  ): Promise<ITeamMember> {
    try {
      const foundTeamMember = await this.findOne(
        {
          team: teamId,
          user: userIdToAddToTeam
        },
        {},
        { lean: true }
      );

      if (foundTeamMember) {
        return foundTeamMember;
      }

      const teamService: TeamService = new TeamService();
      const foundAdjustedTeam = await teamService.findOne(
        { _id: teamId },
        {
          _id: 0,
          ownerType: 1,
          owner: 1
        },
        { lean: true }
      );

      if (!foundAdjustedTeam) {
        throw Failure.NotFound("Team does not exist", { teamId });
      }

      const { owner, ownerType } = foundAdjustedTeam;

      const isOrganizationOwnerType = ownerType === OwnerType.ORGANIZATIONS;

      const status = await this.determineIfUserIsEligibleForOrganizationTeam(
        isOrganizationOwnerType,
        owner,
        userIdToAddToTeam,
        mustBeCurrentOrgMember
      );

      return this.addUserToTeam(teamId, userIdToAddToTeam, role, status);
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async addUserToTeam(
    teamId: ObjectId,
    userId: ObjectId,
    role: string,
    status: TeamMemberStatus
  ): Promise<ITeamMember> {
    try {
      const teamMember = <ITeamMember>(<unknown>{
        team: teamId,
        user: userId,
        role,
        status
      });

      const savedTeamMember = await this.create(teamMember);

      return savedTeamMember;
    } catch (error) {
      throw this.handleError(error);
    }
  }

  @Trace()
  private async determineIfUserIsEligibleForOrganizationTeam(
    isOrganizationOwnerType: boolean,
    owner: ObjectId,
    userId: ObjectId,
    mustBeCurrentOrgMember = true
  ): Promise<TeamMemberStatus> {
    if (!isOrganizationOwnerType) {
      throw Failure.NotImplemented("User Teams not currently implemented");
    }

    try {
      const foundOrganizationMember = await this.organizationMemberService.findOne(
        { user: userId, organization: owner },
        { _id: 1, isBanned: 1 },
        { lean: true }
      );

      if (!foundOrganizationMember && mustBeCurrentOrgMember) {
        throw Failure.Forbidden("User must be Organization member to be added to team.");
      }

      if (foundOrganizationMember?.isBanned) {
        throw Failure.Forbidden("User is banned in organization", { userId, organization: owner });
      }

      // add user to org as member if not already member
      if (!foundOrganizationMember) {
        const member = <IMember>{
          organization: owner,
          user: userId
        };

        await this.organizationMemberService.create(member);
      }

      return TeamMemberStatus.ACTIVE;
    } catch (error) {
      throw this.handleError(error);
    }
  }
}
