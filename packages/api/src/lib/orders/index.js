const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Order } = require("../../backend/models/Order");
const { PromoCode } = require("../../backend/models/PromoCode");
const { createStripePayment } = require("../stripe");

const getOrders = async (userId) => {
  try {
    // query for all orders with user Id
    const orders = await Order.find({ user: userId }).lean();
    return orders;
  } catch (error) {
    logger.error("Orders Library Error: ", error);
    throw error;
  }
};

const createOrder = async (amount, description, userId, type, source, promocodeId) => {
  try {
    let discountAmount = 0.0;

    if (promocodeId) {
      // Validate promocode
      const promo = await PromoCode.findById(promocodeId).lean();

      // Throw error if promocode does not exist
      if (!promo) throw "Promocode does not exist.";

      if (promo.discountType === "VALUE") {
        // value discounts take a fixed amount off of total
        discountAmount = Math.min(promo.amount, amount);
      } else {
        // percentage discounts take a percent amount off of total
        discountAmount = Math.max(promo.amount * amount, 0);
      }

      //Round down to nearest cent
      discountAmount = Math.floor(discountAmount * 100) / 100;
    }

    // create new stripe payment for this order
    const stripePayment = await createStripePayment(userId, source, amount - discountAmount, description);

    // create new Order document with details of transaction
    const order = await new Order({
      type,
      paymentStatus: "PAID",
      user: userId,
      subtotalAmount: amount,
      discountAmount: discountAmount,
      taxAmount: 0,
      paymentId: stripePayment.id,
      refundAmount: 0,
      promocode: promocodeId
    }).save();

    return order;
  } catch (error) {
    logger.error("Orders Library Error - createOrder ", error);
    throw error;
  }
};

module.exports.createOrder = createOrder;
module.exports.getOrders = getOrders;
