import { Failure } from "@efuse/contracts";
import { FilterQuery, QueryOptions } from "mongoose";

import { IUserStats } from "../../backend/interfaces/user-stats";
import { UserStats, UserStatsModel } from "../../backend/models/user-stats";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("user-stats.service")
export class UserStatsService extends BaseService {
  private $userStats: UserStatsModel<IUserStats> = UserStats;

  public async findOne(filter: FilterQuery<IUserStats>): Promise<IUserStats | null>;
  public async findOne(filter: FilterQuery<IUserStats>, projection: any): Promise<IUserStats | null>;
  public async findOne(
    filter: FilterQuery<IUserStats>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<IUserStats | null>;
  @Trace()
  public async findOne(
    filter: FilterQuery<IUserStats>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<IUserStats | null> {
    const userStats = await this.$userStats.findOne(filter, projection, options);
    return userStats;
  }

  /**
   * @summary Create user stats object
   *
   * @param {IUserStats} userStatsBody
   *
   * @returns {Promise<IUserStats>}
   */
  @Trace()
  public async create(userStatsBody: IUserStats): Promise<IUserStats> {
    if (!userStatsBody) {
      throw Failure.BadRequest("body is required");
    }

    const doc = <IUserStats>userStatsBody;

    const userStats = await this.$userStats.create(doc);
    return userStats;
  }

  /**
   * @summary Update user stats object
   *
   * @param {Partial<IUserStats>} userStatsBody
   *
   * @returns {Promise<IUserStats | null>}
   */
  @Trace()
  public async update(filter: FilterQuery<IUserStats>, userStatsBody: Partial<IUserStats>): Promise<IUserStats | null> {
    if (!userStatsBody) {
      throw Failure.BadRequest("body is required");
    }

    const userStats: IUserStats | null = await this.$userStats
      .findOneAndUpdate(filter, { ...userStatsBody }, { new: true })
      .lean();

    return userStats;
  }

  /**
   * @summary Update or create user stats object
   *
   * @param {Partial<IUserStats>} userStatsBody
   * @param {IUserStats} newUserStatsBody
   *
   * @returns {Promise<IUserStats>}
   */
  @Trace()
  public async updateOrCreate(
    filter: FilterQuery<IUserStats>,
    userStatsBody: Partial<IUserStats>,
    newUserStatsBody: IUserStats
  ): Promise<IUserStats> {
    const stats: IUserStats | null = await this.update(filter, userStatsBody);

    if (stats) {
      return stats;
    }

    const userStats = await this.create(newUserStatsBody);
    return userStats;
  }
}
