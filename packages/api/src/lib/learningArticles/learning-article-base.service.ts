import { FilterQuery, QueryOptions } from "mongoose";

import { ILearningArticle } from "../../backend/interfaces";
import { LearningArticle, LearningArticleModel } from "../../backend/models/learning-article";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("learning-article-base.service")
export class LearningArticleBaseService extends BaseService {
  protected readonly $learningArticles: LearningArticleModel<ILearningArticle> = LearningArticle;

  public async findOne(condition: FilterQuery<ILearningArticle>): Promise<ILearningArticle | null>;
  public async findOne(
    condition: FilterQuery<ILearningArticle>,
    projection: QueryOptions | null | undefined
  ): Promise<ILearningArticle | null>;
  public async findOne(
    condition: FilterQuery<ILearningArticle>,
    projection: unknown,
    options: QueryOptions | null | undefined
  ): Promise<ILearningArticle | null>;
  @Trace()
  public async findOne(
    condition: FilterQuery<ILearningArticle>,
    projection?: unknown,
    options?: QueryOptions | null | undefined
  ): Promise<ILearningArticle | null> {
    const learningArticle = await this.$learningArticles.findOne(condition, projection, options);
    return learningArticle;
  }

  public async find(filter: FilterQuery<ILearningArticle>): Promise<ILearningArticle[]>;
  public async find(filter: FilterQuery<ILearningArticle>, projection: any): Promise<ILearningArticle[]>;
  public async find(
    filter: FilterQuery<ILearningArticle>,
    projection: any,
    options: QueryOptions | null | undefined
  ): Promise<ILearningArticle[]>;
  @Trace()
  public async find(
    filter: FilterQuery<ILearningArticle>,
    projection?: any,
    options?: QueryOptions | null | undefined
  ): Promise<ILearningArticle[]> {
    const learningArticles = await this.$learningArticles.find(filter, projection, options);
    return learningArticles;
  }
}
