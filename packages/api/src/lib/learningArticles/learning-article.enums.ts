export enum LearningArticleStatusEnum {
  PUBLISHED = "Published",
  UNLISTED = "Unlisted",
  DRAFT = "Draft",
  REMOVED = "Removed",
  IN_REVIEW = "In Review"
}

export enum LearningArticleAuthorTypeEnum {
  USER = "users",
  ORGANIZATION = "organizations"
}
