import { Failure } from "@efuse/contracts";
import { LeanDocument, Model } from "mongoose";
import { Queue } from "bull";
import moment from "moment";
import { OwnerKind, ChangeStreamOperationTypeEnum } from "@efuse/entities";

import {
  IAlgoliaFormattedOwner,
  ILearningArticle,
  IPopulatedLearningArticle,
  IUser,
  IOrganization,
  IMongodbWatcherResponse,
  ILearningArticleCategory,
  ISitemapUrl
} from "../../backend/interfaces";
import { AlgoliaIndicesEnum } from "../algolia/algolia.enum";
import { AlgoliaService } from "../algolia/algolia.service";
import { Service, Trace } from "../decorators";
import { LearningArticleBaseService } from "./learning-article-base.service";
import Config from "../../async/config";
import { UserService } from "../users/user.service";
import { OrganizationBaseService } from "../organizations/organization-base.service";
import { LearningArticleStatusEnum } from "./learning-article.enums";
import { LearningArticleCategory } from "../../backend/models/LearningArticleCategory";
import { ObjectId } from "../../backend/types";
import { LearningArticleGameService } from "./learning-article-game.service";
import { GeneralHelper } from "../../backend/helpers/general.helper";

const SERVICE = "learning-article.service";

@Service(SERVICE)
export class LearningArticleService extends LearningArticleBaseService {
  public $index = `${process.env.NODE_ENV}_${AlgoliaIndicesEnum.LEARNING_ARTICLES}`;

  private $syncLearningArticlesWithAlgoliaQueue: Queue;

  private $learningArticleCategory: Model<ILearningArticleCategory> = LearningArticleCategory;
  private $learningArticleGameService: LearningArticleGameService;
  private $algoliaService: AlgoliaService;
  private $userService: UserService;
  private $organizationBaseService: OrganizationBaseService;

  constructor() {
    super();

    this.$syncLearningArticlesWithAlgoliaQueue = Config.initQueue("syncLearningArticlesWithAlgolia");

    this.$algoliaService = new AlgoliaService();
    this.$userService = new UserService();
    this.$organizationBaseService = new OrganizationBaseService();
    this.$learningArticleGameService = new LearningArticleGameService();
  }

  /**
   * @summary Search Learning articles using AlgoliaSearch
   *
   * @param {string} query
   * @param {number} limit
   * @param {string} userId
   *
   * @returns {Promise<ILearningArticle[]>}
   */
  @Trace()
  public async search(query: string, limit: number, userId?: string): Promise<ILearningArticle[]> {
    const results = await this.$algoliaService.search<IPopulatedLearningArticle>(this.$index, query, limit, userId);

    // Make sure to depopulate author, category, and associated so that it can be resolved by graphql with the newest data
    const learningArticles: ILearningArticle[] = results.map(
      (result: IPopulatedLearningArticle) =>
        <ILearningArticle>{
          ...result,
          author: (result.author?._id || result.author) as string,
          category: String(result.category?._id) || String(result.category),
          associatedGames: result.associatedGames?.map((game) => game._id)
        }
    );

    return learningArticles;
  }

  /**
   * @summary Populate author & sync with algolia
   *
   * @param {LeanDocument<ILearningArticle>} learningArticle
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async syncWithAlgolia(learningArticle: LeanDocument<ILearningArticle>): Promise<void> {
    try {
      // Remove learning article from algolia if it is not published
      if (learningArticle.status !== LearningArticleStatusEnum.PUBLISHED) {
        this.logger?.info({ _id: learningArticle._id }, "Removing learning article from algolia");

        await this.deleteFromAlgolia(learningArticle._id);

        // Short circuit
        return;
      }

      let author: IUser | IOrganization | null | undefined;

      // Fetch the organization
      if (learningArticle.authorType === OwnerKind.ORGANIZATION) {
        author = await this.$organizationBaseService.findOne({ _id: learningArticle.author }, {}, { lean: true });
      }

      // Fetch the User
      if (!author && learningArticle.authorType === OwnerKind.USER) {
        author = await this.$userService.findOne({ _id: learningArticle.author }, {}, { lean: true });
      }

      // Make sure author is available
      if (!author) {
        throw Failure.UnprocessableEntity("Author not found");
      }

      // Fetch the category
      const category = await this.$learningArticleCategory.findOne(
        { _id: learningArticle.category },
        {},
        { lean: true }
      );

      // Make sure category exists
      if (!category) {
        throw Failure.UnprocessableEntity("Category not found");
      }

      // Format author object
      const formattedAuthor: IAlgoliaFormattedOwner | null = this.$algoliaService.formatOwner(
        author,
        learningArticle.authorType
      );

      // populate games
      const associatedGames = await this.$learningArticleGameService.getAssociatedGames(learningArticle._id);

      // Build object with formatted author
      const articleToBeSynced: Record<string, unknown> = {
        ...learningArticle,
        author: formattedAuthor || {},
        category,
        associatedGames
      };

      // Adding unix timestamp
      if (learningArticle.publishDate) {
        articleToBeSynced.publishDateUnix = moment(learningArticle.publishDate).unix();
      }

      // Sync with algolia
      await this.$algoliaService.syncObject(this.$index, articleToBeSynced);

      this.logger?.info({ _id: articleToBeSynced._id }, "Synced learning article with algolia");
    } catch (error) {
      this.logger?.error(error, "Error while syncing learning articles with algolia");
    }
  }

  /**
   * @summary Populate author & sync with algolia using bull queue
   *
   * @param {LeanDocument<ILearningArticle>} learningArticle
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public syncWithAlgoliaAsync(learningArticle: LeanDocument<ILearningArticle>): void {
    this.$syncLearningArticlesWithAlgoliaQueue
      .add({ learningArticle })
      .catch((error) => this.logger?.error(error, "Error while adding to queue syncLearningArticlesWithAlgoliaQueue"));
  }

  /**
   * @summary Delete learning article from algolia
   *
   * @param {ObjectId} learningArticleId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public async deleteFromAlgolia(learningArticleId: ObjectId): Promise<void> {
    await this.$algoliaService.deleteObject(this.$index, String(learningArticleId));
  }

  /**
   * @summary Delete learning article from algolia using async queue
   *
   * @param {ObjectId} learningArticleId
   *
   * @returns {Promise<void>}
   */
  @Trace()
  public deleteFromAlgoliaAsync(learningArticleId: ObjectId): void {
    this.$algoliaService.deleteObjectAsync(this.$index, String(learningArticleId));
  }

  /**
   * Async Processors
   */
  @Trace({ service: SERVICE, name: "InitAsyncProcessors" })
  public static InitAsyncProcessors(): void {
    const learningArticleService = new LearningArticleService();

    // syncLearningArticlesWithAlgoliaQueue
    learningArticleService.$syncLearningArticlesWithAlgoliaQueue
      .process((job: { data: { learningArticle: LeanDocument<ILearningArticle> } }) => {
        return learningArticleService.syncWithAlgolia(job.data.learningArticle);
      })
      .catch((error) =>
        learningArticleService.logger?.error(error, "Error while processing syncLearningArticlesWithAlgoliaQueue")
      );
  }

  /**
   * @summary Learning Article Watcher to read change streams
   *
   * @param {IMongodbWatcherResponse<LeanDocument<ILearningArticle>>} data
   *
   * @returns {void}
   */
  @Trace({ service: SERVICE, name: "Watcher" })
  public static Watcher(data: IMongodbWatcherResponse<LeanDocument<ILearningArticle>>): void {
    const learningArticleService = new LearningArticleService();

    try {
      if (data.operationType === ChangeStreamOperationTypeEnum.DELETE) {
        learningArticleService.deleteFromAlgoliaAsync(data?.documentKey?._id);
      } else {
        // Sync Data with algolia
        learningArticleService.syncWithAlgoliaAsync(data.fullDocument);
      }
    } catch (error) {
      learningArticleService.logger?.error(error, "Error reading data in learning articles watcher");
    }
  }

  /**
   * @summary Populate article with author, category & url and return full article
   *
   * @param {LeanDocument<ILearningArticle>} article
   *
   * @returns {Promise<ILearningArticle| undefined>}
   */
  @Trace()
  public async generateFullArticle(article: ILearningArticle): Promise<any> {
    try {
      let author: IUser | IOrganization | null | undefined;
      // Associate Author Object
      if (article.authorType === OwnerKind.ORGANIZATION) {
        author = await this.$organizationBaseService.findOne({ _id: article.author }, {}, { lean: true });
      } else {
        author = await this.$userService.findOne({ _id: article.author }, {}, { lean: true });
      }

      // Associate Category
      const category = await this.$learningArticleCategory.findOne({ _id: article.category }, {}, { lean: true });
      if (category) article.category = category;

      // Bind Url
      article.url = GeneralHelper.constructLearningArticleUrl(article.category, article);

      return { ...article, author };
    } catch (error) {
      this.logger?.error(error, "Error generating full learning article");
      return;
    }
  }

  /**
   * @summary Get Article URLs
   *
   * @returns {Promise<ISitemapUrl[]>}
   */
  @Trace()
  public async getArticleUrls(): Promise<ISitemapUrl[] | undefined> {
    try {
      const articles = await this.find({ status: "published", isActive: true }, {}, { lean: true });

      const getUrls: Promise<ISitemapUrl>[] = articles?.map(async (article: ILearningArticle) => {
        const category = await this.$learningArticleCategory.findOne({ _id: article.category }, {}, { lean: true });

        const article_url = GeneralHelper.constructLearningArticleUrl(category, article);

        return { url: article_url, changefreq: "daily", priority: 0.1 };
      });

      const result = await Promise.all(getUrls);

      return result;
    } catch (error) {
      this.logger?.error(error, "Error: generating Learning urls");
      return;
    }
  }
}
