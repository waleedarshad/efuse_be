import { FilterQuery, QueryOptions, Types } from "mongoose";

import { IBulkWriteResponse, ILearningArticleGame } from "../../backend/interfaces";
import { LearningArticleGame, LearningArticleGameModel } from "../../backend/models/learning-article-game";
import { BaseService } from "../base/base.service";
import { Service, Trace } from "../decorators";

@Service("learning-article-game-base.service")
export class LearningArticleGameBaseService extends BaseService {
  protected readonly $learningArticleGames: LearningArticleGameModel<ILearningArticleGame> = LearningArticleGame;

  public async find(condition: FilterQuery<ILearningArticleGame>): Promise<ILearningArticleGame[]>;
  public async find(condition: FilterQuery<ILearningArticleGame>, projection: unknown): Promise<ILearningArticleGame[]>;
  public async find(
    condition: FilterQuery<ILearningArticleGame>,
    projection: unknown,
    options: QueryOptions
  ): Promise<ILearningArticleGame[]>;
  @Trace()
  public async find(
    condition: FilterQuery<ILearningArticleGame>,
    projection?: unknown,
    options?: QueryOptions
  ): Promise<ILearningArticleGame[]> {
    const learningArticleGames = await this.$learningArticleGames.find(condition, projection, options);
    return learningArticleGames;
  }

  /**
   * @summary Bulk update learning article games
   *
   * @param {Types.ObjectId | string} learningArticleId
   * @param {Array<Types.ObjectId | string>} gameIds
   *
   * @returns {Promise<IBulkWriteResponse>}
   */
  @Trace()
  public async bulkUpdate(
    learningArticleId: Types.ObjectId | string,
    gameIds: Array<Types.ObjectId | string>
  ): Promise<IBulkWriteResponse> {
    const learningArticleQuery: Record<string, unknown> = { learningArticle: learningArticleId };

    // Create or update learning article games
    const bulkOperations: any[] = gameIds.map((gameId) => {
      const query: Record<string, unknown> = { ...learningArticleQuery, game: gameId };
      return {
        updateOne: {
          filter: query,
          update: { ...query, game: gameId, updatedAt: Date.now() },
          upsert: true, // create new object if it is not already there
          setDefaultsOnInsert: true, // this would set the schema defaults
          ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
        }
      };
    });

    // Delete games which are not passed
    bulkOperations.push({ deleteMany: { filter: { ...learningArticleQuery, game: { $nin: gameIds } } } });

    // Execute bulkWrite
    const response = await this.$learningArticleGames.bulkWrite(bulkOperations);

    return {
      upserted: response.upsertedCount || 0,
      updated: response.upsertedCount || 0,
      modified: response.modifiedCount || 0,
      deleted: response.deletedCount || 0,
      matched: response.matchedCount || 0
    };
  }

  /**
   * @summary Get a Count of number of documents
   *
   * @param {FilterQuery<ILearningArticleGame>} filter
   *
   * @returns {Promise<number>}
   */
  public async countDocuments(filter?: FilterQuery<ILearningArticleGame>): Promise<number> {
    let count = 0;

    count = await (filter
      ? this.$learningArticleGames.countDocuments(filter)
      : this.$learningArticleGames.countDocuments());

    return count;
  }
}
