import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { DocumentType } from "@typegoose/typegoose";

import { Service, Trace } from "../decorators";
import { LearningArticleGameBaseService } from "./learning-article-game-base.service";
import { LearningArticleBaseService } from "./learning-article-base.service";
import { ILearningArticle } from "../../backend/interfaces";
import { GameBaseService } from "../game/game-base.service";

import { Game } from "../../backend/models/game.model";

@Service("learning-article-game.service")
export class LearningArticleGameService extends LearningArticleGameBaseService {
  private $gameBaseService: GameBaseService;
  private $learningArticleBaseService: LearningArticleBaseService;
  private $learningArticleGameBaseService: LearningArticleGameBaseService;

  constructor() {
    super();

    this.$gameBaseService = new GameBaseService();
    this.$learningArticleBaseService = new LearningArticleBaseService();
    this.$learningArticleGameBaseService = new LearningArticleGameBaseService();
  }

  /**
   * @summary Associate games with the learning article
   *
   * @param {Types.ObjectId | string} learningArticleId
   * @param {Array<Types.ObjectId | string>} gameIds
   *
   * @returns {Promise<ILearningArticle>}
   */
  @Trace()
  public async associateGames(
    learningArticleId: Types.ObjectId | string,
    gameIds: Array<Types.ObjectId | string>
  ): Promise<ILearningArticle | null> {
    // Make sure gameIds are valid object ids
    gameIds.forEach((gameId) => {
      if (!this.isValidObjectId(gameId)) {
        throw Failure.BadRequest("Invalid game ID");
      }
    });

    // Make sure game exists in our DB
    const games = await this.$gameBaseService.find({ _id: { $in: gameIds } });

    // IDs that exist in our DB
    const idsToStore: string[] = games.map((game) => String(game._id));

    // Associate with learning article & Store in DB
    await this.bulkUpdate(learningArticleId, idsToStore);

    // fetch the learning article
    const learningArticle = await this.$learningArticleBaseService.findOne({ learningArticle: learningArticleId });

    return learningArticle;
  }

  /**
   * @summary Get games associated to a learning article
   *
   * @param {Types.ObjectId | string} learningArticleId
   *
   * @returns {Promise<Game[]>}
   */
  @Trace()
  public async getAssociatedGames(learningArticleId: Types.ObjectId | string): Promise<DocumentType<Game>[]> {
    const learningArticleGames = await this.find({ learningArticle: learningArticleId });

    const games = await this.$gameBaseService.find({
      _id: { $in: learningArticleGames.map((articleGame) => articleGame.game) }
    });

    return games;
  }
}
