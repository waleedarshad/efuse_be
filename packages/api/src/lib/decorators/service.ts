import pino from "pino";

import { EFuseAnalyticsUtil } from "../analytics.util";

export interface IServiceOptions {
  name: string;
}

export function Service(nameOrOptions: string | IServiceOptions) {
  return <T extends { new (...args: any[]): {} }>(constructor: T): T => {
    const name: string = typeof nameOrOptions === "object" ? nameOrOptions.name : nameOrOptions;

    return class extends constructor {
      name = `be-${name}`;
      logger = pino({ name });
      stats = EFuseAnalyticsUtil.Stats.childClient({
        globalTags: { service: name, kind: "service" }
      });
    };
  };
}
