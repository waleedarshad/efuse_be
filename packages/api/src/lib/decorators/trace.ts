import { SpanOptions, TraceOptions } from "dd-trace";

import { EFuseAnalyticsUtil } from "../analytics.util";

export interface ITraceOptions {
  name?: string;
  service?: string;
}

/**
 * Trace execution of function for datadog
 *
 * @param nameOrOptions Optional string to define method, or ITraceOptions
 *
 * Note: If applying to a static method, you must pass ITraceOptions with name and service defined.
 */
export const Trace = (nameOrOptions?: string | ITraceOptions) => {
  return (target: Object, propertyKey: string, descriptor: PropertyDescriptor) => {
    const name: string =
      nameOrOptions && typeof nameOrOptions === "object"
        ? nameOrOptions.name || propertyKey
        : nameOrOptions || propertyKey;

    const original = descriptor.value;

    if (typeof original === "function") {
      descriptor.value = function (...args: any[]) {
        let parent = "be-service";
        if (nameOrOptions && typeof nameOrOptions === "object" && nameOrOptions.service) {
          parent = nameOrOptions.service;
        } else {
          parent = (<any>this).name;
        }

        const options: TraceOptions & SpanOptions = {
          analytics: true,
          resource: name,
          service: parent,
          tags: {
            resource: name,
            service: parent
          }
        };

        return EFuseAnalyticsUtil.Tracer.trace(name, options, () => original.apply(this, args));
      };
    }

    return descriptor;
  };
};
