import yargs from "yargs";

import Config from "../../async/config";

// we should accept most bull queue options in this decorators
export const Async = () => {
  return (target: Object, propertyKey: string, descriptor: PropertyDescriptor) => {
    // initialize our async queue.  NOTE:  This has logic around it to handle making sure this
    // is a global singleton, wrapping with dd-trace, etc.
    const queue = Config.initQueue(propertyKey);
    const original = descriptor.value;

    // initialize the async processor if the flag has been passed properly.
    const { argv }: { argv: any } = yargs.option("async", { default: true });

    if (argv.async) {
      // if async processors are to start, register the processor here.
      // todo:  Need to make sure this is a singleton globally so only one processor is every created
      // as each new processors initializations will create a new redis connection.

      queue.process((job: any) => {
        original.apply(target, job.data);
      });
    }

    descriptor.value = (...args) => {
      // the function was just called.  We now add this function call to the created queue.
      queue.add(args);
    };
    return descriptor;
  };
};
