const { Logger } = require("@efuse/logger");
const { ObjectId } = require("mongodb");
const logger = Logger.create({ name: "lib/blacklist/blacklist.js" });
const { toLower } = require("lodash");
const { EFuseAnalyticsUtil } = require("../analytics.util");

const { Blacklist } = require("../../backend/models/Blacklist");
const { LoungeFeed } = require("../../backend/models/LoungeFeed");
const { Feed } = require("../../backend/models/Feed");
const { HomeFeed } = require("../../backend/models/HomeFeed");

const { Slack } = require("../slack");
const BlacklistAsync = require("../../async/blacklist");

const { DOMAIN } = require("@efuse/key-store");

/**
 * @summary Get all blacklist objects by type
 *
 * @param {String} type - e.g feeds
 * @return {Promise<any[]>} Returns an array of blacklists
 */
const getBlacklistByType = async (type) => {
  return new Promise(async (resolve, reject) => {
    try {
      const blacklists = await Blacklist.find({ type }).select("_id pattern action type").lean();
      return resolve(blacklists);
    } catch (error) {
      logger.error({ message: error.message }, "Error while fetching Blacklist");
      return reject(error);
    }
  });
};

/**
 * @summary Validate a particular text with the DB & return the matched blacklist object
 *
 * @param {String} contentToMatch - e.g leaked.wiki is a great website lol
 * @param {any[] | null} blacklists - Optional but an array of blacklist objects can be passed
 * @param {String} type - Default to feeds, it will automatically build blacklist array based on provided type
 * @return {Promise<any>} {mactchFound: Boolean, blacklist: Object}
 */
const validateContent = async (contentToMatch, blacklists = null, type = "feeds") => {
  contentToMatch = toLower(contentToMatch);
  return new Promise(async (resolve, reject) => {
    try {
      if (!blacklists) {
        blacklists = await getBlacklistByType(type);
      }
      if (!blacklists || blacklists.length === 0) {
        logger.info("Blacklist is empty");
        return resolve({ matchFound: false });
      }
      for (const blacklist of blacklists) {
        logger.info(`Matching pattern ${blacklist.pattern}`);
        const pattern = toLower(blacklist.pattern);
        if (pattern && contentToMatch.includes(pattern)) {
          logger.info({ matchFound: true, blacklist }, "Feed content match found in one of the blacklist pattern");
          return resolve({ matchFound: true, blacklist });
        }
      }
      return resolve({ matchFound: false });
    } catch (error) {
      logger.error({ message: error.message }, "Error while running blacklist validation");
      return reject(error);
    }
  });
};

/**
 * @summary Validate content of a particular feed to be created, send a slack message if it is blacklisted.
 *
 * @param {String} content | content of the feed about to be created
 * @param {String} username | username of the author of feed
 * @return {Promise<boolean>} true or false
 */
const validateFeedContent = async (content, username) => {
  return new Promise(async (resolve) => {
    try {
      const { matchFound, blacklist } = await validateContent(content);
      if (!matchFound) {
        return resolve(false);
      }
      EFuseAnalyticsUtil.incrementMetric("blacklist.feed_content");

      await Slack.sendMessage(
        `Feed has been blacklisted as it's content matched one of the blacklist pattern. \nFeed content:  ${content}\nBlacklist pattern: ${blacklist.pattern}\nPortfolio: ${DOMAIN}/u/${username}`,
        "#flagged-content"
      );

      return resolve(true);
    } catch (error) {
      logger.error({ message: error.message }, "Error while validating feed.content with blacklist collection");
      return resolve(false);
    }
  });
};

/**
 * @summary Method to cleanup Feed
 *
 * @param {ObjectId} feedId - ID of the feed to be deleted
 * @param {String} content - content of the feed, which will be cross verified with blacklist collection
 */
const cleanupFeed = async (feedId, content) => {
  try {
    const { matchFound } = await validateContent(content);
    if (!matchFound) {
      logger.info({ content }, `No match found for feed ${feedId}`);
      return;
    }
    logger.info("Removing corresponding lounge feeds");
    // todo: investigate
    // @ts-ignore
    await LoungeFeed.delete({ feed: feedId }).exec();
    logger.info("Removing corresponding home feeds");
    // todo: investigate
    // @ts-ignore
    await HomeFeed.delete({ feed: feedId }).exec();
    logger.info("Removing the corresponding feed");
    // todo: investigate
    // @ts-ignore
    await Feed.delete({ _id: feedId }).exec();
    logger.info({ feedId, content }, "Corresponding data has been removed for feed");
    return;
  } catch (error) {
    logger.error({ message: error.message, feedId }, "Error while verifying & cleaning up Feed");
    return;
  }
};

/**
 * @summary Async method to add job to queue to cleanup Feed
 *
 * @param {ObjectId} feedId - ID of the feed to be deleted
 * @param {String} content - content of the feed, which will be cross verified with blacklist collection
 */
const cleanupFeedAsync = async (feedId, content) => {
  return await BlacklistAsync.cleanupFeedQueue.add({ feedId, content });
};

module.exports.cleanupFeed = cleanupFeed;
module.exports.cleanupFeedAsync = cleanupFeedAsync;
module.exports.getBlacklistByType = getBlacklistByType;
module.exports.validateContent = validateContent;
module.exports.validateFeedContent = validateFeedContent;
