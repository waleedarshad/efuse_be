const { Logger } = require("@efuse/logger");

const { RedisHelper } = require("../../backend/helpers/redis.helper");

const logger = Logger.create();

const { client } = RedisHelper.instance();
const redisClient = client;

const expressRouteCache10s = require("express-redis-cache")({
  // todo: investigate
  // @ts-ignore
  client: client,
  expire: 10
});
const expressRouteCache300s = require("express-redis-cache")({
  // todo: investigate
  // @ts-ignore
  client: client,
  expire: 300
});

const getAsync = RedisHelper.instance().getAsync;
const setAsync = (key, value, expiration = 0) => {
  RedisHelper.instance()
    .setAsync(key, value)
    .catch((error) => logger.error(error, "Error resolving setAsync promise"));
  RedisHelper.instance()
    .expireAsync(key, expiration)
    .catch((error) => logger.error(error, "Error resolving expireAsync promise"));
};
const expireAsync = RedisHelper.instance().expireAsync;

module.exports.expireAsync = expireAsync;
module.exports.expressRouteCache10s = expressRouteCache10s;
module.exports.expressRouteCache300s = expressRouteCache300s;
module.exports.getAsync = getAsync;
module.exports.redisClient = redisClient;
module.exports.setAsync = setAsync;
