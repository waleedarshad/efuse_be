import { DocumentType } from "@typegoose/typegoose";
import { IOpportunity } from "../../backend/interfaces/opportunity";
import { FilterQuery } from "mongoose";
import { Developer } from "../../backend/models/developer.model";

import { IIpInfo } from "../../backend/interfaces/ip-info";
import { IUser } from "../../backend/interfaces/user";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";

declare global {
  namespace Express {
    export interface Request {
      graphql: Boolean;
      ipInfo: IIpInfo;
      user: IUser;
      bulletTrain: FeatureFlagService;

      // External API related
      developer: DocumentType<Developer>;
      verify_scope: string[];
      parsedFilters: FilterQuery<IOpportunity>;
    }
  }
}
