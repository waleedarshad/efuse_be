import { DocumentQuery, Document, Aggregate } from "mongoose";

declare module "cachegoose" {
  function cachegoose(mongoose: Mongoose, cacheOptions: cachegoose.Types.IOptions): void;

  namespace cachegoose {
    namespace Types {
      interface IOptions {
        engine?: string;
        port?: number;
        host?: string;
      }
    }

    function clearCache(customKey: any, cb: any): void;
  }
  // @ts-ignore
  export = cachegoose;
}

declare module "mongoose" {
  interface Aggregate<T> {
    cache(ttl?: number, customKey?: string): this;
  }

  interface DocumentQuery<T, DocType extends Document, QueryHelpers = Record<never, never>> {
    cache(ttl?: number, customKey?: string): this;
  }

  interface Query<ResultType, DocType extends Document, QueryHelpers = Record<never, never>> {
    cache(ttl?: number, customKey?: string): Query<T>;
  }
}
