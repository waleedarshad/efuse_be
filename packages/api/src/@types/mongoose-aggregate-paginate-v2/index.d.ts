import * as mongoose from "mongoose";
import { Aggregate } from "mongoose";

declare module "mongoose" {
  interface AggregateCustomLabels {
    docs?: string;
    hasNextPage?: string;
    hasPrevPage?: string;
    limit?: string;
    meta?: { [key: string]: unknown };
    nextPage?: string;
    page?: string;
    pagingCounter?: string;
    prevPage?: string;
    totalDocs?: string;
    totalPages?: string;
  }

  interface AggregatePaginateOptions {
    allowDiskUse?: boolean;
    collation?: CollationOptions;
    countQuery?: Aggregate;
    customLabels?: AggregateCustomLabels;
    lean?: boolean;
    leanWithId?: boolean;
    limit?: number;
    offset?: number;
    options?: QueryOptions | null | undefined;
    page?: number;
    pagination?: boolean;
    projection?: any;
    select?: unknown | string;
    sort?: unknown | string;
  }

  interface AggregatePaginateResult<T> {
    [customLabel: string]: T[] | number | boolean | null | undefined;
    docs: T[];
    hasNextPage: boolean;
    hasPrevPage: boolean;
    limit: number;
    meta?: unknown;
    nextPage?: number | null;
    page?: number;
    pagingCounter: number;
    prevPage?: number | null;
    totalDocs: number;
    totalPages: number;
  }

  interface AggregatePaginateModel<T extends Document> extends Model<T> {
    aggregatePaginate(
      query?: FilterQuery<T>,
      options?: AggregatePaginateOptions,
      callback?: (err: unknown, result: AggregatePaginateResult<T>) => void
    ): Promise<AggregatePaginateResult<T>>;
  }

  function model(
    name: string,
    schema?: Schema,
    collection?: string,
    skipInit?: boolean
  ): AggregatePaginateModel<unknown>;
}

declare function _(schema: mongoose.Schema): void;

export = _;

declare namespace _ {
  const aggregatePaginate: { options: mongoose.AggregatePaginateOptions };
}
