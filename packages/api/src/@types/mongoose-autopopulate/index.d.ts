import { QueryOptions } from "mongoose";

declare module "mongoose" {
  interface QueryOptions extends QueryOptions {
    autopopulate?: boolean;
  }
}
