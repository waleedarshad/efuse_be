import { Logger } from "@efuse/logger";
import { NextFunction, Request, Response } from "express";
import { IOpportunity } from "../interfaces/opportunity";
import { FilterQuery } from "mongoose";

const logger = Logger.create();

const OPPORTUNITY_TYPE = {
  scholarship: "Scholarship",
  job: "Job",
  team: "Team Opening",
  event: "Event"
};

export const parseOpportunityFilters = (req: Request, _res: Response, next: NextFunction) => {
  const { filters } = req.query;
  if (filters) {
    try {
      const decodedFilters = decodeURIComponent(filters as string);
      const parsedFilters: { title: string; opportunityType: string } = JSON.parse(decodedFilters);
      const query: FilterQuery<IOpportunity> = {};

      if (parsedFilters.title) {
        query.$text = { $search: parsedFilters.title };
      }

      if (parsedFilters.opportunityType) {
        const formatted = parsedFilters.opportunityType.trim().toLowerCase();
        const searchableType: string = OPPORTUNITY_TYPE[formatted];

        if (searchableType) {
          query.opportunityType = searchableType;
        }
      }

      req.parsedFilters = query;
    } catch (error) {
      logger.error(`Opportunity Search: Error parsing filters | ${filters}`, error);
      req.parsedFilters = {};
    }
  } else {
    req.parsedFilters = {};
  }
  return next();
};
