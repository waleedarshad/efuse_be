const { GOOGLE_RECAPTCHA3_SERVER_KEY } = require("@efuse/key-store");
const axios = require("axios");

module.exports = async (captchaToken) => {
  // todo: investigate
  // @ts-ignore
  const response = await axios({
    method: "post",
    url: "https://www.google.com/recaptcha/api/siteverify",
    params: {
      secret: GOOGLE_RECAPTCHA3_SERVER_KEY,
      response: captchaToken
    }
  });

  // Score ranges from 0 to 1, with 1 being most likely to be human
  const scoreThreshold = 0.5;
  const { success, score } = response.data;
  return success && score > scoreThreshold;
};
