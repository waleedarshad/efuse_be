import { NextFunction, Request, Response } from "express";
import { DeveloperService } from "../../lib/developers/developers.service";

const developerService = new DeveloperService();

export const validateApiKey = async (req: Request, res: Response, next: NextFunction) => {
  const API_KEY_ERROR = "EFUSE_API_KEY is missing or incorrect";
  const { efuse_api_key } = req.headers as { [key: string]: string };

  if (!efuse_api_key) {
    return res.status(401).end(API_KEY_ERROR);
  }

  const developer = await developerService.findOne(
    {
      apiKey: efuse_api_key,
      active: true
    },
    null,
    { lean: true }
  );

  if (!developer) {
    return res.status(401).end(API_KEY_ERROR);
  }

  req.developer = developer;

  next();
};
