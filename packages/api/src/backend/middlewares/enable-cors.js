const cors = require("cors");
const cache = require("memory-cache");
const { CORS_ORIGIN_WHITELIST, CORS_MAXAGE } = require("@efuse/key-store");

const getOrigin = (whitelist) => {
  if (!whitelist) {
    return true;
  }
  const key = "corsOrigin";
  const cachedOrigin = cache.get(key);
  if (cachedOrigin) {
    return cachedOrigin;
  } else {
    const origin = whitelist.split(",").map((origin) => new RegExp(origin));
    cache.put(key, origin);
    return origin;
  }
};

const corsOptionsDelegate = (req, callback) => {
  const origin = getOrigin(CORS_ORIGIN_WHITELIST);
  const maxAge = Number.parseInt(CORS_MAXAGE, 10) || 300;
  callback(null, { origin, maxAge });
};

module.exports = (app) => {
  app.use(cors(corsOptionsDelegate));
};
