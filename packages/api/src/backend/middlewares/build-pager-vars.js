module.exports = (req, res, next) => {
  const { page, pageSize, limit } = req.query;
  req.query.page = page === undefined ? 1 : parseInt(page);
  req.query.pageSize = pageSize === undefined ? 1 : parseInt(pageSize);
  req.query.limit = limit === undefined ? 1 : parseInt(limit);
  next();
};
