import { NextFunction, Request, RequestHandler, Response } from "express";
import { Logger } from "@efuse/logger";

import { EFuseSegmentAnalyticsUtil } from "../../lib/efuse-segment-analytics/efuse-segment-analytics.util";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { IIdentityAnonymous, IIdentityUser } from "../interfaces";

const logger = Logger.create();

export function trackEvent(event: string, resourceFieldName: string, extraProperties: any = {}): RequestHandler {
  return (req: Request, res: Response, next: NextFunction) => {
    try {
      const { headers, ipInfo, params, user, useragent } = req as {
        headers: any;
        ipInfo: any;
        params: any;
        user: any;
        useragent: any;
      };

      // Fetch ID of the event to be tracked
      const resourceId = params[resourceFieldName];

      let identity: IIdentityUser | IIdentityAnonymous;

      // Set identity & increment metric
      if (user) {
        // set userId from req
        EFuseAnalyticsUtil.incrementMetric("analytics.track.with_user_id_from_req");
        identity = { userId: String(user._id) };
      } else if (headers.ajs_user_id) {
        // when we have a route which is available for both logged-in & logged-out user
        EFuseAnalyticsUtil.incrementMetric("analytics.track.with_user_id_from_headers");
        identity = { userId: headers.ajs_user_id as string };
      } else if (headers.ajs_anonymous_id) {
        // for logged-out users
        EFuseAnalyticsUtil.incrementMetric("analytics.track.with_anonymous_id_from_headers");
        identity = { anonymousId: headers.ajs_anonymous_id as string };
      } else {
        // when anonymousId is not received from FE then Fallback to user's IP
        EFuseAnalyticsUtil.incrementMetric("analytics.track.with_anonymous_id_from_ip");
        identity = { anonymousId: ipInfo.ip };
      }

      // extra properties
      const properties: { [resourceFieldName: string]: string } | any = resourceId
        ? { [resourceFieldName]: resourceId }
        : {};

      // Field to be tracked
      const trackedResource = { param: resourceFieldName, value: resourceId };

      // Track event

      EFuseSegmentAnalyticsUtil.track(
        identity,
        event,
        { trackedResource, extraProperties: { ...properties, ...extraProperties } },
        {
          userAgent: useragent?.source,
          ip: ipInfo?.ip,
          location: { city: ipInfo?.city, country: ipInfo?.country, region: ipInfo?.region },
          timezone: ipInfo?.timezone,
          os: useragent?.os
        }
      );

      // Increment Metric
      EFuseAnalyticsUtil.incrementMetric(`analytics.track.${event}`);
    } catch (error) {
      logger.error(error, "Something went wrong while tracking event");
    }
    // Pass control to next middleware
    next();
  };
}
