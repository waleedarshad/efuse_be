const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const SessionsController = require("../controllers/sessions.controller");
const { User } = require("../models/User");
const { LoginValidations } = require("../validations/login.validations");
const validateRecaptcha3 = require("./validate-recaptcha3");

module.exports = async (req, res) => {
  const invalidLogin = { error: "Invalid Email or Password" };
  const blockedLogin = {
    error: "You are blocked by Admin, Please contact admin."
  };
  const userParams = req.body;
  const { email, captchaToken } = userParams;
  const { errors, isValid } = LoginValidations.validate(userParams);

  const logFunc = (msg) => {
    logger.info(`[${email}] /auth/login ${msg}`);
  };

  // confirm that the platform is set
  let { platform } = req.body;
  platform = platform || userParams.platform;

  if (!platform) {
    logger.error("Platform is not defined.");

    return res.sendStatus(403);
  }

  if (!isValid) {
    logFunc("Error logging user in: JSON.stringify(errors)");
    return res.status(400).json(errors);
  }

  const dbUser = await User.findOne({
    email: userParams.email.toLowerCase()
  }).select("+password +streamChatToken +email +emailVerificationConfirmedAt +status +dateOfBirth");

  // Only validate reCAPTCHA for logins on Production Web request
  const captchaDisabled = await req.bulletTrain.hasFeature("killswitch:global_disable_captcha", dbUser?._id);
  const isProduction = process.env.NODE_ENV === "production";
  if (isProduction && !captchaDisabled && platform !== "mobile") {
    if (!captchaToken) {
      logFunc("Recaptcha not found");
      return res.status(403).json({ error: "Recaptcha is invalid" });
    }

    const isCaptchaVerified = await validateRecaptcha3(captchaToken);
    if (!isCaptchaVerified) {
      logFunc("Recaptcha is invalid or score is too low");
      return res.status(403).json({ error: "Recaptcha is invalid" });
    }
  }

  if (!dbUser) {
    logFunc("Cannot find user in the database");
    return res.status(422).json(invalidLogin);
  }

  if (dbUser.status === "Blocked") {
    logFunc(`User blocked from logging in: ${JSON.stringify(blockedLogin)}`);
    return res.status(403).json(blockedLogin);
  }

  // Get feature flag for forcing email verification on login
  const allowUnverifiedEmailLogin = await req.bulletTrain.hasFeature("allow_unverified_email_login");

  // Only block user from logging in if the feature flag is disabled and user email is not verified
  if (dbUser && !dbUser.emailVerificationConfirmedAt && !allowUnverifiedEmailLogin) {
    logger.info("dbUser.emailVerificationConfirmedAt", dbUser.emailVerificationConfirmedAt);
    logger.info("allow_unverified_email_login", allowUnverifiedEmailLogin);
    // return res.status(403).json({ error: "Your email is not verified" });
  }

  // Forward User, and common properties
  req.dbUser = dbUser;
  req.logFunc = logFunc;
  req.invalidLoginError = invalidLogin;

  return SessionsController.create(req, res);
};
