import { Request, Response, NextFunction } from "express";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";

import { Logger, ILogger } from "@efuse/logger";

enum KillSwitchEnums {
  DISABLE_LOGIN = "killswitch:global_disable_login",
  DISABLE_REGISTRATIONS = "killswitch:global_disable_registrations",
  DISABLE_LOUNGE_FEEDS = "killswitch:global_disable_lounge_feeds",
  DISABLE_CREATE_FEEDS = "killswitch:global_disable_create_feeds"
}

const logger: ILogger = Logger.create({ name: "global-kill-switch.middleware" });

export class GlobalKillSwitch {
  public static loginKillSwitch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await GlobalKillSwitch.checkRequestAvailability(KillSwitchEnums.DISABLE_LOGIN, res, next);
  };

  public static registrationKillSwitch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await GlobalKillSwitch.checkRequestAvailability(KillSwitchEnums.DISABLE_REGISTRATIONS, res, next);
  };

  public static loungeFeedKillSwitch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await GlobalKillSwitch.checkRequestAvailability(KillSwitchEnums.DISABLE_LOUNGE_FEEDS, res, next);
  };

  public static createFeedKillSwitch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    await GlobalKillSwitch.checkRequestAvailability(KillSwitchEnums.DISABLE_CREATE_FEEDS, res, next);
  };

  private static checkRequestAvailability = async (
    killSwitch: string,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const disableResource = await FeatureFlagService.hasFeature(killSwitch);
      if (!disableResource) {
        next();
      } else {
        EFuseAnalyticsUtil.incrementMetric(`restricted_by:${killSwitch}`);
        res.status(403).json({
          error: "Requested resource is not available. Please try again later"
        });
      }
    } catch (error) {
      logger.error(error, "error checking request availability");
      next();
    }
  };
}
