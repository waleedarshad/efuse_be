import rateLimit from "express-rate-limit";

export const apiRateLimit = rateLimit({
  windowMs: 60000, // 1 minute
  max: 100, // limit each IP to 100 requests per windowMs,
  message: "Too many requests, please try again later."
});
