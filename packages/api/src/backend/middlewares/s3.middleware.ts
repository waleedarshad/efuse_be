import { Failure } from "@efuse/contracts";
import { AWS_S3_BUCKET, AWS_S3_REGION } from "../../config";
import { v4 as uuidv4 } from "uuid";
import S3Router from "react-s3-uploader/s3router";
import { NextFunction, Request, Response, Router } from "express";

export class S3Middleware {
  public static get router(): Router {
    return <Router>S3Router(
      {
        getFileKeyDir: (req: Request): string => <string>req.query.dir,
        bucket: AWS_S3_BUCKET,
        region: AWS_S3_REGION,
        headers: { "Access-Control-Allow-Origin": "*" },
        ACL: "public-read",
        uniquePrefix: false
      },
      (req: Request, res: Response, next: NextFunction): void => {
        try {
          const { objectName } = req.query;
          if (!objectName) {
            throw Failure.BadRequest("objectName is required");
          }

          const fileExtension = (<string>objectName).split(".").pop() as string;
          const filename = `${uuidv4()}-${Date.now()}.${fileExtension}`;
          req.query.objectName = filename;

          next();
        } catch (error) {
          next(error);
        }
      }
    );
  }
}
