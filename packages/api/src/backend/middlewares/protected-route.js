const passport = require("passport");

const protectRoute = passport.authenticate(["efuseJwt", "efuseAuth0Jwt"], { session: false });

const appendUserToLogger = (req, res, next) => {
  const { user } = req;
  if (user) {
    res.log = req.log = req.log.child({
      user: { id: user.id, email: user.email }
    });
  }
  next();
};

module.exports = [protectRoute, appendUserToLogger];
