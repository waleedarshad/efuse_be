const jwt = require("jsonwebtoken");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const useragent = require("express-useragent");

const { User } = require("../models/User");
const { VerifyToken } = require("../../lib/verify-token.service");
const { EfuseAuthEnum } = require("../../lib/auth/auth.enum");

const verifyToken = new VerifyToken();

module.exports = (io) => {
  io.use(async (socket, next) => {
    if (socket.handshake?.query?.token) {
      try {
        const { token } = socket.handshake.query;
        if (token !== "undefined") {
          const user = await verifyToken.validate(token);

          const decodedToken = jwt.decode(token);

          // todo: investigate
          // @ts-ignore
          decodedToken.id = user._id;

          socket.decoded = decodedToken;

          next();
        }
      } catch (error) {
        logger.warn({ error }, "Error: Invalid Token");
        next(new Error("Authentication error"));
      }
    } else {
      logger.warn("Error: Invalid Token");
      next(new Error("Authentication error"));
    }
  }).on("connection", async (socket) => {
    let userAgentObject = {};
    try {
      userAgentObject = useragent.parse(socket.request.headers["user-agent"]);
    } catch (error) {
      logger.warn(error, "Error while parsing user-agent");
      userAgentObject = {};
    }

    await toggleOnlineStatus(true, socket);
    socket.on("subscribeChat", (roomId) => {
      socket.join(roomId);
    });
    socket.on("privateRoom", (userId) => {
      socket.join(userId);
    });
    socket.on("disconnect", async () => {
      await toggleOnlineStatus(false, socket);
    });
  });
};

const toggleOnlineStatus = async (onlineStatus, socket) => {
  let online = onlineStatus;
  const onlineText = online ? "Online" : "Offline";
  const { id } = socket.decoded;
  try {
    const user = await User.findOne({ _id: id }).select("online");

    // todo: investigate
    // @ts-ignore
    user.online = online;

    // todo: investigate
    // @ts-ignore
    await user.save({ validateBeforeSave: false });
  } catch (error) {
    logger.warn(error, `Error: ${onlineText}`);
    online = false;
  }
  socket.broadcast.emit("ONLINE_STATUS", { online, id });
};
