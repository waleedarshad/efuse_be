import { intersection } from "lodash";
import { NextFunction, Request, Response } from "express";

export const validateOpportunitiesScope = (req: Request, res: Response, next: NextFunction) => {
  req.verify_scope = ["opportunities.read"];
  validateScope(req, res, next);
};

export const validatePipelineScope = (req: Request, res: Response, next: NextFunction) => {
  req.verify_scope = ["pipeline.read"];
  validateScope(req, res, next);
};

const validateScope = (req: Request, res: Response, next: NextFunction) => {
  const unauthorizedMsg = "You are not authorized to access this scope";
  try {
    if (!req.developer.scope) {
      return res.status(401).end(unauthorizedMsg);
    }

    const scopes = req.developer.scope.split(" ");

    if (intersection(scopes, req.verify_scope).length === 0) {
      return res.status(401).end(unauthorizedMsg);
    }

    return next();
  } catch {
    return res.status(401).end(unauthorizedMsg);
  }
};
