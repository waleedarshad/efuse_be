module.exports = (req, res, next) => {
  Object.keys(req.body).forEach((key) => {
    if (req.body[key] === "null") req.body[key] = null;
  });
  next();
};
