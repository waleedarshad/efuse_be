import { Request, Response, NextFunction } from "express";
import rateLimit from "express-rate-limit";
import { LOGIN_RATE_LIMIT, SIGNUP_RATE_LIMIT } from "@efuse/key-store";
import { Logger, ILogger } from "@efuse/logger";
import { Failure } from "@efuse/contracts";

import { EFuseAnalyticsUtil } from "../../lib/analytics.util";

enum RateLimitConfigEnum {
  ERROR = "Too many requests, please try again later.",
  LOGGER_NAME = ".rate-limit.middleware"
}

enum RateLimitMetricEnum {
  DEFAULT_METRIC = "rate_limits",
  RESET_PASSWORD = "rate_limits.reset_password",
  USER_REGISTRATION = "rate_limits.user_registrations",
  USER_LOGIN = "rate_limits.user_login",
  DELETE_ACCOUNT = "rate_limits.delete_account"
}

export class RateLimitMiddleware {
  private static logger: ILogger = Logger.create({ name: RateLimitConfigEnum.LOGGER_NAME });

  public static rateLimitResetPassword = rateLimit({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 5, // limit each IP to 5 requests per windowMs,
    handler: (_req: Request, _res: Response, next: NextFunction) =>
      RateLimitMiddleware.onLimitReached(next, RateLimitMetricEnum.RESET_PASSWORD)
  });

  public static rateLimitUserRegistration = rateLimit({
    windowMs: 60 * 60 * 1000, // 60 minutes
    max: Number(SIGNUP_RATE_LIMIT), // limit each IP to 100 request for staging && 10 requests for production per windowMs,
    handler: (_req: Request, _res: Response, next: NextFunction) =>
      RateLimitMiddleware.onLimitReached(next, RateLimitMetricEnum.USER_REGISTRATION)
  });

  public static rateLimitUserLogin = rateLimit({
    windowMs: 60 * 60 * 1000, // 60 minutes
    max: Number(LOGIN_RATE_LIMIT), // limit each IP to 300 requests for staging & 100 requests for production per windowMs,
    handler: (_req: Request, _res: Response, next: NextFunction) =>
      RateLimitMiddleware.onLimitReached(next, RateLimitMetricEnum.USER_LOGIN)
  });

  public static rateLimitDeleteAccount = rateLimit({
    windowMs: 10 * 60 * 1000, // 10 minutes
    max: 5, // limit each IP to 5 requests per windowMs,
    handler: (_req: Request, _res: Response, next: NextFunction) =>
      RateLimitMiddleware.onLimitReached(next, RateLimitMetricEnum.DELETE_ACCOUNT)
  });

  private static onLimitReached = (next: NextFunction, metricName = RateLimitMetricEnum.DEFAULT_METRIC) => {
    EFuseAnalyticsUtil.incrementMetric(metricName);

    RateLimitMiddleware.logger.warn({ msg: "Hit rate limit" });

    return next(Failure.TooManyRequests(RateLimitConfigEnum.ERROR));
  };
}
