import { NextFunction, Request, RequestHandler, Response } from "express";
import * as MemoryCache from "memory-cache";

/**
 * Cache the server response for a number of seconds equal to the TTL
 * @param ttl
 */
export function cache(ttl: number): RequestHandler {
  return (req: Request, res: Response, next: NextFunction) => {
    const key = `__express__${req.originalUrl || req.url}`;
    const cachedBody = MemoryCache.get(key);

    if (cachedBody) {
      // need to parse the cache as it stores a string, but we want to return a json object
      res.send(JSON.parse(cachedBody));
    } else {
      // save the old send

      res["sendResponse"] = res.send;

      // monkey patch the send to include a memory cache save
      res.send = (body) => {
        MemoryCache.put(key, body, ttl * 1000);

        return res["sendResponse"](body);
      };
      next();
    }
  };
}
