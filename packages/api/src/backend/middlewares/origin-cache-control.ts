import { NextFunction, Request, RequestHandler, Response } from "express";

/**
 * Set the Cache-Control headers to signal to the CDN how long to cache the
 * content
 * @param maxAge
 */
export function OriginCacheControl(maxAge: number, staleWhileRevalidate = 0, staleIfError = 0): RequestHandler {
  return (req: Request, res: Response, next: NextFunction) => {
    // need to parse the cache as it stores a string, but we want to return a json object
    // .set("Cache-Control", "public,max-age=60,stale-if-error=600,stale-while-revalidate=300")
    res.set(
      "Cache-Control",
      `public, max-age=${maxAge}, stale-if-error=${staleIfError}, stale-while-revalidate=${staleWhileRevalidate}`
    );
    next();
  };
}
