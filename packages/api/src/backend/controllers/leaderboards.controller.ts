import { NextFunction, Request, Response } from "express";

import { PipelineLeaderboardService } from "../../lib/leaderboards/pipeline-leaderboard.service";
import { BaseController } from "./base.controller";

const NAME = "leaderboards.controller";

export class LeaderboardsController extends BaseController {
  private $pipelineLeaderboardService: PipelineLeaderboardService;

  constructor() {
    super(NAME);

    this.$pipelineLeaderboardService = new PipelineLeaderboardService();
  }

  /**
   * @summary Get LOL Leaderboard
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LeaderboardsController
   */
  public getLOLLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const leaderboard = await this.$pipelineLeaderboardService.getLOLLeaderboardDeprecated();

      res.json({ leaderboard });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Get Overwatch Leaderboard
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LeaderboardsController
   */
  public getOverwatchLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const leaderboard = await this.$pipelineLeaderboardService.getOverwatchLeaderboard();

      res.json({ leaderboard });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
