import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";
import { BaseController } from "./base.controller";
import { validateTournament } from "../validations/tournament";
import { GeneralHelper } from "../helpers/general.helper";
import { File } from "../interfaces/file";
import { ERenaPlayerService } from "../../lib/erena/erena-player.service";
import {
  IERenaMatch,
  IERenaMatchCreateParams,
  IERenaResponse,
  IERenaRoundCreateParams,
  IERenaTournament,
  IERenaTournamentFileUploader,
  IERenaTournamentPopulated
} from "../interfaces/erena";

import { ERenaService } from "../../lib/erena/erena.service";
import { ERenaBracketService } from "../../lib/erena/erena-bracket.service";
import { ERenaTournamentService } from "../../lib/erena/erena-tournament.service";
import { ERenaMatchService } from "../../lib/erena/erena-match.service";
import { ERenaRoundService } from "../../lib/erena/erena-round.service";
import { ERenaSharedService } from "../../lib/erena/erena-shared.service";
import { ERenaGameService } from "../../lib/erena/erena-game.service";

const NAME = "erena.controller";

/**
 * @deprecated This controller should not be used for any new development and is here only for backwards compatibility. Use `/erena-v2/` controllers instead
 */
export class ERenaController extends BaseController {
  private erenaService: ERenaService;
  private erenaBracketService: ERenaBracketService;
  private erenaTournamentService: ERenaTournamentService;
  private erenaPlayerService: ERenaPlayerService;
  private erenaMatchService: ERenaMatchService;
  private erenaRoundService: ERenaRoundService;
  private erenaSharedService: ERenaSharedService;
  private erenaGameService: ERenaGameService;

  constructor() {
    super(NAME);

    this.erenaService = new ERenaService();
    this.erenaBracketService = new ERenaBracketService();
    this.erenaTournamentService = new ERenaTournamentService();
    this.erenaPlayerService = new ERenaPlayerService();
    this.erenaMatchService = new ERenaMatchService();
    this.erenaRoundService = new ERenaRoundService();
    this.erenaSharedService = new ERenaSharedService();
    this.erenaGameService = new ERenaGameService();
  }

  /**
   * @deprecated use ERenaTournamentController.getTeamLeaderboard
   */
  public teamLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query } = req;
      const leaderboard = await this.erenaService.getTeamLeaderboard(
        params.tournamentIdOrSlug,
        query.namespace as string
      );
      res.json(leaderboard);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use ERenaTournamentController.getPlayerLeaderboard
   */
  public playerLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query } = req;
      const leaderboard = await this.erenaService.getPlayerLeaderboard(
        params.tournamentIdOrSlug,
        query.namespace as string
      );
      res.json(leaderboard);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ERenaTournamentController.postTournamnet
   */
  public createTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const tournamentParams: IERenaTournament = req.body as IERenaTournament;
    try {
      this.logger.info("Validating tournament");
      this.logger.info({ body: tournamentParams }, "tournamentParams are:");

      const { errors, isValid } = (await validateTournament(tournamentParams)) as {
        errors: string[];
        isValid: boolean;
      };

      if (!isValid) {
        throw Failure.BadRequest(errors.join(" "));
      }

      const tournament = await this.erenaService.createTournament(tournamentParams);

      if ("game" in tournamentParams) {
        await this.erenaGameService.associateGame(tournament._id, tournamentParams.game as string);
      }

      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ERenaTournamentController.getTournament
   */
  public getTournamentStats = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { namespace } = req.query;
      const tournament = await this.erenaService.getTournamentStats(tournamentIdOrSlug, namespace as string);
      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ErenaMatchController.postMatch
   */
  public createMatch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; user: any; params: any };

      await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(user._id, params.tournamentIdOrSlug);

      const match = await this.erenaMatchService.createMatch(
        user._id,
        params.tournamentIdOrSlug,
        body as IERenaMatchCreateParams
      );
      res.json({ match });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ErenaMatchController.patchMatch
   */
  public updateMatch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; user: any; params: any };

      const match = await this.erenaBracketService.updateMatch(
        user._id,
        params.tournamentIdOrSlug,
        params.matchId,
        body as IERenaMatch
      );
      res.json({ match });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ErenaRoundController.postRound
   */
  public createRound = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; user: any; params: any };

      await this.erenaSharedService.verifyTournamentOwnershipAndRetrieve(user._id, params.tournamentIdOrSlug);

      const round = await this.erenaBracketService.createRound(
        params.tournamentIdOrSlug,
        body as IERenaRoundCreateParams
      );
      res.json({ round });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ErenaRoundController.patchRound
   */
  public updateRound = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; user: any; params: any };

      const round = await this.erenaRoundService.updateRound(user._id, params.roundId, body as IERenaRoundCreateParams);
      res.json({ round });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ErenaTournamentController.updateTournamentAd
   */
  public updateTournamentAd = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, files, params, user } = req as { body: any; files: any[]; params: any; user: any };
      let imageURL: string | undefined;
      const uploadedFiles: IERenaTournamentFileUploader = <IERenaTournamentFileUploader>files;

      if (uploadedFiles && uploadedFiles.image) {
        const image: File = GeneralHelper.buildFileObject(uploadedFiles.image);
        imageURL = image.url;
      }

      const original = await this.erenaSharedService.getTournamentByIdOrSlug(params.tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      const tournament: IERenaTournamentPopulated = await this.erenaTournamentService.addOrRemoveAd(
        original,
        user?._id,
        body.index,
        true,
        imageURL,
        body.link
      );
      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ErenaTournamentController.removeTournamentAd
   */
  public removeTournamentAd = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };

      const original = await this.erenaSharedService.getTournamentByIdOrSlug(params.tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      const tournament: IERenaTournamentPopulated = await this.erenaTournamentService.addOrRemoveAd(
        original,
        user?._id,
        Number(params.index),
        false
      );
      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated Use ERenaTournamentController
   */
  public deleteTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };
      const response: IERenaResponse = await this.erenaTournamentService.markAsDeleted(
        params.tournamentIdOrSlug,
        user?._id
      );
      res.json({ message: response.result });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ERenaTournamentController.getTournament
   */
  public getTournamentInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query } = req;
      const info = await this.erenaTournamentService.getByIdOrSlug(
        params.tournamentIdOrSlug,
        query.namespace as string
      );
      res.json(info);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated No longer in use anywhere
   */

  public getTournamentList = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    throw Failure.NotImplemented("Not implemented.");
  };

  /**
   * @deprecated ERenaTournamentController.updateTournament
   */
  public updateTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, body, files, user } = req as { body: any; files: any[]; params: any; user: any };
      const uploadedFiles: IERenaTournamentFileUploader = <IERenaTournamentFileUploader>files;
      const tournamentParams: IERenaTournament = <IERenaTournament>body;

      const original = await this.erenaTournamentService.verifyPermissionAndRetrieve(
        user._id,
        params.tournamentIdOrSlug
      );

      if (!original) {
        throw Failure.NotFound("Unable to find tournament to update.");
      }

      if (uploadedFiles) {
        if (uploadedFiles.brandLogoUrl) {
          const logoImage: File = GeneralHelper.buildFileObject(uploadedFiles.brandLogoUrl);
          if (logoImage.url) {
            tournamentParams.brandLogoUrl = logoImage.url;
          }
        }

        if (uploadedFiles.backgroundImageUrl) {
          const backgroundImage: File = GeneralHelper.buildFileObject(uploadedFiles.backgroundImageUrl);
          if (backgroundImage.url) {
            tournamentParams.backgroundImageUrl = backgroundImage.url;
          }
        }
      }

      const updatedTournament = await this.erenaTournamentService.update(params.tournamentIdOrSlug, tournamentParams);

      if ("game" in tournamentParams) {
        await this.erenaGameService.associateGame(updatedTournament._id, tournamentParams.game as string);
      }

      res.json(updatedTournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use ERenaTeamController.postTeam
   */
  public createTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const team = await this.erenaService.createTeam(params.tournamentIdOrSlug, user?._id, body.name, body.type);
      res.json(team);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ERenaTeamController.patchTeam
   */
  public updateTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const team = await this.erenaService.updateTeam(
        params.tournamentIdOrSlug,
        user?._id,
        params.erenaTeamId,
        body.name,
        body.type
      );
      res.json(team);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use ERenaTeamController.getTeamsForTournament
   */
  public listTeams = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { query, params } = req;
      const teams = await this.erenaService.getTeamsForTournament(params.tournamentIdOrSlug, query.namespace as string);
      res.json(teams);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use ERenaPlayerController.postPlayer
   */
  public createPlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };

      const player = await this.erenaService.createPlayer(
        params.tournamentIdOrSlug,
        params.erenaTeamId,
        body.name,
        user?._id,
        body.type,
        body?.userId
      );
      res.json(player);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated user ERenaPlayerController.patchPlayer
   */
  public updatePlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };

      const player = await this.erenaService.updatePlayerStats(
        params.tournamentIdOrSlug,
        params.erenaPlayerId,
        params.erenaTeamId,
        body,
        user?._id
      );
      res.json(player);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated user ERenaTeamController.deleteTeam
   */
  public deleteTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };
      const response = await this.erenaService.deleteTeam(params.tournamentIdOrSlug, user?._id, params.erenaTeamId);
      res.json({ message: response.result });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated user ERenaPlayerController.deletePlayer
   */
  public deletePlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };
      const response = await this.erenaService.deletePlayer(params.tournamentIdOrSlug, user?._id, params.erenaPlayerId);
      res.json({ message: response.result });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use ERenaPlayerController.getPlayersForTeam
   */
  public listPlayers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const players = await this.erenaPlayerService.getByTeam(req.params.erenaTeamId);
      res.json(players);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated ERenaMatchController.saveScreenshot
   */
  public uploadScreenshot = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const match = await this.erenaBracketService.uploadScreenshot(
        user._id,
        params.tournamentIdOrSlug,
        params.erenaMatchId,
        body.teamOneImage,
        body.teamTwoImage
      );
      res.json({ match });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
