const { Logger } = require("@efuse/logger");
const logger = Logger.create({ name: "controllers/livestream_controller" });

const LiveStreamLib = require("../../lib/livestream");

const getLivestreams = async (req, res) => {
  try {
    const liveStreams = await LiveStreamLib.getCarouselLiveStreams();
    return res.json({ liveStreams });
  } catch (err) {
    const errMsg = err.message;
    logger.error({ message: errMsg, stack: err.stack });
    return res.status(500).json({ error: errMsg });
  }
};

module.exports.getLivestreams = getLivestreams;
