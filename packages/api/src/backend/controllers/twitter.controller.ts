import { Failure } from "@efuse/contracts";
import { OwnerKind, OAuthServiceKind } from "@efuse/entities";
import { DOMAIN, JWT_KEY } from "@efuse/key-store";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";

import { io } from "../config/get-io-object";
import { BaseController } from "./base.controller";
import { IOAuthRequestTokenResponse } from "../interfaces/twitter/auth";
import { IUser } from "../interfaces/user";
import { TwitterAuthConstants } from "../../lib/twitter/twitter-auth-constants";
import { TwitterAuthService } from "../../lib/twitter/twitter-auth.service";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { RedisCacheService } from "../../lib/redis-cache.service";

const CACHE_KEY = "oauth_token";
const CACHE_KEY_REQUESTING_USER = "authed_user";
const BEARER = "Bearer";
const NAME = "twitter.controller";

export class TwitterController extends BaseController {
  private twitterAuthService: TwitterAuthService;
  private redisCacheService: RedisCacheService;

  constructor() {
    super(NAME);

    this.twitterAuthService = new TwitterAuthService();
    this.redisCacheService = new RedisCacheService();
  }

  public login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { bearer } = req.query;
      this.guard(bearer, Failure.BadRequest("No bearer found"));
      EFuseAnalyticsUtil.incrementMetric("auth.external.twitter.login", 1, [`bearer:${<string>bearer}`]);
      this.logger.info({ query: req.query }, "Twitter auth login request");

      const parts = (<string>bearer)?.split(" ") || [];
      const payloadIsCorrect = parts.length === 2 && parts[0] === BEARER;
      this.guard(payloadIsCorrect, Failure.BadRequest("Invalid bearer token"));

      const [, token] = parts;
      this.guard(token, Failure.BadRequest("Token is required"));

      const decodedToken = jwt.verify(token, JWT_KEY);
      this.guard(decodedToken, Failure.Unauthorized("Invalid token"));

      const decodedAsObject = typeof decodedToken === "object";
      this.guard(decodedAsObject, Failure.Unauthorized("Invalid token - contents malformed"));

      const { id }: { id: string } = <any>decodedToken;
      this.guard(id, Failure.Unauthorized("Invalid user"));

      this.logger.info({ id }, "Decoded user id");
      // fetch oauth token & redirect to fe
      const oauthResponse: IOAuthRequestTokenResponse = await this.twitterAuthService.getOauthRequestToken();
      if (!oauthResponse) {
        throw Failure.Unauthorized("An error occurred generating a request token from twitter");
      }

      /**
       * Write to redis, using the returned OAuth token as a key, and the
       * stringified ObjectId as the value. This will allow us to retrieve
       * the appropriate user when the callback is received.
       */
      const key = `${CACHE_KEY}:${oauthResponse.oauth_token}`;
      const value = id;

      this.logger.info({ key, value }, "Storing user id and oauth token");

      await this.redisCacheService.writeToCache(key, value);

      res.redirect(`${TwitterAuthConstants.BASE_URL}/authorize?oauth_token=${oauthResponse.oauth_token}`);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public loginSecured = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { orgId } = req.query;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      this.logger.info({ id: user._id, username: user.username, orgId }, "Twitter secure auth login request");

      EFuseAnalyticsUtil.incrementMetric("auth.external.twitter.loginSecured", 1, [
        `userId:${user._id.toHexString()}`,
        `username:${user.username}`,
        `orgId:${orgId}`
      ]);

      // fetch oauth token & redirect to fe
      const oauthResponse: IOAuthRequestTokenResponse = await this.twitterAuthService.getOauthRequestToken();
      if (!oauthResponse) {
        throw Failure.Unauthorized("An error occurred generating a request token from twitter");
      }

      /**
       * Write to redis, using the returned OAuth token as a key, and the
       * stringified ObjectId as the value. This will allow us to retrieve
       * the appropriate user when the callback is received.
       */
      const key = `${CACHE_KEY}:${oauthResponse.oauth_token}`;
      const value = orgId ? `${OwnerKind.ORGANIZATION}:${orgId}` : `${OwnerKind.USER}:${user._id.toHexString()}`;

      this.logger.info({ key, value }, "Storing user/org id and oauth token");
      await this.redisCacheService.writeToCache(key, value);

      const requestingUserKey = `${CACHE_KEY_REQUESTING_USER}:${oauthResponse.oauth_token}`;
      const requestingUserValue = user._id.toHexString();
      await this.redisCacheService.writeToCache(requestingUserKey, requestingUserValue);

      res.json({ url: `${TwitterAuthConstants.BASE_URL}/authorize?oauth_token=${oauthResponse.oauth_token}` });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public handleCallback = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { oauth_token: token, oauth_verifier: verifier } = req.query;

      EFuseAnalyticsUtil.incrementMetric("auth.external.twitter.callback", 1, [
        `oauth_token:${<string>token}`,
        `oauth_verifier:${<string>verifier}`
      ]);

      this.logger.info({ token, verifier }, "Twitter auth callback");

      /**
       * Retrieve the cached token and user pair, validate it matches
       * this request and send a token request.
       */
      const key = `${CACHE_KEY}:${<string>token}`;
      let ownerId = await this.redisCacheService.readFromCache(key);
      let ownerKind: string = OwnerKind.USER;

      // Making sure owner id exists
      if (!ownerId) {
        throw Failure.Unauthorized("An error occurred linking twitter account");
      }

      const ownerIdSegments = ownerId.split(":");
      if (ownerIdSegments.length > 1) {
        ownerKind = ownerIdSegments[0];

        ownerId = ownerIdSegments[1];
      }

      this.logger.info({ ownerId }, "Retrieved owner id from redis");

      if (!ownerId) {
        throw Failure.Unauthorized("An error occurred linking twitter account");
      }

      const twitterAccountInfo = await this.twitterAuthService.saveOAuthAccessToken(
        ownerId,
        ownerKind as OwnerKind,
        <string>token,
        <string>verifier
      );

      // send socket.io event to refresh the user's page
      const requestingUserKey = `${CACHE_KEY_REQUESTING_USER}:${<string>token}`;
      const requestingUserId = await this.redisCacheService.readFromCache(requestingUserKey);

      if (!requestingUserId) {
        this.logger.warn("Unable to identify user to send linking success event to.");
      } else {
        io.to(requestingUserId).emit("TWITTER_EXTERNAL_ACCOUNT_LINKING", {
          owner: ownerId,
          service: OAuthServiceKind.TWITTER,
          status: "success",
          data: {
            accountName: twitterAccountInfo.username,
            supporterCount: twitterAccountInfo.followers
          }
        });
      }

      let redirectRoute = `${DOMAIN}/settings/external_accounts/twitter?token=${ownerId}`;
      if (ownerKind === OwnerKind.ORGANIZATION) {
        redirectRoute = `${DOMAIN}/callback`;
      }

      res.redirect(redirectRoute);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
