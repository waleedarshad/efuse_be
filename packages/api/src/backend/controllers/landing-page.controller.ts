import { NextFunction, Request, Response } from "express";
import { LandingPageService } from "../../lib/landing-page/landing-page.service";
import { BaseController } from "./base.controller";

const NAME = "landing-page.controller";

export class LandingPageController extends BaseController {
  private landingPageService: LandingPageService;

  constructor() {
    super(NAME);

    this.landingPageService = new LandingPageService();
  }

  /**
   * @deprecated Use GraphQL
   * @summary Get a particular landing page by slug
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LandingPageController
   */
  public getBySlug = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const landingPage = await this.landingPageService.findOneBySlug(req.params.slug);
      res.json({ landingPage });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
