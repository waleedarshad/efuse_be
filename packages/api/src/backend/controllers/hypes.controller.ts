import { Request, Response, NextFunction } from "express";

import { Failure } from "@efuse/contracts";
import { BaseController } from "./base.controller";
import { HypeActionsService } from "../../lib/hype/hype-actions.service";
import { IUser } from "../interfaces";

const NAME = "hypes.controller";
export class HypesController extends BaseController {
  private $hypeActionsService: HypeActionsService;

  constructor() {
    super(NAME);
    this.$hypeActionsService = new HypeActionsService();
  }

  public newPostHype = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const user = req.user as IUser | undefined;

      const { feedId } = req.params as { feedId: string };

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      if (!feedId) {
        throw Failure.BadRequest("Missing feedId");
      }

      const body = req.body as { hypeCount: number } | undefined;
      if (!body?.hypeCount) {
        throw Failure.BadRequest("Missing hypeCount");
      }

      const hypeCount = Number(body.hypeCount);
      const hypeResult = await this.$hypeActionsService.create(feedId, user._id, hypeCount);
      res.json(hypeResult);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
