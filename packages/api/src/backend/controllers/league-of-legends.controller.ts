import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";
import { BaseController } from "./base.controller";

import { LeagueOfLegendsService } from "../../lib/leagueOfLegends/league-of-legends.service";
import { IUserLeagueOfLegendsInfo } from "../interfaces/user-league-of-legends-info";

const NAME = "league-of-legends.controller";
/**
 * Controller to interact with League of Legends api
 */
export class LeagueOfLegendsController extends BaseController {
  private service: LeagueOfLegendsService;

  constructor() {
    super(NAME);

    this.service = new LeagueOfLegendsService();
  }

  /**
   * Updates a user's riot info for League of Legends
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LeagueOfLegendsController
   */
  public updateSummoner = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = req as { user: any };
    const { riotNickname, riotRegion } = req.params;

    try {
      if (!riotNickname) {
        throw Failure.BadRequest("Missing parameter 'riotNickname'");
      }

      const newRiotInfo: IUserLeagueOfLegendsInfo = { riotNickname, riotRegion };

      const leagueInfo = await this.service.updateRiotInfo(user.id, newRiotInfo);

      res.status(200).json(leagueInfo);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Manually refresh and retrieve a user's stats for League of Legends
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LeagueOfLegendsController
   */
  public get = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = req as { user: any };
    const { refreshStats } = req.query as { [key: string]: string };

    try {
      if (!user.leagueOfLegends?.riotNickname) {
        throw Failure.BadRequest("User has not linked League of Legends account.");
      }

      const leagueInfo = await (refreshStats === "true"
        ? this.service.syncUserLeagueOfLegendsStats(user._id)
        : this.service.getUserLeagueOfLegendsStats(user._id));

      res.status(200).json(leagueInfo);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Verify the currently authed user's saved Riot info
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LeagueOfLegendsController
   */
  public verifySummoner = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = req as { user: any };

    try {
      const verified = await this.service.verifyLeagueOfLegendsSummonerByUserId(user._id);

      res.status(200).json({ verified });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Retrieve the stats for the specified summoner.
   * Note: This only retrieves the stats for the current season.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LeagueOfLegendsController
   */
  public getSummonerStats = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { riotNickname, riotRegion } = req.params;

    try {
      if (!riotNickname) {
        throw Failure.BadRequest("Missing parameter 'riotNickname'");
      }

      const leagueStats = await this.service.getLeagueOfLegendsStats(riotNickname, riotRegion);

      if (!leagueStats) {
        throw Failure.UnprocessableEntity("Unable to find summoner stats for supplied name");
      }

      res.status(200).json(leagueStats);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
