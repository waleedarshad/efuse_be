const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");

const { PromoCode } = require("../models/PromoCode");
const { oauthToken, getPayoutAccount } = require("../../lib/stripe");

const completeOauth = async (req, res) => {
  try {
    EFuseAnalyticsUtil.incrementMetric(`stripe_controller.completeOauth.call`);
    const { body, user } = req;
    const { code } = body;

    if (!code) {
      EFuseAnalyticsUtil.incrementMetric(`stripe_controller.completeOauth.failure`);
      return res.status(400).json({ error: "Required parameter: code" });
    }

    // check if user already has payout account
    const account = await getPayoutAccount(user.id);

    // user can only have one payout account - return 400 if they are trying to create a second account
    if (account) {
      EFuseAnalyticsUtil.incrementMetric(`stripe_controller.completeOauth.success`);
      return res.status(200).json({ message: "Payout Account already created for this user." });
    }

    const response = await oauthToken(code, user.id);

    // If successful, send a blank response with 200 status. Client does not need any data from stripe response.
    EFuseAnalyticsUtil.incrementMetric(`stripe_controller.completeOauth.success`);
    return res.status(200).json({});
  } catch (err) {
    EFuseAnalyticsUtil.incrementMetric(`stripe_controller.completeOauth.failure`);
    logger.error("Error in stripe_controller when completing stripe connect account OAuth flow", err);
    return res.status(500).json(err);
  }
};

const getStripePayoutAccount = async (req, res) => {
  try {
    EFuseAnalyticsUtil.incrementMetric(`stripe_controller.getStripeDashboardLink.call`);
    const { user } = req;

    const account = await getPayoutAccount(user.id);

    EFuseAnalyticsUtil.incrementMetric(`stripe_controller.getStripeDashboardLink.success`);
    return res.status(200).json({ account });
  } catch (err) {
    EFuseAnalyticsUtil.incrementMetric(`stripe_controller.getStripeDashboardLink.failure`);
    logger.error("Error in stripe_controller when getting Stripe express dashboard link.", err);
    return res.status(500).json(err);
  }
};

module.exports.completeOauth = completeOauth;
module.exports.getStripePayoutAccount = getStripePayoutAccount;
