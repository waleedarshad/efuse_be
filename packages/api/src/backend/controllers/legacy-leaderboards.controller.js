const LeaderboardLib = require("../../lib/leaderboards");

const deprecatedGetAll = async (req, res, next) => {
  try {
    const { page, limit } = req.query;
    const leaderboards = await LeaderboardLib.deprecatedGetActiveLeaderboard(page, limit);
    return res.json(leaderboards);
  } catch (error) {
    return next(error);
  }
};

const getAll = async (req, res, next) => {
  try {
    const { params, query } = req;
    const { page, limit } = query;
    const leaderboards = await LeaderboardLib.getActiveLeaderboard(params.boardType, page, limit);
    return res.json(leaderboards);
  } catch (error) {
    return next(error);
  }
};

const getTopLeaderboards = async (req, res, next) => {
  try {
    const { limit } = req.params;
    const leaderboards = await LeaderboardLib.getTopperFromAllLeaderboards(limit);
    return res.json(leaderboards);
  } catch (error) {
    return next(error);
  }
};

module.exports.deprecatedGetAll = deprecatedGetAll;
module.exports.getAll = getAll;
module.exports.getTopLeaderboards = getTopLeaderboards;
