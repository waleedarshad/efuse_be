import { Request, Response, NextFunction } from "express";

import { BaseController } from "./base.controller";
import { IUser } from "../interfaces/user";
import { TalentCardsService } from "../../lib/users/talent-cards.service";

const NAME = "talent-cards.controller";

export class TalentCardsController extends BaseController {
  private service: TalentCardsService;

  constructor() {
    super(NAME);

    this.service = new TalentCardsService();
  }

  /**
   * @deprecated In favor of graphQL query GetTalentCards
   */
  public getRandomUsers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user, query } = req;

      const users = await this.service.randomUsers((<IUser>user)._id, Number(query.limit));
      res.json({ users });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
