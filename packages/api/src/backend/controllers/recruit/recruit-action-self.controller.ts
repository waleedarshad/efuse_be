import { NextFunction, Request, Response } from "express";

import { BaseController } from "../base.controller";
import { RecruitActionSelfService } from "../../../lib/recruitment/recruit/recruit-action-self.service";

const NAME = "recruit-action.controller";

export class RecruitActionSelfController extends BaseController {
  private recruitActionSelfService: RecruitActionSelfService;

  constructor() {
    super(NAME);
    this.recruitActionSelfService = new RecruitActionSelfService();
  }

  public performRecruitAction = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };

      const recruitAction = await this.recruitActionSelfService.createORUpdateRecruitAction(
        user._id,
        body.recruitmentProfileId,
        user._id,
        body.action
      );
      res.json({ recruitAction });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
