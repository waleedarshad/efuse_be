import { Request, Response, NextFunction } from "express";

import { BaseController } from "../base.controller";
import { RecruitActionOrganizationService } from "../../../lib/recruitment/recruit/recruit-action-organization.service";

const NAME = "recruit-action-organization.controller";

export class RecruitActionOrganizationController extends BaseController {
  private recruitActionOrganizationService: RecruitActionOrganizationService;
  constructor() {
    super(NAME);
    this.recruitActionOrganizationService = new RecruitActionOrganizationService();
  }

  public performRecruitAction = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const recruitAction = await this.recruitActionOrganizationService.createORUpdateRecruitAction(
        user._id,
        body.recruitmentProfileId,
        params.organizationId,
        body.action
      );
      res.json({ recruitAction });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
