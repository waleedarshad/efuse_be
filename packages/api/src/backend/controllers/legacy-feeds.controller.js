const { Failure } = require("@efuse/contracts");
const _ = require("lodash");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { AutomatedFeed } = require("../models/automated-feed");
const { Feed } = require("../models/Feed");
const { HomeFeed } = require("../models/HomeFeed");
const { Media } = require("../models/media");
const { Organization } = require("../models/Organization");
const { ScheduledFeed } = require("../models/scheduled-feed");

const { GeneralHelper } = require("../helpers/general.helper");

const { aggregateFeedPost } = require("../../lib/feeds");
const { FeedService } = require("../../lib/feeds/feed.service");
const ViewsQueue = require("../../async/views");
const { Slack } = require("../../lib/slack");
const ComprehendLib = require("../../lib/feeds/comprehend");
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");
const { FeedRankService } = require("../../lib/lounge/feed-rank.service");
const { MediaService } = require("../../lib/media/media.service");
const { GiphyService } = require("../../lib/giphy/giphy.service");
const { YoutubeVideoService } = require("../../lib/youtube/youtube-video.service");
const { HomeFeedAsyncService } = require("../../lib/home-feed/home-feed-async.service");

const { DOMAIN } = require("@efuse/key-store");

const feedRankService = new FeedRankService();
const mediaService = new MediaService();
const giphyService = new GiphyService();
const feedService = new FeedService();
const youtubeVideoService = new YoutubeVideoService();
const homeFeedAsyncService = new HomeFeedAsyncService();

const show = async (req, res, next) => {
  try {
    // Make sure id is valid objectId
    if (!feedService.isValidObjectId(req.params.id)) {
      throw Failure.BadRequest("Invalid feed ID");
    }

    const id = GeneralHelper.getObjectId(req.params.id);

    const { user } = req;

    const feed = await HomeFeed.findOne({ _id: id }).lean();

    if (!feed) {
      return res.json({});
    }

    const aggregatedFeed = await aggregateFeedPost(feed, user._id);

    ViewsQueue.incrementFeedViews([feed.feed]);

    res.json({ homeFeed: aggregatedFeed });
  } catch (error) {
    next(error);
  }
};

const index = async (req, res) => {
  const filters = JSON.parse(req.query.filters);
  const { timelineableType } = filters;
  const { query } = req.query;
  let homeFeed = [];
  try {
    if (timelineableType === "organizations") {
      const response = await organizationValid({
        _id: req.params.timelineable
      });
      if (response.error) {
        return res.status(response.statusCode).json({ message: response.message });
      }

      // todo: investigate
      // @ts-ignore
      homeFeed = await getOrganizationFeedUserSpaceAggregate(req, res);
    } else if (query === "true" && timelineableType === "users") {
      // todo: investigate
      // @ts-ignore
      homeFeed = await getUserPostFeed(req, res);
    } else {
      // todo: investigate
      // @ts-ignore
      homeFeed = await getHomeFeedUserSpaceAggregate(req, res, {
        user: req.user.id
      });
    }
    return res.json({ homeFeed });
  } catch (error) {
    return res.status(500).json(error);
  }
};

const deleteFeed = async (req, res) => {
  try {
    const { user, params } = req;
    const { feedId } = params;

    // todo: investigate
    // @ts-ignore
    await Feed.delete({
      user: user.id,
      _id: feedId
    });

    await feedRankService.removeObject(feedId);
    await homeFeedAsyncService.removeFeedAsync(feedId);
    return res.end("Succefully Deleted");
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const removeFromTimeline = async (req, res) => {
  try {
    const { user, params } = req;

    // todo: investigate
    // @ts-ignore
    await HomeFeed.delete({ user: user.id, _id: params.homeFeedId });
    return res.end("Feed removed from Timeline");
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const getFeedToEdit = async (req, res) => {
  try {
    const { user, params } = req;
    const { feedId } = params;
    const feed = await Feed.findOne({ user: user.id, _id: feedId });
    if (feed) {
      const media = await Media.findOne({
        mediaable: feedId,
        mediaableType: "feeds"
      });

      const homeFeed = await HomeFeed.findOne({ feed: feedId, user: user._id }, {}, { autopopulate: false }).populate({
        path: "timelineable",
        select: "verified name username name profileImage profilePicture"
      });

      if (!homeFeed) {
        return res.json({ canEdit: false });
      }

      const { timelineable, timelineableType } = homeFeed;
      let name = `${timelineable.name}`;
      let verified = timelineable.verified;
      let username = timelineable.username;
      let image = timelineable?.profilePicture?.url;

      if (timelineableType === "organizations") {
        name = timelineable.name;
        username = timelineable.name;
        verified = timelineable.verified.status;
        image = timelineable?.profileImage?.url;
      }

      const posterInfo = { id: timelineable.id, verified, name, username, type: timelineableType, image };

      return res.json({ canEdit: true, feed, media, posterInfo });
    } else {
      res.json({ canEdit: false });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const updateFeed = async (req, res, next) => {
  try {
    const { user, params, body } = req;
    const { feedId } = params;
    const { removeMedia, content, mentions, file, giphy, youtubeVideo } = body;

    if (!GeneralHelper.isValidObjectId(feedId)) {
      throw Failure.BadRequest("Invalid feedId");
    }

    const feed = await Feed.findOne({ user: user.id, _id: feedId });

    if (!feed) {
      throw Failure.UnprocessableEntity("Feed not found");
    }

    // Remove all the associated media
    if (removeMedia) {
      const mediaQuery = { mediaable: feedId, user: user.id, mediaableType: "feeds" };

      // Fetch media to be deleted
      const mediaObjects = await mediaService.find(mediaQuery);

      // Build S3 fileKeys
      const fileKeys = mediaService.buildFileKeys(mediaObjects);

      // Delete all associated media
      await mediaService.delete(mediaQuery, fileKeys);
    }

    const contentUpdated = feed.content !== content && content;

    feed.content = content;
    feed.mentions = mentions;

    // Update Media
    let media = null;

    if (!_.isEmpty(file) && !_.isEmpty(file.url)) {
      // todo: investigate
      // @ts-ignore
      media = await mediaService.create({ user: user.id, mediaable: feed._id, mediaableType: "feeds", file });
    } else if (!_.isEmpty(giphy)) {
      media = await giphyService.uploadGiphyAsMedia(giphy, user._id, "feeds", feed._id);
    } else {
      const associatedMedia = await mediaService.getAssociatedMedia(user._id, feedId, "feeds");
      media = associatedMedia.media;
    }

    // Update Youtube Video
    if (youtubeVideo) {
      await youtubeVideoService.validateYoutubeOwnership(user._id, youtubeVideo);

      feed.youtubeVideo = youtubeVideo;
    } else {
      feed.youtubeVideo = undefined;
    }

    const updatedFeed = await feed.save();

    if (contentUpdated) {
      await ComprehendLib.comprehendContentAsync(feed._id);
    }

    return res.json({ updatedFeed, media });
  } catch (error) {
    next(error);
  }
};

const reportFeed = async (req, res) => {
  try {
    const { user, body } = req;
    const { description, homeFeedId } = body;
    const home = await HomeFeed.findOne({ _id: homeFeedId });
    if (!home) return res.status(422).json({ message: "Feed Not Found" });
    const isUser = home.timelineableType === "users";
    const feed = await Feed.findOne({ _id: home.feed });
    feed.reports.push({ reportedBy: user._id, description: description });
    await feed.save();

    let creatorLink = "";
    if (isUser) {
      creatorLink = `<${DOMAIN}/u/${feed.user.username}|@${feed.user.username}>`;
    } else {
      const org = await Organization.findOne({ _id: home.timelineable });

      // todo: investigate
      // @ts-ignore
      creatorLink = `<${DOMAIN}/org/${org.shortName}|@${org.name}>`;
    }

    const postLink = isUser
      ? `${DOMAIN}/users/posts?id=${feed.user._id}&feedId=${home._id}`
      : `${DOMAIN}/organizations/timeline?id=${home.timelineable._id}&feedId=${home._id}`;

    await Slack.sendMessage(
      `*Reporting user:*  <${DOMAIN}/u/${user.username}|@${user.username}>\n*Post Creator:*  ${creatorLink}\n*Post link:*  ${postLink}\n*Post Content:*  ${feed.content}\n*Description:*  ${description}`,
      "#community-carl"
    );

    const reports = feed.reports.filter((report) => report.reportedBy);
    const reportedBy = [];

    _.forEach(reports, (reported) => {
      if (typeof reported.reportedBy === "object") {
        reportedBy.push(reported.reportedBy._id.toString());
      } else {
        reportedBy.push(reported.reportedBy.toString());
      }
    });

    if (_.uniq(reportedBy).length === 3) {
      // todo: investigate
      // @ts-ignore
      await HomeFeed.delete({ _id: homeFeedId });
    }

    return res.json({
      success: true,
      flashType: "success",
      message: "You have reported a Post"
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const getScheduledPosts = async (req, res) => {
  const { user } = req;
  try {
    const scheduledPosts = await ScheduledFeed.find({
      user: user._id,
      processed: { $ne: true }
    });
    return res.json(scheduledPosts);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const deleteScheduledPost = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.delete.calls");
  const { user } = req;
  const { id } = req.params;
  try {
    await ScheduledFeed.deleteOne({
      user: user._id,
      _id: id
    });
    const remainingPosts = await ScheduledFeed.find({
      user: user._id,
      processed: { $ne: true }
    });
    EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.delete.success");
    return res.json(remainingPosts);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.delete.error");
    return res.status(500).json({ error });
  }
};

const createAutomatedPost = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("automatedFeeds.create.calls");
  const { user } = req;
  try {
    const { content, crossPosts, file } = req.body;
    let media;

    if (!content) return res.status(400).json({ error: "Content is required" });

    if (file && file.url) {
      media = await Media.create({
        user: req.user._id,
        mediaableType: "feeds",
        file
      });
    }

    const automatedFeed = await AutomatedFeed.create({
      content,
      crossPosts,
      media: media ? media._id : null,
      user: user._id
    });
    EFuseAnalyticsUtil.incrementMetric("automatedFeeds.create.success");
    return res.status(201).json(automatedFeed);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    EFuseAnalyticsUtil.incrementMetric("automatedFeeds.create.error");
    return res.status(500).json({ error });
  }
};

const getAutomatedPosts = async (req, res) => {
  const { user } = req;
  try {
    const automatedFeeds = await AutomatedFeed.find({ user: user._id }).sort("lastSentAt");
    return res.json(automatedFeeds);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const retrieveAutomatedPost = async (req, res) => {
  const { user } = req;
  const { id } = req.params;
  try {
    const automatedFeed = await AutomatedFeed.findById(id);
    if (!automatedFeed) return res.status(404);
    return res.json(automatedFeed);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const updateAutomatedPost = async (req, res) => {
  const { user } = req;
  const { id } = req.params;

  try {
    const { content, crossPosts, file } = req.body;
    let { media } = req.body;

    if (file && file.url) {
      media = await Media.create({
        user: req.user._id,
        mediaableType: "feeds",
        file
      });
    }
    const automatedFeed = await AutomatedFeed.findByIdAndUpdate(
      id,
      {
        $set: {
          content: content,
          crossPosts: crossPosts,
          media: media ? media._id : null
        }
      },
      { new: true }
    );
    return res.json(automatedFeed);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const deleteAutomatedPost = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("automatedFeeds.delete.calls");
  const { user } = req;
  const { id } = req.params;
  try {
    await AutomatedFeed.findByIdAndDelete(id);
    EFuseAnalyticsUtil.incrementMetric("automatedFeeds.delete.success");
    const remainingFeed = await AutomatedFeed.find({ user: user._id }).sort("lastSentAt");
    return res.json(remainingFeed);
  } catch (error) {
    if (user) error.user = user.email || user._id;
    logger.error(error);
    EFuseAnalyticsUtil.incrementMetric("automatedFeeds.delete.error");
    return res.status(500).json({ error });
  }
};

const getOrganizationFeedUserSpaceAggregate = async (req, res) => {
  const { params, query } = req;
  const { page, pageSize } = query;
  const { timelineable } = params;
  const organization = await Organization.findOne({ _id: timelineable }).lean();
  // todo: investigate
  // @ts-ignore
  const { captains } = organization; // TODO: [perms] Use permissions system
  const orgCaptains = captains ? captains : [];
  // todo: investigate
  // @ts-ignore
  const userIds = [organization.user, ...orgCaptains];

  const homeFeed = await HomeFeed.paginate(
    {
      user: { $in: userIds },
      timelineableType: "organizations",
      timelineable: timelineable,
      original: true,
      deleted: { $ne: true }
    },
    {
      page: page,
      limit: pageSize,
      populate: [],
      lean: true,
      sort: { createdAt: -1 }
    }
  );

  const feeds = homeFeed.docs
    .filter((doc) => doc)
    .map((feed) => {
      return aggregateFeedPost(feed, req.user.id);
    });

  const results = await Promise.all(feeds);
  homeFeed.docs = results.filter((doc) => doc);
  extractFeedIdsAndSetViews(homeFeed.docs);
  return homeFeed;
};

const getHomeFeedUserSpaceAggregate = async (req, res, query) => {
  const { page, pageSize } = req.query;
  const homeFeed = await HomeFeed.paginate(
    { user: req.user.id, deleted: { $ne: true } },
    {
      page: page,
      limit: pageSize,
      populate: [],
      lean: true,
      sort: { createdAt: -1 }
    }
  );
  const feeds = homeFeed.docs
    .filter((doc) => doc)
    .map((feed) => {
      return aggregateFeedPost(feed, req.user.id);
    });
  const results = await Promise.all(feeds);
  homeFeed.docs = results.filter((f) => f);
  // Increment Feed views
  extractFeedIdsAndSetViews(homeFeed.docs);
  return homeFeed;
};

const validateUserPostFeed = async (timelineable) => {
  return await Feed.exists({ user: timelineable });
};

const getUserPostFeed = async (req, res, query) => {
  const { page, pageSize } = req.query;
  const timelineable = GeneralHelper.getObjectId(req.params.timelineable);
  // When any visitor visits the porftfolio feed of any user
  const currentProfilerId = req.user._id;
  const homeFeed = await Feed.paginate(
    {
      user: timelineable,
      deleted: { $ne: true },
      "reports.reportedBy": { $ne: req.user._id }
    },
    {
      page: page,
      limit: pageSize,
      populate: [],
      lean: true,
      sort: { createdAt: -1 }
    }
  );
  const feeds = homeFeed.docs.map(async (post) => {
    const feed = await HomeFeed.findOne({
      user: timelineable,
      feed: post._id,
      timelineableType: "users"
    }).lean();
    // passed feed only when feed exists
    return feed ? aggregateFeedPost(feed, currentProfilerId) : {};
  });
  const results = await Promise.all(feeds);
  homeFeed.docs = results.filter((result) => !_.isEmpty(result));
  // Increment Feed views
  extractFeedIdsAndSetViews(homeFeed.docs);
  return homeFeed;
};

const extractFeedIdsAndSetViews = (collection) =>
  ViewsQueue.incrementFeedViews(collection.filter((obj) => obj).map((obj) => obj.feed));

const organizationValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Organization Id" };
  }

  const organizationExist = await Organization.exists(query);
  if (!organizationExist) {
    return { error: true, statusCode: 422, message: "Organization not found" };
  }
  return { error: false };
};

const boostFeed = async (req, res, next) => {
  try {
    const { user, body, params } = req;
    await feedService.toggleFeedBoost(user, params.feedId, body.boosted);
    return res.json({ success: true });
  } catch (error) {
    return next(error);
  }
};

module.exports.boostFeed = boostFeed;
module.exports.createAutomatedPost = createAutomatedPost;
module.exports.deleteAutomatedPost = deleteAutomatedPost;
module.exports.deleteFeed = deleteFeed;
module.exports.deleteScheduledPost = deleteScheduledPost;
module.exports.extractFeedIdsAndSetViews = extractFeedIdsAndSetViews;
module.exports.getAutomatedPosts = getAutomatedPosts;
module.exports.getFeedToEdit = getFeedToEdit;
module.exports.getHomeFeedUserSpaceAggregate = getHomeFeedUserSpaceAggregate;
module.exports.getOrganizationFeedUserSpaceAggregate = getOrganizationFeedUserSpaceAggregate;
module.exports.getScheduledPosts = getScheduledPosts;
module.exports.getUserPostFeed = getUserPostFeed;
module.exports.index = index;
module.exports.organizationValid = organizationValid;
module.exports.removeFromTimeline = removeFromTimeline;
module.exports.reportFeed = reportFeed;
module.exports.retrieveAutomatedPost = retrieveAutomatedPost;
module.exports.show = show;
module.exports.updateAutomatedPost = updateAutomatedPost;
module.exports.updateFeed = updateFeed;
module.exports.validateUserPostFeed = validateUserPostFeed;
