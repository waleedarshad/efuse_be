const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const PubgLib = require("../../lib/game_stats/pubg");
const { getFullUserProfileById } = require("../../lib/users");

const addAccount = async (req, res) => {
  try {
    const { user, body } = req;
    const { platforms } = user.pubg;

    const { _id, ...fields } = body;
    if (_id) {
      const platform = platforms.id(_id);
      platform.username = fields.username;
      platform.platform = fields.platform;
    } else {
      platforms.push(fields);
    }
    const updatedUser = await user.save({ validateBeforeSave: false });
    await PubgLib.syncPubgStatsAsync(user);
    const userObject = await getFullUserProfileById(updatedUser._id);
    return res.json({ updatedUser: userObject });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const removeAccount = async (req, res) => {
  try {
    const { user, params } = req;
    const { pubg } = user;
    const platform = pubg.platforms.id(params.platformId);
    if (pubg.stats) {
      pubg.stats = pubg.stats.filter((stat) => stat.platform !== platform.platform);
    }
    pubg.platforms.pull({ _id: platform._id });
    const updatedUser = await user.save({ validateBeforeSave: false });
    const userObject = await getFullUserProfileById(updatedUser._id);
    return res.json({ updatedUser: userObject });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

module.exports.addAccount = addAccount;
module.exports.removeAccount = removeAccount;
