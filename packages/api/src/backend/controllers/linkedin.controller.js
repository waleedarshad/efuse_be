const { User } = require("../models/User");
const { DOMAIN } = require("@efuse/key-store");

const setToken = (req, res, next) => {
  req.app.set("userId", req.query.token);
  next();
};

const getLinkedinProfile = (accessToken, refreshToken, profile, done) => {
  return done(null, profile);
};

const updateUser = async (req, res) => {
  const endPoint = `${DOMAIN}/settings/external_accounts/linkedin`;
  const { id } = req.user._json;
  try {
    const user = await User.findOne({ _id: req.app.get("userId") });

    // todo: investigate
    // @ts-ignore
    user.linkedinId = id;

    // todo: investigate
    // @ts-ignore
    user.linkedinVerified = true;

    // todo: investigate
    // @ts-ignore
    await user.save({ validateBeforeSave: false });
    return res.redirect(`${endPoint}?token=${id}`);
  } catch {
    return res.redirect(`${endPoint}`);
  }
};

module.exports.getLinkedinProfile = getLinkedinProfile;
module.exports.updateUser = updateUser;
module.exports.setToken = setToken;
