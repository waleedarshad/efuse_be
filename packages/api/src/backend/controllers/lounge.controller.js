const { uniq, forEach } = require("lodash");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Feed } = require("../models/Feed");
const { LoungeFeed } = require("../models/LoungeFeed");
const { Organization } = require("../models/Organization");

const { Slack } = require("../../lib/slack");
const FeedController = require("./legacy-feeds.controller");
const ViewsQueue = require("../../async/views");

const { FeedRankService } = require("../../lib/lounge/feed-rank.service");
const { FeaturedLoungeService } = require("../../lib/lounge/featured-lounge.service");
const { LoungeFeedService } = require("../../lib/lounge/lounge-feed.service");

const { DOMAIN } = require("@efuse/key-store");

const featuredLoungeService = new FeaturedLoungeService();
const feedRankService = new FeedRankService();
const loungeFeedService = new LoungeFeedService();

const discoverFeeds = async (req, res) => {
  try {
    const { type, page, pageSize } = req.query;
    const currentUserId = req.user.id;
    const post = await loungeFeedService.getPaginatedLoungeFeed(type, currentUserId, page, pageSize);
    ViewsQueue.incrementFeedViews(post.docs.map((lounge) => lounge.feed));
    return res.json(post);
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ err });
  }
};

const getLoungefeedById = async (req, res, next) => {
  try {
    const { user, params } = req;
    const loungeFeed = await loungeFeedService.getLoungeById(params.id, user.id);
    ViewsQueue.incrementFeedViews([loungeFeed.feed]);
    return res.json({ loungeFeed });
  } catch (err) {
    return next(err);
  }
};

const hideLoungeFeed = async (req, res) => {
  const { loungeFeedId } = req.params;
  if (req.user.roles.includes("admin")) {
    try {
      const feed = await LoungeFeed.findByIdAndUpdate({ _id: loungeFeedId }, { $set: { published: false } });
      await feedRankService.removeObject(feed.feed);
      return res.send({
        loungeFeedId: feed._id,
        published: false,
        canAccess: true
      });
    } catch (err) {
      logger.error({ err });
      return res.status(500).json({ err });
    }
  } else {
    return res.send({ canAccess: false });
  }
};

const reportLoungePost = async (req, res) => {
  try {
    const { user, body } = req;
    const { description, loungeFeedId } = body;
    const lounge = await LoungeFeed.findOne({ _id: loungeFeedId });
    if (!lounge) return res.status(422).json({ message: "Feed Not Found" });
    const feed = await Feed.findOne({ _id: lounge.feed });
    feed.reports.push({ reportedBy: user._id, description: description });
    await feed.save();

    let creatorLink = "";
    if (lounge.timelineableType === "users") {
      creatorLink = `<${DOMAIN}/u/${feed.user.username}|@${feed.user.username}>`;
    } else {
      const org = await Organization.findOne({ _id: lounge.timelineable });

      // todo: investigate
      // @ts-ignore
      creatorLink = `<${DOMAIN}/org/${org.shortName}|@${org.name}>`;
    }

    await Slack.sendMessage(
      `*Reporting user:*  <${DOMAIN}/u/${user.username}|@${user.username}>\n*Post Creator:*  ${creatorLink}\n*Post link:*  ${DOMAIN}/lounge/featured?feedId=${lounge._id}\n*Post Content:*  ${feed.content}\n*Description:*  ${description}`,
      "#community-carl"
    );

    const reports = feed.reports.filter((report) => report.reportedBy);
    const reportedBy = [];

    forEach(reports, (reported) => {
      if (typeof reported.reportedBy === "object") {
        reportedBy.push(reported.reportedBy._id.toString());
      } else {
        reportedBy.push(reported.reportedBy.toString());
      }
    });

    if (uniq(reportedBy).length === 3) {
      // todo: investigate
      // @ts-ignore
      await LoungeFeed.delete({ _id: loungeFeedId });
      await feedRankService.removeObject(feed._id);
    }

    return res.json({
      success: true,
      flashType: "success",
      message: "You have reported a Post"
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const getFeaturedFeed = async (req, res, next) => {
  try {
    const { query, user } = req;
    const { page, pageSize } = query;
    const featuredFeed = await featuredLoungeService.getPaginatedLoungeFeed(user._id, user.roles, page, pageSize);
    return res.json(featuredFeed);
  } catch (error) {
    return next(error);
  }
};

module.exports.discoverFeeds = discoverFeeds;
module.exports.getFeaturedFeed = getFeaturedFeed;
module.exports.getLoungefeedById = getLoungefeedById;
module.exports.hideLoungeFeed = hideLoungeFeed;
module.exports.reportLoungePost = reportLoungePost;
