const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { LearningArticle } = require("../models/learning-article");
const { LearningArticleCategory } = require("../models/LearningArticleCategory");
const { User } = require("../models/User");
const { Organization } = require("../models/Organization");
const { GeneralHelper } = require("../helpers/general.helper");
const { LearningArticlesValidations } = require("../validations/learning-articles.validations");
const { LearningArticleGameService } = require("../../lib/learningArticles/learning-article-game.service");
const WhitelistHelper = require("../helpers/whitelist_helper");
const { LearningArticleStatusEnum } = require("../../lib/learningArticles/learning-article.enums");

const learningArticleGameService = new LearningArticleGameService();

const createArticle = async (req, res) => {
  try {
    const { image } = req.files;
    const { body } = req;
    const { errors, isValid } = LearningArticlesValidations.validate(body);

    if (!isValid) {
      return res.status(400).json(errors);
    }
    const { author, authorType } = body;
    const authorError = { error: "Author not found" };

    // Validate Author
    if (authorType === "organizations") {
      const organization = await Organization.findOne({ _id: author }).lean();

      // TODO: [perms] Use Permission system
      if (
        // todo: investigate
        // @ts-ignore
        !organization.captains.map((id) => id.toString()).includes(req.user.id) &&
        // todo: investigate
        // @ts-ignore
        organization.user !== req.user.id &&
        !req.user.roles.includes("admin")
      )
        return res.status(404).json("You must own or be a captain of this organization.");

      if (!organization) {
        return res.status(404).json(authorError);
      }
    } else {
      const user = await User.findOne({ _id: author }).lean();
      if (!user) {
        return res.status(404).json(authorError);
      }
    }
    // Validate Category
    const categoryObject = await LearningArticleCategory.findOne({
      _id: body.category
    }).lean();
    if (!categoryObject) {
      errors.error = "Category not found";
      return res.status(404).json(errors);
    }
    // Validate Slug
    body.slug = body.slug.trim().toLowerCase();
    const existingSlug = await LearningArticle.findOne({
      slug: body.slug
    }).lean();
    if (existingSlug) {
      errors.error = "Slug has already been taken";
      return res.status(400).json(errors);
    }
    if (image)
      Object.assign(body, {
        image: GeneralHelper.buildFileObject(image)
      });
    if (body.video)
      Object.assign(body, {
        video: JSON.parse(body.video)
      });

    const article = await new LearningArticle(body).save();

    // create learning article games
    if (body.games) await learningArticleGameService.associateGames(article._id, body.games);

    const url = GeneralHelper.constructLearningArticleUrl(categoryObject, article);
    return res.json({ article, url });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const editArticle = async (req, res) => {
  const { body } = req;
  const article = await LearningArticle.findById(req.params.id);
  let newGames;

  // article does not exist
  if (!article) return res.status(422).json({ error: "Article not found" });

  // This is a safer way to update one or more parts of the learning article.
  // Instead of requiring all fields be sent to update route, you can send specific article properties.
  // Ex: if you want to just update the title, you can send just `title` in the request body
  if (typeof body.title !== "undefined") article.title = body.title;
  if (typeof body.body !== "undefined") article.body = body.body;
  if (typeof body.summary !== "undefined") article.summary = body.summary;
  if (body.traits) article.traits = body.traits;
  if (body.motivations) article.motivations = body.motivations;
  if (body.video) article.video = JSON.parse(body.video);
  if (req.files && req.files.image) article.image = GeneralHelper.buildFileObject(req.files.image);

  // automatically allow any status changes but "published"
  if (body.status && body.status !== LearningArticleStatusEnum.PUBLISHED) article.status = body.status;

  // validate and update published status change
  if (body.status && body.status === LearningArticleStatusEnum.PUBLISHED) {
    // require summary
    if (article.summary.length === 0)
      return res.status(400).json({ error: "You must enter a summary before publishing." });

    // require image
    if (article.image.url === "https://efuse.gg/static/images/no_image.jpg")
      return res.status(400).json({
        error: "You must upload an image to the summary card before publishing."
      });

    article.status = body.status;

    // Setting publishDate only when first time article has been published
    if (!article.publishDate) {
      // todo: investigate
      // @ts-ignore
      article.publishDate = Date.now();
    }
  }

  // update author to organization
  if (body.authorType === "organizations" && body.author) {
    const organization = await Organization.findOne({
      _id: body.author
    }).lean();

    // must be eFuse admin, org owner, or org captain to post as organization
    // TODO: [perms] Use permissions system
    if (
      // todo: investigate
      // @ts-ignore
      !organization.captains.map((id) => id.toString()).includes(req.user.id) &&
      // todo: investigate
      // @ts-ignore
      organization.user !== req.user.id &&
      !req.user.roles.includes("admin")
    )
      return res.status(400).json({ error: "You must own or be a captain of this organization." });

    if (!organization) return res.status(422).json({ error: "Organization not found" });

    article.author = body.author;
    article.authorType = body.authorType;
  }

  // update author to user
  if (body.authorType === "users" && body.author) {
    const user = await User.findOne({ _id: body.author }).lean();

    if (!user) return res.status(404).json({ error: "User not found" });

    article.author = body.author;
    article.authorType = body.authorType;
  }

  // validate and update category
  if (body.category) {
    const categoryObject = await LearningArticleCategory.findOne({
      _id: body.category
    }).lean();

    if (!categoryObject) return res.status(404).json({ error: "Category not found" });

    article.category = body.category;
  }

  // update article games
  if (body.games) {
    await learningArticleGameService.associateGames(article._id, body.games);
  }

  newGames = await learningArticleGameService.getAssociatedGames(article._id);

  // save all changes
  const updatedArticle = await article.save();

  // failed to save
  if (!updatedArticle) return res.status(400).json({ error: "Article failed to update." });

  // todo: currently this is for what ever reason not returning the value of associatedGames on updatedArticle
  // @ts-ignore
  updatedArticle.associatedGames = newGames;

  // return updated article to client
  return res.json({
    message: "Article updated successfully",
    article: updatedArticle
  });
};

const uploadImage = async (req, res) => {
  const { image } = req.files;
  return res.json({
    image: GeneralHelper.buildFileObject(image)
  });
};

const getArticles = async (req, res) => {
  try {
    const { page, pageSize, filters } = req.query;
    let filtersObject = {};
    if (filters) {
      filtersObject = JSON.parse(filters);
    }

    // if searching by categorySlug
    // get category id and remove categorySlug in filtersObject
    if (filtersObject["categorySlug"]) {
      try {
        const category = await LearningArticleCategory.findOne({
          slug: filtersObject["categorySlug"]
        });
        if (category) {
          filtersObject["category"] = category._id;
        } else {
          logger.warn(`This category (${filtersObject["categorySlug"]}) does not exist.`);
          res.status(500).json({
            error: `This category (${filtersObject["categorySlug"]}) does not exist.`
          });
        }
      } catch (error) {
        logger.error(error);
        res.status(500).json(error);
      }

      delete filtersObject["categorySlug"];
    }

    /*
  Filters should be passed from FE, by doing so API will be more
  generic and in future we can search any field directly from FE,
  without making anychange to BE,
  For example to filter categories FE should send filters object as
  const filters = {category: {$in: []}}
  JSON.stringify(filters)
  */
    const articles = await LearningArticle.paginate(
      {
        isActive: true,
        ...filtersObject
      },
      {
        page,
        limit: pageSize,
        lean: true,
        populate: [],
        sort: { publishDate: -1 }
      }
    );

    const articlesWithAssociatedObjects = articles.docs.map(async (article) => {
      // Associate Author Object
      if (article.authorType === "organizations") {
        article.author = await Organization.findOne({
          _id: article.author
        }).lean();
      } else {
        // todo: investigate
        // @ts-ignore
        article.author = await User.findOne({ _id: article.author })
          .lean()
          .select(`${WhitelistHelper.allowedUserFields} bio`);
      }
      // Associate Category
      article.category = await LearningArticleCategory.findOne({
        _id: article.category
      }).lean();
      // Bind Url

      // todo: investigate
      // @ts-ignore
      article.url = GeneralHelper.constructLearningArticleUrl(article.category, article);

      // Associate Games
      // @ts-ignore
      article.associatedGames = await learningArticleGameService.getAssociatedGames(article._id);

      return article;
    });
    const results = await Promise.all(articlesWithAssociatedObjects);
    articles.docs = results;
    return res.json({ articles });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const getOwnedArticles = async (req, res) => {
  try {
    const { page, pageSize, filters } = req.query;
    let filtersObject = {};
    if (filters) {
      filtersObject = JSON.parse(filters);
    }

    var ownedOrganizations = await Organization.find({
      $or: [{ user: req.user._id }, { captains: { $in: req.user._id } }]
    })
      .select("_id")
      .lean();

    //array of ids containing all orgs owned or captain of
    ownedOrganizations = ownedOrganizations.map((org) => org._id);

    // if searching by categorySlug
    // get category id and remove categorySlug in filtersObject
    if (filtersObject["categorySlug"]) {
      try {
        const category = await LearningArticleCategory.findOne({
          slug: filtersObject["categorySlug"]
        });
        if (category) {
          filtersObject["category"] = category._id;
        } else {
          logger.warn(`This category (${filtersObject["categorySlug"]}) does not exist.`);
          res.status(500).json({
            error: `This category (${filtersObject["categorySlug"]}) does not exist.`
          });
        }
      } catch (error) {
        logger.error(error);
        res.status(500).json(error);
      }

      delete filtersObject["categorySlug"];
    }

    /*
  Filters should be passed from FE, by doing so API will be more
  generic and in future we can search any field directly from FE,
  without making anychange to BE,
  For example to filter categories FE should send filters object as
  const filters = {category: {$in: []}}
  JSON.stringify(filters)
  */
    const articles = await LearningArticle.paginate(
      {
        isActive: true,
        $or: [{ author: req.user._id }, { author: { $in: ownedOrganizations } }],
        ...filtersObject
      },
      {
        page,
        limit: pageSize,
        lean: true,
        populate: [],
        sort: { createdAt: -1 }
      }
    );

    const articlesWithAssociatedObjects = articles.docs.map(async (article) => {
      // Associate Author Object
      if (article.authorType === "organizations") {
        article.author = await Organization.findOne({
          _id: article.author
        }).lean();
      } else {
        // todo: investigate
        // @ts-ignore
        article.author = await User.findOne({ _id: article.author })
          .lean()
          .select(`${WhitelistHelper.allowedUserFields} bio`);
      }
      // Associate Category
      article.category = await LearningArticleCategory.findOne({
        _id: article.category
      }).lean();
      // Bind Url

      // todo: investigate
      // @ts-ignore
      article.url = GeneralHelper.constructLearningArticleUrl(article.category, article);

      // Associate Category
      // @ts-ignore
      article.associatedGames = await learningArticleGameService.getAssociatedGames(article._id);

      return article;
    });
    const results = await Promise.all(articlesWithAssociatedObjects);
    articles.docs = results;
    return res.json({ articles });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const getCategories = async (req, res) => {
  try {
    const categories = await LearningArticleCategory.find({ isActive: true });
    return res.json({ categories });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

module.exports.createArticle = createArticle;
module.exports.editArticle = editArticle;
module.exports.getArticles = getArticles;
module.exports.getCategories = getCategories;
module.exports.getOwnedArticles = getOwnedArticles;
module.exports.uploadImage = uploadImage;
