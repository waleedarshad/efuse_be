import { Types } from "mongoose";
import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";
import { OrganizationsService } from "../../lib/organizations/organizations.service";
import { BaseController } from "./base.controller";
import { OrganizationGameService } from "../../lib/organizations/organization-game.service";
import { IGame, IOrganization, IUser } from "../interfaces";
import { OrganizationValidations } from "../validations/organization.validations";
import { Organization } from "../models/Organization";
import { GeneralHelper } from "../helpers/general.helper";

const NAME = "organizations.controller";

export class NewOrganizationsController extends BaseController {
  private service: OrganizationsService;
  private organizationGameService: OrganizationGameService;

  constructor() {
    super(NAME);

    this.service = new OrganizationsService();
    this.organizationGameService = new OrganizationGameService();
  }

  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { profileImage, headerImage } = <any>req.files;
      // @ts-ignore
      const { body, user }: { body: any; user: IUser } = req;
      const { errors, isValid } = OrganizationValidations.validate(body);

      if (!isValid) {
        this.logger.error({ errors });
        res.status(400).json(errors);

        return;
      }

      const count = await Organization.countDocuments({ user: user?._id });
      if (count > 20 && !user?.roles.includes("admin")) {
        res.status(400).json({ message: "You cannot create more than 20 Organizations" });
        return;
      }

      const organizationBody = { ...body };
      delete organizationBody.games;

      const gameIds = body.games.length > 0 ? JSON.parse(body.games) : [];

      const organizationObject: IOrganization = Object.assign(organizationBody, {
        profileImage: GeneralHelper.buildFileObject(profileImage),
        headerImage: GeneralHelper.buildFileObject(headerImage),
        user: user?._id
      });

      const newOrganization = await this.service.createOrganization(user, organizationObject, gameIds);

      res.status(200).json({
        message: "Organization created successfully",
        organization: newOrganization
      });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getOrgToEdit = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { params, user } = req as { params: any; user: any };

    const currentUser = user;
    const organizationId = params.id;

    try {
      const orgToEdit = await this.service.getOrgToEdit(currentUser, organizationId);

      const organization: IOrganization = orgToEdit.organization.toObject() as IOrganization;
      organization.associatedGames = (await this.organizationGameService.getAssociatedGames(
        organization._id
      )) as IGame[];

      res.status(200).json({ canEdit: orgToEdit.canEdit, organization });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public changeOwner = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, params, user } = req as { body: any; params: any; user: any };

    const currentUser = user;
    const organizationId = params.id;
    const requestBody = body as { userId: string };

    try {
      const response = await this.service.changeOwner(currentUser, organizationId, requestBody.userId);

      res.status(200).json(response);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public async getBySlug(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { params, user } = <never>req;
      const { slug } = params;

      this.guard(slug, Failure.BadRequest("Missing parameter 'slug'"));
      this.guard(user, Failure.BadRequest("Missing user information!"));

      const tree = await this.service.findOne({ slug });
      this.guard(tree, Failure.InternalServerError("Failed to find organization tree"));

      res.status(200).json({ count: [tree].length, items: [tree] });
    } catch (error) {
      this.handleError(error, next);
    }
  }

  public kickMember = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { params, query, user } = req as { params: any; query: any; user: any };

    const currentUser = user;
    const organizationId = params.id;
    const userId = query.userId as string;

    try {
      const response = await this.service.kickMember(currentUser, organizationId, userId);

      res.status(200).json(response);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public updateAbout = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };

      const body: { about: string | undefined } | undefined = req.body as { about: string | undefined } | undefined;

      const organization = await this.service.updateAbout(user, params.organizationId, body);

      organization.associatedGames = (await this.organizationGameService.getAssociatedGames(
        organization?._id
      )) as IGame[];

      res.json({ success: true, organization });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public updateGames = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };

      const body: { games: Array<string | Types.ObjectId> | undefined } | undefined = req.body as
        | { games: Array<string | Types.ObjectId> | undefined }
        | undefined;

      if (!body) {
        throw Failure.BadRequest("body is required");
      }

      const organization = await this.service.updateGames(user, params.organizationId, body.games);

      res.json({ organization });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
