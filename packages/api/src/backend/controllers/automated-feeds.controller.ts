import { NextFunction, Request, Response } from "express";

import { Types } from "mongoose";
import { AutomatedFeedService } from "../../lib/automated-feed/automated-feed.service";
import { IAutomatedFeed } from "../interfaces/automated-feed";
import { BaseController } from "./base.controller";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";

import * as TwitchLib from "../../lib/twitch";

const NAME = "automated-feeds.controller";
export class AutomatedFeedsController extends BaseController {
  private automatedFeedService: AutomatedFeedService;

  constructor() {
    super(NAME);

    this.automatedFeedService = new AutomatedFeedService();
  }

  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric("automatedFeeds.create.calls");

      const { body, user } = req as { body: any; user: any };

      // Validate feed, create giphy & media object
      const feedAttachments = await this.automatedFeedService.validateAndCreateAttachments(
        user._id,
        user.username,
        body.content,
        body.twitchClip,
        body.file,
        body.files,
        body.giphy,
        body.youtubeVideo
      );

      const automatedFeedBody: IAutomatedFeed = <IAutomatedFeed>{
        user: user._id,
        content: body.content,
        mentions: body.mentions,
        timelineable: body.timelineable,
        timelineableType: body.timelineableType,
        verified: user.verified || false,
        crossPosts: body.crossPosts,
        media: feedAttachments.media?._id,
        mediaObjects: feedAttachments.mediaObjects?.map((m) => m._id as Types.ObjectId),
        automatedType: body.automatedType,
        platform: body.platform,
        associatedGame: body.associatedGame,
        youtubeVideo: body.youtubeVideo
      };

      const automatedFeed = await this.automatedFeedService.create(automatedFeedBody);

      await TwitchLib.toggleTwitchAutomatedMessageOn(user._id, body.automatedType);

      EFuseAnalyticsUtil.incrementMetric("automatedFeeds.create.success");
      res.json(automatedFeed);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("automatedFeeds.create.failure");
      this.handleError(error, next);
    }
  };
}
