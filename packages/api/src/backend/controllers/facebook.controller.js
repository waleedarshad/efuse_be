const { FACEBOOK_CLIENT_ID, FACEBOOK_CLIENT_SECRET } = require("@efuse/key-store");
const axios = require("axios");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../models/User");

const authenticateFacebook = async (req, res) => {
  logger.info("FacebookAuth backend called");
  const { user } = req;
  try {
    const { facebookId, accessToken } = req.body;
    const url = `https://graph.facebook.com/oauth/access_token?grant_type=fb_exchange_token&client_id=${FACEBOOK_CLIENT_ID}&client_secret=${FACEBOOK_CLIENT_SECRET}&fb_exchange_token=${accessToken}`;

    let msg;
    if (!facebookId) {
      msg = "facebookId is required";
      logger.warn(msg, { user: user.email });
      return res.status(400).json({ error: msg });
    } else if (!accessToken) {
      msg = "token is required";
      logger.warn(msg, { user: user.email });
      return res.status(400).json({ error: msg });
    }

    // todo: investigate
    // @ts-ignore
    const response = await axios.get(url);

    const { access_token, token_type } = response.data;
    user.facebookID = facebookId;
    user.facebookAccessToken = access_token;
    user.facebookTokenType = token_type;
    user.facebookVerified = true;
    await user.save({ validateBeforeSave: false });
    logger.info("Updated user object successfully");
    return res.json({ user });
  } catch (error) {
    logger.error("Failed to request user's Facebook long lived access token", {
      user: user.email,
      error
    });
    return res.status(400).json({ error });
  }
};

const getFacebookPages = async (req, res) => {
  logger.info("getFacebookPages backend called");

  const { user } = req;

  try {
    if (!user.facebookID || !user.facebookAccessToken) {
      const msg = "user has not authenticated Facebook";
      logger.warn(msg, { user: user.email });
      return res.status(401).json({ error: msg });
    } else if (new Date(user.facebookAccessTokenExpiresAt) < new Date()) {
      logger.info("User's facebook auth token has expired", {
        user: user.email
      });
      // TODO: Refresh token
    }

    const url = `https://graph.facebook.com/v6.0/${user.facebookID}/accounts?access_token=${user.facebookAccessToken}`;
    // todo: investigate
    // @ts-ignore
    const response = await axios.get(url);
    const { data } = response.data;
    return res.json(data);
  } catch (error) {
    const msg = error.message ? error.message : error;
    logger.error("Failed to get user's Facebook Pages list", {
      // todo: investigate
      // @ts-ignore
      user: user.email,
      error: msg
    });
    return res.status(400).json({ error: msg });
  }
};

const addFacebookPage = async (req, res) => {
  logger.info("addFacebookPage backend called");
  const { user } = req;
  const { pageName, pageId } = req.body.data;
  try {
    let msg;
    if (!user.facebookID || !user.facebookAccessToken) {
      msg = "user has not authenticated Facebook";
      logger.warn(msg, { user: user.email });
      return res.status(401).json({ error: msg });
    } else if (!pageId) {
      msg = "pageId is required";
      logger.warn(msg, { user: user.email });
      return res.status(400).json({ error: msg });
    } else if (!pageName) {
      msg = "pageName is required";
      logger.warn(msg, { user: user.email });
      return res.status(400).json({ error: msg });
    } else if (new Date(user.facebookAccessTokenExpiresAt) < new Date()) {
      logger.info("User's facebook auth token has expired", {
        user: user.email
      });
      // TODO: Refresh token
    }

    const url = `https://graph.facebook.com/${pageId}?access_token=${user.facebookAccessToken}&fields=access_token`;
    // todo: investigate
    // @ts-ignore
    const response = await axios.get(url);

    const { access_token } = response.data;

    if (!user.facebookPages) {
      user.facebookPages = [];
    }

    user.facebookPages.push({ pageId, pageName, accessToken: access_token });
    await user.save({ validateBeforeSave: false });
    return res.json({ user });
  } catch (error) {
    const msg = error.message ? error.message : error;
    logger.error("Failed to get Facebook page's access token", {
      user: user.email,
      error: msg
    });
    return res.status(400).json({ error: msg });
  }
};

const removeFacebookPage = async (req, res) => {
  logger.info("removeFacebookPage backend called");

  const { user } = req;

  try {
    const { id } = req.params;

    if (!id || !user.facebookPages) {
      return res.status(404);
    }

    user.facebookPages = user.facebookPages.filter((page) => page._id !== id);
    await user.save({ validateBeforeSave: false });
    return res.json({ user });
  } catch (error) {
    const msg = error.message ? error.message : error;
    logger.error("Failed to remove user's Facebook page", {
      // todo: investigate
      // @ts-ignore
      user: user.email,
      error: msg
    });
    return res.status(400).json({ error: msg });
  }
};

module.exports.addFacebookPage = addFacebookPage;
module.exports.authenticateFacebook = authenticateFacebook;
module.exports.getFacebookPages = getFacebookPages;
module.exports.removeFacebookPage = removeFacebookPage;
