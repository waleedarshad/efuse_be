import { NextFunction, Request, Response } from "express";
import { BaseController } from "./base.controller";
import createError from "http-errors";
import { ACTIONS_CONFIG } from "../config/actions-config";

const NAME = "hasura-graphql.controller";

export class HasuraGraphqlController extends BaseController {
  constructor() {
    super(NAME);
  }

  /**
   * @summary Handle graphql webhook
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof HasuraGraphqlWebhook
   */
  public handler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body } = req;
      const actionName: string = body.action.name;

      const func = ACTIONS_CONFIG[actionName];
      if (func) {
        req.graphql = true;
        await func(req, res, next);
      } else {
        throw createError(404, `Action ${actionName} not found!`);
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
