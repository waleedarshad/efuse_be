const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Member } = require("../models/member");
const NotificationsHelper = require("../helpers/notifications.helper");
const { Organization } = require("../models/Organization");
const { OrganizationInvitation } = require("../models/OrganizationInvitation");
const { User } = require("../models/User");
const { TeamMember } = require("../models/team/team-member");

const create = async (req, res) => {
  try {
    const { user, body } = req;
    const { invitees } = body;
    invitees.map(async (userId, index) => {
      try {
        const userResponse = await User.findById(userId);

        const invitationResponse = await OrganizationInvitation.findOne({
          invitee: userId
        });

        if (!invitationResponse) {
          try {
            const invitation = await new OrganizationInvitation({
              inviter: user.id,
              // todo: investigate
              // @ts-ignore
              invitee: userResponse._id,
              organization: body.orgId
            }).save();

            const organization = await Organization.findById(body.orgId);
            await NotificationsHelper.organizationInvitationNotification(user, invitation, organization);
            if (invitees.length === index + 1)
              return res.send({
                isNewUser: true,
                invited: true,
                flashType: "success",
                message: "Invitation successfully sent"
              });
          } catch (error) {
            logger.error(error);
            return res.status(500).json({ error });
          }
        } else {
          if (invitees.length === index + 1)
            return res.send({
              isNewUser: false,
              invited: false,
              flashType: "success",
              message: "Successully sent invitations."
            });
        }
      } catch (error) {
        return res.status(404).send({ error });
      }
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const acceptInvitation = async (req, res) => {
  try {
    const { user, params } = req;
    const response = await Member.find({
      user: user._id,
      organization: params.orgId
    });

    await checkIfUserIsAskedToJoinTeam(user, "accepted");

    if (response.length > 0) {
      return res.json({
        invited: false,
        message: "You have already joined this organization",
        flashType: "success"
      });
    } else {
      try {
        await Member.create({
          user: user._id,
          organization: params.orgId
        });
        try {
          const invitation = await OrganizationInvitation.findOneAndUpdate(
            { invitee: user._id, organization: params.orgId },
            {
              $set: {
                status: "accepted"
              }
            },
            { new: true }
          );

          return res.json({
            invited: true,
            invitation: invitation,
            message: "Invitation accepted.",
            flashType: "success"
          });
        } catch (error) {
          return res.status(500).send({ error });
        }
      } catch (error) {
        logger.error(error);
        return res.status(500).send({ error });
      }
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const rejectInvitation = async (req, res) => {
  try {
    const { user, params } = req;
    await checkIfUserIsAskedToJoinTeam(user, "rejected");

    const invitation = await OrganizationInvitation.findOneAndUpdate(
      { invitee: user._id, organization: params.orgId },
      {
        $set: {
          status: "rejected"
        }
      },
      { new: true }
    );

    return res.json({
      invited: false,
      invitation: invitation,
      message: "Invitation rejected.",
      flashType: "danger"
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).send({ error });
  }
};

const checkIfUserIsAskedToJoinTeam = async (user, inviteStatus) => {
  try {
    // todo: investigate
    // @ts-ignore
    const foundTeamMember = await TeamMember.findOne({ user: user._id, status: "pending" }, { _id: 1 }, { lean: true });
    if (foundTeamMember && inviteStatus === "accepted") {
      // todo: investigate
      // @ts-ignore
      await TeamMember.updateOne({ _id: foundTeamMember._id }, { status: "active" });
    } else if (foundTeamMember && inviteStatus === "rejected") {
      // todo: investigate
      // @ts-ignore
      await TeamMember.delete(foundTeamMember._id);
    }
  } catch (error) {
    logger.error(error);

    throw error;
  }
};

module.exports.acceptInvitation = acceptInvitation;
module.exports.checkIfUserIsAskedToJoinTeam = checkIfUserIsAskedToJoinTeam;
module.exports.create = create;
module.exports.rejectInvitation = rejectInvitation;
