import { NextFunction, Request, Response } from "express";
import { BaseController } from "./base.controller";
import { BRAZE_WEBHOOK_SECRET } from "@efuse/key-store";

import { Notification } from "../models/Notification";

import { NotifyableType, IBrazeWebhookBody } from "../interfaces";

import { io } from "../config/get-io-object";

const NAME = "braze-webhook.controller";

export class BrazeWebhookController extends BaseController {
  constructor() {
    super(NAME);
  }

  /**
   * @summary Handle Braze webhook
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof BrazeWebhook
   */
  public handler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      // Validate incoming webhook request
      if (req.headers.authorization !== BRAZE_WEBHOOK_SECRET) {
        res.status(400).send({ message: "Authorization Header is invalid" });
        this.logger.error(
          { headers: req.headers },
          `Error handling braze webhook... Authorization header not found or invalid.`
        );
        return;
      }

      const body = req.body as IBrazeWebhookBody;
      const { content, sender, receiver, notifyable, notifyableType, timeline } = body;

      // Check notifyableType
      if (!Object.values(NotifyableType).includes(body.notifyableType)) {
        res.status(400).send({ message: "Invalid notifyableType" });
        return;
      }

      const notification: IBrazeWebhookBody = {
        content,
        sender,
        receiver,
        notifyable,
        notifyableType
      };

      if (timeline) {
        notification["timeline"] = timeline;
      }

      this.logger.info({ notification }, "Preparing to add a notification from braze.");

      if (!notifyable) {
        res.status(400).send("Bad Request");
        return;
      }

      await new Notification(notification).save();
      await io.to(receiver).emit("BUMP_NOTIFICATION_COUNT", {});

      this.logger.info({ notification }, "Successfully added a notification from braze.");

      res.status(200).send("OK");
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
