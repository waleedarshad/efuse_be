import { Failure } from "@efuse/contracts";
import { CLOUDFLARE_WEBHOOK_TOKEN } from "@efuse/key-store";
import crypto from "crypto";
import { NextFunction, Request, Response } from "express";
import { MediaProfileEnum } from "@efuse/entities";
import { ITranscodeMedia } from "../interfaces";
import { IMedia } from "../interfaces/media";
import { BaseController } from "./base.controller";
import { MediaService } from "../../lib/media/media.service";
import { TranscodeMediaService } from "../../lib/media/transcode-media.service";
import { MediaConvertJobStatus } from "../../lib/media/transcode-media.enum";

const NAME = "cloudflare-stream-webhook.controller";

export class CloudflareWebhookController extends BaseController {
  private $mediaService: MediaService;
  private $transcodeMediaService: TranscodeMediaService;

  constructor() {
    super(NAME);

    this.$mediaService = new MediaService();
    this.$transcodeMediaService = new TranscodeMediaService();
  }

  /**
   * @summary Handle cloudflare stream webhook
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  public handler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const isRequestValid: boolean = this.validateRequest(req);

      if (!isRequestValid) {
        this.logger?.error("Cloudflare webhook request is invalid.");
        return;
      }

      const result = req.body; // https://developers.cloudflare.com/stream/uploading-videos/using-webhooks#example-post-request-body-sent-in-response-to-successful-encoding

      if (result.readyToStream) {
        this.logger?.info({ result });
        const { uid, playback, meta, size } = result;

        const transcodeMedia: ITranscodeMedia | null = await this.$transcodeMediaService.findOne({ jobId: uid });

        // Make sure transcodeMedia object exists
        if (!transcodeMedia) {
          throw Failure.UnprocessableEntity(
            `Received a Cloudflare webhook notification with uid: ${uid} which doesn't exist in TranscodeMedia collection`
          );
        }

        if (transcodeMedia.status === MediaConvertJobStatus.complete) {
          // This means the media objects were already created
          // Nothing to do here
          return;
        }

        await this.storeHlsMedia(transcodeMedia, playback.hls, meta.name, size);
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  private storeHlsMedia = async (
    transcodeMedia: ITranscodeMedia,
    hls: string,
    filename: string,
    fileSize: number
  ): Promise<void> => {
    // Fetch original media
    const originalMedia: IMedia | null = await this.$mediaService.findOne({ _id: transcodeMedia.originalMedia });

    // Make sure media object exists
    if (!originalMedia) {
      throw Failure.UnprocessableEntity(
        `Corresponding media object not found against ID: ${String(transcodeMedia.originalMedia)}`
      );
    }

    // Build media object
    const mediaObject: IMedia = <IMedia>{
      file: {
        filename,
        contentType: originalMedia.file.contentType,
        url: hls
      },
      fileOriginalUrl: originalMedia.fileOriginalUrl,
      profile: MediaProfileEnum.HLS,
      mediaable: originalMedia.mediaable,
      mediaableType: originalMedia.mediaableType,
      user: originalMedia.user,
      size: fileSize
    };

    // Create new Media Object
    const createdMediaObject: IMedia = await this.$mediaService.create(mediaObject);

    // Mark transcodeMedia status as COMPLETE
    await this.$transcodeMediaService.update(
      { _id: transcodeMedia._id },
      { processed: true, status: MediaConvertJobStatus.complete, hlsMedia: createdMediaObject._id }
    );
  };

  /**
   * @summary Validate if the incoming request is from cloudflare or not. Docs: https://developers.cloudflare.com/stream/uploading-videos/using-webhooks#verify-webhook-authenticity
   *
   * @param {Request} req
   * @returns {Boolean}
   */
  private validateRequest = (req: Request): boolean => {
    const webhookSignature = req.get("webhook-signature");

    if (!webhookSignature) {
      this.logger?.error("No webhook signature in the request.");
      return false;
    }

    // Webhook signature looks like "time=1617624151,sig1=0b05c05b4bb6730e1309a3d6263067565e914fff8603aa0ecc496672ba2c4299"
    // Getting time and sig from that string
    const [time, sig] = webhookSignature.split(",").map((header) => {
      return header.split("=")[1];
    });

    const timeInt = Number.parseInt(time, 10);

    if (!timeInt || !sig) {
      this.logger?.error("Webhook signature doesn't consist of time and signature");
      return false;
    }

    const now = new Date().valueOf();
    const webhookTime = new Date(timeInt * 1000).valueOf();

    const hours = Math.abs(now - webhookTime) / 36e5;

    if (hours > 1) {
      // Invalidate the incoming request if it's been more than 1 hour since the video upload process started
      this.logger?.error("Webhook timestamp is too old (> 1 hour).");
      return false;
    }

    const message = `${time}.${JSON.stringify(req.body)}`;

    // Docs: https://developers.cloudflare.com/stream/uploading-videos/using-webhooks#step-3-create-the-expected-signature
    const hash = crypto.createHmac("sha256", CLOUDFLARE_WEBHOOK_TOKEN).update(message).digest("hex");

    return crypto.timingSafeEqual(Buffer.from(hash), Buffer.from(sig));
  };
}
