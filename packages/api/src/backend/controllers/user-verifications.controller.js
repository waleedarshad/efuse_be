const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { VerificationRequest } = require("../models/VerificationRequest");

const verificationRequest = async (req, res, next) => {
  try {
    const { user } = req;
    const newRequest = await new VerificationRequest({
      user: user.id
    }).save();

    user.verifyAccountRequest = true;
    user.verifyAccountRequestDate = Date.now();

    await user.save({ validateBeforeSave: false });

    return res.json({ newRequest });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

module.exports.verificationRequest = verificationRequest;
