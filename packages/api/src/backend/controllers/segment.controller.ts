import { Request, Response, NextFunction } from "express";

import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { BaseController } from "./base.controller";

enum SegmentControllerEnums {
  Track = "track",
  SEGMENT_EVENT_TRACK = "segment.event.TRACK",
  SEGMENT = "segment",
  WEBHOOK = "webhook"
}

export class SegmentController extends BaseController {
  constructor() {
    super("segment.controller");
  }

  public SegmentHandler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body } = req;

      if (body.type === SegmentControllerEnums.Track) {
        EFuseAnalyticsUtil.incrementMetric(SegmentControllerEnums.SEGMENT_EVENT_TRACK, 1, [body.event]);
      }
      this.logger?.info(
        { ...body, service: SegmentControllerEnums.SEGMENT, source: SegmentControllerEnums.WEBHOOK },
        "/api/__webhooks__/segment"
      );

      res.end("Ending segment webhook request");
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
