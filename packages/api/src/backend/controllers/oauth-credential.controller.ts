import { Failure } from "@efuse/contracts";
import { OAuthServiceKind } from "@efuse/entities";
import { Request, Response, NextFunction } from "express";

import { BaseController } from "./base.controller";
import { OAuthCredentialService } from "../../lib/oauth-credential.service";
import { RiotProfileService } from "../../lib/riot/riot-profile/riot-profile.service";
import { UserHelperService } from "../../lib/users/user-helper.service";

const NAME = "oauth-credential.controller";

export class OAuthCredentialController extends BaseController {
  protected declare $service: OAuthCredentialService;

  private riotProfileService: RiotProfileService;

  private userHelperService: UserHelperService;

  constructor() {
    super(NAME);

    this.$service = new OAuthCredentialService();
    this.riotProfileService = new RiotProfileService();
    this.userHelperService = new UserHelperService();
  }

  /**
   * Attempts to determine if `owner` has authed with external `service`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @return {*}  {Promise<void>}
   * @memberof OAuthCredentialController
   */
  public getOwnerConnectionStatus = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = <never>req;
      const { owner, service } = params;

      this.guard(owner, Failure.BadRequest("Missing parameter 'owner'"));
      this.guard(service, Failure.BadRequest("Missing parameter 'service'"));

      let authService: OAuthServiceKind = service;

      if (service === OAuthServiceKind.VALORANT) {
        authService = OAuthServiceKind.RIOT;
      }

      let result = await this.$service.getConnectionStatus(owner, authService);

      if (result && service === OAuthServiceKind.VALORANT) {
        // do special check because Riot can auth Valorant OR League of Legends
        const riotProfile = await this.riotProfileService.findOne({ owner });

        if (!riotProfile?.valorantProfile?.puuid) {
          result = false;
        }
      }

      res.status(200).json({ count: 1, items: [result] });
    } catch (error: any) {
      this.handleError(error, next);
    }
  };

  public unlinkExternalAccount = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = <never>req;
      const { owner, service } = params;

      this.guard(owner, Failure.BadRequest("Missing parameter 'owner'"));
      this.guard(service, Failure.BadRequest("Missing parameter 'service'"));

      await this.userHelperService.unLinkExternalAccounts(owner, service);

      res.status(200).json({ count: 1, items: [true] });
    } catch (error: any) {
      this.handleError(error, next);
    }
  };
}
