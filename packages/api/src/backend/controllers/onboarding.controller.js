const _ = require("lodash");
const bcrypt = require("bcryptjs");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../models/User");
const { GeneralHelper } = require("../helpers/general.helper");

const create = async (req, res) => {
  const { email } = req.params;
  const { body } = req;
  const { key } = body;
  const params = body[key];
  const { errors, isValid } = require(`../validations/${key}`)(params);
  const { password } = params;
  if (!isValid) {
    return res.status(400).json(errors);
  }

  if (password !== undefined) {
    bcrypt.hash(password, 10, async (err, hash) => {
      if (err) throw err;
      params["password"] = hash;
      await updateUser(res, email, params);
    });
  } else {
    await updateUser(res, email, params);
  }
};

const saveAttachments = async (req, res) => {
  const { email } = req.params;
  const { profilePicture, headerImage } = req.files;
  const { body } = req;

  const params = {
    bio: body.bio
  };

  if (body.profilePicture === undefined) {
    Object.assign(params, {
      profilePicture: GeneralHelper.buildFileObject(profilePicture)
    });
  }

  if (body.headerImage === undefined) {
    Object.assign(params, {
      headerImage: GeneralHelper.buildFileObject(headerImage)
    });
  }

  await updateUser(res, email, params);
};

const professional = async (req, res) => {
  const { email } = req.params;
  const { resume } = req.files;
  const { body } = req;
  const { title, organization, interest, phoneNumber, role } = body;

  const params = {
    title,
    organization,
    interest,
    phoneNumber,
    role
  };

  if (body.resume === undefined) {
    Object.assign(params, { resume: GeneralHelper.buildFileObject(resume) });
  }

  await updateUser(res, email, params);
};

const updateUser = async (res, email, params) => {
  try {
    const user = await User.findOne({ email: email });
    if (params.role) {
      // todo: investigate
      // @ts-ignore
      _.uniq(user.roles.push(params.role));
      params = _.omit(params, ["role"]);
    }
    if (params.organization) {
      // todo: investigate
      // @ts-ignore
      _.uniq(user.organizations.push(params.organization));
      params = _.omit(params, ["organization"]);
    }
    const keys = Object.keys(params);

    // todo: investigate
    // @ts-ignore
    keys.forEach((key) => (user[key] = params[key]));

    try {
      // todo: investigate
      // @ts-ignore
      const newUser = await user.save({ validateBeforeSave: false });

      return res.json({ user: newUser, email: newUser.email });
    } catch (error) {
      logger.error(error);
      return res.status(500).json({ message: error.message });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

module.exports.create = create;
module.exports.professional = professional;
module.exports.saveAttachments = saveAttachments;
module.exports.updateUser = updateUser;
