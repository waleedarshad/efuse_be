import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";

import { YoutubeService } from "../../lib/youtube/youtube.service";
import { BaseController } from "./base.controller";

const NAME = "youtube-account-info.controller";

export class YoutubeAccountInfoController extends BaseController {
  private service: YoutubeService;

  constructor() {
    super(NAME);

    this.service = new YoutubeService();
  }

  /**
   * Attempts to retrieve the youtube account info for owner
   *
   * @param owner The ID of the owner
   *
   * @memberof YoutubeAccountInfoController
   */
  public getByOwner = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const entity = await this.service.findByOwner(id);

      if (!entity) {
        res.status(200).json({ count: 0, items: [] });
      } else {
        res.status(200).json({
          count: 1,
          items: [entity]
        });
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
