import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "../base.controller";
import { ERenaLeaderboardService } from "../../../lib/erena/erena-leaderboard.service";

const NAME = "erena-leaderboard.controller";

export class ERenaLeaderboardController extends BaseController {
  private service: ERenaLeaderboardService;

  constructor() {
    super(NAME);

    this.service = new ERenaLeaderboardService();
  }

  /**
   * Attempts to retrieve team leaderboard information information based on passed tournamentIdOrSlug
   *
   * @param tournamentIdOrSlug The ID or slug of the tournament to pull data for
   * @param query.namespace (Optional) The namespace to be pull data for the Twitch plugin instead of the tournamentIdOrSlug
   */
  public getTeamLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { namespace } = req.query;

      if (!tournamentIdOrSlug && !namespace) {
        throw Failure.BadRequest("tournamentIdOrSlug or namespace required.");
      }

      const results = await this.service.getTeamLeaderboard(tournamentIdOrSlug, <string>namespace);

      if (!results) {
        throw Failure.NotFound("Unable to find tournament or namespace");
      }

      res.status(200).json(results);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve player leaderboard information based on passed tournamentIdOrSlug
   *
   * @param tournamentIdOrSlug The ID or slug of the tournament to pull data for
   * @param query.namespace (Optional) The namespace to be pull data for the Twitch plugin instead of the tournamentIdOrSlug
   */
  public getPlayerLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { namespace } = req.query;

      if (!tournamentIdOrSlug && !namespace) {
        throw Failure.BadRequest("tournamentIdOrSlug or namespace required.");
      }

      const results = await this.service.getPlayerLeaderboard(tournamentIdOrSlug, <string>namespace, false);

      if (!results) {
        throw Failure.NotFound("Unable to find tournament or namespace");
      }

      res.status(200).json(results);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
