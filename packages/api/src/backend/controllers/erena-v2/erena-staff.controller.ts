import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "../base.controller";
import { BrazeCampaignName } from "../../../lib/braze/braze.enum";
import { BrazeService } from "../../../lib/braze/braze.service";
import { ERenaRoles } from "../../interfaces/erena/erena-enums";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaStaffService } from "../../../lib/erena/erena-staff.service";
import { IERenaStaff } from "../../interfaces/erena/erena-staff";
import { IUser } from "../../interfaces/user";
import { sendTournamentStaffNotification } from "../../helpers/notifications.helper";
import { UserService } from "../../../lib/users/user.service";

const NAME = "erena-Staff.controller";

export class ERenaStaffController extends BaseController {
  private service: ERenaStaffService;
  private erenaSharedService: ERenaSharedService;
  private userService: UserService;
  private brazeService: BrazeService;

  constructor() {
    super(NAME);

    this.service = new ERenaStaffService();
    this.erenaSharedService = new ERenaSharedService();
    this.userService = new UserService();
    this.brazeService = new BrazeService();
  }

  /**
   * Attempts to delete the eRena Staff with matching `id` also updates the team's score
   *
   * @param id The ID of the Staff to delete
   *
   * @memberof ERenaStaffController
   */
  public deleteStaff = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { id } = req.params;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const staff = await this.service.findById(id);

      if (!staff) {
        throw Failure.UnprocessableEntity("Could not find Staff to delete");
      }

      // verify permissions
      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(staff.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const response = await this.service.delete(id);

      if (response) {
        res.status(200).json(staff);
      } else {
        throw Failure.InternalServerError("Failed to delete Staff");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena Staff with matching `id`
   *
   * @param id The ID of the Staff to retrieve
   *
   * @memberof ERenaStaffController
   */
  public getStaff = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { id } = req.params;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const entity = await this.service.findById(id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find Staff");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena Staffs with matching tournament id or slug
   *
   * @param id The ID of the tournament to retrieve Staffs for
   * @query team (Optional) Team ID to filter Staffs
   * @query includeScores (Optional) Indicates that the Staff's scores for the tournament should be summed and included
   *
   * @memberof ERenaStaffController
   */
  public getStaffForTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { userId } = req.query;

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      if (userId && !this.service.isValidObjectId(<string>userId)) {
        throw Failure.BadRequest("Invalid user id");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      const queryValue = {
        tournament: tournament?._id
      };

      if (userId) {
        queryValue["user"] = userId;
      }

      const staffs = await this.service.find(queryValue, {}, {});

      res.json(staffs);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to create a new eRena Staff
   *
   * @param tournamentIdOrSlug The ID of the tournament to post Staffs
   * @param body the staff data
   *
   * @memberof ERenaStaffController
   */
  public postStaff = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { body, user } = <{ body: IERenaStaff; user: Optional<IUser> }>req;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      if (!body.user) {
        throw Failure.BadRequest("Missing user.");
      }

      const newStaff = await this.userService.findOne({ _id: body.user });
      if (!newStaff) {
        throw Failure.BadRequest("Unable to find user.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      body.tournament = tournament._id;

      const staffBody: Partial<IERenaStaff> = {
        user: body.user,

        tournament: tournament._id,
        role: body.role
      };

      const staff = await this.service.create(staffBody);

      if (!staff) {
        throw Failure.InternalServerError("Failed to create staff");
      } else if (staffBody.role !== ERenaRoles.owner) {
        await sendTournamentStaffNotification(user, newStaff, tournament);
        // Send notification via Braze
        this.brazeService.sendCampaignMessageAsync(BrazeCampaignName.STAFF_ADDED_TO_ERENA_EVENT, {
          trigger_properties: {
            tournament_name: tournament.tournamentName,
            admin_username: user.username,
            erenaSlug: tournament.slug
          },
          recipients: [{ external_user_id: String(newStaff._id) }]
        });
      }

      res.status(200).json(staff);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to update the eRena staff with matching `id`
   *
   * @param id The ID of the Staff to update
   * @param body the staff data to update (only updates role)
   *
   * @memberof ERenaStaffController
   */
  public patchStaff = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = <{ body: IERenaStaff; user: Optional<IUser> }>req;
      const { id } = req.params;

      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.service.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      // only update name, isActive
      const staffUpdatePartial: Partial<IERenaStaff> = <Partial<IERenaStaff>>{
        role: body.role
      };

      // verify permissions
      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const updated = await this.service.update(id, staffUpdatePartial);

      if (updated) {
        res.status(200).json(updated);
      } else {
        throw Failure.NotFound("Failed to update Staff");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
