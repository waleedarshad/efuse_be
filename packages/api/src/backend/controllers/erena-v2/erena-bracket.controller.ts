import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "../base.controller";
import { ERenaBracketService } from "../../../lib/erena/erena-bracket.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTeamService } from "../../../lib/erena/erena-team.service";
import { IERenaMatch, IERenaRound, IERenaTeam, IERenaTeamPopulated } from "../../interfaces/erena";
import { ErenaHelper } from "../../helpers/erena-v2.helper";

const NAME = "erena-bracket.controller";

export class ERenaBracketController extends BaseController {
  private service: ERenaBracketService;
  private erenaSharedService: ERenaSharedService;
  private erenaTeamService: ERenaTeamService;

  public constructor() {
    super(NAME);

    this.service = new ERenaBracketService();
    this.erenaSharedService = new ERenaSharedService();
    this.erenaTeamService = new ERenaTeamService();
  }

  /**
   * Attempts to retrieve the eRena bracket with matching `id`
   *
   * @param id The ID of the bracket to retrieve
   *
   * @memberof ERenaBracketController
   */
  public getBracket = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const entity = await this.service.findById(id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find score");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Retrieve bracket for a specified tournament
   *
   * @param tournamentIdOrSlug The id or slug of the tournament to get the bracket
   * @param query.includeRounds (Optional) If "true" populate round info
   *
   * @memberof ERenaBracketController
   */
  public getBracketForTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { includeRounds, includeMatches, includeTeams, includePlayers, includeScores } = req.query;

      const popRounds = includeRounds === "true";
      const popMatches = popRounds && includeMatches === "true";
      const popTeams = popMatches && includeTeams === "true";
      const popPlayers = popTeams && includePlayers === "true";
      const popScores = popTeams && includeScores === "true";

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament");
      }

      const foundBracket = await this.service.findByTournament(tournament._id);

      if (!foundBracket) {
        if (!tournament) {
          throw Failure.BadRequest("Unable to find bracket");
        }
      }

      // strip the extra mongoose properties and functionality so we're dealing with a plain json object
      const bracket = foundBracket?.toObject();

      // by default the findByTournament populates the rounds, matches, and teams as part of the query
      // we just need to remove them if the request doesn't require them
      if (bracket?.rounds && !popRounds) {
        bracket.rounds = <[]>(<unknown>undefined);
      } else if (bracket?.rounds && !popMatches) {
        // set each match to an undefined object
        bracket.rounds.forEach((round: any) => {
          // todo: investigate, this can't be right...

          round.round.matches = <[]>(<unknown>undefined);
        });
      } else if (bracket?.rounds && popMatches && !popTeams) {
        // iterate over the rounds and matches to set teams to undefined objects
        (<{ round: IERenaRound }[]>bracket.rounds).forEach((round) => {
          (<{ match: IERenaMatch }[]>round?.round?.matches).forEach((match) => {
            if (match?.match?.teams) {
              match.match.teams = <[]>(<unknown>undefined);
            }
          });
        });
      }

      if (bracket?.rounds && popTeams && popScores) {
        // populate scores, optionally populate players by fetching teams with scores and replacing the originals
        const teams = await this.erenaTeamService.findTeamsByTournament(tournamentIdOrSlug);
        const populatedTeams = await this.erenaTeamService.populateTeamsWithScores(teams, popPlayers);

        (<{ round: IERenaRound }[]>bracket.rounds).forEach((round) => {
          (<{ match: IERenaMatch }[]>round?.round?.matches).forEach((match) => {
            (<{ team: IERenaTeam }[]>match.match.teams).forEach((team) => {
              team.team = <IERenaTeam>populatedTeams.find((populatedTeam) => populatedTeam._id.equals(team.team?._id));
            });
          });
        });
      } else if (bracket?.rounds && popTeams && popPlayers) {
        // populate players, but don't populate scores by fetching teams with players and replacing the originals
        const teams = await this.erenaTeamService.findTeamsByTournament(tournamentIdOrSlug);
        const populatedTeams = await this.erenaTeamService.populateTeamsWithPlayers(teams);

        (<{ round: IERenaRound }[]>bracket.rounds).forEach((round) => {
          (<{ match: IERenaMatch }[]>round?.round?.matches).forEach((match) => {
            (<{ team: IERenaTeam }[]>match.match.teams).forEach((team) => {
              team.team = <IERenaTeamPopulated>(
                populatedTeams.find((populatedTeam) => populatedTeam._id.equals(team.team?._id))
              );
            });
          });
        });
      }

      if (bracket) {
        const formattedBracket = await ErenaHelper.formatBracketResponse(bracket);
        res.status(200).json(formattedBracket);
      } else {
        throw Failure.NotFound("Failed to find bracket for tournament");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
