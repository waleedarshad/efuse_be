import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";

import { Types } from "mongoose";
import { BaseController } from "../base.controller";
import { IUser } from "../../interfaces/user";
import { ERenaMatchService } from "../../../lib/erena/erena-match.service";
import { IERenaMatch, IERenaMatchCreateParams } from "../../interfaces/erena";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";

const NAME = "erena-match.controller";

export class ERenaMatchController extends BaseController {
  private service: ERenaMatchService;
  private erenaSharedService: ERenaSharedService;

  public constructor() {
    super(NAME);

    this.service = new ERenaMatchService();
    this.erenaSharedService = new ERenaSharedService();
  }

  /**
   * Attempts to retrieve the eRena match with matching `id`
   *
   * @param id The ID of the match to retrieve
   *
   * @memberof ERenaMatchController
   */
  public getMatch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const entity = await this.service.findById(id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find score");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve matches for a tournament
   * @param tournamentIdOrSlug the ID or slug of the related tournament
   * @query round the id of the round to pull matches for
   */
  public getMatchesByTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { tournamentIdOrSlug } = req.params;
    const { round } = req.query;

    try {
      const results = await this.service.findByTournament(tournamentIdOrSlug, <string>round);

      res.json(results);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to create a new eRena Match
   *
   * @param tournamentIdOrSlug the ID or slug of the related tournament
   * @param body the match data
   *
   * @memberof ERenaMatchController
   */
  public postMatch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = <{ body: IERenaMatchCreateParams; user: Optional<IUser> }>req;
      const { tournamentIdOrSlug } = req.params;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const match = await this.service.createMatch(user._id, tournamentIdOrSlug, body);

      if (!match) {
        throw Failure.InternalServerError("Failed to create match");
      }

      res.json(match);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to update an existing eRena Match
   *
   * @param matchId the ID or slug of the match to be updated
   * @param body the match data
   *
   * @memberof ERenaMatchController
   */
  public patchMatch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = <{ body: IERenaMatch; user: Optional<IUser> }>req;
      const { id } = req.params;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const existingMatch = await this.service.findById(id);
      if (!existingMatch) {
        throw Failure.UnprocessableEntity("Unable to find match");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(
        (<Types.ObjectId>existingMatch.tournament).toHexString()
      );
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const match = await this.service.updateMatch(id, body);

      res.json(match);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to set screenshots on the match
   *
   * @param matchId the ID or slug of the match to be updated
   * @param body the match data
   *
   * @memberof ERenaMatchController
   */
  public saveScreenShot = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user, body } = <{ body: any; user: Optional<IUser> }>req;
      const { id } = req.params;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const existingMatch = await this.service.findById(id);
      if (!existingMatch) {
        throw Failure.UnprocessableEntity("Unable to find match");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(
        (<Types.ObjectId>existingMatch.tournament).toHexString()
      );
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const match = await this.service.uploadScreenshot(user._id, id, body.teamOneImage, body.teamTwoImage);

      res.json(match);
    } catch (error) {
      return next(error);
    }
  };
}
