import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import { Types } from "mongoose";

import { BaseController } from "../base.controller";
import { GeneralHelper } from "../../helpers/general.helper";
import { ERenaRoles, ERenaTournamentStatus } from "../../interfaces/erena/erena-enums";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTournamentService } from "../../../lib/erena/erena-tournament.service";
import { File } from "../../interfaces/file";
import { IERenaTournament, IERenaTournamentFileUploader, IERenaTournamentPopulated } from "../../interfaces/erena";
import { IUser } from "../../interfaces/user";
import { ERenaGameService } from "../../../lib/erena/erena-game.service";

const NAME = "erena-tournament.controller";

export class ERenaTournamentController extends BaseController {
  private service: ERenaTournamentService;
  private erenaSharedService: ERenaSharedService;
  private erenaGameService: ERenaGameService;

  constructor() {
    super(NAME);

    this.service = new ERenaTournamentService();
    this.erenaSharedService = new ERenaSharedService();
    this.erenaGameService = new ERenaGameService();
  }

  /**
   * Attempt to retrieve tournament information based on passed tournamentIdOrSlug.
   *
   * @param tournamentIdOrSlug The ID or slug of the tournament to pull data for
   * @param query.includeStats (Optional) Indicates whether the tournament should be populated with stats/scores
   * @param query.namespace (Optional) The namespace to be pull data for the Twitch plugin instead of the tournamentIdOrSlug
   *
   * @memberof ERenaTournamentController
   */
  public getTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { namespace } = req.query;

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing tournament id or slug");
      }

      const result = await this.service.getByIdOrSlug(tournamentIdOrSlug, namespace as string);

      res.json(result);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated Use GraphQL
   * Get paginated list of tournaments
   *
   * @memberof ERenaTournamentController
   */
  public listTournaments = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { page, limit, status, sortBy, sortDirection, startDateMin, startDateMax, endDateMin, endDateMax } =
        req.query;

      const results = await this.service.getAll(
        Number(page),
        Number(limit),
        <ERenaTournamentStatus[]>status,
        String(sortBy),
        Number(sortDirection),
        Number(startDateMin),
        Number(startDateMax),
        Number(endDateMin),
        Number(endDateMax)
      );

      res.json(results);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempt to create a tournament
   *
   * @param {IERenaTournament} body The tournament to create.
   *
   * @memberof ERenaTournamentController
   */
  public postTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const tournamentParams = <IERenaTournament>req.body;
    const { user } = <{ user: Optional<IUser> }>req;

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const { errors, isValid } = await this.service.validateTournament(tournamentParams, user);

      if (!isValid) {
        throw Failure.BadRequest(errors.join(" "));
      }

      const tournament = await this.service.create(tournamentParams, user._id);

      if ("game" in tournamentParams) {
        await this.erenaGameService.associateGame(tournament._id, tournamentParams.game as string);
      }

      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Updates an existing tournament with the passed data.
   *
   * @param tournamentIdOrSlug The id or slug of the tournament to update
   * @param body The tournament data to update
   * @param files Uploaded tournament files
   *
   * @memberof ERenaTournamentController
   */
  public patchTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { files, user } = <{ files: any[]; user: Optional<IUser> }>req;
      const { tournamentIdOrSlug } = req.params;
      const uploadedFiles: IERenaTournamentFileUploader = <IERenaTournamentFileUploader>files;
      const tournament: IERenaTournament = <IERenaTournament>req.body;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = <IERenaTournament>await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, (<Types.ObjectId>original._id).toHexString())) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, original))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const updatedTournament = await this.service.update(tournamentIdOrSlug, tournament, uploadedFiles);

      if ("game" in tournament) {
        await this.erenaGameService.associateGame(updatedTournament._id, tournament.game as string);
      }

      res.json(updatedTournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Set a tournament status to "Deleted"
   * Note: this does not permanently delete a tournament.
   *
   * @param tournamentIdOrSlug The id or slug of the tournament to be marked "Deleted"
   *
   * @memberof ERenaTournamentController
   */
  public deleteTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { tournamentIdOrSlug } = req.params;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(
          user._id,
          (<Types.ObjectId>original._id).toHexString(),
          ERenaRoles.owner
        )) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, original))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const patchValue = { status: ERenaTournamentStatus.deleted };
      const updatedTournament = await this.service.update((<Types.ObjectId>original._id).toHexString(), patchValue);

      res.json(updatedTournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Set a tournament ad
   *
   * @files image to use for the ad
   * @body Objecting containing the link and index for the ad
   * @param tournamentIdOrSlug The id or slug of the tournament to add the ad
   *
   * @memberof ERenaTournamentController
   */
  public updateTournamentAd = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { files, user } = <{ files: any[]; user: Optional<IUser> }>req;

      const { link } = req.body;
      const { tournamentIdOrSlug, index } = req.params;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, (<Types.ObjectId>original._id).toHexString())) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, original))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const uploadedFiles: IERenaTournamentFileUploader = <IERenaTournamentFileUploader>files;

      let imageURL: string | undefined;

      if (uploadedFiles && uploadedFiles.image) {
        const image: File = GeneralHelper.buildFileObject(uploadedFiles.image);
        imageURL = image.url;
      }

      const tournament: IERenaTournamentPopulated = await this.service.addOrRemoveAd(
        original,
        user?._id,
        Number(index),
        true,
        imageURL,
        link
      );
      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Remove a tournament ad
   *
   * @files image to use for the ad
   * @param tournamentIdOrSlug The id or slug of the tournament to add the ad
   * @param index The index ad to remove
   *
   * @memberof ERenaTournamentController
   */
  public removeTournamentAd = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { tournamentIdOrSlug, index } = req.params;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!original) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, (<Types.ObjectId>original._id).toHexString())) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, original))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const tournament: IERenaTournamentPopulated = await this.service.addOrRemoveAd(
        original,
        user?._id,
        Number(index),
        false
      );

      res.json(tournament);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
