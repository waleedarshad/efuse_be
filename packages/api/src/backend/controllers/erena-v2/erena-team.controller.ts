import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import { Types, FilterQuery } from "mongoose";

import { BaseController } from "../base.controller";
import { IERenaTeam } from "../../interfaces/erena/erena-team";
import { IUser } from "../../interfaces/user";
import { ERenaTeamService } from "../../../lib/erena/erena-team.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaMatchService } from "../../../lib/erena/erena-match.service";

const NAME = "erena-team.controller";

export class ERenaTeamController extends BaseController {
  private service: ERenaTeamService;
  private erenaSharedService: ERenaSharedService;
  private erenaMatchService: ERenaMatchService;

  constructor() {
    super(NAME);

    this.service = new ERenaTeamService();
    this.erenaSharedService = new ERenaSharedService();
    this.erenaMatchService = new ERenaMatchService();
  }

  /**
   * Attempts to delete the eRena Team with matching `id`
   *
   * @param id The ID of the team to delete
   *
   * @memberof ERenaTeamController
   */
  public deleteTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const team = await this.service.findById(id);

      if (!team) {
        throw Failure.NotFound("Could not find team to delete");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(team.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const entity = await this.service.deleteTeamAndPlayers(user._id, id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to delete Team");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena team with matching `id`
   *
   * @param id The ID of the team to retrieve
   * @query includePlayers (Optional) Indicates that players should be populated on the teams
   * @query includeScores (Optional) Indicates that the team score should be included (and player scores)
   *
   * @memberof ERenaTeamController
   */
  public getTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;
    const { includePlayers, includeScores } = req.query;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      let entity = await this.service.findById(id, true);

      if (includeScores === "true") {
        const entities = await this.service.populateTeamsWithScores([entity], includePlayers === "true");

        if (entities?.length > 0) {
          entity = entities[0];
        }
      } else if (includePlayers === "true") {
        const entities = await this.service.populateTeamsWithPlayers([entity]);

        if (entities?.length > 0) {
          entity = entities[0];
        }
      }

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find team");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena teams for the tournament with a matching 'tournamentIdOrSlug'
   *
   * @param id The ID of the team to retrieve
   * @query includePlayers (Optional) Indicates that players should be populated on the teams
   * @query includeScores (Optional) Indicates that the team score should be included (and player scores)
   *
   * @memberof ERenaTeamController
   */
  public getTeamsForTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { tournamentIdOrSlug } = req.params;
      const { includePlayers, includeScores, namespace, roundId, matchId } = req.query;

      let teams: IERenaTeam[] = [];

      if (matchId && !this.service.isValidObjectId(<string>matchId)) {
        throw Failure.BadRequest("Invalid matchId");
      }

      if (roundId && !this.service.isValidObjectId(<string>roundId)) {
        throw Failure.BadRequest("Invalid roundId");
      }

      if (matchId) {
        // retrieve match, then retrieve teams that are listed
        const match = await this.erenaMatchService.findById(<string>matchId);

        if (match) {
          // get the team ids
          const ids = match.teams.map((team) => <Types.ObjectId>team.team);
          const filter: FilterQuery<IERenaTeam> = { _id: ids };

          teams = await this.service.find(filter, ERenaTeamService.ProjectValues, { lean: true });
        }
      } else if (roundId) {
        // retrieve round matches, then retrieve teams for those matches
        const matches = await this.erenaMatchService.findByTournament(tournamentIdOrSlug, <string>roundId);
        if (matches?.length > 0) {
          // get the team ids from the matches, this will return and array of arrays or teamIds
          const teamIds = matches.map((match) => {
            return match.teams.map((team) => <Types.ObjectId>team.team);
          });

          // this looks wonky, but it takes an array of arrays and flattens it to one array with all the items
          const ids = <string[]>Array.prototype.concat.apply([], teamIds);
          const filter = { _id: ids };

          teams = await this.service.find(filter, ERenaTeamService.ProjectValues, { lean: true });
        }
      } else {
        teams = await this.service.findTeamsByTournament(tournamentIdOrSlug, namespace as string);
      }

      if (includeScores === "true") {
        teams = await this.service.populateTeamsWithScores(teams, includePlayers === "true");
      } else if (includePlayers === "true") {
        teams = await this.service.populateTeamsWithPlayers(teams);
      }

      res.json(teams);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to create a new eRena team
   *
   * @param tournamentIdOrSlug The ID of the tournament to post teams
   * @param body the team data
   *
   * @memberof ERenaTeamController
   */
  public postTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaTeam; user: Optional<IUser> }>req;
    const { tournamentIdOrSlug } = req.params;

    try {
      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing tournament id");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      body.tournament = tournament._id;

      const team = await this.service.create(body);

      if (!team) {
        throw Failure.InternalServerError("Failed to create team");
      }

      res.status(200).json(team);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to update the eRena team with matching `id`
   *
   * @param id The ID of the team to update
   * @param body the team data to update
   *
   * @memberof ERenaTeamController
   */
  public patchTeam = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaTeam; user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.service.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      // Only update the name and type
      const updateData = {
        name: body.name,
        type: body.type
      };

      const updated = await this.service.update(id, updateData);

      if (updated) {
        res.status(200).json(updated);
      } else {
        throw Failure.NotFound("Failed to update team");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
