import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import { FilterQuery } from "mongoose";

import { BaseController } from "../base.controller";
import { IERenaScore } from "../../interfaces/erena/erena-score";
import { IUser } from "../../interfaces/user";
import { ERenaScoreService } from "../../../lib/erena/erena-score.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaScoringKinds } from "../../interfaces/erena/erena-enums";
import { ERenaPlayerService } from "../../../lib/erena/erena-player.service";

const NAME = "erena-score.controller";

export class ERenaScoreController extends BaseController {
  private service: ERenaScoreService;
  private erenaSharedService: ERenaSharedService;
  private erenaPlayerService: ERenaPlayerService;

  constructor() {
    super(NAME);

    this.service = new ERenaScoreService();
    this.erenaSharedService = new ERenaSharedService();
    this.erenaPlayerService = new ERenaPlayerService();
  }

  /**
   * Attempts to delete the eRena Score with matching `id`
   *
   * @param id The ID of the score to delete
   *
   * @memberof ERenaScoreController
   */
  public deleteScore = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const score = await this.service.findById(id);

      if (!score) {
        throw Failure.NotFound("Could not find score to delete");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(score.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const entity = await this.service.delete(id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to delete Score");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena score with matching `id`
   *
   * @param id The ID of the score to retrieve
   *
   * @memberof ERenaScoreController
   */
  public getScore = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const entity = await this.service.findById(id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find score");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena scores for the matching 'tournamentId'
   *
   * @param tournamentId The ID of the tournament to retrieve scores for
   * @query match (optional) Limits results to match
   * @query team (optional) Limits results to scores for a team (including players)
   * @query owner (optional) Limits results to scores for a specific owner (player or team)
   *
   * @memberof ERenaScoreController
   */
  public getScoresForTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { tournamentIdOrSlug } = req.params;
    const { match, team, owner } = req.query;

    try {
      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentId'");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament");
      }

      const filter: FilterQuery<IERenaScore> = {
        tournament: tournament?._id
      };

      if (match) {
        if (!this.service.isValidObjectId(<string>match)) {
          throw Failure.BadRequest("Invalid match id");
        }

        filter.match = <string>match;
      }

      if (team) {
        if (!this.service.isValidObjectId(<string>team)) {
          throw Failure.BadRequest("Invalid team id");
        }

        const filterValue = [<string>team];
        const players = await this.erenaPlayerService.getByTeam(<string>team);

        if (players?.length > 0) {
          filterValue.push(...players.map((player) => <string>player._id));
        }

        filter.ownerId = { $in: filterValue };
      }

      if (owner) {
        if (!this.service.isValidObjectId(<string>owner)) {
          throw Failure.BadRequest("Invalid owner id");
        }

        filter.ownerId = <string>owner;
      }

      const entities = await this.service.find(filter, {}, { lean: true });

      if (entities) {
        res.status(200).json(entities);
      } else {
        throw Failure.NotFound("Failed to find scores");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to create a new eRena score
   *
   * @param tournamentIdOrSlug The ID of the tournament to post scores
   * @param body the score data
   *
   * @memberof ERenaScoreController
   */
  public postScore = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaScore; user: Optional<IUser> }>req;
    const { tournamentIdOrSlug } = req.params;

    try {
      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing tournament id");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      if (tournament.bracketType === ERenaScoringKinds.bracket && !body.match) {
        throw Failure.BadRequest("Missing match id on score object.");
      }

      body.tournament = tournament._id;

      body.erenaKind =
        tournament.bracketType === ERenaScoringKinds.bracket ? ERenaScoringKinds.bracket : ERenaScoringKinds.pointRace;

      const validationErrors = this.service.validateIncomingFields(body);
      if (validationErrors.length > 0) {
        throw Failure.BadRequest(validationErrors.join(" | "));
      }

      const entity = await this.service.create(body);

      if (!entity) {
        throw Failure.InternalServerError("Failed to create score");
      }

      res.status(200).json(entity);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to update the eRena score with matching `id`
   * Note: only updates the score value
   *
   * @param id The ID of the score to update
   * @param body the score data to update
   *
   * @memberof ERenaScoreController
   */
  public patchScore = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaScore; user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (body.score === undefined) {
        throw Failure.BadRequest("Missing 'score'");
      }

      body.score = Number(body.score);

      const original = await this.service.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      // only update the score
      original.score = body.score;

      const updated = await this.service.update(id, original);

      if (updated) {
        res.status(200).json(updated);
      } else {
        throw Failure.NotFound("Failed to update score");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
