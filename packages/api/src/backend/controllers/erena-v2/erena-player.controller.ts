import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import { FilterQuery } from "mongoose";

import { BaseController } from "../base.controller";
import { IERenaPlayer } from "../../interfaces/erena/erena-player";
import { IUser } from "../../interfaces/user";
import { ERenaPlayerService } from "../../../lib/erena/erena-player.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";
import { ERenaTeamService } from "../../../lib/erena/erena-team.service";

const NAME = "erena-player.controller";

export class ERenaPlayerController extends BaseController {
  private service: ERenaPlayerService;
  private erenaSharedService: ERenaSharedService;
  private erenaTeamService: ERenaTeamService;

  constructor() {
    super(NAME);

    this.service = new ERenaPlayerService();
    this.erenaSharedService = new ERenaSharedService();
    this.erenaTeamService = new ERenaTeamService();
  }

  /**
   * Attempts to delete the eRena Player with matching `id` also updates the team's score
   *
   * @param id The ID of the player to delete
   *
   * @memberof ERenaPlayerController
   */
  public deletePlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const player = await this.service.findById(id);

      if (!player) {
        throw Failure.UnprocessableEntity("Could not find player to delete");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(player.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const response = await this.service.delete(id);

      if (response) {
        res.status(200).json(player);
      } else {
        throw Failure.InternalServerError("Failed to delete Player");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena player with matching `id`
   *
   * @param id The ID of the player to retrieve
   * @query includeScores (Optional) Indicates that the player's scores for the tournament should be summed and included
   *
   * @memberof ERenaPlayerController
   */
  public getPlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;
    const { includeScores } = req.query;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      let entity = await this.service.findById(id);

      if (includeScores === "true") {
        const populatedPlayers = await this.service.populatePlayerScores([entity]);

        if (populatedPlayers?.length > 0) {
          entity = populatedPlayers[0];
        }
      }

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find player");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the eRena players with matching tournament id or slug
   *
   * @param id The ID of the tournament to retrieve players for
   * @query team (Optional) Team ID to filter players
   * @query includeScores (Optional) Indicates that the player's scores for the tournament should be summed and included
   *
   * @memberof ERenaPlayerController
   */
  public getPlayersForTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { tournamentIdOrSlug } = req.params;
    const { team, includeScores } = req.query;

    try {
      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      const filter: FilterQuery<IERenaPlayer> = {
        tournament: tournament?._id
      };

      if (team) {
        if (!this.service.isValidObjectId(<string>team)) {
          throw Failure.BadRequest("Invalid team id");
        }

        filter.team = <string>team;
      }

      let players = await this.service.find(filter, {}, { lean: true });

      if (includeScores === "true") {
        const populatedPlayers = await this.service.populatePlayerScores(players);

        if (populatedPlayers?.length > 0) {
          players = populatedPlayers;
        }
      }

      res.json(players);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to create a new eRena player
   *
   * @param tournamentIdOrSlug The ID of the tournament to post players
   * @param body the player data
   *
   * @memberof ERenaPlayerController
   */
  public postPlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaPlayer; user: Optional<IUser> }>req;
    const { teamId } = req.params;

    try {
      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!teamId) {
        throw Failure.BadRequest("Missing team id");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const team = await this.erenaTeamService.findById(teamId);

      if (!team) {
        throw Failure.BadRequest("Could not find team");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(team.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      body.team = team._id;

      body.tournament = tournament._id;

      const playerPartial: Partial<IERenaPlayer> = <Partial<IERenaPlayer>>{
        team: body.team,
        type: body.type,
        user: body.user,
        name: body.name,
        isActive: body.isActive,
        tournament: body.tournament
      };

      const player = await this.service.create(playerPartial);

      if (!player) {
        throw Failure.InternalServerError("Failed to create player");
      }

      res.status(200).json(player);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to update the eRena player with matching `id`
   *
   * @param id The ID of the player to update
   * @param body the player data to update (only updates name, isActive)
   *
   * @memberof ERenaPlayerController
   */
  public patchPlayer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaPlayer; user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.service.findById(id);

      if (!original) {
        throw Failure.NotFound("Could not find item to update");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString());
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id)) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      // only update name, isActive
      const playerUpdatePartial: Partial<IERenaPlayer> = <Partial<IERenaPlayer>>{
        name: body.name,
        isActive: body.isActive
      };

      const updated = await this.service.update(id, playerUpdatePartial);

      if (updated) {
        res.status(200).json(updated);
      } else {
        throw Failure.NotFound("Failed to update player");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
