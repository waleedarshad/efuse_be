import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "../base.controller";
import { IUser } from "../../interfaces/user";
import { ERenaRoundService } from "../../../lib/erena/erena-round.service";
import { IERenaRoundCreateParams, IERenaTournament } from "../../interfaces/erena";
import { ERenaBracketService } from "../../../lib/erena/erena-bracket.service";
import { ERenaSharedService } from "../../../lib/erena/erena-shared.service";

const NAME = "erena-round.controller";

export class ERenaRoundController extends BaseController {
  private service: ERenaRoundService;
  private erenaBracketService: ERenaBracketService;
  private erenaSharedService: ERenaSharedService;

  public constructor() {
    super(NAME);

    this.service = new ERenaRoundService();
    this.erenaBracketService = new ERenaBracketService();
    this.erenaSharedService = new ERenaSharedService();
  }

  /**
   * Attempts to retrieve the eRena round with matching `id`
   *
   * @param id The ID of the round to retrieve
   *
   * @memberof ERenaRoundController
   */
  public getRound = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      const entity = await this.service.findById(id);

      if (entity) {
        res.status(200).json(entity);
      } else {
        throw Failure.NotFound("Failed to find score");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Retrieve rounds for a specified tournament
   *
   * @param tournamentIdOrSlug The id or slug of the tournament to get rounds for
   *
   * @memberof ERenaRoundController
   */
  public getRoundsForTournament = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { tournamentIdOrSlug } = req.params;

    try {
      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);

      const rounds = await this.service.find({ tournament: tournament?._id }, {}, { lean: true });

      if (rounds) {
        res.status(200).json(rounds);
      } else {
        throw Failure.NotFound("Failed to find rounds for tournament");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to create a new eRena round
   *
   * @param tournamentIdOrSlug the ID or slug of the related tournament
   * @param body the round data
   *
   * @memberof ERenaRoundController
   */
  public postRound = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = <{ body: IERenaRoundCreateParams; user: Optional<IUser> }>req;
      const { tournamentIdOrSlug } = req.params;

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      if (!tournamentIdOrSlug) {
        throw Failure.BadRequest("Missing parameter 'tournamentIdOrSlug'");
      }

      const tournament = <IERenaTournament>await this.erenaSharedService.getTournamentByIdOrSlug(tournamentIdOrSlug);
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id.toHexString())) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const round = await this.erenaBracketService.createRound(tournament._id, body);

      if (!round) {
        throw Failure.InternalServerError("Failed to create round");
      }

      res.json(round);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to update an existing eRena round
   *
   * @param id the ID or slug of the round to be updated
   * @param body the round data
   *
   * @memberof ERenaRoundController
   */
  public patchRound = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, user } = <{ body: IERenaRoundCreateParams; user: Optional<IUser> }>req;
    const { id } = req.params;

    try {
      if (!id) {
        throw Failure.BadRequest("Missing parameter 'id'");
      }

      if (!body) {
        throw Failure.BadRequest("Missing request body");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const original = await this.service.findById(id);

      const tournament = <IERenaTournament>(
        await this.erenaSharedService.getTournamentByIdOrSlug(original.tournament.toHexString())
      );
      if (!tournament) {
        throw Failure.BadRequest("Unable to find tournament.");
      }

      // verify permissions
      if (
        !(await this.erenaSharedService.isUserStaff(user._id, tournament._id.toHexString())) &&
        !(await this.erenaSharedService.verifyTournamentOwnership(user._id, tournament))
      ) {
        throw Failure.Forbidden("You are not authorized to perform this action.");
      }

      const round = await this.service.updateRound(user._id, id, body);

      if (round) {
        res.status(200).json(round);
      } else {
        throw Failure.NotFound("Failed to update round");
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
