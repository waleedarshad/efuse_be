import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import { Types } from "mongoose";

import { AutomatedFeedsController } from "./automated-feeds.controller";
import { BaseController } from "./base.controller";
import { FeedService } from "../../lib/feeds/feed.service";
import { HomeFeedService } from "../../lib/home-feed/home-feed.service";
import { IUser } from "../interfaces/user";
import { ScheduledFeedsController } from "./scheduled-feeds.controller";
import { GeneralHelper } from "../helpers/general.helper";

const NAME = "feeds.controller";

export class FeedsController extends BaseController {
  private feedsService: FeedService;
  private homeFeedService: HomeFeedService;
  private scheduledFeedsController: ScheduledFeedsController;
  private automatedFeedsController: AutomatedFeedsController;

  constructor() {
    super(NAME);

    this.feedsService = new FeedService();
    this.scheduledFeedsController = new ScheduledFeedsController();
    this.automatedFeedsController = new AutomatedFeedsController();
    this.homeFeedService = new HomeFeedService();
  }

  /**
   * @deprecated In favor create method
   */
  public legacyCreate = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body } = req;

      if (body.isScheduled) {
        // Create scheduled post
        await this.scheduledFeedsController.create(req, res, next);
      } else if (body.isAutomated) {
        // Create automated post
        await this.automatedFeedsController.create(req, res, next);
      } else {
        // Create normal post
        await this.create(req, res, next);
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = <{ body: any; params: { [key: string]: any }; user: Optional<IUser> }>req;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      // Validate feed, create giphy & media object

      const feedAttachments = await this.feedsService.validateAndCreateAttachments(
        user._id,
        user.username,
        body.content,
        body.twitchClip,
        body.file,
        body.files,
        body.giphy,
        body.youtubeVideo
      );

      const homeFeed = await this.feedsService.create(
        user._id,
        body.timelineable,
        body.timelineableType,
        user.verified || false,
        body.platform,
        body.mentions,
        body.content,
        feedAttachments.media?._id,
        feedAttachments.mediaObjects?.map((m) => m._id as Types.ObjectId),
        body.twitchClip,
        undefined,
        body.shared,
        body.sharedFeed,
        body.sharedTimeline,
        body.sharedTimelineable,
        body.sharedTimelineableType,
        body.sharedPostUser,
        body.crossPosts,
        body.associatedGame,
        body.youtubeVideo
      );

      res.json({ homeFeed });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Get singular home feed post
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @returns Singular Formatted Feed Post
   *
   * @memberof FeedController
   */
  public getHomeFeedPost = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { id } = req.params;

      if (!id || !this.feedsService.isValidObjectId(id)) {
        throw Failure.BadRequest("Invalid id");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const loungeFeedPost = await this.homeFeedService.getHomeFeedPost(id, user._id.toHexString());

      res.json(loungeFeedPost);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Get user feed by id
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @returns Formatted feed
   *
   * @memberof FeedController
   */
  public getUserFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { page, limit } = req.query;
    const { userId } = req.params;
    const userObjectId = GeneralHelper.getObjectId(req.params.userId);
    try {
      if (!userId) {
        throw Failure.BadRequest("Missing parameters 'userId'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const homeFeed = await this.feedsService.getPaginatedUserFeed(
        user._id,
        userObjectId,
        Number(page),
        Number(limit)
      );
      res.json(homeFeed);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Get organization feed by Id
   *
   * @param {Request} req
   * @param {Request} res
   * @param {NextFunction} next
   *
   * @returns Formatted feed
   *
   * @memberof FeedsController
   */
  public getOrganizationFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { page, limit } = req.query;
    const { orgId } = req.params;
    const orgObjectId = GeneralHelper.getObjectId(orgId);

    try {
      if (!orgId) {
        throw Failure.BadRequest("Missing parameter 'orgId'");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const organizationFeed = await this.feedsService.getPaginatedOrganizationFeed(
        user._id,
        // todo: investigate & fix
        // @ts-ignore
        orgObjectId,
        Number(page),
        Number(limit)
      );
      res.json(organizationFeed);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Get current user's following feed
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @returns Formatted feed
   *
   * @memberof FeedController
   */
  public getFollowingFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { page, limit } = req.query;

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const homeFeed = await this.feedsService.getFollowingFeed(user._id, Number(page), Number(limit));

      res.json(homeFeed);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Update feed object by Id
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @returns Formatted feed
   *
   * @memberof FeedsController
   */
  public updateFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = <{ body: any; user: Optional<IUser> }>req;
      const { feedId } = req.params;

      if (!feedId) {
        throw Failure.BadRequest("feedId is required");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const feed = await this.feedsService.updateFeed(user._id, feedId, body);

      res.json(feed);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
