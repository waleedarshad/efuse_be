import { Failure } from "@efuse/contracts";
import { Request, Response, NextFunction } from "express";

import { BaseController } from "./base.controller";
import { TwitterAccountInfoService } from "../../lib/twitter-account-info.service";

const NAME = "twitter-account-info.controller";

export class TwitterAccountInfoController extends BaseController {
  protected declare $service: TwitterAccountInfoService;

  constructor() {
    super(NAME);

    this.$service = new TwitterAccountInfoService();
  }

  /**
   * Attempts to retrieve the resource at `id`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @return {*}  {Promise<void>}
   */
  public async get(req: Request, res: Response, next: NextFunction): Promise<void> {
    try {
      const { params, user } = <never>req;
      const { id } = params;

      this.guard(id, Failure.BadRequest("Missing parameter 'id'"));
      this.guard(user, Failure.BadRequest("Missing user information!"));

      const entity = await this.$service.findOne(id, { cache: false });
      this.guard(entity, Failure.UnprocessableEntity("Failed to get resource"));

      res.status(200).json({ count: [entity].length, items: [entity] });
    } catch (error: any) {
      this.handleError(error, next);
    }
  }

  /**
   * Attempts to retrieve the resource with owner `id`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @return {*}  {Promise<void>}
   * @memberof TwitterAccountInfoController
   */
  public getByOwner = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = <never>req;
      const { id } = params;

      this.guard(id, Failure.BadRequest("Missing parameter 'id'"));

      const entity = await this.$service.findOne({ owner: id });

      if (!entity) {
        res.status(200).json({ count: 0, items: [] });
      } else {
        res.status(200).json({ count: [entity].length, items: [entity] });
      }
    } catch (error: any) {
      this.handleError(error, next);
    }
  };
}
