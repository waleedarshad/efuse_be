const _ = require("lodash");
const { InviteCodeType } = require("@efuse/entities");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Organization } = require("../models/Organization");
const { Member } = require("../models/member");
const { OrganizationRequest } = require("../models/organization-request");
const { OrganizationFollower } = require("../models/organization-follower");
const { User } = require("../models/User");
const { InviteCode } = require("../models/invite-code");

const { OrganizationValidations } = require("../validations/organization.validations");
const { GeneralHelper } = require("../helpers/general.helper");
const SchemaHelper = require("../helpers/schema_helper");
const { SENDGRID_API_KEY } = require("@efuse/key-store");
const NotificationsHelper = require("../helpers/notifications.helper");
const { OrganizationACLService } = require("../../lib/organizations/organization-acl.service");
const { OrganizationsService } = require("../../lib/organizations/organizations.service");
const { OrganizationGameService } = require("../../lib/organizations/organization-game.service");
const { InviteCodeService } = require("../../lib/invite-code/invite-code.service");

const organizationACLService = new OrganizationACLService();
const organizationsService = new OrganizationsService();
const organizationGameService = new OrganizationGameService();
const inviteCodeService = new InviteCodeService();

const organizationValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Organization Id" };
  }

  const organizationExist = await Organization.exists(query);
  if (!organizationExist) {
    return { error: true, statusCode: 422, message: "Organization not found" };
  }
  return { error: false };
};

// TODO: [perms] Update to use permission system
const getMemberRole = (memberId, owner, captains) => {
  if (String(memberId) === String(owner)) {
    return "Owner";
  }
  if (captains?.find((id) => String(id) === String(memberId))) {
    return "Captain";
  }
  return "Member";
};

// TODO: [perms] Update to use permission system
const OrgMembersAggregate = async (member, res) => {
  try {
    member.user = await User.findOne(member.user).lean().select("name username verified profilePicture roles");
    member.organization = await Organization.findOne(member.organization).lean();

    // Set member role
    member.role = await getMemberRole(member.user?._id, member.organization.user, member.organization.captains);

    return member;
  } catch (error) {
    logger.error(error);
    res.status(500).json({ error: error.message });
  }
};

// TODO: [perms] Update to use permission system
const memberAggregate = async (organization, user, res, isOrgShow = false) => {
  try {
    if (!_.isEmpty(user)) {
      const member = await Member.findOne({
        organization: organization._id,
        user: user._id,
        deleted: { $ne: true }
      })
        .cache(5)
        .lean();

      if (member) {
        organization.member = member;
      }

      const organizationFollower = await OrganizationFollower.findOne({
        followee: organization._id,
        follower: user._id,
        deleted: { $ne: true }
      })
        .cache(5)
        .lean();

      if (organizationFollower) {
        organization.organizationFollower = organizationFollower;
      }

      const orgRequest = await OrganizationRequest.findOne({
        organization: organization._id,
        user: user._id,
        deleted: { $ne: true }
      })
        .cache(60)
        .lean();

      if (orgRequest) organization.orgRequest = orgRequest;

      /**
       * Making sure invite codes are only added for
       * eFuse admin
       * organization captains
       * organization owner
       */
      // TODO: [perms] Use permissions system
      if (
        user.roles?.includes("admin") ||
        organization.captains?.includes(user._id) ||
        String(organization.user) === String(user._id)
      ) {
        const inviteCodes = await InviteCode.find(
          {
            organization: organization._id,
            active: true
          },
          {},
          { lean: true }
        ).cache(3600);

        for (const inviteCode of inviteCodes) {
          const referralCodes = await User.find({ referral: inviteCode.code }, { referral: 1 })
            .cache(3600) // These should virtually never change.  Cache for an hour.
            .lean();
          inviteCode.signupCount = referralCodes.length;
        }

        organization.inviteCodes = inviteCodes;
      }
    }

    const associatedUser = await User.findOne({ _id: organization.user })
      .cache(60)
      .lean()
      .select("name username verified profilePicture roles");

    organization.associatedUser = associatedUser;

    const membersCount = await Member.countDocuments({
      organization: organization._id,
      deleted: { $ne: true }
    }).cache(60);

    organization.membersCount = membersCount;

    if (isOrgShow) {
      const followersCount = await OrganizationFollower.countDocuments({
        followee: organization._id
      }).cache(60);

      organization.followersCount = followersCount;
    }

    organization.associatedGames = await organizationGameService.getAssociatedGames(organization._id);

    return organization;
  } catch (error) {
    logger.error({ error });
    res.status(500).json({ error: error.message });
  }
};

const getOrganizations = async (req, res, query) => {
  try {
    const { page, pageSize } = req.query;
    const { user } = req;
    const organizations = await Organization.paginate(
      { ...query },
      {
        page: page,
        limit: pageSize,
        populate: [],
        lean: true,
        sort: { followersCount: -1 }
      }
    );

    const aggregatedOrganizations = organizations.docs.map((org) => {
      return memberAggregate(org, user, res);
    });
    const resultantOrgs = await Promise.all(aggregatedOrganizations);
    organizations.docs = resultantOrgs;
    return res.json({ organizations });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const associateGames = async (userId, organizationId, body) => {
  if ("games" in body) {
    try {
      const gameIds = body.games.length > 0 ? JSON.parse(body.games) : [];

      // Make sure valid ids are passed
      await organizationGameService.associateGames(userId, organizationId, gameIds);
    } catch (error) {
      logger.error(error, "Error while associating games");
    }
  }
};

// TODO: [perms] Update to use permission system to retrieve members
const getMembersByOrganizationId = async (req, res) => {
  try {
    const { page, pageSize } = req.query;
    const response = await organizationValid({ _id: req.params.id });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const orgId = GeneralHelper.getObjectId(req.params.id);
    const query = { organization: orgId, deleted: { $ne: true } };
    try {
      const members = await Member.paginate(
        { ...query },
        {
          page: page,
          limit: pageSize,
          lean: true,
          sort: { createdAt: -1 }
        }
      );

      const aggregatedMembers = members.docs.map((member) => {
        return OrgMembersAggregate(member, res);
      });
      const result = await Promise.all(aggregatedMembers);

      // Sort by member role
      const priority = { Owner: 0, Captain: 1, Member: 2 };
      const sortedMembers = result.sort((a, b) => priority[a.role] - priority[b.role]);
      members.docs = sortedMembers;

      res.json({ members });
    } catch (error) {
      logger.error(error);
      res.status(500).json(error);
    }
  } catch (error) {
    logger.error(error);
    res.status(500).json({ err: error.message });
  }
};

const update = async (req, res) => {
  try {
    const { user, params } = req;

    // todo: investigate
    // @ts-ignore
    const { organization } = await organizationACLService.authorizedToAccess(user._id, params.id);

    let organizationParams = { ...req.body };

    delete organizationParams.games;

    organizationParams = _.omit(organizationParams, ["_id"]);
    const { errors, isValid } = OrganizationValidations.validate(organizationParams);
    const { profileImage, headerImage } = req.files;

    if (!isValid) {
      return res.status(400).json(errors);
    }
    let organizationObject = {};
    organizationParams = _.omit(organizationParams, ["playerCards"]);
    organizationParams = _.omit(organizationParams, ["promoVideo"]);
    organizationParams = _.omit(organizationParams, ["honors"]);
    organizationParams = _.omit(organizationParams, ["events"]);
    organizationParams = _.omit(organizationParams, ["videoCarousel"]);

    if (profileImage !== undefined && headerImage !== undefined) {
      organizationObject = Object.assign(organization, organizationParams, {
        profileImage: GeneralHelper.buildFileObject(profileImage),
        headerImage: GeneralHelper.buildFileObject(headerImage)
      });
    } else if (profileImage !== undefined) {
      organizationParams = _.omit(organizationParams, ["headerImage"]);
      organizationObject = Object.assign(organization, organizationParams, {
        profileImage: GeneralHelper.buildFileObject(profileImage)
      });
    } else if (headerImage !== undefined) {
      organizationParams = _.omit(organizationParams, ["profileImage"]);
      organizationObject = Object.assign(organization, organizationParams, {
        headerImage: GeneralHelper.buildFileObject(headerImage)
      });
    } else {
      organizationParams = _.omit(organizationParams, ["profileImage"]);
      organizationParams = _.omit(organizationParams, ["headerImage"]);
      organizationObject = Object.assign(organization, organizationParams);
    }
    try {
      const updatedOrganization = await organizationObject.save();

      await associateGames(user._id, updatedOrganization._id, req.body);

      res.json({
        message: "Organization updated successfully",
        id: updatedOrganization._id,
        canEdit: true
      });
    } catch (error) {
      logger.error(error);
      return res.status(400).json(error);
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const show = async (req, res) => {
  try {
    let { id } = req.params;
    let query;
    const { user } = req;
    if (id.length < 24) {
      query = { slug: id };
      const response = await organizationValid(query);
      if (response.error) {
        return res.status(response.statusCode).json({ message: response.message });
      }
    } else {
      const response = await organizationValid({ _id: req.params.id });
      if (response.error) {
        return res.status(response.statusCode).json({ message: response.message });
      }
      id = GeneralHelper.getObjectId(req.params.id);
      query = { _id: id };
    }

    try {
      const organization = await Organization.findOne(query).lean();
      const aggregatedOrganization = await memberAggregate(organization, user, res);

      return res.json({ organization: aggregatedOrganization });
    } catch (error) {
      logger.error(error);
      return res.status(422).json({ error });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error.message });
  }
};

const showBySlug = async (req, res) => {
  let slug = null;
  try {
    slug = req.params.slug;
    const response = await organizationValid({ shortName: slug });

    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }

    const organization = await Organization.findOne({ shortName: slug }).lean();
    const aggregatedOrganization = await memberAggregate(organization, req.user, res, true);

    return res.json({ organization: aggregatedOrganization });
  } catch (error) {
    logger.error("Failed to retrieve organization data by slug", error);

    res.status(500).json(error.message);
  }
};

const index = async (req, res) => {
  let regexObj = GeneralHelper.convertToObjectIds("organizations", req);
  if (req.user?.roles?.includes("admin")) {
    await getOrganizations(req, res, regexObj);
  } else {
    regexObj = {
      ...regexObj,
      $or: [{ publishStatus: "visible" }, { publishStatus: { $exists: false } }]
    };
    await getOrganizations(req, res, regexObj);
  }
};

// TODO: [perms] Update to use permission system
const owned = async (req, res) => {
  try {
    const userId = GeneralHelper.getObjectId(req.user.id);
    const query = {
      user: userId
    };
    await getOrganizations(req, res, query);
  } catch (error) {
    logger.error(error.message);
    res.status(500).json({ err: error.message });
  }
};

const recentOrganizations = async (req, res) => {
  const query = {
    _id: { $nin: req.user.organizations },
    user: { $ne: req.user.id },
    $or: [{ publishStatus: "visible" }, { publishStatus: { $exists: false } }]
  };
  await getOrganizations(req, res, query);
};

// TODO: [perms] Update to create permission role on join
const joinOrganization = async (req, res, next) => {
  try {
    const { user, params, body } = req;

    // Validate & Fetch organization
    const organization = await organizationACLService.validateOrganization(params.id);

    let validInviteCode = false;
    if (body.inviteCode) {
      // TODO: update to use invite code service
      const inviteCodes = await InviteCode.find({
        code: body.inviteCode,
        organization: organization._id,
        type: InviteCodeType.ORGANIZATION_MEMBER_INVITE,
        active: true
      }).lean();

      if (!inviteCodes || inviteCodes.length === 0) {
        res.status(422).json({ error: "This invite code is not valid." });
      } else {
        validInviteCode = true;
      }
    }

    try {
      const count = await Member.countDocuments({ user: req.user.id });

      if (count === 20000) {
        return res.json({
          message: "You cannot join more than 100 organizations",
          flashType: "notice",
          status: 405
        });
      } else if (organization.status === "Open" || (body.inviteCode && validInviteCode)) {
        const createdRequest = await new OrganizationRequest({
          organization: organization._id,
          user: user.id,
          status: "approved"
        }).save();
        try {
          const newMember = await new Member({
            organization: req.params.id,
            user: req.user.id,
            createdAt: Date.now()
          }).save();

          // await organizationACLService.addUserOrgRole(req.params.id, req.user.id, OrganizationUserRoleEnum.MEMBER);

          return res.json({
            message: "You have successfully joined the organization",
            member: newMember,
            orgRequest: createdRequest,
            flashType: "success",
            status: 204
          });
        } catch (error) {
          return res.status(400).json(error);
        }
      } else {
        const orgRequest = await new OrganizationRequest({
          organization: organization._id,
          user: user.id,
          status: "pending"
        }).save();

        return res.json({
          message: "Your request has been sent to organization admin",
          organizationRequest: orgRequest,
          flashType: "success",
          status: 204
        });
      }
    } catch (error) {
      return res.status(400).json(error);
    }
  } catch (error) {
    next(error);
  }
};

const leaveOrganization = async (req, res) => {
  try {
    const { user, params } = req;
    await Organization.findOneAndUpdate({ _id: params.id }, { $pull: { captains: user.id } });
    try {
      await Member.findOneAndDelete({ organization: params.id, user: user.id });
      try {
        await OrganizationRequest.findOneAndDelete({
          organization: params.id,
          user: user.id
        });

        // await organizationACLService.removeUserOrgRole(params.id, user.id);

        try {
          const members = await Member.find({ organization: params.id });
          return res.send({
            members: members,
            message: "You have been removed from the organization as a member",
            flashType: "danger"
          });
        } catch (error) {
          logger.error(error);
          return res.status(500).json(error);
        }
      } catch {
        return res.status(404).json({ notfound: "No record found" });
      }
    } catch {
      return res.status(404).json({ notfound: "No record found" });
    }
  } catch {
    return res.status(404).json({ notfound: "No record found" });
  }
};

const addAsCaptain = async (req, res, next) => {
  try {
    const { user, params, body } = req;
    const response = await organizationsService.addAsCaptain(user, params.id, body.userId);
    return res.json(response);
  } catch (error) {
    return next(error);
  }
};

const removeAsCaptain = async (req, res, next) => {
  try {
    const { params, body, user } = req;
    const response = await organizationsService.removeAsCaptain(user, params.id, body.userId);
    return res.json(response);
  } catch (error) {
    return next(error);
  }
};

const banMember = async (req, res, next) => {
  try {
    const { user, params, body } = req;
    const response = await organizationsService.banMember(user, params.id, body.userId);
    return res.json(response);
  } catch (error) {
    return next(error);
  }
};

const unBanMember = async (req, res, next) => {
  try {
    const { user, params, body } = req;
    const response = await organizationsService.unBanMember(user, params.id, body.userId);
    return res.json(response);
  } catch (error) {
    return next(error);
  }
};

const updatePromoVideo = async (req, res) => {
  try {
    const { params, body, user } = req;
    const updatedOrganization = await Organization.findOneAndUpdate(
      { _id: params.organizationId, user: user.id },
      { $set: { promoVideo: body.promoVideo } },
      { new: true }
    );

    if (updatedOrganization) {
      const associatedGames = await organizationGameService.getAssociatedGames(params.organizationId);

      return res.json({ success: true, organization: { ...updatedOrganization.toObject(), associatedGames } });
    } else {
      return res.status(404).json({ success: false, message: "No Record Found!" });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const changePublishStatus = async (req, res) => {
  const { params, body, user } = req;

  if (!["draft", "visible"].includes(body.status)) {
    res.status(400).json({
      success: false,
      message: "This is not an acceptable organization status."
    });
  }

  const org = await Organization.findById({
    _id: params.organizationId
  });

  let allowOrgToBeVisible = true;
  let reasons = "";

  // Check if organization meets all requirements to become Visibile

  // todo: investigate
  // @ts-ignore
  if (org.profileImage.url === SchemaHelper.fileSchema.url.default) {
    allowOrgToBeVisible = false;
    reasons = reasons + ", Profile image";
  }

  // todo: investigate
  // @ts-ignore
  if (org.headerImage.url === SchemaHelper.fileSchema.url.default) {
    allowOrgToBeVisible = false;
    reasons = reasons + ", Header image";
  }

  // todo: investigate
  // @ts-ignore
  if (!org.about || org.about.length === 0) {
    allowOrgToBeVisible = false;
    reasons = reasons + ", About";
  }

  // todo: investigate
  // @ts-ignore
  if (!org.description || org.description.length === 0) {
    allowOrgToBeVisible = false;
    reasons = reasons + ", Summary";
  }

  // at least two or more organization portfolio sections are required.
  let count = 0;

  // player card
  // todo: investigate
  // @ts-ignore
  if (org.playerCards && org.playerCards.length > 0) count++;

  // highlight reel
  // todo: investigate
  // @ts-ignore
  if (org.promoVideo.isVideo) count++;

  // events
  // todo: investigate
  // @ts-ignore
  if (org.events.length > 0) count++;

  // honors
  // todo: investigate
  // @ts-ignore
  if (org.honors.length > 0) count++;

  // video clip carousel
  // todo: investigate
  // @ts-ignore
  if (org.videoCarousel.length > 0) count++;

  logger.info("count", count);

  if (count < 2) {
    allowOrgToBeVisible = false;
    reasons =
      reasons + ", Two (2) of the following sections: Player Cards, Highlight Reel, Events, Honors, or Video clips";
  }

  // allow admins to publish any org, even if they do not meet requirements
  if (user.roles.includes("admin")) allowOrgToBeVisible = true;

  if (body.status === "visible" && !allowOrgToBeVisible) {
    res.status(400).json({
      success: false,
      message: `Organization does not meet requirements to be visible. Missing requirements: ${reasons.slice(
        2,
        reasons.length
      )}`
    });
  }
  try {
    const updatedOrganization = await Organization.findOneAndUpdate(
      { _id: params.organizationId },
      { $set: { publishStatus: body.status } },
      { new: true }
    );

    return updatedOrganization
      ? res.json({ success: true, organization: updatedOrganization })
      : res.status(404).json({ success: false, message: "No Record Found!" });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const manageAccolades = async (req, res) => {
  try {
    const { user, body, params } = req;
    const organization = await Organization.findOne({
      _id: params.organizationId,
      user: user.id
    });

    if (organization) {
      const { _id, key, ...fields } = body;
      const { image } = req.files;
      const accolades = organization[key];
      const description = GeneralHelper.validateHtml(fields.description);

      if (_id) {
        let existingItem = accolades.id(_id);
        Object.keys(fields).forEach((key) => (existingItem[key] = key === "description" ? description : fields[key]));
        if (image !== undefined) {
          existingItem.image = GeneralHelper.buildFileObject(image);
        } else {
          existingItem = _.omit(existingItem, ["image"]);
        }
        logger.info("existing accolade", existingItem);
      } else {
        accolades.push({
          ...fields,
          description,
          image: GeneralHelper.buildFileObject(image)
        });
      }
      try {
        const updatedOrganization = await organization.save({
          validateBeforeSave: false
        });

        const associatedGames = await organizationGameService.getAssociatedGames(organization._id);

        return res.json({ success: true, organization: { ...updatedOrganization.toObject(), associatedGames } });
      } catch (error) {
        logger.error(error);
        return res.status(500).json({ err: error });
      }
    } else {
      res.status(404).json({
        success: false,
        message: "You are not authorized to perform this action!"
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const removeAccoldate = async (req, res) => {
  try {
    const { user, params } = req;
    const { organizationId, accoladeKey, accoladeId } = params;
    const organization = await Organization.findOne({
      _id: organizationId,
      user: user.id
    });

    if (organization) {
      organization[accoladeKey].pull({
        _id: GeneralHelper.getObjectId(accoladeId)
      });
      const updatedOrganization = await organization.save({
        validateBeforeSave: false
      });

      const associatedGames = await organizationGameService.getAssociatedGames(organization._id);

      return res.json({ success: true, organization: { ...updatedOrganization.toObject(), associatedGames } });
    } else {
      return res.status(404).json({
        success: false,
        message: "You are not authorized to perform this action!"
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const deleteItem = async (req, res) => {
  try {
    const { user, params } = req;
    const { organizationId } = params;
    await Organization.findOneAndDelete({
      _id: organizationId,
      user: user._id
    });

    return res.send({ success: true, organizationId: organizationId });
  } catch {
    return res.status(404).send({ message: "Record not found" });
  }
};

const managePlayerCard = async (req, res) => {
  const { params, body } = req;
  const { image } = req.files;
  const organization = await Organization.findById({
    _id: params.organizationId
  });

  // todo: investigate
  // @ts-ignore
  const { playerCards } = organization;

  if (body._id) {
    let playerCard = playerCards.id(body._id);
    playerCard.title = body.title;
    playerCard.subTitle = body.subTitle;
    playerCard.url = body.url;
    playerCard = GeneralHelper.saveFile(image, "image", playerCard);
  } else {
    if (body._id) {
      delete body._id;
    }
    let playerCard = { ...body };
    playerCard = GeneralHelper.saveFile(image, "image", playerCard);
    playerCards.push(playerCard);
  }
  try {
    // todo: investigate
    // @ts-ignore
    const updatedOrganization = await organization.save({
      validateBeforeSave: false
    });

    const associatedGames = await organizationGameService.getAssociatedGames(organization?._id);

    return res.json({ organization: { ...updatedOrganization.toObject(), associatedGames } });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const removePlayerCard = async (req, res) => {
  const { params } = req;
  const organization = await Organization.findById({
    _id: params.organizationId
  });
  // todo: investigate
  // @ts-ignore
  organization.playerCards.pull({ _id: params.id });
  try {
    // todo: investigate
    // @ts-ignore
    const updatedOrganization = await organization.save({
      validateBeforeSave: false
    });

    const associatedGames = await organizationGameService.getAssociatedGames(organization?._id);

    return res.json({ organization: { ...updatedOrganization.toObject(), associatedGames } });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const manageCarouselVideo = async (req, res) => {
  const { params, body, user } = req;
  const organization = await Organization.findById({
    _id: params.organizationId,
    user: user.id
  });

  if (organization) {
    const { videoCarousel } = organization;
    if (body._id) {
      const { title, url, uploaded, video } = body;

      // todo: investigate
      // @ts-ignore
      const existingVideo = videoCarousel.id(body._id);
      if (!existingVideo) {
        return res.status(422).json({ error: "Video not found" });
      }
      existingVideo.title = title;
      existingVideo.url = uploaded ? "" : url;
      existingVideo.uploaded = uploaded;
      existingVideo.video = uploaded ? video : {};
    } else {
      if (body._id) {
        delete body._id;
      }
      const videoObject = { ...body };

      // todo: investigate
      // @ts-ignore
      videoCarousel.addToSet(videoObject);
    }
    try {
      const updatedOrganization = await organization.save({
        validateBeforeSave: false
      });

      const associatedGames = await organizationGameService.getAssociatedGames(organization._id);

      res.json({ organization: { ...updatedOrganization.toObject(), associatedGames } });
    } catch (error) {
      logger.error(error);
      res.status(500).json({ err: error });
    }
  } else {
    return res.status(404).json({
      success: false,
      message: "You are not authorized to perform this action!"
    });
  }
};

const deleteCarouselVideo = async (req, res) => {
  try {
    const { user, params } = req;
    const { organizationId, videoCarouselId } = params;
    const organization = await Organization.findOne({
      _id: organizationId,
      user: user.id
    });

    if (organization) {
      // todo: investigate
      // @ts-ignore
      organization.videoCarousel.pop({ _id: GeneralHelper.getObjectId(videoCarouselId) });

      const updatedOrganization = await organization.save({
        validateBeforeSave: false
      });

      const associatedGames = await organizationGameService.getAssociatedGames(organization._id);

      return res.json({ success: true, organization: { ...updatedOrganization.toObject(), associatedGames } });
    } else {
      return res.status(404).json({
        success: false,
        message: "You are not authorized to perform this action!"
      });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const discordServer = async (req, res) => {
  const { params } = req;
  const { discordServer } = req.body;
  const organization = await Organization.findById({
    _id: params.organizationId
  });

  // todo: investigate
  // @ts-ignore
  organization.discordServer = discordServer;
  try {
    // todo: investigate
    // @ts-ignore
    const updatedOrganization = await organization.save({
      validateBeforeSave: false
    });

    return res.json({ organization: updatedOrganization });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

const removeDiscordServer = async (req, res) => {
  const { params } = req;
  const organization = await Organization.findById({
    _id: params.organizationId
  });

  // todo: investigate
  // @ts-ignore
  organization.discordServer = "";
  try {
    // todo: investigate
    // @ts-ignore
    const updatedOrganization = await organization.save({
      validateBeforeSave: false
    });

    return res.json({ organization: updatedOrganization });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error });
  }
};

// TODO: [perms] Use permissions system instead of captains on org object
const currentUsersPostableOrganizations = async (req, res) => {
  const logError = (error) => logger.error(`Error Finding Organizations for user | ${req.user.id}`, error);
  try {
    const { user, query } = req;
    const { page, pageSize } = query;
    const userId = GeneralHelper.getObjectId(user.id);
    const dbQuery = {
      $and: [
        {
          $or: [{ user: userId }, { captains: userId }]
        },
        {
          $or: [{ publishStatus: "visible" }, { publishStatus: { $exists: false } }]
        }
      ]
    };
    const organizations = await Organization.paginate(dbQuery, {
      page,
      limit: pageSize,
      select: "_id name verified profileImage",
      lean: true,
      sort: { name: 0 }
    });
    return res.json(organizations);
  } catch (error) {
    logError(error);
    return res.status(500).json({ err: error.message });
  }
};

const inviteByEmail = async (req, res) => {
  const { params, body } = req;

  if (SENDGRID_API_KEY === undefined) {
    logger.error("SENDGRID_API_KEY environment variable is undefined.");
    res.status(500).json({ error: "Environment variable not set." });
  }

  try {
    const organization = await Organization.findById({
      _id: params.organizationId
    });

    // check if organization exists
    // todo: investigate
    // @ts-ignore
    if (!organization || organization.length === 0) {
      res.status(422).json({ error: "This organization does not exist." });
    }
    let validInviteCode = false;

    if (body.inviteCode) {
      // TODO: update to use invite code service
      const inviteCodes = await InviteCode.find(
        {
          code: body.inviteCode,
          organization: params.organizationId,
          type: InviteCodeType.ORGANIZATION_MEMBER_INVITE,
          active: true
        },
        {},
        { lean: true }
      );

      if (!inviteCodes || inviteCodes.length !== 1) {
        res.status(422).json({ error: "This invite code is not valid." });
      } else {
        validInviteCode = true;
      }
    }

    // invite code was never sent or is invalid
    if (!validInviteCode) res.status(422).json({ error: "This invite code is not valid." });

    const sgMail = require("@sendgrid/mail");
    logger.info("SENDGRID_API_KEY", SENDGRID_API_KEY);
    sgMail.setApiKey(SENDGRID_API_KEY);

    const msg = {
      template_id: "d-37cb066dc86643d9a7c5bc25eeb68fc1",
      dynamic_template_data: {
        // todo: investigate
        // @ts-ignore
        organization: organization.name,
        inviteUrl: `https://efuse.gg/i/${body.inviteCode}`,

        // todo: investigate
        // @ts-ignore
        imageUrl: organization.profileImage.url
      },
      to: body.emails,
      from: "info@efuse.gg"
    };

    // sendMultiple() will send individual emails to mulitple recipients ... this will not be able to see the other recipients
    try {
      // todo: investigate
      // @ts-ignore
      await sgMail.sendMultiple(msg);

      return res.json({
        success: true,
        payload: msg,
        message: "Email invitations have successfully been sent."
      });
    } catch (error) {
      logger.error(error);

      if (error.response) {
        const { response } = error;
        const { body } = response;
        logger.error(body);
        res.status(500).json({ error: body });
      } else {
        logger.error(error);
        res.status(500).json({ error: "Send Grid error" });
      }
    }
  } catch (error) {
    logger.error(error);
    res.status(500).json({ error: error.message });
  }
};

module.exports.addAsCaptain = addAsCaptain;
module.exports.associateGames = associateGames;
module.exports.banMember = banMember;
module.exports.changePublishStatus = changePublishStatus;
module.exports.currentUsersPostableOrganizations = currentUsersPostableOrganizations;
module.exports.deleteCarouselVideo = deleteCarouselVideo;
module.exports.deleteItem = deleteItem;
module.exports.discordServer = discordServer;
module.exports.getMembersByOrganizationId = getMembersByOrganizationId;
module.exports.getOrganizations = getOrganizations;
module.exports.index = index;
module.exports.inviteByEmail = inviteByEmail;
module.exports.joinOrganization = joinOrganization;
module.exports.leaveOrganization = leaveOrganization;
module.exports.manageAccolades = manageAccolades;
module.exports.manageCarouselVideo = manageCarouselVideo;
module.exports.managePlayerCard = managePlayerCard;
module.exports.memberAggregate = memberAggregate;
module.exports.organizationValid = organizationValid;
module.exports.OrgMembersAggregate = OrgMembersAggregate;
module.exports.owned = owned;
module.exports.recentOrganizations = recentOrganizations;
module.exports.removeAccoldate = removeAccoldate;
module.exports.removeAsCaptain = removeAsCaptain;
module.exports.removeDiscordServer = removeDiscordServer;
module.exports.removePlayerCard = removePlayerCard;
module.exports.show = show;
module.exports.showBySlug = showBySlug;
module.exports.unBanMember = unBanMember;
module.exports.update = update;
module.exports.updatePromoVideo = updatePromoVideo;
