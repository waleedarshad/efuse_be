import { NextFunction, Request, Response } from "express";

import { BaseController } from "../controllers/base.controller";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";
import { FeatureFlagAsyncService } from "./../../lib/feature-flags/feature-flag-async.service";
import { Slack } from "../../lib/slack";
import { IFlagSmithWebhookBody, IFlagSmithWebhookData } from "../interfaces/flagsmith";

const NAME = "feature-flag.controller";
const SLACK_CHANNEL = "#bullettrain-updates";

export class FeatureFlagController extends BaseController {
  private $featureFlagsAsyncService: FeatureFlagAsyncService;

  constructor() {
    super(NAME);

    this.$featureFlagsAsyncService = new FeatureFlagAsyncService();
  }

  public webhookHandler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { data, event_type } = req.body as IFlagSmithWebhookBody;

      const { identity_identifier } = data.new_state;

      switch (event_type) {
        case "FLAG_UPDATED":
          // If an identity is present in the webhook this is for a specific user
          if (identity_identifier) {
            this.logger.info(`[${identity_identifier}] Updating feature flags for user`);
            await FeatureFlagService.refreshUserFeatureFlags(identity_identifier);
            await this.sendNotification(data);

            res.end();
          } else {
            await this.sendNotification(data);
            this.$featureFlagsAsyncService.refreshGlobalFeatureFlags();

            res.end();
          }
          break;
        default:
          res.end();
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Send notification to slack channel #bullettrain-updates
   *
   * @param {IFlagSmithWebhookData} data - data received from BT webhook
   */
  private sendNotification = async (data: IFlagSmithWebhookData) => {
    const { changed_by, new_state } = data;

    const { feature, enabled, environment, identity_identifier, feature_segment } = new_state;

    // Default value
    let enabledForText = "for everyone globally";

    // Check identity
    if (identity_identifier) {
      enabledForText = `for a specific user ${identity_identifier}`;
    }

    // Check segment
    if (feature_segment) {
      enabledForText = `for segment ${feature_segment.segment.name}`;
    }

    // Send notification
    try {
      return await Slack.sendMessage(
        `${changed_by} changed ${feature.name} to ${enabled ? "visible" : "hidden"} in environment ${
          environment.name
        } ${enabledForText}`,
        SLACK_CHANNEL
      );
    } catch (error) {
      this.logger.error(error, "Error while sending slack notification when flag-smith flag is changed");
    }
  };
}
