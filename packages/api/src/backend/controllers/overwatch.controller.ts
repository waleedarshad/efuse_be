import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "./base.controller";
import { IUserOverwatchStats } from "../interfaces/user-overwatch-stats";
import { OverwatchService } from "../../lib/overwatch/overwatch.service";

const NAME = "overwatch.controller";

export class OverwatchController extends BaseController {
  private service: OverwatchService;

  constructor() {
    super(NAME);

    this.service = new OverwatchService();
  }

  /**
   * Update updateOverwatchStats
   */

  public updateOverwatchStats = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = req as { user: any };
    const { platform, nickname } = req.params;

    try {
      if (!nickname) {
        throw Failure.BadRequest("Missing parameter 'nickname'");
      }
      if (!platform) {
        throw Failure.BadRequest("Missing parameter 'platform'");
      }

      const newOverwatchInfo: IUserOverwatchStats = { nickname, platform };

      const overwatchInfo = await this.service.updateOverwatchStats(user.id, newOverwatchInfo);

      res.status(200).json(overwatchInfo);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Get getOverwatchStats
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof OverwatchController
   */

  public getOverwatchStats = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { platform, nickname } = req.params;

    try {
      if (!nickname) {
        throw Failure.BadRequest("Missing parameter 'nickname'");
      }
      if (!platform) {
        throw Failure.BadRequest("Missing parameter 'platform'");
      }

      const overwatchInfo = await this.service.getOverwatchStats(platform, nickname);
      res.status(200).json(overwatchInfo);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Manually refresh and retrieve a user's stats for overwatch
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof OverwatchController
   */
  public get = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = req as { user: any };
    const { refreshStats } = req.query as { [key: string]: string };

    try {
      if (!user.overwatch?.nickname) {
        throw Failure.BadRequest("User has not linked overwatch account.");
      }

      const overwatchInfo = await (refreshStats === "true"
        ? this.service.syncUserOverwatchStats(user._id)
        : this.service.getUserOverwatchStats(user._id));

      res.status(200).json(overwatchInfo);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
