import { NextFunction, Request, Response } from "express";

import { GameBaseService } from "../../lib/game/game-base.service";
import { BaseController } from "./base.controller";

const NAME = "game.controller";

export class GameController extends BaseController {
  protected declare $gameBaseService: GameBaseService;

  constructor() {
    super(NAME);

    this.$gameBaseService = new GameBaseService();
  }

  /**
   * @deprecated Use GraphQL
   * Retrieve all the Game
   *
   * @memberof GameController
   */
  public list = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const games = await this.$gameBaseService.find({});
      res.json({ count: games.length, items: games });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated Use GraphQL
   * Retrieve a particular game by ID
   *
   * @param gameId
   *
   * @memberof GameController
   */
  public get = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { gameId } = req.params;

      const game = await this.$gameBaseService.findById(gameId);

      res.json({ count: [game].length, items: [game] });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
