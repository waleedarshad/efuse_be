import { NextFunction, Request, Response } from "express";
import { MediaService } from "../../lib/media/media.service";
import { BaseController } from "./base.controller";

const NAME = "media.controller";

export class MediaController extends BaseController {
  private mediaService: MediaService;

  constructor() {
    super(NAME);

    this.mediaService = new MediaService();
  }

  /**
   * @summary Get paginated feed media of a user
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof MediaController
   */
  public getPaginatedFeedMediaOfUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query } = req;

      const paginatedMedia = await this.mediaService.getPaginatedFeedMediaOfUser(
        params.userId,
        Number(query.page),
        Number(query.pageSize)
      );

      res.json({ media: paginatedMedia });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
