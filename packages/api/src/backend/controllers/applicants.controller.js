// Libraries
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const createError = require("http-errors");
const { Failure } = require("@efuse/contracts");

// Models
const { Applicant } = require("../models/applicant");
const { User } = require("../models/User");
const { Opportunity } = require("../models/Opportunity");

// Helpers
const { GeneralHelper } = require("../helpers/general.helper");
const NotificationsHelper = require("../helpers/notifications.helper");
const WhitelistHelper = require("../helpers/whitelist_helper");

// Libs
const { createOrder } = require("../../lib/orders");
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");
const { ValidateRequirementsService } = require("../../lib/applicants/validate-requirements.service");
const { OpportunityRankService } = require("../../lib/opportunities/opportunity-rank.service");
const { ApplicantService } = require("../../lib/applicants/applicant.service");
const { ApplicantFeatureFlags } = require("../../lib/applicants/applicant-feature-flags");
const { StreaksService } = require("../../lib/streaks/streaks.service");

const { BrazeCampaignName } = require("../../lib/braze/braze.enum");
const { BrazeService } = require("../../lib/braze/braze.service");
const { ApplicantEntityTypeEnum, ApplicantStatusEnum } = require("../../lib/applicants/applicant.enum");

const opportunityRankService = new OpportunityRankService();
const applicantService = new ApplicantService();
const streaksService = new StreaksService();
const brazeService = new BrazeService();
const validateRequirementsService = new ValidateRequirementsService();

/**
 * @deprecated In favor of ApplicantApply mutation
 */
const create = async (req, res, next) => {
  const { user, params, body } = req;
  const { opportunityId } = params;

  // Make sure opportunityID is valid
  if (!applicantService.isValidObjectId(opportunityId)) {
    throw Failure.BadRequest("Invalid opportunityId");
  }

  const query = {
    user: user.id,
    entity: opportunityId,
    entityType: ApplicantEntityTypeEnum.OPPORTUNITY
  };

  try {
    EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.call");

    const opp = await Opportunity.findById(opportunityId);

    if (!opp) {
      EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.failure");
      logger.error("Opportunity does not exist.");
      throw createError(422, "Opportunity does not exist.");
    }

    // Validate requirements
    const { canApply, requirementsMet, requirementsNotMet } =
      await validateRequirementsService.validateByRequirementIds(user.id, opp.requirements);

    if (!canApply) {
      throw createError(406, "Requirements are not met", {
        properties: { canApply, requirementsMet, requirementsNotMet }
      });
    }

    if (opp.streamRequired) {
      const applicantUser = await User.findById(user.id);

      if (!applicantUser) {
        EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.failure");
        logger.error("User does not exist.");
        throw createError(422, "User does not exist");
      }

      // opp.streamRequired requires at least one stream be connected.
      if (!applicantUser.youtubeChannelId && !applicantUser.twitchVerified) {
        // Disabling this rejection until mobile app uses the webview
        // throw createError(
        //   406,
        //   "Requirement Failed: You must have a stream linked to your portfolio."
        // );
        logger.info("[DISABLED] Applicant failed stream requirement.");
      }
    }
    const applicant = await Applicant.findOne(query, {}, { autopopulate: false }).lean();

    // Don't let the user to apply again
    if (applicant) {
      EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.success");
      return res.json({ applicant, applied: true });
    }

    // Parse candidate questions
    if (body.candidateQuestions) {
      query.candidateQuestions = JSON.parse(body.candidateQuestions) || [];
    }

    // only create payment and order if opportunity requires application fee
    if (opp.applicationCost && opp.applicationCost > 0) {
      try {
        // attempt payment via stripe - if successful, create new order
        await createOrder(
          opp.applicationCost,
          `eFuse Opportunity Application Fee: ${opp.title}`,
          user.id,
          "PAID_OPPORTUNITY",
          req.body.source,
          req.body.promocode || null
        );
      } catch (error) {
        EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.failure");
        if (error.statusCode && error.raw.message) {
          // return specific error from stripe API
          throw createError(402, error.raw.message);
        }
        // unknown order error
        logger.error(error, "Unknown order error");
        throw createError(400, "Something went wrong!");
      }
    }

    // Making sure to store entity & entityType fields
    // For now storing opportunity as well so that we can rollback easily if we had to
    const newApplicant = await new Applicant({ ...query, opportunity: opportunityId }).save();

    // Adding opportunity to async queue for rank re-calculation
    opportunityRankService.setObjectRankAsync(opportunityId);

    EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.success");

    NotificationsHelper.applyNotification(user, opportunityId).catch((error) =>
      logger.error(error, "Error resolving applyNotificationPromise")
    );

    // Send notification to opportunity owner letting them know someone applied.
    brazeService.sendCampaignMessageAsync(BrazeCampaignName.OPPORTUNITY_APPLICANT_APPLIED, {
      trigger_properties: {
        applicant_username: user.username,
        applicant_user_id: String(user._id),
        opportunity_title: opp.title,
        opportunity_id: String(opportunityId)
      },
      // todo: investigate
      // @ts-ignore
      recipients: [{ external_user_id: String(opp.user._id) }]
    });

    // Send Email Notification to applicant
    const enableApplicantApplyEmail = await req.bulletTrain.hasFeature(
      ApplicantFeatureFlags.ENABLE_APPLICANT_APPLY_EMAIL,
      user._id
    );

    if (enableApplicantApplyEmail) {
      // email confirmation sent to applicant
      applicantService.notifyAsync(user._id, BrazeCampaignName.OPPORTUNITY_APPLICATION_RECEIVED, opp);
    }
    await streaksService.checkStreak(user._id, "dailyUsage", "Opportunity Apply");
    return res.json({ applicant: newApplicant, applied: true });
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("applicants.controller.create.failure");
    logger.error(error, "Error in create() function - applicants.controller.js");
    return next(error);
  }
};

const index = async (req, res, next) => {
  try {
    const { page, pageSize, filters, query } = req.query;

    const { opportunityId } = req.params;

    // Make sure opportunityID is valid
    if (!applicantService.isValidObjectId(opportunityId)) {
      throw Failure.BadRequest("Invalid opportunityId");
    }

    const acceptedTypes = Object.values(ApplicantStatusEnum);

    const applicationStatusFilter =
      ["all", "", undefined].includes(query) || !acceptedTypes.includes(query) ? {} : { status: { $eq: query } };

    const opp = await Opportunity.findById(opportunityId);

    if (!opp) return res.status(422).json({ err: "No opportunity found" });

    // TODO: [perms] Update to use permission system
    // todo: investigate
    // @ts-ignore
    const isOrgAndCaptain = opp.organization && opp.organization.captains.includes(req.user.id);

    // Make sure user is present
    if (!opp.user) {
      throw Failure.UnprocessableEntity("Opportunity owner not found");
    }

    if (String(opp.user) === String(req.user._id) || req.user.roles.includes("admin") || isOrgAndCaptain) {
      const applicantsCollection = await Applicant.find({
        entity: opportunityId,
        entityType: ApplicantEntityTypeEnum.OPPORTUNITY,
        ...applicationStatusFilter
      }).lean();

      const userIds = applicantsCollection.map((applicant) => applicant.user);

      const users = await User.paginate(
        { _id: { $in: userIds }, ...GeneralHelper.jsonParser(filters) },
        {
          page,
          limit: pageSize,
          sort: { createdAt: -1 },
          populate: [],
          lean: true,
          select: WhitelistHelper.allowedApplicantFields
        }
      );

      const { docs, ...pagination } = users;

      const applicants = docs.map(async (user) => {
        const applicant = applicantsCollection.find((applicant) => String(applicant.user) === String(user._id));
        if (applicant) {
          applicant.user = user;
        }
        return applicant;
      });

      const results = await Promise.all(applicants);

      return res.json({ users: { docs: results, ...pagination }, canAccess: true });
    } else {
      return res.json({ canAccess: false });
    }
  } catch (error) {
    next(error);
  }
};

const updateStatus = async (req, res, next) => {
  try {
    const { body, user, params } = req;
    const { applicantId, opportunityId } = params;

    const opportunity = await Opportunity.findById(opportunityId);
    if (!opportunity) {
      throw createError(422, "No Opportunity Found");
    }

    // todo: investigate
    // @ts-ignore
    if (!opportunity.user.equals(user.id)) {
      throw createError(403, "You are not allowed to perform this action");
    }

    const updatedApplication = await Applicant.findOneAndUpdate(
      { _id: applicantId },
      { $set: { ...body } },
      { new: true }
    );

    // Send email notification to applicant only when application is accepted
    const enableApplicationAcceptedEmail = await req.bulletTrain.hasFeature(
      ApplicantFeatureFlags.ENABLE_APPLICATION_ACCEPTED_EMAIL,
      // todo: investigate
      // @ts-ignore
      updatedApplication.user
    );

    if (enableApplicationAcceptedEmail && body.sendEmail === true && body.status === ApplicantStatusEnum.ACCEPTED) {
      applicantService.notifyAsync(
        // todo: investigate
        // @ts-ignore
        updatedApplication.user,
        BrazeCampaignName.OPPORTUNITY_APPLICATION_ACCEPTED,
        opportunity
      );
    }

    res.json({ applicant: updatedApplication, success: true });
  } catch (error) {
    next(error);
  }
};

const validateRequirements = async (req, res, next) => {
  try {
    const { user, params } = req;

    const entity = await applicantService.getEntity(params.opportunityId, ApplicantEntityTypeEnum.OPPORTUNITY);

    const { canApply, requirementsMet, requirementsNotMet } =
      await validateRequirementsService.validateByRequirementIds(user._id, entity.requirements);

    return res.json({ canApply, requirementsMet, requirementsNotMet });
  } catch (error) {
    return next(error);
  }
};

module.exports.create = create;
module.exports.index = index;
module.exports.updateStatus = updateStatus;
module.exports.validateRequirements = validateRequirements;
