const moment = require("moment");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../models/User");
const { Session } = require("../models/session");

const WhitelistHelper = require("../helpers/whitelist_helper");
const { matchPassword } = require("../../lib/users");
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");
const { TokenService } = require("../../lib/users/token.service");

const tokenService = new TokenService();

const test = (req, res) => {
  User.findById("5cff495db727c37b83c38b0b")
    .then(() => {
      return res.json({ message: "It's alive", useragent: req.useragent });
    })
    .catch((error) => {
      logger.error(error);
      return res.status(500).json("Test error.");
    });
};

const create = async (req, res) => {
  const { dbUser, logFunc, invalidLoginError, body } = req;
  const userParams = body.user ? body.user : body;

  logFunc("*** Initiating Auth ***");

  try {
    const userId = dbUser._id;

    const matched = await matchPassword(userId, userParams.password);
    if (!matched) {
      logFunc(`Error invalid password: ${JSON.stringify(invalidLoginError)}`);
      return res.status(401).json(invalidLoginError);
    }

    const { firstRun } = dbUser;
    if (firstRun) {
      dbUser.firstRun = false;
    }

    await dbUser.save({ validateBeforeSave: false });

    const session = await tokenService.createSession(dbUser._id, req.useragent, req.ipInfo);
    logFunc("Logged in successfully!");

    return res.json({
      ...session,
      user: WhitelistHelper.userPayload(dbUser),
      message: "Logged in successfully!"
    });
  } catch (error) {
    logFunc("Error saving user session to database");
    logFunc(error);

    logger.error(error);

    return res.status(500).json(invalidLoginError);
  }
};

const refreshToken = async (req, res) => {
  const { refreshToken } = req.body;
  try {
    const errorMsg = "Invalid Refresh Token";
    if (!refreshToken) {
      logger.error("Refresh not found in req.body");
      return res.status(400).end(errorMsg);
    }

    const session = await Session.findOne({
      refreshToken,
      active: true,
      expiry: { $gt: new Date() }
    });

    if (!session) {
      logger.error({ refreshToken }, `Refresh Token Not found or expired`);
      return res.status(422).end(errorMsg);
    }

    const user = await User.findOne({ _id: session.user }).select("_id status").lean();

    if (!user) {
      logger.error({ session, refreshToken }, `User Not found`);
      return res.status(422).end(errorMsg);
    }

    if (user.status === "Blocked") {
      logger.error({ session, user }, `User has been blocked`);
      return res.status(422).json({ message: "Your account has been blocked" });
    }

    // Creating 5 minutes grace period window, to allow user to user the same refresh token within 5 minutes of initial use
    if (session.used && session.usedAt) {
      const usedAt = moment(session.usedAt);
      const fiveMinWindow = moment().subtract(5, "m");
      if (usedAt.diff(fiveMinWindow, "minutes") <= 0) {
        logger.error(
          { user, refreshToken, session },
          `Refresh token has already been used and exceeded the 5 minute grace period`
        );

        return res.status(422).end(errorMsg);
      }
    }

    const newSession = await tokenService.createSession(user._id, req.useragent, req.ipInfo);
    session.used = true;

    // Updating usedAt only if it not set, so that we can properly create grace period window
    if (!session.usedAt) {
      // todo: investigate
      // @ts-ignore
      session.usedAt = Date.now();
    }

    await session.save();

    return res.json(newSession);
  } catch (error) {
    logger.error({ refreshToken, error }, `Error while refreshing token`);

    return res.status(422).json(error);
  }
};

const createFromThirdParty = async (req, res, authID, authSource, authData, authName, user) => {
  const localUser = user;
  logger.info("Initiating 3rd parth login", { authID, authSource });
  EFuseAnalyticsUtil.incrementMetric(`3rdPartyAuth.${authSource}.calls`);

  if (!user) {
    logger.warn(`User not found for ${authSource} id: ${authID}`);
    EFuseAnalyticsUtil.incrementMetric(`3rdPartyAuth.${authSource}.failed`);

    return res.status(422).json({
      error: `No user account found linked to this ${authName} account.  Please log in or sign up!`,
      type: "warning",
      title: "Not Found"
    });
  }

  if (user.status === "Blocked") {
    logger.warn("User has been blocked", { user: user.email || user._id });
    EFuseAnalyticsUtil.incrementMetric(`3rdPartyAuth.${authSource}.failed`);

    return res.status(422).json({
      error: "Your account has been blocked",
      type: "danger",
      title: "Error"
    });
  }

  if (user.firstRun) {
    localUser.firstRun = false;
  }

  if (authSource === "discordUserId") {
    localUser.discordAccessToken = authData.discordAccessToken;
    localUser.discordTokenType = authData.discordTokenType;
    localUser.discordRefreshToken = authData.discordRefreshToken;
    localUser.discordTokenScope = authData.discordTokenScope;
    localUser.discordTokenExpiresAt = authData.discordTokenExpiresAt;
  } else if (authSource === "twitchID") {
    localUser.twitchAccessToken = authData.twitchAccessToken;
    localUser.twitchRefreshToken = authData.twitchRefreshToken;
    localUser.twitchAccessTokenExpiresAt = authData.twitchAccessTokenExpiresAt;
    localUser.twitchScope = authData.twitchScope;
    localUser.twitchTokenType = authData.twitchTokenType;
  }

  await user.save({ validateBeforeSave: false });

  const session = await tokenService.createSession(user._id, req.useragent, req.ipInfo);

  return res.json({
    ...session,
    user: WhitelistHelper.userPayload(user),
    message: "Logged in successfully!"
  });
};

module.exports.create = create;
module.exports.createFromThirdParty = createFromThirdParty;
module.exports.refreshToken = refreshToken;
module.exports.test = test;
