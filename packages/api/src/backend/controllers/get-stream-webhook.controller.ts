import { NextFunction, Request, Response } from "express";

import { WebhookType } from "../../lib/streamchat/streamchat.enum";
import { StreamChatService } from "../../lib/streamchat/streamchat.service";
import { BrazeCampaignName } from "../../lib/braze/braze.enum";
import { BrazeService } from "../../lib/braze/braze.service";
import { UserService } from "../../lib/users/user.service";
import { BaseController } from "./base.controller";

const NAME = "get-stream-webhook.controller";
export class GetStreamWebhookController extends BaseController {
  private streamchatService: StreamChatService;
  private brazeService: BrazeService;
  private userService: UserService;

  constructor() {
    super(NAME);

    this.streamchatService = new StreamChatService();
    this.brazeService = new BrazeService();
    this.userService = new UserService();
  }

  /**
   * @summary Handle GetStream webhook
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof GetStreamWebhookController
   */
  public handler = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body } = req;

      switch (body.type) {
        case WebhookType.flagged:
          await this.streamchatService.messageFlaggedWebhookHandlerAsync(body);
          break;
        case WebhookType.newMessage: {
          const senderUserId = body.user.id;
          const sender = await this.userService.findOne({ _id: body.user.id });
          const recipients = body.members.filter((member) => member.user_id !== senderUserId);

          // this allows sending multiple notifications to each member (Example: group chats)
          await Promise.all(
            recipients.map((recipient) => {
              return this.brazeService.sendCampaignMessageAsync(BrazeCampaignName.DIRECT_MESSAGE, {
                trigger_properties: {
                  sender_user_id: senderUserId,
                  message: body.message.text,
                  sender_username: sender?.username,
                  sender_profile_picture_url: sender?.profilePicture?.url
                },
                recipients: [{ external_user_id: recipient.user_id }]
              });
            })
          );
          break;
        }
        default:
          // Unhandled webhook
          break;
      }

      this.logger.info({ ...body, service: "streamchat", source: "webhook" }, "/api/__webhooks__/streamchat");

      res.send("OK");
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
