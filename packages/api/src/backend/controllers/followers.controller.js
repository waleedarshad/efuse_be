const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { isEmpty } = require("lodash");

const { Friend } = require("../models/friend");
const { User } = require("../models/User");
const { GeneralHelper } = require("../helpers/general.helper");
const NotificationsHelper = require("../helpers/notifications.helper");
const { UserHooksService } = require("../../lib/hooks/user-hooks.service");
const { HomeFeedAsyncService } = require("../../lib/home-feed/home-feed-async.service");
const { StreaksService } = require("../../lib/streaks/streaks.service");
const { BrazeCampaignName } = require("../../lib/braze/braze.enum");
const { BrazeService } = require("../../lib/braze/braze.service");

const streaksService = new StreaksService();
const brazeService = new BrazeService();
const homeFeedAsyncService = new HomeFeedAsyncService();
const userHooksService = new UserHooksService();

const isFollowed = async (req, res) => {
  if (!GeneralHelper.isValidObjectId(req.params.id)) {
    logger.info(`${req.params.id} Followee ID malformed`);
    return res.status(400).json({ error: "Invalid Followee id" });
  }
  try {
    const response = await validateUserId(req.params.id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const friend = await Friend.findOne({
      follower: req.user.id,
      followee: req.params.id
    });
    return res.json({ isFollowed: !!friend });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const isFollowsYou = async (req, res) => {
  try {
    const response = await validateUserId(req.params.id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const friend = await Friend.findOne({
      follower: req.params.id,
      followee: req.user.id
    });
    return res.json({ isFollowsYou: !!friend });
  } catch (error) {
    logger.error(error);
    res.status(500).json(error);
  }
};

const follow = async (req, res) => {
  try {
    const { user, params } = req;
    const response = await validateUserId(params.id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const friend = await Friend.findOne({
      follower: user.id,
      followee: params.id
    });
    if (!friend) {
      const newFriend = new Friend({ follower: user.id, followee: params.id });
      const createdFriend = await newFriend.save();
      try {
        await NotificationsHelper.followNotification(req.user, params.id, createdFriend);

        //get follower user to retrieve username for braze campaign
        const newUser = await User.findById(user.id);

        brazeService.sendCampaignMessageAsync(BrazeCampaignName.USER_FOLLOW, {
          trigger_properties: {
            // todo: investigate
            // @ts-ignore
            follower_username: newUser.username,
            follower_user_id: String(user.id),
            // todo: investigate
            // @ts-ignore
            follower_profile_image_url: newUser.profilePicture.url,
            friend_id: String(createdFriend._id)
          },
          recipients: [{ external_user_id: String(params.id) }]
        });

        await homeFeedAsyncService.addFolloweesFeedToFollowerOnFollowAsync(params.id, user.id);

        const doc = await createdFriend.populate("follower").execPopulate();

        // todo: investigate
        // @ts-ignore
        const newFriendDoc = newFriend._doc;
        const follower = {
          id: user.id,
          // todo: investigate
          // @ts-ignore
          name: newUser.username
        };
        const followee = {
          id: params.id
        };
        await userHooksService.newUserFollowsUser(follower, followee);
        await streaksService.checkStreak(user.id, "dailyUsage", "Follow");
        return res.json({
          isFollowed: true,
          friend: {
            _id: newFriendDoc._id,
            // todo: investigate
            // @ts-ignore
            follower: newFriend.follower._id,
            // todo: investigate
            // @ts-ignore
            followee: newFriend.followee._id,
            associatedFollower: {
              // todo: investigate
              // @ts-ignore
              ...doc._doc.follower._doc,
              isFollower: newFriend
            }
          }
        });
      } catch {
        return res.json({ isFollowed: false });
      }
    } else {
      return res.json({ isFollowed: true });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const unfollow = async (req, res) => {
  try {
    const { user, params } = req;
    const response = await validateUserId(params.id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    await Friend.findOneAndDelete({
      follower: user.id,
      followee: params.id
    });

    return res.json({
      isFollowed: false,
      unFollowedTo: params.id,
      unFollowedBy: user._id
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const countFriends = async (req, res) => {
  try {
    let { id } = req.query;
    id = id ? id : req.user.id;
    const response = await validateUserId(id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const counts = await Promise.all([
      Friend.countDocuments({ followee: id }).exec(),
      Friend.countDocuments({ follower: id }).exec()
    ]);
    return res.json({ followers: counts[0], followees: counts[1] });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const getFollowers = async (req, res) => {
  try {
    const id = GeneralHelper.getObjectId(req.params.id);
    const response = await validateUserId(id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const friends = await getFriends(req, { followee: id });
    res.json({ friends });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ err: error.message });
  }
};

const getFollowees = async (req, res) => {
  try {
    const id = GeneralHelper.getObjectId(req.params.id);
    const response = await validateUserId(id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const friends = await getFriends(req, { follower: id });
    res.json({ friends });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ err: error.message });
  }
};

const getFolloweeIds = async (req, res) => {
  try {
    const id = GeneralHelper.getObjectId(req.params.id);
    const findResult = await Friend.find({ follower: id }).select("followee -_id");

    const finalArray = findResult.map((friend) => friend.followee);
    return res.status(200).json(finalArray);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error.message });
  }
};

const getFriends = async (req, query) => {
  return new Promise(async (resolve, reject) => {
    try {
      const { page, pageSize } = req.query;
      const friends = await Friend.paginate(
        { ...query },
        {
          page,
          limit: pageSize,
          lean: true,
          sort: { createdAt: -1 }
        }
      );
      const aggregatedFriends = await Promise.all(
        friends.docs.map(async (friend) => {
          try {
            return await reaggregateFriend(friend, req.user.id);
          } catch {
            return {};
          }
        })
      );
      const results = aggregatedFriends.filter((friend) => !isEmpty(friend));
      friends.docs = results;
      return resolve(friends);
    } catch (error) {
      return reject(error);
    }
  });
};

// Not in use anywhere
const mentionAggregate = (req, res) => {
  try {
    const id = GeneralHelper.getObjectId(req.user.id);
    const aggregate = Friend.aggregate([
      {
        $match: { followee: id }
      },
      {
        $lookup: {
          from: "users",
          let: { followerId: "$follower" },
          pipeline: [
            {
              $project: { name: { $concat: ["$firstName", " ", "$lastName"] } }
            },
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$_id", "$$followerId"] }]
                },
                name: { $regex: new RegExp(req.query.query, "i") }
              }
            }
          ],
          as: "user"
        }
      },
      { $unwind: "$user" }
    ]);
    return aggregate;
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error.message });
  }
};

const reaggregateFriend = async (friend, userId) => {
  return new Promise(async (resolve, reject) => {
    try {
      friend.associatedFollower = await User.findById(friend.follower)
        .lean()
        .select("name username verified profilePicture roles");

      // Make sure associated followers exists
      if (!friend.associatedFollower) {
        return resolve({});
      }

      const isFollower = await Friend.findOne({
        followee: friend.associatedFollower._id,
        follower: userId
      }).lean();

      if (isFollower) friend.associatedFollower.isFollower = isFollower;

      friend.associatedFollowee = await User.findById(friend.followee)
        .lean()
        .select("name username verified profilePicture roles");

      if (friend.associatedFollowee) {
        const isFollowee = await Friend.findOne({
          followee: friend.associatedFollowee._id,
          follower: userId
        }).lean();

        if (isFollowee) friend.associatedFollowee.isFollowee = isFollowee;
      } else {
        friend.associatedFollowee = {};
      }

      return resolve(friend);
    } catch (error) {
      logger.error(error);
      return reject(error);
    }
  });
};

const validateUserId = async (id) => {
  if (!GeneralHelper.isValidObjectId(id)) {
    logger.error(`${id} UserId malformed`);
    return { error: true, statusCode: 400, message: "UserId malformed" };
  }
  const userExists = await User.exists({ _id: id });
  if (!userExists) {
    logger.error(`${id} User not found`);
    return { error: true, statusCode: 422, message: "User not found" };
  }
  return { error: false };
};

module.exports.countFriends = countFriends;
module.exports.follow = follow;
module.exports.getFolloweeIds = getFolloweeIds;
module.exports.getFollowees = getFollowees;
module.exports.getFollowers = getFollowers;
module.exports.getFriends = getFriends;
module.exports.isFollowed = isFollowed;
module.exports.isFollowsYou = isFollowsYou;
module.exports.mentionAggregate = mentionAggregate;
module.exports.reaggregateFriend = reaggregateFriend;
module.exports.unfollow = unfollow;
module.exports.validateUserId = validateUserId;
