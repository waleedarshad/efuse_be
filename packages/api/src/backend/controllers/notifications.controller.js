const { Notification } = require("../models/Notification");

const index = async (req, res) => {
  const logger = req.log.child({ func: "index" });
  try {
    const { user, query } = req;
    const { page, pageSize } = query;
    const notifications = await Notification.paginate(
      { receiver: user.id },
      {
        page: page,
        limit: pageSize,
        sort: { createdAt: -1 }
      }
    );
    return res.json({ notifications });
  } catch (error) {
    logger.error(error, "Error getting notifications");
    return res.status(500).json({ error });
  }
};

const getUnreadCount = async (req, res) => {
  const logger = req.log.child({ func: "getUnreadCount" });
  try {
    const count = await Notification.countDocuments({
      receiver: req.user.id,
      read: false,
      readCount: false
    });
    return res.json({ newNotifications: count });
  } catch (error) {
    logger.error(error, "Error getting notifications count");
    return res.status(500).json({ error });
  }
};

const markAllAsRead = async (req, res) => {
  const logger = req.log.child({ func: "markAllAsRead" });
  try {
    await Notification.updateMany({ receiver: req.user.id, read: false }, { read: true, readCount: true });
    return res.end("Notifications marked as read");
  } catch (error) {
    logger.error(error, "Error marking notifications as read");
    return res.status(500).json({ error });
  }
};

const markAsRead = async (req, res) => {
  const logger = req.log.child({ func: "markAsRead" });
  try {
    await Notification.findOneAndUpdate({ _id: req.params.notificationId }, { read: true, readCount: true });
    return res.end("Notification marked as read");
  } catch (error) {
    logger.error(error, "Error marking notifications as read");
    return res.status(500).json({ error });
  }
};

const clearUnreadCount = async (req, res) => {
  const logger = req.log.child({ func: "clearUnreadCount" });
  try {
    await Notification.updateMany({ receiver: req.user.id, readCount: false }, { readCount: true });
    return res.end("Notifications marked as read");
  } catch (error) {
    logger.error(error, "Error marking notifications as read");
    return res.status(500).json({ error });
  }
};

module.exports.clearUnreadCount = clearUnreadCount;
module.exports.getUnreadCount = getUnreadCount;
module.exports.index = index;
module.exports.markAllAsRead = markAllAsRead;
module.exports.markAsRead = markAsRead;
