const { Failure } = require("@efuse/contracts");
const _ = require("lodash");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Applicant } = require("../models/applicant");
const { Opportunity } = require("../models/Opportunity");
const { Organization } = require("../models/Organization");
const { User } = require("../models/User");

const { GeneralHelper } = require("../helpers/general.helper");
const { OpportunityValidations } = require("../validations/opportunity.validations");
const ViewsQueue = require("../../async/views");
const OpportunityLib = require("../../lib/opportunities");
const { OpportunityRankService } = require("../../lib/opportunities/opportunity-rank.service");
const { OrganizationAccessTypes } = require("../../lib/organizations/organization-access-types");
const { OrganizationACLService } = require("../../lib/organizations/organization-acl.service");
const { MongooseErrorEnum } = require("../../lib/errors.enum");
const { OpportunityGameService } = require("../../lib/opportunities/opportunity-game.service");
const { Slack } = require("../../lib/slack");
const { NODE_ENV } = require("@efuse/key-store");

const opportunityRankService = new OpportunityRankService();
const organizationACLService = new OrganizationACLService();
const opportunityGameService = new OpportunityGameService();

const doesUserHavePermission = async (authedUser, opportunityParams) => {
  if (opportunityParams.onBehalfOfOrganization === "false" && authedUser.id !== opportunityParams.user) {
    return false;
  }
  if (opportunityParams.onBehalfOfOrganization === "true") {
    const { organization } = await organizationACLService.authorizedToAccess(
      authedUser.id,
      opportunityParams.organization,
      OrganizationAccessTypes.ANY
    );

    if (!organization) {
      return false;
    }
  }

  return true;
};

const associateGame = async (opportunity_id, opportunityParams) => {
  await opportunityGameService.associateGame(opportunity_id, opportunityParams.game);
};

/**
 * @summary Get paginated collection of aggregate opportunities
 *
 * @param {Request} req
 * @param {unknown} query
 * @param {unknown} sortParam - When passed takes precedence over the default sort order
 *
 */
const opportunitiesAggregate = async (req, query = {}, sortParam = {}) => {
  return new Promise(async (resolve, reject) => {
    // todo: investigate
    // @ts-ignore
    const enableRankBasedOpportunities = await req.bulletTrain.hasFeature(
      "enable_rank_based_opportunities",

      // todo: investigate
      // @ts-ignore
      req.user?._id
    );
    const defaultSort = enableRankBasedOpportunities
      ? { "metadata.rank": -1 }
      : { external: 1, createdAt: -1, randomSeed: 1 };
    const sort = _.isEmpty(sortParam) ? defaultSort : sortParam;

    // todo: investigate
    // @ts-ignore
    const { page, pageSize } = req.query;
    try {
      // todo: investigate
      // @ts-ignore
      const opportunities = await Opportunity.paginate(query, {
        page,
        limit: Number.parseInt(pageSize, 10),
        lean: true,
        populate: [],
        sort
      });

      const formattedResponse = opportunities.docs.map(async (opportunity) => {
        // todo: investigate
        // @ts-ignore
        const result = await OpportunityLib.buildAggregate(opportunity, req.user);
        return result;
      });
      const results = await Promise.all(formattedResponse);
      opportunities.docs = _.shuffle(results);
      resolve(opportunities);
    } catch (error) {
      logger.error(error);
      reject(error);
    }
  });
};

const getOpportunities = async (req, res, query, sort = null) => {
  try {
    const opportunities = await opportunitiesAggregate(req, query, sort);

    await ViewsQueue.incrementOpportunityViews(opportunities.docs.map((opp) => opp._id));

    return opportunities;
  } catch (error) {
    logger.error(error, "error in getOpportunities");
    throw error;
  }
};

const processOpportunityBody = (params, authedUserId) => {
  const returnParams = params;
  if ("requirements" in params) {
    returnParams.requirements = returnParams.requirements.length > 0 ? JSON.parse(returnParams.requirements) : [];
  }

  returnParams.shortName = returnParams.shortName?.toLowerCase();

  // when submitting on behalf of an org, we have to set the user based on the current authorized user
  if (params.onBehalfOfOrganization && !params.user) {
    returnParams.user = authedUserId;
  } else if (!params.onBehalfOfOrganization) {
    // otherwise set the user to the passed owner
    returnParams.user = params.owner;
    delete returnParams.owner;
  }

  return returnParams;
};

const organizationValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Organization Id" };
  }

  const organizationExist = await Organization.exists(query);
  if (!organizationExist) {
    return { error: true, statusCode: 422, message: "Organization not found" };
  }
  return { error: false };
};

const opportunityValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Opportunity Id" };
  }
  const opportunityExists = await Opportunity.exists(query);
  if (!opportunityExists) {
    logger.error("Opportunity not found");
    return { error: true, statusCode: 422, message: "Opportunity not found" };
  }
  return { error: false };
};

const validateUserId = async (id) => {
  if (!GeneralHelper.isValidObjectId(id)) {
    logger.error(`${id} UserId malformed`);
    return { error: true, statusCode: 400, message: "UserId malformed" };
  }
  const userExists = await User.exists({ _id: id });
  if (!userExists) {
    logger.error(`${id} User not found`);
    return { error: true, statusCode: 422, message: "User not found" };
  }
  return { error: false };
};

const getOpportunity = async (req, query) => {
  req.query.page = 1;
  req.query.pageSize = 1;

  const opportunity = await opportunitiesAggregate(req, query);

  if (_.isEmpty(opportunity) || opportunity.docs.length === 0) {
    throw Failure.BadRequest("Opportunity not found");
  }

  return { opportunity: opportunity.docs[0] };
};

const create = async (req, res, next) => {
  try {
    const opportunityParams = processOpportunityBody(req.body, req.user._id.toString());

    if (!(await doesUserHavePermission(req.user, opportunityParams))) {
      logger.error(
        `Current user: ${req.user.id} attempting to create opportunity for ${
          opportunityParams.onBehalfOfOrganization ? "org: " : "user: "
        } ${opportunityParams.owner}`
      );
      throw Failure.Forbidden("You are not authorized to perform this action");
    }

    const { errors, isValid } = OpportunityValidations.validate(opportunityParams);
    const { image } = req.files;
    if (!isValid) {
      logger.warn(errors);
      return res.status(400).json(errors);
    }

    // Make sure shortName is unique
    const shortNameUniquenessErrorResult = await OpportunityLib.shortNameUniquenessError(opportunityParams.shortName);

    if (!shortNameUniquenessErrorResult.isValid) {
      logger.info({ shortName: opportunityParams.shortName }, "shortName has already been taken");
      return res.status(400).json({ error: shortNameUniquenessErrorResult.error });
    }

    if (opportunityParams.onBehalfOfOrganization === "false") {
      delete opportunityParams.organization;
    }

    if (opportunityParams["gameAffiliated?"] === "false") {
      delete opportunityParams.game;
    }

    if (opportunityParams.candidateQuestions) {
      opportunityParams.candidateQuestions = JSON.parse(opportunityParams.candidateQuestions) || [];
    }

    const opportunityObject = new Opportunity(
      Object.assign(opportunityParams, {
        image: GeneralHelper.buildFileObject(image),
        user: req.user.id,
        status: "hidden"
      })
    );

    const verifiedStatus = await OpportunityLib.markOpportunityAsVerified(opportunityObject, req.user);
    opportunityObject.verified.status = verifiedStatus;

    const createdOpportunity = await opportunityObject.save();

    // Associate Game
    await associateGame(createdOpportunity._id, opportunityParams);

    const opportunity = await OpportunityLib.buildAggregate(createdOpportunity.toObject(), req.user);

    try {
      await sendNewOpportunitySlackMesage(opportunity);
    } catch (error) {
      logger.error(error, error.message);
    }

    return res.json({ opportunity, message: "Opportunity created successfully" });
  } catch (error) {
    return next(error);
  }
};

const sendNewOpportunitySlackMesage = async (opportunity) => {
  if (NODE_ENV === "production") {
    const messageContent = [
      "*New Opportunity Created* :tada:",
      `Please assess and verify it at https://efuse.retool.com/apps/Company/Opportunities`,
      "",
      "*Opportunity at a Glance*",
      `*Url -* https://efuse.gg/o/${opportunity.shortName}`,
      `*Title -* "${opportunity.title}"`,
      `*Type|Subtype -* ${opportunity.opportunityType} | ${opportunity.subType}`,
      `*Created By -* @${opportunity.associatedUser.username} - https://efuse.gg/u/${opportunity.associatedUser.username}`
    ];

    if (opportunity.onBehalfOfOrganization) {
      messageContent.push(
        `*On Behalf of Org -* @${opportunity.associatedOrganization.name} - https://efuse.gg/org/${opportunity.associatedOrganization.shortName}`
      );
    }

    await Slack.sendMessage(messageContent.join("\n"), "#opportunity-feed");
  }
};

const index = async (req, res, next) => {
  try {
    let query = GeneralHelper.convertToObjectIds("opportunities", req);

    let flaggedFeature = false;

    flaggedFeature = await (res.user
      ? req.bulletTrain.hasFeature("external_opportunities", req.user._id)
      : req.bulletTrain.hasFeature("external_opportunities"));

    // By default we should not show external opportunities
    let externalOpportunityFilter = {
      $or: [{ external: { $exists: false } }, { external: false }]
    };

    if (flaggedFeature) {
      // todo: investigate
      // @ts-ignore
      externalOpportunityFilter = {};
    }

    query = {
      ...query,
      $and: [
        {
          $or: [{ publishStatus: { $in: ["open", "closed"] } }, { publishStatus: { $exists: false } }]
        },
        { $or: [{ status: "visible" }, { status: { $exists: false } }] },
        externalOpportunityFilter
      ]
    };
    const opportunities = await getOpportunities(req, res, query);
    return res.json({ opportunities });
  } catch (error) {
    // If it is a typecast Error
    if (error.message.includes(MongooseErrorEnum.castToObjectId)) {
      next(Failure.BadRequest("Query contained invalid filters", error));
    } else {
      next(error);
    }
  }
};

const organizationOpportunities = async (req, res) => {
  try {
    if (req.query.id) {
      const response = await organizationValid({ _id: req.query.id });
      if (response.error) {
        return res.status(response.statusCode).json({ message: response.message });
      }
    }

    const id = GeneralHelper.getObjectId(req.query.id);
    const query = {
      ...GeneralHelper.convertToObjectIds("opportunities", req),
      organization: id,
      $and: [
        {
          $or: [{ publishStatus: { $in: ["open", "closed"] } }, { publishStatus: { $exists: false } }]
        },
        { $or: [{ status: "visible" }, { status: { $exists: false } }] }
      ]
    };
    const opportunities = await getOpportunities(req, res, query);
    return res.json({ opportunities });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

const userOpportunities = async (req, res, next) => {
  try {
    const id = GeneralHelper.getObjectId(req.query.id);
    const response = await validateUserId(id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const query = {
      ...GeneralHelper.convertToObjectIds("opportunities", req),
      $and: [
        { user: id },
        { organization: null },
        {
          $or: [{ publishStatus: { $in: ["open", "closed"] } }, { publishStatus: { $exists: false } }]
        },
        { $or: [{ status: "visible" }, { status: { $exists: false } }] }
      ]
    };
    const opportunities = await getOpportunities(req, res, query);
    return res.json({ opportunities });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

/**
 * @deprecated Use the search functionality in the Search Service.
 */
const search = async (req, res) => {
  await Opportunity.find(req.body.filters).exec((err, opportunities) => {
    if (err) {
      logger.error(err.message);
      return res.status(500).json(err);
    }
    return res.json({ opportunities });
  });
};

const show = async (req, res, next) => {
  let id = null;

  try {
    const response = await opportunityValid({ _id: req.params.id });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }

    id = GeneralHelper.getObjectId(req.params.id);

    const opportunity = await getOpportunity(req, { _id: id });

    return res.json(opportunity);
  } catch (error) {
    next(error);
  }
};

const showBySlug = async (req, res, next) => {
  let slug = null;
  try {
    slug = req.params.slug;

    const response = await opportunityValid({ shortName: slug });

    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }

    const opportunity = await getOpportunity(req, { shortName: slug });

    return res.json(opportunity);
  } catch (error) {
    next(error);
  }
};

const getMaxMoneyIncluded = async (req, res) => {
  try {
    const opportunities = await Opportunity.find().sort({ money: -1 }).limit(1);

    return res.json({
      opportunity: opportunities.length > 0 ? opportunities[0] : {}
    });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

const getOpportunityToEdit = async (req, res) => {
  try {
    const { user } = req;
    const opportunity = await Opportunity.findById(req.params.id).lean();

    // todo: investigate
    // @ts-ignore
    if (String(opportunity.user) === String(user._id) || user.roles.includes("admin")) {
      if (opportunity) {
        // todo: investigate
        // @ts-ignore
        opportunity.associatedGame = await opportunityGameService.getAssociatedGame(opportunity._id);
      }

      return res.json({ canEdit: true, opportunity });
    }
    return res.json({ canEdit: false });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

const update = async (req, res) => {
  try {
    const opportunity = await Opportunity.findById(req.params.id);

    // todo: investigate
    // @ts-ignore
    if (opportunity.user.equals(req.user.id) || req.user.roles.includes("admin")) {
      let opportunityParams = processOpportunityBody(req.body, req.user._id.toString());
      // opportunityParams.owner = "5de96ece2675c74369af2ae3";

      // todo: investigate
      // @ts-ignore
      if (opportunity.user._id.toString() !== opportunityParams.user) {
        logger.error(
          // todo: investigate
          // @ts-ignore
          `${req.user.id} is attempting change user from ${opportunity.user._id.toString()} to ${
            opportunityParams.user
          } for opportunity ${req.params.id}`
        );
        throw Failure.Forbidden("You are not authorized to perform this action.");

        // todo: investigate
        // @ts-ignore
      } else if (opportunity.onBehalfOfOrganization.toString() !== opportunityParams.onBehalfOfOrganization) {
        logger.error(
          `${req.user.id} is attempting change ${
            // todo: investigate
            // @ts-ignore
            opportunity.onBehalfOfOrganization
              ? "org opportunity to user opportunity"
              : "user opportunity to org opportunity"
          } for opportunity ${req.params.id}`
        );
        throw Failure.Forbidden("You are not authorized to perform this action.");
      } else if (
        opportunityParams.onBehalfOfOrganization === "true" &&
        // todo: investigate
        // @ts-ignore
        opportunity.organization._id.toString() !== opportunityParams.organization
      ) {
        logger.error(
          // todo: investigate
          // @ts-ignore
          `${req.user.id} is attempting change org from ${opportunity.organization._id.toString()} to ${
            opportunityParams.organization
          } for opportunity ${req.params.id}`
        );
        throw Failure.Forbidden("You are not authorized to perform this action.");
      } else if (!(await doesUserHavePermission(req.user, opportunityParams))) {
        logger.error("currentUser is not eFuse admin, organization captain, nor owner of opportunity");
        throw Failure.Forbidden("You are not authorized to perform this action");
      }

      if (opportunityParams.candidateQuestions) {
        opportunityParams.candidateQuestions = JSON.parse(opportunityParams.candidateQuestions) || [];
      }
      opportunityParams = _.omit(opportunityParams, ["_id"]);
      opportunityParams = _.omit(opportunityParams, ["metadata"]);
      const { errors, isValid } = OpportunityValidations.validate(opportunityParams);
      const { image } = req.files;
      if (!isValid) {
        logger.warn(errors);
        return res.status(400).json(errors);
      }
      let opportunityObject = {};
      if (opportunityParams.onBehalfOfOrganization === "false") {
        delete opportunityParams.organization;
      }
      opportunityParams = _.omit(opportunityParams, ["verified"]);

      if (image !== undefined) {
        opportunityObject = Object.assign(opportunity, opportunityParams, {
          image: GeneralHelper.buildFileObject(image)
        });
      } else {
        opportunityParams = _.omit(opportunityParams, ["image"]);
        opportunityObject = Object.assign(opportunity, opportunityParams);
      }
      try {
        const updatedOpportunity = await opportunityObject.save();

        // Associate Game
        await associateGame(updatedOpportunity._id, opportunityParams);

        return res.json({
          message: "Opportunity updated successfully",
          id: updatedOpportunity._id,
          canEdit: true
        });
      } catch (error) {
        logger.warn(error);
        return res.status(400).json(error);
      }
    } else {
      return res.json({ canEdit: false });
    }
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

const owned = async (req, res) => {
  try {
    const id = GeneralHelper.getObjectId(req.user.id);
    const query = {
      ...GeneralHelper.convertToObjectIds("opportunities", req),
      user: id,
      $and: [
        {
          $or: [{ publishStatus: { $in: ["open", "closed"] } }, { publishStatus: { $exists: false } }]
        },
        {
          $or: [{ status: { $in: ["visible", "hidden"] } }, { status: { $exists: false } }]
        }
      ]
    };
    const opportunities = await getOpportunities(req, res, query, { updatedAt: -1 });
    return res.json({ opportunities });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

const applied = async (req, res) => {
  try {
    const { query, user } = req;
    const { page, pageSize } = query;
    const applicants = await Applicant.paginate(
      { user: user.id },
      {
        page,
        limit: pageSize,
        sort: { createdAt: -1 },
        populate: [],
        lean: true
      }
    );
    const { docs, ...pagination } = applicants;
    const opportunityIds = docs.map((applicant) => applicant.opportunity);
    const newQuery = {
      ...GeneralHelper.convertToObjectIds("opportunities", req),
      _id: { $in: opportunityIds }
    };
    const loadedOpportunities = await Opportunity.find(newQuery, {}, { autopopulate: false }).lean();
    const opportunities = loadedOpportunities.map(async (opportunity) => {
      const result = await OpportunityLib.buildAggregate(opportunity, user);

      return result;
    });
    const results = await Promise.all(opportunities);
    return res.json({ opportunities: { docs: results, ...pagination } });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

// Deletes a Opportunities record.
const deleteItem = async (req, res) => {
  try {
    await Opportunity.findOneAndDelete({ _id: req.params.id });
    return res.json({ success: true, id: req.params.id });
  } catch {
    return res.status(404).json({ notfound: "No record found" });
  }
};

const getAll = async (req, res) => {
  try {
    const opportunities = await Opportunity.find();

    return res.json({
      opportunities
    });
  } catch (error) {
    logger.error(error, error.message);
    return res.status(500).json({ error: error.message });
  }
};

const verify = async (req, res) => {
  try {
    const opportunity = await Opportunity.findById(req.params.id);

    if (opportunity) {
      const updatedOpportunity = await Opportunity.update(
        { _id: opportunity._id },
        {
          $set: {
            "verified.status": !opportunity.verified.status
          }
        }
      );
      opportunityRankService.setObjectRankAsync(opportunity._id);
      return res.json({
        message: "Opportunity verified successfully",

        // todo: investigate
        // @ts-ignore
        id: updatedOpportunity._id,
        canEdit: true
      });
    }

    return res.json({
      message: "Opportunity not found!",
      id: req.params._id,
      canEdit: false
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error: error.message });
  }
};

const alias = async (req, res) => {
  try {
    const opportunity = await Opportunity.findOne({
      shortName: req.params.name.toLowerCase()
    }).select("_id");

    if (opportunity === null) {
      logger.warn(`Invalid opportunity alias: ${req.params.name}`);
      return res.status(400).json({});
    }
    return res.status(200).json({ id: opportunity._id });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error: error.message });
  }
};

const statusUpdate = async (req, res) => {
  try {
    const opportunity = await Opportunity.findOne({ _id: req.params.id });
    if (opportunity) {
      const updatedOpportunity = await Opportunity.findOneAndUpdate(
        { _id: opportunity._id },
        {
          $set: {
            publishStatus: req.body.publishStatus
          }
        },
        { new: true }
      );
      opportunityRankService.setObjectRankAsync(opportunity._id);
      return res.json({
        // todo: investigate
        // @ts-ignore
        message: `Opportunity status has been successfully changed to ${updatedOpportunity.publishStatus}`,
        updatedOpportunity
      });
    }
    return res.status(404).json({ message: "Not Found" });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error: error.message });
  }
};

const promoted = async (req, res) => {
  try {
    const organizations = await Organization.find({
      "verified.status": true,
      $or: [{ publishStatus: "visible" }, { publishStatus: { $exists: false } }]
    })
      .sort({ followersCount: -1 })
      .select("_id name verified profileImage user")
      .lean();
    const promotedOpportunities = await Promise.all(
      organizations.map(async (organization) => {
        const opportunities = await Opportunity.find({
          publishStatus: "open",
          status: "visible",
          organization: organization._id
        })
          .sort({ "metadata.views": -1 })
          .limit(4)
          .lean();

        const aggregatedOpportunities = await Promise.all(
          opportunities.map(async (opportunity) => {
            const returnOpportunity = opportunity;

            // todo: investigate
            // @ts-ignore
            returnOpportunity.associatedUser = await User.findOne({
              _id: opportunity.user
            })
              .select("_id name username profilePicture")
              .lean();

            // todo: investigate
            // @ts-ignore
            returnOpportunity.associatedOrganization = organization;

            return returnOpportunity;
          })
        );
        return aggregatedOpportunities;
      })
    );
    const buildResponse = _.flatten(promotedOpportunities);
    return res.json(buildResponse);
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error: error.message });
  }
};

const recentOpportunities = async (req, res, next) => {
  try {
    const opportunities = await OpportunityLib.recentOpportunities(req.user);
    return res.json(opportunities);
  } catch (error) {
    return next(error);
  }
};

const validateShortName = async (req, res, next) => {
  try {
    const { shortName } = req.body;

    // Validate format
    const shortNameFormatErrorResult = OpportunityLib.shortNameFormatError(shortName);
    if (!shortNameFormatErrorResult.isValid) {
      return res.json({ isFormatValid: false, error: shortNameFormatErrorResult.error });
    }

    // Validate uniqueness
    const shortNameUniquenessErrorResult = await OpportunityLib.shortNameUniquenessError(shortName);
    if (!shortNameUniquenessErrorResult.isValid) {
      return res.json({ isUnique: false, error: shortNameUniquenessErrorResult.error });
    }

    // if it reaches here then it is valid
    return res.json({ isFormatValid: true, isUnique: true });
  } catch (error) {
    return next(error);
  }
};

module.exports.alias = alias;
module.exports.applied = applied;
module.exports.associateGame = associateGame;
module.exports.create = create;
module.exports.deleteItem = deleteItem;
module.exports.doesUserHavePermission = doesUserHavePermission;
module.exports.getAll = getAll;
module.exports.getMaxMoneyIncluded = getMaxMoneyIncluded;
module.exports.getOpportunities = getOpportunities;
module.exports.getOpportunity = getOpportunity;
module.exports.getOpportunityToEdit = getOpportunityToEdit;
module.exports.index = index;
module.exports.opportunitiesAggregate = opportunitiesAggregate;
module.exports.opportunityValid = opportunityValid;
module.exports.organizationOpportunities = organizationOpportunities;
module.exports.organizationValid = organizationValid;
module.exports.owned = owned;
module.exports.processOpportunityBody = processOpportunityBody;
module.exports.promoted = promoted;
module.exports.recentOpportunities = recentOpportunities;
module.exports.search = search;
module.exports.sendNewOpportunitySlackMesage = sendNewOpportunitySlackMesage;
module.exports.show = show;
module.exports.showBySlug = showBySlug;
module.exports.statusUpdate = statusUpdate;
module.exports.update = update;
module.exports.userOpportunities = userOpportunities;
module.exports.validateShortName = validateShortName;
module.exports.validateUserId = validateUserId;
module.exports.verify = verify;
