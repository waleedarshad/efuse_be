const { OwnerKind } = require("@efuse/entities");
const {
  DISCORD_API_URL,
  DISCORD_CLIENT_ID,
  DISCORD_CLIENT_SECRET,
  DISCORD_LOGIN_REDIRECT_URL,
  DISCORD_SIGNUP_REDIRECT_URL,
  TWITCH_CLIENT_ID,
  TWITCH_LOGIN_REDIRECT_URL,
  TWITCH_SIGNUP_REDIRECT_URL,
  TWITCH_SECRET
} = require("@efuse/key-store");
const axios = require("axios");
const fetch = require("node-fetch");
const FormData = require("form-data");

const { User } = require("../models/User");
const validateRecaptcha3 = require("../middlewares/validate-recaptcha3");
const { OrganizationInvitation } = require("../models/OrganizationInvitation");
const { Member } = require("../models/member");

const { GeneralHelper } = require("../helpers/general.helper");
const { UsernameValidationsService } = require("../validations/username.validations");
const { EmailValidationService } = require("../validations/email.validations");
const { setPassword, sendEmailVerificationEmail } = require("../../lib/users");
const { RedisHelper } = require("../helpers/redis.helper");
const SessionsController = require("./sessions.controller");
const { createStripeCustomerAsync } = require("../../lib/stripe");
const { setUserChatTokenAsync } = require("../../lib/messaging/streamchat");
const TwitchHandlerLib = require("../../lib/creatortools/twitchHandler");
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");
const { UserService } = require("../../lib/users/user.service");
const { MemberBusinessRulesService } = require("../../lib/member/member-business-rules.service");
const { PathwayService } = require("../../lib/pathway/pathway.service");
const { UserValidationService } = require("../../lib/users/user-validation.service");
const { InviteCodeService } = require("../../lib/invite-code/invite-code.service");
const { InviteCodeType } = require("@efuse/entities");

const { Failure } = require("@efuse/contracts");
const { AuthHelper } = require("../helpers/auth.helper");
const authHelper = new AuthHelper();
const $userValidationService = new UserValidationService();

const create = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("registrations_controller.create.call");
  let logger = req.log.child({ func: "create" });

  try {
    logger.info({ msg: "Starting registration process" });

    const userParams = req.body.user;
    if (userParams === undefined) {
      const error = { error: "Empty Request Body" };
      logger.error(error);

      return res.status(403).json(error);
    }
    userParams.email = userParams.email.trim();

    // confirm that the platform is set
    let { platform } = req.body;
    platform = platform || userParams.platform;

    if (!platform) {
      logger.error("Platform is not defined.");

      return res.sendStatus(403);
    }

    // create a new logger with extra information, this is used in DataDog for alerts.
    logger = logger.child({ email: userParams.email, platform, userParams });

    // Only validate reCAPTCHA for logins on Production Web request
    const captchaDisabled = await req.bulletTrain.hasFeature("killswitch:global_disable_captcha");
    const isProduction = process.env.NODE_ENV === "production";
    if (isProduction && !captchaDisabled && platform !== "mobile") {
      if (userParams.captchaToken) {
        const isCaptchaVerified = await validateRecaptcha3(userParams.captchaToken);

        if (isCaptchaVerified) {
          logger.info({ msg: "Recaptcha successfully validated" });
        } else {
          logger.error({
            msg: "Recaptcha is invalid or score is too low"
          });

          return res.status(403).json({ err: "Recaptcha is invalid" });
        }
      } else {
        logger.error({
          msg: "Recaptcha token was not found."
        });

        return res.status(403).json({ err: "Recaptcha token was invalid." });
      }
    }

    // validate email
    const { valid: emailValid, error: emailError } = await $userValidationService.validateEmail(userParams.email);

    if (!emailValid) {
      throw Failure.BadRequest(emailError);
    }

    // Make sure it is a valid username
    const { valid, error } = await $userValidationService.validateUsername(userParams.username);

    if (!valid) {
      throw Failure.BadRequest(error);
    }

    let nameToBeStored = $userValidationService.validateName(userParams.name);
    $userValidationService.validateDateOfBirth(userParams.dateOfBirth);

    // make sure password is valid
    const validationError = authHelper.validatePassword(userParams.password);
    if (validationError) {
      throw Failure.BadRequest(validationError.password, validationError);
    }

    if (userParams.useAlias) {
      const us = new UserService();
      const userAlias = us.createUserAlias();
      nameToBeStored = userAlias.name;
    }

    const username = userParams.username.trim();

    const newUser = new User({
      email: userParams.email.toLowerCase(),
      name: nameToBeStored,
      username,
      dateOfBirth: userParams.dateOfBirth,
      address: userParams.location,
      userSignupDevice: userParams.userSignupDevice || "platform n/a",
      traits: userParams.traits,
      motivations: userParams.motivations,
      referral: userParams.referral,
      userAgent: userParams.userAgent,
      referralUrl: userParams.referralUrl,
      signupUrl: userParams.signupUrl,
      weeklyNewsLetter: userParams.weeklyNewsLetter
    });

    const errors = {};

    // If pathway is selected
    if (userParams.pathway) {
      // Make sure it is a valid ID
      if (!GeneralHelper.isValidObjectId(userParams.pathway)) {
        errors.pathway = "Invalid ID";
        return res.status(400).json(errors);
      }

      // Make sure pathway exists in our DB
      const pathwayService = new PathwayService();
      const pathway = await pathwayService.findOne({ _id: userParams.pathway, active: true });

      if (!pathway) {
        errors.pathway = "Pathway not found";
        return res.status(422).json(errors);
      }

      newUser.pathway = userParams.pathway;
    }

    if (userParams.discordUserId) {
      logger.info({
        msg: "User signing up with discord.  Grabbing results from redis"
      });
      const discordDataStr = await RedisHelper.instance().getAsync(`discordSignup:${userParams.discordUserId}`);

      // todo: investigate
      // @ts-ignore
      const discordData = JSON.parse(discordDataStr);

      logger.info({ msg: "Received discordsData from redis", discordData });

      newUser.discordUserId = userParams.discordUserId;
      newUser.discordUserName = discordData.discordUserName;
      newUser.discordVerified = true;
      newUser.discordAccessToken = discordData.discordAccessToken;
      newUser.discordTokenType = discordData.discordTokenType;
      newUser.discordRefreshToken = discordData.discordRefreshToken;
      newUser.discordTokenScope = discordData.discordTokenScope;
      newUser.discordTokenExpiresAt = discordData.discordTokenExpiresAt;
      newUser.signupSocial = "discord";
    }

    let didSignupWithTwitch = false;
    if (userParams.twitchID) {
      didSignupWithTwitch = true;
      logger.info({
        msg: "User signing up with twitch.  Grabbing results from redis"
      });

      const twitchDataStr = await RedisHelper.instance().getAsync(`twitchSignup:${userParams.twitchID}`);

      // todo: investigate
      // @ts-ignore
      const twitchData = JSON.parse(twitchDataStr);

      logger.info({ msg: "Received twitchDataStr from redis", twitchData });

      newUser.twitchVerified = true;
      newUser.twitchID = userParams.twitchID;
      newUser.twitchUserName = twitchData.twitchUserName;
      newUser.twitchDisplayName = twitchData.twitchDisplayName;
      newUser.twitchProfileImage = twitchData.twitchProfileImage;
      newUser.twitchAccessToken = twitchData.twitchAccessToken;
      newUser.twitchRefreshToken = twitchData.twitchRefreshToken;
      newUser.twitchAccessTokenExpiresAt = twitchData.twitchAccessTokenExpiresAt;
      newUser.twitchScope = twitchData.twitchScope;
      newUser.twitchTokenType = twitchData.twitchTokenType;
      newUser.signupSocial = "twitch";
    }

    const createdUser = await newUser.save({ validateBeforeSave: false });
    try {
      // todo: investigate
      // @ts-ignore
      await setPassword(createdUser._id, userParams.password);
    } catch (error) {
      logger.error({
        msg: "Error setting the password.  Removing user",
        error
      });
      await User.findOneAndRemove({ _id: createdUser._id }).exec();

      return res.status(400).json({ err: error.message });
    }

    const invitedUser = await OrganizationInvitation.findOne({
      inviteeEmail: createdUser.email
    });

    if (invitedUser) {
      const orgId = GeneralHelper.getObjectId(invitedUser.organization);
      const userId = GeneralHelper.getObjectId(createdUser._id);

      await new Member({
        user: userId,
        organization: orgId,
        createdAt: Date.now()
      }).save();
    }

    const inviteCodeService = new InviteCodeService();
    await inviteCodeService.create({
      type: InviteCodeType.USER,
      user: createdUser._id
    });

    const userObjectId = GeneralHelper.getObjectId(createdUser._id);

    await createdUser.save({ validateBeforeSave: false });

    if (didSignupWithTwitch) {
      await TwitchHandlerLib.subscribeToStreamChangeWebhookAsync(createdUser._id, OwnerKind.USER);
    }

    // Create new Payment Profile to store Stripe Customer ID
    logger.info({ msg: "Creating Stripe Payment Profile" });
    await createStripeCustomerAsync(createdUser.email, `${createdUser.name}`, userObjectId.toString());

    // Kick off async user chat token async job
    await setUserChatTokenAsync(createdUser._id);

    // Get feature flag for sending email verification on signup
    const hasFeature = await req.bulletTrain.hasFeature("send_email_verification_on_signup");

    // Only send email if feature flag is enabled
    if (hasFeature) {
      logger.info({ msg: "Sending Verification Email" });
      await sendEmailVerificationEmail(createdUser.email);
    } else {
      logger.info({
        msg: "Email verification not sent - disabled by feature flag"
      });
    }

    logger.info("User saved successfully");
    EFuseAnalyticsUtil.incrementMetric("registrations_controller.create.success");

    // Taken from `validate-auth.js
    req.dbUser = createdUser;
    req.logFunc = (msg) => {
      logger.info(`[${userParams.email}] /auth/login ${msg}`);
    };
    req.invalidLoginError = { error: "Invalid Email or Password" };
    req.email = userParams.email;
    req.password = userParams.password;
    req.captchaToken = userParams.captchaToken;
    req.platform = userParams.platform;

    // Create user a member of the organization, when organization is passed in body
    if (userParams.organization) {
      logger.info("Adding user to async job for making member of organization");
      const memberBusinessRulesService = new MemberBusinessRulesService();
      await memberBusinessRulesService.createMemberOnSignupAsync(userParams.organization, createdUser._id);
    }

    return SessionsController.create(req, res);
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("registrations_controller.create.failure");

    const userParams = req.body.user || {};
    logger.error({ error, userParams });

    return res.status(400).json({ err: error.message });
  }
};

const validateEmailDeprecated = async (req, res) => {
  const { email } = req.params;
  const { invalidFormat, isValid } = await EmailValidationService.validateEmailDeprecated(email);
  if (invalidFormat) {
    return res.json({ invalidFormat });
  }
  if (!isValid) {
    return res.json({ valid: isValid });
  }
  return res.json({ valid: true });
};

const validateEmail = async (req, res) => {
  const userValidationService = new UserValidationService();

  const { email } = req.graphql ? req.body.input : req.body;
  const { valid, error } = await userValidationService.validateEmail(email);
  return res.json({ valid, error });
};

const validateUsernameDeprecated = async (req, res) => {
  const { username } = req.params;
  const { invalidUsername, isValid } = await UsernameValidationsService.validateUsernameDeprecated(username);

  if (invalidUsername) {
    return res.json({ invalidUsername });
  }

  if (!isValid) {
    return res.json({ valid: isValid });
  }

  return res.json({ valid: true });
};

const validateUsername = async (req, res, next) => {
  try {
    const userValidationService = new UserValidationService();
    const { username } = req.body;

    const { valid, error } = await userValidationService.validateUsername(username);

    res.json({ valid, error });
  } catch (error) {
    next(error);
  }
};

const discordSignup = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("registrations_controller.discordSignup.call");
  const logger = req.log;

  try {
    logger.info("Starting Discord Signup");
    const data = new FormData();
    data.append("client_id", DISCORD_CLIENT_ID);
    data.append("client_secret", DISCORD_CLIENT_SECRET);
    data.append("grant_type", "authorization_code");
    data.append("redirect_uri", req.body.isLogin ? DISCORD_LOGIN_REDIRECT_URL : DISCORD_SIGNUP_REDIRECT_URL);
    data.append("scope", "identify email guilds");
    data.append("code", req.body.code);

    // todo: investigate
    // @ts-ignore
    const response = await fetch(`${DISCORD_API_URL}oauth2/token`, {
      method: "POST",
      body: data
    });

    const postResponse = await response.json();

    logger.info({ msg: "Discord oauth2/token response", postResponse });

    const expsIn = postResponse.expires_in || 0;
    const discordData = {
      discordAccessToken: postResponse.access_token,
      discordTokenType: postResponse.token_type,
      discordRefreshToken: postResponse.refresh_token,
      discordTokenScope: postResponse.scope,
      discordTokenExpiresAt: new Date(Date.now() + Number(expsIn) * 1000)
    };

    // todo: investigate
    // @ts-ignore
    const discordUserResp = await axios.get(`${DISCORD_API_URL}users/@me`, {
      headers: { Authorization: `Bearer ${postResponse.access_token}` }
    });

    logger.info({
      msg: "Discord users/@me response",
      data: discordUserResp.data
    });

    discordData.discordVerified = true;
    discordData.discordUserId = discordUserResp.data.id;
    discordData.discordUserName = discordUserResp.data.username;
    discordData.email = discordUserResp.data.email;

    const authUser = await User.findOne({
      discordUserId: discordData.discordUserId
    }).select("+status");

    if (req.body.isLogin || authUser) {
      logger.info({ msg: "User found for discordUserId.  Logging user in." });
      return SessionsController.createFromThirdParty(
        req,
        res,
        discordData.discordUserId,
        "discordUserId",
        discordData,
        "Discord",
        authUser
      );
    }

    logger.info({
      msg: "No user found with discord.  Adding discordData session to redis",
      discordData
    });

    await RedisHelper.instance().setAsync(`discordSignup:${discordData.discordUserId}`, JSON.stringify(discordData));
    EFuseAnalyticsUtil.incrementMetric("registrations_controller.discordSignup.success");

    return res.json({
      email: discordData.email,
      discordUserName: discordData.discordUserName,
      discordUserId: discordData.discordUserId
    });
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("registrations_controller.discordSignup.failure");
    logger.error({
      msg: "Failed to authenticate the user with Discord",
      errorMsg: error.message,
      stack: error.stack
    });

    return res.status(500).json({ error: "Failed to authenticate Discord account" });
  }
};

const twitchSignup = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("registrations_controller.twitchSignup.call");

  const logger = req.log;
  logger.info("Beginning twitch signup Oauth flow");

  try {
    // todo: investigate
    // @ts-ignore
    const response = await axios.post(
      `https://id.twitch.tv/oauth2/token?client_id=${TWITCH_CLIENT_ID}&client_secret=${TWITCH_SECRET}&code=${
        req.body.code
      }&grant_type=authorization_code&redirect_uri=${
        req.body.isLogin ? TWITCH_LOGIN_REDIRECT_URL : TWITCH_SIGNUP_REDIRECT_URL
      }`
    );
    logger.info({ msg: "Twitch OAuth response", response: response.data });

    const { access_token, refresh_token, expires_in, scope, token_type } = response.data;
    const tokenExpiresAt = new Date(Date.now() + Number(expires_in) * 1000);

    // todo: investigate
    // @ts-ignore
    const userResponse = await axios.get("https://api.twitch.tv/helix/users", {
      headers: {
        Authorization: `Bearer ${access_token}`,
        "Client-ID": TWITCH_CLIENT_ID
      }
    });
    const twitchUserData = userResponse.data.data[0];

    logger.info({
      msg: "Request made to retrieve /helix/users twitch information",
      twitchUserData
    });

    const twitchData = {
      twitchVerified: true,
      twitchID: twitchUserData.id,
      email: twitchUserData.email,
      twitchUserName: twitchUserData.login,
      twitchDisplayName: twitchUserData.display_name,
      twitchProfileImage: twitchUserData.profile_image_url,
      twitchAccessToken: access_token,
      twitchRefreshToken: refresh_token,
      twitchAccessTokenExpiresAt: tokenExpiresAt,
      twitchScope: scope,
      twitchTokenType: token_type
    };

    const authUser = await User.findOne({
      twitchID: twitchData.twitchID
    }).select("+status");

    if (req.body.isLogin || authUser) {
      logger.info({ msg: "User found for twitch ID.  Logging them in." });
      return SessionsController.createFromThirdParty(
        req,
        res,
        twitchData.twitchID,
        "twitchID",
        twitchData,
        "Twitch",
        authUser
      );
    }

    logger.info({
      msg: "User not found with twitch ID.  Setting data in redis to retrieve on signup.",
      twitchData
    });

    await RedisHelper.instance().setAsync(`twitchSignup:${twitchData.twitchID}`, JSON.stringify(twitchData));
    EFuseAnalyticsUtil.incrementMetric("registrations_controller.twitchSignup.success");

    return res.json({
      email: twitchData.email,
      twitchUserName: twitchData.twitchUserName,
      twitchID: twitchData.twitchID
    });
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("registrations_controller.twitchSignup.failure");
    logger.error({ msg: "Failed to authenticate the user with Twitch", error });

    return res.status(500).json({ error: "Failed to authenticate Twitch account" });
  }
};

module.exports.create = create;
module.exports.discordSignup = discordSignup;
module.exports.twitchSignup = twitchSignup;
module.exports.validateEmail = validateEmail;
module.exports.validateEmailDeprecated = validateEmailDeprecated;
module.exports.validateUsername = validateUsername;
module.exports.validateUsernameDeprecated = validateUsernameDeprecated;
