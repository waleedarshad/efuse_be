const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { OrganizationFollower } = require("../models/organization-follower");
const { Organization } = require("../models/Organization");
const { GeneralHelper } = require("../helpers/general.helper");

const create = async (req, res) => {
  const { organizationId } = req.params;
  const { user } = req;
  try {
    const organization = await Organization.findOne({ _id: organizationId });
    if (!organization) {
      return res.status(422).json({ error: "Organization not found!" });
    }
    const organizationFollower = await OrganizationFollower.findOne({
      followee: organizationId,
      follower: user.id
    });
    if (organizationFollower) {
      return res.json({
        isFollowed: false,
        followObject: organizationFollower
      });
    }
    const newFollower = await new OrganizationFollower({
      follower: user.id,
      followee: organizationId
    }).save();

    // todo: investigate
    // @ts-ignore
    organization.followersCount += 1;
    await organization.save();
    return res.json({ isFollowed: true, followObject: newFollower });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const deleteItem = async (req, res) => {
  const { organizationId } = req.params;
  try {
    const organization = await Organization.findOne({ _id: organizationId });
    if (!organization) {
      return res.status(422).json({ error: "Organization not found!" });
    }
    const organizationFollower = await OrganizationFollower.findOne({
      followee: organizationId,
      follower: req.user.id
    });
    if (!organizationFollower) {
      return res.end("Unfollowed");
    }
    await organizationFollower.remove();

    // todo: investigate
    // @ts-ignore
    organization.followersCount -= 1;
    await organization.save();
    return res.end("Unfollowed");
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const index = async (req, res) => {
  try {
    const { organizationId } = req.params;
    const { page, pageSize } = req.query;
    const response = await organizationValid({ _id: organizationId });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const followers = await OrganizationFollower.paginate(
      { followee: organizationId },
      { page: page, limit: pageSize, sort: { createdAt: -1 } }
    );

    return res.json({ followers });
  } catch (err) {
    logger.error(err);
    return res.status(500).json({ err });
  }
};

const organizationValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Organization Id" };
  }

  const organizationExist = await Organization.exists(query);
  if (!organizationExist) {
    return { error: true, statusCode: 422, message: "Organization not found" };
  }
  return { error: false };
};

module.exports.create = create;
module.exports.deleteItem = deleteItem;
module.exports.index = index;
module.exports.organizationValid = organizationValid;
