import { NextFunction, Request, Response } from "express";
import { Failure } from "@efuse/contracts";
import { OwnerKind, PlatformEnum } from "@efuse/entities";

import { AuthHelper } from "../helpers/auth.helper";
import { AuthService } from "../../lib/auth/auth.service";
import { BaseController } from "./base.controller";
import {
  createStripeCustomerCard,
  deleteStripeCustomerCard,
  getStripeCustomerCards,
  getStripeSetupIntent
} from "../../lib/stripe";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { GamePlatformsService } from "../../lib/game_platforms/game-platforms.service";
import { getOrders } from "../../lib/orders";

import { IPathway } from "../interfaces/pathway";
import { IStripeError } from "../interfaces/stripe";
import { IUser, IComponentLayout, DiscordHooks } from "../interfaces/user";
import { OrganizationsService } from "../../lib/organizations/organizations.service";
import { PathwayService } from "../../lib/pathway/pathway.service";
import { Slack } from "../../lib/slack";
import { UserBanningService } from "../../lib/users/user-banning.service";
import { UserHelperService } from "../../lib/users/user-helper.service";
import { UserService } from "../../lib/users/user.service";
import { YoutubeService } from "../../lib/youtube/youtube.service";
import { SessionService } from "../../lib/session/session.service";
import WhitelistHelper from "../helpers/whitelist_helper";
import { StreamChatService } from "../../lib/streamchat/streamchat.service";

const NAME = "users.controller";

export class UsersController extends BaseController {
  private authHelper: AuthHelper;
  private authService: AuthService;
  private gamePlatformService: GamePlatformsService;
  private organizationService: OrganizationsService;
  private pathwayService: PathwayService;
  private userBanningService: UserBanningService;
  private userHelperService: UserHelperService;
  private userService: UserService;
  private youtubeService: YoutubeService;
  private streamChatService: StreamChatService;
  private sessionService: SessionService;

  constructor() {
    super(NAME);

    this.authHelper = new AuthHelper();
    this.authService = new AuthService();
    this.gamePlatformService = new GamePlatformsService();
    this.organizationService = new OrganizationsService();
    this.pathwayService = new PathwayService();
    this.userBanningService = new UserBanningService();
    this.userHelperService = new UserHelperService();
    this.userService = new UserService();
    this.youtubeService = new YoutubeService();
    this.streamChatService = new StreamChatService();
    this.sessionService = new SessionService();
  }

  /**
   * @summary Add discord server
   *
   * @param body.discordServer
   *
   * @memberof UsersController
   */
  public addDiscordWebhook = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { name?: string; url?: string }; user?: IUser };

      if (!body || !body.name || !body.url) {
        throw Failure.BadRequest("name and url are required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const userObj = await this.userService.findOne({ _id: incomingUser._id });

      if (!userObj) {
        throw Failure.BadRequest("unable to find user");
      }

      const { name, url } = body;

      if (!userObj?.discordWebhooks) {
        userObj.discordWebhooks = [{ name, url }];
      } else {
        userObj.discordWebhooks.push({ name, url });
      }

      await userObj.save({ validateBeforeSave: false });

      res.json({ user: userObj });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use graphql endpoint
   * @summary Add game platform
   *
   * @param body.platform
   * @param body.displayName
   *
   * @return {GamePlatform}
   * @memberof UsersController
   */
  public addGamePlatform = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as {
        body?: { displayName?: string; platform?: PlatformEnum };
        user?: IUser;
      };

      if (!body || !body.displayName || !body.platform) {
        throw Failure.BadRequest("name and platforms are required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { displayName, platform } = body;
      const gamePlatform = await this.gamePlatformService.addPlatform(incomingUser._id, platform, displayName);

      res.json({ gamePlatform });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Get userId by alias
   *
   * @param userId
   *
   * @memberof UsersController
   */
  public alias = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = req as { params?: { name?: string } };

      if (!params || !params.name) {
        throw Failure.BadRequest("name is required");
      }

      const { name } = params;
      const slug = name.toLowerCase();
      const response = await this.userHelperService.alias(slug);

      res.json(response);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public ban = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = req as { params?: { username?: string } };

      if (!params || !params.username) {
        throw Failure.BadRequest("Missing username");
      }

      const { username } = params;
      await this.userBanningService.ban(username);

      res.sendStatus(200);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public blockUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user: incomingUser } = req as { params?: { id?: string }; user?: IUser };

      if (!params || !params.id) {
        throw Failure.BadRequest("params are missing");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { id } = params;
      const user = await this.userHelperService.blockUser(id, incomingUser);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public blockedUsers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const blockedUsers = await this.userHelperService.blockedUsers(incomingUser._id);

      res.json(blockedUsers);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Create payments
   *
   * @returns Stripe response
   */
  public createPayment = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.createPayment.call`);

      const { body, user: incomingUser } = req as { body?: { token?: string }; user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      if (!body || !body.token) {
        throw Failure.BadRequest("Stripe token is required");
      }

      const { token } = body;

      const stripeResponse = await createStripeCustomerCard(incomingUser.id, token);

      EFuseAnalyticsUtil.incrementMetric(`users.controller.createPayment.success`);

      res.send(stripeResponse);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.createPayment.failure`);

      this.handleStripeError(error, next);
    }
  };

  public deleteAccount = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { password?: string }; user?: IUser };

      if (!body || !body.password) {
        throw Failure.BadRequest("Password is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      // verify password
      const { password } = body;
      const isMatched = await this.authService.matchPassword(incomingUser._id, password);

      /**
       * When password is not matched. Sending a generic message, so that if
       * it is a hacker we don't inform about password not being matched.
       */
      if (!isMatched) {
        throw Failure.BadRequest("Something went wrong while deleting account");
      }

      // if it reached here then password is matched go ahead & delete user
      await this.userService.softDelete(incomingUser._id);

      await Promise.all([
        // revoke session
        this.sessionService.revokeSessionAsync(incomingUser._id),

        // slack notification
        Slack.sendMessage(`The user with email account *${incomingUser.email}* is deleted.`, "#privacy")
      ]);

      res.json({ success: true });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated use graphql endpoint
   * @summary Delete game platform
   *
   * @param body.platformId
   *
   * @return {GamePlatform, deleted: boolean}
   * @memberof UsersController
   */
  public deleteGamePlatform = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user: incomingUser } = req as { params?: { platformId?: string }; user?: IUser };

      if (!params || !params.platformId) {
        throw Failure.BadRequest("platformId is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { platformId } = params;
      const gamePlatform = await this.gamePlatformService.deleteGamePlatform(incomingUser._id, platformId);

      res.json({ gamePlatform, deleted: true });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary detach Payment Method
   *
   * @returns Stripe response
   */
  public detachPaymentMethod = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.detachPaymentMethod.call`);

      const { paymentMethodId } = req.params;

      if (!paymentMethodId) {
        throw Failure.BadRequest("payment id is required");
      }

      const stripeResponse = await deleteStripeCustomerCard(paymentMethodId);

      EFuseAnalyticsUtil.incrementMetric(`users.controller.detachPaymentMethod.success`);

      res.send(stripeResponse);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getPayments.failure`);

      this.handleStripeError(error, next);
    }
  };

  public discordAuth = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { code?: string }; user?: IUser };

      if (!body || !body.code) {
        throw Failure.BadRequest("Discord code is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { code } = body;
      const user = await this.userHelperService.discordAuth(code, incomingUser._id);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public discordServer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { discordServer?: string }; user?: IUser };

      if (!body || !body.discordServer) {
        throw Failure.BadRequest("discordServer is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user: IUser | null = await this.userService.findOne({ _id: incomingUser._id });

      if (!user) {
        throw Failure.BadRequest("Unable to find user");
      }

      const { discordServer } = body;
      user.discordServer = discordServer;

      const updatedUser = await user.save({ validateBeforeSave: false });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public facebookAuth = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { access_token?: string }; user?: IUser };

      if (!body || !body.access_token) {
        throw Failure.BadRequest("access_token is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { access_token: accessToken } = body;
      const user = await this.userHelperService.facebookAuth(accessToken, incomingUser._id);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Generate reset password link & send email to user
   *
   * @param body.email
   *
   * @memberof UsersController
   */
  public generateLinkAndSendResetPasswordEmail = async (
    req: Request,
    res: Response,
    next: NextFunction
  ): Promise<void> => {
    try {
      const { body } = req as { body?: { email?: string } };

      if (!body || !body.email) {
        throw Failure.BadRequest("email is required");
      }

      const { email } = body;
      await this.userHelperService.generateLinkAndSendResetPasswordEmail(email);

      res.json({
        emailSent: true,
        message: "Reset password instructions have been successfully sent to your email address.",
        flashType: "success"
      });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Get all orders
   *
   * @returns orders array
   */
  public getAllOrders = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getOrders.call`);

      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      // get all orders belonging to user
      const orders = await getOrders(incomingUser.id);

      EFuseAnalyticsUtil.incrementMetric(`users.controller.getOrders.success`);

      res.send({ orders });
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getOrders.failure`);
      this.handleStripeError(error, next);
    }
  };

  public getCurrentUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user = await this.userHelperService.getCurrentUser(incomingUser.id);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getCurrentUserOrganizations = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const organizations = await this.organizationService.getUserOrganizations(incomingUser.id);

      res.json({ organizations });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary get payments
   *
   * @returns card object
   */
  public getPayments = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getPayments.call`);

      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const cards = await getStripeCustomerCards(incomingUser.id);

      EFuseAnalyticsUtil.incrementMetric(`users.controller.getPayments.success`);

      res.send({ cards });
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getPayments.failure`);

      this.handleStripeError(error, next);
    }
  };

  public getProfileByIdOrUsername = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user: incomingUser } = req as { params?: { idOrUsername?: string }; user?: IUser };

      if (!params || !params.idOrUsername) {
        throw Failure.BadRequest("id or username not found");
      }

      const { idOrUsername } = params;

      const user = await this.userHelperService.getProfileByIdOrUsername(incomingUser, idOrUsername);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary set stripe intent
   *
   * @returns card
   */
  public getSetupIntent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getPayments.call`);

      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const intent = await getStripeSetupIntent(incomingUser.id);

      EFuseAnalyticsUtil.incrementMetric(`users.controller.getSetupIntent.success`);

      res.send({ intent });
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric(`users.controller.getPayments.failure`);

      this.handleStripeError(error, next);
    }
  };

  public getUserById = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user: incomingUser } = req as { params?: { id?: string }; user?: IUser };

      if (!params || !params.id) {
        throw Failure.BadRequest("Missing id parameter");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { id } = params;
      if (!this.userService.isValidObjectId(id)) {
        throw Failure.BadRequest("Invalid user id");
      }

      const user = await this.userHelperService.getUserById(id, incomingUser._id?.equals(id));

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getUserByUsername = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = req as { params?: { username?: string } };

      if (!params || !params.username) {
        throw Failure.BadRequest("Missing username");
      }

      const { username } = params;
      const user = await this.userHelperService.getUserByUsername(username);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Get youtube stream
   *
   * @param userId
   *
   * @memberof UsersController
   */
  public getYoutubeStream = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = req as { params?: { userId?: string } };

      if (!params || !params.userId) {
        throw Failure.BadRequest("userId is required");
      }

      const { userId } = params;
      const stream = await this.youtubeService.getYoutubeStream(userId);

      res.json(stream);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public logout = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body } = req as { body?: { refreshToken?: string } };

      if (!body || !body.refreshToken) {
        throw Failure.BadRequest("Token is required");
      }

      const { refreshToken } = body;

      await this.sessionService.revokeSingleSessionAsync(refreshToken);

      res.sendStatus(200);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public resetPassword = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params } = req as { body?: { password?: string }; params?: { token?: string } };

      if (!body || !body.password) {
        throw Failure.BadRequest("Password is required");
      }

      if (!params || !params.token) {
        throw Failure.BadRequest("Token is required");
      }

      const { password } = body;
      const { token } = params;

      // make sure password is valid
      const validationError = this.authHelper.validatePassword(password);
      if (validationError) {
        throw Failure.BadRequest(validationError.password, validationError);
      }

      // Find user against token
      const user = await this.userService.findOne(
        { resetPasswordToken: token },
        { _id: 1, email: 1, resetPasswordToken: 1, resetPasswordExpires: 1 }
      );

      // Make sure user is present
      if (!user) {
        throw Failure.UnprocessableEntity("User not found");
      }

      // Make sure token is valid
      if (!user.resetPasswordExpires) {
        throw Failure.UnprocessableEntity("Invalid token");
      }

      // Make sure token is not expired
      const tokenExpired = new Date() > user.resetPasswordExpires;
      if (tokenExpired) {
        throw Failure.BadRequest("Reset password token has expired");
      }

      // Make sure user always enters a new password
      const matchedPasswordHistory = await this.authService.matchPasswordHistory(user.id, password);
      if (matchedPasswordHistory) {
        throw Failure.BadRequest("You have recently used this password. Please try a new password");
      }

      // Update password
      await this.authService.updatePassword(user._id, password);

      // Expire token
      await this.userService.update(user._id, { resetPasswordToken: undefined, resetPasswordExpires: undefined });

      // Revoke session
      await this.sessionService.revokeSessionAsync(user._id);

      res.json({ passwordUpdated: true, flashType: true, message: "Success! Your password has been updated." });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary remove discord server
   *
   *
   * @memberof UsersController
   */
  public removeDiscordServer = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user: IUser | null = await this.userService.findOne({ _id: incomingUser._id });

      if (!user) {
        throw Failure.BadRequest("Unable to find user");
      }

      user.discordServer = "";
      const updatedUser = await user.save({ validateBeforeSave: false });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Add discord server
   *
   * @param params.id webhook Id
   *
   * @memberof UsersController
   */
  public removeDiscordWebhook = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user: incomingUser } = req as { params?: { id?: string }; user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      if (!params || !params.id || !incomingUser.discordWebhooks) {
        throw Failure.BadRequest("discordWebhooks are required");
      }

      const webhookId = req.params.id;
      const userObj: IUser | null = await this.userService.findOne({ _id: incomingUser._id });

      if (!userObj) {
        throw Failure.BadRequest("unable to find user");
      }

      userObj.discordWebhooks = <DiscordHooks[]>(
        userObj.discordWebhooks?.filter((hook) => hook._id?.toHexString() !== webhookId)
      );

      await userObj.save({ validateBeforeSave: false });

      res.json({ user: userObj });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removePic = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { query, user: incomingUser } = req as { query?: { type?: string }; user?: IUser };

      if (!query || !query.type) {
        throw Failure.BadRequest("params missing");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { type } = query;
      const user = await this.userHelperService.removePic(incomingUser, type);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public selectPathway = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { pathwayId?: string }; user?: IUser };

      if (!body || !body.pathwayId) {
        throw Failure.BadRequest("Missing or invalid body");
      }

      const { pathwayId } = body;

      // make sure id is valid
      if (!this.userService.isValidObjectId(pathwayId)) {
        throw Failure.BadRequest("invalid pathway ID");
      }

      // make sure pathway exists & is active
      const pathway: IPathway | null = await this.pathwayService.findOne(
        { _id: pathwayId, active: true },
        {},
        { lean: true }
      );

      if (!pathway) {
        throw Failure.UnprocessableEntity("Pathway not found");
      }

      await this.userService.update(incomingUser!._id, { $set: { pathway: pathway._id as string } });

      res.json({ pathway });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public setLocale = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params } = req as { body?: { locale?: string }; params?: { id?: string } };

      if (!params || !params.id) {
        throw Failure.BadRequest("Missing id paramater");
      }

      if (!body || !body.locale) {
        throw Failure.BadRequest("Missing locale");
      }

      const { id } = params;
      const { locale } = body;

      const user: IUser = await this.userService.setLocale(id, locale);

      res.json({ user: WhitelistHelper.userPayload(user) });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Toggle public traits motiviations
   *
   * @param body.publicTraitsMotivations
   *
   * @return {IUser}
   * @memberof UsersController
   */
  public togglePublicTraitsMotivations = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { publicTraitsMotivations?: boolean }; user?: IUser };

      if (!body || !body.publicTraitsMotivations) {
        throw Failure.BadRequest("publicTraitsMotivations is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user = await this.userService.findOne({ _id: incomingUser._id });
      if (!user) {
        throw Failure.BadRequest("User is required");
      }

      const { publicTraitsMotivations } = body;
      user.publicTraitsMotivations = publicTraitsMotivations;

      await user.save({ validateBeforeSave: false });

      res.json(user);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public twitchAuth = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { code?: string }; user?: IUser };

      if (!body || !body.code) {
        throw Failure.BadRequest("twitch code is required");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { code } = body;
      const user = await this.userHelperService.twitchAuth(code, incomingUser._id);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public unban = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params } = req as { params?: { username?: string } };

      if (!params || !params.username) {
        throw Failure.BadRequest("Missing username");
      }

      const { username } = params;
      await this.userBanningService.unban(username);

      res.sendStatus(200);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public unblockUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user: incomingUser } = req as { params?: { id?: string }; user?: IUser };

      if (!params || !params.id) {
        throw Failure.BadRequest("params are missing");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { id } = params;
      const updatedResponse = await this.userHelperService.unblockUser(id, incomingUser);

      res.json(updatedResponse);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public unLinkExternalAccounts = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { service?: string }; user?: IUser };

      if (!body || !body.service) {
        throw Failure.BadRequest("unlinking account params are missing");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { service } = body;
      const user = await this.userHelperService.unLinkExternalAccounts(incomingUser._id, service, OwnerKind.USER);

      res.json({ success: true, user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public unlinkGameAccount = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: { game?: string }; user?: IUser };

      if (!body || !body.game) {
        throw Failure.BadRequest("params missing");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const { game } = body;
      const user = await this.userHelperService.unlinkGameAccount(incomingUser, game);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   *  @summary updateComponentLayout
   *
   * @return user
   *
   * @memberof UsersController
   */
  public updateComponentLayout = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: IComponentLayout[]; user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user: IUser | null = await this.userService.findOne(
        { _id: incomingUser._id },
        { _id: 1, componentLayout: 1 }
      );

      if (!user) {
        throw Failure.BadRequest("Unable to find user");
      }

      user.componentLayout = body;
      await user.save({ validateBeforeSave: false });

      res.send(user);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public updateCurrentUser = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, files, user: incomingUser } = req as { body: any; files: any[]; user: any };

      if (!body) {
        throw Failure.BadRequest("Missing user in body");
      }

      if (!this.userHelperService.validateUserUpdateFields(body)) {
        throw Failure.Forbidden("Invalid update request.");
      }

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user: any = await this.userHelperService.updateCurrentUser(incomingUser, body, files as any);

      await this.streamChatService.updateStreamChatUser(user._id, user.name, user.profilePicture.url);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public enableAlias = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const user: IUser = await this.userService.enableUserAlias(incomingUser._id);

      res.json({ user });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  private handleStripeError = (error: any, next: NextFunction): void => {
    const err: IStripeError = <IStripeError>error;

    // return specific error from stripe API
    if (err?.statusCode && err?.raw?.message) {
      const { message } = err.raw;

      this.handleError(Failure.PaymentRequired(message, error), next);
    } else {
      this.handleError(error, next);
    }
  };
}
