import { NextFunction, Request, Response } from "express";
import { BaseController } from "./base.controller";
import { OrganizationACLService } from "../../lib/organizations/organization-acl.service";
import { OrganizationPermissionSubjects, OrganizationUserRoleEnum } from "../../lib/permissions/enums";

const NAME = "organization-permissions.controller";

export class OrganizationPermissionsController extends BaseController {
  private orgAclService: OrganizationACLService;

  constructor() {
    super(NAME);

    this.orgAclService = new OrganizationACLService();
  }

  /**
   * Only intended to be used from ReTool
   */
  public addOrgPermission = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body } = req as { body: any; params: any; user: any };
    const {
      orgId,
      object,
      action,
      roles
    }: {
      orgId: string;
      object: string;
      action: OrganizationPermissionSubjects;
      roles: OrganizationUserRoleEnum[];
    } = body;

    try {
      // const success = await this.orgAclService.addOrgPermission(orgId, object, action, roles);

      // res.json({ success });
      res.json({ success: false });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
