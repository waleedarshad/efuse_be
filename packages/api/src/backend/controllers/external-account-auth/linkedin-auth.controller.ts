import { AuthenticateOptions, Profile } from "passport";
import { Request, Response } from "express";

import { ExternalAccountAuthController } from "./external-account-auth.controller";
import { LinkedinAuthService } from "../../../lib/external-account-auth/linkedin-auth.service";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";

const NAME = "linkedin-auth.controller";
const PROVIDER = "linkedin";
const OPTIONS: AuthenticateOptions = {};

export class LinkedinAuthController extends ExternalAccountAuthController {
  private $linkedinAuthService: LinkedinAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS);

    this.$linkedinAuthService = new LinkedinAuthService();
  }

  /**
   * @summary Handle linkedin callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof LinkedinAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: Profile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$linkedinAuthService.storeProfile(authInfo.ownerId, accessToken, refreshToken, profile);

      res.redirect(`${this.$redirectURL}?token=${<string>profile.id}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
