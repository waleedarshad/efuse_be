import { BACKEND_DOMAIN, DOMAIN } from "@efuse/key-store";
import { Request, Response } from "express";
import { Profile } from "passport";

import { ExternalAccountAuthController } from "./external-account-auth.controller";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";
import { StateSpaceAuthService } from "../../../lib/external-account-auth/state-space-auth.service";

const NAME = "state-space-auth.controller";
const PROVIDER = "state-space";
const OPTIONS = {};
const REDIRECT_URL = `${DOMAIN}/settings/external_accounts/statespace/callback`;
const CONSENT_URL = `${BACKEND_DOMAIN}/api/auth/statespace`;

export class StateSpaceAuthController extends ExternalAccountAuthController {
  private $stateSpaceAuthService: StateSpaceAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS, REDIRECT_URL, CONSENT_URL);

    this.$stateSpaceAuthService = new StateSpaceAuthService();
  }

  /**
   * @summary Handle state space callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof StateSpaceAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: Profile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$stateSpaceAuthService.storeProfile(
        authInfo.ownerId,
        authInfo.ownerKind,
        accessToken,
        refreshToken,
        profile
      );

      res.redirect(`${this.$redirectURL}?token=${authInfo.ownerId}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
