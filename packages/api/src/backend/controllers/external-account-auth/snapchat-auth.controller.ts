import { AuthenticateOptions, Profile } from "passport";
import { Request, Response } from "express";

import { SnapchatAuthService } from "../../../lib/external-account-auth/snapchat-auth.service";
import { ExternalAccountAuthController } from "./external-account-auth.controller";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";

const NAME = "snapchat-auth.controller";
const PROVIDER = "snapchat";
const OPTIONS: AuthenticateOptions = {};

export class SnapchatAuthController extends ExternalAccountAuthController {
  private $snapchatAuthService: SnapchatAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS);

    this.$snapchatAuthService = new SnapchatAuthService();
  }

  /**
   * @summary Handle snapchat callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof SnapchatAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: Profile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$snapchatAuthService.storeProfile(authInfo.ownerId, accessToken, refreshToken, profile);

      res.redirect(`${this.$redirectURL}?token=${<string>profile.id}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
