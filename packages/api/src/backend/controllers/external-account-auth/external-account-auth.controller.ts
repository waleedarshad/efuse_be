import { Failure } from "@efuse/contracts";
import { OwnerKind } from "@efuse/entities";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import passport, { AuthenticateOptions, Profile } from "passport";
import { DOMAIN, BACKEND_DOMAIN } from "@efuse/key-store";

import { BaseController } from "../base.controller";
import { ExternalAccountAuthService } from "../../../lib/external-account-auth/external-account-auth.service";
import { IUser } from "../../interfaces";

const FE_EXTERNAL_ACCOUNT_URL = `${DOMAIN}/settings/external_accounts`;
const CONSENT_BASE_URL = `${BACKEND_DOMAIN}/api/auth`;

export class ExternalAccountAuthController extends BaseController {
  private $provider: string;
  private $consentURL: string;
  private $authenticateOptions: AuthenticateOptions;

  protected $redirectURL: string;
  protected $externalAccountAuthService: ExternalAccountAuthService;

  constructor(
    name: string,
    provider: string,
    authenticateOptions: AuthenticateOptions,
    redirectURL?: string,
    consentURL?: string
  ) {
    super(name);

    this.$provider = provider;
    this.$redirectURL = redirectURL || `${FE_EXTERNAL_ACCOUNT_URL}/${provider}`;
    this.$consentURL = consentURL || `${CONSENT_BASE_URL}/${provider}`;

    this.$authenticateOptions = authenticateOptions;

    this.$externalAccountAuthService = new ExternalAccountAuthService(provider);
  }

  /**
   * @summary Store user's ID in redis & generate consent URL
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof ExternalAccountAuthController
   */
  public generateConsentURL = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { orgId } = req.query;

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const ownerId = <string>orgId || user._id.toHexString();
      const ownerKind = orgId ? OwnerKind.ORGANIZATION : OwnerKind.USER;

      const token = await this.$externalAccountAuthService.generateConsentToken(
        ownerId,
        ownerKind,
        user._id.toHexString()
      );

      res.json({ url: `${this.$consentURL}?token=${token}` });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Load consent screen
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof ExternalAccountAuthController
   */
  public getConsent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { token } = req.query;

      const state = await this.$externalAccountAuthService.generateState(token as string);

      passport.authenticate(this.$provider, { ...this.$authenticateOptions, state })(req, res, next);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary External Auth callback handler
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof ExternalAccountAuthController
   */
  public callbackHandler = (req: Request, res: Response, next: NextFunction): void => {
    try {
      passport.authenticate(this.$provider, { failureRedirect: this.$redirectURL, session: false })(req, res, next);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Verify profile
   *
   * @param {Request} req
   * @param {string} accessToken
   * @param {string} refreshToken
   * @param {Function} done
   *
   * @memberof GoogleAuthController
   */
  public verifyProfile = async (
    req: Request,
    accessToken: string,
    refreshToken: string,
    profile: Profile,
    done: Function
  ): Promise<void> => {
    const externalAuthRequestInfo = await this.$externalAccountAuthService.verifyState(req.query.state as string);

    if (externalAuthRequestInfo) {
      done(null, { accessToken, refreshToken, profile, externalAuthRequestInfo });
    } else done(null, false);
  };
}
