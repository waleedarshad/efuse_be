import { IRiotProfile } from "@efuse/passport-riot";
import { Request, Response } from "express";

import { ExternalAccountAuthController } from "./external-account-auth.controller";
import { RiotAuthService } from "../../../lib/external-account-auth/riot-auth.service";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";

const NAME = "riot-auth.controller";
const PROVIDER = "riot";
const OPTIONS = {};

export class RiotAuthController extends ExternalAccountAuthController {
  private $riotAuthService: RiotAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS);

    this.$riotAuthService = new RiotAuthService();
  }

  /**
   * @summary Handle riot callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof RiotAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: IRiotProfile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$riotAuthService.storeProfile(
        authInfo.ownerId,
        authInfo.ownerKind,
        accessToken,
        refreshToken,
        profile
      );

      res.redirect(`${this.$redirectURL}?token=${authInfo.ownerId}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
