import { AuthenticateOptions } from "passport";
import { Request, Response } from "express";
import { BACKEND_DOMAIN, DOMAIN } from "@efuse/key-store";

import { ExternalAccountAuthController } from "./external-account-auth.controller";
import { TwitchAuthService } from "../../../lib/external-account-auth/twitch-auth.service";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";
import { ITwitchAuthProfile } from "../../interfaces";

const NAME = "twitch-auth.controller";
const PROVIDER = "twitch.js";
const OPTIONS: AuthenticateOptions = {};
const REDIRECT_URL = `${DOMAIN}/settings/external_accounts/twitch/callback`;
const CONSENT_URL = `${BACKEND_DOMAIN}/api/auth/twitch/consent`;

export class TwitchAuthController extends ExternalAccountAuthController {
  private $twitchAuthService: TwitchAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS, REDIRECT_URL, CONSENT_URL);

    this.$twitchAuthService = new TwitchAuthService();
  }

  /**
   * @summary Handle twitch callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof TwitchAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: ITwitchAuthProfile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$twitchAuthService.storeProfile(
        authInfo.ownerId,
        authInfo.ownerKind,
        authInfo?.requestingUserId,
        accessToken,
        refreshToken,
        profile
      );

      res.redirect(`${this.$redirectURL}?token=${<string>profile.id}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
