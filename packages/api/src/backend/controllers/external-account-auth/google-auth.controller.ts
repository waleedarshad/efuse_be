import { AuthenticateOptions, Profile } from "passport";
import { DOMAIN } from "@efuse/key-store";
import { Request, Response } from "express";

import { ExternalAccountAuthController } from "./external-account-auth.controller";
import { GoogleAuthService } from "../../../lib/external-account-auth/google-auth.service";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";

const NAME = "google-auth.controller";
const PROVIDER = "google";
const OPTIONS: AuthenticateOptions = <any>{ accessType: "offline", prompt: "consent" };
const REDIRECT_URL = `${DOMAIN}/settings/external_accounts/youtube`;

export class GoogleAuthController extends ExternalAccountAuthController {
  private $googleAuthService: GoogleAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS, REDIRECT_URL);

    this.$googleAuthService = new GoogleAuthService();
  }

  /**
   * @summary Handle google callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof GoogleAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: Profile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$googleAuthService.storeAccountInformation(
        authInfo?.ownerId,
        authInfo?.ownerKind,
        authInfo?.requestingUserId,
        accessToken,
        refreshToken,
        profile
      );

      res.redirect(`${this.$redirectURL}?token=${<string>profile.id}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
