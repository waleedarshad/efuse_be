import { Request, Response } from "express";
import { AuthenticateOptions, Profile } from "passport";

import { BnetAuthService } from "../../../lib/external-account-auth/bnet-auth.service";
import { IExternalAuthRequestInfo } from "../../interfaces/external-auth-request-info";
import { ExternalAccountAuthController } from "./external-account-auth.controller";

const NAME = "bnet-auth.controller";
const PROVIDER = "bnet";
const OPTIONS: AuthenticateOptions = {};

export class BnetAuthController extends ExternalAccountAuthController {
  private $bnetAuthService: BnetAuthService;

  constructor() {
    super(NAME, PROVIDER, OPTIONS);

    this.$bnetAuthService = new BnetAuthService();
  }

  /**
   * @summary Handle bnet callback & update profile
   *
   * @param {Request} req
   * @param {Response} res
   *
   * @memberof BnetAuthController
   */
  public updateProfile = async (req: Request, res: Response): Promise<void> => {
    try {
      const { accessToken, refreshToken, profile, externalAuthRequestInfo } = <
        {
          accessToken: string;
          refreshToken: string;
          profile: Profile;
          externalAuthRequestInfo: IExternalAuthRequestInfo;
        }
      >(<unknown>req.user);

      const authInfo = <IExternalAuthRequestInfo>externalAuthRequestInfo;

      await this.$bnetAuthService.storeProfile(authInfo.ownerId, accessToken, refreshToken, profile);

      res.redirect(`${this.$redirectURL}?token=${<string>profile.id}`);
    } catch {
      res.redirect(this.$redirectURL);
    }
  };
}
