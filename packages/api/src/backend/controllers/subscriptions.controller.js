const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const recurly = require("recurly");

const client = new recurly.Client("5539a81a310c403db46f71a7ee58d945");
const { PaymentProfile } = require("../models/PaymentProfile");
const { getRecurlyAccount: getAccount } = require("../../lib/subscriptions");

const getRecurlyAccount = async (req, res) => {
  const { user } = req;
  try {
    const account = await getAccount(user._id);
    // TODO GET PLANS/INVOICES FOR USER FROM RECURLY
    return res.status(200).json({ account });
  } catch (err) {
    console.log("getRecurlyAccount() Error: ", err);
    return res.status(500).json(err);
  }
};

const handleRecurlyWebhook = async (req, res) => {
  const { body } = req;
  try {
    // determin if body is formatted correctly
    if (Object.keys(body).length != 1) return res.status(500).json("Unknown webhook event");

    // first index will include event name
    const eventType = Object.keys(body)[0];
    const accountObj = body[eventType].account;
    const accountCode = accountObj[0].account_code[0];
    const account = await getRecurlyAccount(accountCode);
    // TODO UPDATE PLANS/INVOICES FOR USER
    return res.status(200).json({ account });
  } catch (err) {
    logger.error("Error in Recurly webhook");
    return res.status(500).json({ error: err });
  }
};

module.exports.getRecurlyAccount = getRecurlyAccount;
module.exports.handleRecurlyWebhook = handleRecurlyWebhook;
