import { Request, Response, NextFunction } from "express";
import { Failure } from "@efuse/contracts";
import { BaseController } from "./base.controller";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { UserExperienceService } from "../../lib/experiences/user-experience.service";
import { IUser } from "../interfaces/user";
import { IUserExperience } from "../interfaces/user-experience";

const NAME = "user-experience.controller";

export class UserExperienceController extends BaseController {
  private $userExperienceService: UserExperienceService;
  constructor() {
    super(NAME);
    this.$userExperienceService = new UserExperienceService();
  }

  /**
   * @summary Grab the current user's list of experiences needing displayed
   *
   * @param  {Express.Request} req - Pass the current user in the request
   * @param  {Express.Response} res - An express response object
   * @return {Array} Returns a list of user experiences and whether or not to display them
   * @example .get(`/users/get_user_experience`)
   */
  public index = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.index.call");
    try {
      const { user } = req as { user: any };
      const experiences = await this.$userExperienceService.getDisplayedUserExperiences(user.id);
      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.index.success");
      res.json(experiences);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.index.failure");
      this.handleError(error, next);
    }
  };

  /**
   * @summary Creates a userExperience given a specific experience and user
   *
   * @param  {Express.Request} req - Pass the current user in the request
   * @param  {Express.Response} res - An express response object
   * @return {Experience} Returns a the new userExperience
   */
  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.create.call");

    try {
      const { body, user } = req as { body: any; params: any; query: any; user: any };

      if (!body || !body.experience) {
        throw Failure.BadRequest("Name is a required field");
      }

      const newUserExperience = await this.$userExperienceService.createUserExperience(body.experience, user.id);

      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.create.success");
      res.json(newUserExperience);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.create.failure");

      this.handleError(error, next);
    }
  };

  /**
   * @summary Increments the views for a global experience and/or the subViews in that experience
   *
   * @param  {Express.Request} req - Pass the current user in the request and the body of the experience you'd like to update
   * @param  {Express.Response} res - An express response object
   * @return {Experience} Returns the updated userExperience
   * @example .put(`/users/increment_views`, {experienceId: experienceId, isViewed: true, isGlobal: false, viewName: subView})
   */
  public incrementViews = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.incrementViews.call");
    try {
      const { user } = req as { user: any };
      const body: undefined | { isViewed?: boolean; isGlobal?: boolean; experienceId: string; viewName: string } =
        req.body as undefined | { isViewed?: boolean; isGlobal?: boolean; experienceId: string; viewName: string };

      if (!body || !body.experienceId) {
        throw Failure.BadRequest("params are missing");
      }
      const { isViewed, isGlobal } = body;
      let userExperience: IUserExperience;
      if (isGlobal) {
        const incrementQuery = {
          ...(isViewed ? { "metrics.globalViews": 1 } : { "metrics.globalSkips": 1 })
        };
        userExperience = await this.incrementCounter(body, user, incrementQuery, {});
      } else {
        const incrementQuery = {
          ...(isViewed ? { "metrics.subViews.$.views": 1 } : { "metrics.subViews.$.skips": 1 })
        };
        const findQuery = { "metrics.subViews.name": body.viewName };
        userExperience = await this.incrementCounter(body, user, incrementQuery, findQuery);
      }
      res.json(userExperience);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.incrementViews.failure");
      this.handleError(error, next);
    }
  };

  incrementCounter = async (
    body: { isViewed?: boolean; isGlobal?: boolean; experienceId: string; viewName: string },
    user: IUser,
    incrementQuery: any,
    findQuery = {}
  ): Promise<IUserExperience> => {
    const userExperience = await this.$userExperienceService.findOneAndUpdate(
      { experience: body.experienceId, user: user._id, ...findQuery },

      { $inc: { ...incrementQuery } }
    );
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.incrementViews.success");
    return userExperience;
  };

  /**
   * @summary Loop through all existing experiences and create userExperiences
   *
   * @param  {Express.Request} req - Pass the current user in the request
   * @param  {Express.Response} res - An express response object
   * @return {Array} Returns a list of the created experiences
   */
  public createAllExperiences = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.createAllExperiences.call");
    try {
      const { user } = req as { body: any; params: any; query: any; user: any };
      const experiences = await this.$userExperienceService.createAllUserExperiences(user.id);
      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.createAllExperiences.success");
      res.json(experiences);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("user_experience_controller.createAllExperiences.failure");

      this.handleError(error, next);
    }
  };
}
