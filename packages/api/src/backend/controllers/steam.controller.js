const { DOMAIN } = require("@efuse/key-store");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../models/User");
const { Steam } = require("../models/Steam");
const { SteamOwnedGame } = require("../models/SteamOwnedGame");
const { SteamFriend } = require("../models/SteamFriend");

const SteamServiceLib = require("../../lib/steamservice");

const setToken = (req, res, next) => {
  req.app.set("userId", req.query.token);
  next();
};

const getSteamProfile = (identifier, profile, done) => {
  return done(null, profile);
};

const updateUser = async (req, res) => {
  const endPoint = `${DOMAIN}/settings/external_accounts/steam`;
  const { steamid, personaname } = req.user._json;
  try {
    const user = await User.findOne({ _id: req.app.get("userId") });
    // todo: investigate
    // @ts-ignore
    user.steamId = steamid;
    // todo: investigate
    // @ts-ignore
    user.steamUsername = personaname;
    // todo: investigate
    // @ts-ignore
    user.steamVerified = true;
    // todo: investigate
    // @ts-ignore
    await user.save({ validateBeforeSave: false });
    // todo: investigate
    // @ts-ignore
    await SteamServiceLib.syncSteamProfileAsync(user._id);

    return res.redirect(`${endPoint}?token=${steamid}`);
  } catch (error) {
    logger.error(error);
    return res.redirect(`${endPoint}`);
  }
};

const getFriends = async (req, res) => {
  try {
    const { query, params } = req;
    const { page, pageSize } = query;
    const user = await User.findOne({ _id: params.userId });
    if (user && user.steam) {
      try {
        const { id, steam } = user;
        const friends = await SteamFriend.paginate(
          // todo: investigate
          // @ts-ignore
          { user: id, steam: steam ? steam._id : "" },
          { page, limit: pageSize, sort: { createdAt: -1 } }
        );
        return res.json({ friends });
      } catch (error) {
        logger.error(error);
        return res.status(500).json({ error });
      }
    } else {
      return res.json({ friends: [] });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const getOwnedGames = async (req, res) => {
  try {
    const { query, params } = req;
    const { page, pageSize } = query;
    const user = await User.findOne({ _id: params.userId });

    if (user && user.steam) {
      try {
        const { id, steam } = user;
        const ownedGames = await SteamOwnedGame.paginate(
          // todo: investigate
          // @ts-ignore
          { user: id, steam: steam ? steam._id : "" },
          { page, limit: pageSize, sort: { createdAt: -1 } }
        );
        return res.json({ ownedGames });
      } catch (error) {
        logger.error(error);
        return res.status(500).json({ error });
      }
    } else {
      return res.json({ ownedGames: [] });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

module.exports.getFriends = getFriends;
module.exports.getOwnedGames = getOwnedGames;
module.exports.getSteamProfile = getSteamProfile;
module.exports.setToken = setToken;
module.exports.updateUser = updateUser;
