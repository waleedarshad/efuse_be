import { NextFunction, Request, Response } from "express";
import { OpportunityRankService } from "../../lib/opportunities/opportunity-rank.service";

import { OpportunityService } from "../../lib/opportunities/opportunity.service";
import { BaseController } from "./base.controller";

const NAME = "opportunities.controller";

export class OpportunitiesController extends BaseController {
  private opportunitiesService: OpportunityService;
  private opportunityRankService: OpportunityRankService;

  constructor() {
    super(NAME);

    this.opportunitiesService = new OpportunityService();
    this.opportunityRankService = new OpportunityRankService();
  }

  public toggleBoost = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      await this.opportunitiesService.toggleBoost(user.roles, params.opportunityId, body.boosted);
      this.opportunityRankService.setObjectRankAsync(params.opportunityId);
      res.json({ success: true });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
