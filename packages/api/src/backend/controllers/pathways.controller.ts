import { NextFunction, Request, Response } from "express";

import { BaseController } from "./base.controller";
import { PathwayService } from "../../lib/pathway/pathway.service";
import { IPathway } from "../interfaces/pathway";

const NAME = "pathways.controller";

export class PathwaysController extends BaseController {
  private pathwayService: PathwayService;

  constructor() {
    super(NAME);

    this.pathwayService = new PathwayService();
  }

  public getActive = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const pathways: IPathway[] = await this.pathwayService.getActive();
      res.json(pathways);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
