import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "./base.controller";
import { LoungeFeedService } from "../../lib/lounge/lounge-feed.service";
import { IUser } from "../interfaces/user";
import { FeaturedLoungeService } from "../../lib/lounge/featured-lounge.service";

const NAME = "lounge-feed.controller";

export class LoungeFeedController extends BaseController {
  private loungeFeedService: LoungeFeedService;
  private featuredLoungeService: FeaturedLoungeService;

  constructor() {
    super(NAME);

    this.loungeFeedService = new LoungeFeedService();
    this.featuredLoungeService = new FeaturedLoungeService();
  }

  /**
   * @summary Get verified feed
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LoungeFeedController
   */
  public getVerifiedFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { page, limit } = req.query;
    const { user } = <{ user: Optional<IUser> }>req;

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const feed = await this.loungeFeedService.getVerifiedLoungeFeed(user.id, Number(page), Number(limit));

      res.json(feed);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @summary Get featured lounge feed ranked by "the algorithm"
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LoungeFeedController
   */
  public getRankedFeaturedLoungeFeed = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = <{ user: Optional<IUser> }>req;
    const { page, limit } = req.query;

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const feed = await this.featuredLoungeService.getPaginatedRankLoungeFeedFormatted(
        user._id,
        user.roles,
        Number(page),
        Number(limit)
      );
      res.json(feed);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getLoungeFeedPost = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;
      const { id } = req.params;

      if (!id || !this.loungeFeedService.isValidObjectId(id)) {
        throw Failure.BadRequest("Invalid id");
      }

      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const loungeFeedPost = await this.loungeFeedService.getLoungeFeedPost(id, user._id.toHexString());

      res.json(loungeFeedPost);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
