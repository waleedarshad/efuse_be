import { NextFunction, Request, Response } from "express";
import { OpportunityRequirementBaseService } from "../../lib/opportunities/opportunity-requirement-base.service";

import { BaseController } from "./base.controller";

const NAME = "opportunity-requirements.controller";

export class OpportunityRequirementsController extends BaseController {
  private $opportunityRequirementBaseService: OpportunityRequirementBaseService;

  constructor() {
    super(NAME);

    this.$opportunityRequirementBaseService = new OpportunityRequirementBaseService();
  }

  /**
   * @summary Update a particular member
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof OpportunityRequirementsController
   */
  public getAll = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const requirements = await this.$opportunityRequirementBaseService.find({ isActive: true });

      res.json({ requirements });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
