import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";

import { BaseController } from "./base.controller";
import { ITranscodeMediaStatus } from "../interfaces";
import { SNSMessageDetailType, SNSMessageType } from "../../lib/media/transcode-media.enum";
import { TranscodeMediaHandlerService } from "../../lib/media/transcode-media-handler.service";

const NAME = "transcode-media-handler.controller";

export class TranscodeMediaHandlerController extends BaseController {
  private transcodeMediaHandlerService: TranscodeMediaHandlerService;

  constructor() {
    super(NAME);

    this.transcodeMediaHandlerService = new TranscodeMediaHandlerService();
  }

  public transcodeMediaWebhook = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body } = req;

      const parsedBody = JSON.parse(body);
      this.logger.info(parsedBody, "Parsed Body");

      switch (parsedBody.Type) {
        case SNSMessageType.subscriptionConfirmation: {
          await this.transcodeMediaHandlerService.subscribeEndpoint(parsedBody.TopicArn, parsedBody.Token);
          break;
        }
        case SNSMessageType.notification: {
          const message: ITranscodeMediaStatus = <ITranscodeMediaStatus>JSON.parse(parsedBody.Message);

          // Make sure detail-type is MediaConvert Job State Change
          if (message["detail-type"] === SNSMessageDetailType.mediaConvertState) {
            await this.transcodeMediaHandlerService.transcodeStatusNotification(message);
          }

          break;
        }
        default:
          throw Failure.NotImplemented();
      }

      // Send OK response
      res.send("OK");
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
