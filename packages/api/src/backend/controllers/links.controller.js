const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../models/User");
const { Link } = require("../models/link");
const { LinkClick } = require("../models/LinkClick");

const storeLinkClick = async (req, res) => {
  try {
    const { body } = req;
    const { userId, shortName } = body;
    const user = await User.findOne({ _id: userId }).lean();
    const link = await Link.findOne({ shortName }).lean();
    if (!link) {
      logger.error(body, "Link not found");
      return res.status(422).json({ error: "Link not found!" });
    }
    await new LinkClick({
      user: user ? user._id : null,
      link: link._id
    }).save();
    return res.json({ link });
  } catch (error) {
    logger.error(error, "Error while storing link click");
    return res.json({ error });
  }
};

module.exports.storeLinkClick = storeLinkClick;
