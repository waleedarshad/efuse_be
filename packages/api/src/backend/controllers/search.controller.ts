import { Failure } from "@efuse/contracts";
import { Optional } from "@efuse/types";
import { NextFunction, Request, Response } from "express";
import { Types } from "mongoose";

import { ApplicantEntityTypeEnum } from "../../lib/applicants/applicant.enum";
import { ApplicantService } from "../../lib/applicants/applicant.service";
import { BaseController } from "./base.controller";
import { IApplicant } from "../interfaces/applicant";
import { ISearch, IUser } from "../interfaces";
import { ISearchedUser } from "../interfaces/searched-user";
import { Kinds } from "../../lib/kinds";
import { OrganizationsService } from "../../lib/organizations/organizations.service";
import { SearchHelper } from "../../lib/search/search.helper";
import { SearchLimit } from "../../lib/search/search.enum";
import { SearchService } from "../../lib/search/search.service";
import { UserService } from "../../lib/users/user.service";

const NAME = "search.controller";

export class SearchController extends BaseController {
  private service: SearchService;
  private userService: UserService;
  private organizationsService: OrganizationsService;
  private applicantService: ApplicantService;
  private searchHelper: SearchHelper;

  private emptyResponse = {
    followers: []
  };

  constructor() {
    super(NAME);

    this.service = new SearchService();
    this.userService = new UserService();
    this.organizationsService = new OrganizationsService();
    this.applicantService = new ApplicantService();
    this.searchHelper = new SearchHelper();
  }

  public mentionFollowers = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { query, user } = <{ query: any; user: Optional<IUser> }>req;
    const { query: term } = query as { [key: string]: string };

    if (!query || !term) {
      return next(res.status(200).json(this.emptyResponse));
    }

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const terms = this.searchHelper.sanitizeToArray(term);

      if (terms.length === 0) {
        return next(res.status(200).json(this.emptyResponse));
      }

      const results = await this.service.search(terms, user._id, [Kinds.USER]);
      const followers = results.map((user: ISearchedUser) => {
        return {
          display: `${user.username} (${user.name})`,
          id: user._id as string
        };
      });

      res.status(200).json({ followers });
    } catch (error) {
      const message: string = error.message ? error.message : "Error querying mentionFollowers";

      this.logger.error({ query, error }, message);

      return next(Failure.BadRequest(message));
    }
  };

  public searchGlobalUsersToChat = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { query, user } = <{ query: any; user: Optional<IUser> }>req;
    const { query: term } = query as { [key: string]: string };

    if (!query || !term) {
      return next(res.status(200).json(this.emptyResponse));
    }

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const terms = this.searchHelper.sanitizeToArray(term);

      if (terms.length === 0) {
        return next(res.status(200).json(this.emptyResponse));
      }

      const followers = await this.service.search(terms, user?._id, [Kinds.USER]);

      res.status(200).json({ followers });
    } catch (error) {
      const message: string = error.message ? error.message : "Error querying globalUserSearch";

      this.logger.error({ query, error }, message);

      return next(Failure.BadRequest(message));
    }
  };

  public globalUserSearch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { query, user } = <{ query: any; user: Optional<IUser> }>req;
    const { query: term } = query as { [key: string]: string };
    const emptyResponse = { users: { docs: [] } };

    if (!query || !term) {
      return next(res.status(200).json(emptyResponse));
    }

    if (!user) {
      throw Failure.BadRequest("Missing user");
    }

    try {
      const terms = this.searchHelper.sanitizeToArray(term);

      if (terms.length === 0) {
        return next(res.status(200).json(emptyResponse));
      }

      const users = await this.service.search(terms, user?._id, [Kinds.USER]);

      res.status(200).json({ users: { docs: users } });
    } catch (error) {
      const message: string = error.message ? error.message : "Error querying globalUserSearch";

      this.logger.error({ query, error }, message);

      return next(Failure.BadRequest(message));
    }
  };

  public globalMentionSearch = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { query, user } = <{ query: any; user: Optional<IUser> }>req;
    const { query: term } = query as { [key: string]: string };

    if (!query || !term) {
      return next(res.status(200).json(this.emptyResponse));
    }

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      const terms = this.searchHelper.sanitizeToArray(term);

      if (terms.length === 0) {
        return next(res.status(200).json(this.emptyResponse));
      }

      const results = await this.service.search(terms, user?._id);

      const followers = results.map((user: ISearchedUser) => {
        return {
          display: user.username,
          fullName: user.name,
          id: `${user._id as string}-${user.type}`,
          profilePicture: user.profilePicture.url,
          type: user.type,
          verified: user.verified
        };
      });

      res.status(200).json({ followers });
    } catch (error) {
      const message: string = error.message ? error.message : "Error querying globalMentionSearch";

      this.logger.error({ query, error }, message);

      return next(Failure.BadRequest(message));
    }
  };

  public search = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { query, user } = <{ query: any; user: Optional<IUser> }>req;
    const { term, kind, limit } = query as { [key: string]: string };

    try {
      if (!user) {
        throw Failure.BadRequest("Missing user");
      }

      // numbers destructured from the query string are treated as strings
      const parsedLimit: number = limit ? Number(limit) : SearchLimit.DEFAULT_SEARCH_LIMIT;

      if (!query || !term) {
        return next(res.status(200).json(this.createEmptyResult()));
      }

      if (parsedLimit > SearchLimit.MAX_SEARCH_LIMIT) {
        const message = `Failed: parameter 'limit' exceeds maximum value (${SearchLimit.MAX_SEARCH_LIMIT})`;
        return next(Failure.BadRequest(message));
      }

      const kinds: string[] = this.searchHelper.sanitizeToArray(kind);
      const terms: string[] = this.searchHelper.sanitizeToArray(term);

      /**
       * If there are no search terms after sanitization,
       * we don't want to actually perform the search, but
       * we don't really want to throw an error, just treat
       * it as a nothingburger.
       */
      if (terms.length === 0) {
        return next(res.status(200).json(this.createEmptyResult()));
      }

      const entities: ISearch[] = (await this.service.search(terms, user?._id, kinds, parsedLimit)) as ISearch[];

      if (entities) {
        res.status(200).json({
          users: {
            count: entities.length > parsedLimit ? parsedLimit : entities.length,
            docs: this.searchHelper.sortEntities(entities, parsedLimit)
          }
        });
      } else {
        return next(Failure.InternalServerError("Error: search failed"));
      }
    } catch (error) {
      const message = error.message || "Error: failure during search";

      this.logger.error(message, error.stack);

      next(Failure.InternalServerError(message, error.stack));
    }
  };

  /**
   * Search opportunities for matching entries
   */
  public searchOpportunities = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { query, user: currentUser } = <{ query: any; user: Optional<IUser> }>req;

      // Allow searching for user's applied opportunities only
      const { opportunityIds } = res.locals;

      const {
        page,
        limit,
        term,
        location,
        opportunityType,
        subType,
        publishStatus,
        user,
        organization,
        includeOrganizations,
        sortBy
      } = query;

      if (!page) {
        throw Failure.BadRequest("Missing 'page'");
      }
      if (!limit) {
        throw Failure.BadRequest("Missing 'limit'");
      }

      let limitValue = Number(limit);
      if (limitValue > 100) {
        limitValue = 100;
      }

      const oppTypes = this.convertToArray(opportunityType as string | string[]);
      const subTypes = this.convertToArray(subType as string | string[]);
      const publishStatuses = this.convertToArray(publishStatus as string | string[]);

      // set defaults
      if (publishStatuses.length === 0) {
        publishStatuses.push("open");
      }

      // Lookup for user's ID
      const userIdOrUsernameParam: string | undefined = user as string | undefined;
      const userId: Types.ObjectId | undefined = await this.userService.getUserIdByIdOrUsername(userIdOrUsernameParam);

      // Lookup for organization's ID
      const organizationIdOrShortNameParam: string | undefined = organization as string | undefined;
      const organizationId: Types.ObjectId | undefined =
        await this.organizationsService.getOrganizationIdByIdOrShortName(organizationIdOrShortNameParam);

      // Include organizations

      const includeOrganizationsParam = includeOrganizations && JSON.parse(String(includeOrganizations));
      let organizationsIds: Types.ObjectId[] | undefined;

      // includeOrganization is dependent upon the userId
      // TODO: [perms] Use permissions system instead of org collection
      if (userId && includeOrganizationsParam === true) {
        const organizations = await this.organizationsService.find(
          {
            $or: [{ user: userId }, { captains: userId }]
          },
          { _id: 1 }
        );

        organizationsIds = organizations.map((o): Types.ObjectId => o._id as Types.ObjectId);
      }

      const searchResults = await this.service.searchForOpportunities(
        Number(page),
        Number(limit),
        currentUser?._id,
        <string>term,
        <string>location,
        oppTypes,
        subTypes,
        publishStatuses,
        userId,
        organizationId,
        organizationsIds,
        sortBy as string | undefined,
        opportunityIds
      );
      res.json(searchResults);
    } catch (error) {
      const message = error.message || "Error: failure during search";

      this.logger.error(message, error.stack);

      next(Failure.InternalServerError(message, error.stack));
    }
  };

  private createEmptyResult(): { users: { count: number; docs: [] } } {
    return {
      users: {
        count: 0,
        docs: []
      }
    };
  }

  private convertToArray(incoming: string | string[]): string[] {
    if (!incoming) {
      return [];
    }

    if (Array.isArray(incoming)) {
      return incoming;
    }

    return [incoming];
  }

  /**
   * Search applied opportunities for matching entries
   */
  public searchAppliedOpportunities = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = <{ user: Optional<IUser> }>req;

      const applicants = await this.applicantService.find(
        { user: user?._id, entityType: ApplicantEntityTypeEnum.OPPORTUNITY },
        { entity: 1 },
        // todo: investigate correct usage, is this from a plugin?
        <never>{ autopopulate: false }
      );

      const opportunityIds: Types.ObjectId[] = applicants.map(
        (applicant: IApplicant): Types.ObjectId => <never>applicant.entity
      );

      res.locals = { opportunityIds };

      next();
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
