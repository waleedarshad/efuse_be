const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { PromoCode } = require("../models/PromoCode");
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");

const createPromoCode = async (req, res) => {
  const { body } = req;
  try {
    EFuseAnalyticsUtil.incrementMetric(`promo_code_controller.createPromoCode.call`);
    const promocode = await new PromoCode(body).save();
    EFuseAnalyticsUtil.incrementMetric(`promo_code_controller.createPromoCode.success`);
    return res.status(200).json({ promocode });
  } catch (err) {
    EFuseAnalyticsUtil.incrementMetric(`promo_code_controller.createPromoCode.failure`);
    logger.error("Error in promo_code_controller when creating new promocode", err);
    return res.status(500).json(err);
  }
};

const validatePromoCode = async (req, res) => {
  const { params, query } = req;

  try {
    EFuseAnalyticsUtil.incrementMetric(`promo_code_controller.validatePromoCode.call`);
    const promocodeQuery = { code: params.promocode };

    // if allowedOn is sent in query - add it to mongo DB query object
    if (query.allowedOn) promocodeQuery.allowedOn = query.allowedOn;

    // Query to find promocode matching `Promocode.code`
    const promocode = await PromoCode.findOne(promocodeQuery).select("_id code amount discountType userMessage").lean();

    EFuseAnalyticsUtil.incrementMetric(`promo_code_controller.validatePromoCode.success`);

    // Promocode does not exist
    if (!promocode) return res.status(422).json({ error: "Promocode not found." });

    return res.status(200).json({ promocode });
  } catch (err) {
    EFuseAnalyticsUtil.incrementMetric(`promo_code_controller.validatePromoCode.failure`);
    logger.error("Error in promo_code_controller when validating promocode", err);
    return res.status(500).json(err);
  }
};

module.exports.createPromoCode = createPromoCode;
module.exports.validatePromoCode = validatePromoCode;
