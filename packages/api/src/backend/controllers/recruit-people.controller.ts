import { NextFunction, Request, Response } from "express";
import { RecruitPeopleService } from "../../lib/recruitment/recruit-people.service";
import { IRecruitPeopleRequestBody, IRecruitPeopleUpdateBody } from "../interfaces/recruit-people";
import { BaseController } from "./base.controller";

const NAME = "recruit-people.controller";

export class RecruitPeopleController extends BaseController {
  private recruitPeopleService: RecruitPeopleService;

  constructor() {
    super(NAME);

    this.recruitPeopleService = new RecruitPeopleService();
  }

  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = req as { body: any; user: any };
      const recruitPeople = await this.recruitPeopleService.create(user._id, body as IRecruitPeopleRequestBody);
      res.json({ recruitPeople });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const recruitPeople = await this.recruitPeopleService.update(
        user._id,
        params.recruitPeopleId,
        body as IRecruitPeopleUpdateBody
      );
      res.json({ recruitPeople });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getAll = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = req as { user: any };
      const recruitPeople = await this.recruitPeopleService.getAllPeople(user._id);
      res.json(recruitPeople);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public delete = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, user } = req as { params: any; user: any };
      const recruitPeople = await this.recruitPeopleService.delete(user._id, params.recruitPeopleId);
      res.json({ recruitPeople });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
