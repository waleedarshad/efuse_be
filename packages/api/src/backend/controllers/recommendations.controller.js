const { Types } = require("mongoose");
const { Logger } = require("@efuse/logger");
const _ = require("lodash");
const { RedisRecommendationsEnum } = require("@efuse/entities");

const { User } = require("../models/User");
const { RedisHelper } = require("../helpers/redis.helper");
const { PromotedService } = require("../../lib/ads/promoted.service");

const logger = Logger.create();
const WhitelistHelper = require("../helpers/whitelist_helper");
const SchemaHelper = require("../helpers/schema_helper");
const promotedService = new PromotedService();

const getFollowerRecommendations = async (req, res) => {
  try {
    // Get 40 random verified Users
    const users = await User.aggregate([
      {
        $match: {
          verified: true,
          _id: { $nin: [new Types.ObjectId("5dee92ff69ccf2db8502f474")] }, // this will prevent KEEMSTAR from appearing in recommendations
          bio: { $nin: ["", null] },
          "profilePicture.url": { $nin: [null, SchemaHelper.fileSchema.url.default] }
        }
      },
      { $sample: { size: 40 } },
      { ...WhitelistHelper.associatedUserFields() }
    ]);

    return res.json({ users });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const getLearningRecommendations = async (req, res) => {
  try {
    // get amount of articles requested from client - default to 3 articles
    const { amount = 3 } = req.query;
    promotedService.getPromotedLearningArticlesAsync(amount);
    const learningRecommendations = await RedisHelper.instance().getAsync(RedisRecommendationsEnum.LEARNING);

    const result = learningRecommendations && JSON.parse(learningRecommendations);
    if (!result) {
      return res.json({ articles: [] });
    }

    return res.json({ articles: result });
  } catch (error) {
    logger.error(error, "500 Error in getLearningRecommendations() - recommendations_controller.js");
    res.status(500).json(error);
  }
};

const getOpportunitiesRecommendations = async (req, res) => {
  const logger = req.log.child({ func: "getOpportunitiesRecommendations" });
  try {
    // get amount of opportunities requested from client - default to 3 opportunities
    const { amount = 3 } = req.query;
    promotedService.getPromotedOpportunitiesAsync(amount);
    const opportunitiesRecommendations = await RedisHelper.instance().getAsync(RedisRecommendationsEnum.OPPORTUNITIES);

    const opportunities = opportunitiesRecommendations && JSON.parse(opportunitiesRecommendations);
    if (!opportunities) {
      return res.json({ opportunities: [] });
    }

    return res.json({ opportunities });
  } catch (error) {
    logger.error(error, "500 Error in getOpportunitiesRecommendations() - recommendations_controller.js");
    res.status(500).json(error);
  }
};

const getOrganizationRecommendations = async (req, res) => {
  const logger = req.log.child({ func: "getOrganizationRecommendations" });
  try {
    // get amount of organizations requested from client - default to 3 organizations
    const { amount = 3 } = req.query;
    promotedService.getPromotedOrganizationsAsync(amount);
    const organizationRecommendations = await RedisHelper.instance().getAsync(RedisRecommendationsEnum.ORGANIZATIONS);

    const organizations = organizationRecommendations && JSON.parse(organizationRecommendations);
    if (!organizations) {
      return res.json({ organizations: [] });
    }

    return res.json({ organizations });
  } catch (error) {
    logger.error(error, "500 Error in getOrganizationRecommendations() - recommendations_controller.js");
    res.status(500).json(error);
  }
};

const getUsersRecommendations = async (req, res) => {
  const logger = req.log.child({ func: "getUsersRecommendations" });
  try {
    // get amount of users requested from client - default to 3 users
    const { amount = 3 } = req.query;

    promotedService.getPromotedUsersAsync(amount);
    const usersRecommendations = await RedisHelper.instance().getAsync(RedisRecommendationsEnum.USERS);

    const users = usersRecommendations && JSON.parse(usersRecommendations);
    if (!users) {
      return res.json({ users: [] });
    }

    return res.json({ users });
  } catch (error) {
    logger.error(error, "500 Error in getUsersRecommendations() - recommendations_controller.js");
    res.status(500).json(error);
  }
};

module.exports.getFollowerRecommendations = getFollowerRecommendations;
module.exports.getLearningRecommendations = getLearningRecommendations;
module.exports.getOpportunitiesRecommendations = getOpportunitiesRecommendations;
module.exports.getOrganizationRecommendations = getOrganizationRecommendations;
module.exports.getUsersRecommendations = getUsersRecommendations;
