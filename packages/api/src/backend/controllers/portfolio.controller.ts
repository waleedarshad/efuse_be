import { Request, Response, NextFunction } from "express";
import { Failure } from "@efuse/contracts";
import { cloneDeep } from "lodash";

import { BaseController } from "./base.controller";
import { UserService } from "../../lib/users/user.service";
import { IUser, ISkill } from "../interfaces";
import { UserHelperService } from "../../lib/users/user-helper.service";
import { GeneralHelper } from "../helpers/general.helper";
import { ObjectId } from "../types";
import { LeanDocument } from "mongoose";

const NAME = "portfolio.controller";

export class PortfolioController extends BaseController {
  private $userService: UserService;
  private $userHelperService: UserHelperService;

  constructor() {
    super(NAME);
    this.$userService = new UserService();
    this.$userHelperService = new UserHelperService();
  }

  public managePortfolioBusinessSkills = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { body?: number[]; user?: IUser };

      if (!incomingUser) {
        throw Failure.BadRequest("Missing user");
      }

      const userObject: IUser | null = await this.getUser(incomingUser?._id);

      const businessSkills = <ISkill[]>userObject?.businessSkills || [];

      // Loop through new skills from client
      body?.forEach((newSkill) => {
        // Check if user already has skill
        const alreadyExists = businessSkills.filter((skill) => skill.value === newSkill).length !== 0;

        // Add new skill only if skill does not exist
        if (!alreadyExists) {
          businessSkills.push({ value: newSkill, order: 0 });
        }
      });

      businessSkills.forEach((skill, index) => {
        // Assign order based on array index
        // todo: refactor to have custom order
        skill.order = index;
      });

      const updatedUser = await this.updateUser(userObject._id, { businessSkills });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removePortfolioBusinessSkills = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, params } = req as { params?: { id?: number }; user?: IUser };

      if (params?.id) {
        Failure.BadRequest("missing porfolio Id");
      }

      const userObject: IUser | null = await this.getUser(incomingUser?._id);

      const parsedID = Number(req.params.id);

      const businessSkills =
        <ISkill[]>userObject.businessSkills?.filter((skill) => parsedID !== Number(skill.value)) || [];

      const updatedUser = await this.updateUser(userObject._id, { businessSkills });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public managePortfolioGamingSkills = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user: incomingUser } = req as { body?: number[]; user?: IUser };

      if (!incomingUser) {
        Failure.BadRequest("unable to find user");
      }

      const userObject: IUser | null = await this.getUser(incomingUser?._id);

      const gamingSkills = <ISkill[]>userObject?.gamingSkills || [];

      // Loop through new skills from client
      body?.forEach((newSkill) => {
        // Check if user already has skill
        const alreadyExists = gamingSkills.filter((skill) => skill.value === newSkill).length !== 0;

        // Add new skill only if skill does not exist
        if (!alreadyExists) {
          gamingSkills.push({ value: newSkill, order: 0 });
        }
      });

      gamingSkills.forEach((skill, index) => {
        // Assign order based on array index
        // todo: refactor to have custom order
        skill.order = index;
      });

      const updatedUser = await this.updateUser(userObject?._id, { gamingSkills });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removePortfolioGamingSkills = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, params } = req as { params?: { id?: number }; user?: IUser };

      if (params?.id) {
        Failure.BadRequest("missing porfolio Id");
      }

      const userObject = await this.getUser(incomingUser?._id);

      const parsedID = Number(req.params.id);

      const gamingSkills = <ISkill[]>userObject.gamingSkills?.filter((skill) => parsedID !== Number(skill.value)) || [];

      const updatedUser = await this.updateUser(userObject._id, { gamingSkills });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public manageEducationExperience = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const {
        user: incomingUser,
        body,
        files
      } = req as { files: { image?: Express.Multer.File[] }; body: Record<string, unknown>; user?: IUser };

      const user = await this.getUser(incomingUser?._id);

      const { educationExperience } = user;

      const { image } = files || {};

      // Create copy of existing educationExperience array
      const arrayToBeUpdated = cloneDeep(educationExperience) || [];

      // Handle Updating existing experience
      if (body._id) {
        // Find object
        let existingExperienceObject = arrayToBeUpdated.find((exp) => String(exp._id) === String(body._id));

        // Proceed when object is found
        if (existingExperienceObject) {
          // Remove image from body as it will handled via req.files
          delete body.image;

          // Update values
          existingExperienceObject = {
            ...existingExperienceObject,
            ...body,
            description: GeneralHelper.validateHtml(body.description)
          };

          // Set the uploaded Image
          if (image) {
            existingExperienceObject.image = GeneralHelper.buildFileObject(image);
          }

          // Find index of the object in array
          const objectIndex = arrayToBeUpdated.findIndex((exp) => String(exp._id) === String(body._id));

          // Set the updated object in array
          arrayToBeUpdated[objectIndex] = existingExperienceObject;
        }
      } else {
        // Handle adding new education experience

        const { _id, ...params } = body;

        const eExperience = { ...params };

        // Make sure to strip html
        eExperience.description = GeneralHelper.validateHtml(eExperience.description);

        // Upload Image
        if (image) {
          eExperience.image = GeneralHelper.buildFileObject(image);
        }

        // Push newly added object to array
        arrayToBeUpdated.push(<never>eExperience);
      }

      const updatedUser = await this.updateUser(user._id, { educationExperience: arrayToBeUpdated });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeEducationExperience = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);

      const { educationExperience } = user;

      const updatedUser = await this.updateUser(user._id, {
        educationExperience: educationExperience?.filter((exp) => String(exp._id) !== String(req.params.id)) || []
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public managePortfolioEvent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { user?: IUser; body: Record<string, any> };

      const user = await this.getUser(incomingUser?._id);

      if (!body) {
        throw Failure.BadRequest("invalid body");
      }

      const { portfolioEvents } = user;
      const arrayToBeUpdated = cloneDeep(portfolioEvents) || [];

      if (body._id) {
        let existingPortfolioEvent = portfolioEvents?.find((event) => String(event._id) === body._id);

        // Handle existing experience
        if (existingPortfolioEvent) {
          existingPortfolioEvent = {
            ...existingPortfolioEvent,
            ...body,
            description: GeneralHelper.validateHtml(body.description)
          };

          const objectIndex = arrayToBeUpdated.findIndex((event) => String(event._id) === String(body._id));

          arrayToBeUpdated[objectIndex] = existingPortfolioEvent;
        }
      } else {
        const { _id, ...params } = body;
        // Handle new porfolioEvent
        const pExperience = { ...params };
        pExperience.description = GeneralHelper.validateHtml(pExperience.description);

        arrayToBeUpdated.push(<never>pExperience);
      }

      const updatedUser = await this.updateUser(user._id, { portfolioEvents: arrayToBeUpdated });
      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removePortfolioEvent = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);

      const { portfolioEvents } = user;

      const updatedUser = await this.updateUser(user._id, {
        portfolioEvents: portfolioEvents?.filter((event) => String(event._id) !== String(req.params.id)) || []
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public managePortfolioHonor = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { user?: IUser; body: Record<string, any> };

      const user = await this.getUser(incomingUser?._id);

      if (!body) {
        throw Failure.BadRequest("invalid body");
      }

      const { portfolioHonors } = user;
      const arrayToBeUpdated = cloneDeep(portfolioHonors) || [];

      if (body._id) {
        let existingPortfolioHonors = portfolioHonors?.find((event) => String(event._id) === body._id);

        // Handle existing experience
        if (existingPortfolioHonors) {
          existingPortfolioHonors = {
            ...existingPortfolioHonors,
            ...body,
            description: GeneralHelper.validateHtml(body.description)
          };

          const objectIndex = arrayToBeUpdated.findIndex((event) => String(event._id) === String(body._id));

          arrayToBeUpdated[objectIndex] = existingPortfolioHonors;
        }
      } else {
        const { _id, ...params } = body;
        // Handle new porfolioEvent
        const _portfolioHonors = { ...params };
        _portfolioHonors.description = GeneralHelper.validateHtml(_portfolioHonors.description);

        arrayToBeUpdated.push(<never>_portfolioHonors);
      }

      const updatedUser = await this.updateUser(user._id, { portfolioHonors: arrayToBeUpdated });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removePortfolioHonor = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);

      const { portfolioHonors } = user;

      const updatedUser = await this.updateUser(user._id, {
        portfolioHonors: portfolioHonors?.filter((event) => String(event._id) !== String(req.params.id)) || []
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public manageBusinessExperience = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const {
        user: incomingUser,
        body,
        files
      } = req as {
        files: { image?: Express.Multer.File[] };
        user?: IUser;
        body: Record<string, any>;
      };

      const user = await this.getUser(incomingUser?._id);

      if (!body) {
        throw Failure.BadRequest("invalid body");
      }

      const { image } = files || {};

      const { businessExperience } = user;
      const arrayToBeUpdated = cloneDeep(businessExperience) || [];

      if (body._id) {
        let existingBusinessExperience = businessExperience?.find((event) => String(event._id) === body._id);

        // Handle existing experience
        if (existingBusinessExperience) {
          // Remove image from body as it will handled via req.files
          delete body.image;

          existingBusinessExperience = {
            ...existingBusinessExperience,
            ...body,
            description: GeneralHelper.validateHtml(body.description)
          };

          if (image) {
            existingBusinessExperience.image = GeneralHelper.buildFileObject(image);
          }

          const objectIndex = arrayToBeUpdated.findIndex((event) => String(event._id) === String(body._id));

          arrayToBeUpdated[objectIndex] = existingBusinessExperience;
        }
      } else {
        const { _id, ...params } = body;
        // Handle new porfolioEvent
        const _businessExperience = { ...params };
        _businessExperience.description = GeneralHelper.validateHtml(_businessExperience.description);

        // Upload Image
        if (image) {
          _businessExperience.image = GeneralHelper.buildFileObject(image);
        }

        if (_businessExperience.startDate && _businessExperience.startDate !== "null") {
          _businessExperience.startDate = new Date(_businessExperience.startDate).toISOString();
        } else {
          delete _businessExperience.startDate;
        }

        if (_businessExperience.endDate && _businessExperience.endDate !== "null") {
          _businessExperience.endDate = new Date(_businessExperience.endDate).toISOString();
        } else {
          delete _businessExperience.endDate;
        }

        arrayToBeUpdated.push(<never>_businessExperience);
      }

      const updatedUser = await this.updateUser(user._id, { businessExperience: arrayToBeUpdated });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeBusinessExperience = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);

      const { businessExperience } = user;

      const updatedUser = await this.updateUser(user._id, {
        businessExperience:
          businessExperience?.filter((experience) => String(experience._id) !== String(req.params.id)) || []
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public updateVideoInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as {
        user?: IUser;
        body?: { embeddedLink: string; youtubeChannelId: string };
      };

      if (!body) {
        throw Failure.BadRequest("invalid body");
      }

      const user = await this.getUser(incomingUser?._id);

      const updatedUser = await this.updateUser(user._id, {
        embeddedLink: body.embeddedLink,
        youtubeChannelId: body.youtubeChannelId
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public addYoutubeInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as {
        user?: IUser;
        body?: Record<string, any>;
      };

      if (!body) {
        throw Failure.BadRequest("invalid body");
      }

      const user = await this.getUser(incomingUser?._id);

      const updatedUser = await this.updateUser(user._id, {
        youtube: { ...body, createdAt: Date.now() }
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeYoutubeInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, { youtube: {} });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public addTiktokInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { user?: IUser; body: Record<string, any> };

      if (!body) {
        Failure.BadRequest("Invalid body");
      }

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, {
        tikTok: { ...body, createdAt: Date.now() }
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeTiktokInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, { tikTok: {} });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public addLinkedinInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { user?: IUser; body: { linkedinUsername: string } };

      if (!body) {
        Failure.BadRequest("Invalid body");
      }

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, {
        linkedinUsername: body.linkedinUsername
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeLinkedinInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, { linkedinUsername: "" });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public addSnapchatInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { user?: IUser; body: Record<string, any> };

      if (!body) {
        Failure.BadRequest("Invalid body");
      }

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, {
        snapchat: { ...body }
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeSnapchatInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, { snapchat: { displayName: "" } });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public addInstagramUsername = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser, body } = req as { user?: IUser; body: { username: string } };

      if (!body) {
        Failure.BadRequest("Invalid body");
      }

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, {
        instagramUsername: body.username
      });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public removeInstagramUsername = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user: incomingUser } = req as { user?: IUser };

      const user = await this.getUser(incomingUser?._id);
      const updatedUser = await this.updateUser(user._id, { instagramUsername: "" });

      res.json({ updatedUser });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  private getUser = async (userId: ObjectId | undefined): Promise<IUser> => {
    const user = await this.$userService.findOne({ _id: userId }, {}, { lean: true });

    if (!user) {
      throw Failure.UnprocessableEntity("User not found");
    }
    return user;
  };

  private updateUser = async (userId: ObjectId, values: any): Promise<LeanDocument<IUser> | null> => {
    const user = await this.$userHelperService.update(userId, values);

    // Get updated user's object with all the required associated objects
    const updatedUser = await this.$userHelperService.getUserById(userId);
    return updatedUser;
  };
}
