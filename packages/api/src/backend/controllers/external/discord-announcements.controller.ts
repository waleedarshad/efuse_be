import { NextFunction, Request, Response } from "express";
import { BaseController } from "../base.controller";
import { getAirtableDatav2 } from "../../../lib/airtable";

const NAME = "discord-announcements.controller";
const AIRTABLE_BASE_NAME = "Announcements";
const AIRTABLE_VIEW_NAME = "Future Announcements";
const AIRTABLE_BASE = "appDW0G80V166q8aj";

export class DiscordAnnouncementsController extends BaseController {
  constructor() {
    super(NAME);
  }

  /**
   * @summary Get scheduled announcements from airtable
   *
   * @param {Request} _req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof DiscordAnnouncements
   */
  public handler = async (_req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const data = await getAirtableDatav2(AIRTABLE_BASE_NAME, AIRTABLE_VIEW_NAME, AIRTABLE_BASE);
      const now = new Date();

      const response = data
        .map((row) => ({
          type: row.get("Type"),
          linkUrl: row.get("Link"),
          dateTime: row.get("Release Date/Time") as string,
          id: row.id
        }))
        .filter((result) => result.type && result.linkUrl && result.dateTime)
        .filter((result) => new Date(result.dateTime));

      res.status(200).json({ announcements: response });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
