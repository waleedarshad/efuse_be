import { NextFunction, Request, Response } from "express";
import { BaseController } from "../base.controller";
import { Opportunity } from "../../models/Opportunity";
import { FilterQuery } from "mongoose";
import { IOpportunity } from "../../interfaces/opportunity";

const NAME = "discord-opportunities.controller";

export class DiscordOpportunitiesController extends BaseController {
  constructor() {
    super(NAME);
  }

  /**
   * @summary Search opportunities
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof DiscordOpportunitiesController
   */
  public search = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      let hideExternalOpportunities: FilterQuery<IOpportunity>;
      const { developer } = req;
      const { page } = req.query;

      hideExternalOpportunities = {
        $or: [{ external: { $exists: false } }, { external: false }]
      };

      if (developer.scope && developer.scope.includes("external_opportunities.read")) {
        hideExternalOpportunities = {};
      }

      const query: FilterQuery<IOpportunity> = {
        $and: [
          {
            $or: [{ publishStatus: { $in: ["open", "closed"] } }, { publishStatus: { $exists: false } }]
          },
          { $or: [{ status: "visible" }, { status: { $exists: false } }] },
          hideExternalOpportunities
        ],
        ...req.parsedFilters
      };

      const opportunities = await Opportunity.paginate(query, {
        page: parseInt(page as string),
        limit: 5,
        lean: true,
        populate: [],
        sort: { createdAt: -1 }
      });

      res.json({ opportunities });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
