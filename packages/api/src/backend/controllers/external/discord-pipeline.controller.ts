import { NextFunction, Request, Response } from "express";
import { BaseController } from "../base.controller";
import { Failure } from "@efuse/contracts";

import { ValorantLeaderboard } from "../../../backend/models/valorant/valorant-leaderboard";
import { AimlabLeaderboard } from "../../../backend/models/aimlab";
import { LeagueOfLegendsLeaderboard } from "../../../backend/models/league-of-legends";

const NAME = "discord-pipeline.controller";

const gameMapping = {
  aimlab: AimlabLeaderboard,
  leagueoflegends: LeagueOfLegendsLeaderboard,
  valorant: ValorantLeaderboard
};

export class DiscordPipelineController extends BaseController {
  constructor() {
    super(NAME);
  }

  /**
   * @summary Get pipeline leaderboard
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof DiscordPipelineController
   */
  public getLeaderboard = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query } = req;
      const { page, limit } = query;

      if (!params || !params.game) {
        throw Failure.BadRequest("Missing game parameter");
      }

      const game = params.game;

      if (!gameMapping.hasOwnProperty(game)) {
        throw Failure.BadRequest("Invalid game name.");
      }

      const leaderboardModel:
        | typeof AimlabLeaderboard
        | typeof LeagueOfLegendsLeaderboard
        | typeof ValorantLeaderboard = gameMapping[game];

      const leaderboard = await leaderboardModel.paginate(
        {},
        {
          select: { userId: 1, rank: 1 },
          page: parseInt(page as string),
          limit: parseInt(limit as string),
          sort: { rank: 1 },
          populate: [
            {
              path: "userId",
              select: ["name", "username"]
            }
          ],
          lean: true
        }
      );

      const docs = leaderboard.docs.map((doc) => {
        return {
          user: doc.userId,
          rank: doc.rank
        };
      });

      res.json({
        leaderboard: {
          ...leaderboard,
          docs
        }
      });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
