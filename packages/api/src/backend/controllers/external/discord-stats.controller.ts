import { NextFunction, Request, Response } from "express";
import { DocumentType } from "@typegoose/typegoose";
import { BaseController } from "../base.controller";
import { DiscordStat, DiscordStatModel } from "../../models/discord-stat/discord-stat.model";

const NAME = "discord-stats.controller";

export class DiscordStatsController extends BaseController {
  constructor() {
    super(NAME);
  }

  /**
   * @summary Create a new discord stat
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof DiscordStats
   */
  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const body = req.body as DocumentType<DiscordStat>;
      const { servers } = body;

      for (const server of servers) {
        server.serverName = server["Server Name"];
        delete server["Server Name"];
      }

      const discordStat = await DiscordStatModel.create({ ...body });
      this.logger.info("Discord Stats saved successfully!");

      res.json(discordStat);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
