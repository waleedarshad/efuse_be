import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";
import { AuthenticationMsgsEnum } from "../../lib/auth/auth.enum";

import { MagicService } from "../../lib/magic/magic.service";
import { IUserAgent } from "../interfaces";
import { BaseController } from "./base.controller";

const NAME = "session.controller";

export class SessionController extends BaseController {
  private $magicService: MagicService;

  constructor() {
    super(NAME);

    this.$magicService = new MagicService();
  }

  /**
   * @summary Login user using Magic Link
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof SessionController
   */
  public login = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { headers, useragent, ipInfo } = req;

      // Make sure Authorization headers are set
      if (!headers.authorization) {
        this.logger?.error("Empty Authorization Header");
        throw Failure.BadRequest(AuthenticationMsgsEnum.INVALID_LOGIN);
      }

      const reqBody: { platform: string } | undefined = req.body as { platform: string } | undefined;

      const response = await this.$magicService.login(
        headers?.authorization,
        reqBody?.platform,
        useragent as IUserAgent | undefined,
        ipInfo
      );

      res.json(response);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
