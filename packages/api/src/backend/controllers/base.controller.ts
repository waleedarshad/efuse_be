import { Failure } from "@efuse/contracts";
import { NextFunction } from "express";
import { ILogger, Logger } from "@efuse/logger";

/**
 * BaseController implements controller specific logging
 *
 * @note Use `this.logger` instead of instantiating a new @efuse/logger.
 */
export class BaseController {
  protected logger: ILogger;

  /**
   * Instantiates the `BaseController`
   *
   * @param controllerName
   */
  constructor(private name: string) {
    this.name = `be-${name}`;

    // set the logger so all services can log with the relevant service name
    this.logger = Logger.create({ name: this.name });
  }

  protected guard(resource: unknown, action: Error | Failure): void {
    if (resource === null || resource === undefined || resource === false) {
      throw action;
    }
  }

  protected handleError(error: Error, next: NextFunction): void {
    const httpError: Failure = <Failure>error;
    const message = httpError.message || "there was an error";

    this.logger.error(error, message);

    if (httpError.statusCode) {
      // handle specific Error
      next(error);
    } else {
      // handle general "throw Error"
      next(Failure.InternalServerError(message));
    }
  }
}
