import { Request, Response, NextFunction } from "express";
import { Failure } from "@efuse/contracts";
import { BaseController } from "./base.controller";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { ExperienceService } from "../../lib/experiences/experience.service";
import { Experience } from "../models/experience.model";
import { DocumentType } from "@typegoose/typegoose";

const NAME = "experiences.controller";

export class ExperienceController extends BaseController {
  private $experienceService: ExperienceService;

  constructor() {
    super(NAME);
    this.$experienceService = new ExperienceService();
  }

  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const body: DocumentType<Experience> | undefined = req.body as undefined | DocumentType<Experience>;

      if (!body || !body.name) {
        throw Failure.BadRequest("Name is a required field");
      }
      const { name } = body;
      const foundExperience = <DocumentType<Experience>>await this.$experienceService.findOne({ name });

      if (foundExperience) {
        res.json({ error: "Experience already exists" });
      }
      const experience = await this.$experienceService.create(body);

      EFuseAnalyticsUtil.incrementMetric("experiences_controller.create.success");
      res.json(experience);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("experiences_controller.create.failure");

      this.handleError(error, next);
    }
  };

  public index = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    EFuseAnalyticsUtil.incrementMetric("experiences_controller.index.call");

    try {
      const experiences = await this.$experienceService.getAllExperiences();
      EFuseAnalyticsUtil.incrementMetric("experiences_controller.index.success");
      res.status(200).json(experiences);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("experiences_controller.index.failure");
      this.handleError(error, next);
    }
  };
}
