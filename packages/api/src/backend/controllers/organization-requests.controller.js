const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { Failure } = require("@efuse/contracts");

const { GeneralHelper } = require("../helpers/general.helper");
const { Member } = require("../models/member");
const { Organization } = require("../models/Organization");
const { OrganizationFollower } = require("../models/organization-follower");
const { OrganizationRequest } = require("../models/organization-request");
const { User } = require("../models/User");
const WhitelistHelper = require("../helpers/whitelist_helper");

const approve = async (req, res, next) => {
  try {
    const { params } = req;
    const updatedRequest = await OrganizationRequest.findOneAndUpdate(
      { _id: params.id },
      { status: "approved", updatedAt: new Date() }
    );

    if (!updatedRequest) {
      return next(Failure.UnprocessableEntity("Organization Request not found"));
    }

    try {
      await new Member({
        organization: updatedRequest.organization,
        user: updatedRequest.user,
        createdAt: Date.now()
      }).save();

      try {
        await new OrganizationFollower({
          follower: updatedRequest.user,
          followee: updatedRequest.organization
        }).save();

        return res.json({
          updatedRequest
        });
      } catch (error) {
        logger.error(error);
        return res.status(500).json({ error });
      }
    } catch (error) {
      logger.error(error);
      return res.status(500).json({ error });
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const create = async (req, res) => {
  try {
    const { body, user } = req;
    const createdRequest = await new OrganizationRequest({
      organization: body.organizationId,
      user: user.id,
      status: body.status,
      updatedAt: body.updatedAt
    }).save();

    return res.json({
      request: createdRequest
    });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const index = async (req, res) => {
  try {
    const { page, pageSize, id } = req.query;
    const response = await organizationValid({ _id: id });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const organization = await Organization.findOne({ _id: id });
    const canAccess =
      // todo: investigate
      // @ts-ignore
      (organization.captains && organization.captains.includes(req.user.id)) ||
      // todo: investigate
      // @ts-ignore
      String(organization.user) === String(req.user.id);
    try {
      // todo: investigate
      // @ts-ignore
      const orgRequests = await OrganizationRequest.aggregatePaginate(requestsAggregate(req), {
        page: page,
        limit: pageSize,
        populate: "user",
        sort: { createdAt: -1 }
      });

      return res.json({
        orgRequests: orgRequests,
        canAccess: canAccess
      });
    } catch (error) {
      logger.error(error);
      return res.status(500).json(error);
    }
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ error });
  }
};

const organizationValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Organization Id" };
  }

  const organizationExist = await Organization.exists(query);
  if (!organizationExist) {
    return { error: true, statusCode: 422, message: "Organization not found" };
  }
  return { error: false };
};

const reject = async (req, res, next) => {
  try {
    const { params } = req;

    const updatedDoc = await OrganizationRequest.findOneAndUpdate(
      { _id: params.id },
      { status: "rejected" },
      { lean: true }
    );

    if (!updatedDoc) {
      throw Failure.UnprocessableEntity("Organization Request not found");
    }

    const user = await User.findOne({ _id: updatedDoc.user });

    return res.send({ rejRequest: { ...updatedDoc, associatedUser: user } });
  } catch (error) {
    logger.error(error);
    next(error);
  }
};

const requestsAggregate = (req, res) => {
  try {
    const organizationId = GeneralHelper.getObjectId(req.query.id);
    const orgsAggregate = OrganizationRequest.aggregate([
      {
        $match: { organization: organizationId, status: { $nin: ["approved"] } }
      },
      {
        $lookup: {
          from: "users",
          let: { userId: "$user" },
          pipeline: [
            {
              $match: {
                $expr: {
                  $and: [{ $eq: ["$_id", "$$userId"] }]
                }
              }
            },
            {
              ...WhitelistHelper.associatedUserFields()
            }
          ],
          as: "associatedUser"
        }
      },
      { $unwind: { path: "$associatedUser", preserveNullAndEmptyArrays: true } }
    ]);
    return orgsAggregate;
  } catch (error) {
    logger.error(error);
    res.status(500).json({ err: error.message });
  }
};

module.exports.approve = approve;
module.exports.create = create;
module.exports.index = index;
module.exports.organizationValid = organizationValid;
module.exports.reject = reject;
module.exports.requestsAggregate = requestsAggregate;
