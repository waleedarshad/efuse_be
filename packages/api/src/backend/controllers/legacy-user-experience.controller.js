const { UserExperience } = require("../models/UserExperience");
const mongoose = require("mongoose");
const experienceslib = require("../../lib/experiences");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");

/**
 * @summary Grab the current user's list of experiences needing displayed
 *
 * @param  {Express.Request} req - Pass the current user in the request
 * @param  {Express.Response} res - An express response object
 * @return {Promise<any>} Returns a list of user experiences and whether or not to display them
 * @example .get(`/users/get_user_experience`)
 */
const index = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("user_experience_controller.index.call");
  try {
    // todo: investigate
    // @ts-ignore
    const experiences = await experienceslib.getDisplayedUserExperiences(req.user.id);
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.index.success");
    // todo: investigate
    // @ts-ignore
    return res.status(200).json(experiences);
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.index.failure");
    logger.error({ error: error });
    // todo: investigate
    // @ts-ignore
    res.status(500).json({ error });
    return;
  }
};

/**
 * @summary Loop through all existing experiences and create userExperiences
 *
 * @param  {Express.Request} req - Pass the current user in the request
 * @param  {Express.Response} res - An express response object
 * @return {Promise<any>} Returns a list of the created experiences
 */
const createAllExperiences = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("user_experience_controller.createAllExperiences.call");
  try {
    // todo: investigate
    // @ts-ignore
    const experiences = await experienceslib.createAllUserExperiences(req.user.id);
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.createAllExperiences.success");
    // todo: investigate
    // @ts-ignore
    return res.status(200).json(experiences);
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.createAllExperiences.failure");
    logger.error({ error: error });

    // todo: investigate
    // @ts-ignore
    res.status(500).json({ error });
    // todo: investigate
    // @ts-ignore
    return;
  }
};

/**
 * @summary Creates a userExperience given a specific experience and user
 *
 * @param  {Express.Request} req - Pass the current user in the request
 * @param  {Express.Response} res - An express response object
 * @return {Promise<any>} Returns a the new userExperience
 */
const create = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("user_experience_controller.create.call");
  try {
    // todo: investigate
    // @ts-ignore
    const { body } = req;
    // todo: investigate
    // @ts-ignore
    const newUserExperience = await experienceslib.createUserExperience(body.experience, req.user.id);
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.create.success");
    // todo: investigate
    // @ts-ignore
    return res.status(200).json(newUserExperience);
  } catch (error) {
    // todo: investigate
    // @ts-ignore
    logger.error({ error: error });
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.create.failure");
    // todo: investigate
    // @ts-ignore
    return res.status(500).json({ error });
  }
};

/**
 * @summary Increments the views for a global experience and/or the subViews in that experience
 *
 * @param  {Express.Request} req - Pass the current user in the request and the body of the experience you'd like to update
 * @param  {Express.Response} res - An express response object
 * @return {Promise<any>} Returns the updated userExperience
 * @example .put(`/users/increment_views`, {experienceId: experienceId, isViewed: true, isGlobal: false, viewName: subView})
 */
const incrementViews = async (req, res) => {
  EFuseAnalyticsUtil.incrementMetric("user_experience_controller.incrementViews.call");
  try {
    // todo: investigate
    // @ts-ignore
    const { body, user } = req;
    const { isViewed, isGlobal } = body;
    if (isGlobal) {
      const incrementQuery = {
        ...(isViewed ? { "metrics.globalViews": 1 } : { "metrics.globalSkips": 1 })
      };
      await incrementCounter(res, body, user, incrementQuery);
    } else {
      const incrementQuery = {
        ...(isViewed ? { "metrics.subViews.$.views": 1 } : { "metrics.subViews.$.skips": 1 })
      };
      const findQuery = { "metrics.subViews.name": body.viewName };
      await incrementCounter(res, body, user, incrementQuery, findQuery);
    }
  } catch (error) {
    logger.error({ error: error });
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.incrementViews.failure");

    // todo: investigate
    // @ts-ignore
    return res.status(500).json({ error });
  }
};

const incrementCounter = async (res, body, user, incrementQuery, findQuery = {}) => {
  try {
    const userExperience = await UserExperience.findOneAndUpdate(
      { experience: body.experienceId, user: user.id, ...findQuery },
      { $inc: { ...incrementQuery } }
    );
    EFuseAnalyticsUtil.incrementMetric("user_experience_controller.incrementViews.success");
    res.status(200).json(userExperience);
  } catch (error) {
    logger.error({ error: error });
    return res.status(500).json({ error });
  }
};

module.exports.create = create;
module.exports.createAllExperiences = createAllExperiences;
module.exports.incrementCounter = incrementCounter;
module.exports.incrementViews = incrementViews;
module.exports.index = index;
