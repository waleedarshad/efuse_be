const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { GiphyModel } = require("../models/giphy/giphy.model");
const { MediaService } = require("../../lib/media/media.service");
const { StreaksService } = require("../../lib/streaks/streaks.service");
const { Comment } = require("../models/comment");
const CommentsLib = require("../../lib/users/comments");
const { GeneralHelper } = require("../helpers/general.helper");
const { HypeModel } = require("../models/hype/hype.model");
const UsersLib = require("../../lib/users");
const { GiphyService } = require("../../lib/giphy/giphy.service");
const { MediaTypeEnum } = require("../../lib/media/media.enum");
const { CommentService } = require("../../lib/comments/comment.service");
const { HypeEnum } = require("@efuse/entities");

const streaksService = new StreaksService();
const mediaService = new MediaService();
const giphyService = new GiphyService();
const commentService = new CommentService();
const $hype = HypeModel;

const feedComment = async (req, res, next) => {
  try {
    const { user, body, params } = req;

    const newComment = await commentService.createFeedComment(user, { ...body, feedId: params.feedId });

    // Build comment object
    const comment = {
      comment: {
        ...newComment.toObject(),
        associatedUser: {
          _id: user.id,
          name: user.name,
          roles: user.roles,
          profilePicture: user.profilePicture,
          username: user.username,
          verified: user.verified,
          streaks: await streaksService.lookupDailyStreaks(user._id)
        }
      }
    };

    // Append Giphy object
    if (newComment.giphy) {
      // todo: investigate
      // @ts-ignore
      comment.comment.associatedGiphy = await giphyService.findOne({ _id: newComment.giphy });
    }

    // Append Associated Media
    const associatedMedia = await mediaService.getAssociatedMedia(user._id, newComment._id, MediaTypeEnum.COMMENT);

    if (associatedMedia.media) {
      // todo: investigate
      // @ts-ignore
      comment.comment.associatedMedia = associatedMedia.media;
    }

    // Dispatch websocket event
    await CommentsLib.addCommentToFeedAsync(params.feedId, user.id, comment);

    // Send response
    res.json(comment);
  } catch (error) {
    next(error);
  }
};

const getFeedComments = async (req, res) => {
  try {
    const comments = await commentAggregate(req, {
      commentableType: "feeds",
      commentable: GeneralHelper.getObjectId(req.params.feedId)
    });
    return res.json({ comments });
  } catch (error) {
    logger.error(error);
    res.status(500).json({ err: error.message });
  }
};

const deleteComment = async (req, res, next) => {
  const { user, params } = req;
  const { commentId } = params;
  try {
    await commentService.validatePermission(user, commentId);

    const deletedComment = await commentService.deleteComment(user, commentId);

    // todo: investigate
    // @ts-ignore
    await CommentsLib.removeCommentFromFeedAsync(deletedComment.commentable, user.id, commentId);

    // singleParentDeleted is currently used on FE & if it true then FE filters the comments to remove deleted comment
    res.json({ singleParentDeleted: true });
  } catch (error) {
    next(error);
  }
};

const updateComment = async (req, res, next) => {
  const { user, params, body } = req;
  const { commentId } = params;

  try {
    const comment = await commentService.updateFeedComment(commentId, user._id, body);

    // Get Associated Media
    const associatedMedia = await mediaService.getAssociatedMedia(user._id, comment._id, MediaTypeEnum.COMMENT);

    await CommentsLib.updateCommentOnFeedAsync(req.query.feedId, user.id, commentId, {
      content: body.content,
      mentions: body.mentions,
      ...associatedMedia
    });

    res.end("Comment Updated");
  } catch (error) {
    next(error);
  }
};

const parentComments = async (req, res) => {
  try {
    const comments = await buildThreadedComments(req, null, true);
    return res.json({ comments });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error.message });
  }
};

const commentThread = async (req, res) => {
  try {
    const comments = await buildThreadedComments(req, GeneralHelper.getObjectId(req.params.parent), false);
    return res.json({ comments });
  } catch (error) {
    logger.error(error);
    return res.status(500).json({ err: error.message });
  }
};

const buildThreadedComments = async (req, parent, thread) => {
  return new Promise(async (resolve, reject) => {
    try {
      const buildThread = await commentAggregate(
        req,
        {
          parent,
          commentableType: "feeds",
          commentable: GeneralHelper.getObjectId(req.params.feedId)
        },
        thread
      );
      return resolve(buildThread);
    } catch (error) {
      logger.error(`Error Fetching thread for comment: ${req.params.feedId}`, error);
      return reject(error);
    }
  });
};

const commentAggregate = async (req, query, threaded = false) => {
  return new Promise(async (resolve, reject) => {
    try {
      const userId = GeneralHelper.getObjectId(req.user.id);
      const queryParams = { ...req.query };
      const { page, pageSize } = queryParams;
      const comments = await Comment.paginate(
        { ...query },
        {
          page,
          limit: pageSize,
          lean: true,
          populate: [],
          sort: { createdAt: 1 }
        }
      );
      const commentsWithAssociatedObjects = comments.docs.map(async (comment) => {
        // todo: investigate
        // @ts-ignore
        comment.associatedUser = await UsersLib.getUserById(comment.user, {
          dailyStreak: true
        });
        if (!comment.deleted) {
          const like = await $hype
            .findOne({
              objId: comment._id,
              objType: HypeEnum.COMMENT,
              user: userId
            })
            .lean();

          if (like) {
            comment.like = like;
          }
          if (comment.giphy) {
            comment.associatedGiphy = await GiphyModel.findOne({
              _id: comment.giphy
            }).lean();
          }

          const associatedMedia = await mediaService.getAssociatedMedia(userId, comment._id, "comments");
          if (associatedMedia.media) {
            comment.associatedMedia = associatedMedia.media;
          }
        }
        if (threaded) {
          req.query = {
            page: 1,
            pageSize: 2
          };

          // todo: investigate
          // @ts-ignore
          comment.thread = await buildThreadedComments(req, comment._id, false);
        }
        if (comment.deleted) {
          delete comment.content;
          delete comment.mentions;
          delete comment.giphy;

          // todo: investigate
          // @ts-ignore
          delete comment.likes;
        }
        return comment;
      });
      const results = await Promise.all(commentsWithAssociatedObjects);
      comments.docs = results;
      return resolve(comments);
    } catch (error) {
      logger.error(error);
      return reject(error);
    }
  });
};

module.exports.buildThreadedComments = buildThreadedComments;
module.exports.commentAggregate = commentAggregate;
module.exports.commentThread = commentThread;
module.exports.deleteComment = deleteComment;
module.exports.feedComment = feedComment;
module.exports.getFeedComments = getFeedComments;
module.exports.parentComments = parentComments;
module.exports.updateComment = updateComment;
