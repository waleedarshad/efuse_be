const { OwnerKind } = require("@efuse/entities");
const { BACKEND_DOMAIN, DOMAIN, TWITTER_API_KEY, XBOX_CLIENT_ID, XBOX_CLIENT_SECRET } = require("@efuse/key-store");
const axios = require("axios");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const qs = require("qs");

const { User } = require("../models/User");
const { Friend } = require("../models/friend");

const { verifyWebhookSubscription, handleStreamChangedWebhook } = require("../../lib/twitch");

const TwitchHandlerLib = require("../../lib/creatortools/twitchHandler");
const TwitterCoreLib = require("../../lib/users/twitter");

const { SessionService } = require("../../lib/session/session.service");
const { Slack } = require("../../lib/slack");
const { EFuseAnalyticsUtil } = require("../../lib/analytics.util");

const { TwitchAsyncService } = require("../../lib/twitch/twitch-async.service");
const { TwitchClipAsyncService } = require("../../lib/twitch/twitch-clip-async.service");
const { TwitchFollowerAsyncService } = require("../../lib/twitch/twitch-follower-async.service");
const { TwitchVideoAsyncService } = require("../../lib/twitch/twitch-video-async.service");
const { TwitchAccountInfoAsyncService } = require("../../lib/twitch/twitch-account-info-async.service");

const { XboxLiveAuthService } = require("../../lib/xbox-live-auth/xbox-live-auth.service");

const WhitelistHelper = require("../helpers/whitelist_helper");

const traits = async (req, res, next) => {
  const { user } = req;

  const acceptableTraits = new Set([
    "GAMER",
    "STREAMER",
    "STUDENT",
    "CONTENT_CREATOR",
    "TEAM_MEMBER",
    "TEAM_ADMIN_OWNER",
    "BRAND_MANAGER_MARKETER",
    "INVESTOR",
    "COACH",
    "SCHOOL_ADMIN",
    "SOCIAL_MEDIA_MARKETER",
    "GRAPHIC_DESIGNER",
    "AGENT",
    "BUSINESS_PROFESSIONAL"
  ]);

  const acceptableMotivations = new Set([
    "NETWORK_WITH_GAMERS",
    "NETWORK_WITH_PROFESSIONALS",
    "APPLY_FOR_SCHOLARSHIPS",
    "CREATE_MY_PORTFOLIO",
    "POST_AND_SHARE_CONTENT",
    "FIND_A_JOB",
    "LEARN_MORE_ABOUT_GAMING",
    "DISCOVER_LOCAL_EVENTS",
    "JOIN_A_TEAM"
  ]);

  try {
    // check if trait is acceptable
    req.body.traits.forEach((trait) => {
      if (!acceptableTraits.has(trait)) {
        logger.error(`${trait} is not an acceptable trait`);
        res.status(500).json({});
      }
    });

    // check if motivation is acceptable
    req.body.motivations.forEach((motivation) => {
      if (!acceptableMotivations.has(motivation)) {
        logger.error(`${motivation} is not an acceptable motivation`);
        res.status(500).json({});
      }
    });

    user.traits = req.body.traits;
    user.motivations = req.body.motivations;
    await user.save({ validateBeforeSave: false });
    return res.json({ user: WhitelistHelper.userPayload(user) });
  } catch (error) {
    logger.error(error);
    return res.status(400).json(error);
  }
};

const verifyTwitchWebhook = async (req, res) => {
  try {
    const challenge = req.query["hub.challenge"] ? req.query["hub.challenge"] : null;
    const { userId } = req.query;
    await verifyWebhookSubscription(BACKEND_DOMAIN, userId, challenge);
    res.send(challenge);
  } catch (error) {
    logger.info("Failed to verify Twitch webhook subscription", error);
  }
  res.end();
};

const handleTwitchWebhook = async (req, res) => {
  try {
    EFuseAnalyticsUtil.incrementMetric("twitch.incomingTwitchWebhook.calls");

    const webhookData = req.body.data;
    const { userId } = req.query;
    if (!userId) {
      logger.error(`[${userId}] userId parameter missing.  Not updating twitch for user`);
      EFuseAnalyticsUtil.incrementMetric("twitch.incomingTwitchWebhook.failure");
      res.status(400).end();
      return;
    }
    await handleStreamChangedWebhook(userId, webhookData);
    EFuseAnalyticsUtil.incrementMetric("twitch.incomingTwitchWebhook.success");
    res.end();
  } catch (error) {
    EFuseAnalyticsUtil.incrementMetric("twitch.incomingTwitchWebhook.failure");
    logger.error({ error }, "Failed to handle Twitch webhook subscription");
    res.status(500).end();
  }
};

const authenticateTwitter = async (req, res, next) => {
  logger.info("TwitterAuth backend called");

  const { twitter_auth_token, oauth_verifier } = req.body.data;
  const { user } = req;

  try {
    // todo: investigate
    // @ts-ignore
    const result = await axios.post(
      `https://api.twitter.com/oauth/access_token?oauth_consumer_key=${TWITTER_API_KEY}&oauth_token=${twitter_auth_token}&oauth_verifier=${oauth_verifier}`
    );

    const twitterData = {};
    result.data.split("&").forEach((keyValueStr) => {
      const [k, v] = keyValueStr.split("=");
      twitterData[k] = v;
    });

    user.twitterId = twitterData.user_id;
    user.twitterOauthToken = twitterData.oauth_token;
    user.twitterOauthTokenSecret = twitterData.oauth_token_secret;
    user.twitterUsername = twitterData.screen_name;
    user.twitterVerified = true;
    await user.save({
      validateBeforeSave: false
    });
    logger.info("Updated user object successfully");
    await TwitterCoreLib.getTwitterFollowersAsync(twitterData.screen_name);
    return res.send({ twitterVerified: true });
  } catch (error) {
    logger.warn(error.message ? error.message : error);
    return res.status(401).send({ error });
  }
};

const userObjectUpdater = async (userObject, user, res, twitchAuth = false) => {
  try {
    let localUserObject = userObject;
    localUserObject = Object.assign(user, localUserObject);
    localUserObject = await deleteAccountRequest(localUserObject, user);

    if (!localUserObject) {
      return res.json({
        message: "User deleted Successfully",
        deleted: true
      });
    }

    localUserObject = verifyAccountRequest(localUserObject, user);
    if ("__v" in localUserObject) {
      delete localUserObject.__v;
    }
    const updatedUser = await localUserObject.save({ validateBeforeSave: false });

    if (twitchAuth) {
      const twitchAsyncService = new TwitchAsyncService();
      const twitchClipAsyncService = new TwitchClipAsyncService();
      const twitchFollowersAsyncService = new TwitchFollowerAsyncService();
      const twitchVideoAsyncService = new TwitchVideoAsyncService();
      const twitchAccountInfoAsyncService = new TwitchAccountInfoAsyncService();

      twitchAsyncService.subscribeCronsAsync(user._id);
      twitchClipAsyncService.syncTwitchClipsAsync(user._id, OwnerKind.USER);
      twitchFollowersAsyncService.syncTwitchFollowersAsync(user._id, OwnerKind.USER);
      twitchVideoAsyncService.syncTwitchVideosAsync(user._id, OwnerKind.USER);
      twitchAccountInfoAsyncService.syncTwitchInfoAsync(user._id, OwnerKind.USER);

      await TwitchHandlerLib.subscribeToStreamChangeWebhookAsync(user._id, OwnerKind.USER);
    }
    updatedUser.populate("mainOrganization").populate("affiliatedGames.affiliatedGame", (err, popUser) => {
      return res.json({ user: popUser });
    });
  } catch (error) {
    console.log(error);
    logger.error(error);
    return res.status(400).json(error);
  }
};

const deleteAccountRequest = async (incoming, user) => {
  const sessionService = new SessionService();
  const userObject = incoming;

  if (userObject.deleteAccountRequest) {
    userObject.deleteAccountRequestDate = new Date();
    const deleteUser = await User.delete({ _id: userObject._id });
    if (deleteUser) {
      await sessionService.revokeSessionAsync(userObject._id);

      await Slack.sendMessage(`The user with email account *${userObject.email}* is deleted.`, "#privacy");

      return null;
    }
  }

  return userObject;
};

const verifyAccountRequest = (userObject, user) => {
  if (
    userObject.verifyAccountRequest &&
    (user.verifyAccountRequestDate === undefined ||
      new Date().setHours(0, 0, 0, 0) > user.verifyAccountRequestDate.setHours(0, 0, 0, 0))
  ) {
    userObject.verifyAccountRequestDate = new Date();
  }
  return userObject;
};

const xboxAuth = async (req, res, next) => {
  const { user, body } = req;
  const errorMsg = "Can not verify account";
  try {
    if (!body.code) {
      req.log.info("Code not found in params!");
      return res.status(400).json({ error: errorMsg, failure: true });
    }
    const data = {
      client_id: XBOX_CLIENT_ID,
      grant_type: "authorization_code",
      scope: "Xboxlive.signin Xboxlive.offline_access",
      code: body.code,
      redirect_uri: `${DOMAIN}/settings/external_accounts/xboxlive`,
      client_secret: XBOX_CLIENT_SECRET
    };
    const params = qs.stringify(data);

    // todo: investigate
    // @ts-ignore
    const response = await axios.post(`https://login.live.com/oauth20_token.srf?${params}`, params, {
      headers: { "Content-Type": "application/x-www-form-urlencoded" }
    });

    const $xboxService = new XboxLiveAuthService();
    const xboxData = response.data;

    const xboxLiveData = {
      user: user._id,
      refreshToken: xboxData.refresh_token,
      accessToken: xboxData.access_token,
      authenticationToken: xboxData.authenticationToken
    };

    await $xboxService.create(xboxLiveData);

    return await userObjectUpdater(
      {
        xboxVerified: true,
        xboxUserId: xboxData.user_id
      },
      user,
      res
    );
  } catch (error) {
    let errorMsg = error.message;
    let status = 500;
    if (error.response && error.response.data) {
      errorMsg = error.response.data.error_description;
      status = 400;
    }
    req.log.info({ message: errorMsg }, `Error while exchanging code with token | ${errorMsg}`);
    return res.status(status).json({ error: errorMsg, failure: false });
  }
};

module.exports.authenticateTwitter = authenticateTwitter;
module.exports.deleteAccountRequest = deleteAccountRequest;
module.exports.handleTwitchWebhook = handleTwitchWebhook;
module.exports.traits = traits;
module.exports.userObjectUpdater = userObjectUpdater;
module.exports.verifyAccountRequest = verifyAccountRequest;
module.exports.verifyTwitchWebhook = verifyTwitchWebhook;
module.exports.xboxAuth = xboxAuth;
