import { NextFunction, Request, Response } from "express";
import { UserSettingsService } from "../../lib/user-settings/user-settings";
import { BaseController } from "./base.controller";

const NAME = "user-settings.controller";

export class UserSettingsController extends BaseController {
  private userSettingsService: UserSettingsService;

  constructor() {
    super(NAME);

    this.userSettingsService = new UserSettingsService();
  }

  /**
   * @deprecated - use Graphql getUserSettings
   * Get notification settings for current user
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof UserSettingsController
   */
  public getNotificationSettings = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = req as { user: any };
      const userSettings = await this.userSettingsService.get(user._id, { notifications: 1 });

      res.json(userSettings);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated - use Graphql updateUserSettings
   * Update notification settings for current user
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof UserSettingsController
   */
  public updateNotificationSettings = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = req as { body: any; params: any; query: any; user: any };
      const userSettingsBody = { notifications: body.notifications };

      const userSettings = await this.userSettingsService.update(user._id, userSettingsBody, { notifications: 1 });
      res.json(userSettings);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated - use Graphql getUserSettings
   * Get media settings for current user
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof UserSettingsController
   */
  public getMediaSettings = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { user } = req as { user: any };
      const userSettings = await this.userSettingsService.get(user._id, { media: 1 });

      res.json(userSettings);
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * @deprecated - use Graphql updateUserSettings
   * Update media settings for current user
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof UserSettingsController
   */
  public updateMediaSettings = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = req as { body: any; user: any };
      const userSettingsBody = { media: body.media };

      const userSettings = await this.userSettingsService.update(user._id, userSettingsBody, { media: 1 });
      res.json(userSettings);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
