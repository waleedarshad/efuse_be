import { NextFunction, Request, Response } from "express";
import { MemberBusinessRulesService } from "../../lib/member/member-business-rules.service";
import { BaseController } from "./base.controller";

const NAME = "members.controller";

export class MembersController extends BaseController {
  private memberBusinessRulesService: MemberBusinessRulesService;

  constructor() {
    super(NAME);

    this.memberBusinessRulesService = new MemberBusinessRulesService();
  }

  /**
   * @summary Update a particular member
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof MembersController
   */
  public update = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const member = await this.memberBusinessRulesService.updateMember(
        user._id,
        params.organizationId,
        params.memberId,
        body
      );

      res.json({ member });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
