const { MobileVersion } = require("../../models/MobileVersion");

const getLatestVersion = (req, res) => {
  const { platform, version } = req.params;
  MobileVersion.find({ platform, version })
    .sort({ createdAt: -1 })
    .limit(1)
    .select("platform version supported unsupportedDate status")
    .then((version) => res.json({ version: version.length > 0 ? version[0] : {} }))
    .catch((err) => {
      return res.status(500).json(err);
    });
};

module.exports.getLatestVersion = getLatestVersion;
