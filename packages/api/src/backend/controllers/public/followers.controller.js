const { Friend } = require("../../models/friend");

const countFriends = (req, res) => {
  const { id } = req.query;
  if (!id) {
    return res.status(400).json({ error: "Bad request" });
  }
  Promise.all([Friend.countDocuments({ followee: id }).exec(), Friend.countDocuments({ follower: id }).exec()])
    .then((counts) => res.json({ followers: counts[0], followees: counts[1] }))
    .catch((error) => res.status(400).json({ error: error.message || error }));
};

module.exports.countFriends = countFriends;
