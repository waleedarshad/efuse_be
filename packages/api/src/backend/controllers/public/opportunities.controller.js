const _ = require("lodash");
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Opportunity } = require("../../models/Opportunity");
const { GeneralHelper } = require("../../helpers/general.helper");

const ViewsQueue = require("../../../async/views");
const OpportunityLib = require("../../../lib/opportunities");

const show = async (req, res) => {
  let id = null;
  try {
    const paramsId = req.params.id;
    const response = await opportunityValid({ _id: paramsId });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    id = GeneralHelper.getObjectId(paramsId);
    const opportunity = await getOpportunity(req, res, { _id: id });
    return res.json(opportunity);
  } catch (err) {
    logger.error("err", err);
    return res.status(500).json({ err: err.message });
  }
};

const showBySlug = async (req, res) => {
  let slug = null;
  try {
    slug = req.params.slug;
    const response = await opportunityValid({ shortName: slug });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const opportunity = await getOpportunity(req, res, { shortName: slug });
    return res.json(opportunity);
  } catch (err) {
    logger.error("err", err);
    logger.info("res", res);
    res.status(500).json({ err: err.message });
  }
};

const alias = (req, res) => {
  Opportunity.findOne({ shortName: req.params.name.toLowerCase() })
    .select("_id")
    .then((opp) => {
      if (opp === null) {
        logger.warn(`Invalid opportunity alias: ${req.params.name}`);
        res.status(422).json({});
      } else {
        res.status(200).json({ id: opp._id });
      }
    })
    .catch((error) => {
      logger.error(error);

      res.status(500).json(error.message);
    });
};

const getOpportunity = async (req, res, query, extraData = {}) => {
  req.query.page = 1;
  req.query.pageSize = 1;
  return opportunitiesAggregate(req, query)
    .then((opportunity) => {
      if (_.isEmpty(opportunity) || opportunity.docs.length === 0) {
        return res.status(404).json({ opportunity: "not found!" });
      }
      const firstDoc = opportunity.docs[0];
      ViewsQueue.incrementOpportunityViews([firstDoc._id]).catch((error) =>
        logger.error(error, "Error resolving promise")
      );
      return { opportunity: opportunity.docs[0], ...extraData };
    })
    .catch((err) => {
      logger.error(err.message);
      res.status(500).json(err.message);
    });
};

const opportunitiesAggregate = (req, query = {}) => {
  return new Promise((resolve, reject) => {
    try {
      const { page, pageSize } = req.query;
      Opportunity.paginate(query, {
        page,
        limit: parseInt(pageSize),
        lean: true,
        populate: [],
        sort: { external: 1, createdAt: -1, randomSeed: -1 }
      })
        .then(async (opportunities) => {
          const formattedResponse = opportunities.docs.map(async (opportunity) => {
            opportunity = await OpportunityLib.buildAggregate(opportunity, req.user);
            return opportunity;
          });
          Promise.all(formattedResponse)
            .then((results) => {
              opportunities.docs = _.shuffle(results);
              resolve(opportunities);
            })
            .catch((error) => logger.error(error, "Error resolving promise"));
        })
        .catch((err) => {
          logger.error(err);
          reject(err);
        });
    } catch (err) {
      logger.error(err);
      reject(err.message);
    }
  });
};

const opportunityValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Opportunity Id" };
  }
  const opportunityExists = await Opportunity.exists(query);
  if (!opportunityExists) {
    logger.error("Opportunity not found");
    return { error: true, statusCode: 422, message: "Opportunity not found" };
  }
  return { error: false };
};

module.exports.alias = alias;
module.exports.getOpportunity = getOpportunity;
module.exports.opportunitiesAggregate = opportunitiesAggregate;
module.exports.opportunityValid = opportunityValid;
module.exports.show = show;
module.exports.showBySlug = showBySlug;
