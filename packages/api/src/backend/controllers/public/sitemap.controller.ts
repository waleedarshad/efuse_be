import { Request, Response, NextFunction } from "express";

import { BaseController } from "../base.controller";
import { SitemapService } from "../../../lib/sitemap";

const NAME = "sitemap.controller";

export class SitemapController extends BaseController {
  service = new SitemapService();

  constructor() {
    super(NAME);
  }

  public createSitemap = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      await this.service.setSitemap();
      res.send("OK");
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
