const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { Organization } = require("../../models/Organization");
const { GeneralHelper } = require("../../helpers/general.helper");
const { OrganizationFollower } = require("../../models/organization-follower");
const { User } = require("../../models/User");
const { Member } = require("../../models/member");
const { OrganizationGameService } = require("../../../lib/organizations/organization-game.service");

const memberAggregate = async (organization, res, isOrgShow = false) => {
  try {
    const organizationGameService = new OrganizationGameService();

    const associatedUser = await User.findOne({ _id: organization.user })
      .lean()
      .select("name username verified profilePicture roles");
    organization.associatedUser = associatedUser;
    const membersCount = await Member.countDocuments({
      organization: organization._id
    });
    organization.membersCount = membersCount;
    if (isOrgShow) {
      const followersCount = await OrganizationFollower.countDocuments({
        followee: organization._id
      });
      organization.followersCount = followersCount;
    }

    organization.associatedGames = await organizationGameService.getAssociatedGames(organization._id);

    return organization;
  } catch (error) {
    logger.error("failed to get organization data in memberAggregate()", error);
    return res.status(500).json({ error: error.message || error });
  }
};

const organizationValid = async (query) => {
  if (query._id && !GeneralHelper.isValidObjectId(query._id)) {
    return { error: true, statusCode: 400, message: "Invalid Organization Id" };
  }

  const organizationExist = await Organization.exists(query);
  if (!organizationExist) {
    return { error: true, statusCode: 422, message: "Organization not found" };
  }
  return { error: false };
};

const searchByName = (req, res) => {
  const { name } = req.query;
  Organization.find({ name: { $regex: new RegExp(name, "i") } })
    .then((organizations) => {
      res.json({ organizations });
    })
    .catch((error) => {
      logger.error(error);
      res.status(500).json({ error });
    });
};

const show = async (req, res) => {
  try {
    let { id } = req.params;
    let query;

    if (id.length < 24) {
      query = { slug: id };
      const response = await organizationValid(query);
      if (response.error) {
        return res.status(response.statusCode).json({ message: response.message });
      }
    } else {
      const response = await organizationValid({ _id: req.params.id });
      if (response.error) {
        return res.status(response.statusCode).json({ message: response.message });
      }
      id = GeneralHelper.getObjectId(req.params.id);
      query = { _id: id };
    }

    const organization = await Organization.findOne(query).lean();

    const aggregatedOrganization = await memberAggregate(organization, res, true);
    return res.json({ organization: aggregatedOrganization });
  } catch (error) {
    logger.error("failed to retrieve organization data in show()", error);
    return res.status(500).json({ error: error.message || error });
  }
};

const showBySlug = async (req, res) => {
  let slug = null;
  try {
    slug = req.params.slug;
    const response = await organizationValid({ shortName: slug });
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const organization = await Organization.findOne({ shortName: slug }).lean();
    const aggregatedOrganization = await memberAggregate(organization, res, true);
    return res.json({ organization: aggregatedOrganization });
  } catch (error) {
    logger.error("Failed to retrieve organization data by slug", error);
    res.status(500).json(error.message);
  }
};

const showByInvite = async (req, res) => {
  try {
    const { inviteCode } = req.params;

    logger.info("inviteCode", inviteCode);
    const organization = await Organization.findOne({ inviteCode }).lean();

    if (!organization) {
      return res.status(422).json({ error: "Organization not found" });
    }

    const aggregatedOrganization = await memberAggregate(organization, res, true);

    res.json({ organization: aggregatedOrganization });
  } catch (error) {
    logger.error("failed to retrieve organization data in show()", error);
    res.status(500).json({ error: error.message || error });
  }
};

module.exports.memberAggregate = memberAggregate;
module.exports.organizationValid = organizationValid;
module.exports.searchByName = searchByName;
module.exports.show = show;
module.exports.showByInvite = showByInvite;
module.exports.showBySlug = showBySlug;
