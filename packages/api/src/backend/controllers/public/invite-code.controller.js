// @ts-nocheck
// For some reason TypeScript checking is enabled in this file and it is throwing errors we can't
// easily fix given that it is a JavaScript file. Since they're not actually causing runtime issues
// we're disabling the checking until this file gets converted.
const { InviteCode } = require("../../models/invite-code");
const { User } = require("../../models/User");
const { Organization } = require("../../models/Organization");
const WhitelistHelper = require("../../helpers/whitelist_helper");

const getInviteCode = async (req, res) => {
  const { inviteCode } = req.params;

  try {
    // TODO: update to use invite code service
    let code = await InviteCode.findOne({ code: inviteCode });
    code = code ? code.toObject() : null;
    if (!code) {
      return res.status(422).json({ err: "This invite code does not exist" });
    }

    if (code.user) {
      const connectedUser = await User.findOne({
        _id: code.user
      })
        .lean()
        .select(WhitelistHelper.allowedUserFields);

      if (!connectedUser) return res.status(422).json({ err: "Invite code user does not exist." });

      code.user = connectedUser;
    }

    if (code.organization) {
      const connectedOrganization = await Organization.findOne({
        _id: code.organization
      }).lean();

      if (!connectedOrganization) return res.status(422).json({ err: "Invite code organization does not exist." });

      code.organization = connectedOrganization;
    }

    return res.json({ code });
  } catch (error) {
    const logger = req.log;
    logger.error(error);
    return res.status(500).json(error);
  }
};

module.exports.getInviteCode = getInviteCode;
