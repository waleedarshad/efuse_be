const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { LearningArticle } = require("../../models/learning-article");
const { LearningArticleCategory } = require("../../models/LearningArticleCategory");
const { User } = require("../../models/User");
const { Organization } = require("../../models/Organization");
const { LearningArticleGameService } = require("../../../lib/learningArticles/learning-article-game.service");
const { GeneralHelper } = require("../../helpers/general.helper");
const WhitelistHelper = require("../../helpers/whitelist_helper");

const learningArticleGameService = new LearningArticleGameService();

const getArticles = async (req, res) => {
  const { page, pageSize, filters } = req.query;
  let filtersObject = {};
  if (filters) {
    filtersObject = JSON.parse(filters);
  }

  // if searching by categorySlug
  // get category id and remove categorySlug in filtersObject
  if (filtersObject["categorySlug"]) {
    await LearningArticleCategory.findOne({
      slug: filtersObject["categorySlug"]
    })
      .then((category) => {
        if (category) {
          filtersObject["category"] = category._id;
        } else {
          logger.warn(`This category (${filtersObject["categorySlug"]}) does not exist.`);
          res.status(500).json({
            error: `This category (${filtersObject["categorySlug"]}) does not exist.`
          });
        }
      })
      .catch((error) => {
        logger.error(error);
        res.status(500).json(error);
      });
    delete filtersObject["categorySlug"];
  }

  /*
  Filters should be passed from FE, by doing so API will be more
  generic and in future we can search any field directly from FE,
  without making anychange to BE,
  For example to filter categories FE should send filters object as
  const filters = {category: {$in: []}}
  JSON.stringify(filters)
  */
  LearningArticle.paginate(
    {
      isActive: true,
      ...filtersObject
    },
    { page, limit: pageSize, lean: true, populate: [], sort: { createdAt: -1 } }
  )
    .then(async (articles) => {
      const articlesWithAssociatedObjects = articles.docs.map(async (article) => {
        // Associate Author Object
        if (article.authorType === "organizations") {
          article.author = await Organization.findOne({
            _id: article.author
          }).lean();
        } else {
          // todo: investigate
          // @ts-ignore
          article.author = await User.findOne({ _id: article.author })
            .lean()
            .select(`${WhitelistHelper.allowedUserFields} bio`);
        }
        // Associate Category
        article.category = await LearningArticleCategory.findOne({
          _id: article.category
        }).lean();
        // Bind Url
        // todo: investigate
        // @ts-ignore
        article.url = GeneralHelper.constructLearningArticleUrl(article.category, article);

        // Associate Games
        // @ts-ignore
        article.associatedGames = await learningArticleGameService.getAssociatedGames(article._id);

        return article;
      });
      Promise.all(articlesWithAssociatedObjects)
        .then((results) => {
          articles.docs = results;
          res.json({ articles });
        })
        .catch((error) => logger.error(error, "Error resolving articlesWithAssociatedObjects promise"));
    })
    .catch((error) => {
      logger.error(error);
      res.status(500).json(error);
    });
};

module.exports.getArticles = getArticles;
