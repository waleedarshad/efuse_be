const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { User } = require("../../models/User");
const crypto = require("crypto");
const camelCase = require("lodash/camelCase");
const { GeneralHelper } = require("../../helpers/general.helper");
const {
  getFullUserProfileById,
  getFullUserProfileByUsername,
  sendEmailVerificationEmail,
  sendResetPasswordEmail
} = require("../../../lib/users");

// todo: investigate
// @ts-ignore
const timelineUpdateFlag = (req, res) => {
  const { userId } = req.params;
  const payload = {
    userId,
    flag: req.body.flag
  };
  res.io.sockets.in(userId).emit("TIMELINE_UPDATE_FLAG", payload);
  res.end("Emitted Timeline Update");
};

const addCommentToFeed = (req, res) => {
  const { userId } = req.params;
  const { feedId, comment, homeFeedId } = req.body;
  const payload = {
    feedId,
    ...comment,
    homeFeedId
  };

  res.io.sockets.in(userId).emit("COMMENT_ON_FEED", payload);
  res.end("Emitted New Comment");
};

const removeCommentFromFeed = (req, res) => {
  const { userId } = req.params;
  res.io.sockets.in(userId).emit("DELETE_COMMENT", { ...req.body });
  res.end("Emitted Deleted Comment");
};

const updateCommentOnFeed = (req, res) => {
  const { userId } = req.params;
  res.io.sockets.in(userId).emit("EDIT_COMMENT", { ...req.body });
  res.end("Emitted Edit Comment");
};

const notifyAboutNotification = (req, res) => {
  const { userId } = req.params;
  const { content, receiver } = req.body.populatedNotification;
  res.io.sockets.in(userId).emit("BUMP_NOTIFICATION_COUNT", {});
  res.end("Emitted Notification");
};

/**
 * @deprecated
 * In favor of UsersController#generateLinkAndSendResetPasswordEmail
 */
const sendResetPasswordLink = async (req, res) => {
  const { email } = req.body;
  const logFunc = (msg) => {
    logger.info(`[${email}] ${msg}`);
  };
  logFunc("Password reset request initiated");
  try {
    const user = await User.findOne({ email: email }).select("_id resetPasswordToken email");
    if (!user) {
      logFunc("User not found with this email");
      return res.status(400).json({
        flashType: "danger",
        message: "User not found with this email"
      });
    }
    const token = crypto.randomBytes(32).toString("hex");
    user.resetPasswordToken = token;
    const dt = new Date();
    // todo: investigate
    // @ts-ignore
    user.resetPasswordExpires = dt.setHours(dt.getHours() + 1); // expires after 1 hour
    await user.save({ validateBeforeSave: false });
    await sendResetPasswordEmail(user, token);
    logFunc("Reset password instructions have been successfully sent");
    return res.json({
      emailSent: true,
      message: "Reset password instructions have been successfully sent to your email address.",
      flashType: "success"
    });
  } catch (error) {
    logFunc("Error saving user password reset tokens to user");
    return res.status(500).json({ error: error.message });
  }
};

const resendVerificationEmail = async (req, res) => {
  try {
    const { email } = req.body;
    sendEmailVerificationEmail(email).catch((error) => logger.error(error, "Error resolving promise"));

    res.json({
      emailSent: true,
      flashType: "success",
      message: "Success! We have sent you an email containing a link to verify your email address."
    });
  } catch (error) {
    logger.info("error in resendVerificationEmail(): ", error);
    res.status(500).json({ error });
  }
};

const verifyEmail = (req, res) => {
  const { token } = req.params;
  User.findOne({
    emailVerificationToken: token,
    // todo: investigate
    // @ts-ignore
    emailVerificationExpires: { $gt: new Date().toISOString() }
  })
    .then((user) => {
      if (!user) {
        return res.status(404).json({ error: "Verification token is invalid or expired." });
      }
      user.emailVerificationToken = undefined;
      user.emailVerificationExpires = undefined;
      user.emailVerificationConfirmedAt = new Date();
      user
        .save({ validateBeforeSave: false })
        .then((updatedUser) => {
          res.send({
            emailVerified: true,
            flashType: "success",
            message: "Success! Your email has been verified."
          });
        })
        .catch((error) => res.status(500).json({ error }));
    })
    .catch((error) => res.status(500).json({ error }));
};

const getUserById = async (req, res) => {
  const { id } = req.params;
  try {
    const response = await validateUserId(id);
    if (response.error) {
      return res.status(response.statusCode).json({ message: response.message });
    }
    const user = await getFullUserProfileById(id, req.user && req.user.id == id);
    if (!user) {
      return res.status(422).json({ message: "Invalid userId" });
    }
    return res.json({ user });
  } catch (error) {
    logger.error(error);
    return res.status(500).json(error);
  }
};

const getUserByUsername = async (req, res) => {
  const user = await getFullUserProfileByUsername(req.params.username);
  if (!user) {
    return res.status(422).json({ message: "Invalid username" });
  }
  return res.json({ user });
};

const camelizeKeys = (obj) => {
  if (Array.isArray(obj)) {
    return obj.map((v) => camelizeKeys(v));
  } else if (obj !== null && obj.constructor === Object) {
    return Object.keys(obj).reduce(
      (result, key) => ({
        ...result,
        [camelCase(key)]: camelizeKeys(obj[key])
      }),
      {}
    );
  }
  return obj;
};

const validateUserId = async (id) => {
  if (!GeneralHelper.isValidObjectId(id)) {
    logger.error(`${id} UserId malformed`);
    return { error: true, statusCode: 400, message: "UserId malformed" };
  }
  return { error: false };
};

module.exports.addCommentToFeed = addCommentToFeed;
module.exports.camelizeKeys = camelizeKeys;
module.exports.getUserById = getUserById;
module.exports.getUserByUsername = getUserByUsername;
module.exports.notifyAboutNotification = notifyAboutNotification;
module.exports.removeCommentFromFeed = removeCommentFromFeed;
module.exports.resendVerificationEmail = resendVerificationEmail;
module.exports.sendResetPasswordLink = sendResetPasswordLink;
module.exports.timelineUpdateFlag = timelineUpdateFlag;
module.exports.updateCommentOnFeed = updateCommentOnFeed;
module.exports.validateUserId = validateUserId;
module.exports.verifyEmail = verifyEmail;
