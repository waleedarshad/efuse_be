import { Request, Response, NextFunction } from "express";

import { BaseController } from "./base.controller";
import { RecruitmentProfileService } from "../../lib/recruitment/recruitment-profile.service";

const NAME = "recruitment-profiles.controller";

export class RecruitmentProfilesController extends BaseController {
  private recruitmentProfileService: RecruitmentProfileService;

  constructor() {
    super(NAME);

    this.recruitmentProfileService = new RecruitmentProfileService();
  }

  public manageProfile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, user } = req as { body: any; params: any; user: any };
      const recruitmentProfile = await this.recruitmentProfileService.manageProfile({
        user: user.id,
        ...body
      });
      res.json({ recruitmentProfile });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getProfile = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { body, params, user } = req as { body: any; params: any; user: any };
      const recruitmentProfile = await this.recruitmentProfileService.getRecruitmentProfile(user.id);
      res.json({ recruitmentProfile: recruitmentProfile || {} });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public getProfiles = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query, user } = req as { params: any; query: any; user: any };
      const { page, limit, sort, filters } = query;

      const recruitmentProfiles = await this.recruitmentProfileService.getRecruitmentProfiles(
        user._id,
        params.organizationId,
        sort as string,
        filters as string,
        Number(page),
        Number(limit)
      );
      res.json({ recruitmentProfiles });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  public starredProfiles = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const { params, query, user } = req as { params: any; query: any; user: any };
      const { page, limit, sort, filters } = query;
      const recruitmentProfiles = await this.recruitmentProfileService.getStarredRecruitmentProfiles(
        user._id,
        params.organizationId,
        sort as string,
        filters as string,
        Number(page),
        Number(limit)
      );
      res.json({ recruitmentProfiles });
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
