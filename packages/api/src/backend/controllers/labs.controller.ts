import { Failure } from "@efuse/contracts";
import { NextFunction, Request, Response } from "express";
import { Types } from "mongoose";

import { BaseController } from "./base.controller";
import { ILabsFeature } from "../interfaces";
import { LabsService } from "../../lib/labs";
import { FeatureFlagService } from "../../lib/feature-flags/feature-flag.service";

const NAME = "labs.controller";

export class LabsController extends BaseController {
  private service: LabsService;

  constructor() {
    super(NAME);

    this.service = new LabsService();
  }

  /**
   * Attempts to delete the resource at `id`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LabsController
   */
  public delete = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { params } = req;
    const { id } = params;

    if (!id) {
      next(Failure.BadRequest("Missing parameter 'id'"));
    }

    try {
      const entities = await this.service.delete(id);

      if (entities) {
        res.status(200).json({
          count: entities.length,
          items: entities
        });
      } else {
        next(Failure.UnprocessableEntity("Failed to delete lab"));
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to retrieve the resource at `id`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LabsController
   */
  public get = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { params, user } = req as { params: any; user: any };
    const { id } = params;

    if (!id) {
      next(Failure.BadRequest("Missing parameter 'id'"));
    }

    if (!this.isUser(user)) {
      next(Failure.BadRequest("Missing user information!"));
    }

    try {
      const unchecked = await this.service.find({ _id: id, deleted: false });
      const entities = await this.checkFlags(user._id, unchecked);

      res.status(200).json({
        count: entities.length,
        items: entities
      });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to get all available resources.
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LabsController
   */
  public list = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { user } = req as { user: any };

    if (!this.isUser(user)) {
      next(Failure.BadRequest("Missing user information!"));
    }

    try {
      const unchecked = await this.service.find({
        deleted: false,
        visible: true
      });

      const entities = await this.checkFlags(user._id, unchecked);

      res.status(200).json({ count: entities.length, items: entities });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to patch the resouce at `id`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   * @memberof LabsController
   */
  public patch = (req: Request, res: Response, next: NextFunction): void => {
    next(Failure.NotImplemented());
  };

  /**
   * Attempts to create a resource described by the object in `request.body`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LabsController
   */
  public post = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body } = req;

    if (!body) {
      next(Failure.BadRequest("Missing request body"));
    }

    try {
      const entities = await this.service.create(body);

      if (!entities) {
        next(Failure.InternalServerError("Failed to create lab"));
      }

      res.status(200).json({
        count: 1, // entities.length,
        items: entities
      });
    } catch (error) {
      this.handleError(error, next);
    }
  };

  /**
   * Attempts to replace resourse at `id` with object in `request.body`
   *
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   *
   * @memberof LabsController
   */
  public put = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    const { body, params, user } = req as { body: any; params: any; user: any };
    const { id } = params;

    if (!id) {
      next(Failure.BadRequest("Missing parameter 'id'"));
    }

    if (!body) {
      next(Failure.BadRequest("Missing request body"));
    }

    try {
      const originals = await this.service.findById(id);
      if (!originals || originals.length === 0) {
        next(Failure.UnprocessableEntity("Could not find item to update"));
      }

      // Get flag to be toggled from DB object
      const flag = originals[0].featureFlag;

      const { enabled: value } = body;
      const toggled = await FeatureFlagService.toggleFlag(user._id.toString(), flag, value);

      const labsBody = <ILabsFeature>{ enabled: value };
      const entities = await this.service.update(id, labsBody);

      if (toggled && entities) {
        res.status(200).json({
          count: entities.length,
          items: entities
        });
      } else {
        next(Failure.UnprocessableEntity("Failed to update lab"));
      }
    } catch (error) {
      this.handleError(error, next);
    }
  };

  private checkFlags = async (userId: Types.ObjectId, entities: ILabsFeature[]): Promise<ILabsFeature[]> => {
    /**
     * check length for efficiency
     *    - if zero items to check, return empty array
     *    - if single item, call the service and check the flag
     *    - if mulitple items, get all flags and check them inline
     */
    if (!entities || entities.length === 0) {
      return new Promise((resolve) => {
        resolve([]);
      });
    }

    return Promise.all(
      entities.map(async (entity) => {
        const checked = entity;
        const flag = await FeatureFlagService.getFlag(userId.toString(), entity.featureFlag);

        if (flag) {
          checked.enabled = flag?.enabled;
        }

        return checked;
      })
    );
  };

  private isUser(user: any): user is { _id: string } {
    return !!user && typeof user === "object" && "_id" in user;
  }
}
