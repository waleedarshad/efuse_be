import { NextFunction, Request, Response } from "express";
import { Failure } from "@efuse/contracts";

import { BaseController } from "./base.controller";
import { Auth0Service } from "../../lib/auth/auth0.service";

const NAME = "auth0.controller";

export class Auth0Controller extends BaseController {
  private $auth0Service: Auth0Service;

  constructor() {
    super(NAME);

    this.$auth0Service = new Auth0Service();
  }

  /**
   * @summary Refresh auth0 Token
   *
   * @param body.refreshToken
   *
   * @memberof Auth0Controller
   */
  public refreshAuth0Token = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      const body: undefined | { refreshToken?: string } = req.body as undefined | { refreshToken?: string };

      // Make sure token exists
      if (!body || !body.refreshToken) {
        throw Failure.BadRequest("refreshToken is required");
      }

      const response = await this.$auth0Service.refreshAccessToken(body.refreshToken);

      res.json(response);
    } catch (error) {
      this.handleError(error, next);
    }
  };
}
