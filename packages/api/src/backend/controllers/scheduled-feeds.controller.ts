import { NextFunction, Request, Response } from "express";

import { ScheduledFeedService } from "../../lib/creatortools/scheduled-feed.service";
import { BaseController } from "./base.controller";
import { EFuseAnalyticsUtil } from "../../lib/analytics.util";
import { IScheduledFeed } from "../interfaces/scheduled-feed";

const NAME = "scheduled-feeds.controller";
export class ScheduledFeedsController extends BaseController {
  private scheduledFeedService: ScheduledFeedService;

  constructor() {
    super(NAME);

    this.scheduledFeedService = new ScheduledFeedService();
  }

  public create = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
      EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.create.calls");

      const { body, user } = req as { body: any; user: any };

      // Validate feed, create giphy & media object

      const feedAttachments = await this.scheduledFeedService.validateAndCreateAttachments(
        user._id,
        user.username,
        body.content,
        body.twitchClip,
        body.file,
        body.files,
        body.giphy,
        body.youtubeVideo
      );

      const scheduledFeedBody: IScheduledFeed = <IScheduledFeed>{
        user: user._id,
        content: body.content,
        mentions: body.mentions,
        shared: body.shared,
        sharedFeed: body.sharedFeed,
        sharedTimeline: body.sharedTimeline,
        timelineable: body.timelineable,
        timelineableType: body.timelineableType,
        sharedPostUser: body.sharedPostUser,
        sharedTimelineable: body.sharedTimelineable,
        sharedTimelineableType: body.sharedTimelineableType,
        verified: user.verified || false,
        crossPosts: body.crossPosts,
        media: feedAttachments.media?._id,
        mediaObjects: feedAttachments.mediaObjects?.map((m) => m._id),
        scheduledAt: body.scheduledAt,
        platform: body.platform,
        twitchClip: body.twitchClip,
        associatedGame: body.associatedGame,
        youtubeVideo: body.youtubeVideo
      };

      const scheduledFeed = await this.scheduledFeedService.create(scheduledFeedBody);

      EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.create.success");
      res.json(scheduledFeed);
    } catch (error) {
      EFuseAnalyticsUtil.incrementMetric("scheduledFeeds.create.failure");
      this.handleError(error, next);
    }
  };
}
