const { JWT_KEY } = require("@efuse/key-store");
const jwt = require("jsonwebtoken");

const WhitelistHelper = require("../helpers/whitelist_helper");

const generateBearer = (user, res, message, userObj = {}) => {
  const payload = WhitelistHelper.userPayload(user);

  jwt.sign(payload, JWT_KEY, { expiresIn: "2week" }, (error, token) => {
    if (error) throw error;
    res.json({
      message: message,
      token: `Bearer ${token}`,
      user: userObj
    });
  });
};

module.exports.generateBearer = generateBearer;
