const eGencyLib = require("../../lib/eGency");

const { GeneralHelper } = require("../helpers/general.helper");

const createEgencyBrand = async (req, res) => {
  try {
    const { files, body, user } = req;
    let params = {
      name: body.name,
      creator: user.id
    };
    if (files) {
      params = { ...params, logo: GeneralHelper.buildFileObject(files.logo) };
    }
    const eGencyBrand = await eGencyLib.createEgencyBrand(params);
    return res.json({ eGencyBrand });
  } catch (error) {
    return res.status(500).json({ error: error.message });
  }
};

const getEgencyBrandById = async (req, res) => {
  try {
    const eGencyBrand = await eGencyLib.getEgencyBrandById(req.params.egency_brand_id);
    return res.json({ eGencyBrand });
  } catch (error) {
    const status = error.status || 500;
    return res.status(status).json({ error: error.message });
  }
};

const createEgencyCampaign = async (req, res) => {
  try {
    const { body, params, user } = req;
    const eGencyCampaign = await eGencyLib.createEgencyCampaign({
      ...body,
      brand: params.egency_brand_id,
      creator: user.id
    });
    return res.json({ eGencyCampaign });
  } catch (error) {
    const status = error.status || 500;
    return res.status(status).json({ error: error.message });
  }
};

const createEgencyCampaignOffer = async (req, res) => {
  try {
    const { body, params } = req;
    const campaignOffer = await eGencyLib.createEgencyCampaignOffer({
      user: body.user,
      campaign: params.egency_campaign_id
    });
    return res.json({ campaignOffer });
  } catch (error) {
    const status = error.status || 500;
    return res.status(status).json({ error: error.message });
  }
};

const updateCampaignStatus = async (req, res) => {
  try {
    const { params, body } = req;
    const campaignOffer = await eGencyLib.updateCampaignStatus(params.user_id, params.campaign_offer_id, body.status);
    return res.json({ campaignOffer });
  } catch (error) {
    const status = error.status || 500;
    return res.status(status).json({ error: error.message });
  }
};

const getCampaignByBrand = async (req, res) => {
  try {
    const { params } = req;
    const eGencyCampaign = await eGencyLib.getCampaignByBrand(params.egency_brand_id, params.egency_campaign_id);
    res.json({ eGencyCampaign });
  } catch (error) {
    const status = error.status || 500;
    return res.status(status).json({ error: error.message });
  }
};

module.exports.createEgencyBrand = createEgencyBrand;
module.exports.createEgencyCampaign = createEgencyCampaign;
module.exports.createEgencyCampaignOffer = createEgencyCampaignOffer;
module.exports.getCampaignByBrand = getCampaignByBrand;
module.exports.getEgencyBrandById = getEgencyBrandById;
module.exports.updateCampaignStatus = updateCampaignStatus;
