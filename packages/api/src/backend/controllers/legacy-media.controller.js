const { AWS_S3_BUCKET } = require("@efuse/key-store");
const { Logger } = require("@efuse/logger");
const aws = require("aws-sdk");
const logger = Logger.create();

const { GeneralHelper } = require("../helpers/general.helper");
const { Media } = require("../models/media");
const { User } = require("../models/User");
const { AWS } = require("../config/aws-instance");

const removeFromS3 = (req, res) => {
  const s3 = new AWS.S3();
  const params = { Bucket: AWS_S3_BUCKET, Key: req.query.fileKey };
  s3.deleteObject(params, (err, data) => {
    if (err) {
      logger.error("Error removing file from S3", { err, params });
      if (res) {
        return res.status(500).json(err);
      }
    } else {
      logger.info("File removed from S3", params);
      if (res) {
        res.end("Successfully Deleted!");
      }
    }
  });
};

const getAssociatedMedia = async (mediaable, mediaableType, targetObject, key) => {
  const associatedMedia = await Media.findOne({
    mediaable,
    mediaableType
  }).lean();

  if (associatedMedia) {
    targetObject[key] = associatedMedia;
  }
  if (key === "associatedMedia") {
    const mediaObjects = await Media.find({
      mediaable,
      mediaableType
    }).lean();
    targetObject["attachments"] = mediaObjects;
  }
  return targetObject;
};

const create = async (file, mediaableType, mediaable, user_id) => {
  const associatedMedia = {};
  try {
    if (file && file.url) {
      const media = await Media.create({
        user: user_id,
        mediaableType,
        file,
        mediaable
      });
      associatedMedia.associatedMedia = media.toObject();
    }
  } catch (error) {
    logger.error("Error while creating media", {
      error,
      file,
      mediaableType,
      mediaable,
      user_id
    });
  }
  return associatedMedia;
};

const remove = async (mediaable, mediaableType, user_id, req) => {
  const { removeMedia, fileToBeRemoved } = req.body;
  try {
    if (removeMedia) {
      await Media.findOneAndDelete({
        mediaable,
        user: user_id,
        mediaableType: mediaableType
      }).exec();
      const query = {
        fileKey: `uploads/media/${fileToBeRemoved.filename}`
      };
      req.query = query;
      removeFromS3(req);
    }
  } catch (error) {
    logger.error("Error while deleting media", {
      error,
      mediaable,
      mediaableType,
      user_id
    });
  }
};

const validateUserId = async (id) => {
  if (!GeneralHelper.isValidObjectId(id)) {
    logger.error(`${id} UserId malformed`);
    return { error: true, statusCode: 400, message: "UserId malformed" };
  }
  const userExists = await User.exists({ _id: id });
  if (!userExists) {
    logger.error(`${id} User not found`);
    return { error: true, statusCode: 422, message: "User not found" };
  }
  return { error: false };
};

module.exports.create = create;
module.exports.getAssociatedMedia = getAssociatedMedia;
module.exports.remove = remove;
module.exports.removeFromS3 = removeFromS3;
module.exports.validateUserId = validateUserId;
