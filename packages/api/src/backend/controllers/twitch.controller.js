const { TwitchClipModel } = require("../models/twitch-clip.model");

const getTwitchClips = async (req, res) => {
  const { params, query } = req;

  const { user_id } = params;
  const { page, pageSize } = query;
  const logger = req.log.child({ func: "getTwitchClips" });

  try {
    const clips = await TwitchClipModel.paginate(
      { user: user_id },
      {
        page,
        limit: pageSize,
        populate: [],
        lean: true
      }
    );

    return res.json({ clips });
  } catch (error) {
    logger.error({ error, user_id }, "Error fetching twitch clips of user");

    return res.status(500).json({ error });
  }
};

module.exports.getTwitchClips = getTwitchClips;
