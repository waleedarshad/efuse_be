import { Request, Response, NextFunction } from "express";
import s3Uploader from "./s3-uploader";

export class PlayerCardUploader {
  public static upload = (req: Request, res: Response, next: NextFunction) =>
    s3Uploader("uploads/player-card/").fields([{ name: "image", maxCount: 1 }])(req, res, next);
}
