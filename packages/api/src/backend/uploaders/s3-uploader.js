const { AWS_S3_BUCKET } = require("../../config");
const multer = require("multer");
const multerS3 = require("multer-s3");

const { AWS } = require("../config/aws-instance");

const s3 = new AWS.S3();

const limits = {
  files: 3,
  fileSize: 1024 * 1024 * 100
};

module.exports = (dir) =>
  multer({
    storage: multerS3({
      s3: s3,
      bucket: AWS_S3_BUCKET,
      acl: "public-read",
      contentType: multerS3.AUTO_CONTENT_TYPE,
      metadata: (req, file, cb) => {
        cb(null, { fieldName: file.fieldname });
      },
      key: (req, file, cb) => {
        const newFileName = Date.now() + "-" + file.originalname;
        const fullPath = dir + newFileName;
        cb(null, fullPath);
      }
    }),
    limits: limits
  });
