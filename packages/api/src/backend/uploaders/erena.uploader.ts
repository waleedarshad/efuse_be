import { Request, Response, NextFunction } from "express";
import s3Uploader from "./s3-uploader";

export class ErenaUploader {
  public static upload = (req: Request, res: Response, next: NextFunction) =>
    s3Uploader("uploads/erena/").fields([
      { name: "image", maxCount: 1 },
      { name: "brandLogoUrl", maxCount: 1 },
      { name: "backgroundImageUrl", maxCount: 1 }
    ])(req, res, next);
}
