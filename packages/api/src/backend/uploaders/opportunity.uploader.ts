import { Request, Response, NextFunction } from "express";
import s3Uploader from "./s3-uploader";

export class OpportunityUploader {
  public static upload = (req: Request, res: Response, next: NextFunction) => 
    s3Uploader("uploads/opportunities/").fields([{ name: "image", maxCount: 1 }])(req, res, next);
}