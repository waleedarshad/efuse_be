import { Request, Response, NextFunction } from "express";
import s3Uploader from "./s3-uploader";

export class ProfessionalUploader {
  public static upload = (req: Request, res: Response, next: NextFunction) =>
    s3Uploader("uploads/professionals/").fields([{ name: "resume", maxCount: 1 }])(req,res,next);
}
