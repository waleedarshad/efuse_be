import { Request, Response, NextFunction } from "express";
import s3Uploader from "./s3-uploader";

export class ProfileUploader {
  public static upload = (req: Request, res: Response, next: NextFunction) =>
    s3Uploader("uploads/users/").fields([
      { name: "profilePicture", maxCount: 1 },
      { name: "headerImage", maxCount: 1 },
      { name: "resume", maxCount: 1 }
    ])(req,res,next);
}