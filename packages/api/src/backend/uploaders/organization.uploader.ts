import { Request, Response, NextFunction } from "express";
import s3Uploader from "./s3-uploader";

export class OrganizationUploader {
  public static upload = (req: Request, res: Response, next: NextFunction) => 
    s3Uploader("uploads/organizations/").fields([
      { name: "profileImage", maxCount: 1 },
      { name: "headerImage", maxCount: 1 }
    ])(req, res, next);
  }
