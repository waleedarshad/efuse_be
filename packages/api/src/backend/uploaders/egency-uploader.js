const s3Uploader = require("./s3-uploader");

module.exports = s3Uploader("uploads/egencies/").fields([{ name: "logo", maxCount: 1 }]);
