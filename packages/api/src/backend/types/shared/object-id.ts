import { Types } from "mongoose";

export type ObjectId = Types.ObjectId | string;

export const mongoDbObjectIdRegex = /^[a-f\d]{24}$/i;

export function isObjectId(obj: ObjectId | any): obj is ObjectId {
  const isHexId = (typeof obj === "string" && mongoDbObjectIdRegex.test(obj));
  const isObjId = (obj instanceof Types.ObjectId);

  return isHexId || isObjId;
}
