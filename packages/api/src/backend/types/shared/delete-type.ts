export type DeleteResult = { ok?: number | undefined; n?: number | undefined } & { deletedCount?: number | undefined };
