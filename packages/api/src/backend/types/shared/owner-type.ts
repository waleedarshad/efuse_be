import { IOrganization, IUser } from "../../interfaces";

export type Owner = IUser | IOrganization;
