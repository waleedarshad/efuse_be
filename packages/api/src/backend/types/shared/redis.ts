import Redis from "ioredis";

export type ExpireAsync = {
  (key: Redis.KeyType, seconds: number, callback: Redis.Callback<Redis.BooleanResponse>): void;
  (key: Redis.KeyType, seconds: number): Promise<Redis.BooleanResponse>;
};

export type SetAsync = {
  (
    key: Redis.KeyType,
    value: Redis.ValueType,
    expiryMode?: string | any[] | undefined,
    time?: string | number | undefined,
    setMode?: string | number | undefined
  ): Promise<"OK" | null>;
  (key: Redis.KeyType, value: Redis.ValueType, callback: Redis.Callback<"OK">): void;
  (key: Redis.KeyType, value: Redis.ValueType, setMode: string | any[], callback: Redis.Callback<"OK" | null>): void;
  (
    key: Redis.KeyType,
    value: Redis.ValueType,
    expiryMode: string,
    time: string | number,
    callback: Redis.Callback<"OK">
  ): void;
  (
    key: Redis.KeyType,
    value: Redis.ValueType,
    expiryMode: string,
    time: string | number,
    setMode: string | number,
    callback: Redis.Callback<"OK" | null>
  ): void;
};

export type GetAsync = {
  (key: Redis.KeyType, callback: Redis.Callback<string | null>): void;
  (key: Redis.KeyType): Promise<string | null>;
};
