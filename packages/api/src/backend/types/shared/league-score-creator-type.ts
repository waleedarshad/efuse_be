import { ITeam } from "../../interfaces";
import { Owner } from "./owner-type";

export type LeagueScoreCreator = Owner | ITeam;
