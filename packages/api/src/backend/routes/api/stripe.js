const express = require("express");

const router = express.Router();
const StripeController = require("../../controllers/stripe.controller");

// @route   POST /api/stripe/oauth
// @desc   Complete the stripe connect account connection and get the account ID
router.post("/oauth", StripeController.completeOauth);

// @route   GET /api/stripe/payout_account
// @desc   Retrieve payout account with express dashboard link, if one exists
router.get("/payout_account", StripeController.getStripePayoutAccount);

module.exports = router;
