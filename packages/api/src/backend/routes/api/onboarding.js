const express = require("express");

const router = express.Router();

const OnboardingController = require("../../controllers/onboarding.controller");
const { ProfileUploader } = require("../../uploaders/profile.uploader");
const { ProfessionalUploader } = require("../../uploaders/professional.uploader");

// @route   PATCH /api/onboarding/profile/:email
// @desc    Update Onboarding views
router.patch("/profile/:email", OnboardingController.create);

// @route   PATCH /api/onboarding/attachments/:email
// @desc    Update Onboarding Profile
router.patch("/attachments/:email", ProfileUploader.upload, OnboardingController.saveAttachments);

// @route   PATCH /api/onboarding/professional/:email
// @desc    Update Onboarding  Professional view
router.patch("/professional/:email", ProfessionalUploader.upload, OnboardingController.professional);

module.exports = router;
