const express = require("express");

const router = express.Router();

const PromoCodeController = require("../../controllers/promo-code.controller");

// @route   POST /api/promocode
// @desc    Create new promo code
router.post("/", PromoCodeController.createPromoCode);

// @route   GET /api/promocode/validate/:promocode
// @desc    Validate promocode
router.get("/validate/:promocode", PromoCodeController.validatePromoCode);

module.exports = router;
