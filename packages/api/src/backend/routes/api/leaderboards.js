const express = require("express");

const router = express.Router();

const LeaderboardsController = require("../../controllers/legacy-leaderboards.controller");

const buildPagerVars = require("../../middlewares/build-pager-vars");

// @route   GET /api/leaderboards
// @desc    Get a paginated collection of leaderboards
// @note    This is route is deprecated & will be removed
router.get("", buildPagerVars, LeaderboardsController.deprecatedGetAll);

// @route   GET /api/leaderboards/top
// @desc    Get top 3 leaderboards
router.get("/top", LeaderboardsController.getTopLeaderboards);

// @route   GET /api/leaderboards/:boardType
// @desc    Get leaderboards of a particular type
router.get("/:boardType", buildPagerVars, LeaderboardsController.getAll);

module.exports = router;
