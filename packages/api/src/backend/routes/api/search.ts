import * as express from "express";
import { SearchController } from "../../controllers/search.controller";

const controller = new SearchController();
const router = express.Router();

/**
 * @route   /api/search
 * @desc    Flexible search endpoint
 */
router.get("/", controller.search);

/**
 * @route   /api/search/mentions
 * @desc    Search either user or organization from MongoDB Atlas Search
 * @deprecated - Not removing at the moment to support older mobile app versions
 */
router.get("/mentions", controller.globalMentionSearch);

/**
 * @swagger
 * /api/search/opportunities:
 *  get:
 *    tags:
 *      - Search
 *    summary: Search Opportunities
 *    description: Search through all existing opportunities and return paginated collection.
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/limit'
 *      - $ref: '#/components/parameters/searchTerm'
 *      - $ref: '#/components/parameters/searchLocation'
 *      - $ref: '#/components/parameters/searchOpportunityType'
 *      - $ref: '#/components/parameters/searchSubType'
 *      - $ref: '#/components/parameters/searchPublishStatus'
 *      - $ref: '#/components/parameters/searchUser'
 *      - $ref: '#/components/parameters/searchOrganization'
 *      - $ref: '#/components/parameters/searchIncludeOrganizations'
 *      - $ref: '#/components/parameters/searchSort'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedOpportunity'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 */
router.get("/opportunities", controller.searchOpportunities);

/**
 * @swagger
 * /api/search/applied_opportunities:
 *  get:
 *    tags:
 *      - Search
 *    summary: Search Applied Opportunities
 *    description: Search through all opportunities where user has applied and return paginated collection.
 *    parameters:
 *      - $ref: '#/components/parameters/page'
 *      - $ref: '#/components/parameters/limit'
 *      - $ref: '#/components/parameters/searchTerm'
 *      - $ref: '#/components/parameters/searchLocation'
 *      - $ref: '#/components/parameters/searchOpportunityType'
 *      - $ref: '#/components/parameters/searchSubType'
 *      - $ref: '#/components/parameters/searchPublishStatus'
 *      - $ref: '#/components/parameters/searchUser'
 *      - $ref: '#/components/parameters/searchOrganization'
 *      - $ref: '#/components/parameters/searchIncludeOrganizations'
 *      - $ref: '#/components/parameters/searchSort'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedOpportunity'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 */
router.get("/applied_opportunities", controller.searchAppliedOpportunities, controller.searchOpportunities);

module.exports = router;
