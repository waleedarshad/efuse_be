const express = require("express");

const router = express.Router();

const NotificationsController = require("../../controllers/notifications.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");

// @route   GET /api/notifications
// @desc    Get all notifications

router.get("", buildPagerVars, NotificationsController.index);

// @route   GET /api/notifications/count
// @desc    Get unread notifications count

router.get("/count", NotificationsController.getUnreadCount);

// @route   GET /api/notifications/mark_all_as_read
// @desc    Mark all unread notifications as read

router.get("/mark_all_as_read", NotificationsController.markAllAsRead);

// @route   PUT /api/notifications/mark_as_read/:notificationId
// @desc    Mark indivial unread notifications as read
router.put("/mark_as_read/:notificationId", NotificationsController.markAsRead);

// @route   GET /api/notifications/clear_count
// @desc    Clear read count from notifications bar
router.get("/clear_count", NotificationsController.clearUnreadCount);

module.exports = router;
