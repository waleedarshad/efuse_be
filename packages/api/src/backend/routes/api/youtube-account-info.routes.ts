import { ILogger, Logger } from "@efuse/logger";
import express from "express";

import { YoutubeAccountInfoController } from "../../controllers/youtube-account-info.controller";

export class YoutubeAccountInfoRoutes {
  private $controller: YoutubeAccountInfoController;
  private $logger: ILogger = Logger.create({ name: ".routes.youtube-account-info" });

  public router: express.Router;

  constructor() {
    this.$controller = new YoutubeAccountInfoController();

    this.router = express.Router();

    /**
     * @swagger
     * /api/youtube-account-info/owner/{id}:
     *  get:
     *    tags:
     *      - External Account Info
     *    summary: Get Youtube Account Info by user/org id
     *    description: Get Youtube Account Info by owner (user/org ID)
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/YoutubeAccountInfoResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`/owner/:id`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .getByOwner(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting account info"));
    });
  }
}
