const express = require("express");

const router = express.Router();

const SteamController = require("../../controllers/steam.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");

// @route   GET /api/steam/friends/:userId
// @desc    Get Steam Friends
router.get("/friends/:userId", buildPagerVars, SteamController.getFriends);

// @route   GET /api/steam/owned_games/:userId
// @desc    Get games owned on steam
router.get("/owned_games/:userId", buildPagerVars, SteamController.getOwnedGames);

module.exports = router;
