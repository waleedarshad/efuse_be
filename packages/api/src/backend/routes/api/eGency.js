const express = require("express");

const router = express.Router();

const EgenciesController = require("../../controllers/egencies.controller");
const eGencyUploader = require("../../uploaders/egency-uploader");

// @route   /api/egencies/
// @desc    Create a new eGency Brand
router.post("", eGencyUploader, EgenciesController.createEgencyBrand);

// @route   /api/egencies/:egency_brand_id
// @desc    Get an eGency Brand by Id
router.get("/:egency_brand_id", EgenciesController.getEgencyBrandById);

// @route   /api/egencies/:egency_brand_id/egency_campaigns
// @desc    Create an eGency Campaign
router.post("/:egency_brand_id/egency_campaigns", EgenciesController.createEgencyCampaign);

// @route   /api/egencies/egency_campaigns/:egency_campaign_id/campaign_offer
// @desc    Create an eGency Campaign Offer
router.post("/egency_campaigns/:egency_campaign_id/campaign_offer", EgenciesController.createEgencyCampaignOffer);

// @route   /api/egencies/:user_id/campaign_offers/:campaign_offer_id
// @desc    Update status of a user's campaign offer
router.patch("/:user_id/campaign_offers/:campaign_offer_id", EgenciesController.updateCampaignStatus);

// @route   /api/egencies/:egency_brand_id/egency_campaigns/:egency_campaign_id
// @desc    Get an eGency Campaign by Id
router.get("/:egency_brand_id/egency_campaigns/:egency_campaign_id", EgenciesController.getCampaignByBrand);

module.exports = router;
