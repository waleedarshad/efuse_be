const express = require("express");

const router = express.Router();
const { WorkExperienceUploader } = require("../../uploaders/work-experience.uploader");
const { EducationExperienceUploader } = require("../../uploaders/education-experience.uploader");

const { PortfolioController } = require("../../controllers/portfolio.controller");

const portfolioController = new PortfolioController();

// @route POST /api/portfolio/portfolio_event
router.post("/portfolio_event", portfolioController.managePortfolioEvent);

// @route   DELETE /api/portfolio/portfolio_event/:id
// @desc    Remove a portfolio event from user
router.delete("/portfolio_event/:id", portfolioController.removePortfolioEvent);

// @route   POST /api/portfolio/portfolio_honor
// @desc    Add honor of current user
router.post("/portfolio_honor", portfolioController.managePortfolioHonor);

// @route   DELETE /api/portfolio/portfolio_honor/:id
// @desc    Remove an honor from user
router.delete("/portfolio_honor/:id", portfolioController.removePortfolioHonor);

// @route   POST /api/portfolio/business_experience
// @desc    Add business experience of current user
router.post("/business_experience", WorkExperienceUploader.upload, portfolioController.manageBusinessExperience);

// @route   DELETE /api/portfolio/business_experience/:id
// @desc    Remove a business experience
router.delete("/business_experience/:id", portfolioController.removeBusinessExperience);

// @route   POST /api/portfolio/education_experience
// @desc    Add education experience of current user
router.post("/education_experience", EducationExperienceUploader.upload, portfolioController.manageEducationExperience);

// @route   DELETE /api/portfolio/education_experience/:id
// @desc    Remove an education experience
router.delete("/education_experience/:id", portfolioController.removeEducationExperience);

// @route PUT /api/portfolio/update_video_info/
// @desc    update youtube video info
router.put("/update_video_info", portfolioController.updateVideoInfo);

// @route POST /api/portfolio/business_skill
// @desc Add a business skill to a user model
router.post("/business_skill", portfolioController.managePortfolioBusinessSkills);

// @route DELETE /api/portfolio/business_skill/:id
// @desc Remove a business skill from user model
router.delete("/business_skill/:id", portfolioController.removePortfolioBusinessSkills);

// @route POST /api/portfolio/gaming_skill
// @desc Add a gaming skill to a user model
router.post("/gaming_skill", portfolioController.managePortfolioGamingSkills);

// @route DELETE /api/portfolio/gaming_skill/:id
// @desc Remove a gaming skill from user model
router.delete("/gaming_skill/:id", portfolioController.removePortfolioGamingSkills);

// @route PUT /api/portfolio/youtube
// desc add youtube info
router.put("/youtube", portfolioController.addYoutubeInfo);
// @route DELETE /api/portfolio/youtube
// desc remove youtube info
router.delete("/youtube", portfolioController.removeYoutubeInfo);
// @route PUT /api/portfolio/tiktok
// desc add tiktok info
router.put("/tiktok", portfolioController.addTiktokInfo);
// @route DELETE /api/portfolio/tiktok
// desc remove youtubtiktok info
router.delete("/tiktok", portfolioController.removeTiktokInfo);
// @route PUT /api/portfolio/linkedin
// add linkedin username
router.put("/linkedin", portfolioController.addLinkedinInfo);
// @route DELETE /api/portfolio/linkedin
// remove linkedin username
router.delete("/linkedin", portfolioController.removeLinkedinInfo);
// @route @route PUT /api/portfolio/snapchat
// add snapchat info
router.put("/snapchat", portfolioController.addSnapchatInfo);
// @route @route DELETE /api/portfolio/snapchat
// remove snapchat info
router.delete("/snapchat", portfolioController.removeSnapchatInfo);

// @route   PUT /api/portfolio/instagram
// @desc    Add instagram username
router.put("/instagram", portfolioController.addInstagramUsername);

// @route   DELETE /api/portfolio/instagram
// @desc    Remove instagram username
router.delete("/instagram", portfolioController.removeInstagramUsername);

module.exports = router;
