import { ILogger, Logger } from "@efuse/logger";
import express from "express";

import { TwitterController } from "../../controllers/twitter.controller";
import { GoogleAuthController } from "../../controllers/external-account-auth/google-auth.controller";
import { BnetAuthController } from "../../controllers/external-account-auth/bnet-auth.controller";
import { SnapchatAuthController } from "../../controllers/external-account-auth/snapchat-auth.controller";
import { LinkedinAuthController } from "../../controllers/external-account-auth/linkedin-auth.controller";
import { OAuthCredentialController } from "../../controllers/oauth-credential.controller";
import { TwitchAuthController } from "../../controllers/external-account-auth/twitch-auth.controller";
import { StateSpaceAuthController } from "../../controllers/external-account-auth/state-space-auth.controller";
import { RiotAuthController } from "../../controllers/external-account-auth/riot-auth.controller";

export class ExternalAccountAuthRoutes {
  private $bnetAuthController: BnetAuthController;
  private $googleAuthController: GoogleAuthController;
  private $linkedinAuthController: LinkedinAuthController;
  private $logger: ILogger;
  private $oauthCredentialController: OAuthCredentialController;
  private $snapchatAuthController: SnapchatAuthController;
  private $twitterController: TwitterController;
  private $twitchAuthController: TwitchAuthController;
  private $stateSpaceAuthController: StateSpaceAuthController;
  private $riotAuthController: RiotAuthController;

  public router: express.Router;

  constructor() {
    this.$logger = Logger.create({ name: "external-account-auth.routes" });

    this.$bnetAuthController = new BnetAuthController();
    this.$googleAuthController = new GoogleAuthController();
    this.$linkedinAuthController = new LinkedinAuthController();
    this.$oauthCredentialController = new OAuthCredentialController();
    this.$snapchatAuthController = new SnapchatAuthController();
    this.$twitterController = new TwitterController();
    this.$twitchAuthController = new TwitchAuthController();
    this.$stateSpaceAuthController = new StateSpaceAuthController();
    this.$riotAuthController = new RiotAuthController();

    this.router = express.Router();

    /**
     * @swagger
     * /api/external_auth/status/{owner}/service/{service}:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Determine if owner has authed with service
     *    description: Retrieve boolean indicating if owner (user/org) has authenticated with external service
     *    parameters:
     *    - $ref: '#/components/parameters/externalAuthOwnerParameter'
     *    - $ref: '#/components/parameters/externalAuthServiceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/OAuthStatusResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get(
      "/status/:owner/service/:service",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$oauthCredentialController
          .getOwnerConnectionStatus(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
      }
    );

    /**
     * @swagger
     * /api/external_auth/bnet:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate Battle.net consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/bnet", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$bnetAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/google:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate google consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/google", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$googleAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/linkedin:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate linkedin consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/linkedin", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$linkedinAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/snapchat:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate snapchat consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/snapchat", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$snapchatAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/twitch:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate twitch consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/twitch", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$twitchAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/twitter:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate twitter consent URL for the logged in user
     *    parameters:
     *      - $ref: '#/components/parameters/orgId'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/twitter", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$twitterController
        .loginSecured(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/link/{owner}/service/{service}:
     *  delete:
     *    tags:
     *      - External Account Auth
     *    summary: Unlink owner's external account link
     *    description: Retrieve boolean indicating if owner was successfully unlinked from service
     *    parameters:
     *    - $ref: '#/components/parameters/externalAuthOwnerParameter'
     *    - $ref: '#/components/parameters/externalAuthServiceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/OAuthStatusResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.delete(
      "/link/:owner/service/:service",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$oauthCredentialController
          .unlinkExternalAccount(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while unlinking account"));
      }
    );

    /**
     * @swagger
     * /api/external_auth/statespace:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate State Space consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/statespace", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$stateSpaceAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });

    /**
     * @swagger
     * /api/external_auth/riot:
     *  get:
     *    tags:
     *      - External Account Auth
     *    summary: Get consent URL
     *    description: Generate Riot consent URL for the logged in user
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ConsentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/riot", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$riotAuthController
        .generateConsentURL(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred while linking account"));
    });
  }
}
