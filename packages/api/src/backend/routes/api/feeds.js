const express = require("express");

const router = express.Router();

const CommentsController = require("../../controllers/comments.controller");
const FeedsControllerJS = require("../../controllers/legacy-feeds.controller");
const LoungeController = require("../../controllers/lounge.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");
const { GlobalKillSwitch } = require("../../middlewares/global-kill-switch.middleware");

const { FeedsController } = require("../../controllers/feeds.controller");
const { AutomatedFeedsController } = require("../../controllers/automated-feeds.controller");
const { ScheduledFeedsController } = require("../../controllers/scheduled-feeds.controller");

const { LoungeFeedController } = require("../../controllers/lounge-feed.controller");

const feedsController = new FeedsController();
const automatedFeedsController = new AutomatedFeedsController();
const scheduledFeedsController = new ScheduledFeedsController();
const loungeFeedController = new LoungeFeedController();

/**
 * @swagger
 * /api/feeds/following:
 *  get:
 *    tags:
 *      - Feed
 *    summary: Get current user following feed
 *    description: Get current user following feed
 *    parameters:
 *    - $ref: '#/components/parameters/page'
 *    - $ref: '#/components/parameters/limit'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedFeed'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.get("/following", buildPagerVars, feedsController.getFollowingFeed);

// @route   POST /api/feeds
// @desc    Create a new Post
// @deprecated   We will isolate these into separate endpoints
router.post("", GlobalKillSwitch.createFeedKillSwitch, feedsController.legacyCreate);

// @route   POST /api/feeds/create
// @desc    Create a new Post
router.post("/create", GlobalKillSwitch.createFeedKillSwitch, feedsController.create);

// @route POST /api/feeds/automated/create
// @desc create an automated stream post
router.post("/automated/create", automatedFeedsController.create);

// @route POST /api/feeds/scheduled/create
// @desc create scheduled post
router.post("/scheduled/create", scheduledFeedsController.create);

// @route   GET /api/feeds/featured
// @desc    Fetch featured feed based on ranking algorithm
router.get("/featured", buildPagerVars, LoungeController.getFeaturedFeed);

// @route GET /api/feeds/automated
// @desc will return a list of automated stream posts
router.get("/automated", FeedsControllerJS.getAutomatedPosts);

// @router GET /api/feeds/automated/:id
// @desc retrieve an automated stream post
router.get("/automated/:id", FeedsControllerJS.retrieveAutomatedPost);

// @router PUT /api/feeds/automated/:id
// @desc update an automated stream post
// @optional_params
//    content: string - content of the post
//    crossPosts: array - standard crossPosts array of optional crossPosts
//    media: object - Media object received with automated post
//    file: object - standard uploaded file object
// @note  this is a destructive update, so anything passed in will REPLACE the existing object.
//        only media OR file should be sent. media to keep the original media object, file to replace with new media object
router.put("/automated/:id", FeedsControllerJS.updateAutomatedPost);

// @router DELETE /api/feeds/automated/:id
// @desc delete an automated stream post
router.delete("/automated/:id", FeedsControllerJS.deleteAutomatedPost);

// FIXME: The scheduled posts endpoints are listed above the /:timelineable endpoint because express router uses greedy matching
// So the /scheduled endpoint always matched /:timelineable
// This should be fixed with a better solution

// @route GET /api/feeds/scheduled
// @desc will return a list of scheduled posts
router.get("/scheduled", FeedsControllerJS.getScheduledPosts);

// @router DELETE /api/feeds/scheduled/:id
// @desc delete a scheduled post
router.delete("/scheduled/:id", FeedsControllerJS.deleteScheduledPost);

/**
 * @swagger
 * /api/feeds/verified:
 *  get:
 *    tags:
 *      - Feed
 *    summary: Get Verified feed
 *    description: Get verified lounge feed
 *    parameters:
 *    - $ref: '#/components/parameters/page'
 *    - $ref: '#/components/parameters/limit'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedFeed'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.get("/verified", GlobalKillSwitch.loungeFeedKillSwitch, buildPagerVars, loungeFeedController.getVerifiedFeed);

/**
 * @swagger
 * /api/feeds/lounge/featured:
 *  get:
 *    tags:
 *      - Feed
 *    summary: Get featured feed
 *    description: Get featured lounge feed
 *    parameters:
 *    - $ref: '#/components/parameters/page'
 *    - $ref: '#/components/parameters/limit'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedFeed'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.get(
  "/lounge/featured",
  GlobalKillSwitch.loungeFeedKillSwitch,
  buildPagerVars,
  loungeFeedController.getRankedFeaturedLoungeFeed
);

// @route   GET /api/feeds/:timelineable
// @desc    Get home feed

router.get("/:timelineable", buildPagerVars, FeedsControllerJS.index);

// @route   GET /api/feeds/show/:id
// @desc    Get post by id

router.get("/show/:id", FeedsControllerJS.show);

// @route   POST /api/feeds/:feedId/comment
// @desc    Create a comment against feed

router.post("/:feedId/comment", CommentsController.feedComment);

// @route   GET /api/feeds/:feedId/comments
// @desc    Get comments against a feed

router.get("/:feedId/comments", buildPagerVars, CommentsController.getFeedComments);

// @route   GET /api/feeds/:feedId/comments/thread
// @desc    Get comments with thread against a feed

router.get("/:feedId/comments/thread", buildPagerVars, CommentsController.parentComments);

// @route   GET /api/feeds/:feedId/comments/thread/:parent
// @desc    Get thread of a particular comment against a feed

router.get("/:feedId/comments/thread/:parent", buildPagerVars, CommentsController.commentThread);

// @route   DELETE /api/feeds/:feedId/delete
// @desc    Delete a Feed

router.delete("/:feedId/delete", FeedsControllerJS.deleteFeed);

// @route   GET /api/feeds/:feedId/edit
// @desc    Get a post to edit

router.get("/:feedId/edit", FeedsControllerJS.getFeedToEdit);

// @route   PUT /api/feeds/:feedId/update
// @desc    Update feed

router.put("/:feedId/update", FeedsControllerJS.updateFeed);

// @route   DELETE /api/feeds/:homeFeedId/remove_from_timeline
// @desc    Remove a feed from user's timeline

router.delete("/:homeFeedId/remove_from_timeline", FeedsControllerJS.removeFromTimeline);

// @route PUT /api/feeds/report
// @desc reporting a feed {homeFeedId, description}
router.put("/report", FeedsControllerJS.reportFeed);

// @route GET /api/feeds/discover/latest
// @desc get latest feeds
router.get("/discover/latest", GlobalKillSwitch.loungeFeedKillSwitch, buildPagerVars, LoungeController.discoverFeeds);

// @route PUT /api/feeds/report
// @desc reporting a feed {homeFeedId, description}
router.put("/reportLoungePost", LoungeController.reportLoungePost);

// @route PUT /api/feeds/lounge/hide/:loungeFeedId
// desc hide lounge feed by admin for the users
router.put("/lounge/hide/:loungeFeedId", LoungeController.hideLoungeFeed);

// @route GET /api/feeds/lounge/show/:id
// @desc it will get a single lounge feed by id
router.get("/lounge/show/:id", LoungeController.getLoungefeedById);

// @route PATCH /api/feeds/:feedId/boost
// @desc  Boost a particular feed or unboost it
router.patch("/:feedId/boost", FeedsControllerJS.boostFeed);

/**
 * @swagger
 * /api/feeds/user/{id}:
 *  get:
 *    tags:
 *      - Feed
 *    summary: Get user feed
 *    description: Get user’s feed by userId
 *    parameters:
 *    - $ref: '#/components/parameters/id'
 *    - $ref: '#/components/parameters/page'
 *    - $ref: '#/components/parameters/limit'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedFeed'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.get("/user/:userId", buildPagerVars, feedsController.getUserFeed);

/**
 * @swagger
 * /api/feeds/org/{id}:
 *  get:
 *    tags:
 *      - Feed
 *    summary: Get organization feed
 *    description: Get organization feed
 *    parameters:
 *    - $ref: '#/components/parameters/id'
 *    - $ref: '#/components/parameters/page'
 *    - $ref: '#/components/parameters/limit'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/PaginatedFeed'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.get("/org/:orgId", buildPagerVars, feedsController.getOrganizationFeed);

/**
 * @swagger
 * /api/feeds/{id}:
 *  patch:
 *    tags:
 *      - Feed
 *    summary: Update feed object
 *    description: Update existing feed object based on id
 *    parameters:
 *    - $ref: '#/components/parameters/id'
 *    requestBody:
 *      $ref: '#/components/requestBodies/FeedBody'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/FeedResponse'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.patch("/:feedId", feedsController.updateFeed);

/**
 * @swagger
 * /api/feeds/home/{id}:
 *  get:
 *    tags:
 *      - Feed
 *    summary: Get home feed object
 *    description: Get formatted home feed object
 *    parameters:
 *    - $ref: '#/components/parameters/id'
 *    responses:
 *      200:
 *        $ref: '#/components/responses/FeedResponse'
 *      400:
 *        $ref: '#/components/responses/BadRequest'
 *      401:
 *        $ref: '#/components/responses/Unauthorized'
 *      422:
 *        $ref: '#/components/responses/UnprocessableEntity'
 *      500:
 *        $ref: '#/components/responses/InternalServerError'
 */
router.get("/home/:id", feedsController.getHomeFeedPost);

// @route GET /api/feeds/lounge/:id
// @desc retrieve a single lounge feed post
router.get("/lounge/:id", loungeFeedController.getLoungeFeedPost);

module.exports = router;
