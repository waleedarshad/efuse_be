import { Router } from "express";

import { TalentCardsController } from "../../controllers/talent-cards.controller";

class TalentCardRoutes {
  public readonly router: Router;
  private readonly controller: TalentCardsController;

  constructor() {
    this.controller = new TalentCardsController();
    this.router = Router();

    /**
     * @deprecated In favor of graphQL query GetTalentCards
     * @swagger
     * /api/talent_cards/users:
     *   get:
     *     tags:
     *       - Talent Cards
     *     description: Returns collection of random users which are not followed by current user
     *     responses:
     *       200:
     *         description: Collection of random users
     *         content:
     *           application/json:
     *             schema:
     *               $ref: '#/components/schemas/ArrayOfUser'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     */
    this.router.get("/users", this.controller.getRandomUsers);
  }
}

const talentCardRoutes = new TalentCardRoutes();
module.exports = talentCardRoutes.router;
