import { Router } from "express";

import { RecruitmentProfilesController } from "../../controllers/recruitment-profiles.controller";
import { RecruitPeopleController } from "../../controllers/recruit-people.controller";
import buildPagerVars from "../../middlewares/build-pager-vars";
import { RecruitActionSelfController } from "../../controllers/recruit/recruit-action-self.controller";
import { RecruitActionOrganizationController } from "../../controllers/recruit/recruit-action-organization.controller";

class RecruitmentProfileRoutes {
  public readonly router: Router;
  private recruitPeopleController: RecruitPeopleController;
  private recruitmentProfilesController: RecruitmentProfilesController;
  private recruitActionSelfController: RecruitActionSelfController;
  private recruitActionOrganizationController: RecruitActionOrganizationController;

  constructor() {
    this.router = Router();
    this.recruitPeopleController = new RecruitPeopleController();
    this.recruitmentProfilesController = new RecruitmentProfilesController();
    this.recruitActionSelfController = new RecruitActionSelfController();
    this.recruitActionOrganizationController = new RecruitActionOrganizationController();

    // @route   PATCH /api/recruitment_profile
    // @desc    Create or Update recruitment profile
    this.router.patch("", this.recruitmentProfilesController.manageProfile);

    // @route   GET /api/recruitment_profile
    // @desc    Get logged-in user's recruitment profile
    this.router.get("", this.recruitmentProfilesController.getProfile);

    /**
     * @swagger
     * /api/recruitment_profile/all:
     *  get:
     *    tags:
     *      - Recruitment Profile
     *    summary: Get all resources
     *    description: Get a pagination collection of recruitment profiles
     *    parameters:
     *    - $ref: '#/components/parameters/page'
     *    - $ref: '#/components/parameters/limit'
     *    - $ref: '#/components/parameters/filters'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/PaginatedRecruitmentProfile'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      403:
     *        $ref: '#/components/responses/Forbidden'
     */
    this.router.get("/all", buildPagerVars, this.recruitmentProfilesController.getProfiles);

    // @route   GET /api/recruitment_profile/star
    // @desc    Get paginated collection of starred recruitment profiles
    this.router.get("/star", buildPagerVars, this.recruitmentProfilesController.starredProfiles);

    // @route   POST /api/recruitment_profile/people
    // @desc    Create recruit people object
    this.router.post("/people", this.recruitPeopleController.create);

    // @route   PATCH /api/recruitment_profile/people/:recruitPeopleId
    // @desc    Update recruit people object
    this.router.patch("/people/:recruitPeopleId", this.recruitPeopleController.update);

    // @route   GET /api/recruitment_profile/people
    // @desc    Get all recruit prople objects
    this.router.get("/people", this.recruitPeopleController.getAll);

    // @route   DELETE /api/recruitment_profile/people/:recruitPeopleId
    // @desc    Delete recruit people object
    this.router.delete("/people/:recruitPeopleId", this.recruitPeopleController.delete);

    // @route   GET /api/recruitment_profile/organizations/:organizationId/all
    // @desc    Get paginated collection of recruitment profiles
    this.router.get(
      "/organizations/:organizationId/all",
      buildPagerVars,
      this.recruitmentProfilesController.getProfiles
    );

    // @route   GET /api/recruitment_profile/organizations/:organizationId/star
    // @desc    Get paginated collection of starred recruitment profiles
    this.router.get(
      "/organizations/:organizationId/star",
      buildPagerVars,
      this.recruitmentProfilesController.starredProfiles
    );

    // @route POST /api/recruitment_profile/action
    // desc Perform action on particular recruit
    this.router.post("/action", this.recruitActionSelfController.performRecruitAction);

    // @route POST /api/organizations/:organizationId/action
    // @desc Perform recruiting action on behalf of an organization
    this.router.post(
      "/organization/:organizationId/action",
      this.recruitActionOrganizationController.performRecruitAction
    );
  }
}

const recruitmentProfileRoutes = new RecruitmentProfileRoutes();
module.exports = recruitmentProfileRoutes.router;
