import { Router } from "express";
import { TranscodeMediaHandlerController } from "../../controllers/transcode-media-handler.controller";

class TranscodeMediaRoutes {
  public router: Router;

  private transcodeMediaHandlerController: TranscodeMediaHandlerController;

  constructor() {
    this.router = Router();

    this.transcodeMediaHandlerController = new TranscodeMediaHandlerController();

    // @route   POST  /api/__transcoding__/video
    // @desc    Handle SNS Topic for video transcoding
    this.router.post("/video", this.transcodeMediaHandlerController.transcodeMediaWebhook);
  }
}

const transcodeMediaRoutes = new TranscodeMediaRoutes();
module.exports = transcodeMediaRoutes.router;
