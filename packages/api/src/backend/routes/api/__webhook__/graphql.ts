import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { HasuraGraphqlController } from "../../../controllers/hasura-graphql.controller";

export class HasuraGraphqlWebhookRoutes {
  private $logger: ILogger;
  private $hasuraGraphqlController: HasuraGraphqlController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "hasura-graphql-webhook", level: "debug" });
    this.$hasuraGraphqlController = new HasuraGraphqlController();

    this.router = Router();

    this.router.post("", (req: Request, res: Response, next: NextFunction) => {
      this.$hasuraGraphqlController
        .handler(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while handling webhook hasuraGraphqlContoller")
        );
    });
  }
}
