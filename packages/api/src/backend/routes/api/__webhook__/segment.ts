import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { SegmentController } from "../../../../backend/controllers/segment.controller";

export class Segment {
  private segmentController: SegmentController;
  private $logger: ILogger;

  public router: Router;

  constructor() {
    this.router = Router();
    this.segmentController = new SegmentController();
    this.$logger = Logger.create({ name: "segment", level: "debug" });

    // @route   POST /api/__webhook__/segment
    // @desc    Endpoint to ingress all segment data
    this.router.post("", (req: Request, res: Response, next: NextFunction) => {
      this.segmentController
        .SegmentHandler(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while ingressing all segment data")
        );
    });
  }
}
