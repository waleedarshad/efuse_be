import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { GetStreamWebhookController } from "../../../controllers/get-stream-webhook.controller";

export class GetStreamWebhookRoutes {
  private $logger: ILogger;
  private $getStreamWebhookController: GetStreamWebhookController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "get-stream", level: "debug" });
    this.$getStreamWebhookController = new GetStreamWebhookController();

    this.router = Router();

    this.router.post("", (req: Request, res: Response, next: NextFunction) => {
      this.$getStreamWebhookController
        .handler(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while handling webhook getStreamWebhookController")
        );
    });
  }
}
