import { IRouterWithAsync, Router } from "@awaitjs/express";
import { Failure } from "@efuse/contracts";
import { STREAMCHAT_API_KEY, STREAMCHAT_API_SECRET } from "@efuse/key-store";
import { ILogger, Logger } from "@efuse/logger";
import { Request, Response } from "express";
import { IncomingHttpHeaders } from "http";
import { StreamChat } from "stream-chat";

import { FeatureFlagService } from "../../../../lib/feature-flags/feature-flag.service";
import { FollowerService } from "../../../../lib/follower";
import {
  IStreamchatChannel,
  IStreamchatChannelMember,
  IStreamchatMessage,
  IStreamchatPresendMessageWebhookResponse,
  IStreamchatUser
} from "../../../interfaces/streamchat";

const FOLLOWER_CHECK_FEATURE_FLAG = "streamchat_follower_check";
const RECRUITING_FEATURE_FLAG = "user_recruiting";
const REQUIRED_HEADER = "x-signature";

export class GetStreamMessageWebhookRoutes {
  private followerService: FollowerService;
  private logger: ILogger;
  private streamchatInstance: StreamChat;

  public router: IRouterWithAsync;

  constructor() {
    this.followerService = new FollowerService();
    this.logger = Logger.create({ name: "streamchat-before-message-send-webhook" });
    this.streamchatInstance = new StreamChat(STREAMCHAT_API_KEY, STREAMCHAT_API_SECRET);

    this.router = Router();

    this.router.postAsync("", async (req: Request, res: Response) => {
      const response: { message: Partial<IStreamchatMessage> } = { message: {} };

      const { body: incoming }: { body?: IStreamchatPresendMessageWebhookResponse } = <never>req;

      if (!this.validateHeader(req)) {
        response.message = { text: "invalid request - mismatched headers", type: "error" };
      } else if (!incoming || !incoming.message) {
        response.message = { text: "invalid request - missing message body", type: "error" };
      } else {
        // todo: investigate
        // @ts-ignore
        const followerCheckEnabled: boolean = await FeatureFlagService.hasFeature(FOLLOWER_CHECK_FEATURE_FLAG);

        if (followerCheckEnabled) {
          response.message = await this.determineResponse(incoming);
        } else {
          response.message = this.determineLegacyResponse(incoming);
        }
      }

      res.status(200).json(response);
    });
  }

  private determineLegacyResponse(incoming: IStreamchatPresendMessageWebhookResponse): Partial<IStreamchatMessage> {
    const message: Partial<IStreamchatMessage> = this.sanitizeMessage(incoming);

    return message;
  }

  private async determineResponse(
    incoming: IStreamchatPresendMessageWebhookResponse
  ): Promise<Partial<IStreamchatMessage>> {
    const connected = await this.usersAreConnected(incoming);
    const isRecruiting = await this.isRecruitingMessage(incoming);

    const message: Partial<IStreamchatMessage> =
      connected || isRecruiting
        ? this.sanitizeMessage(incoming)
        : { text: "Unable to send message. You must be mutual followers to message each other.", type: "error" };

    return message;
  }

  private getMessageOriginator(incoming: IStreamchatPresendMessageWebhookResponse): string {
    const { user }: { user: IStreamchatUser } = incoming;

    if (!user) {
      throw Failure.BadRequest("No user found on incoming request");
    }

    return user.id;
  }

  private getMessageRecipients(
    originator: string,
    incoming: IStreamchatPresendMessageWebhookResponse
  ): IStreamchatChannelMember[] {
    const { channel }: { channel: IStreamchatChannel } = incoming;

    if (!channel || !channel.members) {
      throw Failure.BadRequest("Missing chat information on incoming request");
    }

    const { members }: { members: IStreamchatChannelMember[] } = channel;

    return members.filter((member: IStreamchatChannelMember) => member.user_id !== originator);
  }

  private async isRecruitingMessage(incoming: IStreamchatPresendMessageWebhookResponse): Promise<boolean> {
    try {
      const originator = this.getMessageOriginator(incoming);
      const recipients = this.getMessageRecipients(originator, incoming);

      const originatorIsRecruiter = await FeatureFlagService.hasFeature(RECRUITING_FEATURE_FLAG, originator);
      if (originatorIsRecruiter) {
        return true;
      }

      const recruiters = recipients.map(async (recipient: IStreamchatChannelMember) => {
        // todo: investigate
        // @ts-ignore
        const enabled: boolean = await FeatureFlagService.hasFeature(RECRUITING_FEATURE_FLAG, recipient.user_id);

        return enabled;
      });

      const results = await Promise.all(recruiters);
      const matches = results.filter((result: boolean) => result === true);

      return matches.length > 0;
    } catch (error: unknown) {
      this.logger.error(error, "An error occurred determining if this message is a recruiting message");

      return new Promise((resolve) => {
        resolve(false);
      });
    }
  }

  private sanitizeMessage(incoming: IStreamchatPresendMessageWebhookResponse): Partial<IStreamchatMessage> {
    return incoming.message;
  }

  private async usersAreConnected(incoming: IStreamchatPresendMessageWebhookResponse): Promise<boolean> {
    try {
      const originator = this.getMessageOriginator(incoming);
      const recipients = this.getMessageRecipients(originator, incoming);

      const connections = recipients.map(async (recipient: IStreamchatChannelMember) => {
        const connected = await this.followerService.connected(originator, recipient.user_id);

        return connected;
      });

      const results = await Promise.all(connections);

      return results.every((result) => result === true);
    } catch (error: unknown) {
      this.logger.error(error, "An error occurred determining if users are connected");

      return new Promise((resolve) => {
        resolve(false);
      });
    }
  }

  private validateHeader(req: Request): boolean {
    const { headers, body } = req as { headers: IncomingHttpHeaders; body: unknown };
    const rawHeader = headers[REQUIRED_HEADER];

    if (!rawHeader) {
      return false;
    }

    const xSignature = Array.isArray(rawHeader) ? rawHeader[0] : rawHeader;

    return this.streamchatInstance.verifyWebhook(JSON.stringify(body), xSignature);
  }
}
