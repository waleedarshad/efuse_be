import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { CloudflareWebhookController } from "../../../controllers/cf-stream-transcode.controller";

export class CloudflareStreamWebhookRoutes {
  private $logger: ILogger;
  private $cloudflareWebhookController: CloudflareWebhookController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "cloudflare-stream-webhook", level: "debug" });
    this.$cloudflareWebhookController = new CloudflareWebhookController();

    this.router = Router();

    this.router.post("", (req: Request, res: Response, next: NextFunction) => {
      this.$cloudflareWebhookController
        .handler(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while handling webhook cloudflareWebhookController")
        );
    });
  }
}
