import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";
import { BrazeWebhookController } from "../../../controllers/braze-webhook.controller";

export class BrazeWebhookRoutes {
  private $logger: ILogger;
  private $brazeWebhookController: BrazeWebhookController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "braze-webhook", level: "debug" });
    this.$brazeWebhookController = new BrazeWebhookController();

    this.router = Router();

    this.router.post("", (req: Request, res: Response, next: NextFunction) => {
      this.$brazeWebhookController
        .handler(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while handling webhook brazeWebhookController")
        );
    });
  }
}
