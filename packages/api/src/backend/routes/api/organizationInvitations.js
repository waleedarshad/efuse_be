const express = require("express");

const router = express.Router();

const organizationIvitationsController = require("../../controllers/organization-invitations.controller");

// @route POST /api/organization_invitations
router.post("", organizationIvitationsController.create);

// @route PUT /api/organization_invitations/accept/:orgId
router.put("/accept/:orgId", organizationIvitationsController.acceptInvitation);

// @route PUT /api/organization_invitations/reject/:orgId
router.put("/reject/:orgId", organizationIvitationsController.rejectInvitation);

module.exports = router;
