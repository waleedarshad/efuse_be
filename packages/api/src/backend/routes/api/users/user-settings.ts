import { Router, Request, Response, NextFunction } from "express";
import { ILogger, Logger } from "@efuse/logger";
import { UserSettingsController } from "../../../controllers/user-settings.controller";

export class UserSettingRoutes {
  private $logger: ILogger;
  private $userSettingsController: UserSettingsController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "user-settings.routes" });
    this.$userSettingsController = new UserSettingsController();

    this.router = Router();

    // @route   GET /api/users/settings/notifications
    // @desc    Return the notification settings for current user
    this.router.get("/notifications", (req: Request, res: Response, next: NextFunction) => {
      this.$userSettingsController
        .getNotificationSettings(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "unable to get notification settings for current user")
        );
    });

    // @route   PATCH /api/users/settings/notifications
    // @desc    Update the notification settings for current user
    this.router.patch("/notifications", (req: Request, res: Response, next: NextFunction) => {
      this.$userSettingsController
        .updateNotificationSettings(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "unable to Update the notification settings for current user")
        );
    });

    // @route   GET /api/users/settings/media
    // @desc    Return the media settings for current user
    this.router.get("/media", (req: Request, res: Response, next: NextFunction) => {
      this.$userSettingsController
        .getMediaSettings(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "unable to Return the media settings for current user")
        );
    });

    // @route   PATCH /api/users/settings/media
    // @desc    Update the media settings for current user
    this.router.patch("/media", (req: Request, res: Response, next: NextFunction) => {
      this.$userSettingsController
        .updateMediaSettings(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "unable to Return the media settings for current user")
        );
    });
  }
}
