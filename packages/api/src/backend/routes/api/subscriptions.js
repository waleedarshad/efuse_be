const express = require("express");

const router = express.Router();
const SubscriptionController = require("../../controllers/subscriptions.controller");

// @route   POST /api/subscription
// @desc    create new recurly user
router.get("", SubscriptionController.getRecurlyAccount);

module.exports = router;
