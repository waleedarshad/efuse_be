import { Router } from "express";
import { HypesController } from "../../controllers/hypes.controller";

export class HypeRoutes {
  public readonly router: Router;
  private $hypeController: HypesController;

  constructor() {
    this.router = Router();
    this.$hypeController = new HypesController();

    // @route  POST /api/hypes/feed/:id
    // @desc   POST hypes for a feed (aka post)
    this.router.post("/feed/:feedId", this.$hypeController.newPostHype);
  }
}
