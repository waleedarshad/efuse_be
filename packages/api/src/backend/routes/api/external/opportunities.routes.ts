import { Router } from "express";
import { DiscordOpportunitiesController } from "../../../controllers/external/discord-opportunities.controller";
import buildPagerVars from "../../../middlewares/build-pager-vars";
import { validateOpportunitiesScope } from "../../../middlewares/validate-extapi-scope";
import { parseOpportunityFilters } from "../../../middlewares/parse-filters";

export class DiscordOpportunityRoutes {
  private $discordOpportunitiesController: DiscordOpportunitiesController;

  public router: Router;

  constructor() {
    this.$discordOpportunitiesController = new DiscordOpportunitiesController();

    this.router = Router();

    this.router.get(
      "",
      validateOpportunitiesScope,
      buildPagerVars,
      parseOpportunityFilters,
      this.$discordOpportunitiesController.search
    );
  }
}
