import { Router } from "express";
import { DiscordStatsController } from "../../../controllers/external/discord-stats.controller";

export class DiscordStatsRoutes {
  private $discordStatsController: DiscordStatsController;

  public router: Router;

  constructor() {
    this.$discordStatsController = new DiscordStatsController();

    this.router = Router();

    this.router.post("", this.$discordStatsController.create);
  }
}
