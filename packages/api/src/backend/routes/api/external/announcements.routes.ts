import { Router } from "express";
import { DiscordAnnouncementsController } from "../../../controllers/external/discord-announcements.controller";
import buildPagerVars from "../../../middlewares/build-pager-vars";

export class DiscordAnnouncementRoutes {
  private $discordAnnouncementsController: DiscordAnnouncementsController;

  public router: Router;

  constructor() {
    this.$discordAnnouncementsController = new DiscordAnnouncementsController();

    this.router = Router();

    this.router.get("", buildPagerVars, this.$discordAnnouncementsController.handler);
  }
}
