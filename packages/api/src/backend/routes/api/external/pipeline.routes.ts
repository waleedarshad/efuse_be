import { Router } from "express";
import { DiscordPipelineController } from "../../../controllers/external/discord-pipeline.controller";
import buildPagerVars from "../../../middlewares/build-pager-vars";
import { validatePipelineScope } from "../../../middlewares/validate-extapi-scope";

export class DiscordPipelineRoutes {
  private $discordPipelineController: DiscordPipelineController;

  public router: Router;

  constructor() {
    this.$discordPipelineController = new DiscordPipelineController();

    this.router = Router();

    this.router.get("/:game", validatePipelineScope, buildPagerVars, this.$discordPipelineController.getLeaderboard);
  }
}
