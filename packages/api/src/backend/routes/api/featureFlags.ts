import { Router } from "express";
import { FeatureFlagController } from "../../controllers/feature-flag.controller";

export class FeatureFlagRoutes {
  public readonly router: Router;
  private $featureFlagController: FeatureFlagController;

  constructor() {
    this.router = Router();
    this.$featureFlagController = new FeatureFlagController();

    // @route   POST /api/feature_flags/__webhook__
    // @desc    Webhook for bullet train when a feature is updated
    this.router.post("/__webhook__", this.$featureFlagController.webhookHandler);
  }
}
