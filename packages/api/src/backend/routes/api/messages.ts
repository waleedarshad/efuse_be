import { Router } from "express";

import { SearchController } from "../../controllers/search.controller";
const router = Router();
const searchController = new SearchController();

// @route GET /api/messages/search_users
// desc Search user to chat
// Deprecated - Not removing at the moment to support older mobile app versions
router.get("/search_users", searchController.searchGlobalUsersToChat);
module.exports = router;
