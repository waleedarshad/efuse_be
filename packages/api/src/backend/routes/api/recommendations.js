const express = require("express");

const router = express.Router();

const RecommendationsController = require("../../controllers/recommendations.controller");

// @route   GET /api/recommendations/followers
// @desc    returns a list of user recommendations for the user to follow
router.get("/followers", RecommendationsController.getFollowerRecommendations);

// @route   GET /api/recommendations/learning
// @desc    returns a list of learning article recommendations
router.get("/learning", RecommendationsController.getLearningRecommendations);

// @route   GET /api/recommendations/opportunities
// @desc    returns a list of opportunity recommendations
router.get("/opportunities", RecommendationsController.getOpportunitiesRecommendations);

// @route   GET /api/recommendations/organizations
// @desc    returns a list of organization recommendations
router.get("/organizations", RecommendationsController.getOrganizationRecommendations);

// @route   GET /api/recommendations/users
// @desc    returns a list of user recommendations
router.get("/users", RecommendationsController.getUsersRecommendations);

module.exports = router;
