const express = require("express");

const router = express.Router();
const { LearningUploader } = require("../../uploaders/learning.uploader");

const LearningsController = require("../../controllers/learnings.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");

// @route   POST /api/learning
// @desc    Create a new learning article
router.post("", LearningUploader.upload, LearningsController.createArticle);

// @route   PUT /api/learning/:id
// @desc    Updating existing learning article
router.put("/:id", LearningUploader.upload, LearningsController.editArticle);

// @route   POST /api/learning/image-upload
// @desc    Upload artcle images
router.post("/image-upload", LearningUploader.upload, LearningsController.uploadImage);

// @route   GET /api/learning
// @desc    Get paginated learning articles
router.get("", buildPagerVars, LearningsController.getArticles);

// @route   GET /api/learning/owned
// @desc    Get paginated learning articles that you or your orgs authored
router.get("/owned", buildPagerVars, LearningsController.getOwnedArticles);

// @route   GET /api/learning/categories
// @desc    Get all active categories
router.get("/categories", LearningsController.getCategories);

module.exports = router;
