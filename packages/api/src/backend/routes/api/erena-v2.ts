import express from "express";
import { ILogger, Logger } from "@efuse/logger";
import { ERENA_ORIGIN_CACHE_CONTROL } from "@efuse/key-store";

import {
  ERenaStaffController,
  ERenaScoreController,
  ERenaBracketController,
  ERenaMatchController,
  ERenaPlayerController,
  ERenaRoundController,
  ERenaTeamController,
  ERenaTournamentController,
  ERenaLeaderboardController
} from "../../controllers/erena-v2";
import { ErenaUploader } from "../../uploaders/erena.uploader";
import { cache } from "../../middlewares/express-memory-cache";
import { OriginCacheControl } from "../../middlewares/origin-cache-control";

export class ErenaV2Routes {
  private $logger: ILogger;
  private $staffController: ERenaStaffController;
  private $scoreController: ERenaScoreController;
  private $tournamentController: ERenaTournamentController;
  private $matchController: ERenaMatchController;
  private $roundController: ERenaRoundController;
  private $teamController: ERenaTeamController;
  private $playerController: ERenaPlayerController;
  private $leaderboardController: ERenaLeaderboardController;
  private $bracketController: ERenaBracketController;

  public router: express.Router;

  constructor() {
    this.$logger = Logger.create({ name: "erena-v2.routes" });
    this.$staffController = new ERenaStaffController();
    this.$scoreController = new ERenaScoreController();
    this.$tournamentController = new ERenaTournamentController();
    this.$matchController = new ERenaMatchController();
    this.$roundController = new ERenaRoundController();
    this.$teamController = new ERenaTeamController();
    this.$playerController = new ERenaPlayerController();
    this.$leaderboardController = new ERenaLeaderboardController();
    this.$bracketController = new ERenaBracketController();

    this.router = express.Router();

    // ========================
    // ||      STAFF        ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/staff:
     *  get:
     *    tags:
     *      - eRena Staff
     *    summary: Retrieve tournament staff members
     *    description: Retrieve all staff members for a tournament or optionally find a specific staff member.
     *    parameters:
     *      - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *      - $ref: '#/components/parameters/userId'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaStaffArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/staff",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$staffController
          .getStaffForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting staff members for tournament")
          );
      }
    );

    /**
     * @swagger
     * /api/erena/v2/staff/{id}:
     *  get:
     *    tags:
     *      - eRena Staff
     *    summary: Retrieve a single tournament staff member
     *    description: Retrieve staff member based on the entry id
     *    parameters:
     *      - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaStaffResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/staff/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$staffController
        .getStaff(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting Staff member"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/staff:
     *  post:
     *    tags:
     *      - eRena Staff
     *    summary: Create a new staff member
     *    description: Create an eRena Staff member for the specified tournament
     *    parameters:
     *      - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaStaffPost'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaStaffResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      500:
     *        $ref: '#/components/responses/InternalServerError'
     */
    this.router.post(
      "/tournament/:tournamentIdOrSlug/staff",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$staffController
          .postStaff(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while adding Staff member"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/staff/{id}:
     *  patch:
     *    tags:
     *      - eRena Staff
     *    summary: Update a staff member
     *    description: Update an existing eRena Staff member based on id.
     *    parameters:
     *      - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaStaffPatch'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaStaffResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     *      500:
     *        $ref: '#/components/responses/InternalServerError'
     */
    this.router.patch("/staff/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$staffController
        .patchStaff(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while updating existing staff member")
        );
    });

    /**
     * @swagger
     * /api/erena/v2/staff/{id}:
     *  delete:
     *    tags:
     *      - eRena Staff
     *    summary: Delete Staff Member
     *    description: Delete a particular eRena Tournament staff member based on the id
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaStaffResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete("/staff/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$staffController
        .deleteStaff(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while deleting staff"));
    });

    // ========================
    // ||      SCORES        ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/scores:
     *  get:
     *    tags:
     *      - eRena Scores
     *    summary: Get scores for tournament
     *    description: Retrieve all scores for the specified tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaMatchParameter'
     *    - $ref: '#/components/parameters/eRenaTeamParameter'
     *    - $ref: '#/components/parameters/eRenaOwnerParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaScoreArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/scores",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$scoreController
          .getScoresForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting scores for tournament")
          );
      }
    );

    /**
     * @swagger
     * /api/erena/v2/score/{id}:
     *  get:
     *    tags:
     *      - eRena Scores
     *    summary: Retrieve a single score
     *    description: Get score by id.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaScoreResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/score/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$scoreController
        .getScore(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting erena score"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/score:
     *  post:
     *    tags:
     *      - eRena Scores
     *    summary: Create score
     *    description: Create a score for a tournament. If submitting a score for a tournament with a bracket you must specify the match.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaScoreBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaScoreResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.post(
      "/tournament/:tournamentIdOrSlug/score",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$scoreController
          .postScore(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while creating score for tournament")
          );
      }
    );

    /**
     * @swagger
     * /api/erena/v2/score/{id}:
     *  patch:
     *    tags:
     *      - eRena Scores
     *    summary: Update Score.
     *    description: Update an existing score.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaScoreProperty'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaScoreResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch("/score/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$scoreController
        .patchScore(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating erena score"));
    });

    /**
     * @swagger
     * /api/erena/v2/score/{id}:
     *  delete:
     *    tags:
     *      - eRena Scores
     *    summary: Delete Score.
     *    description: Delete an existing score.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaScoreResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete("/score/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$scoreController
        .deleteScore(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while deleting score"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/players:
     *  get:
     *    tags:
     *      - eRena Players
     *    summary: Get players.
     *    description: Get players for a tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaTeamParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeScoreParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaPlayerArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/players",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$playerController
          .getPlayersForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting players for tournament")
          );
      }
    );

    /**
     * @swagger
     * /api/erena/v2/player/{id}?includeScores=true:
     *  get:
     *    tags:
     *      - eRena Players
     *    summary: Get a Player.
     *    description: Get player by Id.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    - $ref: '#/components/parameters/eRenaIncludeScoreParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaPlayerResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/player/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$playerController
        .getPlayer(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting player by Id"));
    });

    /**
     * @swagger
     * /api/erena/v2/team/{teamId}/player:
     *  post:
     *    tags:
     *      - eRena Players
     *    summary: Create a new player.
     *    description: Create a player for a team.
     *    parameters:
     *    - $ref: '#/components/parameters/eRenaTeamIdParameter'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaPlayerBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaPlayerResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.post(
      "/team/:teamId/player",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$playerController
          .postPlayer(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while creating new player"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/player/{id}:
     *  patch:
     *    tags:
     *      - eRena Players
     *    summary: Update a player.
     *    description: Update a player for a team.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaPlayerBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaPlayerResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch("/player/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$playerController
        .patchPlayer(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a player"));
    });

    /**
     * @swagger
     * /api/erena/v2/player/{id}:
     *  delete:
     *    tags:
     *      - eRena Players
     *    summary: Delete a player.
     *    description: Delete a player.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaPlayerResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete("/player/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$playerController
        .deletePlayer(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while deleting a player"));
    });

    // ========================
    // ||       TEAMS        ||
    // ========================
    /**
     * @swagger
     * /api/erena/v2/team/{id}:
     *  get:
     *    tags:
     *      - eRena Teams
     *    summary: Get Team.
     *    description: Retrieve a team based on id.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    - $ref: '#/components/parameters/eRenaIncludeScoreParameter'
     *    - $ref: '#/components/parameters/eRenaIncludePlayersParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentTeam'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/team/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$teamController
        .getTeam(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting a team"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/teams:
     *  get:
     *    tags:
     *      - eRena Teams
     *    summary: Get Teams for a tournament.
     *    description: Retrieve a list of teams for a specified tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaIncludeScoreParameter'
     *    - $ref: '#/components/parameters/eRenaRoundIdParameter'
     *    - $ref: '#/components/parameters/eRenaMatchIdParameter'
     *    - $ref: '#/components/parameters/eRenaIncludePlayersParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentTeamArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/teams",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$teamController
          .getTeamsForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting teams for tournament")
          );
      }
    );

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/team:
     *  post:
     *    tags:
     *      - eRena Teams
     *    summary: Create a team.
     *    description: Create a new team on an existing tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaTeamBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTeamResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.post(
      "/tournament/:tournamentIdOrSlug/team",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$teamController
          .postTeam(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while creating a team"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/team/{id}:
     *  patch:
     *    tags:
     *      - eRena Teams
     *    summary: Update Team.
     *    description: Update Team by id.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaTeamBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTeamResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch("/team/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$teamController
        .patchTeam(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a team by Id"));
    });

    /**
     * @swagger
     * /api/erena/v2/team/{id}:
     *  delete:
     *    tags:
     *      - eRena Teams
     *    summary: Delete a Team.
     *    description: Delete an existing team by Id.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTeamResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete("/team/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$teamController
        .deleteTeam(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while deleting a team"));
    });

    // ========================
    // ||      MATCHES       ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/matches:
     *  get:
     *    tags:
     *      - eRena Matches
     *    summary: Retrieve matches.
     *    description: Retrieve all the matches added to a tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaRoundIdParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaMatchArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/matches",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$matchController
          .getMatchesByTournament(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while retrieve matches"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/match/{id}:
     *  get:
     *    tags:
     *      - eRena Matches
     *    summary: Retrieve match.
     *    description: Retrieve an existing match.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaMatchResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/match/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$matchController
        .getMatch(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while retrieving a match"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/match:
     *  post:
     *    tags:
     *      - eRena Matches
     *    summary: Create match.
     *    description: Create a match between two teams.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaMatchPostBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/IERenaMatchResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.post(
      "/tournament/:tournamentIdOrSlug/match",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$matchController
          .postMatch(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while creating a match"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/match/{id}:
     *  patch:
     *    tags:
     *      - eRena Matches
     *    summary: Update a match.
     *    description: Update existing match and get the updated match and the next match in response.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaMatchPatchBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/IERenaMatchUpdateResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch("/match/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$matchController
        .patchMatch(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a match"));
    });

    /**
     * @swagger
     * /api/erena/v2/match/{id}/screenshots:
     *  patch:
     *    tags:
     *      - eRena Matches
     *    summary: Update a screenshots.
     *    description: Update a match's screenshots.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaIMatchResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch(
      "/match/:id/screenshots",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$matchController
          .saveScreenShot(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a screenshots"));
      }
    );

    // ========================
    // ||      ROUNDS        ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/rounds:
     *  get:
     *    tags:
     *      - eRena Rounds
     *    summary: Retrieve rounds.
     *    description: Retrieve all rounds for a tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaRoundArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/rounds",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$roundController
          .getRoundsForTournament(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while retrieving rounds"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/round/{id}:
     *  get:
     *    tags:
     *      - eRena Rounds
     *    summary: Retrieve round.
     *    description: Retrieve existing round.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaRoundResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/round/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$roundController
        .getRound(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while retrieving a round"));
    });

    /**
     * @swagger
     * /tournament/{tournamentIdOrSlug}/round:
     *  post:
     *    tags:
     *      - eRena Rounds
     *    summary: Create a round on a tournament.
     *    description: Create a round.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaRoundResponse'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaRoundPostBody'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.post(
      "/tournament/:tournamentIdOrSlug/round",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$roundController
          .postRound(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while creating a round"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/round/{id}:
     *  patch:
     *    tags:
     *      - eRena Rounds
     *    summary: Update round.
     *    description: Update an existing round.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaRoundPatchBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaRoundResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch("/round/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$roundController
        .patchRound(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a round"));
    });

    // ========================
    // ||      BRACKETS      ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/bracket/{id}:
     *  get:
     *    tags:
     *      - eRena Bracket
     *    summary: Get Bracket.
     *    description: Get Erena Bracket.
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaBracketResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/bracket/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$bracketController
        .getBracket(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting a erena bracket"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}/bracket:
     *  get:
     *    tags:
     *      - eRena Bracket
     *    summary: Retrieve the bracket.
     *    description: Retrieve the bracket for the specified tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaIncludePlayersParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeRoundsParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeScoreParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeMatchesParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeTeamsParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentBracketResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/bracket",
      OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
      cache(5),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$bracketController
          .getBracketForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting a bracket for a tournament")
          );
      }
    );

    // ========================
    // ||    LEADERBOARDS    ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/leaderboard/{tournamentIdOrSlug}/teams:
     *  get:
     *    tags:
     *      - Team Leaderboard
     *    summary: Retrieve team leaderboard.
     *    description: Retrieve leaderboard team. Optionally specify namespace
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/namespaceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaLeaderboardTeamResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/leaderboard/:tournamentIdOrSlug/teams",
      cache(5),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$leaderboardController
          .getTeamLeaderboard(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while retrieving team leader"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/leaderboard/{tournamentIdOrSlug}/players:
     *  get:
     *    tags:
     *      - Team Leaderboard
     *    summary: Get leaderboard player.
     *    description: Get player leaderboard, optionally specify namespace.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/namespaceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaLeaderboardPlayerResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/leaderboard/:tournamentIdOrSlug/players",
      cache(5),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$leaderboardController
          .getPlayerLeaderboard(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting a leaderboard player")
          );
      }
    );

    // ========================
    // ||    TOURNAMENTS     ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}:
     *  get:
     *    tags:
     *      - Tournament
     *    summary: Retrieve a Tournament.
     *    description: Retrieve a Tournament by the _id, tournamentId, or the slug value..
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/namespaceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug",
      cache(1),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$tournamentController
          .getTournament(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while retrieving a tournament"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/tournaments:
     *  get:
     *    tags:
     *      - Tournament
     *    summary: Get tournament info.
     *    description: Retrieve paginated list of tournaments.
     *    deprecated: true
     *    parameters:
     *    - $ref: '#/components/parameters/page'
     *    - $ref: '#/components/parameters/limit'
     *    - $ref: '#/components/parameters/eRenaStatusParameter'
     *    - $ref: '#/components/parameters/sortBy'
     *    - $ref: '#/components/parameters/sortDirection'
     *    - $ref: '#/components/parameters/startDateMin'
     *    - $ref: '#/components/parameters/startDateMax'
     *    - $ref: '#/components/parameters/endDateMin'
     *    - $ref: '#/components/parameters/endDateMax'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/PaginatedERenaTournament'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/tournaments", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$tournamentController
        .listTournaments(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting tournament info"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament:
     *  post:
     *    tags:
     *      - Tournament
     *    summary: Create a new tournament.
     *    description: Create a tournament. The properties below are the minimum required to create a tournament, but many more properties can be set during creation.
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaTournamentPostBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.post("/tournament", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$tournamentController
        .postTournament(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while creating a new tournament"));
    });

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}:
     *  delete:
     *    tags:
     *      - Tournament
     *    summary: Delete a tournament.
     *    description: Delete an existing tournament by based on the tournament _id, tournamentId, or slug.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete(
      "/tournament/:tournamentIdOrSlug",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$tournamentController
          .deleteTournament(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while deleting a tournament"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/tournament/{tournamentIdOrSlug}:
     *  patch:
     *    tags:
     *      - Tournament
     *    summary: Update a tournament.
     *    description: Update an existing tournament by based on the tournament _id, tournamentId, or slug property.
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaTournamentPatchBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.patch(
      "/tournament/:tournamentIdOrSlug",
      ErenaUploader.upload,
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$tournamentController
          .patchTournament(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a tournament"));
      }
    );

    /**
     * @swagger
     * /ad/{index}/tournament/{tournamentIdOrSlug}:
     *  put:
     *    tags:
     *      - Tournament Ad
     *    summary: Update tournament ad.
     *    description: Update tournament ad.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaAdParameter'
     *    requestBody:
     *      $ref: '#/components/requestBodies/ERenaAdPatchBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.put(
      "/ad/:index/tournament/:tournamentIdOrSlug/",
      ErenaUploader.upload,
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$tournamentController
          .updateTournamentAd(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while updating a tournament ad"));
      }
    );

    /**
     * @swagger
     * /ad/{index}/tournament/{tournamentIdOrSlug}:
     *  delete:
     *    tags:
     *      - Tournament Ad
     *    summary: Remove tournament ad.
     *    description: Remove tournament ad.
     *    parameters:
     *     - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *     - $ref: '#/components/parameters/eRenaAdParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete(
      "/ad/:index/tournament/:tournamentIdOrSlug",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$tournamentController
          .removeTournamentAd(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while removing a tournament Ad"));
      }
    );
  }
}
