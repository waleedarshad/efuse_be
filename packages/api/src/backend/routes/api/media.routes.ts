import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import LegacyMediaController from "../../controllers/legacy-media.controller";
import buildPagerVars from "../../middlewares/build-pager-vars";
import { MediaController } from "../../controllers/media.controller";

export class MediaRoutes {
  private $logger: ILogger;
  private $mediaController: MediaController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "media.routes", level: "debug" });
    this.$mediaController = new MediaController();

    this.router = Router();

    // @route   DELETE /api/media/s3/:fileKey/delete
    // @desc    Removes a file from s3 given file key
    this.router.delete("/s3/delete", LegacyMediaController.removeFromS3);

    // @route   GEt /api/media/:userId
    // @desc    Get media of a user
    this.router.get("/:userId", buildPagerVars, (req: Request, res: Response, next: NextFunction) => {
      this.$mediaController
        .getPaginatedFeedMediaOfUser(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while fetching getPaginatedFeedMediaOfUser")
        );
    });
  }
}
