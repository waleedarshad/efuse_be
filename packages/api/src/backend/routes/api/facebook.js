const express = require("express");
const FacebookController = require("../../controllers/facebook.controller");

const router = express.Router();

// @route  POST /api/facebook/auth
// @desc exchange a facebook user short lived token for a long lived auth token
// @body_data   facebookId <string>: the facebook user id
//              accessToken <string>: the short lived access token returned from Facebook Login
router.post("/auth", FacebookController.authenticateFacebook);

// @route  GET /api/facebook/pages
// @desc get a list of Facebook Pages the user can admin
router.get("/pages", FacebookController.getFacebookPages);

// @route  POST /api/facebook/pages
// @desc add a new Page to the user's facebook pages array for cross posting
// @body_data   pageName <string>: the name of the Facebook Page
//              pageId <string>: the Facebook Page's ID
router.post("/pages", FacebookController.addFacebookPage);

// @route  DELETE /api/facebook/pages/:id
// @desc remove a facebook Page from the user's facebook pages array
// @route_params  id: the eFuse ID for the user's Facebook Page object
router.delete("/pages/:id", FacebookController.removeFacebookPage);

module.exports = router;
