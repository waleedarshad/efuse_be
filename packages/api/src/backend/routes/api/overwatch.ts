import { Router } from "express";

import { OverwatchController } from "../../controllers/overwatch.controller";

const router = Router();
const overwatchController = new OverwatchController();

// @route  POST /api/overwatch/updatestats/:platform/:nickname
// @desc   Update overwatch player's profile stats for the given account
router.post("/stats/:platform/:nickname", overwatchController.updateOverwatchStats);

// @route GET /api/overwatch/:platform/:nickname
// @desc  Retrieve overwatch player's profile stats
router.get("/stats/:platform/:nickname", overwatchController.getOverwatchStats);

// @route GET /api/overwatch
// @desc  Update stats for the current authed user
router.get("/", overwatchController.get);

module.exports = router;
