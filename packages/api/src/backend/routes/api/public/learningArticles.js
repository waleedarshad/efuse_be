const express = require("express");

const router = express.Router();

const LearningsController = require("../../../controllers/public/learnings.controller");
const buildPagerVars = require("../../../middlewares/build-pager-vars");

// @route   GET /api/public/learning
// @desc    Get paginated learning articles

router.get("", buildPagerVars, LearningsController.getArticles);

module.exports = router;
