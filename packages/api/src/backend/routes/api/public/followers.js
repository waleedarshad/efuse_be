const express = require("express");

const router = express.Router();

const FollowersController = require("../../../controllers/public/followers.controller");

// @route   POST /api/public/users/:userId/timeline_update_flag
// @desc    Set a flag when timeline has more posts

router.get("/count", FollowersController.countFriends);

module.exports = router;
