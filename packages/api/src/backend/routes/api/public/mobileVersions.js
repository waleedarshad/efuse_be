const express = require("express");

const router = express.Router();

const MobileVersionsController = require("../../../controllers/public/mobile-versions.controller");

// @route   GET  /api/mobile_versions/:platform/:version
// @desc    Get latest version of a given platform

router.get("/:platform/:version", MobileVersionsController.getLatestVersion);

module.exports = router;
