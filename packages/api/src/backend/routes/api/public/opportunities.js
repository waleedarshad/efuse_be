const express = require("express");

const router = express.Router();

const OpportunitiesController = require("../../../controllers/public/opportunities.controller");
const buildPagerVars = require("../../../middlewares/build-pager-vars");

const PrivateOpportunitiesController = require("../../../controllers/legacy-opportunities.controller");

// @route   GET /api/public/opportunities
// desc     Get all opportunities
router.get("", buildPagerVars, PrivateOpportunitiesController.index);

// @route   GET /api/public/opportunities/show/:id
// desc     GET Opportunity By ID
router.get("/show/:id", OpportunitiesController.show);

// @route   GET /api/public/opportunities/byslug/:id
// desc     GET Opportunity By slug
router.get("/byslug/:slug", OpportunitiesController.showBySlug);

// @route   GET /api/public/opportunities/alias/:name
// @desc    returns the id for an opportunity alias
router.get("/alias/:name", OpportunitiesController.alias);

// @route GET /api/public/opportunities/promoted
// @desc  returns a list of promoted organization's opportunities ordered by date asc
router.get("/promoted", PrivateOpportunitiesController.promoted);

// @route GET /api/public/opportunities/recent
// @desc  Get recent opportunities from each type
router.get("/recent", PrivateOpportunitiesController.recentOpportunities);

module.exports = router;
