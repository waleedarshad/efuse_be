import { Router } from "express";

import * as RecommendationsController from "../../../controllers/recommendations.controller";

const router = Router();

/**
 * @route   GET /api/public/recommendations/followers
 * @desc    returns a list of user recommendations for the user to follow
 */
router.get("/followers", RecommendationsController.getFollowerRecommendations);

/**
 * @route   GET /api/public/recommendations/learning
 * @desc    returns a list of learning article recommendations
 */
router.get("/learning", RecommendationsController.getLearningRecommendations);

/**
 * @route   GET /api/public/recommendations/organizations
 * @desc    returns a list of organization recommendations
 */
router.get("/organizations", RecommendationsController.getOrganizationRecommendations);

module.exports = router;
