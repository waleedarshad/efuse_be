const express = require("express");

const router = express.Router();

const InviteCodeController = require("../../../controllers/public/invite-code.controller");

// @route   POST /api/public/invite_code/:inviteCode
// @desc    Get all invite code info with linked object
router.get("/:inviteCode", InviteCodeController.getInviteCode);

module.exports = router;
