import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { SearchController } from "../../../controllers/search.controller";

export class SearchPublicRoutes {
  private $logger: ILogger;
  private $searchController: SearchController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "search-public.routes", level: "debug" });
    this.$searchController = new SearchController();

    this.router = Router();

    /**
     * @swagger
     * /api/public/search/opportunities:
     *  get:
     *    tags:
     *      - Search
     *    summary: Search Opportunities publicly
     *    description: Search through all existing opportunities and return paginated collection.
     *    security: []
     *    parameters:
     *      - $ref: '#/components/parameters/page'
     *      - $ref: '#/components/parameters/limit'
     *      - $ref: '#/components/parameters/searchTerm'
     *      - $ref: '#/components/parameters/searchLocation'
     *      - $ref: '#/components/parameters/searchOpportunityType'
     *      - $ref: '#/components/parameters/searchSubType'
     *      - $ref: '#/components/parameters/searchPublishStatus'
     *      - $ref: '#/components/parameters/searchUser'
     *      - $ref: '#/components/parameters/searchOrganization'
     *      - $ref: '#/components/parameters/searchIncludeOrganizations'
     *      - $ref: '#/components/parameters/searchSort'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/PaginatedOpportunity'
     */
    this.router.get("/opportunities", (req: Request, res: Response, next: NextFunction) => {
      this.$searchController
        .searchOpportunities(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "An error occurred searching opportunities"));
    });
  }
}
