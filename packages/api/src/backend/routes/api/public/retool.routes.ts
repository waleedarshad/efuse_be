import { ILogger, Logger } from "@efuse/logger";
import express from "express";
import passport from "passport";

import { OrganizationPermissionsController } from "../../../controllers/organization-permissions.controller";

export class RetoolRoutes {
  private $logger: ILogger = Logger.create({ name: ".routes.retool" });
  private $organizationPermissionsController: OrganizationPermissionsController;

  public router: express.Router;

  constructor() {
    this.$organizationPermissionsController = new OrganizationPermissionsController();

    this.router = express.Router();

    // @route   POST /api/retool/org/permission
    // @desc    Creates a new permission for an organization
    this.router.post(
      "/org/permissions",
      passport.authenticate("headerapikey", { session: false }),
      this.$organizationPermissionsController.addOrgPermission
    );
  }
}
