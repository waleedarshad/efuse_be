import { Router } from "express";

const router = Router();

// @route   GET /api/__gitsha__
// @desc    Get the currently running git hash of code
router.get("/", (req, res) => {
  res.status(200).send(process.env.GIT_SHA);
});

module.exports = router;
