import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { LandingPageController } from "../../../controllers/landing-page.controller";

export class LandingPageRoutes {
  private $logger: ILogger;
  private $landingPageController: LandingPageController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "landing-page.routes", level: "debug" });
    this.$landingPageController = new LandingPageController();

    this.router = Router();

    /**
     * @swagger
     * /api/public/landing_pages/{slug}:
     *  get:
     *    tags:
     *      - Landing Page
     *    summary: Get resource
     *    description: Get a particular active landing page object by slug
     *    security: []
     *    parameters:
     *    - $ref: '#/components/parameters/slug'
     *    responses:
     *      200:
     *        description: LandingPage object
     *        content:
     *          application/json:
     *            schema:
     *              type: object
     *              properties:
     *                landingPage:
     *                  $ref: '#/components/schemas/LandingPage'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/:slug", (req: Request, res: Response, next: NextFunction) => {
      this.$landingPageController
        .getBySlug(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurred while getting landing-page by slug")
        );
    });
  }
}
