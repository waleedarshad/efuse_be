const express = require("express");

const router = express.Router();
const UsersController = require("../../../controllers/legacy-users.controller");
const SubscriptionsController = require("../../../controllers/subscriptions.controller");

// @route   GET /api/public/webhooks/twitch_webhook
// @desc    Verify callback URL for twitch webhooks
router.get("/twitch_webhook", UsersController.verifyTwitchWebhook);

// @route   POST /api/public/webhooks/twitch_webhook
// @desc    Handle twitch webhooks
router.post("/twitch_webhook", UsersController.handleTwitchWebhook);

// @route   POST /api/public/webhooks/recurly_webhook
// @desc    Handle Recurly webhooks
router.post("/recurly_webhook", SubscriptionsController.handleRecurlyWebhook);

module.exports = router;
