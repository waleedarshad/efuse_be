import { ERENA_ORIGIN_CACHE_CONTROL } from "@efuse/key-store";
import { ILogger, Logger } from "@efuse/logger";
import express from "express";

import {
  ERenaTournamentController,
  ERenaLeaderboardController,
  ERenaScoreController,
  ERenaBracketController,
  ERenaMatchController,
  ERenaTeamController
} from "../../../controllers/erena-v2";
import { cache } from "../../../middlewares/express-memory-cache";
import { OriginCacheControl } from "../../../middlewares/origin-cache-control";

export class ErenaV2PublicRoutes {
  private $logger: ILogger;

  private $scoreController: ERenaScoreController;
  private $tournamentController: ERenaTournamentController;
  private $matchController: ERenaMatchController;
  private $teamController: ERenaTeamController;
  private $leaderboardController: ERenaLeaderboardController;
  private $bracketController: ERenaBracketController;
  private $path: string;

  public router: express.Router;

  constructor() {
    this.$logger = Logger.create({ name: "erena-v2-public.routes" });
    this.$scoreController = new ERenaScoreController();
    this.$tournamentController = new ERenaTournamentController();
    this.$matchController = new ERenaMatchController();
    this.$teamController = new ERenaTeamController();
    this.$leaderboardController = new ERenaLeaderboardController();
    this.$bracketController = new ERenaBracketController();
    this.$path = "";

    this.router = express.Router();

    // ========================
    // ||      SCORES        ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/public/tournament/{tournamentIdOrSlug}/scores:
     *  get:
     *    tags:
     *      - eRena Scores (Public)
     *    summary: Get scores for tournament match
     *    description: Retrieve all scores for the specified tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaMatchParameter'
     *    - $ref: '#/components/parameters/eRenaTeamParameter'
     *    - $ref: '#/components/parameters/eRenaOwnerParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaScoreArrayResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/scores",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$scoreController
          .getScoresForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting score for tournament")
          );
      }
    );

    // ========================
    // ||       TEAMS        ||
    // ========================

    // @route   GET /api/erena/v2/public/tournament/:tournamentIdOrSlug/teams?includePlayers=true&includeScores=true&roundId=:roundId
    // @desc    Get teams for a tournament
    this.router.get(
      "/tournament/:tournamentIdOrSlug/teams",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$teamController
          .getTeamsForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting teams for tournament")
          );
      }
    );

    // ========================
    // ||      MATCHES       ||
    // ========================

    // @route   POST /api/erena/v2/public/tournament/:tournamentIdOrSlug/matches
    // @desc    Get matches for a tournament
    this.router.get(
      "/tournament/:tournamentIdOrSlug/matches",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$matchController
          .getMatchesByTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting matches by tournament")
          );
      }
    );

    // ========================
    // ||      BRACKETS      ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/public/tournament/{tournamentIdOrSlug}/bracket?includeRounds=true&includeMatches=true&includeTeams=true&includePlayers=true&includeScores=true:
     *  get:
     *    tags:
     *      - eRena Bracket (Public)
     *    summary: Retrieve the bracket.
     *    description: Retrieve the bracket for the specified tournament.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaIncludePlayersParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeRoundsParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeScoreParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeMatchesParameter'
     *    - $ref: '#/components/parameters/eRenaIncludeTeamsParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentBracketResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug/bracket",
      OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
      cache(5),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$bracketController
          .getBracketForTournament(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting bracket for tournament")
          );
      }
    );

    // ========================
    // ||    LEADERBOARDS    ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/public/leaderboard/{tournamentIdOrSlug}/teams?namespace={string}:
     *  get:
     *    tags:
     *      - Team Leaderboard (Public)
     *    summary: Retrieve team leaderboard.
     *    description: Retrieve leaderboard team.optionally specify namespace
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/namespaceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaLeaderboardTeamResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/leaderboard/:tournamentIdOrSlug/teams",
      OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
      cache(5),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$leaderboardController
          .getTeamLeaderboard(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting team leaderboard"));
      }
    );

    /**
     * @swagger
     * /api/erena/v2/public/leaderboard/{tournamentIdOrSlug}/players?namespace={string}:
     *  get:
     *    tags:
     *      - Team Leaderboard (Public)
     *    summary: Get leaderboard player.
     *    description: Get player leaderboard, optionally specify namespace.
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/namespaceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaLeaderboardPlayerResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/leaderboard/:tournamentIdOrSlug/players",
      OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
      cache(5),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$leaderboardController
          .getPlayerLeaderboard(req, res, next)
          .catch((error: unknown) =>
            this.$logger.error({ error }, "an error occurred while getting player leaderboard")
          );
      }
    );

    // ========================
    // ||    TOURNAMENTS     ||
    // ========================

    /**
     * @swagger
     * /api/erena/v2/public/tournament/{tournamentIdOrSlug}:
     *  get:
     *    tags:
     *      - Tournament (Public)
     *    summary: Retrieve a Tournament.
     *    description: Retrieve a Tournament by the _id, tournamentId, or the slug value..
     *    parameters:
     *    - $ref: '#/components/parameters/erenaTournamentIdOrSlug'
     *    - $ref: '#/components/parameters/eRenaIncludeStatsParameter'
     *    - $ref: '#/components/parameters/namespaceParameter'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/ERenaTournamentResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(
      "/tournament/:tournamentIdOrSlug",
      OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
      cache(1),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$tournamentController
          .getTournament(req, res, next)
          .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting tournament"));
      }
    );
  }
}
