const express = require("express");

const router = express.Router();

// @route   GET /api/__status__
// @desc    Get current status of server for load balancing checks
router.get("/", (req, res) => {
  res.status(200).send("OK");
});

module.exports = router;
