const express = require("express");

const router = express.Router();

const PublicController = require("../../../controllers/public/organizations.controller");
const OrganizationsController = require("../../../controllers/legacy-organizations.controller");
const buildPagerVars = require("../../../middlewares/build-pager-vars");

// @route   GET /api/public/organizations/search_by_name
// @desc    Search Organization By Name
router.get("/search_by_name", PublicController.searchByName);

// @route   GET /api/public/organizations/show/:id
// @desc    Get Organization BY ID
router.get("/show/:id", PublicController.show);

// @route   GET /api/public/organizations/byinvite/:inviteCode
// desc     GET organizations by invite code
router.get("/byinvite/:inviteCode", PublicController.showByInvite);

// @route   GET /api/public/organizations/slug/:slug
// desc     GET organizations by slug (shortName)
router.get("/slug/:slug", PublicController.showBySlug);

// @route   GET /api/public/organizations
// @desc    Get all organizations
router.get("/", buildPagerVars, OrganizationsController.index);

module.exports = router;
