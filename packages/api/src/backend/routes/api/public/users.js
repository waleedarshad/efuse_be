const express = require("express");
const passport = require("passport");

const router = express.Router();

const UsersJSController = require("../../../controllers/public/users.controller");
const { UsersController } = require("../../../controllers/users.controller");

const { EventTypes } = require("../../../../lib/efuse-segment-analytics/event-types");
const { trackEvent } = require("../../../middlewares/track-event");
const { RateLimitMiddleware } = require("../../../middlewares/rate-limit.middleware");

const usersController = new UsersController();

// @route   POST /api/public/users/:userId/timeline_update_flag
// @desc    Set a flag when timeline has more posts
router.post("/:userId/timeline_update_flag", UsersJSController.timelineUpdateFlag);

// @route   POST /api/public/users/:userId/add_comment_to_feed
// @desc    Emit new comment
router.post("/:userId/add_comment_to_feed", UsersJSController.addCommentToFeed);

// @route   POST /api/public/users/:userId/delete_comment_from_feed
// @desc    Emit Deleted comment
router.post("/:userId/delete_comment_from_feed", UsersJSController.removeCommentFromFeed);

// @route   POST /api/public/users/:userId/update_comment_on_feed
// @desc    Emit Updated Comment
router.post("/:userId/update_comment_on_feed", UsersJSController.updateCommentOnFeed);

// @route   POST /api/public/users/:userId/notify
// @desc    Emit Notification
router.post("/:userId/notify", UsersJSController.notifyAboutNotification);

// @route   post /api/public/users/send_password_reset
// @desc    send password reset link
router.post(
  "/send_password_reset",
  RateLimitMiddleware.rateLimitResetPassword,
  usersController.generateLinkAndSendResetPasswordEmail
);

// @route   post /api/public/users/reset_password
// @desc    reset password
router.put("/reset_password/:token", usersController.resetPassword);

// @route   put /api/public/users/resend_email_verification
// @desc    Resend email verification mail
router.put("/resend_email_verification", UsersJSController.resendVerificationEmail);

// @route   put /api/public/users/verify_email
// @desc    Verify Email
router.put("/verify_email/:token", UsersJSController.verifyEmail);

// @route   GET /api/public/users/show/:id
// @desc    Get user by id
router.get("/show/:id", UsersJSController.getUserById);

// @route   GET /api/public/users/username/:username
// @desc    Get user by slug
router.get("/username/:username", UsersJSController.getUserByUsername);

// @route   GET /api/public/users/:idOrUsername/portfolio
// @desc    Get user by idOrUsername for portfolio
router.get(
  "/:idOrUsername/portfolio",
  trackEvent(EventTypes.PORTFOLIO_VIEW, "idOrUsername"),
  usersController.getProfileByIdOrUsername
);

// @route   POST /api/public/users/:username/ban
// @desc    Ban the passed user
router.post("/:username/ban", passport.authenticate("headerapikey", { session: false }), usersController.ban);

// @route   POST /api/public/users/:username/unban
// @desc    Unban the passed user
router.post("/:username/unban", passport.authenticate("headerapikey", { session: false }), usersController.unban);

module.exports = router;
