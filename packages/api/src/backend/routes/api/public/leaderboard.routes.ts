import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { LeaderboardsController } from "../../../controllers/leaderboards.controller";

export class LeaderboardRoutes {
  private $logger: ILogger;
  private $leaderboardsController: LeaderboardsController;

  public router: Router;

  constructor() {
    this.$logger = Logger.create({ name: "leaderboard.routes", level: "debug" });
    this.$leaderboardsController = new LeaderboardsController();

    this.router = Router();

    /**
     * @swagger
     * /api/public/leaderboard/leagueoflegends:
     *  get:
     *    tags:
     *      - Leaderboard
     *    summary: LeagueOfLegends Leaderboard
     *    description: Get LeagueOfLegends Leaderboards sorted by Rank/Tier combination for RANKED_SOLO_5x5 queueType
     *    security: []
     *    responses:
     *      200:
     *        $ref: '#/components/responses/LeagueOfLegendsLeaderboardResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/leagueoflegends", (req: Request, res: Response, next: NextFunction): void => {
      this.$leaderboardsController
        .getLOLLeaderboard(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "Error occurred while fetching LOL Leaderboard"));
    });

    /**
     * @swagger
     * /api/public/leaderboard/overwatch:
     *  get:
     *    tags:
     *      - Leaderboard
     *    summary: Overwatch Leaderboard
     *    description: Get Overwatch Leaderboard sorted by wins in Competitive realm
     *    security: []
     *    responses:
     *      200:
     *        $ref: '#/components/responses/OverwatchLeaderboardResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get("/overwatch", (req: Request, res: Response, next: NextFunction): void => {
      this.$leaderboardsController
        .getOverwatchLeaderboard(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "Error occurred while fetching Overwatch Leaderboard")
        );
    });
  }
}
