import { NextFunction, Request, Response, Router } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { PathwaysController } from "../../../controllers/pathways.controller";

export class PathwaysPublicRoutes {
  private $logger: ILogger;

  public router: Router;
  private $pathwaysController: PathwaysController;

  constructor() {
    this.$logger = Logger.create({ name: "pathways.routes", level: "debug" });
    this.$pathwaysController = new PathwaysController();

    this.router = Router();

    /**
     * @swagger
     * /api/public/pathways:
     *   get:
     *     tags:
     *       - Pathways
     *     description: Get all active pathways
     *     responses:
     *       200:
     *         description: Collection of active pathways
     *         content:
     *           application/json:
     *             schema:
     *               type: array
     *               items:
     *                 $ref: '#/components/schemas/Pathway'
     *
     */
    this.router.get("", (req: Request, res: Response, next: NextFunction) => {
      this.$pathwaysController
        .getActive(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting pathways"));
    });
  }
}
