import { Router, Request, Response, NextFunction } from "express";
import { ILogger, Logger } from "@efuse/logger";

import { SitemapController } from "../../../controllers/public/sitemap.controller";

const router = Router();
const controller = new SitemapController();
const logger: ILogger = Logger.create({ name: ".sitemap" });

// @route   POST /api/sitemap
// @desc    Generates new sitemap
router.post("", (req: Request, res: Response, next: NextFunction) => {
  controller
    .createSitemap(req, res, next)
    .catch((error: unknown) => logger.error({ error }, "an error occurred while generating sitemap"));
});

module.exports = router;
