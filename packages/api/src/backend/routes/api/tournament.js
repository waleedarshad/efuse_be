const express = require("express");

const router = express.Router();
// const { expressRouteCache10s } = require("../../../lib/redis/helpers");
const { ErenaUploader } = require("../../uploaders/erena.uploader");
const { ERenaController } = require("../../controllers/erena.controller");
const { cache } = require("../../middlewares/express-memory-cache");
const { OriginCacheControl } = require("../../middlewares/origin-cache-control");
const { ERENA_ORIGIN_CACHE_CONTROL } = require("@efuse/key-store");

const erenaController = new ERenaController();

// @route   /api/tournament/create
// @desc    Create a new tournament
router.post("/create", erenaController.createTournament);

// @route   /api/erena/tournamentIdOrSlug/delete
// @desc    Delete tournament
router.delete("/:tournamentIdOrSlug/delete", erenaController.deleteTournament);

// @route   /api/erena/:tournamentIdOrSlug/update
// @desc    Update tournament
router.patch("/:tournamentIdOrSlug/update", ErenaUploader.upload, erenaController.updateTournament);

// @route   /api/erena/list
// @desc    Get a list of all tournaments
router.get("/list", erenaController.getTournamentList);

// @route   /api/tournament/:tournamentIdOrSlug/info
// @desc    Get info for a tournament, used in Twitch extension
router.get(
  "/:tournamentIdOrSlug/info",
  OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
  cache(5),
  erenaController.getTournamentInfo
);

// @route   /api/erena/:tournamentIdOrSlug/stats
// @desc    Get stats for a particular tournament
router.get(
  "/:tournamentIdOrSlug/stats",
  OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
  cache(1),
  erenaController.getTournamentStats
);

// @route   /api/tournament/:tournamentIdOrSlug/leaderboard/players
// @desc    Get the players leaderboard, used in Twitch extension
router.get(
  "/:tournamentIdOrSlug/leaderboard/players",
  OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
  cache(5),
  erenaController.playerLeaderboard
);

// @route   /api/erena/:tournamentIdOrSlug/leaderboard/teams
// @desc    Get the teams leaderboard, used in Twitch extension
router.get(
  "/:tournamentIdOrSlug/leaderboard/teams",
  OriginCacheControl(Number(ERENA_ORIGIN_CACHE_CONTROL), 300, 300),
  cache(5),
  erenaController.teamLeaderboard
);

// @route   /api/erena/:tournamentIdOrSlug/team/create
// @desc    Create a new team in a tournament
router.post("/:tournamentIdOrSlug/team/create", erenaController.createTeam);

// @route   /api/erena/:tournamentIdOrSlug/team/:erenaTeamId
// @desc    Update an existing team in a tournament
router.patch("/:tournamentIdOrSlug/team/:erenaTeamId", erenaController.updateTeam);

// @route   /api/erena/:tournamentIdOrSlug/team/list
// @desc    List all teams for a given tournament
router.get("/:tournamentIdOrSlug/team/list", erenaController.listTeams);

// @route   /api/erena/:tournamentIdOrSlug/team/:erenaTeamId/player/create
// @desc    Create a new player
router.post("/:tournamentIdOrSlug/team/:erenaTeamId/player/create", erenaController.createPlayer);

// @route   /api/erena/:tournamentIdOrSlug/team/:erenaTeamId/player/:erenaPlayerId
// @desc    Delete a particular player
router.delete("/:tournamentIdOrSlug/team/:erenaTeamId/player/:erenaPlayerId", erenaController.deletePlayer);

// @route   /api/erena/:tournamentIdOrSlug/team/:erenaTeamId/player/:erenaPlayerId
// @desc    Update player
router.patch("/:tournamentIdOrSlug/team/:erenaTeamId/player/:erenaPlayerId", erenaController.updatePlayer);

// @route   /api/erena/:tournamentIdOrSlug/team/:erenaTeamId
// @desc    Delete a new team
router.delete("/:tournamentIdOrSlug/team/:erenaTeamId", erenaController.deleteTeam);

// @route   /api/erena/:tournamentIdOrSlug/team/:erenaTeamId/player/list
// @desc    List all teams for a given tournament
router.get("/:tournamentIdOrSlug/team/:erenaTeamId/player/list", erenaController.listPlayers);

// @route   /api/erena/:tournamentIdOrSlug/ad/update
// @desc    update tournament ad
router.put("/:tournamentIdOrSlug/ad/update", ErenaUploader.upload, erenaController.updateTournamentAd);

// @route   /api/erena/:tournamentIdOrSlug/ad/:index/delete
// @desc    remove tournament ad
router.delete("/:tournamentIdOrSlug/ad/:index/delete", erenaController.removeTournamentAd);

// @route   POST /api/erena/:tournamentIdOrSlug/match
// @desc    Create a match
router.post("/:tournamentIdOrSlug/match", erenaController.createMatch);

// @route   PATCH /api/erena/:tournamentIdOrSlug/match/:matchId
// @desc    Update a particular match
router.patch("/:tournamentIdOrSlug/match/:matchId", erenaController.updateMatch);

// @route   POST /api/erena/:tournamentIdOrSlug/round
// @desc    Create a round
router.post("/:tournamentIdOrSlug/round", erenaController.createRound);

// @route   PATCH /api/erena/:tournamentIdOrSlug/round/:roundId
// @desc    Update a particular round
router.patch("/:tournamentIdOrSlug/round/:roundId", erenaController.updateRound);

// @route   PATCH /api/erena/:tournamentIdOrSlug/match/:erenaMatchId/save_screenshot
// @desc    Save screenshot against a match
router.patch("/:tournamentIdOrSlug/match/:erenaMatchId/save_screenshot", erenaController.uploadScreenshot);

module.exports = router;
