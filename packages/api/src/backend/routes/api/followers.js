const express = require("express");

const router = express.Router();

const FollowersController = require("../../controllers/followers.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");
const { SearchController } = require("../../controllers/search.controller");

const searchController = new SearchController();

// @route   GET /api/followers/is_followed/:id
// @desc    Check whether current user follows a particular user

router.get("/is_followed/:id", FollowersController.isFollowed);

// @route   GET /api/followers/follow/:id
// @desc    Follow a particular User

router.get("/follow/:id", FollowersController.follow);

// @route   DELETE /api/followers/unfollow/:id
// @desc    Unfollow a particular User

router.delete("/unfollow/:id", FollowersController.unfollow);

// @route   GET /api/followers/count
// @desc    Get the number of followers & followees

router.get("/count", FollowersController.countFriends);

// @route   GET /api/followers/get_followers/:id
// @desc    Get the followers for a particular user

router.get("/get_followers/:id", buildPagerVars, FollowersController.getFollowers);

// @route   GET /api/followers/get_followees/:id
// @desc    Get the followee for a particular user

router.get("/get_followees/:id", buildPagerVars, FollowersController.getFollowees);

// @route   GET /api/followers/get_followees_ids/:id
// @desc    Get the array of user ids for all followees for a particular user

router.get("/get_followees_ids/:id", FollowersController.getFolloweeIds);

// @route   GET /api/followers/search
// @desc    Search followers
// Deprecated - Not removing at the moment to support older mobile app versions
router.get("/search", searchController.mentionFollowers);

// @route   GET /api/followers/is_follows/:id
// @desc    Check whether  portflio user follows current user

router.get("/is_follows/:id", FollowersController.isFollowsYou);

module.exports = router;
