const express = require("express");
const OrganizationsController = require("../../controllers/legacy-organizations.controller");
const { NewOrganizationsController } = require("../../controllers/organizations.controller");
const OrganizationFollowersController = require("../../controllers/organization-followers.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");
const mapAsNull = require("../../middlewares/map-as-null");
const { OrganizationUploader } = require("../../uploaders/organization.uploader");
const OpportunitiesController = require("../../controllers/legacy-opportunities.controller");
const { OrganizationAccoladesUploader } = require("../../uploaders/organization-accolades.uploader");
const { PlayerCardUploader } = require("../../uploaders/player-card.uploader");

const { MembersController } = require("../../controllers/members.controller");
const membersController = new MembersController();

class OrganizationsRoutes {
  constructor() {
    this.controller = new NewOrganizationsController();
    this.router = express.Router();

    // @route   GET /api/organizations
    // @desc    Get all organizations
    this.router.get("/", buildPagerVars, OrganizationsController.index);

    // @route   POST /api/organizations
    // @desc    Creates a new Organization
    this.router.post("/", OrganizationUploader.upload, this.controller.create);

    // @route   GET /api/organizations/show/:id
    // @desc    Get Organization BY ID
    this.router.get("/show/:id", OrganizationsController.show);

    // @route   GET /api/organizations/edit/:id
    // @desc    Retrieves an organization by id with field indicating if current user can edit it
    this.router.get("/edit/:id", this.controller.getOrgToEdit);

    // @route   PUT /api/organizations/update/:id
    this.router.put("/update/:id", OrganizationUploader.upload, mapAsNull, OrganizationsController.update);

    // @route   GET /api/organizations/owned
    // @desc    Get organizations owned by current user
    this.router.get("/owned", buildPagerVars, OrganizationsController.owned);

    // @route   GET /api/organizations/postable
    // @desc    Get organizations on which current user can post
    this.router.get("/postable", buildPagerVars, OrganizationsController.currentUsersPostableOrganizations);

    // @route   GET /api/organizations/recent
    // @desc    Get most recent organizations
    this.router.get("/recent", buildPagerVars, OrganizationsController.recentOrganizations);

    // @route   PUT /api/organizations/join/:id
    this.router.put("/join/:id", OrganizationsController.joinOrganization);

    // @route   PUT /api/organizations/leave_organization/:id
    this.router.delete("/leave_organization/:id", OrganizationsController.leaveOrganization);

    // @route   GET /api/organization/members/:id
    // @desc    Get users by organization id
    this.router.get("/members/:id", OrganizationsController.getMembersByOrganizationId);

    // @route   PUT /api/organizations/make_captain/:id
    this.router.put("/make_captain/:id", OrganizationsController.addAsCaptain);

    // @route   PUT /api/organizations/remove_captain/:id
    this.router.put("/remove_captain/:id", OrganizationsController.removeAsCaptain);

    // @route   DELETE /api/organizations/kick_member/:id
    // @desc    Removes user from list of organization members
    this.router.delete("/kick_member/:id", this.controller.kickMember);

    // @route   PUT /api/organizations/ban_member/:id
    this.router.put("/ban_member/:id", OrganizationsController.banMember);

    // @route   PUT /api/organizations/unban_member/:id
    this.router.put("/unban_member/:id", OrganizationsController.unBanMember);

    // @route   PUT /api/organizations/changeowner/:id
    // @desc    Changes orgnization owner
    this.router.put("/changeowner/:id", this.controller.changeOwner);

    // @route   POST /api/organizations/:organizationId/follow
    // @desc    Follow an organization
    this.router.post("/:organizationId/follow", OrganizationFollowersController.create);

    // @route   DELETE /api/organizations/:organizationId/unfollow
    // @desc    Unfollow an organization
    this.router.delete("/:organizationId/unfollow", OrganizationFollowersController.deleteItem);

    // @route   GET /api/organizations/:organizationId/followers
    // @desc    Get followers
    this.router.get("/:organizationId/followers", buildPagerVars, OrganizationFollowersController.index);

    // @route   GET /api/organizations/opportunities
    // @desc    Get organization's opportunities
    this.router.get("/opportunities", buildPagerVars, OpportunitiesController.organizationOpportunities);

    /**
     * @swagger
     * /api/organizations/{organizationId}/about:
     *  patch:
     *    tags:
     *      - Organization
     *    summary: Update about section of an organization
     *    description: Update about section of an organization. Only eFuse admin, organization owner & captains are allowed
     *    parameters:
     *    - $ref: '#/components/parameters/organizationId'
     *    requestBody:
     *      $ref: '#/components/requestBodies/OrganizationAboutBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/Organization'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      403:
     *        $ref: '#/components/responses/Forbidden'
     *      500:
     *        $ref: '#/components/responses/InternalServerError'
     */
    this.router.patch("/:organizationId/about", this.controller.updateAbout);

    // @route   PATCH /api/organizations/:organizationId/promo_video
    // @desc    Update organization's promo video section
    this.router.patch("/:organizationId/promo_video", OrganizationsController.updatePromoVideo);

    // @route   PATCH /api/organizations/:organizationId/change_status
    // @desc    Update organization's publish status
    this.router.patch("/:organizationId/change_status", OrganizationsController.changePublishStatus);

    // @route   PATCH /api/organizations/:organizationId/accolades
    // @desc    Update organization's accolades
    this.router.patch(
      "/:organizationId/accolades",
      OrganizationAccoladesUploader.upload,
      OrganizationsController.manageAccolades
    );

    // @route   DELETE /api/organizations/:organizationId/:accoladeKey/:accoladeId
    // @desc    Delete a particular accolade item
    this.router.delete("/:organizationId/accolades/:accoladeKey/:accoladeId", OrganizationsController.removeAccoldate);

    // @route   POST /api/organizations/:organizationId/player_card
    // @desc    CREATE/UPDATE player cards
    this.router.post(
      "/:organizationId/player_card",
      PlayerCardUploader.upload,
      OrganizationsController.managePlayerCard
    );

    // @route   DELETE /api/organizations/:organizationId/player_card/:id
    // @desc    DELETE player card
    this.router.delete("/:organizationId/player_card/:id", OrganizationsController.removePlayerCard);

    // @route   PUT /api/organizations/:organizationId/discord_server
    // @desc    CREATE/UPDATE discord server
    this.router.put("/:organizationId/discord_server", OrganizationsController.discordServer);

    // @route   PUT /api/organizations/:organizationId/remove_discord_server
    // @desc    Delete the discord server
    this.router.put("/:organizationId/remove_discord_server", OrganizationsController.removeDiscordServer);

    // @route   DELETE /api/organizations/:organizationId
    // @desc    Delete an organization
    this.router.delete("/:organizationId", OrganizationsController.deleteItem);

    // @route   PATCH /api/organizations/:organizationId/carousel_video
    // @desc    Manage organization's carousel videos
    this.router.patch("/:organizationId/carousel_video", OrganizationsController.manageCarouselVideo);

    // @route   DELETE /api/organizations/:organizationId/carousel_video/:videoCarouselId
    // @desc    Delete a particular carousel video by id
    this.router.delete("/:organizationId/carousel_video/:videoCarouselId", OrganizationsController.deleteCarouselVideo);

    // @route   POST /api/organizations/:organizationId/invitebyemail
    // @desc    Invite member using send grid email
    this.router.post("/:organizationId/invitebyemail", OrganizationsController.inviteByEmail);

    // @route   GET /api/public/organizations/slug/:slug
    // desc     GET organizations by slug (shortName)
    this.router.get("/slug/:slug", OrganizationsController.showBySlug);

    // @route   PATCH /api/organizations/:organizationId/members/:memberId
    // @desc    Update a particular member of organization
    this.router.patch("/:organizationId/members/:memberId", membersController.update);

    /**
     * @swagger
     * /api/organizations/{organizationId}/games:
     *  patch:
     *    tags:
     *      - Organization
     *    summary: Update games of an organization
     *    description: Update games of an organization. Only eFuse admin, organization owner & captains are allowed
     *    parameters:
     *    - $ref: '#/components/parameters/organizationId'
     *    requestBody:
     *      $ref: '#/components/requestBodies/OrganizationGamesBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/Organization'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      403:
     *        $ref: '#/components/responses/Forbidden'
     *      500:
     *        $ref: '#/components/responses/InternalServerError'
     */
    this.router.patch("/:organizationId/games", this.controller.updateGames);
  }
}

const organizationsRoutes = new OrganizationsRoutes();
module.exports = organizationsRoutes.router;
