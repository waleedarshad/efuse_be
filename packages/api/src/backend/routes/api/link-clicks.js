const express = require("express");

const router = express.Router();

const LinksController = require("../../controllers/links.controller");

// @route   POST /api/link_click
// @desc    Track a link click

router.post("", LinksController.storeLinkClick);

module.exports = router;
