import { Router } from "express";
import { PathwaysController } from "../../controllers/pathways.controller";

class PathwaysRoutes {
  public readonly router: Router;
  private pathwaysController: PathwaysController;

  constructor() {
    this.router = Router();

    this.pathwaysController = new PathwaysController();

    /**
     * @swagger
     * /api/pathways:
     *   get:
     *     tags:
     *       - Pathways
     *     description: Get all active pathways
     *     responses:
     *       200:
     *         description: Collection of active pathways
     *         content:
     *           application/json:
     *             schema:
     *               type: array
     *               items:
     *                 $ref: '#/components/schemas/Pathway'
     *       401:
     *         $ref: '#/components/responses/Unauthorized'
     *
     */
    this.router.get("", this.pathwaysController.getActive);
  }
}

const pathwaysRoutes = new PathwaysRoutes();
module.exports = pathwaysRoutes.router;
