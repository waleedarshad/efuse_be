const express = require("express");
const passport = require("passport");

const router = express.Router();

const { TwitterController } = require("../../controllers/twitter.controller");
const RegistrationsController = require("../../controllers/registrations.controller");
const SessionsController = require("../../controllers/sessions.controller");
const SteamController = require("../../controllers/steam.controller");
const { GoogleAuthController } = require("../../controllers/external-account-auth/google-auth.controller");
const { BnetAuthController } = require("../../controllers/external-account-auth/bnet-auth.controller");
const { SnapchatAuthController } = require("../../controllers/external-account-auth/snapchat-auth.controller");
const { LinkedinAuthController } = require("../../controllers/external-account-auth/linkedin-auth.controller");
const { TwitchAuthController } = require("../../controllers/external-account-auth/twitch-auth.controller");
const { StateSpaceAuthController } = require("../../controllers/external-account-auth/state-space-auth.controller");
const { Auth0Controller } = require("../../controllers/auth0.controller");
const { RiotAuthController } = require("../../controllers/external-account-auth/riot-auth.controller");
const { SessionController } = require("../../controllers/session.controller");

const { RateLimitMiddleware } = require("../../middlewares/rate-limit.middleware");
const { GlobalKillSwitch } = require("../../middlewares/global-kill-switch.middleware");
const validateAuth = require("../../middlewares/validate-auth");

const { DOMAIN } = require("@efuse/key-store");

const twitterController = new TwitterController();
const googleAuthController = new GoogleAuthController();
const bnetAuthController = new BnetAuthController();
const snapchatAuthController = new SnapchatAuthController();
const linkedinAuthController = new LinkedinAuthController();
const twitchAuthController = new TwitchAuthController();
const stateSpaceAuthController = new StateSpaceAuthController();
const auth0Controller = new Auth0Controller();
const riotAuthController = new RiotAuthController();
const sessionController = new SessionController();

// @route   GET /api/auth/test
// @desc    Test Route to see implementation
router.get("/test", SessionsController.test);

// TODO: remove this endpoints once front end and mobile are completely migrated
// @route   GET /api/auth/twitter
// @desc    Load consent screen
router.get("/twitter", twitterController.login);

// @route   GET /api/auth/twitter/callback
// @desc    Handle twitter callback
router.get("/twitter/callback", twitterController.handleCallback);

// @route   POST /api/auth/login
// @desc    Login Route
router.post("/login", GlobalKillSwitch.loginKillSwitch, RateLimitMiddleware.rateLimitUserLogin, validateAuth);

// @route   POST /api/auth/register
// @desc    Signup Route
router.post(
  "/register",
  GlobalKillSwitch.registrationKillSwitch,
  RateLimitMiddleware.rateLimitUserRegistration,
  RegistrationsController.create
);

// @route   POST /api/auth/discord
// @desc    Signup Route for Discord signup
router.post("/discord", RegistrationsController.discordSignup);

// @route   POST /api/auth/twitch
// @desc    Signup Route for Twitch signup
router.post("/twitch", RegistrationsController.twitchSignup);

// @route GET /api/auth/validate_email/:email
// @deprecated since November, 2020. Please use POST /api/auth/validate_email/:email instead.
// @desc    Email Validation
router.get("/validate_email/:email", RegistrationsController.validateEmailDeprecated);

// @route POST /api/auth/validate_email/:email
// @desc    Email Validation
router.post("/validate_email", RegistrationsController.validateEmail);

// @route GET /api/auth/validate_username/:username
// @deprecated since November, 2020. Please use POST /api/auth/validate_username/:username instead.
// @desc    Username Validation
router.get("/validate_username/:username", RegistrationsController.validateUsernameDeprecated);

// @route POST /api/auth/validate_username/:username
// @desc    Username Validation
router.post("/validate_username", RegistrationsController.validateUsername);

// @route   GET /api/auth/steam
// @desc    Load Steam Consent Screen
router.get("/steam", SteamController.setToken, passport.authenticate("steam"));

// @route   GET /api/auth/steam/callback
// @desc    Steam will redirect to this URL
router.get(
  "/steam/callback",
  passport.authenticate("steam", {
    failureRedirect: `${DOMAIN}/external_accounts/steam`,
    session: false
  }),
  SteamController.updateUser
);

// @route   GET /api/auth/linkedin
// @desc    Load Linkedin Consent Screen
router.get("/linkedin", linkedinAuthController.getConsent);

// @route   GET /api/auth/linkedin/callback
// @desc    Linkedin will redirect to this URL
router.get("/linkedin/callback", linkedinAuthController.callbackHandler, linkedinAuthController.updateProfile);

// @route   GET /api/auth/bnet
// @desc    Load Bnet Consent Screen
router.get("/bnet", bnetAuthController.getConsent);

// @route   GET /api/auth/bnet/callback
// @desc    Bnet will redirect to this URL
router.get("/bnet/callback", bnetAuthController.callbackHandler, bnetAuthController.updateProfile);

// @route   POST /api/auth/refresh_token
// @desc    Refresh current session
router.post("/refresh_token", SessionsController.refreshToken);

// @route   GET /api/auth/snapchat
// @desc    Load Consent Screen
router.get("/snapchat", snapchatAuthController.getConsent);

// @route   GET /api/auth/snapchat/callback
// @desc    Snapchat will redirect to this URL
router.get("/snapchat/callback", snapchatAuthController.callbackHandler, snapchatAuthController.updateProfile);

// @route   GET /api/auth/google
// @desc    Load consent Screen for youtube auth
router.get("/google", googleAuthController.getConsent);

// @route   GET /api/auth/youtube/callback
// @desc    Youtube will redirect to this URL
router.get("/youtube/callback", googleAuthController.callbackHandler, googleAuthController.updateProfile);

// @route   GET /api/auth/twitch/consent
// @desc    Load twitch consent
router.get("/twitch/consent", twitchAuthController.getConsent);

// @route   GET /api/auth/twitch/callback
// @desc    Twitch will redirect to this URL
router.get("/twitch/callback", twitchAuthController.callbackHandler, twitchAuthController.updateProfile);

// @route   GET /api/auth/statespace
// @desc    Load Consent Screen
router.get("/statespace", stateSpaceAuthController.getConsent);

// @route   GET /api/auth/statespace/callback
// @desc    State Space will redirect to this URL
router.get("/statespace/callback", stateSpaceAuthController.callbackHandler, stateSpaceAuthController.updateProfile);

// @route POST /api/auth/refresh_auth0_token
// @desc  Refresh Auth0 Access Token
router.post("/refresh_auth0_token", auth0Controller.refreshAuth0Token);

// @route   GET /api/auth/riot
// @desc    Load Consent Screen
router.get("/riot", riotAuthController.getConsent);

// @route   GET /api/auth/riot/callback
// @desc    Riot will redirect to this URL
router.get("/riot/callback", riotAuthController.callbackHandler, riotAuthController.updateProfile);

// @route   POST /api/auth/v2/login
// @desc    Login using magic link
router.post("/v2/login", sessionController.login);

module.exports = router;
