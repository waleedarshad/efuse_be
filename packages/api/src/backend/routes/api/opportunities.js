const express = require("express");

const router = express.Router();

const OpportunitiesControllerJS = require("../../controllers/legacy-opportunities.controller");
const { OpportunityUploader } = require("../../uploaders/opportunity.uploader");
const mapAsNull = require("../../middlewares/map-as-null");
const buildPagerVars = require("../../middlewares/build-pager-vars");

const { OpportunitiesController } = require("../../controllers/opportunities.controller");
const opportunitiesController = new OpportunitiesController();

// @route   POST /api/opportunities
// desc     For Creating an Opportunity
router.post("", OpportunityUploader.upload, mapAsNull, OpportunitiesControllerJS.create);

// @route   GET /api/opportunities
// desc     Get all opportunities
router.get("", buildPagerVars, OpportunitiesControllerJS.index);

// @route   GET /api/opportunities/all
// desc     Get all opportunitie
router.get("/all", OpportunitiesControllerJS.getAll);

// @route   POST /api/opportunities/search
// desc     Search using query
router.post("/search", OpportunitiesControllerJS.search);

// @route   GET /api/opportunities/show/:id
// desc     GET Opportunity By ID
router.get("/show/:id", OpportunitiesControllerJS.show);

// @route   GET /api/opportunities/byslug/:id
// desc     GET Opportunity By slug
router.get("/byslug/:slug", OpportunitiesControllerJS.showBySlug);

// @route   GET /api/opportunities/get_max_money_included
// desc     GET Opportunity with Maximum Money included
router.get("/get_max_money_included", OpportunitiesControllerJS.getMaxMoneyIncluded);

// @route   GET /api/opportunities/edit/:id
// @desc    GET Opportunity By Id and boolean whether user canEdit or Not
router.get("/edit/:id", OpportunitiesControllerJS.getOpportunityToEdit);

// @route   PUT /api/opportunities/update/:id
// @desc    PUT Update an opportunity and boolean whether user canEdit or Not
router.put("/update/:id", OpportunityUploader.upload, mapAsNull, OpportunitiesControllerJS.update);

// @route   GET /api/opportunities/owned
// @desc    Get opportunities of the current user
router.get("/owned", buildPagerVars, OpportunitiesControllerJS.owned);

// @route   GET /api/opportunities/applied
// @desc    Get opportunities of the current user
router.get("/applied", buildPagerVars, OpportunitiesControllerJS.applied);

// @route   DELETE api/opportunities/:id
// @desc    deletes a record
router.delete("/:id", OpportunitiesControllerJS.deleteItem);

// @route   GET api/opportunities/alias/:name
// @desc    returns the id for an opportunity alias
router.get("/alias/:name", OpportunitiesControllerJS.alias);

// @route  PUT /api/opportunities/update_status/:id {status: value}
router.put("/update_status/:id", OpportunitiesControllerJS.statusUpdate);

// @route GET /api/opportunities/promoted
// @desc  returns a list of promoted organization's opportunities ordered by date asc
router.get("/promoted", OpportunitiesControllerJS.promoted);

// @route   GET /api/opportunities/recent
// @desc    Get recent opportunities 10 from each category
router.get("/recent", OpportunitiesControllerJS.recentOpportunities);

// @route   POST  /api/opportunities/validate_short_name
// @desc    Validate format & uniqueness of short name
router.post("/validate_short_name", OpportunitiesControllerJS.validateShortName);

// @route   PATCH /api/opportunities/:opportunityId/boost
// @desc    Toggle boost for a particular opportunity
router.patch("/:opportunityId/boost", opportunitiesController.toggleBoost);

module.exports = router;
