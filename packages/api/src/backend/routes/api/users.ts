import express from "express";
import { ILogger, Logger } from "@efuse/logger";

import { ProfileUploader } from "../../uploaders/profile.uploader";
import UsersControllerJS from "../../controllers/legacy-users.controller";
import OpportunitiesController from "../../controllers/legacy-opportunities.controller";
import UserVerificationsController from "../../controllers/user-verifications.controller";
import { ExperienceController } from "../../controllers/experiences.controller";
import { UserExperienceController } from "../../controllers/user-experience.controller";

import { RateLimitMiddleware } from "../../middlewares/rate-limit.middleware";

import { UserSettingRoutes } from "./users/user-settings";

import { UsersController } from "../../controllers/users.controller";
import { SearchController } from "../../controllers/search.controller";

import { trackEvent } from "../../middlewares/track-event";
import { EventTypes } from "../../../lib/efuse-segment-analytics/event-types";

export class UserRoutes {
  private $logger: ILogger;

  private $usersController: UsersController;
  private $searchController: SearchController;
  private $userSettingRoutes: UserSettingRoutes;
  private $experienceController: ExperienceController;
  private $userExperienceController: UserExperienceController;

  public router: express.Router;

  constructor() {
    this.$logger = Logger.create({ name: "user.routes" });

    this.$usersController = new UsersController();
    this.$searchController = new SearchController();
    this.$userSettingRoutes = new UserSettingRoutes();
    this.$experienceController = new ExperienceController();
    this.$userExperienceController = new UserExperienceController();

    this.router = express.Router();

    // @desc    user-settings routes
    this.router.use("/settings", this.$userSettingRoutes.router);

    // @ROUTE   PATCH /api/users/set_locale/:id
    // @DESC    Updates the locale
    this.router.patch("/set_locale/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController
        .setLocale(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurded while getting current user organizations")
        );
    });

    // @route   GET /api/users/organizations
    // @desc    Get current user's organizations
    this.router.get("/organizations", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController
        .getCurrentUserOrganizations(req, res, next)
        .catch((error: unknown) =>
          this.$logger.error({ error }, "an error occurded while getting current user organizations")
        );
    });

    // @route   GET /api/users/current_user
    // @desc    Get current user from token
    this.router.get("/current_user", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.getCurrentUser(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while getting current user");
      });
    });

    // @route   PATCH /api/users/current_user
    // @desc    Update current user
    this.router.patch(
      "/current_user",
      ProfileUploader.upload,
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.updateCurrentUser(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while getting user by Id");
        });
      }
    );

    // @route   GET /api/users/show/:id
    // @desc    Get user by id
    this.router.get("/show/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.getUserById(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while getting user by Id");
      });
    });

    // @route   GET /api/users/username/:username
    // @desc    Get user by username
    this.router.get(
      "/username/:username",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.getUserByUsername(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while getting user by username");
        });
      }
    );

    // @route   GET /api/users/discord_auth
    // @desc    Authentication for discord
    this.router.patch("/discord_auth", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.discordAuth(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while Authentication for discord");
      });
    });

    // @route   GET /api/users/facebook_auth
    // @desc    Authentication for discord
    this.router.patch("/facebook_auth", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.facebookAuth(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while Authentication for facebook");
      });
    });

    // @route   GET /api/users/twitch_auth
    // @desc    Authentication for Twitch
    this.router.patch("/twitch_auth", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.twitchAuth(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while Authentication for facebook");
      });
    });

    // @route   GET /api/users/search
    // @desc    Search users
    // Deprecated - Not removing at the moment to support older mobile app versions
    this.router.get("/search", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$searchController.globalUserSearch(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while Search users");
      });
    });

    // @route   GET /api/users/opportunities
    // @desc    Get opportunities of a user
    this.router.get("/opportunities", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      OpportunitiesController.userOpportunities(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while Getting opportunities");
      });
    });

    // @route   POST /api/users/request_verification
    // @desc    Creating account verification request of current user
    this.router.post(
      "/request_verification",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        UserVerificationsController.verificationRequest(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while Creating account verification request of current user");
        });
      }
    );
    // @route GET /api/users/get_youtube_stream/:userId
    // @desc    Get youtube stream
    this.router.get(
      "/get_youtube_stream/:userId",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.getYoutubeStream(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while getting youtube stream");
        });
      }
    );

    //  @route  PUT /api/users/block/:id
    //  @desc   PUT block user will eventually be creating a list of blocked users and ufollow that user to remove seeing feed of that user and remove from
    this.router.put("/block/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.blockUser(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while blocking user");
      });
    });
    // @route PUT /api/users/unblock/:id
    // @desc unblock user
    this.router.put("/unblock/:id", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.unblockUser(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while unblocking user");
      });
    });
    // @route GET /api/users/blocked
    // @desc retrieve blocked users for currently authed user
    this.router.get("/blocked", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.blockedUsers(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while retrieve blocked users");
      });
    });

    // @route PUT /api/users/unlink_external_accounts
    // @desc unlink an account {service: "steam"}
    this.router.put(
      "/unlink_external_account",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.unLinkExternalAccounts(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while unlinking an account");
        });
      }
    );
    // @route   GET /api/users/twitter_auth
    // @desc    twitter authentication
    this.router.patch("/twitter_auth", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      UsersControllerJS.authenticateTwitter(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while twitter authentication");
      });
    });
    // @route  PUT /api/users/unlink_game_account
    // @desc unlink a game account like fortnite etc {game: "fortnite"}
    this.router.put(
      "/unlink_game_account",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.unlinkGameAccount(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while unlink a game account");
        });
      }
    );
    this.router.put("/remove_pic", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.removePic(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while unlink a game account");
      });
    });

    // @route GET /api/users/alias
    // @desc  Get id for a given user slug/alias
    this.router.get("/alias/:name", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.alias(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while getting id for a given user");
      });
    });

    // @route  PUT /api/users/discord_server
    // @desc add a discord server id to the user
    this.router.put("/discord_server", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.discordServer(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while adding a discord server");
      });
    });

    // @route  PUT /api/users/remove_discord_server
    // @desc remove the discord server for being linked
    this.router.put(
      "/remove_discord_server",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.removeDiscordServer(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while remove the discord server");
        });
      }
    );

    // @route  POST /api/users/discord_webhook
    // @desc add a new webhook to the user's discord webhooks list
    this.router.post("/discord_webhook", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.addDiscordWebhook(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while adding a new webhook");
      });
    });

    // @route  DELETE /api/users/discord_webhook
    // @desc remove a webhook from the user's discord webhooks list
    this.router.delete(
      "/discord_webhook/:id",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.removeDiscordWebhook(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while removing webhook");
        });
      }
    );

    // @route  PUT /api/users/traits
    // @desc create or replace all traits on user
    this.router.put("/traits", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      UsersControllerJS.traits(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating or replace all traits");
      });
    });

    // @route /api/users/logout
    // logout user
    this.router.post("/logout", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.logout(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while logging out user");
      });
    });

    // @route PUT /api/users/update_component_layout
    // @desc Update the component Layout
    this.router.put(
      "/update_component_layout",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.updateComponentLayout(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while Updating the component Layout");
        });
      }
    );

    // @route POST /api/users/experience
    // @desc create experience
    this.router.post("/experience", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$experienceController.create(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating experience");
      });
    });
    // @route GET /api/users/experiences
    // @desc get experiences
    this.router.get("/experiences", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$experienceController.index(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while getting experiences");
      });
    });
    // @route POST /api/users/user_experience
    // @desc create user experience
    this.router.post("/user_experience", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$userExperienceController.create(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating experience");
      });
    });
    // @route POST /api/users/user_experience
    // @desc create all user experiences
    this.router.post(
      "/all_user_experiences",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$userExperienceController.createAllExperiences(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );
    // @route GET /api/users/get_user_experience
    // @desc get user experience
    this.router.get(
      "/get_user_experience",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$userExperienceController.index(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );
    // @route PUT /api/users/increment_views
    // @desc increment view counter
    this.router.put("/increment_views", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$userExperienceController.incrementViews(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route GET /api/users/payments
    // @desc Get all credit / debit cards from a user's stripe customer
    this.router.get("/payments", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.getPayments(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route GET /api/users/payments/setup_intent
    // @desc Get a setup intent for use on the FE stripe.js
    this.router.get(
      "/payments/setup_intent",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.getSetupIntent(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );

    // @route DELETE /api/users/payments
    // @desc Remove a single credit / debit card from a user's stripe customer
    this.router.delete(
      "/payments/:paymentMethodId",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.detachPaymentMethod(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );

    // @route POST /api/users/payments
    // @desc Add a single credit / debit card to a user's stripe customer
    this.router.post("/payments", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.createPayment(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route GET /api/users/orders
    // @desc Get all orders connected to the user
    this.router.get("/orders", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.getAllOrders(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route   POST /api/users/game_platforms
    // @desc    Add a Game platform
    this.router.post("/game_platforms", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.addGamePlatform(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route   DELETE /api/users/game_platforms/:platformId
    // @desc    Delete a particular game platform
    this.router.delete(
      "/game_platforms/:platformId",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.deleteGamePlatform(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );

    // @route   PATCH /api/users/xbox_auth
    // @desc    Verify & Update xbox_auth
    this.router.patch("/xbox_auth", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      UsersControllerJS.xboxAuth(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route   PUT /api/users/generate_alias
    // @desc    Generate alias for the user
    this.router.put("/generate_alias", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.enableAlias(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route   PATCH /api/users/public_traits_motivations
    // @desc    Toggle publicTraitsMotivations boolean
    this.router.patch(
      "/public_traits_motivations",
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.togglePublicTraitsMotivations(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );

    // @route   PATCH /api/users/pathways/select
    // @desc    Select a particular pathway
    this.router.patch("/pathways/select", (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$usersController.selectPathway(req, res, next).catch((error: unknown) => {
        this.$logger.error({ error }, "an error occured while creating user experience");
      });
    });

    // @route   GET /api/users/:idOrUsername/portfolio
    // @desc    Get user by idOrUsername for portfolio
    this.router.get(
      "/:idOrUsername/portfolio",
      trackEvent(EventTypes.PORTFOLIO_VIEW, "idOrUsername"),
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.getProfileByIdOrUsername(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );

    /**
     * @swagger
     * /api/users/delete_account:
     *  post:
     *    tags:
     *      - User API
     *    description: Delete current user's account
     *    requestBody:
     *      $ref: '#/components/requestBodies/deleteAccountBody'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/deleteAccountResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.post(
      "/delete_account",
      RateLimitMiddleware.rateLimitDeleteAccount,
      (req: express.Request, res: express.Response, next: express.NextFunction) => {
        this.$usersController.deleteAccount(req, res, next).catch((error: unknown) => {
          this.$logger.error({ error }, "an error occured while creating user experience");
        });
      }
    );
  }
}
