const express = require("express");

const router = express.Router();

const PubgController = require("../../controllers/pubg.controller");

// @route   /api/pubg
// @desc    Add pubg platform

router.patch("", PubgController.addAccount);

// @route   DELETE  /api/pubg/:platformId
// @desc    Remove platform and its stats

router.delete("/:platformId", PubgController.removeAccount);

module.exports = router;
