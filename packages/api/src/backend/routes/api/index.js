const cors = require("cors");

const { SearchPublicRoutes } = require("./public/search-public.routes");
const { MediaRoutes } = require("./media.routes");
const { GetStreamWebhookRoutes } = require("./__webhook__/get-stream");
const { GetStreamMessageWebhookRoutes } = require("./__webhook__/get-stream-messages");
const { HasuraGraphqlWebhookRoutes } = require("./__webhook__/graphql");
const { CloudflareStreamWebhookRoutes } = require("./__webhook__/cf-stream-transcode");
const { BrazeWebhookRoutes } = require("./__webhook__/braze-webhook");
const { LandingPageRoutes } = require("./public/landing-page.routes");
const { S3Middleware } = require("../../middlewares/s3.middleware");
const { ExternalAccountAuthRoutes } = require("./external-account-auth.routes");
const { ErenaV2Routes } = require("./erena-v2");
const { ErenaV2PublicRoutes } = require("./public/erena-v2-public");
const { HypeRoutes } = require("./hypes");

const applicantRoutes = require("./applicants");
const authRoutes = require("./auth");
const commentRoutes = require("./comments");
const eGencyRoutes = require("./eGency");
const facebookRoutes = require("./facebook");
const { FeatureFlagRoutes } = require("./featureFlags");
const feedRoutes = require("./feeds");
const followersRoutes = require("./followers");
const labsRoutes = require("./labs");
const leaderboardRoutes = require("./leaderboards");
const learningArticlesRoutes = require("./learningArticles");
const linkClicksRoutes = require("./link-clicks");
const liveStreamRoutes = require("./livestream");
const lolRoutes = require("./league-of-legends");
const messagesRoutes = require("./messages");
const notificationRoutes = require("./notifications");
const onboardingRoutes = require("./onboarding");
const opportunityRequirements = require("./opportunity-requirements");
const opportuntiyRoutes = require("./opportunities");
const organizationInvitationRoutes = require("./organizationInvitations");
const organizationRequestsRoutes = require("./organizationRequests");
const organizationRoutes = require("./organizations");
const portfolioRoutes = require("./portfolio");
const promocodesRoutes = require("./promocodes");
const protectedRoute = require("../../middlewares/protected-route");
const pubgRoutes = require("./pubg");
const publicFollowersRoutes = require("./public/followers");
const publicInviteCodeRoutes = require("./public/inviteCodes");
const publicLearningArticlesRoutes = require("./public/learningArticles");
const publicMobileVersionRoutes = require("./public/mobileVersions");
const publicOpportunitiesRoutes = require("./public/opportunities");
const publicOrganizationRoutes = require("./public/organizations");
const publicStatusRoute = require("./public/status");
const publicGitSHARoute = require("./public/gitsha");
const publicUserRoutes = require("./public/users");
const recommendationsRoutes = require("./recommendations");
const recruitmentProfilesRoutes = require("./recruitmentProfile");
const searchRoutes = require("./search");
const { Segment } = require("./__webhook__/segment");
const sitemapRoutes = require("./public/sitemap");
const steamRoutes = require("./steam");
const stripeRoutes = require("./stripe");
const subscriptionsRoutes = require("./subscriptions");
const tournamentRoutes = require("./tournament");
const twitchRoutes = require("./twitch");
const { UserRoutes } = require("./users");
const webhooksRoutes = require("./public/webhooks");
const talentCardRoutes = require("./talent-cards");
const publicRecommendationRoutes = require("./public/recommendations");
const pathwayRoutes = require("./pathways");
const transcodeMediaRoutes = require("./transcode-media");
const overwatchRoutes = require("./overwatch");

const { YoutubeAccountInfoRoutes } = require("./youtube-account-info.routes");
const { GameRoutes } = require("./game.routes");
const { LeaderboardRoutes } = require("./public/leaderboard.routes");
const { TwitterAccountInfoRoutes } = require("./db/twitter-account-info.routes");
const { GoogleAccountInfoRoutes } = require("./google-account-info.routes");
const { TwitchAccountInfoRoutes } = require("./db/twitch-account-info.routes");
const { PathwaysPublicRoutes } = require("./public/pathways-public.routes");
const { RetoolRoutes } = require("./public/retool.routes");

// External API
const { validateApiKey } = require("../../middlewares/validate-extapi-key");
const { apiRateLimit } = require("../../middlewares/ratelimit-extapi");
const {
  DiscordAnnouncementRoutes,
  DiscordStatsRoutes,
  DiscordOpportunityRoutes,
  DiscordPipelineRoutes
} = require("./external");

const erenaV2PublicRoutes = new ErenaV2PublicRoutes();
const erenaV2Routes = new ErenaV2Routes();
const userRoutes = new UserRoutes();
const twitterAccountInfoRoutes = new TwitterAccountInfoRoutes();
const googleAccountInfoRoutes = new GoogleAccountInfoRoutes();
const youtubeAccountInfoRoutes = new YoutubeAccountInfoRoutes();
const twitchAccountInfoRoutes = new TwitchAccountInfoRoutes();
const getStreamWebhookRoutes = new GetStreamWebhookRoutes();
const getStreamMessageWebhookRoutes = new GetStreamMessageWebhookRoutes();
const hasuraGraphqlWebhookRoutes = new HasuraGraphqlWebhookRoutes();
const cloudflareStreamWebhookRoutes = new CloudflareStreamWebhookRoutes();
const brazeWebhookRoutes = new BrazeWebhookRoutes();
const landingPageRoutes = new LandingPageRoutes();
const publicLeaderboardRoutes = new LeaderboardRoutes();
const externalAccountAuthRoutes = new ExternalAccountAuthRoutes();
const searchPublicRoutes = new SearchPublicRoutes();
const mediaRoutes = new MediaRoutes();
const pathwaysPublicRoutes = new PathwaysPublicRoutes();
const gameRoutes = new GameRoutes();
const hypesRoutes = new HypeRoutes();
const retoolRoutes = new RetoolRoutes();
const discordAnnouncementRoutes = new DiscordAnnouncementRoutes();
const discordStatRoutes = new DiscordStatsRoutes();
const discordOpportunityRoutes = new DiscordOpportunityRoutes();
const featureFlagRoutes = new FeatureFlagRoutes();
const discordPipelineRoutes = new DiscordPipelineRoutes();
const segmentRoutes = new Segment();

module.exports = (app) => {
  app.use("/api/applicants", protectedRoute, applicantRoutes);
  app.use("/api/auth", authRoutes);
  app.use("/api/external_auth", protectedRoute, externalAccountAuthRoutes.router);
  app.use("/api/feature_flags", featureFlagRoutes.router);
  app.use("/api/comments", protectedRoute, commentRoutes);
  app.use("/api/facebook", protectedRoute, facebookRoutes);
  app.use("/api/feeds", protectedRoute, feedRoutes);
  app.use("/api/followers", protectedRoute, followersRoutes);
  app.use("/api/media", protectedRoute, mediaRoutes.router);
  app.use("/api/messages", protectedRoute, messagesRoutes);
  app.use("/api/notifications", protectedRoute, notificationRoutes);
  app.use("/api/onboarding", onboardingRoutes);
  app.use("/api/opportunities", protectedRoute, opportuntiyRoutes);
  app.use("/api/public/opportunities", publicOpportunitiesRoutes);
  app.use("/api/organizations", protectedRoute, organizationRoutes);
  app.use("/api/public/organizations", publicOrganizationRoutes);
  app.use("/api/public/users", publicUserRoutes);
  app.use("/api/public/followers", publicFollowersRoutes);
  app.use("/api/public/learning", publicLearningArticlesRoutes);
  app.use("/api/public/webhooks", webhooksRoutes);
  app.use("/api/__status__", publicStatusRoute);
  app.use("/api/__gitsha__", publicGitSHARoute);
  app.use("/api/labs", protectedRoute, labsRoutes);
  app.use("/api/users", protectedRoute, userRoutes.router);
  app.use("/api/steam", protectedRoute, steamRoutes);
  app.use("/api/twitch", protectedRoute, twitchRoutes);
  app.use("/api/portfolio", protectedRoute, portfolioRoutes);
  app.use("/api/leagueoflegends", protectedRoute, lolRoutes);
  app.use("/api/recommendations", protectedRoute, recommendationsRoutes);
  app.use("/api/mobile_versions", publicMobileVersionRoutes);
  app.use("/api/learning", protectedRoute, learningArticlesRoutes);
  app.use("/api/pubg", protectedRoute, pubgRoutes);
  app.use("/api/search", protectedRoute, searchRoutes);
  app.use("/api/public/invite_code", publicInviteCodeRoutes);
  app.use("/api/promocode", promocodesRoutes);
  app.use("/api/subscription", protectedRoute, subscriptionsRoutes);
  app.use("/api/sitemap", sitemapRoutes);
  app.use("/api/link_click", linkClicksRoutes);
  app.use("/api/stripe", protectedRoute, stripeRoutes);
  app.use("/api/egencies", protectedRoute, eGencyRoutes);
  app.use("/api/opportunity_requirements", protectedRoute, opportunityRequirements);
  app.use("/api/leaderboards", protectedRoute, leaderboardRoutes);
  app.use("/api/youtube-account-info", protectedRoute, youtubeAccountInfoRoutes.router);

  app.use("/api/__webhook__/segment", segmentRoutes.router);
  app.use("/api/__webhook__/streamchat", getStreamWebhookRoutes.router);
  app.use("/api/__webhook__/streamchat_message", getStreamMessageWebhookRoutes.router);
  app.use("/api/__webhook__/graphql", protectedRoute, hasuraGraphqlWebhookRoutes.router);
  app.use("/api/__webhook__/cf-stream-transcode", cloudflareStreamWebhookRoutes.router);
  app.use("/api/__webhook__/braze-webhook", brazeWebhookRoutes.router);

  app.use("/api/livestream", protectedRoute, liveStreamRoutes);

  app.use("/api/erena/v2/public", cors({ origin: "*" }), erenaV2PublicRoutes.router);
  app.use("/api/erena/v2", protectedRoute, erenaV2Routes.router);

  // Updating cors to support twitch plugin for tournaments
  // TODO: [spavel] Remove these public routes once we've switched to erena/v2
  app.use("/api/tournament", cors({ origin: "*" }), tournamentRoutes);
  app.use("/api/erena", protectedRoute, tournamentRoutes);

  app.use("/api/organization_requests", protectedRoute, organizationRequestsRoutes);
  app.use("/api/organization_invitations", protectedRoute, organizationInvitationRoutes);
  app.use("/api/uploads/s3", protectedRoute, S3Middleware.router);
  app.use("/api/hypes", protectedRoute, hypesRoutes.router);
  app.use("/api/recruitment_profile", protectedRoute, recruitmentProfilesRoutes);
  app.use("/api/talent_cards", protectedRoute, talentCardRoutes);
  app.use("/api/public/recommendations", publicRecommendationRoutes);
  app.use("/api/pathways", protectedRoute, pathwayRoutes);
  app.use("/api/__transcoding__", transcodeMediaRoutes);
  app.use("/api/overwatch", protectedRoute, overwatchRoutes);
  app.use("/api/public/leaderboard", publicLeaderboardRoutes.router);
  app.use("/api/public/landing_pages", landingPageRoutes.router);
  app.use("/api/public/search", searchPublicRoutes.router);
  app.use("/api/public/pathways", pathwaysPublicRoutes.router);
  app.use("/api/games", protectedRoute, gameRoutes.router);

  // entity specific
  app.use("/api/twitter-account-info", protectedRoute, twitterAccountInfoRoutes.router);
  app.use("/api/google-account-info", protectedRoute, googleAccountInfoRoutes.router);
  app.use("/api/twitch-account-info", protectedRoute, twitchAccountInfoRoutes.router);

  // retool
  // this is secured through passport on the routes
  app.use("/api/retool/", retoolRoutes.router);

  // external api
  app.use("/api/external/announcements", apiRateLimit, validateApiKey, discordAnnouncementRoutes.router);
  app.use("/api/external/discordstats", apiRateLimit, validateApiKey, discordStatRoutes.router);
  app.use(`/api/external/opportunities`, apiRateLimit, validateApiKey, discordOpportunityRoutes.router);
  app.use(`/api/external/pipeline`, apiRateLimit, validateApiKey, discordPipelineRoutes.router);
};
