const express = require("express");

const router = express.Router();

const { OpportunityRequirementsController } = require("../../controllers/opportunity-requirements.controller");

const opportunityRequirementsController = new OpportunityRequirementsController();

// @route   GET /api/opportunity_requirements
// @desc    Get all active opportunity requirements
router.get("", opportunityRequirementsController.getAll);

module.exports = router;
