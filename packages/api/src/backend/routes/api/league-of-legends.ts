import { Router } from "express";

import { LeagueOfLegendsController } from "../../controllers/league-of-legends.controller";

const router = Router();
const leagueOfLegendsController = new LeagueOfLegendsController();

// @route  POST /api/leagueoflegends/updatesummoner/:riotNickname/:riotRegion
// @desc   Update the summoner for the given account
router.post("/updatesummoner/:riotNickname/:riotRegion", leagueOfLegendsController.updateSummoner);

// @route  PUT /api/leagueoflegends/verify
// @desc   Verify a league of legends summoner account.  Uses User.leagueOfLegends.verificationToken
router.put("/verify", leagueOfLegendsController.verifySummoner);

// @route GET /api/leagueoflegends/summonerstats/:riotNickname/:riotRegion
// @desc  Retrieve League of Legends stats for the specified summoner
router.get("/summonerstats/:riotNickname/:riotRegion", leagueOfLegendsController.getSummonerStats);

// @route GET /api/leagueoflegends
// @desc  Update stats for the current authed user
router.get("/", leagueOfLegendsController.get);

module.exports = router;
