import { ILogger, Logger } from "@efuse/logger";
import express from "express";

import { TwitchAccountInfoController } from "../../../controllers/twitch-account-info.controller";

export class TwitchAccountInfoRoutes {
  private $controller: TwitchAccountInfoController;
  private $logger: ILogger = Logger.create({ name: ".routes.game" });

  public router: express.Router;

  constructor() {
    this.$controller = new TwitchAccountInfoController();

    this.router = express.Router();

    /**
     * @swagger
     * /api/twitch-account-info/owner/{id}:
     *  get:
     *    tags:
     *      - External Account Info
     *    summary: Get Twitch Account Info by user/org id
     *    description: Get Twitch Account Info by user/org ID
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/TwitchAccountInfoResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`/owner/:id`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .getByOwner(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting account info"));
    });

    /**
     * @swagger
     * /api/twitch-account-info/{id}:
     *  get:
     *    tags:
     *      - External Account Info
     *    summary: Get Twitch Account Info by ID
     *    description: Get Twitch Account Info by ID
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/TwitchAccountInfoResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`/:id`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .get(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting account info"));
    });
  }
}
