import { ILogger, Logger } from "@efuse/logger";
import express from "express";

import { TwitterAccountInfoController } from "../../../controllers/twitter-account-info.controller";

export class TwitterAccountInfoRoutes {
  private $controller: TwitterAccountInfoController;
  private $logger: ILogger = Logger.create({ name: ".routes.game" });

  public router: express.Router;

  constructor() {
    this.$controller = new TwitterAccountInfoController();

    this.router = express.Router();

    /**
     * @swagger
     * /api/twitter-account-info/owner/{id}:
     *  get:
     *    tags:
     *      - External Account Info
     *    summary: Get Twitter Account Info by user/org id
     *    description: Get Twitter Account Info by user/org ID
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/TwitterAccountInfoResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`/owner/:id`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .getByOwner(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting account info"));
    });

    /**
     * @swagger
     * /api/twitter-account-info/{id}:
     *  get:
     *    tags:
     *      - External Account Info
     *    summary: Get Twitter Account Info by ID
     *    description: Get Twitter Account Info by ID
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/TwitterAccountInfoResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`/:id`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .get(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting account info"));
    });
  }
}
