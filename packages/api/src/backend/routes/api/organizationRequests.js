const express = require("express");

const router = express.Router();

const OrganazationRequestsController = require("../../controllers/organization-requests.controller");

// @route POST /api/organization_requests/join
router.post("/join", OrganazationRequestsController.create);

//@route PUT /api/organization_requests/approve/:id
router.put("/approve/:id", OrganazationRequestsController.approve);

//@route PUT /api/organization_requests/reject/:id
router.put("/reject/:id", OrganazationRequestsController.reject);

//@route GET /api/organization_requests/
router.get("", OrganazationRequestsController.index);

module.exports = router;
