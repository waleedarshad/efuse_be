const express = require("express");

const router = express.Router();

const CommentsController = require("../../controllers/comments.controller");

// @route   DELETE /api/comments/:feedId/:commentId/delete
// @desc    Delete a comment
router.delete("/:feedId/:commentId/delete", CommentsController.deleteComment);

// @route   PATCH /api/comments/:commentId/update
// @desc    Update a comment
router.patch("/:commentId/update", CommentsController.updateComment);

module.exports = router;
