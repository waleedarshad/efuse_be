const express = require("express");

const router = express.Router();
const buildPagerVars = require("../../middlewares/build-pager-vars");
const { expressRouteCache300s } = require("../../../lib/redis/helpers");
const { ERenaNamespaces } = require("../../interfaces/erena/erena-enums");

const { getTwitchClips } = require("../../controllers/twitch.controller");

// @route GET /api/twitch/:user_id/clips
// @desc  Get clips of user by id
router.get("/:user_id/clips", buildPagerVars, getTwitchClips);

module.exports = router;
