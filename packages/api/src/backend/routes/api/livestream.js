const express = require("express");

const router = express.Router();

const LiveStreamController = require("../../controllers/livestream.controller");

// @route   GET /api/livestream
// @desc    return active live streams

router.get("", LiveStreamController.getLivestreams);

module.exports = router;
