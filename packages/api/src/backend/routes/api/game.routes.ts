import { ILogger, Logger } from "@efuse/logger";
import express from "express";

import { GameController } from "../../controllers/game.controller";

export class GameRoutes {
  private $controller: GameController;
  private $logger: ILogger = Logger.create({ name: ".routes.game" });
  private $path: string;

  public router: express.Router;

  constructor() {
    this.$controller = new GameController();
    this.$path = "";

    this.router = express.Router();

    /**
     * @swagger
     * /api/games:
     *  get:
     *    tags:
     *      - Game
     *    summary: Game listing
     *    description: Get collection of games
     *    deprecated: true
     *    responses:
     *      200:
     *        $ref: '#/components/responses/GameResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`${this.$path}`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .list(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting games"));
    });

    /**
     * @swagger
     * /api/games/{id}:
     *  get:
     *    tags:
     *      - Game
     *    summary: Get Game by ID
     *    description: Get Game by ID
     *    deprecated: true
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/GameResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`${this.$path}/:id`, (req: express.Request, res: express.Response, next: express.NextFunction) => {
      this.$controller
        .get(req, res, next)
        .catch((error: unknown) => this.$logger.error({ error }, "an error occurred while getting game"));
    });
  }
}
