const express = require("express");

const router = express.Router();

const ApplicantsController = require("../../controllers/applicants.controller");
const buildPagerVars = require("../../middlewares/build-pager-vars");

// @route   POST /api/applicants/opportunityId
// @desc    Apply against and opportunity

router.post("/:opportunityId", ApplicantsController.create);

// @route   GET /api/applicants/opportunityId
// @desc    Get all applicants for an opportunity

router.get("/:opportunityId", buildPagerVars, ApplicantsController.index);

// @route   PATCH /api/applicants/:opportunityId/:applicantId/status
// @desc    Update application status

router.patch("/:opportunityId/:applicantId/status", ApplicantsController.updateStatus);

// @route   GET /api/applicants/:opportunityId/validate_requirements
// @desc    Validate opportunity requirements
router.get("/:opportunityId/validate_requirements", ApplicantsController.validateRequirements);

module.exports = router;
