const express = require("express");
const { LabsController } = require("../../controllers/labs.controller");

class LabsRoutes {
  constructor() {
    this.controller = new LabsController();
    this.path = "/features";
    this.router = express.Router();

    /**
     * @swagger
     * /api/labs/{id}:
     *  delete:
     *    tags:
     *      - eFuse Labs
     *    summary: Delete resource
     *    description: Delete a particular eFuse labs object
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/LabsResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.delete(`${this.path}/:id`, this.controller.delete);

    /**
     * @swagger
     * /api/labs/{id}:
     *  get:
     *    tags:
     *      - eFuse Labs
     *    summary: Get resource
     *    description: Get a particular eFuse labs object
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/LabsResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.get(`${this.path}/:id`, this.controller.get);

    /**
     * @swagger
     * /api/labs:
     *  get:
     *    tags:
     *      - eFuse Labs
     *    summary: Get all resources
     *    description: Get all eFuse lab objects
     *    responses:
     *      200:
     *        $ref: '#/components/responses/LabsResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     */
    this.router.get(`${this.path}`, this.controller.list);

    /**
     * @swagger
     * /api/labs/{id}:
     *  patch:
     *    tags:
     *      - eFuse Labs
     *    summary: Update a particular resource
     *    description: Update a particular eFuse labs object
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    responses:
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      501:
     *        $ref: '#/components/responses/NotImplemented'
     */
    this.router.patch(`${this.path}/:id`, this.controller.patch);

    /**
     * @swagger
     * /api/labs:
     *  post:
     *    tags:
     *      - eFuse Labs
     *    summary: Create a new resource
     *    description: Create eFuse labs object
     *    requestBody:
     *      $ref: '#/components/requestBodies/LabsFeature'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/LabsResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      500:
     *        $ref: '#/components/responses/InternalServerError'
     */
    this.router.post(`${this.path}`, this.controller.post);

    /**
     * @swagger
     * /api/labs/{id}:
     *  put:
     *    tags:
     *      - eFuse Labs
     *    summary: Update a particular resource
     *    description: Update a particular eFuse labs object
     *    parameters:
     *    - $ref: '#/components/parameters/id'
     *    requestBody:
     *      $ref: '#/components/requestBodies/LabsFeature'
     *    responses:
     *      200:
     *        $ref: '#/components/responses/LabsResponse'
     *      400:
     *        $ref: '#/components/responses/BadRequest'
     *      401:
     *        $ref: '#/components/responses/Unauthorized'
     *      422:
     *        $ref: '#/components/responses/UnprocessableEntity'
     */
    this.router.put(`${this.path}/:id`, this.controller.put);
  }
}

const labsRoutes = new LabsRoutes();
module.exports = labsRoutes.router;
