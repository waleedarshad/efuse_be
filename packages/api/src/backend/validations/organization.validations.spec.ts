import { OrganizationValidations } from "./organization.validations";
import { OrganizationValidationErrorsEnum } from "@efuse/entities";

describe("Organization.validations", () => {
  const data = {
    name: "name",
    scale: "22",
    organizationType: "good",
    status: "open",
    description: "abc",
    phoneNumber: "12345",
    verified: {
      status: true
    }
  };

  describe("validateName", () => {
    it("returns the correct error when name is not provided", () => {
      const expected = { errors: { name: OrganizationValidationErrorsEnum.NAME }, isValid: false };

      const actual = OrganizationValidations.validate({ ...data, name: "" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateScale", () => {
    it("returns the correct error when scale is not provided", () => {
      const expected = { errors: { scale: OrganizationValidationErrorsEnum.SCALE }, isValid: false };

      const actual = OrganizationValidations.validate({ ...data, scale: "" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateOrganizationType", () => {
    it("returns the correct error when organizationType is not provided", () => {
      const expected = {
        errors: { organizationType: OrganizationValidationErrorsEnum.ORGANIZATION_TYPE },
        isValid: false
      };

      const actual = OrganizationValidations.validate({ ...data, organizationType: "" });

      expect(actual).toEqual(expected);
    });
  });
  describe("validateStatus", () => {
    it("returns the correct error when status is not provided", () => {
      const expected = { errors: { status: OrganizationValidationErrorsEnum.STATUS }, isValid: false };

      const actual = OrganizationValidations.validate({ ...data, status: "" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateDescription", () => {
    it("returns the correct error when description is not provided", () => {
      const expected = { errors: { description: OrganizationValidationErrorsEnum.DESCRIPTION }, isValid: false };

      const actual = OrganizationValidations.validate({ ...data, description: "" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validatePhoneNumber", () => {
    it("returns the correct error when phoneNumber is not provided", () => {
      const expected = { errors: { phoneNumber: OrganizationValidationErrorsEnum.PHONE_NUMBER }, isValid: false };

      const actual = OrganizationValidations.validate({ ...data, phoneNumber: "" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateAllParams", () => {
    it("returns no error when correct params are provided", () => {
      const expected = { errors: {}, isValid: true };

      const actual = OrganizationValidations.validate(data);

      expect(actual).toEqual(expected);
    });
  });
});
