const _ = require("lodash");

const validateExperience = (data) => {
  const errors = {};

  // Name
  if (_.isEmpty(data.name)) {
    errors.name = "Name is required";
  }
  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};

module.exports.validateExperience = validateExperience;
