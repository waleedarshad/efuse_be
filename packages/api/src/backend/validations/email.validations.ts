import { Trace } from "@efuse/decorators";
import { EmailValidationErrorEnum } from "@efuse/entities";
import { isEmpty } from "lodash";
import isEmail from "validator/lib/isEmail";

import { BaseService } from "../../lib/base";
import { Service } from "../../lib/decorators";
import { EmailEnum } from "../../lib/users/user.enum";
import { IEmailValidationErrors, IValidateEmailDeprecatedResponse, IValidationResponse } from "../interfaces";
import { User } from "../models/User";

@Service("email.validations")
export class EmailValidationService extends BaseService {
  /**
   * @deprecated In favor of UserValidationService#validateEmail
   * @param email Email of the user to be validated.
   * @param userId User of the user whose email is to be validated.
   * @returns Object containing email validation errors if any.
   */
  @Trace()
  public static validateEmailDeprecated = async (
    email: string,
    userId = ""
  ): Promise<IValidateEmailDeprecatedResponse> => {
    const errors: IEmailValidationErrors = {};

    email = email.toLowerCase().trim();

    let invalidFormat = false;

    if (isEmpty(email)) {
      errors.email = EmailValidationErrorEnum.REQUIRED;
    } else if (!isEmail(email)) {
      errors.email = EmailValidationErrorEnum.INVALID;
      invalidFormat = true;
    } else {
      const user = await User.findOneWithDeleted({ email }, { email: 1 }).lean();

      // signupbot@efuse.gg is the dedicated email for Cross Browser Testing automated tests
      if (user && user._id.toString() !== userId.toString() && email !== EmailEnum.SIGNUP_BOT) {
        errors.email = EmailValidationErrorEnum.TAKEN;
      }
    }

    return {
      errors,
      isValid: isEmpty(errors),
      invalidFormat
    };
  };

  /**
   * @deprecated In favor of UserValidationService#validateEmail
   * @param email Email of the user to be validated.
   * @param userId User of the user whose email is to be validated.
   * @returns Object containing email validation errors if any.
   */
  @Trace()
  public static validateEmail = async (email: string, userId = ""): Promise<IValidationResponse> => {
    email = email.toLowerCase().trim();

    let error = "";
    let valid = true;

    if (isEmpty(email)) {
      error = EmailValidationErrorEnum.REQUIRED;
      valid = false;
    } else if (!isEmail(email)) {
      error = EmailValidationErrorEnum.INVALID;
      valid = false;
    } else {
      const user = await User.findOneWithDeleted({ email }, { email: 1 }).lean();
      // signupbot@efuse.gg is the dedicated email for Cross Browser Testing automated tests
      if (user && user._id.toString() !== userId.toString() && email !== EmailEnum.SIGNUP_BOT) {
        error = EmailValidationErrorEnum.TAKEN;
        valid = false;
      }
    }

    return { valid, error };
  };
}
