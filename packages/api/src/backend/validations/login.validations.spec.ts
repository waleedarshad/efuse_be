import { LoginValidations } from "./login.validations";

describe("learning-articles.validations", () => {
  describe("validateLogin", () => {
    it("returns the correct error when both email and password are not provided", () => {
      const expected = { errors: { email: "Email is required", password: "Password is required" }, isValid: false };

      const actual = LoginValidations.validate({ email: "", password: "" });

      expect(actual).toEqual(expected);
    });

    it("returns the correct error when email is not provided", () => {
      const expected = { errors: { email: "Email is required" }, isValid: false };

      const actual = LoginValidations.validate({ email: "", password: "asdwqca123a" });

      expect(actual).toEqual(expected);
    });

    it("returns the correct error when password is not provided", () => {
      const expected = { errors: { password: "Password is required" }, isValid: false };

      const actual = LoginValidations.validate({ email: "email.com", password: "" });

      expect(actual).toEqual(expected);
    });
    it("returns no error when email and password is provided", () => {
      const expected = { errors: {}, isValid: true };

      const actual = LoginValidations.validate({ email: "email.com", password: "password" });

      expect(actual).toEqual(expected);
    });
  });
});
