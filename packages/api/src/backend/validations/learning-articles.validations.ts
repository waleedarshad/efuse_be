import { isEmpty } from "lodash";

interface ParamsType {
  category: string;
  slug: string;
  author: string;
  authorType: string;
  title: string;
}

interface Errors {
  error?: string;
}

interface ValidationReturnType {
  errors: Errors;
  isValid: boolean;
}

export class LearningArticlesValidations {
  public static validate = (params: ParamsType): ValidationReturnType => {
    const errors: Errors = {};

    const { category, slug, title, author, authorType } = params;

    // Category validations
    if (isEmpty(category)) {
      errors.error = "Category is required";
    } else if (category.length !== 24) {
      errors.error = "Invalid category";
    }

    //Slug validations
    if (isEmpty(slug)) {
      errors.error = "Slug is required";
    } else if (slug.length < 3 || slug.length > 24) {
      errors.error = "Slug should not be less than 3 or more than 24 chars";
    }

    // Title validations
    if (isEmpty(title)) {
      errors.error = "Title is required";
    }

    // Author Validations
    if (isEmpty(author)) {
      errors.error = "Author is required";
    }

    if (!["users", "organizations"].includes(authorType)) {
      errors.error = "Invalid author type";
    }

    return {
      errors,
      isValid: isEmpty(errors)
    };
  };
}
