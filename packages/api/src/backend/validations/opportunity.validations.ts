import { isEmpty } from "lodash";

import { shortNameFormatError } from "../../lib/opportunities";

interface DataType {
  opportunityType?: string;
  subType?: string;
  title?: string;
  description?: string;
  dueDate?: string;
  startDate?: string;
  minimumAge?: string;
  organization?: string;
  onBehalfOfOrganization?: string;
  self?: string;
  shortName?: any;
}

interface ValidationReturnType {
  errors: DataType;
  isValid: boolean;
}

export class OpportunityValidations {
  public static validate = (data: DataType): ValidationReturnType => {
    const errors: DataType = {};

    if (isEmpty(data.opportunityType)) {
      errors.opportunityType = "opportunityType is required";
    }

    if (isEmpty(data.subType)) {
      errors.subType = "subType is required";
    }

    if (isEmpty(data.title)) {
      errors.title = "title is required";
    }

    if (isEmpty(data.description)) {
      errors.description = "description is required";
    }

    if (data["applicationDueDate?"] === "true") {
      if (data.dueDate === "null") {
        errors.dueDate = "dueDate is required";
      }
    }

    if (data["applicationStartDate?"] === "true") {
      if (data.startDate === "null") {
        errors.startDate = "startDate is required";
      }
    }

    if (data["minimumAgeRequired?"] === "true") {
      if (isEmpty(data.minimumAge)) {
        errors.minimumAge = "minimumAge is required";
      }
    }

    if (isEmpty(data.onBehalfOfOrganization)) {
      errors.onBehalfOfOrganization = "onBehalfOfOrganization is required";
    } else if (data.onBehalfOfOrganization === "true") {
      if (isEmpty(data.organization)) {
        errors.organization = "organization is required";
      }
    } else if (data.onBehalfOfOrganization === "false") {
      if (isEmpty(data.self)) {
        errors.self = "self is required";
      }
    }

    const shortNameValidationResult = shortNameFormatError(data.shortName);
    if (!shortNameValidationResult.isValid) {
      errors.shortName = shortNameValidationResult.error;
    }

    return {
      errors,
      isValid: isEmpty(errors)
    };
  };
}
