import { isEmpty } from "lodash";
import { OrganizationValidationErrorsEnum } from "@efuse/entities";
import { IOrganization, IOrganizationValidationError } from "../../backend/interfaces";
import { IInputValidationResponse } from "../../backend/interfaces/validations/validation";

export class OrganizationValidations {
  public static validate = (data: Partial<IOrganization>): IInputValidationResponse<IOrganizationValidationError> => {
    const errors: IOrganizationValidationError = {};
    if (isEmpty(data.name)) {
      errors.name = OrganizationValidationErrorsEnum.NAME;
    }

    if (isEmpty(data.scale)) {
      errors.scale = OrganizationValidationErrorsEnum.SCALE;
    }

    if (isEmpty(data.organizationType)) {
      errors.organizationType = OrganizationValidationErrorsEnum.ORGANIZATION_TYPE;
    }

    if (isEmpty(data.status)) {
      errors.status = OrganizationValidationErrorsEnum.STATUS;
    }

    if (isEmpty(data.description)) {
      errors.description = OrganizationValidationErrorsEnum.DESCRIPTION;
    }

    if (!isEmpty(data.verified) && isEmpty(data.phoneNumber)) {
      errors.phoneNumber = OrganizationValidationErrorsEnum.PHONE_NUMBER;
    }

    return {
      errors,
      isValid: isEmpty(errors)
    };
  };
}
