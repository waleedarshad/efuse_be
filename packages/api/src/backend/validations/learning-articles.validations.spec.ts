import { LearningArticlesValidations } from "./learning-articles.validations";

describe("learning-articles.validations", () => {
  const data = {
    author: "123",
    authorType: "users",
    category: "60b931347519bd0012fdf870",
    title: "abcd",
    slug: "abc"
  };

  describe("validateCategory", () => {
    it("returns the correct error when category is not provided", () => {
      const expected = { errors: { error: "Category is required" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, category: "" });

      expect(actual).toEqual(expected);
    });

    it("returns the correct error when invalid category is provided", () => {
      const expected = { errors: { error: "Invalid category" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, category: "asdwqca123a" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateSlug", () => {
    it("returns the correct error when slug is not provided", () => {
      const expected = { errors: { error: "Slug is required" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, slug: "" });

      expect(actual).toEqual(expected);
    });

    it("returns the correct error when invalid slug is provided", () => {
      const expected = { errors: { error: "Slug should not be less than 3 or more than 24 chars" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, slug: "a" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateTitle", () => {
    it("returns the correct error when title is not provided", () => {
      const expected = { errors: { error: "Title is required" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, title: "" });

      expect(actual).toEqual(expected);
    });
  });
  describe("validateAuthor", () => {
    it("returns the correct error when author is not provided", () => {
      const expected = { errors: { error: "Author is required" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, author: "" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateAuthorType", () => {
    it("returns the correct error when invalid author type is provided", () => {
      const expected = { errors: { error: "Invalid author type" }, isValid: false };

      const actual = LearningArticlesValidations.validate({ ...data, authorType: "eFuse" });

      expect(actual).toEqual(expected);
    });
  });

  describe("validateAllParams", () => {
    it("returns no error when correct params are provided", () => {
      const expected = { errors: {}, isValid: true };

      const actual = LearningArticlesValidations.validate(data);

      expect(actual).toEqual(expected);
    });
  });
});
