import { Trace } from "@efuse/decorators";
import { UsernameValidationErrorEnum } from "@efuse/entities";
import { isEmpty } from "lodash";

import { BaseService } from "../../lib/base";
import { Service } from "../../lib/decorators";
import { BAD_WORDS } from "../data/bad-words";
import { TOP_FOLLOWERS } from "../data/top-followers";
import { IUser } from "../interfaces";
import {
  IValidateUsernameDeprecatedResponse,
  IUsernameError,
  IValidationResponse
} from "../interfaces/validations/validation";
import { User } from "../models/User";

@Service("username.validations")
export class UsernameValidationsService extends BaseService {
  /**
   * Validates the username of the user.
   * @deprecated In favor of UserValidationService#validateUsername
   * @param username username to validate.
   * @param userId Id of the user whose username is to be validated.
   * @returns object containing validation errors if any.
   */

  @Trace()
  public static validateUsernameDeprecated = async (
    username: string,
    userId = ""
  ): Promise<IValidateUsernameDeprecatedResponse> => {
    const errors: IUsernameError = {};
    const usernameRegex = /^[\w-]*$/;

    let invalidUsername = false;

    if (isEmpty(username)) {
      errors.username = UsernameValidationErrorEnum.REQUIRED;
    } else if (!usernameRegex.test(username)) {
      errors.username = UsernameValidationErrorEnum.ONLY_ALPHA_NUMERIC;
    } else if (username.length < 4) {
      errors.username = UsernameValidationErrorEnum.ATLEAST_4_CHARACTERS;
    } else if (TOP_FOLLOWERS.includes(username)) {
      errors.username = UsernameValidationErrorEnum.TAKEN;
      invalidUsername = true;
    } else if (BAD_WORDS.includes(username.toLowerCase())) {
      errors.username = UsernameValidationErrorEnum.SEXUAL_OR_OFFENSIVE;
    } else {
      const user: IUser = await User.findOneWithDeleted({ username }, { username: 1 }).lean();

      // Check whether a user with this username exists and that it isn't the current user
      // signupbot is the dedicated username for Cross Browser Testing automated tests
      if (user && user._id.toString() !== userId.toString() && username !== "signupbot") {
        errors.username = UsernameValidationErrorEnum.TAKEN;
      }
    }

    return {
      errors,
      isValid: isEmpty(errors),
      invalidUsername
    };
  };

  /**
   * @deprecated In favor of UserValidationService#validateUsername
   * @params @username Id of the user whose username is to be validated.
   * @params @userId object containing validation errors if any.
   * @returns object containing validation errors if any.
   */
  @Trace()
  public static validateUsername = async (username: string, userId = ""): Promise<IValidationResponse> => {
    username = username.trim();

    let error = "";
    let valid = true;

    const usernameRegex = /^[\w-]*$/;

    if (isEmpty(username)) {
      error = UsernameValidationErrorEnum.REQUIRED;
      valid = false;
    } else if (!usernameRegex.test(username)) {
      error = UsernameValidationErrorEnum.INVALID_CHARACTERS;
      valid = false;
    } else if (username.length < 4) {
      error = UsernameValidationErrorEnum.ATLEAST_4_CHARACTERS;
      valid = false;
    } else if (TOP_FOLLOWERS.includes(username)) {
      error = UsernameValidationErrorEnum.TAKEN;
      valid = false;
    } else if (BAD_WORDS.includes(username.toLowerCase())) {
      error = UsernameValidationErrorEnum.SEXUAL_OR_OFFENSIVE;
      valid = false;
    } else {
      const user: IUser = await User.findOneWithDeleted({ username }, { username: 1 }).lean();

      // Check whether a user with this username exists and that it isn't the current user
      // signupbot is the dedicated username for Cross Browser Testing automated tests
      if (user && user._id.toString() !== userId.toString() && username !== "signupbot") {
        error = UsernameValidationErrorEnum.TAKEN;
        valid = false;
      }
    }

    return { valid, error };
  };
}
