import { isEmpty } from "lodash";

interface DataType {
  email: string;
  password: string;
}

interface Errors {
  email?: string;
  password?: string;
}

interface ValidationReturnType {
  errors: Errors;
  isValid: boolean;
}

export class LoginValidations {
  public static validate = (data: DataType): ValidationReturnType => {
    const errors: Errors = {};
    const email = isEmpty(data.email) ? "" : data.email;
    const password = isEmpty(data.password) ? "" : data.password;

    // Email Validation

    if (isEmpty(email)) {
      errors.email = "Email is required";
    }

    // Password Validation

    if (isEmpty(password)) {
      errors.password = "Password is required";
    }

    // Result

    return {
      errors,
      isValid: isEmpty(errors)
    };
  };
}
