const _ = require("lodash");
const { ERenaTournament } = require("../models/erena");
const { GeneralHelper } = require("../helpers/general.helper");
const { OpportunityService } = require("../../lib/opportunities/opportunity.service");

const opportunityService = new OpportunityService();

/**
 * @deprecated Use the `validateTournament` method on the ERenaTournamentService class
 * @method module:Validations.validateTournament
 * @summary Validate if the tournament has the required fields needed to create
 * @example
 * const tournamentParams = req.body;
 * validateTournament(tournamentParams);
 * @returns {Promise<any>} (errors, isValid)
 * @param tournament
 */
const validateTournament = async (tournament) => {
  const errors = [];

  if (_.isEmpty(tournament.slug)) errors.push("Tournament slug is required.");

  if (_.isEmpty(tournament.tournamentId)) errors.push("Tournament id is required.");

  if (_.isEmpty(tournament.tournamentName)) errors.push("Tournament name is required.");

  if (_.isEmpty(tournament.tournamentDescription)) errors.push("Tournament description is required.");

  if (_.isEmpty(tournament.startDatetime)) errors.push("Tournament start date and time are required.");

  const slugCount = await ERenaTournament.find({ slug: tournament.slug });
  if (slugCount.length > 0) errors.push("Slug already taken.");

  const idCount = await ERenaTournament.find({
    tournamentId: tournament.tournamentId
  });
  if (idCount.length > 0) errors.push("Tournament id already taken.");

  // Validate Opportunity param
  if (!_.isEmpty(tournament.opportunity)) {
    // Make sure opportunity ID is valid
    if (!GeneralHelper.isValidObjectId(tournament.opportunity)) {
      errors.push("Invalid opportunity ID");
    } else {
      // Make sure opportunity exists in our DB
      const opportunity = await opportunityService.findOne({
        _id: tournament.opportunity
      });

      if (!opportunity) {
        errors.push("Opportunity not found");
      } else if (opportunity.publishStatus !== "open" || opportunity.status !== "visible") {
        errors.push("Make sure opportunity is open & visible");
      }
    }
  }

  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};

const validateTeam = (team) => {
  const errors = {};

  if (_.isEmpty(team.name)) {
    errors.name = "Name is required";
  }

  return {
    errors,
    isValid: _.isEmpty(errors)
  };
};

module.exports.validateTeam = validateTeam;
module.exports.validateTournament = validateTournament;
