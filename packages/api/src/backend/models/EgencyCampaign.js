const mongoose = require("mongoose");

const { timestamps } = require("../helpers/timestamp.helper");

// ref models
require("./User").User;
require("./EgencyBrand");

const { Schema } = mongoose;
const eGencyCampaignSchema = new Schema({
  creator: { type: Schema.Types.ObjectId, ref: "users" },
  brand: { type: Schema.Types.ObjectId, ref: "egencybrands" },
  title: { type: String },
  description: { type: String },
  startDate: { type: Date },
  endDate: { type: Date },
  objective: {
    type: String,
    enum: ["promote_org", "link_clicks", "post_engagements"]
  },
  linkUrl: { type: String },
  platforms: { type: String, enum: ["fb", "twitch", "instagram"] },
  budget: {
    amount: { type: Number },
    currency: { type: String },
    unit: { type: String, enum: ["day", "week", "month"] }
  },
  paymentStatus: { type: String, enum: ["pending", "success", "failed"] }
});

eGencyCampaignSchema.plugin(timestamps);

const EgencyCampaign = mongoose.model("egencycampaigns", eGencyCampaignSchema);

module.exports.EgencyCampaign = EgencyCampaign;
