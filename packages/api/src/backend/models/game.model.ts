import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";

import { File } from "./file";
import { timestamps } from "../helpers/timestamp.helper";

@gqlObjectType("Game", { description: "The Game Model" })
@plugin(timestamps)
export class Game {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId; // Added it for graphQL, otherwise typegoose automatically generates & handles _id

  @gqlField((_type) => String)
  @prop({ type: () => String })
  description?: string;

  @gqlField((_type) => String)
  @prop({ type: () => String, required: true, unique: true })
  slug!: string;

  @gqlField((_type) => File)
  @prop({ _id: false, required: true })
  gameImage!: File;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  kind?: string;

  @gqlField((_type) => String)
  @prop({ type: () => String, required: true })
  title!: string;

  @gqlField({ nullable: true })
  createdAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL

  @gqlField({ nullable: true })
  updatedAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL
}

const gameSchema = buildSchema(Game);

/**
 * To support legacy models we need to generate schema & add model to Typegoose ideally we would `getModelForClass` method
 * If we don't generate the model like below, then our existing legacy schemas would not find this collection registered &
 * we would not be able to reference it in legacy schemas.
 */
export const GameModel = addModelToTypegoose(model("games", gameSchema), Game);
