const mongoose = require("mongoose");
const autopopulate = require("mongoose-autopopulate");
const mongooseDelete = require("mongoose-delete");
const aggregatePaginate = require("mongoose-aggregate-paginate-v2");

const { timestamps } = require("../helpers/timestamp.helper");
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");
const WhitelistHelper = require("../helpers/whitelist_helper");

const { Schema } = mongoose;

const homeFeedSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate
  },
  feed: {
    type: Schema.Types.ObjectId,
    ref: "feeds",
    required: true,
    autopopulate: true
  },
  timelineable: {
    type: Schema.Types.ObjectId,
    refPath: "timelineableType",
    required: true,
    autopopulate: true
  },
  timelineableType: {
    type: String,
    enum: ["users", "organizations"],
    default: "users"
  },
  original: { type: Boolean, default: false }
});

homeFeedSchema.index({ user: 1, feed: 1, timelineable: 1, timelineableType: 1, deleted: 1, original: 1 });

homeFeedSchema.plugin(aggregatePaginate);
homeFeedSchema.plugin(mongoosePaginate);
// todo: investigate
// @ts-ignore
homeFeedSchema.plugin(autopopulate);
homeFeedSchema.plugin(mongooseDelete, {
  indexFields: "all",
  deletedAt: true,
  overrideMethods: ["count", "find", "findOne", "findOneAndUpdate", "updateMany"]
});
homeFeedSchema.plugin(timestamps);

const HomeFeed = mongoose.model("homeFeeds", homeFeedSchema);

module.exports.HomeFeed = HomeFeed;
