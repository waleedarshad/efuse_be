const mongoose = require("mongoose");

const { timestamps } = require("../helpers/timestamp.helper");
const SchemaHelper = require("../helpers/schema_helper");

// ref model
require("./User").User;

const { Schema } = mongoose;
const eGencySchema = new Schema({
  name: { type: String },
  logo: SchemaHelper.fileSchema,
  creator: { type: Schema.Types.ObjectId, ref: "users" }
});

eGencySchema.plugin(timestamps);

const EgencyBrand = mongoose.model("egencybrands", eGencySchema);

module.exports.EgencyBrand = EgencyBrand;
