import { model } from "mongoose";
import { prop, addModelToTypegoose, plugin, Ref, buildSchema } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { timestamps } from "../../helpers/timestamp.helper";
import { User } from "../User";
import { IUser } from "../../interfaces/user";

@gqlObjectType("XboxLiveAuth", { description: "The model of type xboxLive" })
@plugin(timestamps)
export class XboxLiveAuth {
  @prop({ ref: User, required: true, unique: true })
  user: Ref<IUser>;

  @gqlField((_type) => String)
  @prop({ type: String })
  scope!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  accessToken!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  refreshToken!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  authenticationToken!: string;
}

const XboxLiveAuthSchema = buildSchema(XboxLiveAuth);

export const XboxLiveAuthModel = addModelToTypegoose(model("xboxliveauths", XboxLiveAuthSchema), XboxLiveAuth);
