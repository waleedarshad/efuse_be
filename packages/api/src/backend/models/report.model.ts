import { model } from "mongoose";
import { prop, addModelToTypegoose, Ref, plugin, buildSchema } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID, registerEnumType } from "type-graphql";

import { IUser } from "../interfaces";
import { User } from "./User";
import { ReportEnum } from "@efuse/entities";
import { timestamps } from "../helpers/timestamp.helper";
import { ObjectId } from "../../backend/types";
import { IFeed, IComment } from "../../backend/interfaces";

@gqlObjectType("Report", { description: "The model of report type" })
@plugin(timestamps)
export class Report {
  @gqlField((_type) => ID)
  readonly _id!: ObjectId;

  @prop({ ref: User, required: true, unique: true, index: true })
  user!: Ref<IUser>;

  @gqlField((_type) => String)
  @prop({ required: true, refPath: "reportkind", index: true })
  reportItem!: Ref<IFeed> | Ref<IComment>;

  @gqlField((_type) => ReportEnum)
  @prop({ type: String, required: true, enum: ReportEnum })
  reportkind!: ReportEnum;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  reportReason!: string;

  @gqlField({ nullable: true })
  createdAt?: Date;

  @gqlField({ nullable: true })
  updatedAt?: Date;
}

registerEnumType(ReportEnum, {
  name: "ReportEnum"
});
const reportSchema = buildSchema(Report);

export const ReportModel = addModelToTypegoose(model("reports", reportSchema), Report);
