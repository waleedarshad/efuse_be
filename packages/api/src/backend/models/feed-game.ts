import { Schema, Document, PaginateModel, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IFeedGame } from "../interfaces/feed/feed-game";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";

const feedGameSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "games", index: true },
  feed: { type: Schema.Types.ObjectId, ref: "feeds", index: true }
});

feedGameSchema.index({ game: 1, feed: 1 });

feedGameSchema.plugin(timestamps);
feedGameSchema.plugin(mongoosePaginate);

export type FeedGameModel<T extends Document> = PaginateModel<T>;
export const FeedGame: FeedGameModel<IFeedGame> = <FeedGameModel<IFeedGame>>model("feedgames", feedGameSchema);
