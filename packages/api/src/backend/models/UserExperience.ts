import * as mongoose from "mongoose";
import { timestamps } from "../helpers/timestamp.helper";
import { IUserExperience } from "../interfaces/user-experience";

const { Schema } = mongoose;

const schema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", required: true },
  experience: {
    type: Schema.Types.ObjectId,
    ref: "experiences",
    required: true
  },
  metrics: {
    globalViews: { type: Number },
    globalSkips: { type: Number },
    subViews: [
      {
        name: { type: String },
        views: { type: Number },
        skips: { type: Number }
      }
    ]
  }
});
schema.plugin(timestamps);

schema.index({
  user: 1,
  experience: 1
});

export type UserExperienceModel<T extends mongoose.Document> = mongoose.Model<T>;

export const UserExperience: UserExperienceModel<IUserExperience> = <UserExperienceModel<IUserExperience>>(
  mongoose.model<IUserExperience>("userexperiences", schema)
);
