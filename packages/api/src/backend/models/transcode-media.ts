import { Schema, Model, model, Document } from "mongoose";
import { timestamps } from "../helpers/timestamp.helper";
import { ITranscodeMedia } from "../interfaces";

const transcodeMediaSchema = new Schema({
  jobId: { type: String, index: true },
  originalMedia: { type: Schema.Types.ObjectId, ref: "media", index: true },
  transcodedMedia: [{ type: Schema.Types.ObjectId, ref: "media" }],
  hlsMedia: { type: Schema.Types.ObjectId, ref: "media" },
  processed: { type: Boolean, default: false, index: true },
  status: { type: String, enum: ["COMPLETE", "ERROR", "PROGRESSING"], index: true },
  error: { code: { type: Number }, message: { type: String } }
});

transcodeMediaSchema.index({ originalMedia: 1, status: 1, processed: 1 });

transcodeMediaSchema.plugin(timestamps);

export type TranscodeMediaModel<T extends Document> = Model<T>;
export const TranscodeMedia: TranscodeMediaModel<ITranscodeMedia> = <TranscodeMediaModel<ITranscodeMedia>>(
  model<ITranscodeMedia>("transcodemedia", transcodeMediaSchema)
);
