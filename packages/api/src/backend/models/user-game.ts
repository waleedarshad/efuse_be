import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IUserGame } from "../interfaces";

const userGameSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "games", index: true },
  user: { type: Schema.Types.ObjectId, ref: "users", index: true }
});

userGameSchema.index({ game: 1, user: 1 });

userGameSchema.plugin(timestamps);

export type UserGameModel<T extends Document> = Model<T>;
export const UserGame: UserGameModel<IUserGame> = <UserGameModel<IUserGame>>model("usergames", userGameSchema);
