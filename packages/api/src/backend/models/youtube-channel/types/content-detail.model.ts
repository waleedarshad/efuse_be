import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("Playlist", { description: "related Playlist type" })
class Playlist {
  @gqlField((_type) => String)
  @prop({ type: String })
  likes!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  favorites!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  type!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  uploads!: string;
}

@gqlObjectType("ContentDetail", { description: "youtube count detail type" })
export class ContentDetail {
  @gqlField((_type) => Playlist)
  @prop({ type: Playlist })
  relatedPlaylists!: Playlist;
}
