import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("Default", { description: "A type that describes Thumbnail Default and medium types" })
class Default {
  @gqlField((_type) => String)
  @prop({ type: String })
  url!: string;

  @gqlField((_type) => Number)
  @prop({ type: Number })
  width!: number;

  @gqlField((_type) => Number)
  @prop({ type: Number })
  height!: number;
}

@gqlObjectType("YoutubeChannelThumbnail", { description: "A type that describes Thumbnail types" })
export class YoutubeChannelThumbnail {
  @gqlField((_type) => Default)
  @prop({ type: Default })
  default!: Default;

  @gqlField((_type) => Default)
  @prop({ type: Default })
  medium!: Default;
}
