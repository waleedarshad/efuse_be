import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("Statistics", { description: "youtube statistics type" })
export class Statistics {
  @gqlField((_type) => String)
  @prop({ type: String })
  viewCount!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  subscriberCount!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  hiddenSubscriberCount!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  videoCount!: string;
}
