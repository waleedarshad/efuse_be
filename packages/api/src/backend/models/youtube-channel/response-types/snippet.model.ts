import { prop } from "@typegoose/typegoose";
import { YoutubeChannelThumbnail } from "../types";
import { Localized } from "./localized.model";

export class Snippet {
  @prop({ kind: String })
  title!: string;

  @prop({ kind: String })
  description!: string;

  @prop({ kind: String })
  publishedAt!: string;

  @prop({ kind: YoutubeChannelThumbnail })
  thumbnails!: YoutubeChannelThumbnail;

  @prop({ kind: Localized })
  localized!: Localized;
}
