import { prop } from "@typegoose/typegoose";

export class PageInfo {
  @prop({ type: Number })
  totalResults!: number;

  @prop({ type: Number })
  resultsPerPage!: number;
}
