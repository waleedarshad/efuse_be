import { prop } from "@typegoose/typegoose";

export class Localized {
  @prop({ kind: String })
  title!: string;

  @prop({ kind: String })
  description!: string;
}
