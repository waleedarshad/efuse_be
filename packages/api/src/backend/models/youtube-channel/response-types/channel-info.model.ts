import { prop } from "@typegoose/typegoose";
import { ContentDetail, Statistics } from "../types";
import { Snippet } from "./snippet.model";

export class ChannelInfo {
  @prop({ kind: String })
  kind!: string;

  @prop({ kind: String })
  etag!: string;

  @prop({ kind: String })
  id!: string;

  @prop({ kind: Snippet })
  snippet!: Snippet;

  @prop({ kind: ContentDetail })
  contentDetails!: ContentDetail;

  @prop({ kind: Statistics })
  statistics!: Statistics;
}
