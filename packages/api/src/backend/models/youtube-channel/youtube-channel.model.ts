import { model } from "mongoose";
import { OwnerKind } from "@efuse/entities";
import { addModelToTypegoose, prop, buildSchema, index, plugin } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID, registerEnumType } from "type-graphql";
import { timestamps } from "../../helpers/timestamp.helper";
import { YoutubeChannelThumbnail, ContentDetail, Statistics } from "./types";
import { ObjectId } from "../../types";

@gqlObjectType("YoutubeChannel", { description: "The Youtube channel model" })
@index({ etag: 1, id: 1 })
@plugin(timestamps)
export class YoutubeChannel {
  @gqlField((_type) => ID)
  readonly _id!: ObjectId;

  @gqlField((_type) => String)
  @prop({ type: String })
  owner!: string;

  @gqlField((_type) => OwnerKind)
  @prop({ type: String, enum: OwnerKind })
  ownerKind!: OwnerKind;

  @gqlField((_type) => String)
  @prop({ type: String })
  kind!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  etag!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  id!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  title!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  description!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  publishedAt!: string;

  @gqlField((_type) => YoutubeChannelThumbnail)
  @prop({ type: YoutubeChannelThumbnail, _id: false })
  thumbnail!: YoutubeChannelThumbnail;

  @gqlField((_type) => ContentDetail)
  @prop({ type: ContentDetail, _id: false })
  contentDetails!: ContentDetail;

  @gqlField((_type) => Statistics)
  @prop({ type: Statistics, _id: false })
  statistics!: Statistics;

  @gqlField({ nullable: true })
  createdAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL

  @gqlField({ nullable: true })
  updatedAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL
}

registerEnumType(OwnerKind, {
  name: "OwnerKind"
});

const youtubeChannelSchema = buildSchema(YoutubeChannel);

export const YoutubeChannelModel = addModelToTypegoose(model("youtubechannels", youtubeChannelSchema), YoutubeChannel);
