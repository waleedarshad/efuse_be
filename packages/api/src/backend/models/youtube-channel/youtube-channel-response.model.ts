import { prop } from "@typegoose/typegoose";
import { ChannelInfo, PageInfo } from "./response-types";

export class YoutubeChannelResponse {
  @prop({ kind: String })
  owner!: string;

  @prop({ type: String })
  etag!: string;

  @prop({ type: PageInfo })
  pageInfo!: PageInfo;

  @prop({ type: ChannelInfo })
  items!: ChannelInfo;
}
