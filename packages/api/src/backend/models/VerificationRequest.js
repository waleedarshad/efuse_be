const mongoose = require("mongoose");
const autopopulate = require("mongoose-autopopulate");

const { Schema } = mongoose;
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");

const WhitelistHelper = require("../helpers/whitelist_helper");
const { timestamps } = require("../helpers/timestamp.helper");

const requestSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "users",
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate
  },
  acceptedBy: { type: Schema.Types.ObjectId, ref: "users", autopopulate: true },
  acceptedAt: { type: Date },
  rejectedBy: { type: Schema.Types.ObjectId, ref: "users", autopopulate: true },
  rejectedAt: { type: Date },
  status: {
    type: String,
    enum: ["accepted", "rejected", "pending"],
    default: "pending"
  }
});

// todo: investigate
// @ts-ignore
requestSchema.plugin(autopopulate);
requestSchema.plugin(mongoosePaginate);
requestSchema.plugin(timestamps);

const VerificationRequest = mongoose.model("verificationRequests", requestSchema);

module.exports.VerificationRequest = VerificationRequest;
