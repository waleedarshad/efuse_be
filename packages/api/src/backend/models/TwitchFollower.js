const mongoose = require("mongoose");

const { Schema } = mongoose;
const autopopulate = require("mongoose-autopopulate");
const paginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");
const { timestamps } = require("../helpers/timestamp.helper");

const twitchFollowersSchema = new Schema({
  total: { type: Number },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  data: [
    {
      fromId: { type: Number },
      fromName: { type: String },
      toId: { type: Number },
      toName: { type: String },
      followedAt: { type: Date }
    }
  ]
});

twitchFollowersSchema.plugin(paginate);
// todo: investigate
// @ts-ignore
twitchFollowersSchema.plugin(autopopulate);
twitchFollowersSchema.plugin(timestamps);

const TwitchFollower = mongoose.model("twitchFollowers", twitchFollowersSchema);

module.exports.TwitchFollower = TwitchFollower;
