import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeagueRound } from "../../interfaces";

const schema = new Schema({
  value: {
    type: Number
  },
  name: {
    type: String,
    required: true
  },
  bracket: { type: Schema.Types.ObjectId, ref: "leaguebrackets" },
  startDate: {
    type: Date
  }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeagueRoundModel<T extends Document> = PaginateModel<T>;
export const LeagueRound: LeagueRoundModel<ILeagueRound> = <LeagueRoundModel<ILeagueRound>>(
  model<ILeagueRound>("leaguerounds", schema)
);
