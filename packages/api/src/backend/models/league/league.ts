import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { LeagueEntryType, LeagueState, OwnerType } from "@efuse/entities";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeague } from "../../interfaces";

const schema = new Schema({
  name: { type: String, required: true },
  imageUrl: String,
  description: String,
  owner: { type: Schema.Types.ObjectId, required: true },
  ownerType: { type: String, enum: Object.values(OwnerType), required: true },
  entryType: { type: String, enum: Object.values(LeagueEntryType) },
  game: { type: Schema.Types.ObjectId, ref: "games", required: true },
  teams: { type: [Schema.Types.ObjectId], ref: "teams", required: true },
  state: { type: String, enum: Object.values(LeagueState), required: true },
  rules: { type: String, default: "" }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

schema.index({ owner: 1, ownerType: 1 });

export type LeagueModel<T extends Document> = PaginateModel<T>;
export const League: LeagueModel<ILeague> = <LeagueModel<ILeague>>model<ILeague>("league", schema);
