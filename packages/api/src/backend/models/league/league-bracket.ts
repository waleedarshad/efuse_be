import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { LeagueBracketType } from "@efuse/entities";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeagueBracket } from "../../interfaces";

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  pool: { type: Schema.Types.ObjectId, ref: "leaguepools" },
  type: {
    type: String,
    enum: Object.values(LeagueBracketType)
  }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeagueBracketModel<T extends Document> = PaginateModel<T>;
export const LeagueBracket: LeagueBracketModel<ILeagueBracket> = <LeagueBracketModel<ILeagueBracket>>(
  model<ILeagueBracket>("leaguebrackets", schema)
);
