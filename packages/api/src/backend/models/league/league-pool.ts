import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeaguePool } from "../../interfaces";

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  event: { type: Schema.Types.ObjectId, ref: "leagueevents" },
  teams: { type: [Schema.Types.ObjectId], ref: "teams" }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeaguePoolModel<T extends Document> = PaginateModel<T>;
export const LeaguePool: LeaguePoolModel<ILeaguePool> = <LeaguePoolModel<ILeaguePool>>(
  model<ILeaguePool>("leaguepools", schema)
);
