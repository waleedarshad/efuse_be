import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeagueMatch } from "../../interfaces";
import { LeagueMatchState } from "@efuse/entities";

const schema = new Schema({
  teams: { type: [Schema.Types.ObjectId], ref: "teams" },
  winner: { type: Schema.Types.ObjectId, ref: "teams" },
  value: {
    type: Number
  },
  name: {
    type: String
  },
  round: { type: Schema.Types.ObjectId, ref: "leaguerounds" },
  state: { type: String, enum: Object.values(LeagueMatchState), required: true },
  startTime: {
    type: Date
  },
  endTime: {
    type: Date
  }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeagueMatchModel<T extends Document> = PaginateModel<T>;
export const LeagueMatch: LeagueMatchModel<ILeagueMatch> = <LeagueMatchModel<ILeagueMatch>>(
  model<ILeagueMatch>("leaguematches", schema)
);
