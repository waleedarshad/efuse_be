import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeagueCompetitorScore } from "../../interfaces";
import { LeagueScoreCompetitorType, LeagueScoreCreatorType } from "@efuse/entities";

const schema = new Schema({
  creator: {
    type: Schema.Types.ObjectId,
    required: true
  },
  creatorType: {
    type: String,
    enum: Object.values(LeagueScoreCreatorType),
    required: true
  },
  image: {
    type: String
  },
  value: {
    type: Number
  },
  competitor: {
    type: Schema.Types.ObjectId,
    required: true
  },
  competitorType: {
    type: String,
    enum: Object.values(LeagueScoreCompetitorType),
    required: true
  },
  gameScore: { type: Schema.Types.ObjectId, ref: "leaguegamescores" }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeagueCompetitorScoreModel<T extends Document> = PaginateModel<T>;
export const LeagueCompetitorScore: LeagueCompetitorScoreModel<ILeagueCompetitorScore> = <
  LeagueCompetitorScoreModel<ILeagueCompetitorScore>
>model<ILeagueCompetitorScore>("leaguecompetitorscores", schema);
