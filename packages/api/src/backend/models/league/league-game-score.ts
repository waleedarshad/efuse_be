import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeagueGameScore } from "../../interfaces";
import { LeagueScoreCreatorType, LeagueGameScoreType } from "@efuse/entities";

const schema = new Schema({
  creator: {
    type: Schema.Types.ObjectId,
    required: true
  },
  creatorType: {
    type: String,
    enum: Object.values(LeagueScoreCreatorType),
    required: true
  },
  gameNumber: {
    type: Number
  },
  image: {
    type: String
  },
  match: { type: Schema.Types.ObjectId, ref: "leaguematches" },
  type: {
    type: String,
    enum: Object.values(LeagueGameScoreType)
  }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeagueGameScoreModel<T extends Document> = PaginateModel<T>;
export const LeagueGameScore: LeagueGameScoreModel<ILeagueGameScore> = <LeagueGameScoreModel<ILeagueGameScore>>(
  model<ILeagueGameScore>("leaguegamescores", schema)
);
