import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { LeagueBracketType, LeagueEventState, LeagueEventTimingMode } from "@efuse/entities";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ILeagueEvent } from "../../interfaces";

const schema = new Schema({
  bracketType: {
    type: String,
    enum: Object.values(LeagueBracketType)
  },
  description: {
    type: String
  },
  details: { type: String, default: "" },
  gamesPerMatch: {
    type: Number
  },
  league: {
    type: Schema.Types.ObjectId,
    ref: "leagues"
  },
  maxTeams: {
    type: Number
  },
  name: {
    type: String,
    required: true
  },
  rules: { type: String, default: "" },
  startDate: { type: Date },
  state: { type: String, enum: Object.values(LeagueEventState), default: LeagueEventState.PENDING_TEAMS },
  teams: { type: [Schema.Types.ObjectId], ref: "teams" },
  timingMode: {
    type: String,
    enum: Object.values(LeagueEventTimingMode)
  }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

export type LeagueEventModel<T extends Document> = PaginateModel<T>;
export const LeagueEvent: LeagueEventModel<ILeagueEvent> = <LeagueEventModel<ILeagueEvent>>(
  model<ILeagueEvent>("leagueevents", schema)
);
