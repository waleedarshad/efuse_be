const mongoose = require("mongoose");

const { Schema } = mongoose;
const SchemaHelper = require("../helpers/schema_helper");
const { timestamps } = require("../helpers/timestamp.helper");

const paymentProfileScheme = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  type: { type: String, enum: ["STRIPE", "RECURLY"] },
  customerId: { type: String, required: true, unique: true },
  active: { type: Boolean, default: true }
});

paymentProfileScheme.plugin(timestamps);

const PaymentProfile = mongoose.model("paymentprofiles", paymentProfileScheme);

module.exports.PaymentProfile = PaymentProfile;
