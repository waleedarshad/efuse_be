import { model, Types } from "mongoose";
import { prop, plugin, Ref, buildSchema, addModelToTypegoose, index } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";

import { timestamps } from "../../helpers/timestamp.helper";
import { User } from "../User";
import { IUser } from "../../interfaces";
import { ClippyCommand } from "./clippy-command.model";

@index({ user: 1, token: 1 })
@gqlObjectType("Clippy", { description: "The Clippy Model" })
@plugin(timestamps)
export class Clippy {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId; // Added for graphQL

  @prop({ ref: User, required: true, unique: true })
  user!: Ref<IUser>;

  @gqlField((_type) => String)
  @prop({ type: () => String, required: true, unique: true })
  token!: string;

  @gqlField((_type) => [ClippyCommand])
  commands!: ClippyCommand[];
}

const clippySchema = buildSchema(Clippy);

export const ClippyModel = addModelToTypegoose(model("clippy", clippySchema, "clippy"), Clippy);
