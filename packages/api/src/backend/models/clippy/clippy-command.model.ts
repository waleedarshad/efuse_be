import { model, Types } from "mongoose";
import { prop, plugin, Ref, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID, registerEnumType, Int } from "type-graphql";
import {
  ClippyCommandKindEnum,
  ClippySortOrderEnum,
  ClippyDateRangeEnum,
  ClippyAllowedRolesEnum
} from "@efuse/entities";

import { timestamps } from "../../helpers/timestamp.helper";
import { Clippy } from "./clippy.model";

@gqlObjectType("ClippyCommand", { description: "The Clippy Command Model" })
@plugin(timestamps)
export class ClippyCommand {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId;

  @gqlField((_type) => String)
  @prop({ type: String })
  command!: string;

  @gqlField((_type) => Clippy)
  @prop({ ref: () => Clippy, index: true })
  clippy!: Ref<Clippy>;

  @gqlField((_type) => ClippyCommandKindEnum)
  @prop({ type: String, enum: ClippyCommandKindEnum })
  kind!: ClippyCommandKindEnum;

  @gqlField((_type) => ClippySortOrderEnum, { nullable: true })
  @prop({ type: String, enum: ClippySortOrderEnum })
  sortOrder?: ClippySortOrderEnum;

  @gqlField((_type) => ClippyDateRangeEnum, { nullable: true })
  @prop({ type: String, enum: ClippyDateRangeEnum })
  dateRange?: ClippyDateRangeEnum;

  @gqlField((_type) => [ClippyAllowedRolesEnum], { nullable: true })
  @prop({
    type: String,
    enum: ClippyAllowedRolesEnum,
    default: [ClippyAllowedRolesEnum.BROADCASTER, ClippyAllowedRolesEnum.MODERATOR]
  })
  allowedRoles?: ClippyAllowedRolesEnum[];

  @gqlField((_type) => Boolean)
  @prop({ type: Boolean, default: true })
  active!: boolean;

  @gqlField((_type) => Int, { nullable: true })
  @prop({ type: Number })
  minimumViews?: number;

  @gqlField((_type) => Boolean, { nullable: true })
  @prop({ type: Boolean })
  queueClips?: boolean;
}

const clippyCommandsSchema = buildSchema(ClippyCommand);

export const ClippyCommandModel = addModelToTypegoose(
  model("clippy.commands", clippyCommandsSchema, "clippy.commands"),
  ClippyCommand
);

// Register ClippyCommandKindEnum for GraphQL
registerEnumType(ClippyCommandKindEnum, {
  name: "ClippyCommandKindEnum"
});

// Register ClippySortOrderEnum for GraphQL
registerEnumType(ClippySortOrderEnum, {
  name: "ClippySortOrderEnum"
});

// Register ClippyDateRangeEnum for GraphQL
registerEnumType(ClippyDateRangeEnum, {
  name: "ClippyDateRangeEnum"
});

// Register ClippyAllowedRolesEnum for GraphQL
registerEnumType(ClippyAllowedRolesEnum, {
  name: "ClippyAllowedRolesEnum"
});
