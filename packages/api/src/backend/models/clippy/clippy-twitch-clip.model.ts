import { Field, ObjectType, ID, Int, Float } from "type-graphql";

import { TwitchGame } from "../twitch/twitch-game.model";

@ObjectType("ClippyTwitchClip", { description: "The ClippyTwitchClip Model" })
export class ClippyTwitchClip {
  @Field((_type) => ID)
  id!: string;

  @Field((_type) => String)
  url!: string;

  @Field((_type) => String)
  embed_url!: string;

  @Field((_type) => String)
  broadcaster_id!: string;

  @Field((_type) => String)
  broadcaster_name!: string;

  @Field((_type) => String)
  creator_id!: string;

  @Field((_type) => String)
  creator_name!: string;

  @Field((_type) => String)
  video_id!: string;

  @Field((_type) => String, { nullable: true })
  game_id?: string;

  @Field((_type) => String)
  language!: string;

  @Field((_type) => String)
  title!: string;

  @Field((_type) => Int)
  view_count!: number;

  @Field((_type) => String)
  created_at!: string;

  @Field((_type) => String)
  thumbnail_url!: string;

  @Field((_type) => Float)
  duration!: number;

  @Field((_type) => String, { nullable: true })
  mp4_url(): string | null {
    try {
      return this.thumbnail_url.split("-preview")[0] + ".mp4";
    } catch {
      return null;
    }
  }

  @Field((_type) => TwitchGame, { nullable: true })
  game?: TwitchGame;
}
