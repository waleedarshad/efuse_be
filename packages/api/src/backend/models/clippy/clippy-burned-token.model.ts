import { model } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";

import { timestamps } from "../../helpers/timestamp.helper";

@plugin(timestamps)
export class ClippyBurnedToken {
  @prop({ type: () => String, required: true, unique: true, index: true })
  token!: string;
}

const clippyBurnedToken = buildSchema(ClippyBurnedToken);

export const ClippyBurnedTokenModel = addModelToTypegoose(
  model("clippy.burned.tokens", clippyBurnedToken, "clippy.burned.tokens"),
  ClippyBurnedToken
);
