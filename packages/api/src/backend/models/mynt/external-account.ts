import { Schema } from "mongoose";

export const externalAccountSchema = new Schema({
  address: { type: String },
  config: { type: String },
  profileImgUrl: { type: String },
  user: {
    username: { type: String }
  }
});
