import { Schema } from "mongoose";

export const myntAssetContractSchema = new Schema({
  name: { type: String },
  address: { type: String },
  type: { type: String },
  schemaName: { type: String },
  sellerFeeBasisPoints: { type: Number },
  buyerFeeBasisPoints: { type: Number },
  description: { type: String },
  tokenSymbol: { type: String },
  imageUrl: { type: String },
  stats: { type: Object },
  traitStats: [{ type: Object }],
  externalLink: { type: String },
  wikiLink: { type: String }
});
