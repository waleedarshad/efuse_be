import { Schema, Document, Model, model } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IStagedNFT } from "../../interfaces/mynt/staged/staged-nft";

const schema = new Schema({
  assetFile: { type: String },
  name: { type: String },
  externalLink: { type: String },
  description: { type: String },
  traits: { type: Object },
  unlockableContent: { type: String },
  eFuseUserId: { type: String },
  isListed: { type: Boolean },
  sellPrice: { type: String },
  transactionHistory: [{ type: Object }]
});

schema.plugin(timestamps);

export type StagedNFTModel<T extends Document> = Model<T>;

export const StagedNFT: StagedNFTModel<IStagedNFT> = <StagedNFTModel<IStagedNFT>>(
  model<IStagedNFT>("stagednfts", schema)
);
