import { Schema, Document, Model, model } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IStagedNFTTransaction } from "../../interfaces/mynt/staged/staged-nft-transaction";

const schema = new Schema({
  purchaserId: { type: String },
  sellerId: { type: String },
  nftId: { type: String },
  orderId: { type: String },
  payerId: { type: String },
  purchasePrice: { type: String }
});

schema.plugin(timestamps);

export type StagedNFTTransactionModel<T extends Document> = Model<T>;

export const StagedNFTTransaction: StagedNFTTransactionModel<IStagedNFTTransaction> = <
  StagedNFTTransactionModel<IStagedNFTTransaction>
>model<IStagedNFTTransaction>("stagednfttransactions", schema);
