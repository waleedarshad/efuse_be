import { Document, PaginateModel, Schema, model } from "mongoose";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { externalCollectionSchema } from "./external-collection";
import { myntAssetContractSchema } from "./mynt-asset-contract";
import { externalAccountSchema } from "./external-account";
import { myntTraitsSchema } from "./mynt-traits";
import { myntAssetEventSchema } from "./mynt-asset-event";
import { IMyntAsset } from "../../interfaces/mynt";

const myntAssetSchema = new Schema({
  openSeaId: { type: String, index: true },
  tokenId: { type: String, index: true },
  tokenAddress: { type: String },
  schemaName: { type: String },
  version: { type: String },
  name: { type: String },
  decimals: { type: Number },
  assetContract: myntAssetContractSchema,
  assetCollection: externalCollectionSchema,
  description: { type: String },
  owner: externalAccountSchema,
  isPresale: { type: Boolean },
  imageUrl: { type: String },
  imagePreviewUrl: { type: String },
  imageUrlOriginal: { type: String },
  imageUrlThumbnail: { type: String },
  animationUrl: { type: String },
  animationOriginalUrl: { type: String },
  openSeaLink: { type: String },
  externalLink: { type: String },
  traits: myntTraitsSchema,
  numSales: { type: Number },
  lastSale: myntAssetEventSchema,
  backgroundColor: { type: String },
  transferFee: { type: String },
  creator: externalAccountSchema
});

myntAssetSchema.plugin(timestamps);
myntAssetSchema.plugin(mongoosePaginate);

export type MyntAssetModel<T extends Document> = PaginateModel<T>;

export const MyntAsset: MyntAssetModel<IMyntAsset> = <MyntAssetModel<IMyntAsset>>(
  model<IMyntAsset>("myntAssets", myntAssetSchema)
);
