import { Schema } from "mongoose";

export const myntTraitsSchema = new Schema({
  rarity: { type: String },
  game: { type: String },
  owner: { type: String },
  mediaType: { type: String }
});
