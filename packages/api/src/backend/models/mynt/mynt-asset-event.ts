import { Schema } from "mongoose";
import { externalAccountSchema } from "./external-account";

export const myntAssetEventSchema = new Schema({
  eventType: { type: String },
  eventTimestamp: { type: Date },
  auctionType: { type: String },
  totalPrice: { type: String },
  transaction: {
    fromAccount: externalAccountSchema,
    toAccount: externalAccountSchema,
    createdDate: { type: Date },
    modifiedDate: { type: Date },
    transactionHash: { type: String },
    transactionIndex: { type: String },
    blockNumber: { type: String },
    blockHash: { type: String },
    timestamp: { type: Date }
  },
  paymentToken: { type: Object }
});
