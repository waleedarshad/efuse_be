import { Schema } from "mongoose";

export const externalCollectionSchema = new Schema({
  name: { type: String },
  slug: { type: String },
  editors: [{ type: String }],
  hidden: { type: Boolean },
  featured: { type: Boolean },
  createdDate: { type: Date },
  description: { type: String },
  imageUrl: { type: String },
  largeImageUrl: { type: String },
  featuredImageUrl: { type: String },
  stats: { type: Object },
  displayData: { type: Object },
  payoutAddress: { type: String },
  traitStats: { type: Object },
  externalLink: { type: String },
  wikiLink: { type: String }
});
