import { model } from "mongoose";
import { prop, plugin, addModelToTypegoose, buildSchema } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";
import { timestamps } from "../../helpers/timestamp.helper";
import { ObjectId } from "../../../backend/types";
@gqlObjectType("BlockedUser", { description: "The Blocked User Model" })
@plugin(timestamps)
export class BlockedUser {
  @gqlField((_type) => ID)
  readonly _id!: ObjectId;

  @gqlField((_type) => String)
  @prop({ type: () => String, index: true })
  blockerId!: string;

  @gqlField((_type) => String)
  @prop({ type: () => String, index: true })
  blockeeId!: string;
}

const blockedUserSchema = buildSchema(BlockedUser);

export const BlockedUserModel = addModelToTypegoose(model("blocked_users", blockedUserSchema), BlockedUser);
