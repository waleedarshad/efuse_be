import { ID, Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { File } from "../file";
import { ObjectId } from "../../types";
@gqlObjectType("BlockedUserInfo", { description: "BlockedUser info model" })
export class BlockedUserInfo {
  @gqlField((_type) => ID)
  _id!: ObjectId;

  @gqlField((_type) => File)
  profilePicture!: File;

  @gqlField((_type) => String)
  name!: string;

  @gqlField((_type) => String)
  username!: string;
}
