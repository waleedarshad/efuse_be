const mongoose = require("mongoose");

const { Schema } = mongoose;
const SchemaHelper = require("../helpers/schema_helper");
const { timestamps } = require("../helpers/timestamp.helper");

const ordersScheme = new Schema({
  type: {
    type: String,
    required: true,
    enum: ["PAID_OPPORTUNITY"]
  },
  paymentStatus: {
    type: String,
    required: true,
    enum: ["PAID", "REFUNDED", "DECLINED"]
  },
  user: { type: Schema.Types.ObjectId, ref: "users" },
  promocode: { type: Schema.Types.ObjectId, ref: "promocodes" },
  paymentId: { type: String, required: true, unique: true },
  subtotalAmount: { type: Number, required: true },
  discountAmount: { type: Number, required: true },
  refundAmount: { type: Number, required: true },
  taxAmount: { type: Number, required: true }
});

ordersScheme.plugin(timestamps);

const Order = mongoose.model("orders", ordersScheme);

module.exports.Order = Order;
