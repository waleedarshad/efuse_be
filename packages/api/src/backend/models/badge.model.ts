import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";

import { timestamps } from "../helpers/timestamp.helper";

@gqlObjectType("Badge", { description: "The Badge Model" })
@plugin(timestamps)
export class Badge {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId; // Added it for graphQL, otherwise typegoose automatically generates & handles _id

  @gqlField((_type) => String)
  @prop({ type: () => String })
  name!: string;

  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  active!: boolean;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  icon?: string;

  @gqlField((_type) => String)
  @prop({ type: () => String, index: true, unique: true })
  slug!: string;

  @gqlField({ nullable: true })
  createdAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL

  @gqlField({ nullable: true })
  updatedAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL
}

const badgeSchema = buildSchema(Badge);

export const BadgeModel = addModelToTypegoose(model("badges", badgeSchema), Badge);
