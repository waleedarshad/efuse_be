import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { ITrackingEvent } from "../interfaces";

const trackingEventsSchema = new Schema({
  name: { type: String, index: true },
  properties: { type: Schema.Types.Mixed },
  trackedResource: {
    param: { type: String, index: true },
    value: { type: String, index: true }
  },
  identity: { userId: { type: Schema.Types.ObjectId, ref: "users" }, anonymousId: { type: String } },
  context: {
    userAgent: { type: String },
    ip: { type: String },
    location: { city: { type: String }, country: { type: String }, region: { type: String } },
    timezone: { type: String },
    os: { type: String },
    active: { type: Boolean },
    referrer: {
      type: { type: String },
      name: { type: String },
      url: { type: String },
      link: { type: String }
    }
  }
});

trackingEventsSchema.plugin(timestamps);

trackingEventsSchema.index({ name: 1, "trackedResource.param": 1, "trackedResource.value": 1 });

export type TrackingEventModel<T extends Document> = Model<T>;
export const TrackingEvent: TrackingEventModel<ITrackingEvent> = <TrackingEventModel<ITrackingEvent>>(
  model<ITrackingEvent>("trackingevents", trackingEventsSchema)
);
