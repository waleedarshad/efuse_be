const mongoose = require("mongoose");

const { Schema } = mongoose;
const { timestamps } = require("../helpers/timestamp.helper");

const pubgSchema = new Schema({
  type: {
    type: String
  },
  attributes: {
    bestRankPoint: {
      type: Number
    },
    gameModeStats: {
      duo: {
        assists: {
          type: Number
        },
        boosts: {
          type: Number
        },
        dBNOs: {
          type: Number
        },
        dailyKills: {
          type: Number
        },
        dailyWins: {
          type: Number
        },
        damageDealt: {
          type: Number
        },
        days: {
          type: Number
        },
        headshotKills: {
          type: Number
        },
        heals: {
          type: Number
        },
        killPoints: {
          type: Number
        },
        kills: {
          type: Number
        },
        longestKill: {
          type: Number
        },
        longestTimeSurvived: {
          type: Number
        },
        losses: {
          type: Number
        },
        maxKillStreaks: {
          type: Number
        },
        mostSurvivalTime: {
          type: Number
        },
        rankPoints: {
          type: Number
        },
        rankPointsTitle: {
          type: String
        },
        revives: {
          type: Number
        },
        rideDistance: {
          type: Number
        },
        roadKills: {
          type: Number
        },
        roundMostKills: {
          type: Number
        },
        roundsPlayed: {
          type: Number
        },
        suicides: {
          type: Number
        },
        swimDistance: {
          type: Number
        },
        teamKills: {
          type: Number
        },
        timeSurvived: {
          type: Number
        },
        top10s: {
          type: Number
        },
        vehicleDestroys: {
          type: Number
        },
        walkDistance: {
          type: Number
        },
        weaponsAcquired: {
          type: Number
        },
        weeklyKills: {
          type: Number
        },
        weeklyWins: {
          type: Number
        },
        winPoints: {
          type: Number
        },
        wins: {
          type: Number
        }
      },
      "duo-fpp": {
        assists: {
          type: Number
        },
        boosts: {
          type: Number
        },
        dBNOs: {
          type: Number
        },
        dailyKills: {
          type: Number
        },
        dailyWins: {
          type: Number
        },
        damageDealt: {
          type: Number
        },
        days: {
          type: Number
        },
        headshotKills: {
          type: Number
        },
        heals: {
          type: Number
        },
        killPoints: {
          type: Number
        },
        kills: {
          type: Number
        },
        longestKill: {
          type: Number
        },
        longestTimeSurvived: {
          type: Number
        },
        losses: {
          type: Number
        },
        maxKillStreaks: {
          type: Number
        },
        mostSurvivalTime: {
          type: Number
        },
        rankPoints: {
          type: Number
        },
        rankPointsTitle: {
          type: String
        },
        revives: {
          type: Number
        },
        rideDistance: {
          type: Number
        },
        roadKills: {
          type: Number
        },
        roundMostKills: {
          type: Number
        },
        roundsPlayed: {
          type: Number
        },
        suicides: {
          type: Number
        },
        swimDistance: {
          type: Number
        },
        teamKills: {
          type: Number
        },
        timeSurvived: {
          type: Number
        },
        top10s: {
          type: Number
        },
        vehicleDestroys: {
          type: Number
        },
        walkDistance: {
          type: Number
        },
        weaponsAcquired: {
          type: Number
        },
        weeklyKills: {
          type: Number
        },
        weeklyWins: {
          type: Number
        },
        winPoints: {
          type: Number
        },
        wins: {
          type: Number
        }
      },
      solo: {
        assists: {
          type: Number
        },
        boosts: {
          type: Number
        },
        dBNOs: {
          type: Number
        },
        dailyKills: {
          type: Number
        },
        dailyWins: {
          type: Number
        },
        damageDealt: {
          type: Number
        },
        days: {
          type: Number
        },
        headshotKills: {
          type: Number
        },
        heals: {
          type: Number
        },
        killPoints: {
          type: Number
        },
        kills: {
          type: Number
        },
        longestKill: {
          type: Number
        },
        longestTimeSurvived: {
          type: Number
        },
        losses: {
          type: Number
        },
        maxKillStreaks: {
          type: Number
        },
        mostSurvivalTime: {
          type: Number
        },
        rankPoints: {
          type: Number
        },
        rankPointsTitle: {
          type: String
        },
        revives: {
          type: Number
        },
        rideDistance: {
          type: Number
        },
        roadKills: {
          type: Number
        },
        roundMostKills: {
          type: Number
        },
        roundsPlayed: {
          type: Number
        },
        suicides: {
          type: Number
        },
        swimDistance: {
          type: Number
        },
        teamKills: {
          type: Number
        },
        timeSurvived: {
          type: Number
        },
        top10s: {
          type: Number
        },
        vehicleDestroys: {
          type: Number
        },
        walkDistance: {
          type: Number
        },
        weaponsAcquired: {
          type: Number
        },
        weeklyKills: {
          type: Number
        },
        weeklyWins: {
          type: Number
        },
        winPoints: {
          type: Number
        },
        wins: {
          type: Number
        }
      },
      "solo-fpp": {
        assists: {
          type: Number
        },
        boosts: {
          type: Number
        },
        dBNOs: {
          type: Number
        },
        dailyKills: {
          type: Number
        },
        dailyWins: {
          type: Number
        },
        damageDealt: {
          type: Number
        },
        days: {
          type: Number
        },
        headshotKills: {
          type: Number
        },
        heals: {
          type: Number
        },
        killPoints: {
          type: Number
        },
        kills: {
          type: Number
        },
        longestKill: {
          type: Number
        },
        longestTimeSurvived: {
          type: Number
        },
        losses: {
          type: Number
        },
        maxKillStreaks: {
          type: Number
        },
        mostSurvivalTime: {
          type: Number
        },
        rankPoints: {
          type: Number
        },
        rankPointsTitle: {
          type: String
        },
        revives: {
          type: Number
        },
        rideDistance: {
          type: Number
        },
        roadKills: {
          type: Number
        },
        roundMostKills: {
          type: Number
        },
        roundsPlayed: {
          type: Number
        },
        suicides: {
          type: Number
        },
        swimDistance: {
          type: Number
        },
        teamKills: {
          type: Number
        },
        timeSurvived: {
          type: Number
        },
        top10s: {
          type: Number
        },
        vehicleDestroys: {
          type: Number
        },
        walkDistance: {
          type: Number
        },
        weaponsAcquired: {
          type: Number
        },
        weeklyKills: {
          type: Number
        },
        weeklyWins: {
          type: Number
        },
        winPoints: {
          type: Number
        },
        wins: {
          type: Number
        }
      },
      squad: {
        assists: {
          type: Number
        },
        boosts: {
          type: Number
        },
        dBNOs: {
          type: Number
        },
        dailyKills: {
          type: Number
        },
        dailyWins: {
          type: Number
        },
        damageDealt: {
          type: Number
        },
        days: {
          type: Number
        },
        headshotKills: {
          type: Number
        },
        heals: {
          type: Number
        },
        killPoints: {
          type: Number
        },
        kills: {
          type: Number
        },
        longestKill: {
          type: Number
        },
        longestTimeSurvived: {
          type: Number
        },
        losses: {
          type: Number
        },
        maxKillStreaks: {
          type: Number
        },
        mostSurvivalTime: {
          type: Number
        },
        rankPoints: {
          type: Number
        },
        rankPointsTitle: {
          type: String
        },
        revives: {
          type: Number
        },
        rideDistance: {
          type: Number
        },
        roadKills: {
          type: Number
        },
        roundMostKills: {
          type: Number
        },
        roundsPlayed: {
          type: Number
        },
        suicides: {
          type: Number
        },
        swimDistance: {
          type: Number
        },
        teamKills: {
          type: Number
        },
        timeSurvived: {
          type: Number
        },
        top10s: {
          type: Number
        },
        vehicleDestroys: {
          type: Number
        },
        walkDistance: {
          type: Number
        },
        weaponsAcquired: {
          type: Number
        },
        weeklyKills: {
          type: Number
        },
        weeklyWins: {
          type: Number
        },
        winPoints: {
          type: Number
        },
        wins: {
          type: Number
        }
      },
      "squad-fpp": {
        assists: {
          type: Number
        },
        boosts: {
          type: Number
        },
        dBNOs: {
          type: Number
        },
        dailyKills: {
          type: Number
        },
        dailyWins: {
          type: Number
        },
        damageDealt: {
          type: Number
        },
        days: {
          type: Number
        },
        headshotKills: {
          type: Number
        },
        heals: {
          type: Number
        },
        killPoints: {
          type: Number
        },
        kills: {
          type: Number
        },
        longestKill: {
          type: Number
        },
        longestTimeSurvived: {
          type: Number
        },
        losses: {
          type: Number
        },
        maxKillStreaks: {
          type: Number
        },
        mostSurvivalTime: {
          type: Number
        },
        rankPoints: {
          type: Number
        },
        rankPointsTitle: {
          type: String
        },
        revives: {
          type: Number
        },
        rideDistance: {
          type: Number
        },
        roadKills: {
          type: Number
        },
        roundMostKills: {
          type: Number
        },
        roundsPlayed: {
          type: Number
        },
        suicides: {
          type: Number
        },
        swimDistance: {
          type: Number
        },
        teamKills: {
          type: Number
        },
        timeSurvived: {
          type: Number
        },
        top10s: {
          type: Number
        },
        vehicleDestroys: {
          type: Number
        },
        walkDistance: {
          type: Number
        },
        weaponsAcquired: {
          type: Number
        },
        weeklyKills: {
          type: Number
        },
        weeklyWins: {
          type: Number
        },
        winPoints: {
          type: Number
        },
        wins: {
          type: Number
        }
      }
    }
  },
  relationships: {
    matchesSolo: {
      data: [{ id: { type: String }, type: { type: String } }]
    },
    matchesSoloFPP: {
      data: [{ id: { type: String }, type: { type: String } }]
    },
    matchesDuo: {
      data: [{ id: { type: String }, type: { type: String } }]
    },
    matchesDuoFPP: {
      data: [{ id: { type: String }, type: { type: String } }]
    },
    matchesSquad: {
      data: [{ id: { type: String }, type: { type: String } }]
    },
    matchesSquadFPP: {
      data: [{ id: { type: String }, type: { type: String } }]
    },
    season: {
      data: {
        type: {
          type: String
        },
        id: {
          type: String
        }
      }
    },
    player: {
      data: {
        type: {
          type: String
        },
        id: {
          type: String
        }
      }
    }
  },
  platform: { type: String },
  jobRanAt: { type: Date, default: Date.now }
});

pubgSchema.plugin(timestamps);

const Pubg = mongoose.model("pubgstats", pubgSchema);

module.exports.Pubg = Pubg;
