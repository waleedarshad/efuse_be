const mongoose = require("mongoose");

const { Schema } = mongoose;

const { timestamps } = require("../helpers/timestamp.helper");

const blacklistsSchema = new Schema({
  type: { type: String, enum: ["feeds"], index: true },
  pattern: { type: String },
  action: { type: String, enum: ["delete"] }
});

blacklistsSchema.plugin(timestamps);

const Blacklist = mongoose.model("blacklists", blacklistsSchema);

module.exports.Blacklist = Blacklist;
