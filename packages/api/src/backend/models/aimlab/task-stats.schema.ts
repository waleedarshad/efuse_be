import { Schema } from "mongoose";

import { AimlabTaskSchema } from "./task.schema";

export const AimlabTaskStatsSchema = new Schema({
  averageScore: Number,
  playCount: Number,
  task: AimlabTaskSchema,
  topScore: Number
});
