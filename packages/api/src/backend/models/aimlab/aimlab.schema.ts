import { Schema } from "mongoose";

import { AimlabRankSchema } from "./rank.schema";

export const AimlabSchema = new Schema({
  ranks: [AimlabRankSchema]
});
