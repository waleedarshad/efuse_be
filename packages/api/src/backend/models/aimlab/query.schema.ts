import { Schema } from "mongoose";

import { AimlabSchema } from "./aimlab.schema";
import { AimlabProfileSchema } from "./profile.model";
import { AimlabUserSchema } from "./user.schema";

export const AimlabQuerySchema = new Schema({
  aimlab: AimlabSchema,
  aimlabProfile: AimlabProfileSchema,
  viewer: AimlabUserSchema
});
