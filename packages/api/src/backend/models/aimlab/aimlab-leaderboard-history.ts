import { Document, model, Model, Schema, SchemaTypes } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { IAimlabLeaderboard } from "../../interfaces/leaderboard";

const AimlabLeaderboardHistorySchema = new Schema({
  rank: {
    type: Number
  },
  userId: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  recruitmentProfileId: {
    type: SchemaTypes.ObjectId,
    ref: "recruitmentprofiles",
    required: true,
    index: true
  },
  aimlabStatsId: {
    type: SchemaTypes.ObjectId,
    ref: "aimlabstats",
    required: true,
    index: true
  }
});

AimlabLeaderboardHistorySchema.plugin(mongooseAggregate);
AimlabLeaderboardHistorySchema.plugin(mongoosePaginate);
AimlabLeaderboardHistorySchema.plugin(timestamps);

export type AimlabLeaderboardHistoryModel<T extends Document> = Model<T>;

export const AimlabLeaderboardHistory: AimlabLeaderboardHistoryModel<IAimlabLeaderboard> = <
  AimlabLeaderboardHistoryModel<IAimlabLeaderboard>
>model<IAimlabLeaderboard>("aimlableaderboardhistory", AimlabLeaderboardHistorySchema, "aimlableaderboardhistory");
