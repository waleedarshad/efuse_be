import { Document, model, PaginateModel, Schema, SchemaTypes } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { IAimlabLeaderboard } from "../../interfaces/leaderboard";

const AimlabLeaderboardSchema = new Schema({
  rank: {
    type: Number
  },
  userId: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  recruitmentProfileId: {
    type: SchemaTypes.ObjectId,
    ref: "recruitmentprofiles",
    required: true,
    index: true
  },
  aimlabStatsId: {
    type: SchemaTypes.ObjectId,
    ref: "aimlabstats",
    required: true,
    index: true
  }
});

AimlabLeaderboardSchema.plugin(mongooseAggregate);
AimlabLeaderboardSchema.plugin(mongoosePaginate);
AimlabLeaderboardSchema.plugin(timestamps);

export type AimlabLeaderboardModel<T extends Document> = PaginateModel<T>;

export const AimlabLeaderboard: AimlabLeaderboardModel<IAimlabLeaderboard> = <
  AimlabLeaderboardModel<IAimlabLeaderboard>
>model<IAimlabLeaderboard>("aimlableaderboard", AimlabLeaderboardSchema, "aimlableaderboard");
