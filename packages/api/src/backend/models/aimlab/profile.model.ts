import { IAimlabProfile } from "@efuse/contracts";
import { Document, model, Model, Schema, SchemaTypes } from "mongoose";

import { AimlabRankingSchema } from "./ranking.schema";
import { AimlabSkillScoreSchema } from "./skill-score.schema";
import { AimlabTaskStatsSchema } from "./task-stats.schema";
import { AimlabUserSchema } from "./user.schema";
import { timestamps } from "../../helpers/timestamp.helper";

export interface AimlabProfileDto extends IAimlabProfile, Document {
  createdAt: Date;
  updatedAt: Date;
}

export const AimlabProfileSchema = new Schema({
  owner: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  ranking: AimlabRankingSchema,
  skillScores: [AimlabSkillScoreSchema],
  taskStats: [AimlabTaskStatsSchema],
  user: AimlabUserSchema,
  username: String
});

AimlabProfileSchema.plugin(timestamps);

export type AimlabProfileModel<T extends Document> = Model<T>;

export const AimlabProfile: AimlabProfileModel<AimlabProfileDto> = <AimlabProfileModel<AimlabProfileDto>>(
  model<AimlabProfileDto>("aimlabstats", AimlabProfileSchema)
);
