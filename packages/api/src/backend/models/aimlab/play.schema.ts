import { Schema } from "mongoose";

import { AimlabAppVersionSchema } from "./app-version.schema";

export const AimlabPlaySchema = new Schema({
  createdAt: String,
  endedAt: String,
  mode: Number,
  performanceScores: Object,
  playId: String,
  rawDataUrl: String,
  score: Number,
  startedAt: String,
  taskSlug: String,
  version: AimlabAppVersionSchema,
  weaponName: String,
  weaponType: String
});
