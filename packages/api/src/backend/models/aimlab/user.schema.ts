import { Schema } from "mongoose";

import { AimlabPlaySchema } from "./play.schema";

export const AimlabUserSchema = new Schema({
  id: String,
  playfabID: String,
  plays: [AimlabPlaySchema]
});
