import { AimlabRankTier } from "@efuse/contracts";
import { Schema } from "mongoose";

export const AimlabRankSchema = new Schema({
  displayName: String,
  level: Number,
  maxSkill: Number,
  minSkill: Number,
  tier: { type: String, enum: Object.values(AimlabRankTier) }
});
