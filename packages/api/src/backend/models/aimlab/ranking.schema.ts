import { Schema } from "mongoose";

import { AimlabRankSchema } from "./rank.schema";

export const AimlabRankingSchema = new Schema({
  rank: AimlabRankSchema,
  skill: { type: Number, index: true }
});
