import { IAimlabProfile } from "@efuse/contracts";
import { Document, model, Model, Schema, SchemaTypes } from "mongoose";

import { AimlabRankingSchema } from "./ranking.schema";
import { AimlabSkillScoreSchema } from "./skill-score.schema";
import { AimlabTaskStatsSchema } from "./task-stats.schema";
import { AimlabUserSchema } from "./user.schema";
import { timestamps } from "../../helpers/timestamp.helper";

export interface AimlabHistoricalStatsDto extends IAimlabProfile, Document {
  createdAt: Date;
  updatedAt: Date;
}

export const AimlabHistoricalStatsSchema = new Schema({
  owner: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  ranking: AimlabRankingSchema,
  skillScores: [AimlabSkillScoreSchema],
  taskStats: [AimlabTaskStatsSchema],
  user: AimlabUserSchema,
  username: String
});

AimlabHistoricalStatsSchema.plugin(timestamps);

export type AimlabHistoricalStatsModel<T extends Document> = Model<T>;

export const AimlabHistoricalStats: AimlabHistoricalStatsModel<AimlabHistoricalStatsDto> = <
  AimlabHistoricalStatsModel<AimlabHistoricalStatsDto>
>model<AimlabHistoricalStatsDto>("aimlabstats.historicalresults", AimlabHistoricalStatsSchema);
