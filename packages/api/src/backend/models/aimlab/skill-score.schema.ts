import { Schema } from "mongoose";

export const AimlabSkillScoreSchema = new Schema({
  name: String,
  score: Number
});
