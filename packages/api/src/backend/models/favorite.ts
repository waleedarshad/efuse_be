import { PaginateModel, Document, Schema, model } from "mongoose";
import { FavoriteOwnerType, FavoriteRelatedModel } from "@efuse/entities";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { IFavorite } from "../interfaces/favorite";
import { timestamps } from "../helpers/timestamp.helper";

const relatedModels = Object.values(FavoriteRelatedModel);
const ownerTypes = Object.values(FavoriteOwnerType);

const schema = new Schema({
  creator: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  relatedDoc: { type: Schema.Types.ObjectId, refPath: "relatedModel" },
  relatedModel: { type: String, enum: relatedModels },
  ownerType: { type: String, enum: ownerTypes },
  owner: { type: Schema.Types.ObjectId, refPath: "ownerType" }
});

schema.plugin(timestamps);
schema.plugin(mongooseAggregate);
schema.plugin(mongoosePaginate);

// this compound index will be used alot
schema.index({ relatedDoc: 1, owner: 1 });

export type FavoriteModel<T extends Document> = PaginateModel<T>;

export const Favorite: FavoriteModel<IFavorite> = <FavoriteModel<IFavorite>>model<IFavorite>("favorites", schema);
