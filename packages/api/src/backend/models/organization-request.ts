import { Schema, Document, model, PaginateModel, AggregatePaginateModel } from "mongoose";

import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import autopopulate from "mongoose-autopopulate";

import mongooseDelete from "mongoose-delete";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";
import { IOrganizationRequest } from "../interfaces";

const OrganizationRequestSchema = new Schema({
  organization: { type: Schema.Types.ObjectId, ref: "organizations", required: true },
  user: { type: Schema.Types.ObjectId, ref: "users", required: true },
  status: { type: String }
});

/**
 * Indexes
 */
OrganizationRequestSchema.index({ organization: 1, user: 1 });

/**
 * Plugins
 */
OrganizationRequestSchema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate", "updateMany"],
  indexFields: "all"
});
// todo: investigate
// @ts-ignore
OrganizationRequestSchema.plugin(autopopulate);
OrganizationRequestSchema.plugin(mongoosePaginate);
OrganizationRequestSchema.plugin(mongooseAggregate);
OrganizationRequestSchema.plugin(timestamps);

export type OrganizationRequestModel<T extends Document> = AggregatePaginateModel<T> &
  PaginateModel<T> &
  mongooseDelete.SoftDeleteModel<T>;

export const OrganizationRequest: OrganizationRequestModel<IOrganizationRequest> = <
  OrganizationRequestModel<IOrganizationRequest>
>model("organizationRequests", OrganizationRequestSchema);
