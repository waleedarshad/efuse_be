import { Field, ObjectType } from "type-graphql";
import { File } from "../file";
import { PaginatedResponse } from "../../../graphql/util/pagination";

@ObjectType("HypeUser", { description: "HypeUser Graphql type" })
export class HypeUser {
  @Field((_type) => String)
  id!: string;

  @Field((_type) => String)
  name!: string;

  @Field((_type) => String)
  username!: string;

  @Field((_type) => File)
  profilePicture?: File;

  @Field((_type) => Boolean)
  verified!: boolean;

  @Field((_type) => Boolean)
  currentUserFollows!: boolean;

  @Field((_type) => Boolean)
  isFollowedByUser!: boolean;
}

@ObjectType("HypedUser", { description: "Hype pagination" })
export class HypedUser extends PaginatedResponse(HypeUser) {}
