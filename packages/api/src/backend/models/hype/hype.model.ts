import { model, PaginateModel } from "mongoose";
import { plugin, prop, Ref, addModelToTypegoose, buildSchema, DocumentType } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID, Int } from "type-graphql";
import { User } from "../User";
import { IFeed, IComment, IUser } from "../../../backend/interfaces";
import { HypeEnum } from "@efuse/entities";
import { ObjectId } from "../../types";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { PaginatedResponse } from "../../../graphql/util/pagination";

@plugin(timestamps)
@plugin(mongoosePaginate)
@gqlObjectType("Hype", { description: "Hype Model" })
export class Hype {
  @gqlField((_type) => ID)
  readonly _id!: ObjectId;

  @gqlField((_type) => ID)
  readonly id?: ObjectId;

  @prop({ ref: User, required: true, index: true })
  user!: Ref<IUser>;

  @gqlField((_type) => String)
  @prop({ refPath: "objType", required: true, index: true })
  objId!: Ref<IComment> | Ref<IFeed>;

  @gqlField((_type) => String)
  @prop({ type: String, required: true, enum: HypeEnum })
  objType!: HypeEnum;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  platform?: string;

  @gqlField((_type) => Int)
  @prop({ type: Number, required: true })
  hypeCount!: number;

  createdAt?: Date; // Handled by timestamps plugin

  updatedAt?: Date; // Handled by timestamps plugin
}

const hypeSchema = buildSchema(Hype);

export const HypeModel = <PaginateModel<DocumentType<Hype>>>addModelToTypegoose(model("hypes", hypeSchema), Hype);

@gqlObjectType("HypePagination", { description: "Hyped pagination" })
export class HypePagination extends PaginatedResponse(Hype) {}
