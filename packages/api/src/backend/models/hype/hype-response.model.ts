import { ObjectType, Field } from "type-graphql";
import { Hype } from "./hype.model";

@ObjectType("HypeResponse", { description: "HypeResponse Graphql type" })
export class HypeResponse {
  @Field((_of) => Boolean)
  newHype!: boolean;

  @Field((_of) => Hype, { nullable: true })
  hype?: Hype;
}
