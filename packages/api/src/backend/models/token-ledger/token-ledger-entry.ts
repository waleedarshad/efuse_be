import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../../helpers/timestamp.helper";
import { ITokenLedgerEntry, TokenLedgerAction, TokenLedgerKind } from "../../interfaces";

const schema = new Schema({
  action: { type: String, enum: Object.values(TokenLedgerAction), index: true },
  balance: { type: Number },
  ledgerKind: { type: String, enum: Object.values(TokenLedgerKind), index: true },
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  value: { type: Number }
});

schema.plugin(timestamps);

export type TokenLedgerEntryModel<T extends Document> = Model<T>;
export const TokenLedgerEntry: TokenLedgerEntryModel<ITokenLedgerEntry> = <TokenLedgerEntryModel<ITokenLedgerEntry>>(
  model("tokenledger", schema)
);
