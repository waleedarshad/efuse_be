import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IOpportunityGame } from "../interfaces";

const opportunityGameSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "games", index: true },
  opportunity: { type: Schema.Types.ObjectId, ref: "opportunities", index: true }
});

opportunityGameSchema.index({ game: 1, opportunity: 1 });

opportunityGameSchema.plugin(timestamps);

export type OpportunityGameModel<T extends Document> = Model<T>;
export const OpportunityGame: OpportunityGameModel<IOpportunityGame> = <OpportunityGameModel<IOpportunityGame>>(
  model("opportunitygames", opportunityGameSchema)
);
