import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

@gqlObjectType("ThumbnailFields")
export class ThumbnailFields {
  @gqlField((_type) => String)
  @prop({ type: String })
  @gqlField((_type) => String)
  url!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  height!: number;

  @gqlField((_type) => String)
  @prop({ type: String })
  width!: number;
}
