import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

import { ThumbnailFields } from "./thumbnail-fields.model";

@gqlObjectType("YoutubeVideoThumbnail")
export class YoutubeVideoThumbnail {
  @gqlField((_type) => ThumbnailFields)
  @prop({ type: ThumbnailFields })
  default!: ThumbnailFields;

  @gqlField((_type) => ThumbnailFields)
  @prop({ type: ThumbnailFields })
  medium!: ThumbnailFields;

  @gqlField((_type) => ThumbnailFields)
  @prop({ type: ThumbnailFields })
  high!: ThumbnailFields;

  @gqlField((_type) => ThumbnailFields)
  @prop({ type: ThumbnailFields })
  standard!: ThumbnailFields;

  @gqlField((_type) => ThumbnailFields)
  @prop({ type: ThumbnailFields })
  maxres!: ThumbnailFields;
}
