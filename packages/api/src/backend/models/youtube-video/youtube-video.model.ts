import { model, PaginateModel } from "mongoose";
import { prop, plugin, index, addModelToTypegoose, buildSchema, DocumentType, Ref } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";
import { OwnerKind } from "@efuse/entities";

import { YoutubeVideoThumbnail } from "./thumbnail.model";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { IUser, IOrganization } from "../../../backend/interfaces";
import { PaginatedResponse } from "../../../graphql/util/pagination";
import { ObjectId } from "../../../backend/types";

@gqlObjectType("YoutubeVideo")
@plugin(timestamps)
@plugin(mongoosePaginate)
@index({ owner: 1, ownerKind: 1, externalId: 1 })
export class YoutubeVideo {
  @gqlField((_type) => ID)
  readonly _id!: ObjectId;

  @gqlField((_type) => String)
  @prop({ type: String })
  kind!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  etag!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  externalId!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  title!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  description!: string;

  @gqlField((_type) => YoutubeVideoThumbnail)
  @prop({ type: YoutubeVideoThumbnail })
  thumbnails!: YoutubeVideoThumbnail;

  @gqlField((_type) => String)
  @prop({ type: String })
  playlistId!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  videoId!: string;

  @gqlField((_type) => Date)
  @prop({ type: Date })
  videoPublishedAt!: Date;

  @gqlField((_type) => String, { nullable: true })
  @prop({ refPath: "ownerKind" })
  owner?: Ref<IUser> | Ref<IOrganization>;

  @gqlField((_type) => String, { nullable: false })
  @prop({ type: String, required: true, enum: OwnerKind })
  ownerKind!: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  videoUrl?: string;
}

const youtubeVideoSchema = buildSchema(YoutubeVideo);

export const YoutubeVideoModel = <PaginateModel<DocumentType<YoutubeVideo>>>(
  addModelToTypegoose(model("youtubevideos", youtubeVideoSchema), YoutubeVideo)
);

@gqlObjectType("YoutubeVidePagination", { description: "Youtube Video pagination" })
export class YoutubeVideoPagination extends PaginatedResponse(YoutubeVideo) {}
