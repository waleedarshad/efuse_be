import * as mongoose from "mongoose";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import schemaHelper from "../helpers/schema_helper";
import { CdnHelper } from "../helpers/cdn.helper";
import { timestamps } from "../helpers/timestamp.helper";
import { IMedia } from "../interfaces/media";

const { Schema } = mongoose;

const mediaSchema = new Schema({
  file: schemaHelper.fileSchema,
  user: { type: Schema.Types.ObjectId, required: true, ref: "users", index: true },
  mediaable: { type: Schema.Types.ObjectId, refPath: "mediaableType" },
  mediaableType: {
    type: String,
    required: true,
    enum: ["feeds", "messages", "comments"]
  },
  profile: { type: String, enum: ["sd", "hd", "uhd", "hls"], index: true },
  size: { type: Number, index: true }
});

mediaSchema.index({ mediaable: 1, mediaableType: 1 });
mediaSchema.index({ mediaable: 1, mediaableType: 1, profile: 1 });
mediaSchema.index({ user: 1, mediaableType: 1, profile: 1 });

mediaSchema.plugin(mongoosePaginate);
mediaSchema.plugin(CdnHelper.cdn);
mediaSchema.plugin(timestamps);

export type MediaModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;
export const Media: MediaModel<IMedia> = <MediaModel<IMedia>>mongoose.model<IMedia>("media", mediaSchema);
