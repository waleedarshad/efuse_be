import { prop } from "@typegoose/typegoose";
import { EmailNotification } from "./notification/email-notification";
import { PushNotification } from "./notification/push-notification";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("UserNotification", { description: "User notification settings" })
export class UserNotification {
  @gqlField((_type) => EmailNotification)
  @prop({ type: () => EmailNotification, _id: false })
  email!: EmailNotification;

  @gqlField((_type) => PushNotification)
  @prop({ type: () => PushNotification, _id: false })
  push!: PushNotification;
}
