import { prop } from "@typegoose/typegoose";
import { MediaProfileEnum } from "@efuse/entities";
import { Field as gqlField, ObjectType as gqlObjectType, registerEnumType } from "type-graphql";

@gqlObjectType("MediaSettings", { description: "A type that describe user media settings" })
export class MediaSettings {
  @gqlField((_type) => MediaProfileEnum)
  @prop({ type: () => String, enum: MediaProfileEnum, default: MediaProfileEnum.HD })
  profile!: MediaProfileEnum;

  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  mute!: Boolean;

  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  autoplay!: Boolean;
}

registerEnumType(MediaProfileEnum, {
  name: "MediaProfileEnum"
});
