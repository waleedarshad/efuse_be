import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

import {
  ChatNotification,
  PostNotification,
  FollowNotification,
  OpportunityNotification
} from "./notification-category";

@gqlObjectType("PushNotification", { description: "User push notification settings" })
export class PushNotification {
  @gqlField((_type) => ChatNotification)
  @prop({ type: () => ChatNotification, _id: false, required: true })
  chatNotifications!: ChatNotification;

  @gqlField((_type) => PostNotification)
  @prop({ type: () => PostNotification, _id: false, required: true })
  postNotifications!: PostNotification;

  @gqlField((_type) => FollowNotification)
  @prop({ type: () => FollowNotification, _id: false, required: true })
  followNotifications!: FollowNotification;

  @gqlField((_type) => OpportunityNotification)
  @prop({ type: () => OpportunityNotification, _id: false, required: true })
  opportunityNotifications!: OpportunityNotification;
}
