import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("PostNotification", { description: "User post notification settings" })
export class PostNotification {
  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  newHype!: Boolean;

  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  newComment!: Boolean;

  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  newShare!: Boolean;
}
