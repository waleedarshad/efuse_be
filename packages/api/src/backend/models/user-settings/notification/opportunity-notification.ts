import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("OpportunityNotification", { description: "User opportunity notification settings" })
export class OpportunityNotification {
  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  newApplicant!: Boolean;
}
