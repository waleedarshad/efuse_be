import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

import {
  ChatNotification,
  PostNotification,
  FollowNotification,
  OpportunityNotification,
  SummaryNotification
} from "./notification-category";

@gqlObjectType("EmailNotification", { description: "A type that describes EmailNotification" })
export class EmailNotification {
  @gqlField((_type) => ChatNotification)
  @prop({ type: () => ChatNotification, _id: false })
  chatNotifications!: ChatNotification;

  @gqlField((_type) => PostNotification)
  @prop({ type: () => PostNotification, _id: false })
  postNotifications!: PostNotification;

  @gqlField((_type) => FollowNotification)
  @prop({ type: () => FollowNotification, _id: false })
  followNotifications!: FollowNotification;

  @gqlField((_type) => OpportunityNotification)
  @prop({ type: () => OpportunityNotification, _id: false })
  opportunityNotifications!: OpportunityNotification;

  @gqlField((_type) => SummaryNotification)
  @prop({ type: () => SummaryNotification, _id: false })
  summaryNotifications!: SummaryNotification;
}
