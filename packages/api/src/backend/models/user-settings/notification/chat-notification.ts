import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("ChatNotification", { description: "User chat notification settings" })
export class ChatNotification {
  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true, required: true })
  newMessage!: Boolean;
}
