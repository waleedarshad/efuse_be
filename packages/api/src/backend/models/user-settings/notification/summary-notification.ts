import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("SummaryNotification", { description: "User summary notification settings" })
export class SummaryNotification {
  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  weeklySummary!: Boolean;
}
