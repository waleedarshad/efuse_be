import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("FollowNotification", { description: "User follow notification settings" })
export class FollowNotification {
  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  newUserFollow!: Boolean;
}
