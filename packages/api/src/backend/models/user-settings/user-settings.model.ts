import { model, Types } from "mongoose";
import { prop, plugin, Ref, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";

import { User } from "../User";
import { IUser } from "../../interfaces";
import { UserNotification } from "./user-notification";
import { MediaSettings } from "./media-settings/media-settings";
import { DefaultSettings } from "../../../lib/user-settings/default-settings";
import { timestamps } from "../../helpers/timestamp.helper";
@gqlObjectType("UserSettings", { description: "The User Settings Model" })
@plugin(timestamps)
export class UserSettings {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId; // Added for Graphql

  @prop({ ref: User, required: true, unique: true, index: 1 })
  user!: Ref<IUser>;

  @gqlField((_type) => UserNotification)
  @prop({
    type: () => UserNotification,
    _id: false,
    default: DefaultSettings.defaults
  })
  notifications!: UserNotification;

  @gqlField((_type) => Boolean)
  @prop({ type: () => Boolean, default: true })
  showStreaks?: Boolean;

  @gqlField((_type) => MediaSettings)
  @prop({ type: () => MediaSettings, _id: false, required: true, default: DefaultSettings.defaultsMediaSettings })
  media?: MediaSettings;

  @gqlField({ nullable: true })
  createdAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL

  @gqlField({ nullable: true })
  updatedAt?: Date; // Handled by timestamps plugin, so we only need to define it for graphQL
}

const userSettingsSchema = buildSchema(UserSettings);

export const UserSettingsModel = addModelToTypegoose(
  model("usersettings", userSettingsSchema, "usersettings"),
  UserSettings
);
