const mongoose = require("mongoose");

const { Schema } = mongoose;

const { timestamps } = require("../helpers/timestamp.helper");

const mobileSchema = new Schema({
  platform: { type: String, required: true, enum: ["ios", "android"] },
  version: { type: String, required: true },
  supported: { type: Boolean, default: true },
  unsupportedDate: { type: Date },
  user: { type: Schema.Types.ObjectId, required: true, ref: "users" },
  status: { type: String, enum: ["SUPPORTED", "UNSUPPORTED", "WIND_DOWN"] }
});

mobileSchema.plugin(timestamps);

const MobileVersion = mongoose.model("mobileversions", mobileSchema);

module.exports.MobileVersion = MobileVersion;
