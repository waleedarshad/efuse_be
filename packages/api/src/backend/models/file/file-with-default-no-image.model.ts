import { prop } from "@typegoose/typegoose";
import { NoImageEnum } from "@efuse/entities";

import { File as IFile } from "../../interfaces";

export class FileWithDefaultNoImage implements IFile {
  @prop({ type: () => String, default: NoImageEnum.filename })
  filename?: string;

  @prop({ type: () => String, default: NoImageEnum.contentType })
  contentType?: string;

  @prop({ type: () => String, default: NoImageEnum.url })
  url?: string;
}
