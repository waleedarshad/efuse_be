import { prop } from "@typegoose/typegoose";
import { MindBlownImageEnum } from "@efuse/entities";

import { File as IFile } from "../../interfaces";

export class FileWithDefaultMindBlownImage implements IFile {
  @prop({ type: () => String, default: MindBlownImageEnum.filename })
  filename?: string;

  @prop({ type: () => String, default: MindBlownImageEnum.contentType })
  contentType?: string;

  @prop({ type: () => String, default: MindBlownImageEnum.url })
  url?: string;
}
