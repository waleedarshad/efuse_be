import { prop } from "@typegoose/typegoose";
import { BriefcaseImageEnum } from "@efuse/entities";

import { File as IFile } from "../../interfaces";

export class FileWithDefaultBriefcaseImage implements IFile {
  @prop({ type: () => String, default: BriefcaseImageEnum.filename })
  filename?: string;

  @prop({ type: () => String, default: BriefcaseImageEnum.contentType })
  contentType?: string;

  @prop({ type: () => String, default: BriefcaseImageEnum.url })
  url?: string;
}
