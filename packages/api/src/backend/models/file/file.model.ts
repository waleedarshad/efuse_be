import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

import { File as IFile } from "../../interfaces";

@gqlObjectType("File", { description: "A type that describes a File" })
export class File implements IFile {
  @gqlField({ nullable: true, description: "The name of the given file" })
  @prop({ type: () => String })
  filename?: string;

  @gqlField({ nullable: true, description: "The MIME content-type for the given file" })
  @prop({ type: () => String })
  contentType?: string;

  @gqlField({ nullable: true, description: "The URL for which the file can be retrieved" })
  @prop({ type: () => String })
  url?: string;
}
