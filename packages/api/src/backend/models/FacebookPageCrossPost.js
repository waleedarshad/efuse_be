const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const mongoose = require("mongoose");

const { Schema } = mongoose;

const SHOULD_NOT_RETRY_ERRORS = {};

const facebookPageCrossPostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true
  },
  createdAt: { type: Date, default: Date.now },
  sentAt: { type: Date },
  content: { type: String, required: true },
  media: { type: Schema.Types.ObjectId, ref: "media" },
  checksum: { type: String, required: true },
  facebookPostId: { type: String },
  facebookPictureId: { type: String },
  facebookVideoId: { type: String },
  errorCode: { type: Number },
  errorMessage: { type: String },
  retryCount: { type: Number }
});

facebookPageCrossPostSchema.index({ user: 1, createdAt: 1 });
facebookPageCrossPostSchema.index({ checksum: 1, createdAt: 1 });
facebookPageCrossPostSchema.index({ errorCode: 1, retryCount: 1 });

facebookPageCrossPostSchema.methods.markSuccess = async ({ postId = null, pictureId = null, videoId = null }) => {
  this.sentAt = new Date();
  // todo: investigate
  // @ts-ignore
  this.markModified("sentAt");
  this.facebookPostId = postId;
  this.facebookPictureId = pictureId;
  this.facebookVideoId = videoId;
  // todo: investigate
  // @ts-ignore
  this.errorCode = undefined;
  // todo: investigate
  // @ts-ignore
  this.errorMessage = undefined;

  // todo: investigate
  // @ts-ignore
  return await this.save();
};

facebookPageCrossPostSchema.methods.markFailed = async (error, cb) => {
  // todo: investigate
  // @ts-ignore
  this.errorCode = error.code;

  // todo: investigate
  // @ts-ignore
  this.errorMessage = error.message;
  if (SHOULD_NOT_RETRY_ERRORS.hasOwnProperty(this.errorCode)) {
    // todo: investigate
    // @ts-ignore
    this.retryCount = 99;

    if (SHOULD_NOT_RETRY_ERRORS[this.errorCode].shouldUnlink) {
      logger.warn("Unlinking Facebook account due to error response.", {
        // todo: investigate
        // @ts-ignore
        user: this.user,
        code: this.errorCode,
        message: this.errorMessage
      });
      // TODO: Unlink facebook account
    }
  } else if (this.retryCount === undefined || this.retryCount === null) {
    // todo: investigate
    // @ts-ignore
    this.retryCount = 0;
  } else {
    this.retryCount++;
  }

  // todo: investigate
  // @ts-ignore
  return this.save(cb);
};

const FacebookPageCrossPost = mongoose.model("facebookPageCrossPost", facebookPageCrossPostSchema);

module.exports.FacebookPageCrossPost = FacebookPageCrossPost;
