const mongoose = require("mongoose");
const autopopulate = require("mongoose-autopopulate");
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");

const { Schema } = mongoose;

const WhitelistHelper = require("../helpers/whitelist_helper");
const { timestamps } = require("../helpers/timestamp.helper");

const notificationSchema = new Schema({
  content: { type: String, required: true },
  sender: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate
  },
  receiver: { type: Schema.Types.ObjectId, ref: "users", required: true },
  read: { type: Boolean, default: false },
  readCount: { type: Boolean, default: false },
  notifyable: {
    type: Schema.Types.ObjectId,
    refPath: "notifyableType",
    required: true,
    autopopulate: true
  },
  notifyableType: {
    type: String,
    enum: [
      "messages",
      "friends",
      "comments",
      "feeds",
      "homeFeeds",
      "opportunities",
      "organizationInvitations",
      "erenatournaments",
      "hypes"
    ],
    required: true
  },
  timeline: { type: Schema.Types.ObjectId, ref: "homeFeeds" },
  target: { type: String, enum: ["all", "web", "mobile"], default: "all" }
});

notificationSchema.index({ receiver: 1, read: 1 });

// todo: investigate
// @ts-ignore
notificationSchema.plugin(autopopulate);
notificationSchema.plugin(mongoosePaginate);
notificationSchema.plugin(timestamps);

const Notification = mongoose.model("notifications", notificationSchema);

module.exports.Notification = Notification;
