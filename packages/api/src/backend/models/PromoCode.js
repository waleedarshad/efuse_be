const mongoose = require("mongoose");

const { Schema } = mongoose;
const { timestamps } = require("../helpers/timestamp.helper");

const promoCodesScheme = new Schema({
  // array of permissions controling what products / services this code can be used on.
  allowedOn: [
    {
      type: String,
      required: true,
      enum: ["PAID_OPPORTUNITY"]
    }
  ],

  // controls logic on discounting as a fixed dollar amount ("VALUE"), or a percentage off the total ("PERCENTAGE")
  discountType: {
    type: String,
    required: true,
    enum: ["PERCENTAGE", "VALUE"]
  },

  // this is the alphanumber code that user will be asked to enter on order screen
  code: { type: String, unique: true, required: true },

  // amount of discount - either percentage (0.20 for 20%) or value (20.00 for $20 off)
  amount: { type: Number, required: true },

  // allows code to be used one time for each user
  oneTimeUse: { type: Boolean, default: true },

  // exterbak use only - shown to user after applying promocode. This will help a "success" message to user confirming the promocode was added to order
  userMessage: { type: String },

  // internal use only - not shown to user. This will help us keep track of why this promocode was created
  purpose: { type: String },

  // controls whether code can be used on order
  active: { type: Boolean, default: true }
});

promoCodesScheme.plugin(timestamps);

const PromoCode = mongoose.model("promocodes", promoCodesScheme);

module.exports.PromoCode = PromoCode;
