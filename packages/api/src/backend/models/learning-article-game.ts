import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { ILearningArticleGame } from "../interfaces";

const LearningArticleGameSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "games", index: true },
  learningArticle: { type: Schema.Types.ObjectId, ref: "learning-articles", index: true }
});

LearningArticleGameSchema.index({ game: 1, article: 1 });

LearningArticleGameSchema.plugin(timestamps);

export type LearningArticleGameModel<T extends Document> = Model<T>;
export const LearningArticleGame: LearningArticleGameModel<ILearningArticleGame> = <
  LearningArticleGameModel<ILearningArticleGame>
>model("learningarticlegames", LearningArticleGameSchema);
