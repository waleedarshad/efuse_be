const mongoose = require("mongoose");

const { Schema } = mongoose;
const SchemaHelper = require("../helpers/schema_helper");
const { timestamps } = require("../helpers/timestamp.helper");

const payoutAccountScheme = new Schema({
  stripeAccountId: { type: String, required: true, unique: true },
  stripePublishableKey: { type: String, required: true },
  user: { type: Schema.Types.ObjectId, ref: "users", required: true },
  scope: { type: String, required: true },
  accessToken: { type: String, required: true },
  refreshToken: { type: String, required: true },
  active: { type: Boolean, default: true }
});

payoutAccountScheme.plugin(timestamps);

const PayoutAccount = mongoose.model("payoutaccount", payoutAccountScheme);

module.exports.PayoutAccount = PayoutAccount;
