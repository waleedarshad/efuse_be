import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import {
  GiphyImage,
  Downsized,
  DownsizedMedium,
  DownsizedSmall,
  DownsizedStill,
  FixedHeight,
  FixedHeightDownsampled,
  FixedHeightSmall,
  FixedHeightStill,
  FixedWidth,
  FixedWidthDownsampled,
  FixedWidthSmall,
  FixedWidthSmallStill,
  FixedWidthStill,
  Looping,
  Original,
  OriginalMp4,
  OriginalStill,
  Preview,
  PreviewGif,
  PreviewWebp
} from "./dimensions";

@gqlObjectType("ImageSize", { description: "ImageSize Type" })
export class ImageSize {
  @gqlField((_type) => GiphyImage, { nullable: true })
  @prop({ type: () => GiphyImage })
  downsized_large?: GiphyImage;

  @gqlField((_type) => GiphyImage, { nullable: true })
  @prop({ type: () => GiphyImage })
  fixed_height_small_still?: GiphyImage;

  @gqlField((_type) => Original, { nullable: true })
  @prop({ type: () => Original })
  original?: Original;

  @gqlField((_type) => FixedHeightDownsampled, { nullable: true })
  @prop({ type: () => FixedHeightDownsampled })
  fixed_height_downsampled?: FixedHeightDownsampled;

  @gqlField((_type) => DownsizedStill, { nullable: true })
  @prop({ type: () => DownsizedStill })
  downsized_still?: DownsizedStill;

  @gqlField((_type) => FixedHeightStill, { nullable: true })
  @prop({ type: () => FixedHeightStill })
  fixed_height_still?: FixedHeightStill;

  @gqlField((_type) => DownsizedMedium, { nullable: true })
  @prop({ type: () => DownsizedMedium })
  downsized_medium?: DownsizedMedium;

  @gqlField((_type) => Downsized, { nullable: true })
  @prop({ type: () => Downsized })
  downsized?: Downsized;

  @gqlField((_type) => PreviewWebp, { nullable: true })
  @prop({ type: () => PreviewWebp })
  preview_webp?: PreviewWebp;

  @gqlField((_type) => OriginalMp4, { nullable: true })
  @prop({ type: () => OriginalMp4 })
  original_mp4?: OriginalMp4;

  @gqlField((_type) => FixedHeightSmall, { nullable: true })
  @prop({ type: () => FixedHeightSmall })
  fixed_height_small?: FixedHeightSmall;

  @gqlField((_type) => FixedHeight, { nullable: true })
  @prop({ type: () => FixedHeight })
  fixed_height?: FixedHeight;

  @gqlField((_type) => DownsizedSmall, { nullable: true })
  @prop({ type: () => DownsizedSmall })
  downsized_small?: DownsizedSmall;

  @gqlField((_type) => Preview, { nullable: true })
  @prop({ type: () => Preview })
  preview?: Preview;

  @gqlField((_type) => FixedWidthDownsampled, { nullable: true })
  @prop({ type: () => FixedWidthDownsampled })
  fixed_width_downsampled?: FixedWidthDownsampled;

  @gqlField((_type) => FixedWidthSmallStill, { nullable: true })
  @prop({ type: () => FixedWidthSmallStill })
  fixed_width_small_still?: FixedWidthSmallStill;

  @gqlField((_type) => FixedWidthSmall, { nullable: true })
  @prop({ type: () => FixedWidthSmall })
  fixed_width_small?: FixedWidthSmall;

  @gqlField((_type) => OriginalStill, { nullable: true })
  @prop({ type: () => OriginalStill })
  original_still?: OriginalStill;

  @gqlField((_type) => FixedWidthStill, { nullable: true })
  @prop({ type: () => FixedWidthStill })
  fixed_width_still?: FixedWidthStill;

  @gqlField((_type) => Looping, { nullable: true })
  @prop({ type: () => Looping })
  looping?: Looping;

  @gqlField((_type) => FixedWidth, { nullable: true })
  @prop({ type: () => FixedWidth })
  fixed_width?: FixedWidth;

  @gqlField((_type) => PreviewGif, { nullable: true })
  @prop({ type: () => PreviewGif })
  preview_gif?: PreviewGif;

  @gqlField({ nullable: true })
  createdAt?: Date;

  @gqlField({ nullable: true })
  updatedAt?: Date;
}
