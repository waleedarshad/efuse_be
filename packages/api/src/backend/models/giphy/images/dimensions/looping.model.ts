import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

@gqlObjectType("Looping")
export class Looping {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4_size?: string;
}
