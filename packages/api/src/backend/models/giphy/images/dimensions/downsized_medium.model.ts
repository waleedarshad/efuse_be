import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("DownsizedMedium")
export class DownsizedMedium extends GiphyImage {}
