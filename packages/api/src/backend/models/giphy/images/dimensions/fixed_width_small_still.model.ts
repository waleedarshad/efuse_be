import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("FixedWidthSmallStill")
export class FixedWidthSmallStill extends GiphyImage {}
