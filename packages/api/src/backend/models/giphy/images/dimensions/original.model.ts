import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";
import { prop } from "@typegoose/typegoose";

@gqlObjectType("Original")
export class Original extends GiphyImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  frames?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  hash?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4_size?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  webp?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  webp_size?: string;
}
