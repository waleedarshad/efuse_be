import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

import { BaseImage } from "../base/base-image.model";

@gqlObjectType("Still480", { description: "Still480 image type" })
export class Still480 extends BaseImage {
  @gqlField((_type) => String)
  @prop({ type: String, nullable: true })
  url?: string;
}
