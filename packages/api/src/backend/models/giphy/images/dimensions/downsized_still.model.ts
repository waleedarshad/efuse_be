import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

import { BaseImage } from "../base/base-image.model";

@gqlObjectType("DownsizedStill")
export class DownsizedStill extends BaseImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  size?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  url?: string;
}
