import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("FixedWidthStill")
export class FixedWidthStill extends GiphyImage {}
