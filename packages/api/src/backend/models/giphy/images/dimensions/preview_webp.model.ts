import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("PreviewWebp")
export class PreviewWebp extends GiphyImage {}
