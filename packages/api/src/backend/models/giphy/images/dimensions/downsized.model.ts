import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("Downsized")
export class Downsized extends GiphyImage {}
