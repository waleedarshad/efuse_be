import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("OriginalStill")
export class OriginalStill extends GiphyImage {}
