import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("FixedHeightSmall")
export class FixedHeightSmall extends GiphyImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4_size?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  webp?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  webp_size?: string;
}
