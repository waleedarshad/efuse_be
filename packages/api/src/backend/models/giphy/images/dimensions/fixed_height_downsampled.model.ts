import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";
import { prop } from "@typegoose/typegoose";

@gqlObjectType("FixedHeightDownsampled")
export class FixedHeightDownsampled extends GiphyImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  webp?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  webp_size?: string;
}
