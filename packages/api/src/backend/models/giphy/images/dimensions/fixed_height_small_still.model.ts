import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("FixedHeightSmallStill")
export class FixedHeightSmallStill extends GiphyImage {
  @gqlField((_type) => String)
  @prop({ type: String, nullable: true })
  webp?: string;

  @gqlField((_type) => String)
  @prop({ type: String, nullable: true })
  webp_size?: string;

  @gqlField((_type) => String)
  @prop({ type: String, nullable: true })
  mp4?: string;

  @gqlField((_type) => String)
  @prop({ type: String, nullable: true })
  mp4_size?: string;
}
