import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("DownsizeLarge")
export class DownsizeLarge extends GiphyImage {}
