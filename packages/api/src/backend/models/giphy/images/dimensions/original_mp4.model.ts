import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { BaseImage } from "../base/base-image.model";
import { prop } from "@typegoose/typegoose";

@gqlObjectType("OriginalMp4")
export class OriginalMp4 extends BaseImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4_size?: string;
}
