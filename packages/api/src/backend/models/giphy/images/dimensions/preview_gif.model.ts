import { ObjectType as gqlObjectType } from "type-graphql";
import { GiphyImage } from "../base/giphy-image.model";

@gqlObjectType("PreviewGif")
export class PreviewGif extends GiphyImage {}
