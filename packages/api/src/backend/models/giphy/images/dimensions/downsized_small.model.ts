import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

import { BaseImage } from "../base/base-image.model";

@gqlObjectType("DownsizedSmall", { description: "DownsizedSmall type" })
export class DownsizedSmall extends BaseImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  mp4_size?: string;
}
