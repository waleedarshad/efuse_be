import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("GiphyImage", { description: "GiphyImage Model" })
export class GiphyImage {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  height?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  size?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  width?: string;
}
