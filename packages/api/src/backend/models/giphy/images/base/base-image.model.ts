import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("BaseImage", { description: "Base Image Type" })
export class BaseImage {
  @gqlField((_type) => String)
  @prop({ type: String })
  height?: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  width?: string;
}
