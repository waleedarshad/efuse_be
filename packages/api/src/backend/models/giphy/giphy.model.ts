import { model } from "mongoose";
import { prop, plugin, addModelToTypegoose, buildSchema } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";
import { timestamps } from "../../helpers/timestamp.helper";
import { ImageSize } from "./images";
import { GiphyAnalytics } from "./analytics/analytics.model";
import { GiphyUser } from "./user/giphy-user.model";
import { ObjectId } from "../../types";
@gqlObjectType("Giphy", { description: "The Giphy model" })
@plugin(timestamps)
export class Giphy {
  @gqlField((_type) => ID)
  readonly _id!: ObjectId; // Added for Graphql

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  type?: string;

  @gqlField((_type) => String, { nullable: false })
  @prop({ type: String })
  id!: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  slug?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  bitly_gif_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  bitly_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  embed_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  username?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  source?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  title?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  rating?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  content_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  source_tld?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  source_post_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  is_sticker?: number;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  import_datetime?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  trending_datetime?: string;

  @gqlField((_type) => ImageSize, { nullable: true })
  @prop({ type: () => ImageSize })
  images?: ImageSize;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  analytics_response_payload?: string;

  @gqlField((_type) => GiphyAnalytics, { nullable: true })
  @prop({ type: () => GiphyAnalytics })
  analytics?: GiphyAnalytics;

  @gqlField((_type) => GiphyUser, { nullable: true })
  @prop({ type: GiphyUser })
  user?: GiphyUser;
}

const giphySchema = buildSchema(Giphy);

export const GiphyModel = addModelToTypegoose(model("giphygifs", giphySchema), Giphy);
