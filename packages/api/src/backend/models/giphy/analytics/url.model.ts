import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("Url")
export class Url {
  @gqlField((_type) => String)
  @prop({ type: String })
  url?: string;
}
