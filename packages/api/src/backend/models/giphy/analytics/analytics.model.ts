import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

import { Url } from "./url.model";

@gqlObjectType("GiphyAnalytics")
export class GiphyAnalytics {
  @gqlField((_type) => Url, { nullable: true })
  @prop({ type: Url })
  onload?: Url;

  @gqlField((_type) => Url, { nullable: true })
  @prop({ type: () => Url })
  onclick?: Url;

  @gqlField((_type) => Url, { nullable: true })
  @prop({ type: () => Url })
  onset?: Url;
}
