import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { prop } from "@typegoose/typegoose";

@gqlObjectType("GiphyUser")
export class GiphyUser {
  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  avatar_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  banner_image?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  banner_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  profile_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  username?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  display_name?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  description?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  instagram_url?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: String })
  website_url?: string;

  @gqlField((_type) => Boolean, { nullable: true })
  @prop({ type: Boolean })
  is_verified?: boolean;
}
