const mongoose = require("mongoose");

const { Schema } = mongoose;

/**
 * @deprecated use `TwitchAccountInfo`
 */
const twitchSchema = new Schema({
  viewCount: {
    type: Number
  },
  displayName: {
    type: String
  },
  profileImageUrl: {
    type: String
  },
  description: {
    type: String
  },
  type: {
    type: String
  },
  broadcasterType: {
    type: String
  },
  webhookSubscription: {
    isActive: { type: Boolean, default: false },
    callback: { type: String },
    expiresAt: { type: Date }
  },
  stream: {
    isLive: { type: Boolean, default: false },
    title: { type: String },
    game: {
      // not used
      type: Schema.Types.ObjectId,
      ref: "twitchTopGames",
      autopopulate: true
    },
    viewerCount: { type: Number },
    startedAt: { type: Date },
    endedAt: { type: Date },
    lastLiveAt: { type: Date } // not used
  },
  sendStartStreamMessage: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: null }
});

/**
 * @deprecated use TwitchAccountInfo
 *
 * TODO: [org-clout] remove all uses of this model and move data to `TwitchAccountInfo`
 */
const Twitch = mongoose.model("twitches", twitchSchema);

module.exports.Twitch = Twitch;
