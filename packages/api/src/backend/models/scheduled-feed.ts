import mongoose from "mongoose";

import autopopulate from "mongoose-autopopulate";
import SchemaHelper from "../helpers/schema_helper";
import { timestamps } from "../helpers/timestamp.helper";
import { CronStatusEnum } from "../../lib/track-cron/track-cron.enum";
import { IScheduledFeed } from "../interfaces/scheduled-feed";

const { Schema } = mongoose;

const scheduledFeedSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", required: true, index: true },
  content: { type: String },
  mentions: SchemaHelper.mentionSchema,
  shared: { type: Boolean, default: false },
  sharedFeed: { type: Schema.Types.ObjectId, ref: "feeds" },
  sharedTimeline: { type: Schema.Types.ObjectId, ref: "homeFeeds" },
  timelineable: { type: Schema.Types.ObjectId, refPath: "timelineableType", required: true },
  timelineableType: { type: String, enum: ["users", "organizations"], default: "users" },
  sharedPostUser: { type: Schema.Types.ObjectId, ref: "users" },
  sharedTimelineable: { type: Schema.Types.ObjectId, refPath: "sharedTimelineableType" },
  sharedTimelineableType: { type: String, enum: ["users", "organizations"] },
  verified: { type: Boolean, default: false },
  crossPosts: [String],
  media: { type: Schema.Types.ObjectId, ref: "media", autopopulate: true },
  mediaObjects: [{ type: Schema.Types.ObjectId, ref: "media", autopopulate: true }],
  twitchClip: { type: Schema.Types.ObjectId, ref: "twitchclips" },
  scheduledAt: { type: Date, index: true },
  processed: { type: Boolean, default: false, index: true },
  platform: { type: String },
  giphy: { type: Schema.Types.ObjectId, ref: "giphygifs" },
  status: { type: String, enum: Object.values(CronStatusEnum) },
  errorMessage: { type: String },
  jobSetAt: { type: Date },
  jobCompletedAt: { type: Date },
  associatedGame: { type: Schema.Types.ObjectId, ref: "games", autopopulate: true },
  youtubeVideo: { type: Schema.Types.ObjectId, ref: "youtubevideos" }
});

scheduledFeedSchema.plugin(autopopulate);
scheduledFeedSchema.plugin(timestamps);

scheduledFeedSchema.index({ scheduledAt: 1, processed: 1 });

export type ScheduledFeedModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;

export const ScheduledFeed: ScheduledFeedModel<IScheduledFeed> = <ScheduledFeedModel<IScheduledFeed>>(
  mongoose.model<IScheduledFeed>("scheduledfeeds", scheduledFeedSchema)
);
