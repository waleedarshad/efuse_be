import { model } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose, DocumentType } from "@typegoose/typegoose";
import { timestamps } from "../helpers/timestamp.helper";
import { PaginateModel, AggregatePaginateModel } from "mongoose";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";

@plugin(timestamps)
@plugin(mongoosePaginate)
export class Developer {
  @prop({ type: () => String, unique: true, required: true })
  apiKey!: string;

  @prop({ type: () => Number, default: 0 })
  allowedRequestPerHour?: Number;

  @prop({ type: () => String })
  scope?: string;

  @prop({ type: () => Boolean, default: true })
  active?: Boolean;

  createdAt?: Date; // Handled by timestamps plugin

  updatedAt?: Date; // Handled by timestamps plugin
}

const developerSchema = buildSchema(Developer);

export const DeveloperModel = <
  PaginateModel<DocumentType<Developer>> & AggregatePaginateModel<DocumentType<Developer>>
>addModelToTypegoose(model("developers", developerSchema), Developer);
