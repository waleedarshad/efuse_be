import { model, Types } from "mongoose";
import { prop, plugin, Ref, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID, registerEnumType } from "type-graphql";
import { PlatformEnum } from "@efuse/entities";

import { User } from "./User";
import { IUser } from "../../backend/interfaces";
import { timestamps } from "../helpers/timestamp.helper";

@gqlObjectType("GamePlatform", { description: "User Game Platform model" })
@plugin(timestamps)
export class GamePlatform {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId;

  @prop({ ref: User, required: true, index: true })
  user!: Ref<IUser>;

  @gqlField((_type) => PlatformEnum)
  @prop({ type: String, enum: PlatformEnum })
  platform!: PlatformEnum;

  @gqlField(() => String)
  @prop({ type: String })
  displayName!: string;

  @gqlField({ nullable: true })
  createdAt?: Date;
  @gqlField({ nullable: true })
  updatedAt?: Date;
}

const GamePlatformSchema = buildSchema(GamePlatform);

registerEnumType(PlatformEnum, {
  name: "PlatformEnum"
});

export const GamePlatformModel = addModelToTypegoose(
  model("gameplatforms", GamePlatformSchema, "gameplatforms"),
  GamePlatform
);
