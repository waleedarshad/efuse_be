import { Schema, Document, PaginateModel, model } from "mongoose";
import autopopulate from "mongoose-autopopulate";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";
import WhitelistHelper from "../helpers/whitelist_helper";
import { IApplicant } from "../interfaces";
import { ApplicantEntityTypeEnum, ApplicantStatusEnum } from "../../lib/applicants/applicant.enum";

const applicantSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulateInApplicants,
    ref: "users"
  },
  opportunity: {
    type: Schema.Types.ObjectId,
    autopopulate: true,
    ref: "opportunities",
    index: true
  },
  candidateQuestions: [
    {
      index: { type: Number },
      question: { type: String },
      media: { filename: { type: String }, contentType: { type: String }, url: { type: String } }
    }
  ],
  status: { type: String, default: ApplicantStatusEnum.NO_ACTION, enum: Object.values(ApplicantStatusEnum) },
  entityType: {
    type: String,
    default: ApplicantEntityTypeEnum.OPPORTUNITY,
    enum: Object.values(ApplicantEntityTypeEnum),
    index: true
  },
  entity: { type: Schema.Types.ObjectId, refPath: "entityType", index: true, required: true }
});

applicantSchema.index({ user: 1, opportunity: 1, createdAt: -1 });
applicantSchema.index({ opportunity: 1, createdAt: -1 });
applicantSchema.index({ user: 1, opportunity: 1 });
applicantSchema.index({ entity: 1, entityType: 1 });
applicantSchema.index({ user: 1, entity: 1, entityType: 1 });
applicantSchema.index({ entity: 1, entityType: 1, createdAt: -1 });
applicantSchema.index({ user: 1, entity: 1, entityType: 1, createdAt: -1 });

applicantSchema.plugin(mongoosePaginate);
applicantSchema.plugin(autopopulate);
applicantSchema.plugin(timestamps);

export type ApplicantModel<T extends Document> = PaginateModel<T>;

export const Applicant: ApplicantModel<IApplicant> = <ApplicantModel<IApplicant>>model("applicants", applicantSchema);
