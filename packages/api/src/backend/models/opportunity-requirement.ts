import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IOpportunityRequirements } from "../interfaces";

const opportunityRequirementsSchema = new Schema({
  name: { type: String },
  description: { type: String },
  isActive: { type: Boolean, default: false },
  target: { type: String, enum: ["User"], default: "User" },
  property: { type: String },
  custom: { type: Boolean, default: false }
});

opportunityRequirementsSchema.plugin(timestamps);

export type OpportunityRequirementModel<T extends Document> = Model<T>;

export const OpportunityRequirement: OpportunityRequirementModel<IOpportunityRequirements> = <
  OpportunityRequirementModel<IOpportunityRequirements>
>model("opportunityrequirements", opportunityRequirementsSchema);
