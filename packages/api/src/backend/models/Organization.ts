import { AggregatePaginateModel, Document, model, PaginateModel, Schema } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import mongooseDelete from "mongoose-delete";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { OrganizationPublishStatusEnum } from "../../lib/organizations/organization.enum";
import * as SchemaHelper from "../helpers/schema_helper";
import { timestamps } from "../helpers/timestamp.helper";
import { IOrganization } from "../interfaces";

const schema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  scale: { type: String },
  website: { type: String },
  status: { type: String, required: true },
  organizationType: { type: String, required: true },
  email: { type: String },
  phoneNumber: { type: String },
  toBeVerified: { type: Boolean },
  location: { type: String },
  verified: {
    status: { type: Boolean, default: false },
    verifiedBy: { type: Schema.Types.ObjectId, ref: "users" },
    verifiedAt: { type: Date, default: Date.now }
  },
  profileImage: SchemaHelper.fileSchema,
  headerImage: SchemaHelper.fileSchemaDefaultsNoImage,

  // owner of the organization
  user: { type: Schema.Types.ObjectId, ref: "users" },
  captains: [{ type: Schema.Types.ObjectId, ref: "users" }],
  about: { type: String },
  events: [
    {
      title: { type: String },
      subTitle: { type: String },
      eventDate: { type: Date },
      description: { type: String },
      image: SchemaHelper.fileSchemaDefaultsBriefcase
    }
  ],
  honors: [
    {
      title: { type: String },
      subTitle: { type: String },
      description: { type: String },
      image: SchemaHelper.fileSchemaDefaultsBriefcase
    }
  ],
  promoVideo: {
    title: { type: String },
    description: { type: String },
    embeddedUrl: { type: String },
    isVideo: { type: Boolean, default: false },
    videoFile: {
      filename: {
        type: String
      },
      contentType: {
        type: String
      },
      url: {
        type: String
      }
    }
  },
  playerCards: [
    {
      title: { type: String },
      subTitle: { type: String },
      url: { type: String },
      image: SchemaHelper.fileSchema
    }
  ],
  discordServer: { type: String },
  publishStatus: {
    type: String,
    default: OrganizationPublishStatusEnum.VISIBLE,
    index: true,
    enum: Object.values(OrganizationPublishStatusEnum)
  },
  slug: { type: String, unique: true },
  shortName: { type: String, index: true, unique: true },
  videoCarousel: [
    {
      url: { type: String },
      title: { type: String },
      uploaded: { type: Boolean, default: false },
      video: {
        filename: {
          type: String
        },
        contentType: {
          type: String
        },
        url: {
          type: String
        }
      }
    }
  ],
  followersCount: { type: Number, default: 0 },
  promoted: { type: Boolean, default: false }
});

// indexes
schema.index({ name: "text" });

// plugins
schema.plugin(mongoosePaginate);
schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate"]
});

export type OrganizationModel<T extends Document> = AggregatePaginateModel<T> &
  PaginateModel<T> &
  mongooseDelete.SoftDeleteModel<T>;

export const Organization: OrganizationModel<IOrganization> = <OrganizationModel<IOrganization>>(
  model<IOrganization>("organizations", schema)
);
