import { Schema, Document, Model, model } from "mongoose";
import autopopulate from "mongoose-autopopulate";

import SchemaHelper from "../../helpers/schema_helper";
import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaMatch } from "../../interfaces/erena";

const ERenaMatchSchema = new Schema({
  matchDate: { type: Date },
  winner: {
    type: Schema.Types.ObjectId,
    ref: "erenateams",
    autopopulate: true
  },
  matchNumber: { type: Number },
  tournament: {
    type: Schema.Types.ObjectId,
    ref: "erenatournaments",
    index: true
  },
  teams: [
    {
      team: {
        type: Schema.Types.ObjectId,
        ref: "erenateams",
        autopopulate: true,
        index: true
      }
    }
  ],
  teamOneImage: SchemaHelper.fileSchemaDefaultsNoImage,
  teamTwoImage: SchemaHelper.fileSchemaDefaultsNoImage,

  /**
   * @deprecated
   */
  teamOneScore: {
    type: Number
  },
  /**
   * @deprecated
   */
  teamTwoScore: {
    type: Number
  }
});

ERenaMatchSchema.index({ matchNumber: 1, tournament: 1 });

ERenaMatchSchema.plugin(autopopulate);
ERenaMatchSchema.plugin(timestamps);

export type ERenaMatchModel<T extends Document> = Model<T>;
export const ERenaMatch: ERenaMatchModel<IERenaMatch> = <ERenaMatchModel<IERenaMatch>>(
  model<IERenaMatch>("erenamatches", ERenaMatchSchema)
);
