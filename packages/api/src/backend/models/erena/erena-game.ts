import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaGame } from "../../interfaces";

const erenaGameSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "games", index: true },
  tournament: { type: Schema.Types.ObjectId, ref: "erenatournaments", index: true }
});

erenaGameSchema.index({ game: 1, tournament: 1 });

erenaGameSchema.plugin(timestamps);

export type ERenaGameModel<T extends Document> = Model<T>;
export const ERenaGame: ERenaGameModel<IERenaGame> = <ERenaGameModel<IERenaGame>>model("erenagames", erenaGameSchema);
