import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaTeam } from "../../interfaces/erena";
import { ERenaType } from "../../interfaces/erena/erena-enums";

const ERenaTeamSchema = new Schema({
  name: { type: String },
  isActive: { type: Boolean },
  tournament: { type: Schema.Types.ObjectId, ref: "erenatournaments", index: true },
  type: {
    type: String,
    default: ERenaType.whiteLabel,
    enum: Object.values(ERenaType)
  },

  /**
   * @deprecated
   */
  stats: {
    kills: { type: Number, default: 0 },
    value: { type: Number, default: 0 },
    name: { type: String, default: "Points" } // Name for the stats to show up e.g. "Points", "Kills"
  },
  /**
   * @deprecated
   */
  airtableId: { type: String, index: true },
  // This is the airtable ID for the "Base"
  /**
   * @deprecated
   */
  airtableBaseId: { type: String },
  /**
   * @deprecated
   */
  lastAirtableModificationTime: { type: String }
});

ERenaTeamSchema.plugin(timestamps);

export type ERenaTeamModel<T extends Document> = Model<T>;

export const ERenaTeam: ERenaTeamModel<IERenaTeam> = <ERenaTeamModel<IERenaTeam>>(
  model<IERenaTeam>("erenateams", ERenaTeamSchema)
);
