import { Schema, Document, PaginateModel, model } from "mongoose";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaTournament } from "../../interfaces/erena";
import {
  ERenaScoringKinds,
  ERenaTournamentOwnerKinds,
  ERenaTournamentStatus,
  ERenaType
} from "../../interfaces/erena/erena-enums";

const ERenaTournamentSchema = new Schema({
  slug: { type: String, lowercase: true, trim: true, index: true },
  tournamentId: { type: String, unique: true, index: true },
  tournamentName: { type: String },
  hideLeaderboard: { type: Boolean, default: false },
  totalTeams: { type: Number },
  totalRounds: { type: Number },
  logoURL: { type: String },
  playersPerTeam: { type: Number },
  currentRound: { type: Number, default: 1 },
  isFinal: { type: Boolean, default: false },
  startDatetime: { type: Date },
  endDatetime: { type: Date },
  brandName: { type: String },
  brandLogoUrl: { type: String },
  logoUrl: { type: String },
  landingPageUrl: { type: String },
  extensionLinkoutUrl: { type: String },
  twitchChannel: { type: String },
  youtubeChannel: { type: String },
  rulesMarkdown: { type: String },
  status: {
    type: String,
    enum: Object.values(ERenaTournamentStatus),
    default: ERenaTournamentStatus.scheduled
  },
  type: {
    type: String,
    default: ERenaType.whiteLabel,
    enum: Object.values(ERenaType)
  },

  // Advertisement Images
  imageUrl1: { type: String },
  imageUrl2: { type: String },
  imageUrl3: { type: String },
  imageUrl4: { type: String },
  imageUrl5: { type: String },
  imageUrl6: { type: String },
  imageLink1: { type: String },
  imageLink2: { type: String },
  imageLink3: { type: String },
  imageLink4: { type: String },
  imageLink5: { type: String },
  imageLink6: { type: String },

  websiteUrl: { type: String },
  organization: { type: String },
  tournamentDescription: { type: String },

  primaryColor: { type: String },
  secondaryColor: { type: String },
  backgroundImageUrl: { type: String },
  extensionPointer: { type: Boolean, default: false },
  extensionNamespace: { type: String, default: "erena" },

  gameSelection: { type: String },
  challongeLeaderboard: { type: String },
  owner: { type: Schema.Types.ObjectId, refPath: "ownerType" },
  ownerType: { type: String, enum: Object.values(ERenaTournamentOwnerKinds) },
  bracketType: {
    type: String,
    enum: Object.values(ERenaScoringKinds),
    default: ERenaScoringKinds.pointRace
  },
  opportunity: { type: Schema.Types.ObjectId, ref: "opportunities" },
  requirements: {
    type: Array,
    of: { type: Schema.Types.ObjectId, ref: "opportunityrequirements" }
  },

  /**
   * @deprecated
   */
  globalStats: {
    teamsRemaining: { type: Number },
    playersRemaining: { type: Number },
    totalKills: { type: Number },
    totalValue: { type: Number },
    statName: { type: String }
  },
  /**
   * @deprecated
   */
  statName: { type: String },
  /**
   * @deprecated
   */
  lastAirtableModificationTime: { type: String },
  // This is the airtable id for the specific tournament record
  /**
   * @deprecated
   */
  airtableId: { type: String },
  // This is the airtable ID for the "Base"
  /**
   * @deprecated
   */
  airtableBaseId: { type: String, index: true }
});

ERenaTournamentSchema.plugin(timestamps);
ERenaTournamentSchema.plugin(mongoosePaginate);

ERenaTournamentSchema.index({
  airtableBaseId: 1,
  tournamentId: 1
});

export type ERenaTournamentModel<T extends Document> = PaginateModel<T>;

export const ERenaTournament: ERenaTournamentModel<IERenaTournament> = <ERenaTournamentModel<IERenaTournament>>(
  model<IERenaTournament>("erenatournaments", ERenaTournamentSchema)
);
