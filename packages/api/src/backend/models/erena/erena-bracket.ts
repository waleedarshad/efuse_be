import { Schema, Document, Model, model } from "mongoose";
import autopopulate from "mongoose-autopopulate";

import { IERenaBracket } from "../../interfaces/erena";
import { timestamps } from "../../helpers/timestamp.helper";

const ERenaBracketSchema = new Schema({
  totalTeams: { type: Number },
  totalRounds: { type: Number },
  totalMatches: { type: Number },
  tournament: {
    type: Schema.Types.ObjectId,
    ref: "erenatournaments",
    index: true
  },
  rounds: [
    {
      round: {
        type: Schema.Types.ObjectId,
        ref: "erenarounds",
        autopopulate: true
      }
    }
  ]
});

ERenaBracketSchema.plugin(autopopulate);
ERenaBracketSchema.plugin(timestamps);

export type ERenaBracketModel<T extends Document> = Model<T>;
export const ERenaBracket: ERenaBracketModel<IERenaBracket> = <ERenaBracketModel<IERenaBracket>>(
  model<IERenaBracket>("erenabrackets", ERenaBracketSchema)
);
