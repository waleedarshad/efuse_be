import { Schema, Document, Model, model } from "mongoose";
import autopopulate from "mongoose-autopopulate";

import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaRound } from "../../interfaces/erena";

const ERenaRoundSchema = new Schema({
  roundDate: { type: Date },
  roundTitle: { type: String },
  roundNumber: { type: Number },
  tournament: {
    type: Schema.Types.ObjectId,
    ref: "erenatournaments",
    index: true
  },
  matches: [
    {
      match: {
        type: Schema.Types.ObjectId,
        ref: "erenamatches",
        autopopulate: true
      }
    }
  ]
});

ERenaRoundSchema.index({ roundNumber: 1, tournament: 1 });

ERenaRoundSchema.plugin(autopopulate);
ERenaRoundSchema.plugin(timestamps);

export type ERenaRoundModel<T extends Document> = Model<T>;
export const ERenaRound: ERenaRoundModel<IERenaRound> = <ERenaRoundModel<IERenaRound>>(
  model<IERenaRound>("erenarounds", ERenaRoundSchema)
);
