import { Document, Schema, Model, model } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaFeaturedVideo } from "../../interfaces/erena";

const schema = new Schema({
  title: { type: String },
  description: { type: String },
  youtubeLink: { type: String },
  order: { type: Number, index: 1 }
});

schema.plugin(timestamps);

export type ERenaFeaturedVideoModel<T extends Document> = Model<T>;

export const ERenaFeaturedVideo: ERenaFeaturedVideoModel<IERenaFeaturedVideo> = <
  ERenaFeaturedVideoModel<IERenaFeaturedVideo>
>model<IERenaFeaturedVideo>("erenaFeaturedVideo", schema);
