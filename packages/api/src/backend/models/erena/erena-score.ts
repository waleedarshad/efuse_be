import { Document, Schema, Model, model } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { ERenaScoreOwnerKinds, ERenaScoringKinds } from "../../interfaces/erena/erena-enums";
import { IERenaScore } from "../../interfaces/erena/erena-score";

const schema = new Schema({
  erenaKind: {
    type: String,
    required: true,
    enum: Object.values(ERenaScoringKinds)
  },
  ownerKind: {
    type: String,
    required: true,
    enum: Object.values(ERenaScoreOwnerKinds)
  },
  ownerId: {
    type: Schema.Types.ObjectId,
    required: true,
    index: true
  },
  match: {
    type: Schema.Types.ObjectId,
    index: true,
    required: false,
    ref: "erenamatches"
  },
  tournament: {
    type: Schema.Types.ObjectId,
    index: true,
    required: true,
    ref: "erenatournaments"
  },
  score: {
    type: Number,
    required: true
  }
});

// enforce one score per player/team per (tournament or match)
schema.index({ erenaKind: 1, ownerId: 1, tournament: 1, match: 1 }, { unique: true });
schema.plugin(timestamps);

export type ERenaScoreModel<T extends Document> = Model<T>;

export const ERenaScore: ERenaScoreModel<IERenaScore> = <ERenaScoreModel<IERenaScore>>(
  model<IERenaScore>("erenascores", schema)
);
