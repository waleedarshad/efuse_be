import { Document, Schema, Model, model } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaFeaturedEvent } from "../../interfaces/erena";

const schema = new Schema({
  tournament: {
    type: Schema.Types.ObjectId,
    index: { unique: true },
    required: true,
    ref: "erenatournaments"
  },
  active: {
    type: Boolean,
    index: true,
    default: false
  }
});

schema.plugin(timestamps);

export type ERenaFeaturedEventModel<T extends Document> = Model<T>;

export const ErenaFeaturedEvent: ERenaFeaturedEventModel<IERenaFeaturedEvent> = <
  ERenaFeaturedEventModel<IERenaFeaturedEvent>
>model<IERenaFeaturedEvent>("erenaFeaturedEvent", schema);
