import { Schema, Document, Model, model } from "mongoose";
import autopopulate from "mongoose-autopopulate";

import { timestamps } from "../../helpers/timestamp.helper";
import { IERenaPlayer } from "../../interfaces/erena";
import { ERenaType } from "../../interfaces/erena/erena-enums";

const ERenaPlayerSchema = new Schema({
  name: { type: String },
  isActive: { type: Boolean },
  team: { type: Schema.Types.ObjectId, ref: "erenateams", index: true },
  tournament: { type: Schema.Types.ObjectId, ref: "erenatournaments", index: true },
  type: {
    type: String,
    default: ERenaType.whiteLabel,
    enum: Object.values(ERenaType)
  },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    index: true,
    autopopulate: true
  },

  /**
   * @deprecated
   */
  stats: {
    kills: { type: Number, default: 0 },
    value: { type: Number, default: 0 },
    name: { type: String, default: "Points" } // Name for the stats to show up e.g. "Points", "Kills"
  },
  /**
   * @deprecated
   */
  airtableId: { type: String },
  // This is the airtable ID for the "Base"
  /**
   * @deprecated
   */
  airtableBaseId: { type: String },
  /**
   * @deprecated
   */
  lastAirtableModificationTime: { type: String }
});

ERenaPlayerSchema.plugin(timestamps);
ERenaPlayerSchema.plugin(autopopulate);

export type ERenaPlayerModel<T extends Document> = Model<T>;

export const ERenaPlayer: ERenaPlayerModel<IERenaPlayer> = <ERenaPlayerModel<IERenaPlayer>>(
  model<IERenaPlayer>("erenaplayers", ERenaPlayerSchema)
);
