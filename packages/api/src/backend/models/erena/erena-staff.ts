import { Schema, Document, Model, model } from "mongoose";
import autopopulate from "mongoose-autopopulate";

import { timestamps } from "../../helpers/timestamp.helper";
import WhitelistHelper from "../../helpers/whitelist_helper";
import { ERenaRoles } from "../../interfaces/erena/erena-enums";
import { IERenaStaff } from "../../interfaces/erena/erena-staff";

const ERenaStaffSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate,
    index: true
  },
  tournament: { type: Schema.Types.ObjectId, ref: "erenatournaments", index: true },
  role: {
    type: String,
    enum: Object.values(ERenaRoles),
    index: true
  }
});

// limit to one instance per user per role per tournament
// I.E. don't allow the same user to have two entries of "administrator" for the same tournament
ERenaStaffSchema.index({ user: 1, tournament: 1, role: 1 }, { unique: true });

ERenaStaffSchema.plugin(timestamps);
ERenaStaffSchema.plugin(autopopulate);

export type ERenaStaffModel<T extends Document> = Model<T>;

export const ERenaStaff: ERenaStaffModel<IERenaStaff> = <ERenaStaffModel<IERenaStaff>>(
  model<IERenaStaff>("erenastaff", ERenaStaffSchema)
);
