const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const mongoose = require("mongoose");

const { Schema } = mongoose;

const twitterUtils = require("../../lib/creatortools/utils/twitter");

const SHOULD_RETRY_ERRORS = {
  88: {
    message: "Rate limit exceeded",
    description: "The request limit for this resource has been reached for the current rate limit window."
  },
  130: {
    message: "Over capacity",
    description: "Corresponds with HTTP 503. Twitter is temporarily over capacity."
  },
  185: {
    message: "User is over daily status update limit",
    description:
      "Corresponds with HTTP 403. Thrown when a Tweet cannot be posted due to the user having no allowance remaining to post. Despite the text in the error message indicating that this error is only thrown when a daily limit is reached, this error will be thrown whenever a posting limitation has been reached. Posting allowances have roaming windows of time of unspecified duration."
  },
  324: {
    message: "The validation of media ids failed.",
    description: "Corresponds with HTTP 400. There was a problem with the media ID submitted with the Tweet."
  },
  325: {
    message: "A media id was not found.",
    description: "Corresponds with HTTP 400. The media ID attached to the Tweet was not found."
  }
};

const SHOULD_NOT_RETRY_ERRORS = {
  17: {
    message: "No user matches for specified terms.",
    description:
      "Corresponds with HTTP 404. It was not possible to find a user profile matching the parameters specified.",
    shouldUnlink: true
  },
  32: {
    message: "Could not authenticate you",
    description: "Corresponds with HTTP 401. There was an issue with the authentication data for the request.",
    shouldUnlink: true
  },
  50: {
    message: "User not found.",
    description: "Corresponds with HTTP 404. The user is not found.",
    shouldUnlink: true
  },
  63: {
    message: "User has been suspended.",
    description: "Corresponds with HTTP 403 The user account has been suspended and information cannot be retrieved.",
    shouldUnlink: true
  },
  64: {
    message: "Your account is suspended and is not permitted to access this feature",
    description: "Corresponds with HTTP 403. The access token being used belongs to a suspended user.",
    shouldUnlink: true
  },
  89: {
    message: "Invalid or expired token",
    description: "The access token used in the request is incorrect or has expired.",
    shouldUnlink: true
  },
  99: {
    message: "Unable to verify your credentials.",
    description:
      "Corresponds with HTTP 403. The OAuth credentials cannot be validated. Check that the token is still valid.",
    shouldUnlink: true
  },
  187: {
    message: "Status is a duplicate.",
    description: "The status text has already been Tweeted by the authenticated account.",
    shouldUnlink: false
  },
  326: {
    message: "To protect our users from spam and other malicious activity, this account is temporarily locked.",
    description:
      "Corresponds with HTTP 403. The user should log in to https://twitter.com to unlock their account before the user token can be used.",
    shouldUnlink: true
  },
  354: {
    message: "The text of your direct message is over the max character limit.",
    description:
      "Corresponds with HTTP 403. The message size exceeds the number of characters permitted in a Direct Message.",
    shouldUnlink: false
  },
  407: {
    message: "The given URL is invalid.",
    description:
      "Corresponds with HTTP 400. A URL included in the Tweet could not be handled. This may be because a non-ASCII URL could not be converted, or for other reasons.",
    shouldUnlink: false
  }
};

const twitterCrossPostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true
  },
  createdAt: { type: Date, default: Date.now },
  sentAt: { type: Date },
  content: { type: String, required: true },
  media: { type: Schema.Types.ObjectId, ref: "media" },
  checksum: { type: String, required: true },
  errorCode: { type: Number },
  errorMessage: { type: String },
  retryCount: { type: Number }
});

twitterCrossPostSchema.index({ user: 1, createdAt: 1 });
twitterCrossPostSchema.index({ checksum: 1, createdAt: 1 });
twitterCrossPostSchema.index({ errorCode: 1, retryCount: 1 });

twitterCrossPostSchema.methods.markSuccess = async (cb) => {
  this.sentAt = new Date();
  // todo: investigate
  // @ts-ignore
  this.markModified("sentAt");
  // todo: investigate
  // @ts-ignore
  this.errorCode = undefined;
  // todo: investigate
  // @ts-ignore
  this.errorMessage = undefined;
  // todo: investigate
  // @ts-ignore
  return await this.save(cb);
};

twitterCrossPostSchema.methods.markFailed = async (errors, cb) => {
  // todo: investigate
  // @ts-ignore
  this.errorCode = errors[0].code;
  // todo: investigate
  // @ts-ignore
  this.errorMessage = errors[0].message;

  if (SHOULD_NOT_RETRY_ERRORS.hasOwnProperty(this.errorCode)) {
    // todo: investigate
    // @ts-ignore
    this.retryCount = 99;

    if (SHOULD_NOT_RETRY_ERRORS[this.errorCode].shouldUnlink) {
      logger.warn("Unlinking Twitter account due to error response.", {
        // todo: investigate
        // @ts-ignore
        user: this.user,
        code: this.errorCode,
        message: this.errorMessage
      });

      // todo: investigate
      twitterUtils
        // @ts-ignore
        .unlinkTwitterAccount(this.user)
        .catch((error) => logger.error(error, "Error resolving unlinkTwitterAccount promise"));
    }
  } else if (this.retryCount === undefined || this.retryCount === null) {
    // todo: investigate
    // @ts-ignore
    this.retryCount = 0;
  } else {
    this.retryCount++;
  }

  // todo: investigate
  // @ts-ignore
  return this.save(cb);
};

const TwitterCrossPost = mongoose.model("twitterCrossPost", twitterCrossPostSchema);

module.exports.TwitterCrossPost = TwitterCrossPost;
