import { Schema, Document, Model, model } from "mongoose";
import { InviteCodeType, InviteResourceType } from "@efuse/entities";
import { timestamps } from "../helpers/timestamp.helper";
import { IInviteCode } from "../interfaces";

const inviteCodeSchema = new Schema({
  code: { type: String, required: true, unique: true },
  type: {
    type: String,
    required: true,
    enum: Object.values(InviteCodeType),
    index: true
  },
  user: { type: Schema.Types.ObjectId, ref: "user" },
  organization: { type: Schema.Types.ObjectId, ref: "organization" },
  active: { type: Boolean, default: true },
  resource: { type: Schema.Types.ObjectId },
  resourceType: {
    type: String,
    enum: Object.values(InviteResourceType),
    index: true
  }
});

/**
 * Indexes
 */
inviteCodeSchema.index({ organization: 1, active: -1 });

/**
 * Plugins
 */
inviteCodeSchema.plugin(timestamps);

export type InviteCodeModel<T extends Document> = Model<T>;
export const InviteCode: InviteCodeModel<IInviteCode> = <InviteCodeModel<IInviteCode>>(
  model("inviteCodes", inviteCodeSchema)
);
