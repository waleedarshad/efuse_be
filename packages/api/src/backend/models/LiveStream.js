const mongoose = require("mongoose");

const { timestamps } = require("../helpers/timestamp.helper");

const { Schema } = mongoose;

const schema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users" },
  platform: {
    type: String,
    default: "twitch",
    enum: ["twitch"]
  },
  startedAt: {
    type: Date,
    default: new Date()
  },
  endedAt: {
    type: Date
  },
  streamTitle: {
    type: String
  },
  viewers: {
    type: Number
  },
  views: {
    type: Number
  },
  thumbnailUrl: {
    type: String
  },
  twitchUserAvatar: {
    type: String
  },
  game: {
    id: { type: Number },
    name: { type: String },
    box_art_url: { type: String }
  }
});

schema.index({
  endedAt: -1
});

schema.plugin(timestamps);

const Livestream = mongoose.model("livestream", schema);

module.exports.Livestream = Livestream;
