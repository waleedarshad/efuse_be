import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";

@gqlObjectType("TwitchGame", { description: "The TwitchGame Model" })
export class TwitchGame {
  @gqlField((_type) => ID)
  id!: string;

  @gqlField((_type) => String)
  name!: string;

  @gqlField((_type) => String)
  box_art_url!: string;
}
