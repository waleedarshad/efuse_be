const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const mongoose = require("mongoose");

const { Schema } = mongoose;

const SHOULD_NOT_RETRY_ERRORS = {};

const discordCrossPostSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true
  },
  createdAt: { type: Date, default: Date.now },
  sentAt: { type: Date },
  content: { type: String, required: true },
  media: { type: Schema.Types.ObjectId, ref: "media" },
  mediaObjects: [{ type: Schema.Types.ObjectId, ref: "media" }],
  checksum: { type: String, required: true },
  errorCode: { type: Number },
  errorMessage: { type: String },
  retryCount: { type: Number }
});

discordCrossPostSchema.index({ user: 1, createdAt: 1 });
discordCrossPostSchema.index({ checksum: 1, createdAt: 1 });
discordCrossPostSchema.index({ errorCode: 1, retryCount: 1 });

discordCrossPostSchema.methods.markSuccess = async function (cb) {
  this.sentAt = new Date();

  // todo: investigate
  // @ts-ignore
  this.markModified("sentAt");
  this.errorCode = undefined;
  this.errorMessage = undefined;

  // todo: investigate
  // @ts-ignore
  const result = await this.save(cb);
  return result;
};

discordCrossPostSchema.methods.markFailed = async function (error, cb) {
  this.errorCode = error.code;
  this.errorMessage = error.message;
  if (SHOULD_NOT_RETRY_ERRORS.hasOwnProperty(this.errorCode)) {
    this.retryCount = 99;

    if (SHOULD_NOT_RETRY_ERRORS[this.errorCode].shouldUnlink) {
      logger.warn(
        {
          // todo: investigate
          // @ts-ignore
          user: this.user,
          code: this.errorCode,
          message: this.errorMessage
        },
        "Unlinking Discord account due to error response."
      );
      // TODO: Unlink discord account
    }
  } else if (this.retryCount === undefined || this.retryCount === null) {
    this.retryCount = 0;
  } else {
    // todo: investigate
    // @ts-ignore
    this.retryCount += 1;
  }
  // todo: investigate
  // @ts-ignore
  const result = await this.save(cb);
  return result;
};

const DiscordCrossPost = mongoose.model("discordCrossPost", discordCrossPostSchema);

module.exports.DiscordCrossPost = DiscordCrossPost;
