import { Schema, Document, PaginateModel, model } from "mongoose";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import SchemaHelper from "../helpers/schema_helper";
import { timestamps } from "../helpers/timestamp.helper";
import { ILearningArticle } from "../interfaces";
import {
  LearningArticleStatusEnum,
  LearningArticleAuthorTypeEnum
} from "../../lib/learningArticles/learning-article.enums";
const learningArticlesSchema = new Schema({
  slug: { type: String, unique: true },
  title: { type: String },
  body: { type: String },
  status: {
    type: String,
    enum: Object.values(LearningArticleStatusEnum),
    default: LearningArticleStatusEnum.DRAFT
  },
  author: { type: Schema.Types.ObjectId, refPath: "authorType" },
  authorType: { type: String, enum: Object.values(LearningArticleAuthorTypeEnum) },
  category: { type: Schema.Types.ObjectId, ref: "learningarticlecategories" },
  summary: { type: String },
  image: SchemaHelper.fileSchemaDefaultsNoImage,
  video: {
    filename: {
      type: String
    },
    contentType: {
      type: String
    },
    url: {
      type: String
    }
  },
  isActive: { type: Boolean, default: true },
  promoted: { type: Boolean, default: false },
  traits: [{ type: String }],
  motivations: [{ type: String }],
  publishDate: { type: Date }
});

learningArticlesSchema.plugin(timestamps);
learningArticlesSchema.plugin(mongoosePaginate);

export type LearningArticleModel<T extends Document> = PaginateModel<T>;

export const LearningArticle: LearningArticleModel<ILearningArticle> = <LearningArticleModel<ILearningArticle>>(
  model<ILearningArticle>("learningarticles", learningArticlesSchema)
);
