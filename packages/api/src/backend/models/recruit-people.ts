import * as mongoose from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IRecruitPeople } from "../interfaces/recruit-people";

const { Schema } = mongoose;

const recruitPeopleSchema = new Schema({
  recruitmentProfile: { type: Schema.Types.ObjectId, ref: "recruitmentprofiles", index: true, required: true },
  name: { type: String, required: true },
  type: { type: String, enum: ["Parent", "Coach", "Mentor", "Guardian", "Family Member"], required: true },
  phone: { type: String },
  email: { type: String }
});

recruitPeopleSchema.plugin(timestamps);

export type RecruitPeopleModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;

export const RecruitPeople: RecruitPeopleModel<IRecruitPeople> = <RecruitPeopleModel<IRecruitPeople>>(
  mongoose.model<IRecruitPeople>("recruitpeoples", recruitPeopleSchema)
);
