const mongoose = require("mongoose");

const { Schema } = mongoose;
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");
const { timestamps } = require("../helpers/timestamp.helper");

const steamOwnedGameSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  steam: {
    type: Schema.Types.ObjectId,
    ref: "steams",
    required: true,
    index: true
  },
  appid: {
    type: Number,
    index: true
  },
  name: {
    type: String
  },
  playtimeForever: {
    type: Number
  },
  imgIconUrl: {
    type: String
  },
  imgLogoUrl: {
    type: String
  },
  hasCommunityVisibleStats: {
    type: Boolean
  },
  playtimeWindowsForever: {
    type: Number
  },
  playtimeMacForever: {
    type: Number
  },
  playtimeLinuxForever: {
    type: Number
  }
});

steamOwnedGameSchema.plugin(mongoosePaginate);
steamOwnedGameSchema.plugin(timestamps);

const SteamOwnedGame = mongoose.model("steamOwnedGames", steamOwnedGameSchema);

module.exports.SteamOwnedGame = SteamOwnedGame;
