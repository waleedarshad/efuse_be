import * as mongoose from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";

import { IRank } from "../interfaces/rank";

const { Schema } = mongoose;

const rankSchema = new Schema({
  document: { type: Schema.Types.ObjectId, refPath: "documentType", index: true },
  documentType: { type: String, enum: ["feeds", "organizations", "opportunities"], index: true },
  rankValue: { type: Number, index: true }
});

rankSchema.index({ document: 1, documentType: 1 });

rankSchema.plugin(timestamps);
rankSchema.plugin(mongoosePaginate);

export type RankModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;

export const Rank: RankModel<IRank> = <RankModel<IRank>>mongoose.model<IRank>("ranks", rankSchema);
