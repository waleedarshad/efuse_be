import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { ITrackCron } from "../interfaces";
import { CronEntityKind, CronStatusEnum } from "../../lib/track-cron/track-cron.enum";

const trackCronSchema = new Schema({
  kind: { type: String, index: true },
  entity: { type: Schema.Types.ObjectId, refPath: "entityKind", index: true },
  entityKind: { type: String, index: true, enum: Object.values(CronEntityKind) },
  method: { type: String, index: true },
  lastRanAt: { type: Date, index: true },
  lastProcessedAt: { type: Date },
  lastStatus: { type: String, enum: Object.values(CronStatusEnum), index: true },
  errorMessage: { type: String },
  lastPaginationCursor: { type: String }
});

trackCronSchema.index({ kind: 1, method: 1, lastRanAt: 1 });
trackCronSchema.index({ entity: 1, entityKind: 1, kind: 1, method: 1 });
trackCronSchema.index({ entity: 1, entityKind: 1, kind: 1 });
trackCronSchema.index({ entity: 1, entityKind: 1 });

trackCronSchema.plugin(timestamps);

export type TrackCronModel<T extends Document> = Model<T>;
export const TrackCron: TrackCronModel<ITrackCron> = <TrackCronModel<ITrackCron>>model("trackcrons", trackCronSchema);
