import { model, Types } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";

import { timestamps } from "../helpers/timestamp.helper";

@gqlObjectType("LandingPage", { description: "The LandingPage Model" })
@plugin(timestamps)
export class LandingPage {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId;

  @gqlField((_type) => String)
  @prop({ type: () => String, index: true, unique: true, required: true })
  slug!: string;

  @gqlField((_type) => String)
  @prop({ type: () => String })
  title!: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  subtitle?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  mainImage?: string;

  @gqlField((_type) => [String], { nullable: true })
  @prop({ type: () => String })
  secondaryImages?: string[];

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  buttonCallToAction?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  body?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  purpose?: string;

  @gqlField((_type) => String, { nullable: true })
  @prop({ type: () => String })
  modalTitle?: string;

  @gqlField((_type) => Boolean)
  @prop({ _id: false, default: true, index: true })
  active!: boolean;

  @gqlField({ nullable: true })
  createdAt?: Date;

  @gqlField({ nullable: true })
  updatedAt?: Date;
}

const landingPageSchema = buildSchema(LandingPage);

export const LandingPageModel = addModelToTypegoose(model("landingpages", landingPageSchema), LandingPage);
