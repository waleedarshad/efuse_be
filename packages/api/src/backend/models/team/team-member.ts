import { Schema, Document, model, PaginateModel, Types } from "mongoose";

import { TeamMemberStatus } from "@efuse/entities";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ITeamMember } from "../../interfaces/team";

const schema = new Schema({
  team: {
    type: Types.ObjectId,
    ref: "teams",
    required: true
  },
  user: {
    type: Types.ObjectId,
    ref: "users",
    required: true
  },
  role: {
    type: String
  },
  status: {
    type: String,
    enum: Object.values(TeamMemberStatus),
    required: true
  }
});

schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

schema.index({ team: 1, user: 1 });
schema.index({ team: 1, status: 1 });

export type TeamMemberModel<T extends Document> = PaginateModel<T>;
export const TeamMember: TeamMemberModel<ITeamMember> = <TeamMemberModel<ITeamMember>>(
  model<ITeamMember>("teammembers", schema)
);
