import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import { OwnerType } from "@efuse/entities";
import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import SchemaHelper from "../../helpers/schema_helper";
import { ITeam } from "../../interfaces/team";

const schema = new Schema({
  name: {
    type: String,
    required: true
  },
  game: {
    type: Schema.Types.ObjectId,
    ref: "games",
    required: true
  },
  description: {
    type: String
  },
  image: SchemaHelper.fileSchemaDefaultsNoImage,
  ownerType: { type: String, enum: Object.values(OwnerType), required: true },
  owner: { type: Schema.Types.ObjectId, required: true }
});

schema.plugin(mongooseAggregate);
schema.plugin(timestamps);
schema.plugin(mongoosePaginate);

schema.index({ owner: 1, ownerType: 1 });

export type TeamModel<T extends Document> = PaginateModel<T>;
export const Team: TeamModel<ITeam> = <TeamModel<ITeam>>model<ITeam>("teams", schema);
