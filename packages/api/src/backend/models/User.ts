import * as mongoose from "mongoose";

import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongooseDelete from "mongoose-delete";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import portfolioProgress from "../../lib/mongoose-plugins/portfolio-progress";
import revokeSession from "../../lib/mongoose-plugins/revoke-session";
import SchemaHelper from "../helpers/schema_helper";
import { timestamps } from "../helpers/timestamp.helper";
import { CdnHelper } from "../helpers/cdn.helper";
import { IUser } from "../interfaces/user";

const { Schema } = mongoose;

// Pull in model references
import "./league-of-legends/LeagueOfLegendsStat";
import "./Organization";
import "./pathway";
import "./Pubg";
import "./Steam";
import "./Twitch";

const schema = new Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true, unique: true, select: false },
    password: { type: String, required: true, select: false },
    dateOfBirth: { type: Date, required: true, select: false },
    address: { type: String, required: true, select: false },
    zipCode: { type: String, required: true, select: false },
    gender: { type: String, required: true, select: false },
    traits: [{ type: String, select: false }],
    motivations: [{ type: String, select: false }],
    // professional fields
    title: { type: String, required: true },
    organizations: [{ type: Schema.Types.ObjectId, ref: "organizations" }],
    mainOrganization: { type: Schema.Types.ObjectId, ref: "organizations", autopopulate: true },
    interest: { type: String, required: true },
    // student fields
    phoneNumber: { type: String, required: true, select: false },
    school: { type: String, required: true },
    educationLevel: { type: String, required: true },
    graduationYear: { type: String },
    graduationMonth: { type: String },
    gpa: { type: String },
    major: { type: String },
    act: { type: String },
    sat: { type: String },
    gpaScale: { type: String },
    bio: { type: String },
    firstRun: { type: Boolean, default: true },
    // file fields
    profilePicture: SchemaHelper.fileSchema,
    headerImage: SchemaHelper.fileSchemaDefaultsNoImage,
    resume: { filename: { type: String }, contentType: { type: String }, url: { type: String } },
    // roles
    roles: [{ type: String }],
    locale: { type: String, default: "en", select: false },
    status: { type: String, default: "Active", enum: ["Active", "Blocked"], select: false, index: true },
    lastLoginAt: { type: Date, select: false },
    // web notification settings
    applyNotification: { type: Boolean, default: true, select: false },
    chatNotification: { type: Boolean, default: true, select: false },
    commentsNotification: { type: Boolean, default: true, select: false },
    followsNotification: { type: Boolean, default: true, select: false },
    hypedPostCommentNotification: { type: Boolean, default: true, select: false },
    likeNotification: { type: Boolean, default: true, select: false },
    mentionsNotification: { type: Boolean, default: true, select: false },
    newsOrganizationNotification: { type: Boolean, default: true, select: false },
    opportunityOrganizationNotification: { type: Boolean, default: true, select: false },
    organizationInvitationNotification: { type: Boolean, default: true, select: false },
    postOrganizationNotification: { type: Boolean, default: true, select: false },
    repliesNotification: { type: Boolean, default: true, select: false },
    sharesNotification: { type: Boolean, default: true, select: false },
    showOnline: { type: Boolean, default: true, select: false },
    // mobile push notification settings
    applyNotificationMobile: { type: Boolean, default: true, select: false },
    chatNotificationMobile: { type: Boolean, default: true, select: false },
    commentsNotificationMobile: { type: Boolean, default: true, select: false },
    followsNotificationMobile: { type: Boolean, default: true, select: false },
    hypedPostCommentNotificationMobile: { type: Boolean, default: true, select: false },
    likeNotificationMobile: { type: Boolean, default: true, select: false },
    mentionsNotificationMobile: { type: Boolean, default: true, select: false },
    newsOrganizationNotificationMobile: { type: Boolean, default: true, select: false },
    opportunityOrganizationNotificationMobile: { type: Boolean, default: true, select: false },
    organizationInvitationNotificationMobile: { type: Boolean, default: true, select: false },
    postOrganizationNotificationMobile: { type: Boolean, default: true, select: false },
    repliesNotificationMobile: { type: Boolean, default: true, select: false },
    sharesNotificationMobile: { type: Boolean, default: true, select: false },
    showOnlineMobile: { type: Boolean, default: true, select: false },
    // end notification settings
    battlenetId: { type: String },
    battlenetVerified: { type: Boolean, default: false },
    blockedUsers: [{ type: Schema.Types.ObjectId, ref: "users", select: false }],
    businessExperience: [
      {
        title: { type: String },
        subTitle: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        present: { type: Boolean, default: true },
        image: SchemaHelper.fileSchemaDefaultsBriefcase,
        description: { type: String }
      }
    ],
    businessSkills: [{ value: { type: Number }, order: { type: Number } }],
    componentLayout: [{ name: { type: String } }],
    deleteAccountRequest: { type: Boolean, default: false, select: false },
    deleteAccountRequestDate: { type: Date, select: false },
    discordAccessToken: { type: String, select: false },
    discordRefreshToken: { type: String, select: false },
    discordServer: { type: String },
    discordTokenExpiresAt: { type: Date, select: false },
    discordTokenScope: { type: String, select: false },
    discordTokenType: { type: String, select: false },
    discordUserId: { type: String },
    discordUserName: { type: String },
    discordUserUniqueId: { type: String },
    discordVerified: { type: Boolean, default: false, index: true },
    discordWebhooks: [{ name: { type: String }, url: { type: String } }],
    educationExperience: [
      {
        level: { type: Number },
        subTitle: { type: String },
        school: { type: String },
        degree: { type: String },
        fieldofstudy: { type: String },
        grade: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        present: { type: Boolean, default: true },
        image: SchemaHelper.fileSchemaDefaultsBriefcase,
        description: { type: String }
      }
    ],
    emailVerificationConfirmedAt: { type: Date, select: false },
    emailVerificationExpires: { type: Date, select: false, index: true },
    emailVerificationToken: { type: String, select: false, index: true },
    embeddedLink: { type: String },
    esportsExperience: [
      {
        title: { type: String },
        subTitle: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        present: { type: Boolean, default: true },
        description: { type: String },
        image: SchemaHelper.fileSchemaDefaultsBriefcase
      }
    ],
    facebookAccessToken: { type: String, select: false },
    facebookAccessTokenExpiresAt: { type: Date, select: false },
    facebookID: { type: String, select: false },
    facebookLink: { type: String },
    facebookPages: [
      { pageId: { type: String }, pageName: { type: String }, accessToken: { type: String, select: false } }
    ],
    facebookTokenType: { type: String, select: false },
    facebookVerified: { type: Boolean, default: false, index: true },
    gamingSkills: [{ value: { type: Number }, order: { type: Number } }],
    goldSubscriber: { type: Boolean, default: false },
    goldSubscriberSince: { type: Date },
    /** @deprecated use GoogleAccountInfo */
    google: {
      id: { type: String, select: false },
      name: { type: String },
      profile: { type: String },
      picture: { type: String },
      gender: { type: String },
      locale: { type: String }
    },
    /** @deprecated use GoogleAccountInfo */
    googleVerified: { type: Boolean, default: false, index: true },
    instagramLink: { type: String },
    instagramUsername: { type: String },
    lastUsernameUpdatedAt: { type: Date, select: false },
    leagueOfLegends: {
      riotNickname: { type: String },
      riotRegion: { type: String },
      verified: { type: Boolean, index: true },
      riotId: { type: String },
      verificationToken: { type: String, select: false },
      stats: { type: Schema.Types.ObjectId, ref: "leagueoflegendsstats", autopopulate: true, index: true }
    },
    overwatch: {
      nickname: { type: String },
      platform: { type: String },
      stats: { type: Schema.Types.ObjectId, ref: "overwatchstats", autopopulate: true, index: true }
    },
    linkedin: {
      id: { type: String, select: false },
      displayName: { type: String },
      name: { givenName: { type: String }, familyName: { type: String } },
      photos: [{ type: String }]
    },
    linkedinLink: { type: String },
    linkedinUsername: { type: String },
    linkedinVerified: { type: Boolean, default: false, index: true },
    migratedToCognito: { type: Boolean, default: false, select: false },
    og: { number: { type: Number }, show: { type: Boolean, default: false } },
    online: { type: Boolean, default: false },
    passwordMigrated: { type: Boolean, default: false, select: false },
    passwordMigratedAt: { type: Date, select: false },
    portfolioEvents: [
      { title: { type: String }, subTitle: { type: String }, eventDate: { type: Date }, description: { type: String } }
    ],
    portfolioHonors: [{ title: { type: String }, subTitle: { type: String }, description: { type: String } }],
    portfolioProgress: { type: Number, default: 0 },
    portfolioProgressUpdatedAt: { type: Date, default: Date.now },
    pubg: {
      platforms: [{ username: { type: String }, platform: { type: String }, accountId: { type: String } }],
      stats: [{ type: Schema.Types.ObjectId, ref: "pubgstats", autopopulate: true }]
    },
    publicTraitsMotivations: { type: Boolean, default: false },
    referral: { type: String, select: false }, // referral / invite code used by the user to signup
    referralUrl: { type: String, select: false },
    resetPasswordExpires: { type: Date, select: false, index: true },
    resetPasswordToken: { type: String, select: false, index: true },
    rocketleague: {
      platforms: [{ username: { type: String }, platform: { type: String } }],
      stats: [
        {
          trnScore: { type: Number },
          goalShotRatio: { type: Number },
          wins: { type: Number },
          goals: { type: Number },
          saves: { type: Number },
          mvps: { type: Number },
          assists: { type: Number },
          mvpWinsRatio: { type: Number },
          platform: { type: String },
          shots: { type: Number },
          ranks: [
            {
              season: { type: String },
              ranks: [
                {
                  title: { type: String },
                  subTitle: { type: String },
                  ratingScore: { type: Number },
                  ratingPercent: { type: String },
                  games: { type: Number },
                  winStreak: { type: Number }
                }
              ]
            }
          ]
        }
      ]
    },
    signupSocial: { type: String, select: false },
    signupUrl: { type: String, select: false },
    slug: { type: String, index: true },
    snapchat: {
      id: { type: String },
      displayName: { type: String },
      bitmoji: { avatarId: { type: String }, avatarUrl: { type: String } }
    },
    snapchatVerified: { type: Boolean, default: false },
    socialExperience: [
      {
        platform: { type: String },
        headline: { type: String },
        username: { type: String },
        startDate: { type: Date },
        endDate: { type: Date },
        present: { type: Boolean, default: true },
        description: { type: String }
      }
    ],
    stats: { type: Schema.Types.ObjectId, ref: "userstats", autopopulate: true },
    steam: { type: Schema.Types.ObjectId, ref: "steams", autopopulate: true },
    steamId: { type: String },
    steamUsername: { type: String },
    steamVerified: { type: Boolean, default: false, index: true },
    streamChatToken: { type: String, required: true, select: false },
    tikTok: {
      tiktokUsername: { type: String },
      tiktokFollowers: { type: Number },
      tiktokVerified: { type: Boolean },
      createdAt: { type: Date }
    },
    tiktokLink: { type: String },

    /** @deprecated use TwitchACcountInfo */
    twitch: { type: Schema.Types.ObjectId, ref: "twitches", autopopulate: true },

    /** @deprecated use TwitchACccountInfo */
    twitchDisplayName: { type: String },
    /** @deprecated use TwitchACccountInfo */
    twitchFollowersCount: { type: Number },
    /** @deprecated use TwitchAccountInfo */
    twitchID: { type: String, index: true },
    /** @deprecated use TwitchAccountInfo */
    twitchLink: { type: String },
    /** @deprecated use TwitchAccountInfo */
    twitchProfileImage: { type: String },
    /** @deprecated use TwitchAccountInfo */
    twitchUserName: { type: String },
    /** @deprecated use TwitchAccountInfo */
    twitchVerified: { type: Boolean, default: false, index: true },
    /** @deprecated use TwitchAccountInfo */
    twitchEmail: { type: String, select: false },

    /** @deprecated use OAuthCredential */
    twitchAccessToken: { type: String, select: false },
    /** @deprecated use OAuthCredential */
    twitchAccessTokenExpiresAt: { type: Date, select: false },
    /** @deprecated use OAuthCredential */
    twitchRefreshToken: { type: String, select: false },
    /** @deprecated use OAuthCredential */
    twitchScope: [{ type: String }],
    /** @deprecated use OAuthCredential */
    twitchTokenType: { type: String, select: false },

    /** @deprecated use TwitterAccountInfo */
    twitterfollowersCount: { type: Number },
    /** @deprecated use TwitterAccountInfo */
    twitterId: { type: String, select: false },
    /** @deprecated use TwitterAccountInfo */
    twitterLink: { type: String },
    /** @deprecated use TwitterAccountInfo */
    twitterUsername: { type: String, index: true },
    /** @deprecated use TwitterAccountInfo */
    twitterVerified: { type: Boolean, default: false, index: true },

    /** @deprecated use OAuthCredential */
    twitterOauthToken: { type: String, select: false },
    /** @deprecated use OAuthCredential */
    twitterOauthTokenSecret: { type: String, select: false },

    userAgent: { type: String, select: false }, // browser used by user to get signed up
    username: { type: String, required: true, unique: true, index: true },
    userSignupDevice: { type: String, select: false },
    verified: { type: Boolean, default: false, index: true },
    verifyAccountRequest: { type: Boolean, default: false, select: false },
    verifyAccountRequestDate: { type: Date, select: false },
    weeklyNewsLetter: { type: Boolean, default: false },
    xboxGamertag: { type: String },
    xboxUserId: { type: String, index: true },
    xboxVerified: { type: Boolean, default: false, index: true },
    youtube: {
      youtubeChannelId: { type: String },
      youtubeFollowers: { type: Number },
      youtubeVerified: { type: Boolean },
      youtubeUsername: { type: String },
      createdAt: { type: Date }
    },
    youtubeChannelId: { type: String },
    youtubeChannel: { type: Schema.Types.ObjectId, ref: "youtubechannels" },
    pathway: { type: Schema.Types.ObjectId, ref: "pathways" },
    name: { type: String }
  },
  { collation: { locale: "en_US", strength: 2 } }
);

// indexes
schema.index({ _id: 1, shared: 1, deleted: 1 });
schema.index({ apexLegendsStat: 1, "apexLegendsStat.platforms": 1 });
schema.index({ createdAt: -1 });
schema.index({ discordUserId: 1 }, { collation: { locale: "en_US", strength: 2 } });
schema.index({ email: 1, verified: -1 });
schema.index({ firstName: "text", lastName: "text", email: "text", username: "text" });
schema.index({ firstName: 1, verified: -1 });
schema.index({ lastName: 1, verified: -1 });
schema.index({ resetPasswordToken: 1 }, { collation: { locale: "en_US", strength: 2 } });
schema.index({ steamId: 1 }, { collation: { locale: "en_US", strength: 2 } });
schema.index({ email: 1, deleted: 1 });
schema.index({ username: 1, deleted: 1 });

// plugins
schema.plugin(CdnHelper.cdn, ["profilePicture", "headerImage"]);
schema.plugin(mongooseAggregate);
schema.plugin(mongoosePaginate);

schema.plugin(portfolioProgress);

schema.plugin(revokeSession);
schema.plugin(timestamps);
schema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate"],
  indexFields: ["deleted", "deletedBy"]
});

export type UserModel<T extends mongoose.Document> = mongoose.PaginateModel<T> & mongooseDelete.SoftDeleteModel<T>;

export const User: UserModel<IUser> = <UserModel<IUser>>mongoose.model<IUser>("users", schema);
