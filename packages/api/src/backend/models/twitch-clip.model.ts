import { Types, model } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { timestamps } from "../helpers/timestamp.helper";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";

class HostedFile {
  @prop()
  url?: string;

  @prop()
  contentType?: string;

  @prop()
  filename?: string;

  @prop()
  originalUrl?: string;
}

// TODO: there's got a be a better way to extend type properties from plugins.
// just adding them manually here like the typegoose docs do.
@plugin(timestamps)
@plugin(mongoosePaginate)
export class TwitchClip {
  readonly _id!: string | Types.ObjectId;

  @prop({ index: true })
  clip_id?: string;

  @prop()
  url?: string;

  @prop()
  embed_url?: string;

  @prop()
  broadcaster_id?: string;

  @prop()
  broadcaster_name?: string;

  @prop()
  creator_id?: string;

  @prop()
  creator_name?: string;

  @prop()
  video_id?: string;

  @prop()
  game_id?: string;

  @prop()
  language?: string;

  @prop()
  title?: string;

  @prop()
  view_count?: string;

  @prop()
  created_at?: Date;

  @prop()
  updated_at?: Date;

  @prop()
  thumbnail_url!: string;

  @prop({ ref: "users", index: true })
  user?: Types.ObjectId;

  @prop({ default: Date.now })
  storedInDb!: Date;

  @prop()
  jobRanAt?: Date;

  @prop({ default: false })
  hosted!: boolean;

  @prop()
  hostedFile?: HostedFile;

  @prop()
  hostedAt?: Date;

  @prop({ enum: ["SUCCESS", "ERROR"] })
  hostingResult?: string;

  @prop()
  hostingError?: string;

  static paginate: (query: any, options: any, callback?: any) => Promise<any>;
}

const twitchClipSchema = buildSchema(TwitchClip);

export const TwitchClipModel = addModelToTypegoose(model("twitchclips", twitchClipSchema), TwitchClip);
