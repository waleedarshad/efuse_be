import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IUserStats } from "../interfaces/user-stats";

const schema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  hypeScore: { type: Number, index: true },
  hypeScoreUpdatedAt: { type: Date },
  hypeScoreRanking: { type: Number, default: 5 },
  hypeScorePercentile: { type: Number, default: 10 },
  postViews: { type: Number, index: true },
  totalEngagements: { type: Number, index: true },
  totalHypes: { type: Number, index: true }
});

schema.plugin(timestamps);

schema.index({ user: 1, hypeScore: 1, hypeScoreUpdatedAt: 1 });

export type UserStatsModel<T extends Document> = Model<T>;

export const UserStats: UserStatsModel<IUserStats> = <UserStatsModel<IUserStats>>model("userstats", schema);
