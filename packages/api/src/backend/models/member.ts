import { Document, Schema, model, PaginateModel, AggregatePaginateModel } from "mongoose";
import aggregatePaginate from "mongoose-aggregate-paginate-v2";
import autopopulate from "mongoose-autopopulate";
import mongooseDelete from "mongoose-delete";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";
import WhitelistHelper from "../helpers/whitelist_helper";
import { IMember } from "../interfaces/member";

const memberSchema = new Schema({
  title: { type: String },
  organization: { type: Schema.Types.ObjectId, ref: "organizations", autopopulate: true },
  user: { type: Schema.Types.ObjectId, ref: "users", autopopulate: WhitelistHelper.fieldsForUserAutopopulate },
  isBanned: { type: Boolean, default: false }
});

/**
 * Indexes
 */
memberSchema.index({ organization: 1, user: 1 });

/**
 * Plugins
 */
memberSchema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate", "countDocuments", "find"],
  indexFields: "all"
});
memberSchema.plugin(aggregatePaginate);
memberSchema.plugin(autopopulate);
memberSchema.plugin(mongoosePaginate);
memberSchema.plugin(timestamps);

export type MemberModel<T extends Document> = AggregatePaginateModel<T> &
  PaginateModel<T> &
  mongooseDelete.SoftDeleteModel<T>;

export const Member: MemberModel<IMember> = <MemberModel<IMember>>model("members", memberSchema);
