import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IPartnerApplicant } from "../interfaces";
import { PartnerApplicantStatusEnum } from "../../lib/partner-applicant/partner-applicant.enum";

const partnerApplicantSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  status: { type: String, enum: Object.values(PartnerApplicantStatusEnum), index: true }
});

partnerApplicantSchema.index({ user: 1, status: 1 });

partnerApplicantSchema.plugin(timestamps);

export type PartnerApplicantModel<T extends Document> = Model<T>;
export const PartnerApplicant: PartnerApplicantModel<IPartnerApplicant> = <PartnerApplicantModel<IPartnerApplicant>>(
  model("partnerapplicants", partnerApplicantSchema)
);
