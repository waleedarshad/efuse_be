import { Document, model, Model, Schema, SchemaTypes } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { ILeagueOfLegendsLeaderboard } from "../../interfaces/leaderboard";

const LeagueOfLegendsLeaderboardHistorySchema = new Schema({
  rank: {
    type: Number,
    required: true,
    index: true
  },
  userId: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  recruitmentProfileId: {
    type: SchemaTypes.ObjectId,
    ref: "recruitmentprofiles",
    required: true,
    index: true
  },
  leagueOfLegendsStatsId: {
    type: SchemaTypes.ObjectId,
    ref: "leagueoflegendstats",
    required: true,
    index: true
  }
});

LeagueOfLegendsLeaderboardHistorySchema.plugin(mongooseAggregate);
LeagueOfLegendsLeaderboardHistorySchema.plugin(mongoosePaginate);
LeagueOfLegendsLeaderboardHistorySchema.plugin(timestamps);

export type LeagueOfLegendsLeaderboardHistoryModel<T extends Document> = Model<T>;

export const LeagueOfLegendsLeaderboardHistory: LeagueOfLegendsLeaderboardHistoryModel<ILeagueOfLegendsLeaderboard> = <
  LeagueOfLegendsLeaderboardHistoryModel<ILeagueOfLegendsLeaderboard>
>model<ILeagueOfLegendsLeaderboard>(
  "leagueoflegendsleaderboardhistory",
  LeagueOfLegendsLeaderboardHistorySchema,
  "leagueoflegendsleaderboardhistory"
);
