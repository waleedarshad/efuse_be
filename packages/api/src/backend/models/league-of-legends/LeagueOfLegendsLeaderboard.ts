import { Document, model, PaginateModel, Schema, SchemaTypes } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { ILeagueOfLegendsLeaderboard } from "../../interfaces/leaderboard";

const LeagueOfLegendsLeaderboardSchema = new Schema({
  rank: {
    type: Number,
    required: true,
    index: true
  },
  userId: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  recruitmentProfileId: {
    type: SchemaTypes.ObjectId,
    ref: "recruitmentprofiles",
    required: true,
    index: true
  },
  leagueOfLegendsStatsId: {
    type: SchemaTypes.ObjectId,
    ref: "leagueoflegendstats",
    required: true,
    index: true
  }
});

LeagueOfLegendsLeaderboardSchema.plugin(mongooseAggregate);
LeagueOfLegendsLeaderboardSchema.plugin(mongoosePaginate);
LeagueOfLegendsLeaderboardSchema.plugin(timestamps);

export type LeagueOfLegendsLeaderboardModel<T extends Document> = PaginateModel<T>;

export const LeagueOfLegendsLeaderboard: LeagueOfLegendsLeaderboardModel<ILeagueOfLegendsLeaderboard> = <
  LeagueOfLegendsLeaderboardModel<ILeagueOfLegendsLeaderboard>
>model<ILeagueOfLegendsLeaderboard>(
  "leagueoflegendsleaderboard",
  LeagueOfLegendsLeaderboardSchema,
  "leagueoflegendsleaderboard"
);
