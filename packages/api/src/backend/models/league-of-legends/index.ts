export { LeagueOfLegendsChampionData } from "./LeagueOfLegendsChampionData";
export { LeagueOfLegendsLeaderboard } from "./LeagueOfLegendsLeaderboard";
export { LeagueOfLegendsLeaderboardHistory } from "./LeagueOfLegendsLeaderboardHistory";
export { LeagueOfLegendsStats } from "./LeagueOfLegendsStat";
export { LeagueOfLegendsVersionData } from "./LeagueOfLegendsVersionData";
