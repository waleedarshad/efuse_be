import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { ILeagueOfLegendsChampionData } from "../../interfaces/leagueOfLegends/league-of-legends-champion-data";

const schema = new Schema({
  key: { type: String, index: true },
  name: { type: String },
  version: { type: String },
  sourceIconImage: { type: String },
  sourceLoadingImage: { type: String },
  sourceSplashImage: { type: String },
  iconImage: { type: String },
  loadingImage: { type: String },
  splashImage: { type: String }
});

schema.plugin(timestamps);

export type LeagueOfLegendsChampionDataModel<T extends Document> = Model<T>;

export const LeagueOfLegendsChampionData: LeagueOfLegendsChampionDataModel<ILeagueOfLegendsChampionData> = <
  LeagueOfLegendsChampionDataModel<ILeagueOfLegendsChampionData>
>model<ILeagueOfLegendsChampionData>("leagueoflegendschampions", schema);
