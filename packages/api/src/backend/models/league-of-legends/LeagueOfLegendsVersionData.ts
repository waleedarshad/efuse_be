import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { ILeagueOfLegendsVersionData } from "../../interfaces/leagueOfLegends/league-of-legends-version-data";

const schema = new Schema({
  version: { type: String }
});

schema.plugin(timestamps);

export type LeagueOfLegendsVersionDataModel<T extends Document> = Model<T>;

export const LeagueOfLegendsVersionData: LeagueOfLegendsVersionDataModel<ILeagueOfLegendsVersionData> = <
  LeagueOfLegendsVersionDataModel<ILeagueOfLegendsVersionData>
>model<ILeagueOfLegendsVersionData>("leagueoflegendsversion", schema);
