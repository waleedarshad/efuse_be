import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { ILeagueOfLegendsStats } from "../../interfaces/leagueOfLegends/league-of-legends-stats";

const schema = new Schema({
  userInfo: {
    platformSlug: {
      type: String
    }
  },
  leagueInfo: [
    {
      freshBlood: { type: Boolean },
      hotStreak: { type: Boolean },
      inactive: { type: Boolean },
      leagueId: { type: String },
      leaguePoints: { type: Number },
      losses: { type: Number },
      miniSeries: {
        losses: { type: Number },
        progress: { type: String },
        target: { type: Number },
        wins: { type: Number }
      },
      queueType: { type: String },
      rank: { type: String },
      summonerId: { type: String },
      summonerName: { type: String },
      tier: { type: String },
      tierImage: { type: String },
      veteran: { type: Boolean },
      wins: { type: Number }
    }
  ],
  championInfo: [
    {
      championPointsUntilNextLevel: { type: Number },
      chestGranted: { type: Boolean },
      championId: { type: Number },
      championName: { type: String },
      iconImage: { type: String },
      splashImage: { type: String },
      loadingImage: { type: String },
      lastPlayTime: { type: Number },
      championLevel: { type: Number },
      summonerId: { type: String },
      championPoints: { type: Number },
      championPointsSinceLastLevel: { type: Number },
      tokensEarned: { type: Number }
    }
  ],
  leaderboardRankTierScore: { type: Number, default: 0, index: true },
  leaderboardRankTier: { type: String, index: true }
});

schema.index({ leaderboardRankTierScore: 1, leaderboardRankTier: 1 });

schema.plugin(timestamps);

export type LeagueOfLegendsStatsModel<T extends Document> = Model<T>;

export const LeagueOfLegendsStats: LeagueOfLegendsStatsModel<ILeagueOfLegendsStats> = <
  LeagueOfLegendsStatsModel<ILeagueOfLegendsStats>
>model<ILeagueOfLegendsStats>("leagueoflegendsstats", schema);
