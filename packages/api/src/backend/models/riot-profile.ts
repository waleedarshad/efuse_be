import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IRiotProfile } from "../interfaces";

const riotProfileSchema = new Schema({
  owner: { type: Schema.Types.ObjectId, refPath: "ownerKind", index: true },
  ownerKind: { type: String, index: true },
  userInfo: {
    success: { type: Boolean },
    cpid: { type: String },
    sub: { type: String },
    jti: { type: String }
  },
  valorantProfile: {
    success: { type: Boolean },
    puuid: { type: String },
    gameName: { type: String },
    tagLine: { type: String },
    shard: { type: String }
  },
  leagueOfLegendsProfile: {
    success: { type: Boolean },
    id: { type: String },
    accountId: { type: String },
    puuid: { type: String },
    name: { type: String },
    profileIconId: { type: String },
    revisionDate: { type: Number },
    summonerLevel: { type: Number },
    shard: { type: String }
  }
});

riotProfileSchema.plugin(timestamps);

riotProfileSchema.index({ owner: 1, ownerKind: 1 });

export type RiotProfileModel<T extends Document> = Model<T>;

export const RiotProfile: RiotProfileModel<IRiotProfile> = <RiotProfileModel<IRiotProfile>>(
  model("riotprofiles", riotProfileSchema)
);
