import { Schema, Document, model, AggregatePaginateModel, PaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import mongooseDelete from "mongoose-delete";

import { IFriend } from "../interfaces/friend";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";

const schema = new Schema({
  followee: { type: Schema.Types.ObjectId, ref: "users", required: true, index: true },
  follower: { type: Schema.Types.ObjectId, ref: "users", required: true, index: true }
});

schema.plugin(mongooseAggregate);
schema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate", "updateMany", "countDocuments"]
});
schema.plugin(mongoosePaginate);
schema.plugin(timestamps);

export type FriendModel<T extends Document> = AggregatePaginateModel<T> &
  PaginateModel<T> &
  mongooseDelete.SoftDeleteModel<T>;

export const Friend: FriendModel<IFriend> = model("friends", schema) as FriendModel<IFriend>;
