const mongoose = require("mongoose");

const { Schema } = mongoose;

const { timestamps } = require("../helpers/timestamp.helper");
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");

const leaderboardSchema = new Schema({
  placement: { type: Number },
  placementPercent: { type: Number },
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  streak: { type: Schema.Types.ObjectId, ref: "streaks", index: true },
  isActive: { type: Boolean, default: false, index: true },
  boardType: {
    type: String,
    enum: ["streaks", "views", "engagements"],
    index: true
  },
  stats: { type: Schema.Types.ObjectId, ref: "userstats", index: true },
  valueAtTheTimeOfCreation: { type: Number, default: 0 },

  // This will be deleted once we migrate data
  streakDuration: { type: Number }
});

leaderboardSchema.index({ user: 1, boardType: 1, isActive: 1 });

leaderboardSchema.index({ boardType: 1, isActive: 1 });

leaderboardSchema.plugin(timestamps);
leaderboardSchema.plugin(mongoosePaginate);

const Leaderboard = mongoose.model("leaderboards", leaderboardSchema);

module.exports.Leaderboard = Leaderboard;
