import { PaginateModel, model, Document, Schema, AggregatePaginateModel } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import { IRecruitmentProfile } from "../interfaces/recruitment-profile";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import recruitmentProfileProgress from "../../lib/mongoose-plugins/recruitment-profile-progress";
import { timestamps } from "../helpers/timestamp.helper";

const recruitmentProfileSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  name: { type: String },
  graduationClass: { type: String, index: true },
  addressLine1: { type: String },
  addressLine2: { type: String },
  city: { type: String },
  state: { type: String, index: true },
  zipCode: { type: String },
  country: { type: String, index: true },
  desiredCollegeMajor: [{ type: String }],
  desiredCollegeSize: { type: String },
  desiredCollegeAttributes: [{ type: String }],
  desiredCollegeRegion: { type: String },
  gender: { type: String, index: true },
  esportsCareerField: { type: String },
  highSchoolGPA: { type: String },
  usaTestScores: { type: String },
  satTestScore: { type: String },
  actTestScore: { type: String },
  toeflTestScores: { type: String },
  extracurricularActivities: { type: String },
  gradingScale: { type: String },
  primaryGame: { type: String, index: true },
  primaryGameUsername: { type: String },
  primaryGameHoursPlayed: { type: String },
  primaryGameRole: { type: String },
  secondaryGame: { type: String },
  secondaryGameUsername: { type: String },
  secondaryGameHoursPlayed: { type: String },
  secondaryGameRole: { type: String },
  highSchoolClubTeam: { type: String },
  coachName: { type: String },
  coachEmail: { type: String },
  coachPhone: { type: String },
  isHidden: { type: Boolean, default: false, index: true },
  profileProgress: { type: Number, index: true },
  profileProgressUpdatedAt: { type: Date },
  highSchool: { type: String },
  disableMessaging: { type: Boolean, default: false }
});

// index text fields for searching
recruitmentProfileSchema.index({ "$**": "text" });

recruitmentProfileSchema.plugin(timestamps);
recruitmentProfileSchema.plugin(mongoosePaginate);
recruitmentProfileSchema.plugin(mongooseAggregate);
recruitmentProfileSchema.plugin(recruitmentProfileProgress);

export type RecruitmentProfileModel<T extends Document> = PaginateModel<T> & AggregatePaginateModel<T>;

export const RecruitmentProfile: RecruitmentProfileModel<IRecruitmentProfile> = <
  RecruitmentProfileModel<IRecruitmentProfile>
>model<IRecruitmentProfile>("recruitmentprofiles", recruitmentProfileSchema);
