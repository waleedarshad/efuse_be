import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IOrganizationGame } from "../interfaces";

const organizationGameSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "games", index: true },
  organization: { type: Schema.Types.ObjectId, ref: "organizations", index: true }
});

organizationGameSchema.index({ game: 1, organization: 1 });

organizationGameSchema.plugin(timestamps);

export type OrganizationGameModel<T extends Document> = Model<T>;
export const OrganizationGame: OrganizationGameModel<IOrganizationGame> = <OrganizationGameModel<IOrganizationGame>>(
  model("organizationgames", organizationGameSchema)
);
