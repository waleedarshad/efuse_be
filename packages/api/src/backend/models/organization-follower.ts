import { Schema, Document, model, PaginateModel } from "mongoose";
import autopopulate from "mongoose-autopopulate";
import mongooseDelete from "mongoose-delete";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";
import * as WhitelistHelper from "../helpers/whitelist_helper";
import { IOrganizationFollower } from "../interfaces";

const schema = new Schema({
  followee: { type: Schema.Types.ObjectId, ref: "organizations", required: true },
  follower: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate
  }
});

// indexes
schema.index({ followee: 1, follower: 1 });

// plugins
// todo: investigate
// @ts-ignore
schema.plugin(autopopulate);
schema.plugin(mongoosePaginate);
schema.plugin(timestamps);
schema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate", "updateMany"],
  indexFields: "all"
});

export type OrganizationFollowerModel<T extends Document> = PaginateModel<T> & mongooseDelete.SoftDeleteModel<T>;

export const OrganizationFollower: OrganizationFollowerModel<IOrganizationFollower> = <
  OrganizationFollowerModel<IOrganizationFollower>
>model("organizationFollowers", schema);
