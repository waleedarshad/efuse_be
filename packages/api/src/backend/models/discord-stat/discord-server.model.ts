import { prop } from "@typegoose/typegoose";

export class ServerSchema {
  @prop({ type: () => String })
  serverName?: string;

  @prop({ type: () => Number })
  humans?: Number;

  @prop({ type: () => Number })
  bots?: Number;

  @prop({ type: () => Boolean })
  configured?: Boolean;

  @prop({ type: () => Boolean })
  toggled?: Boolean;

  @prop({ type: () => Number })
  announcements_sent?: Number;
}
