import { model } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";

import { timestamps } from "../../helpers/timestamp.helper";
import { ServerSchema } from "./discord-server.model";

@plugin(timestamps)
export class DiscordStat {
  @prop({ type: () => Number })
  total_users!: number;

  @prop({ type: () => Number })
  total_servers!: number;

  @prop({ type: () => ServerSchema })
  servers!: ServerSchema[];

  createdAt?: Date; // Handled by timestamps plugin

  updatedAt?: Date; // Handled by timestamps plugin
}

const discordStat = buildSchema(DiscordStat);

export const DiscordStatModel = addModelToTypegoose(model("discordstats", discordStat), DiscordStat);
