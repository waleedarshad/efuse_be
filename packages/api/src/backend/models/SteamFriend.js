const mongoose = require("mongoose");

const { Schema } = mongoose;
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");
const { timestamps } = require("../helpers/timestamp.helper");

const steamFriendSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  steam: {
    type: Schema.Types.ObjectId,
    ref: "steams",
    required: true,
    index: true
  },
  steamid: {
    type: String,
    index: true
  },
  relationship: {
    type: String
  },
  friendSince: {
    type: Date
  }
});

steamFriendSchema.plugin(mongoosePaginate);
steamFriendSchema.plugin(timestamps);

const SteamFriend = mongoose.model("steamFriends", steamFriendSchema);

module.exports.SteamFriend = SteamFriend;
