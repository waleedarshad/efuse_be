const mongoose = require("mongoose");
const autopopulate = require("mongoose-autopopulate");

const { Schema } = mongoose;
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");
const mongooseAggregate = require("mongoose-aggregate-paginate-v2");
const WhitelistHelper = require("../helpers/whitelist_helper");
const { timestamps } = require("../helpers/timestamp.helper");

const OrganizationInvitationSchema = new Schema({
  inviter: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate
  },
  invitee: { type: Schema.Types.ObjectId, ref: "users", required: true },
  inviteeEmail: { type: String },
  organization: {
    type: Schema.Types.ObjectId,
    ref: "organizations",
    required: true
  },
  status: { type: String, default: "invited" }
});

// todo: investigate
// @ts-ignore
OrganizationInvitationSchema.plugin(autopopulate);
OrganizationInvitationSchema.plugin(mongoosePaginate);
OrganizationInvitationSchema.plugin(mongooseAggregate);
OrganizationInvitationSchema.plugin(timestamps);

const OrganizationInvitation = mongoose.model("organizationInvitations", OrganizationInvitationSchema);

module.exports.OrganizationInvitation = OrganizationInvitation;
