const mongoose = require("mongoose");

const { Schema } = mongoose;
const { timestamps } = require("../helpers/timestamp.helper");

const SchemaHelper = require("../helpers/schema_helper");

const articleCategoriesSchema = new Schema({
  name: { type: String, unique: true },
  slug: { type: String, unique: true },
  image: SchemaHelper.fileSchemaDefaultsNoImage,
  isActive: { type: Boolean, default: true },
  user: { type: Schema.Types.ObjectId, ref: "users" }
});

articleCategoriesSchema.plugin(timestamps);

const LearningArticleCategory = mongoose.model("learningarticlecategories", articleCategoriesSchema);

module.exports.LearningArticleCategory = LearningArticleCategory;
