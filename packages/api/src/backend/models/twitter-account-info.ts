import { timestamps } from "@efuse/mongoose";
import { Schema, Document, Model, model } from "mongoose";

import { ITwitterAccountInfo } from "../interfaces";

const schema = new Schema({
  description: { type: String },
  followers: { type: Number },
  kind: { type: String, default: "TwitterAccountInfo" },
  name: { type: String },
  owner: { type: String },
  ownerKind: { type: String },
  picture: { type: String },
  profileBannerImage: { type: String },
  profileImage: { type: String },
  username: { type: String },
  verified: { type: String },
  verifiedByTwitter: { type: String }
});

schema.plugin(timestamps);

export type TwitterAccountInfoModel<T extends Document> = Model<T>;

export const TwitterAccountInfo: TwitterAccountInfoModel<ITwitterAccountInfo> = <
  TwitterAccountInfoModel<ITwitterAccountInfo>
>model("twitteraccountinfo", schema, "twitteraccountinfo");
