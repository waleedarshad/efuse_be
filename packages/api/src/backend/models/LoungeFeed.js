const mongoose = require("mongoose");

const { Schema } = mongoose;
const paginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");
const autopopulate = require("mongoose-autopopulate");
const mongooseDelete = require("mongoose-delete");
const { timestamps } = require("../helpers/timestamp.helper");

const loungeFeedSchema = new Schema(
  {
    user: {
      type: Schema.Types.ObjectId,
      ref: "users",
      required: true,
      index: true
    },
    feed: {
      type: Schema.Types.ObjectId,
      ref: "feeds",
      required: true,
      index: true
    },
    timelineable: {
      type: Schema.Types.ObjectId,
      refPath: "timelineableType",
      required: true,
      index: true
    },
    timelineableType: {
      type: String,
      enum: ["users", "organizations"],
      default: "users",
      index: true
    },
    userVerified: { type: Boolean, index: true, default: false },
    published: { type: Boolean, default: true },
    gold: { type: Boolean, default: false, index: true }
  },
  { capped: { max: 1000 } }
);

// todo: investigate
// @ts-ignore
loungeFeedSchema.plugin(autopopulate);
loungeFeedSchema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "find", "findOne", "findOneAndUpdate", "updateMany"]
});
loungeFeedSchema.plugin(timestamps);
loungeFeedSchema.plugin(paginate);

const LoungeFeed = mongoose.model("loungeFeeds", loungeFeedSchema);

module.exports.LoungeFeed = LoungeFeed;
