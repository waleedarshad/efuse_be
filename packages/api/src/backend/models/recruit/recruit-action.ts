import * as mongoose from "mongoose";

import { timestamps } from "../../helpers/timestamp.helper";
import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { IRecruitAction, ACTION } from "../../interfaces/recruit/recruit-action";

const { Schema } = mongoose;

const recruitAction = new Schema({
  recruitmentProfile: {
    type: Schema.Types.ObjectId,
    ref: "recruitmentprofiles",
    index: true
  },
  recruiterUser: {
    type: Schema.Types.ObjectId,
    ref: "users",
    index: true
  },
  ownerType: { type: String, index: true },
  owner: { type: String, index: true },
  action: {
    type: String,
    enum: Object.values(ACTION),
    default: ACTION.NO_ACTION
  }
});

recruitAction.plugin(timestamps);
recruitAction.plugin(mongoosePaginate);

recruitAction.index({ recruitmentProfile: 1, recruiterUser: 1, ownerType: 1, owner: 1 });
recruitAction.index({ ownerType: 1, owner: 1 });

export type RecruitActionModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;
export const RecruitAction: RecruitActionModel<IRecruitAction> = <RecruitActionModel<IRecruitAction>>(
  mongoose.model<IRecruitAction>("recruitactions", recruitAction)
);
