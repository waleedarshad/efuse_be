import { model, Types } from "mongoose";

import { prop, addModelToTypegoose, plugin, buildSchema } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType, ID } from "type-graphql";
import { timestamps } from "../../helpers/timestamp.helper";
import { PlatformInfo, UserInfo, OverwatchSegmentData } from "./types";

@gqlObjectType("Overwatch", { description: "The Overwatch Model" })
@plugin(timestamps)
export class OverwatchStats {
  @gqlField((_type) => ID)
  readonly _id!: string | Types.ObjectId; // Added for Graphql

  @gqlField((_type) => PlatformInfo)
  @prop({ type: PlatformInfo, _id: false })
  platformInfo!: PlatformInfo;

  @gqlField((_type) => UserInfo)
  @prop({ type: UserInfo, _id: false })
  userInfo?: UserInfo;

  @gqlField((_type) => [OverwatchSegmentData])
  @prop({ type: [OverwatchSegmentData], _id: false })
  segments?: OverwatchSegmentData[];

  @gqlField((_type) => String)
  @prop({ type: String })
  expiryDate!: string;

  @gqlField((_type) => Number)
  @prop({ type: Number, index: true, default: 0 })
  leaderboardScore!: number;

  @gqlField((_type) => Date)
  createdAt?: Date;

  @gqlField((_type) => Date)
  updatedAt?: Date;
}

const OverwatchSchema = buildSchema(OverwatchStats);

export const OverwatchStatsModel = addModelToTypegoose(model("overwatchstats", OverwatchSchema), OverwatchStats);
