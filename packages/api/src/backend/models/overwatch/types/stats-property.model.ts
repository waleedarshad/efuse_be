import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("StatsProperty", { description: "Type of StatsProperty" })
export class StatsProperty {
  @gqlField((_type) => String)
  @prop({ type: String })
  displayValue!: string;

  @gqlField((_type) => Number)
  @prop({ type: Number })
  percentile!: number;
}
