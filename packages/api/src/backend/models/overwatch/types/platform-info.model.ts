import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("PlatformInfo", { description: "Type of PlatformInfo" })
export class PlatformInfo {
  @gqlField((_type) => String)
  @prop({ type: String })
  platformSlug!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  platformUserId!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  platformUserHandle!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  platformUserIdentifier!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  avatarUrl!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  additionalParameters!: string;
}
