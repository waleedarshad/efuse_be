import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { StatsProperty } from "./stats-property.model";

@gqlObjectType("SegmentStats", { description: "Type of SegmentStats" })
export class SegmentStats {
  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  timePlayed!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  wins!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  matchesPlayed!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  timeSpentOnFire!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  cards!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  wlPercentage!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  medals!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  goldMedals!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  silverMedals!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  bronzeMedals!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  multiKills!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  soloKills!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  objectiveKills!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  environmentalKills!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  finalBlows!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  damageDone!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  healingDone!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  eliminations!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  deaths!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  kd!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  kg!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  objectiveTime!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  defensiveAssists!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  offensiveAssists!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostEliminations!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostFinalBlows!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostDamageDone!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostHealingDone!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostDefensiveAssists!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostOffensiveAssists!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostObjectiveKills!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostObjectiveTime!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostSoloKills!: StatsProperty;

  @gqlField((_type) => StatsProperty)
  @prop({ type: StatsProperty })
  mostTimeSpentOnFire!: StatsProperty;
}
