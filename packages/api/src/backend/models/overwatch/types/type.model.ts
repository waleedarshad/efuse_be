import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("NameType", { description: "String type" })
export class NameType {
  @gqlField((_type) => String)
  @prop({ type: String })
  type!: string;
}
