import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

@gqlObjectType("Attribute", { description: "Type of Attribute" })
export class Attribute {
  @gqlField((_type) => String)
  @prop({ type: String })
  realm!: string;
}
