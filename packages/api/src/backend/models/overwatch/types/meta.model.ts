import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { NameType } from "./type.model";
@gqlObjectType("MetaData", { description: "Type of MetaData" })
export class MetaData {
  @gqlField((_type) => String)
  @prop({ type: String })
  name!: NameType;
}
