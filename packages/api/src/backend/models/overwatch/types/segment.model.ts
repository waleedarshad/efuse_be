import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
import { SegmentStats } from "./segment-stats.model";
import { MetaData } from "./meta.model";
import { Attribute } from "./attribute.model";

@gqlObjectType("OverwatchSegmentData", { description: "Type of Segment" })
export class OverwatchSegmentData {
  @gqlField((_type) => String)
  @prop({ type: String })
  segmentType!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  realm!: string;

  @gqlField((_type) => String)
  @prop({ type: MetaData })
  metadata!: MetaData;

  @gqlField((_type) => Date)
  @prop({ type: Date })
  expiryDate!: Date;

  @gqlField((_type) => SegmentStats)
  @prop({ type: SegmentStats, _id: false })
  stats!: SegmentStats;

  @gqlField((_type) => Attribute)
  @prop({ type: Attribute })
  attributes?: Attribute;

  @gqlField((_type) => String)
  @prop({ type: String })
  type?: string;
}
