import { prop } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";
@gqlObjectType("UserInfo", { description: "Type of UserInfo" })
export class UserInfo {
  @gqlField((_type) => Number)
  @prop({ type: Number })
  userId!: number;

  @gqlField((_type) => Boolean)
  @prop({ type: Boolean })
  isPremium!: boolean;

  @gqlField((_type) => Boolean)
  @prop({ type: Boolean })
  isVerified!: boolean;

  @gqlField((_type) => Boolean)
  @prop({ type: Boolean })
  isInfluencer!: boolean;

  @gqlField((_type) => String)
  @prop({ type: String })
  countryCode!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  customAvatarUrl!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  customHeroUrl!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  socialAccounts!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  pageviews!: string;

  @gqlField((_type) => String)
  @prop({ type: String })
  isSuspicious!: string;
}
