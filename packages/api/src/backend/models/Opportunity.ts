import { AggregatePaginateModel, Document, model, PaginateModel, Schema } from "mongoose";
import autopopulate from "mongoose-autopopulate";
import mongooseAggregatePaginate from "mongoose-aggregate-paginate-v2";

import { IOpportunity } from "../interfaces";
import * as SchemaHelper from "../helpers/schema_helper";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";
import { OpportunityStatusEnum } from "../../lib/opportunities/opportunity-status.enum";
import { OpportunityPublishStatusEnum } from "../../lib/opportunities/opportunity-publish-status.enum";

const schema = new Schema({
  opportunityType: { type: String, required: true },
  subType: { type: String, required: true },
  title: { type: String, required: true },
  onBehalfOfOrganization: { type: Boolean, default: false },
  organization: {
    type: Schema.Types.ObjectId,
    ref: "organizations",
    autopopulate: true
  },
  self: { type: String },
  location: { type: String },
  "applicationDueDate?": { type: Boolean, default: false },
  dueDate: { type: Date },
  "gameAffiliated?": { type: Boolean, default: false },
  "minimumAgeRequired?": { type: Boolean, default: false },
  minimumAge: { type: Number },
  "moneyIncluded?": { type: Boolean, default: false },
  link: { type: String },
  description: { type: String },
  verified: {
    status: { type: Boolean, default: false },
    verifiedBy: { type: Schema.Types.ObjectId, ref: "users" },
    verifiedAt: { type: Date, default: Date.now }
  },
  image: SchemaHelper.fileSchema,
  user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  gpaRequired: { type: Boolean, default: false },
  gpa: { type: Number },
  satRequired: { type: Boolean, default: false },
  sat: { type: Number },
  actRequired: { type: Boolean, default: false },
  act: { type: Number },
  streamRequired: { type: Boolean, default: false },
  goldRequired: { type: Boolean, default: false, index: true },
  status: { type: String, default: OpportunityStatusEnum.HIDDEN, enum: Object.values(OpportunityStatusEnum) },
  candidateQuestions: [{ index: { type: Number }, question: { type: String }, inputType: { type: String } }],
  shortName: { type: String, unique: true, index: true }, // Used for assigning shortnames for urls e.g. https://efuse.gg/o/sundance
  publishStatus: {
    type: String,
    default: OpportunityPublishStatusEnum.OPEN,
    enum: Object.values(OpportunityPublishStatusEnum)
  },

  // Experience level (e.g. "Junior", "Senior")
  experience_level: { type: String },

  // Job contract commitment (e.g. "Part-time", "Full-time")
  contract: [{ type: String }],

  // Sector for opportunity (e.g. "Design", "Software")
  sector: [{ type: String }],

  company_name: { type: String },

  // For our external opportunities
  external: { type: Boolean, default: false },
  external_url: { type: String },
  external_source: { type: String },
  external_id: { type: String },

  // Random Seed to be regularly rotated for having a consistent but regular shuffling
  // to simulate freshness
  randomSeed: { type: Number },

  // To allow for paid opportunites. User must pay this amount to apply for opportunity
  applicationCost: { type: Number },

  metadata: {
    views: { type: Number, default: 0 },
    boosted: { type: Boolean, default: false },
    rank: { type: Number, default: 0, index: true }
  },
  promoted: { type: Boolean, default: false },
  traits: { type: String },
  motivations: { type: String },
  startDate: { type: Date },
  moneyIncluded: { type: Number },
  openings: { type: Number },
  requirements: {
    type: Array,
    of: {
      type: Schema.Types.ObjectId,
      ref: "opportunityrequirements"
    }
  },
  applicationResponseDate: { type: Date },
  followUpInstructions: { type: String }
});

schema.index({
  opportunityType: "text",
  subType: "text",
  title: "text"
});

schema.index({
  status: 1,
  publishStatus: 1
});

schema.index({
  status: 1,
  publishStatus: 1,
  opportunityType: 1
});

schema.index({
  user: 1,
  organization: 1,
  publishStatus: 1,
  status: 1
});

schema.plugin(mongooseAggregatePaginate);
schema.plugin(autopopulate);
schema.plugin(mongoosePaginate);
schema.plugin(timestamps);

export type OpportunityModel<T extends Document> = AggregatePaginateModel<T> & PaginateModel<T>;

export const Opportunity: OpportunityModel<IOpportunity> = <OpportunityModel<IOpportunity>>(
  model<IOpportunity>("opportunities", schema)
);
