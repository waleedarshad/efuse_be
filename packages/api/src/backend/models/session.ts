import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { ISession } from "../interfaces";

const sessionSchema = new Schema({
  refreshToken: { type: String, required: true, unique: true },
  active: { type: Boolean, default: true, index: true },
  revokedAt: { type: Date },
  user: { type: Schema.Types.ObjectId, ref: "users", required: true, index: true },
  used: { type: Boolean, default: false },
  usedAt: { type: Date },
  expiry: { type: Date },
  userAgent: {
    isYaBrowser: { type: String },
    isAuthoritative: { type: String },
    isMobile: { type: String },
    isTablet: { type: String },
    isiPad: { type: String },
    isiPod: { type: String },
    isiPhone: { type: String },
    isAndroid: { type: String },
    isBlackberry: { type: String },
    isOpera: { type: String },
    isIE: { type: String },
    isEdge: { type: String },
    isIECompatibilityMode: { type: String },
    isSafari: { type: String },
    isFirefox: { type: String },
    isWebkit: { type: String },
    isChrome: { type: String },
    isKonqueror: { type: String },
    isOmniWeb: { type: String },
    isSeaMonkey: { type: String },
    isFlock: { type: String },
    isAmaya: { type: String },
    isPhantomJS: { type: String },
    isEpiphany: { type: String },
    isDesktop: { type: String },
    isWindows: { type: String },
    isLinux: { type: String },
    isLinux64: { type: String },
    isMac: { type: String },
    isChromeOS: { type: String },
    isBada: { type: String },
    isSamsung: { type: String },
    isRaspberry: { type: String },
    isBot: { type: String },
    isCurl: { type: String },
    isAndroidTablet: { type: String },
    isWinJs: { type: String },
    isKindleFire: { type: String },
    isSilk: { type: String },
    isCaptive: { type: String },
    isSmartTV: { type: String },
    isUC: { type: String },
    isFacebook: { type: String },
    isAlamoFire: { type: String },
    isElectron: { type: String },
    silkAccelerated: { type: String },
    browser: { type: String },
    version: { type: String },
    os: { type: String },
    platform: { type: String },
    geoIp: { type: Schema.Types.Mixed },
    source: { type: String }
  },
  location: {
    ip: { type: String },
    range: { type: [Number] },
    country: { type: String },
    region: { type: String },
    eu: { type: String },
    timezone: { type: String },
    city: { type: String },
    ll: { type: [Number] },
    metro: { type: Number },
    area: { type: Number },
    error: { type: String }
  }
});

sessionSchema.plugin(timestamps);

sessionSchema.index({ user: 1, active: 1, used: 1 });
sessionSchema.index({ user: 1, createdAt: 1 });

export type SessionModel<T extends Document> = Model<T>;

export const Session: SessionModel<ISession> = <SessionModel<ISession>>model("sessions", sessionSchema);
