import * as mongoose from "mongoose";

import { NoteRelatedModel, NoteOwnerType } from "@efuse/entities";
import aggregatePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";

import { INote } from "../interfaces/note";

const relatedModels = Object.values(NoteRelatedModel);
const ownerTypes = Object.values(NoteOwnerType);

const { Schema } = mongoose;

const notesSchema = new Schema({
  author: { type: Schema.Types.ObjectId, ref: "users", index: true },
  content: { type: String },
  relatedDoc: { type: Schema.Types.ObjectId, refPath: "relatedModel", index: true },
  relatedModel: { type: String, enum: relatedModels, index: true },
  owner: { type: Schema.Types.ObjectId, refPath: "ownerType", index: true },
  ownerType: { type: String, enum: ownerTypes, index: true }
});

notesSchema.index({ relatedDoc: 1, relatedModel: 1, owner: 1, ownerType: 1 });

notesSchema.plugin(aggregatePaginate);
notesSchema.plugin(timestamps);

export type NoteModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;

export const Note: NoteModel<INote> = <NoteModel<INote>>mongoose.model<INote>("notes", notesSchema);
