import { model } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";

import { timestamps } from "../helpers/timestamp.helper";

@plugin(timestamps)
export class GDPRAudit {
  @prop({ type: Number })
  users?: number;

  @prop({ type: Number })
  comments?: number;

  @prop({ type: Number })
  hypes?: number;

  @prop({ type: Number })
  feeds?: number;

  @prop({ type: Number })
  homeFeeds?: number;

  @prop({ type: Number })
  loungeFeeds?: number;

  @prop({ type: Number })
  organizations?: number;

  @prop({ type: Number })
  members?: number;

  @prop({ type: Number })
  organizationFollowers?: number;

  @prop({ type: Number })
  organizationRequests?: number;
}

const gdprSchema = buildSchema(GDPRAudit);

export const GDPRAuditModel = addModelToTypegoose(model("gdpraudits", gdprSchema), GDPRAudit);
