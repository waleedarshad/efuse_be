import { Schema, PaginateModel, Document, model } from "mongoose";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { ITwitchVideo } from "../interfaces";

const twitchVideoSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  video_id: { type: String, index: true },
  user_id: { type: String, index: true },
  user_name: { type: String },
  title: { type: String },
  description: { type: String },
  created_at: { type: Date },
  published_at: { type: Date },
  url: { type: String },
  thumbnail_url: { type: String },
  viewable: { type: String },
  view_count: { type: Number },
  language: { type: String },
  type: { type: String },
  duration: { type: String },
  jobRanAt: { type: Date },
  createdAt: { type: Date, default: Date.now }
});

twitchVideoSchema.plugin(mongoosePaginate);

twitchVideoSchema.index({ user: 1, video_id: 1 });

export type TwitchVideoModel<T extends Document> = PaginateModel<T>;
export const TwitchVideo: TwitchVideoModel<ITwitchVideo> = <TwitchVideoModel<ITwitchVideo>>(
  model("twitchVideos", twitchVideoSchema)
);
