const mongoose = require("mongoose");

const { Schema } = mongoose;
const mongoosePaginate = require("../../lib/mongoose-plugins/mongoose-paginate-v2");

const autopopulate = require("mongoose-autopopulate");
const SchemaHelper = require("../helpers/schema_helper");
const mongooseAggregate = require("mongoose-aggregate-paginate-v2");
const WhitelistHelper = require("../helpers/whitelist_helper");
const mongooseDelete = require("mongoose-delete");
const { timestamps } = require("../helpers/timestamp.helper");
const { linkParser } = require("../../lib/mongoose-plugins/link-parser");

const feedSchema = new Schema({
  content: { type: String },
  comments: { type: Number, default: 0 },
  likes: { type: Number, default: 0 },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate,
    index: true
  },
  mentions: SchemaHelper.mentionSchema,
  sharedFeed: { type: Schema.Types.ObjectId, ref: "feeds" },
  shared: { type: Boolean, default: false },
  sharedTimeline: { type: Schema.Types.ObjectId, ref: "homeFeeds" },
  sharedTimelineable: {
    type: Schema.Types.ObjectId,
    refPath: "sharedTimelineableType"
  },
  sharedTimelineableType: {
    type: String,
    enum: ["users", "organizations"]
  },
  reports: [
    {
      reportedBy: {
        type: Schema.Types.ObjectId,
        ref: "users",
        autopopulate: true
      },
      description: { type: String },
      createdAt: { type: Date, default: Date.now }
    }
  ],
  giphy: { type: Schema.Types.ObjectId, ref: "giphygifs" },
  platform: { type: String, select: false },
  metadata: {
    views: { type: Number, default: 0 },
    hashtags: { type: Array },
    comprehendAnalysis: {
      type: new Schema({
        languages: {
          type: Array,
          of: {
            languageCode: { type: String },
            score: { type: String }
          }
        },
        sentiments: {
          sentiment: { type: String },
          sentimentScore: {
            positive: { type: String },
            negative: { type: String },
            neutral: { type: String },
            mixed: { type: String }
          }
        },
        entities: {
          type: Array,
          of: {
            score: { type: String },
            type: { type: String },
            text: { type: String },
            beginOffset: { type: Number },
            endOffset: { type: Number }
          }
        }
      }),
      select: false
    },
    boosted: { type: Boolean, default: false },
    sharedLinks: { type: Array, of: String }
  },
  twitchClip: { type: Schema.Types.ObjectId, ref: "twitchclips" },
  scheduledFeed: { type: Schema.Types.ObjectId, ref: "scheduledfeeds", index: true },
  youtubeVideo: { type: Schema.Types.ObjectId, ref: "youtubevideos", index: true }
});

feedSchema.index({ _id: 1, "reports.reportedBy": 1 });
feedSchema.index({ user: 1, deleted: 1, "reports.reportedBy": 1 });
feedSchema.index({ _id: 1, shared: 1, "reports.reportedBy": 1 });
feedSchema.index({ deleted: 1, "reports.reportedBy": 1 });

feedSchema.plugin(mongoosePaginate);

// todo: investigate
// @ts-ignore
feedSchema.plugin(autopopulate);
feedSchema.plugin(mongooseAggregate);
feedSchema.plugin(mongooseDelete, {
  indexFields: "all",
  deletedAt: true,
  overrideMethods: ["count", "find", "findOne", "findOneAndUpdate", "updateMany"]
});
feedSchema.plugin(timestamps);
feedSchema.plugin(linkParser);

const Feed = mongoose.model("feeds", feedSchema);

module.exports.Feed = Feed;
