import { Schema } from "mongoose";
import { ValorantAbilityCastsSchema } from "./valorant-ability-casts";

export const ValorantMatchPlayerStatsSchema = new Schema({
  score: { type: Number },
  roundsPlayed: { type: Number },
  kills: { type: Number },
  deaths: { type: Number },
  assists: { type: Number },
  playtimeMillis: { type: Number },
  abilityCasts: ValorantAbilityCastsSchema
});
