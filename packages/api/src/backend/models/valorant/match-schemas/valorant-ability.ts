import { Schema } from "mongoose";

export const ValorantAbilitySchema = new Schema({
  grenadeEffects: { type: String },
  ability1Effects: { type: String },
  ability2Effects: { type: String },
  ultimateEffects: { type: String }
});
