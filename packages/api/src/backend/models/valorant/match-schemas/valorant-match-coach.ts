import { Schema } from "mongoose";

export const ValorantMatchCoachSchema = new Schema({
  puuid: { type: String },
  teamId: { type: String }
});
