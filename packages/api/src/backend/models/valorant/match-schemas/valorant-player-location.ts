import { Schema } from "mongoose";
import { ValorantLocationSchema } from "./valorant-location";

export const ValorantPlayerLocationSchema = new Schema({
  puuid: { type: String },
  viewRadians: { type: Number },
  location: ValorantLocationSchema
});
