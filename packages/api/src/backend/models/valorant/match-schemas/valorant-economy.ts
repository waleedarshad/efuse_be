import { Schema } from "mongoose";

export const ValorantEconomySchema = new Schema({
  loadoutValue: { type: Number },
  weapon: { type: String },
  armor: { type: String },
  remaining: { type: Number },
  spent: { type: Number }
});
