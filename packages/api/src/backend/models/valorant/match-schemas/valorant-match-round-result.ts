import { Schema } from "mongoose";
import { ValorantLocationSchema } from "./valorant-location";
import { ValorantPlayerLocationSchema } from "./valorant-player-location";
import { ValorantPlayerRoundStatsSchema } from "./valorant-player-round-stats";

export const ValorantMatchRoundResultSchema = new Schema({
  roundNum: { type: Number },
  roundResult: { type: String },
  roundCeremony: { type: String },
  winningTeam: { type: String },
  bombPlanter: { type: String },
  bombDefuser: { type: String },
  plantRoundTime: { type: Number },
  playPlayerLocations: [ValorantPlayerLocationSchema],
  plantLocation: ValorantLocationSchema,
  plantSite: { type: String },
  defuseRoundTime: { type: Number },
  defuseLocation: ValorantLocationSchema,
  playerStats: [ValorantPlayerRoundStatsSchema],
  roundResultCode: { type: String }
});
