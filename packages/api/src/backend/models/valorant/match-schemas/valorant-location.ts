import { Schema } from "mongoose";

export const ValorantLocationSchema = new Schema({
  x: { type: String },
  y: { type: String }
});
