import { Schema } from "mongoose";
import { ValorantAbilitySchema } from "./valorant-ability";
import { ValorantDamageSchema } from "./valorant-damage";
import { ValorantEconomySchema } from "./valorant-economy";
import { ValorantKillSchema } from "./valorant-kill";

export const ValorantPlayerRoundStatsSchema = new Schema({
  puuid: { type: String },
  kills: [ValorantKillSchema],
  damage: [ValorantDamageSchema],
  score: { type: Number },
  economy: [ValorantEconomySchema],
  ability: [ValorantAbilitySchema]
});
