import { Schema } from "mongoose";

export const ValorantFinishingDamageSchema = new Schema({
  damageType: { type: String },
  damageItem: { type: String },
  isSecondaryFireMode: { type: Boolean }
});
