import { Schema } from "mongoose";
import { ValorantFinishingDamageSchema } from "./valorant-finishing-damage";
import { ValorantLocationSchema } from "./valorant-location";
import { ValorantPlayerLocationSchema } from "./valorant-player-location";

export const ValorantKillSchema = new Schema({
  timeSinceGameStartMillis: { type: Number },
  timeSinceRoundStartMillis: { type: Number },
  killer: { type: String },
  victim: { type: String },
  victimLocation: ValorantLocationSchema,
  assistants: [{ type: String }],
  playerLocations: [ValorantPlayerLocationSchema],
  finishingDamage: ValorantFinishingDamageSchema
});
