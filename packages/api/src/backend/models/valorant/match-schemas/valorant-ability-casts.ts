import { Schema } from "mongoose";

export const ValorantAbilityCastsSchema = new Schema({
  grenadeCasts: { type: Number },
  ability1Casts: { type: Number },
  ability2Casts: { type: Number },
  ultimateCasts: { type: Number }
});
