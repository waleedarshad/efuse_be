import { Schema } from "mongoose";

export const ValorantDamageSchema = new Schema({
  receiver: { type: String },
  damage: { type: Number },
  legshots: { type: Number },
  bodyshots: { type: Number },
  headshots: { type: Number }
});
