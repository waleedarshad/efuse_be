import { Schema } from "mongoose";

export const ValorantMatchInfoSchema = new Schema({
  matchId: { type: String, index: true },
  mapId: { type: String },
  gameLengthMillis: { type: Number },
  gameStartMillis: { type: Number, index: true },
  provisioningFlowId: { type: String },
  isCompleted: { type: Boolean, index: true },
  customGameName: { type: String },
  queueId: { type: String, index: true },
  gameMode: { type: String },
  isRanked: { type: Boolean, index: true },
  seasonId: { type: String, index: true }
});
