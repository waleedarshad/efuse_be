import { Schema } from "mongoose";

export const ValorantMatchTeamSchema = new Schema({
  teamId: { type: String },
  won: { type: Boolean },
  roundsPlayed: { type: Number },
  roundsWon: { type: Number },
  numPoints: { type: Number }
});
