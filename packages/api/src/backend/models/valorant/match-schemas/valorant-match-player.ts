import { Schema } from "mongoose";
import { ValorantMatchPlayerStatsSchema } from "./valorant-match-player-stats";

export const ValorantMatchPlayerSchema = new Schema({
  puuid: { type: String },
  gameName: { type: String },
  tagLine: { type: String },
  teamId: { type: String },
  partyId: { type: String },
  characterId: { type: String },
  stats: ValorantMatchPlayerStatsSchema,
  competitiveTier: { type: Number }
});
