import { Schema } from "mongoose";

export const ValorantMatchListEntrySchema = new Schema({
  matchId: { type: String },
  gameStartTimeMillis: { type: Number },
  teamId: { type: String }
});
