import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantMatchDTO } from "../../interfaces/valorant/valorant-match-dto";
import {
  ValorantMatchCoachSchema,
  ValorantMatchInfoSchema,
  ValorantMatchPlayerSchema,
  ValorantMatchTeamSchema,
  ValorantMatchRoundResultSchema
} from "./match-schemas";

export const ValorantMatchSchema = new Schema({
  matchInfo: ValorantMatchInfoSchema,
  players: [ValorantMatchPlayerSchema],
  coaches: [ValorantMatchCoachSchema],
  teams: [ValorantMatchTeamSchema],
  roundResults: [ValorantMatchRoundResultSchema]
});

ValorantMatchSchema.plugin(timestamps);
// We were running into issues with disk io being high. This is to help with that.
ValorantMatchSchema.index({
  "matchInfo.matchId": 1,
  "matchInfo.queueId": 1,
  "matchInfo.seasonId": 1,
  "matchInfo.gameStartMillis": -1
});

export type ValorantMatchModel<T extends Document> = Model<T>;

export const ValorantMatch: ValorantMatchModel<IValorantMatchDTO> = <ValorantMatchModel<IValorantMatchDTO>>(
  model<IValorantMatchDTO>("valorantmatches", ValorantMatchSchema)
);
