import { Document, model, Model, Schema, SchemaTypes } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantLeaderboard } from "../../interfaces/leaderboard";

const ValorantLeaderboardHistorySchema = new Schema({
  rank: {
    type: Number,
    required: true,
    index: true
  },
  userId: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  recruitmentProfileId: {
    type: SchemaTypes.ObjectId,
    ref: "recruitmentprofiles",
    required: true,
    index: true
  },
  valorantStatsId: {
    type: SchemaTypes.ObjectId,
    ref: "valorantstats",
    required: true,
    index: true
  }
});

ValorantLeaderboardHistorySchema.plugin(mongooseAggregate);
ValorantLeaderboardHistorySchema.plugin(mongoosePaginate);
ValorantLeaderboardHistorySchema.plugin(timestamps);

export type ValorantLeaderboardHistoryModel<T extends Document> = Model<T>;

export const ValorantLeaderboardHistory: ValorantLeaderboardHistoryModel<IValorantLeaderboard> = <
  ValorantLeaderboardHistoryModel<IValorantLeaderboard>
>model<IValorantLeaderboard>(
  "valorantleaderboardhistory",
  ValorantLeaderboardHistorySchema,
  "valorantleaderboardhistory"
);
