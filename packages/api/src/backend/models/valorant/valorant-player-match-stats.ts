import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantPlayerMatchStats } from "../../interfaces/valorant/valorant-player-match-stats";

const schema = new Schema({
  // general account details
  puuid: { type: String, index: true },
  agent: { type: String },
  actId: { type: String, index: true },

  // ranking
  competitiveTier: { type: Number },

  // match stats
  gameMode: { type: String },
  matchId: { type: String, index: true },
  won: { type: Boolean },
  damage: { type: Number },
  kills: { type: Number },
  deaths: { type: Number },
  assists: { type: Number },
  timePlayed: { type: Number },
  timeStarted: { type: Number },
  queueId: { type: String }
});

schema.plugin(timestamps);
// We were running into issues with disk io being high with valorant matches. This is similar, so added it in.
schema.index({
  matchId: 1,
  queueId: 1,
  actId: 1,
  timePlayed: -1
});

export type ValorantPlayerMatchStatsModel<T extends Document> = Model<T>;

export const ValorantPlayerMatchStats: ValorantPlayerMatchStatsModel<IValorantPlayerMatchStats> = <
  ValorantPlayerMatchStatsModel<IValorantPlayerMatchStats>
>model<IValorantPlayerMatchStats>("valorantplayermatchstats", schema);
