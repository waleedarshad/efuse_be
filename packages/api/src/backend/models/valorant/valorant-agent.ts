import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantAgentDTO } from "../../interfaces/valorant/valorant-agent-dto";

export const ValorantAgentSchema = new Schema({
  uuid: { type: String },
  displayName: { type: String },
  description: { type: String },
  developerName: { type: String },
  displayIcon: { type: String },
  displayIconSmall: { type: String },
  bustPortrait: { type: String },
  fullPortrait: { type: String },
  assetPath: { type: String }
});

ValorantAgentSchema.plugin(timestamps);

export type ValorantAgentModel<T extends Document> = Model<T>;

export const ValorantAgent: ValorantAgentModel<IValorantAgentDTO> = <ValorantAgentModel<IValorantAgentDTO>>(
  model<IValorantAgentDTO>("valorantagents", ValorantAgentSchema)
);
