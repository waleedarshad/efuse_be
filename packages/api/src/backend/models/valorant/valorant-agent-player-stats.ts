import { Schema } from "mongoose";
import { ValorantAgentSchema } from "./valorant-agent";

export const AgentPlayerStatsSchema = new Schema({
  agent: { type: String },
  wins: { type: Number },
  losses: { type: Number },
  kills: { type: Number },
  deaths: { type: Number },
  assists: { type: Number },
  damage: { type: Number },
  timePlayed: { type: Number },
  agentData: ValorantAgentSchema
});
