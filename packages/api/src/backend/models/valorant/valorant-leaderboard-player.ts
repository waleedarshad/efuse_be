import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantLeaderboardPlayerDTO } from "../../interfaces/valorant/valorant-leaderboard-player-dto";

const schema = new Schema({
  puuid: { type: String, index: true },
  gameName: { type: String },
  tagLine: { type: String },
  leaderboardRank: { type: Number },
  rankedRating: { type: Number },
  numberOfWins: { type: Number },
  competitiveTier: { type: Number },
  actId: { type: String, index: true },
  region: { type: String, index: true }
});

schema.plugin(timestamps);

export type ValorantLeaderboardPlayerModel<T extends Document> = Model<T>;

export const ValorantLeaderboardPlayer: ValorantLeaderboardPlayerModel<IValorantLeaderboardPlayerDTO> = <
  ValorantLeaderboardPlayerModel<IValorantLeaderboardPlayerDTO>
>model<IValorantLeaderboardPlayerDTO>("valorantleaderboardplayers", schema);
