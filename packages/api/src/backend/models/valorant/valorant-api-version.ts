import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantApiVersion } from "../../interfaces/valorant/valorant-api-version";

const schema = new Schema({
  branch: { type: String },
  version: { type: String },
  buildVersion: { type: String },
  buildDate: { type: Date, index: 1 }
});

schema.plugin(timestamps);

export type ValorantApiVersionModel<T extends Document> = Model<T>;

export const ValorantApiVersion: ValorantApiVersionModel<IValorantApiVersion> = <
  ValorantApiVersionModel<IValorantApiVersion>
>model<IValorantApiVersion>("valorantapiversion", schema);
