import { Document, model, PaginateModel, Schema, SchemaTypes } from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";

import mongoosePaginate from "../../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantLeaderboard } from "../../interfaces/leaderboard";

const ValorantLeaderboardSchema = new Schema({
  rank: {
    type: Number,
    required: true,
    index: true
  },
  userId: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },
  recruitmentProfileId: {
    type: SchemaTypes.ObjectId,
    ref: "recruitmentprofiles",
    required: true,
    index: true
  },
  valorantStatsId: {
    type: SchemaTypes.ObjectId,
    ref: "valorantstats",
    required: true,
    index: true
  }
});

ValorantLeaderboardSchema.plugin(mongooseAggregate);
ValorantLeaderboardSchema.plugin(mongoosePaginate);
ValorantLeaderboardSchema.plugin(timestamps);

export type ValorantLeaderboardModel<T extends Document> = PaginateModel<T>;

export const ValorantLeaderboard: ValorantLeaderboardModel<IValorantLeaderboard> = <
  ValorantLeaderboardModel<IValorantLeaderboard>
>model<IValorantLeaderboard>("valorantleaderboard", ValorantLeaderboardSchema, "valorantleaderboard");
