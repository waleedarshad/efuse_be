import { Document, model, Model, Schema, SchemaTypes } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantStats } from "../../interfaces/valorant/valorant-stats";
import { AgentPlayerStatsSchema } from "./valorant-agent-player-stats";

const schema = new Schema({
  user: {
    type: SchemaTypes.ObjectId,
    ref: "users",
    required: true,
    index: true
  },

  // general account details
  puuid: { type: String, index: true },
  gameName: { type: String, index: true },
  tagLine: { type: String, index: true },
  region: { type: String, index: true },
  episodeId: { type: String },
  episodeName: { type: String },
  actId: { type: String },
  actName: { type: String },
  lastMatchTime: { type: Number },

  // ranking
  competitiveTier: { type: Number },
  competitiveTierName: { type: String },
  competitiveTierImage: { type: String },
  rankedRating: { type: Number },
  leaderboardRank: { type: Number },

  // match stats
  wins: { type: Number },
  losses: { type: Number },
  damage: { type: Number },
  kills: { type: Number },
  deaths: { type: Number },
  assists: { type: Number },

  // agent stats
  agentMatchStats: [AgentPlayerStatsSchema]
});

schema.plugin(timestamps);

export type ValorantStatsModel<T extends Document> = Model<T>;

export const ValorantStats: ValorantStatsModel<IValorantStats> = <ValorantStatsModel<IValorantStats>>(
  model<IValorantStats>("valorantstats", schema)
);
