import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantMatchListDTO } from "../../interfaces/valorant/valorant-match-list-dto";
import { ValorantMatchListEntrySchema } from "./valorant-match-list-entry";

export const ValorantMatchListSchema = new Schema({
  puuid: { type: String },
  history: [ValorantMatchListEntrySchema]
});

ValorantMatchListSchema.plugin(timestamps);

export type ValorantMatchListModel<T extends Document> = Model<T>;

export const ValorantMatchList: ValorantMatchListModel<IValorantMatchListDTO> = <
  ValorantMatchListModel<IValorantMatchListDTO>
>model<IValorantMatchListDTO>("valorantmatchlist", ValorantMatchListSchema);
