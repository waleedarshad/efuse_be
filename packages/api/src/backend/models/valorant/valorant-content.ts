import { Document, model, Model, Schema } from "mongoose";
import { timestamps } from "../../helpers/timestamp.helper";
import { IValorantContentDTO } from "../../interfaces/valorant/valorant-content-dto";

const contentItemSchema = new Schema({
  name: { type: String },
  id: { type: String },
  assetName: { type: String }
});

const actSchema = new Schema({
  name: { type: String },
  id: { type: String },
  isActive: { type: Boolean },
  parentId: { type: String },
  type: { type: String }
});

const schema = new Schema({
  region: { type: String, index: true },
  version: { type: String },
  characters: [contentItemSchema],
  playerCards: [contentItemSchema],
  playerTitles: [contentItemSchema],
  acts: [actSchema]
});

schema.plugin(timestamps);

export type ValorantContentModel<T extends Document> = Model<T>;

export const ValorantContent: ValorantContentModel<IValorantContentDTO> = <ValorantContentModel<IValorantContentDTO>>(
  model<IValorantContentDTO>("valorantcontent", schema)
);
