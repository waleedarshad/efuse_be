import * as mongoose from "mongoose";
import autopopulate from "mongoose-autopopulate";

import SchemaHelper from "../helpers/schema_helper";
import { timestamps } from "../helpers/timestamp.helper";

import { IAutomatedFeed } from "../interfaces/automated-feed";

const { Schema } = mongoose;

const automatedFeedSchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true
  },
  content: { type: String },
  mentions: SchemaHelper.mentionSchema,
  timelineable: {
    type: Schema.Types.ObjectId,
    refPath: "timelineableType",
    required: true
  },
  timelineableType: {
    type: String,
    enum: ["users", "organizations"],
    default: "users"
  },
  verified: { type: Boolean, default: false },
  crossPosts: [String],
  media: { type: Schema.Types.ObjectId, ref: "media", autopopulate: true },
  mediaObjects: [{ type: Schema.Types.ObjectId, ref: "media", autopopulate: true }],
  automatedType: {
    type: String,
    enum: ["startStream", "endStream", "gameChange", "titleChange"]
  },
  twitchClip: { type: Schema.Types.ObjectId, ref: "twitchClips" },
  lastSentAt: SchemaHelper.updatedAtSchema,
  platform: { type: String },
  youtubeVideo: { type: Schema.Types.ObjectId, ref: "youtubevideos" }
});

automatedFeedSchema.plugin(autopopulate);
automatedFeedSchema.plugin(timestamps);

export type AutomatedFeedModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;
export const AutomatedFeed: AutomatedFeedModel<IAutomatedFeed> = <AutomatedFeedModel<IAutomatedFeed>>(
  mongoose.model<IAutomatedFeed>("automatedFeeds", automatedFeedSchema)
);
