import { Schema, Document, Model, model } from "mongoose";
import { ITwitchFriend } from "../interfaces";

const twitchFriendsSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  from_id: { type: String, index: true },
  from_name: { type: String },
  to_id: { type: String, index: true },
  to_name: { type: String },
  followed_at: { type: String },
  jobRanAt: { type: Date },
  createdAt: { type: Date, default: Date.now }
});

twitchFriendsSchema.index({ user: 1, from_id: 1, to_id: 1 });

export type TwitchFriendModel<T extends Document> = Model<T>;
export const TwitchFriend: TwitchFriendModel<ITwitchFriend> = <TwitchFriendModel<ITwitchFriend>>(
  model("twitchfriends", twitchFriendsSchema)
);
