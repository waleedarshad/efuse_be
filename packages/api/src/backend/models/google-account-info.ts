import { timestamps } from "@efuse/mongoose";
import { Schema, Document, Model, model } from "mongoose";

import { IGoogleAccountInfo } from "../interfaces";

const schema = new Schema({
  kind: { type: String, default: "GoogleAccountInfo" },
  name: { type: String },
  owner: { type: String },
  ownerKind: { type: String },
  picture: { type: String },
  verified: { type: String }
});

schema.index({ kind: 1, name: 1, owner: 1, ownerKind: 1 });

schema.plugin(timestamps);

export type GoogleAccountInfoModel<T extends Document> = Model<T>;

export const GoogleAccountInfo: GoogleAccountInfoModel<IGoogleAccountInfo> = <
  GoogleAccountInfoModel<IGoogleAccountInfo>
>model("googleaccountinfo", schema, "googleaccountinfo");
