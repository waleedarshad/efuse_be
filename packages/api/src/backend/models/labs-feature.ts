import * as mongoose from "mongoose";
import mongooseAggregate from "mongoose-aggregate-paginate-v2";
import mongooseDelete from "mongoose-delete";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";

import { ILabsFeature } from "../interfaces";

const schema = new mongoose.Schema({
  description: { type: String, required: false },
  enabled: { type: Boolean, default: false, required: true },
  featureFlag: { type: String, required: true },
  label: { type: String, required: true },
  referralsRequired: { type: Number, required: false },
  visible: { type: Boolean, default: true }
});

// indexes
schema.index({ _id: 1, label: 1, visible: 1 });
schema.index({ _id: 1, deleted: -1, visible: 1 });

// plugins
schema.plugin(mongooseAggregate);
schema.plugin(mongooseDelete);
schema.plugin(mongoosePaginate);
schema.plugin(timestamps);

export type LabsModel<T extends mongoose.Document> = mongoose.AggregatePaginateModel<T> &
  mongoose.PaginateModel<T> &
  mongooseDelete.SoftDeleteModel<T>;

export const LabsFeature: LabsModel<ILabsFeature> = <LabsModel<ILabsFeature>>(
  mongoose.model<ILabsFeature>("labsfeatures", schema)
);
