import * as mongoose from "mongoose";

import { ILink } from "../interfaces/link";
import { timestamps } from "../helpers/timestamp.helper";

const { Schema } = mongoose;

const linksSchema = new Schema({
  shortName: { type: String, index: true, unique: true },
  url: { type: String, index: true },
  opengraph: {
    title: { type: String },
    description: { type: String },
    domain: { type: String },
    img: { type: String }
  }
});

linksSchema.plugin(timestamps);

export type LinkModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;

export const Link: LinkModel<ILink> = <LinkModel<ILink>>mongoose.model<ILink>("links", linksSchema);
