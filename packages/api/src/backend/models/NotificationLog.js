const mongoose = require("mongoose");

const { Schema } = mongoose;

const { timestamps } = require("../helpers/timestamp.helper");

const notificationLogSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users", index: true },
  type: { type: String, index: true, enum: ["push", "in-app", "email"] },
  content: { type: String },
  notification: { type: Schema.Types.ObjectId, ref: "notifications" },
  error: { type: Schema.Types.Mixed },
  status: { type: String, enum: ["RATE_LIMIT_REACHED", "SENT", "ERROR"] }
});

notificationLogSchema.plugin(timestamps);

const NotificationLog = mongoose.model("notificationLogs", notificationLogSchema);

module.exports.NotificationLog = NotificationLog;
