import { timestamps } from "@efuse/mongoose";
import { Schema, Document, Model, model } from "mongoose";

import { ITwitchAccountInfo } from "../interfaces";

const schema = new Schema({
  accountEmail: { type: String },
  accountId: { type: String },
  accountType: { type: String },
  broadcasterType: { type: String },
  description: { type: String },
  displayName: { type: String },
  followers: { type: String },
  kind: { type: String, default: "TwitchAccountInfo" },
  link: { type: String },
  owner: { type: String },
  ownerKind: { type: String },
  profileImage: { type: String },
  sendStartStreamMessage: { type: Boolean, default: false },
  username: { type: String },
  verified: { type: Boolean },
  viewCount: { type: Number },
  webhookSubscription: {
    callback: { type: String },
    expiresAt: { type: Date },
    isActive: { type: Boolean, default: false }
  },
  stream: {
    endedAt: { type: Date },
    isLive: { type: Boolean, default: false },
    lastLiveAt: { type: Date },
    startedAt: { type: Date },
    title: { type: String },
    viewerCount: { type: Number }
  }
});

schema.index({ kind: 1, owner: 1, ownerKind: 1, verfied: 1, user: 1 });

schema.plugin(timestamps);

export type TwitchAccountInfoModel<T extends Document> = Model<T>;

export const TwitchAccountInfo: TwitchAccountInfoModel<ITwitchAccountInfo> = <
  TwitchAccountInfoModel<ITwitchAccountInfo>
>model("twitchaccountinfo", schema, "twitchaccountinfo");
