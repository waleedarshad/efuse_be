const mongoose = require("mongoose");

const { timestamps } = require("../helpers/timestamp.helper");

// ref models
require("./User").User;
require("./EgencyCampaign");

const { Schema } = mongoose;
const eGencyOfferSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users" },
  campaign: { type: Schema.Types.ObjectId, ref: "egencycampaigns" },
  status: {
    type: String,
    default: "pending",
    enum: ["accepted", "declined", "pending"]
  }
});

eGencyOfferSchema.plugin(timestamps);

const EgencyOffer = mongoose.model("egencyoffers", eGencyOfferSchema);

module.exports.EgencyOffer = EgencyOffer;
