const mongoose = require("mongoose");

const { Schema } = mongoose;
const { timestamps } = require("../helpers/timestamp.helper");

const steamSchema = new Schema({
  steamid: {
    type: String,
    index: true
  },
  communityvisibilitystate: {
    type: Number
  },
  profilestate: {
    type: Number
  },
  personaname: {
    type: String
  },
  lastlogoff: {
    type: Date
  },
  profileurl: {
    type: String
  },
  avatar: {
    type: String
  },
  avatarmedium: {
    type: String
  },
  avatarfull: {
    type: String
  },
  personastate: {
    type: Number
  },
  realname: {
    type: String
  },
  primaryclanid: {
    type: String
  },
  timecreated: {
    type: Date
  },
  personastateflags: {
    type: Number
  },
  loccountrycode: {
    type: String
  },
  locstatecode: {
    type: String
  },
  loccityid: {
    type: Number
  },
  recentlyPlayedGames: [
    {
      appid: {
        type: Number
      },
      name: {
        type: String
      },
      playtime2weeks: {
        type: Number
      },
      playtimeForever: {
        type: Number
      },
      imgIconUrl: {
        type: String
      },
      imgLogoUrl: {
        type: String
      },
      playtimeWindowsForever: {
        type: Number
      },
      playtimeMacForever: {
        type: Number
      },
      playtimeLinuxForever: {
        type: Number
      },
      recordCreatedAt: {
        type: Date,
        default: Date.now
      }
    }
  ]
});

steamSchema.plugin(timestamps);

const Steam = mongoose.model("steams", steamSchema);

module.exports.Steam = Steam;
