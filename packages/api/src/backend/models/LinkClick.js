const mongoose = require("mongoose");

const { timestamps } = require("../helpers/timestamp.helper");

// Pull in model reference
require("./User").User;

const { Schema } = mongoose;
const linkClicksSchema = new Schema({
  link: { type: Schema.Types.ObjectId, ref: "links" },
  user: { type: Schema.Types.ObjectId, ref: "users" }
});

linkClicksSchema.plugin(timestamps);

const LinkClick = mongoose.model("linkClicks", linkClicksSchema);

module.exports.LinkClick = LinkClick;
