import { model } from "mongoose";
import { prop, plugin, buildSchema, addModelToTypegoose } from "@typegoose/typegoose";
import { Field as gqlField, ObjectType as gqlObjectType } from "type-graphql";

import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";

@gqlObjectType("Experience", { description: "The Experience Model" })
@plugin(mongoosePaginate)
@plugin(timestamps)
export class Experience {
  @gqlField((_type) => String)
  @prop({ type: () => String })
  name!: string;

  @gqlField((_type) => [String])
  @prop({ type: () => [String] })
  subViews!: string[];
}

const experienceSchema = buildSchema(Experience);

export const ExperienceModel = addModelToTypegoose(model("experiences", experienceSchema), Experience);
