import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IStreak } from "../interfaces/streak";

const schema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "users" },

  // What is the unique streakName?
  streakName: { type: String, index: true },

  // What icon should be shown on the FE to pull this off
  streakIcon: { type: String, index: true },

  // This value is the most recent day that a streak was active.  This is to make it very easy
  // to do a date lookup of the previous day to see if a streak is still active.
  //
  // This should align to the following format:
  // "7/16/2020"
  // new Date().toLocaleDateString("en-US")
  mostRecentActiveStreakDay: { type: String },

  // We should add a cron here and mark all old streaks inactive
  isActive: { type: Boolean, default: true },

  // How long has the streak been going on?
  // Typically this is in "days", but using "duration" in case we want different units
  streakDuration: { type: Number, index: true, default: 1 }
});

// Create multikey index to make sure multiple streaks cant exist
schema.index({ user: 1, streakName: 1, mostRecentActiveStreakDay: 1 }, { unique: true });
schema.index({ createdAt: 1 });
schema.index({ createdAt: -1 });
schema.index({ isActive: 1, streakDuration: 1, streakName: 1 });
schema.index({ user: 1, isActive: 1, streakDuration: 1, streakName: 1 });

schema.plugin(timestamps);

export type StreakModel<T extends Document> = Model<T>;

export const Streak: StreakModel<IStreak> = <StreakModel<IStreak>>model("streak", schema);
