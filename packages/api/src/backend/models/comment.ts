import { Schema, Document, model, PaginateModel } from "mongoose";
import mongooseDelete from "mongoose-delete";
import autopopulate from "mongoose-autopopulate";

import SchemaHelper from "../helpers/schema_helper";
import mongoosePaginate from "../../lib/mongoose-plugins/mongoose-paginate-v2";
import { timestamps } from "../helpers/timestamp.helper";
import WhitelistHelper from "../helpers/whitelist_helper";
import updateFeedRank from "../../lib/mongoose-plugins/update-feed-rank";
import { IComment } from "../interfaces/comment";

const commentSchema = new Schema({
  content: { type: String },
  user: {
    type: Schema.Types.ObjectId,
    ref: "users",
    required: true,
    autopopulate: WhitelistHelper.fieldsForUserAutopopulate
  },
  commentable: {
    type: Schema.Types.ObjectId,
    required: true,
    refPath: "commentableType"
  },
  commentableType: {
    type: String,
    required: true,
    enum: ["feeds"]
  },
  mentions: SchemaHelper.mentionSchema,
  likes: { type: Number, default: 0 },
  giphy: { type: Schema.Types.ObjectId, ref: "giphygifs" },
  parent: { type: Schema.Types.ObjectId, ref: "comments" },
  platform: { type: String, select: false }
});

commentSchema.index({ parent: 1, commentableType: 1, commentable: 1, createdAt: -1 });

commentSchema.plugin(autopopulate);
commentSchema.plugin(mongoosePaginate);

commentSchema.plugin(mongooseDelete, {
  deletedAt: true,
  overrideMethods: ["count", "findOne", "findOneAndUpdate", "updateMany"]
});

commentSchema.plugin(timestamps);
commentSchema.plugin(updateFeedRank, { objectTypeField: "commentableType", objectIdField: "commentable" });

export type CommentModel<T extends Document> = PaginateModel<T> & mongooseDelete.SoftDeleteModel<T>;

// todo: investigate & fix
// @ts-ignore
export const Comment: CommentModel<IComment> = <CommentModel<IComment>>model("comments", commentSchema);
