const mongoose = require("mongoose");

const { Schema } = mongoose;

const { timestamps } = require("../helpers/timestamp.helper");

require("./User").User;

const secureSchema = new Schema({
  password: { type: String, required: true },
  user: {
    type: Schema.Types.ObjectId,
    required: true,
    ref: "users",
    index: true
  },
  history: [
    {
      password: { type: String },
      createdAt: { type: Date },
      updatedAt: { type: Date }
    }
  ],
  snapchat: {
    accessToken: { type: String },
    refreshToken: { type: String }
  },
  google: {
    accessToken: { type: String },
    refreshToken: { type: String }
  },
  bnet: {
    accessToken: { type: String },
    refreshToken: { type: String }
  },
  linkedin: {
    accessToken: { type: String },
    refreshToken: { type: String }
  },
  twitch: {
    accessToken: { type: String },
    refreshToken: { type: String }
  }
});

secureSchema.plugin(timestamps);

const Secure = mongoose.model("secures", secureSchema);

module.exports.Secure = Secure;
