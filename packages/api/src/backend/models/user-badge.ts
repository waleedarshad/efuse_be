import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IUserBadge } from "../interfaces";

const userBadgeSchema = new Schema({
  badge: { type: Schema.Types.ObjectId, ref: "badges", index: true },
  user: { type: Schema.Types.ObjectId, ref: "userbadges", index: true }
});

userBadgeSchema.index({ badge: 1, user: 1 });

userBadgeSchema.plugin(timestamps);

export type UserBadgeModel<T extends Document> = Model<T>;

export const UserBadge: UserBadgeModel<IUserBadge> = <UserBadgeModel<IUserBadge>>(
  model<IUserBadge>("userbadges", userBadgeSchema)
);
