import { model, Schema } from "mongoose";
import { prop, buildSchema, addModelToTypegoose, modelOptions, Severity } from "@typegoose/typegoose";

@modelOptions({ options: { allowMixed: Severity.ALLOW } })
export class Constant {
  // Feature the constant belongs to for example FEED_RANK_ALGORITHM
  @prop({ type: () => String, unique: true, required: true, index: true })
  feature!: String;

  // Making it more generic
  @prop({ type: Schema.Types.Mixed, required: true })
  variables!: Record<string, unknown>;
}

const constantSchema = buildSchema(Constant);
export const ConstantModel = addModelToTypegoose(model("constants", constantSchema), Constant);
