import * as mongoose from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IPathway } from "../interfaces/pathway";

const { Schema } = mongoose;

const pathwaysSchema = new Schema({
  name: { type: String, required: true },
  description: { type: String },
  active: { type: Boolean, default: true, index: true },
  webRedirectUrl: { type: String }
});

pathwaysSchema.plugin(timestamps);

export type PathwayModel<T extends mongoose.Document> = mongoose.PaginateModel<T>;
export const Pathway: PathwayModel<IPathway> = <PathwayModel<IPathway>>(
  mongoose.model<IPathway>("pathways", pathwaysSchema)
);
