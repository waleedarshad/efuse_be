import { Schema, Document, Model, model } from "mongoose";

import { timestamps } from "../helpers/timestamp.helper";
import { IEfusePermission } from "../interfaces";
import { PermissionActionsEnum } from "../../lib/permissions/enums";

const efusePermissionSchema = new Schema({
  subject: { type: String },
  object: { type: String },
  action: { type: String, enum: Object.values(PermissionActionsEnum) },
  tenant: { type: String }
});

efusePermissionSchema.plugin(timestamps);

export type EfusePermissionModel<T extends Document> = Model<T>;
export const EfusePermission: EfusePermissionModel<IEfusePermission> = <EfusePermissionModel<IEfusePermission>>(
  model("efuse_permission", efusePermissionSchema, "efuse_permission")
);
