import { OAuthServiceKind, OwnerKind } from "@efuse/entities";
import { timestamps } from "@efuse/mongoose";
import autopopulate from "mongoose-autopopulate";
import { Schema, Document, Model, model } from "mongoose";

import { IOAuthCredential } from "../interfaces";

const schema = new Schema({
  accessToken: { type: String },
  accessTokenExpiration: { type: String },
  accessTokenSecret: { type: String },
  idToken: { type: String },
  owner: { type: String, index: true, required: true },
  ownerKind: { type: String, enum: Object.values(OwnerKind), default: OwnerKind.USER, required: true },
  refreshToken: { type: String },
  scope: { type: [String] },
  service: { type: String, enum: Object.values(OAuthServiceKind), default: OAuthServiceKind.GOOGLE, required: true },
  tokenType: { type: String }
});

schema.plugin(autopopulate);
schema.plugin(timestamps);

export type OAuthCredentialModel<T extends Document> = Model<T>;

export const OAuthCredential: OAuthCredentialModel<IOAuthCredential> = <OAuthCredentialModel<IOAuthCredential>>(
  model("oauthcredential", schema, "oauthcredential")
);
