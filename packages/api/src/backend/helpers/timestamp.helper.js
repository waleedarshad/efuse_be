const timestamps = (schema) => {
  const { typeKey } = schema.options;

  schema.add({ createdAt: { [typeKey]: Date, default: Date.now, index: true } });
  schema.add({ updatedAt: { [typeKey]: Date, index: true } });

  function setTimestamp(next) {
    this.updatedAt = Date.now();
    next();
  }

  schema.pre("save", setTimestamp);
  schema.pre("update", setTimestamp);
  schema.pre("updateOne", setTimestamp);
  schema.pre("findOneAndUpdate", setTimestamp);
};

module.exports.timestamps = timestamps;
