import { IERenaBracket } from "../interfaces/erena/erena-bracket";

export class ErenaHelper {
  public static formatBracketResponse = (bracket): IERenaBracket => {
    const bracketToFormat = bracket;

    bracketToFormat.rounds = bracketToFormat?.rounds?.map((round) => {
      const newRound = round.round;
      newRound.matches = newRound?.matches?.map((match) => {
        const newMatch = match.match;
        newMatch.teams = newMatch?.teams?.map((team) => {
          return team.team;
        });
        return newMatch;
      });
      return newRound;
    });

    return bracketToFormat;
  };
}
