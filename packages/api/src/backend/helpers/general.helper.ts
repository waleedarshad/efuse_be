import { Failure } from "@efuse/contracts";
import { omit } from "lodash";
import mongoose from "mongoose";
import sanitizeHtml from "sanitize-html";
import crypto from "crypto";
import { Logger } from "@efuse/logger";
import path from "path";
import moment from "moment";

import { CONVERTABLE_IDS } from "./data/convertable-ids";

const logger = Logger.create();

export class GeneralHelper {
  public static saveFile = (file, key, obj) => {
    let buildObj = {};
    buildObj =
      file !== undefined
        ? Object.assign(obj, {
            [key]: GeneralHelper.buildFileObject(file)
          })
        : omit(obj, [key]);
    return buildObj;
  };

  public static buildFileObject = (file) => {
    return file === undefined
      ? {}
      : {
          filename: file[0].originalname,
          contentType: file[0].contentType,
          url: file[0].location
        };
  };

  public static getObjectId = (id) => mongoose.Types.ObjectId(id);

  public static convertToObjectIds = (key, req) => {
    if (req.query.filters) {
      try {
        const filters = JSON.parse(req.query.filters);
        const keys = Object.keys(filters);

        CONVERTABLE_IDS[key].forEach((id) => {
          if (keys.includes(id)) filters[id] = GeneralHelper.getObjectId(filters[id]);
        });

        return filters;
      } catch (error) {
        logger.error(error, `Attempted to parse query: ${JSON.stringify(req.query)}`);
        throw Failure.BadRequest("Query contained invalid filters.");
      }
    }
    return {};
  };

  public static reviver = (key, value) => {
    if (value.toString().indexOf("__REGEXP ") === 0) {
      const m = value.split("__REGEXP ")[1].match(/\/(.*)\/(.*)?/);
      return new RegExp(m[1], m[2] || "");
    }
    return value;
  };

  public static jsonParser = (data) => JSON.parse(data, GeneralHelper.reviver);

  public static generateToken = () => {
    return crypto.randomBytes(32).toString("hex");
  };

  public static validateHtml = (html) =>
    sanitizeHtml(html, {
      allowedTags: [
        "h1",
        "h2",
        "h3",
        "h4",
        "h5",
        "h6",
        "p",
        "b",
        "i",
        "u",
        "strong",
        "a",
        "ul",
        "li",
        "ol",
        "div",
        "hr",
        "br",
        "nl",
        "span"
      ],
      allowedAttributes: {
        a: ["href", "target"]
      },
      selfClosing: ["br", "hr"],
      allowedSchemes: ["http", "https", "mailto"],
      allowedSchemesAppliedToAttributes: ["href"]
    });

  public static constructLearningArticleUrl = (category, article) => `/news/${category.slug}/${article.slug}`;

  public static randomString = (length, chars) => {
    let result = "";

    for (let i = length; i > 0; --i) {
      result += chars[Math.round(Math.random() * (chars.length - 1))];
    }

    return result;
  };

  /**
   * @summary Change filename extension
   *
   * @param {String} filename - Name of the file (ex: picture.png)
   * @param {String} ext - New extension (ex: .jpeg)
   * @return {String} - Returns the filename with new extension
   */

  public static changeExtension = (filename, ext) => {
    return path.join(path.dirname(filename), path.basename(filename, path.extname(filename)) + ext);
  };

  public static convertUnixTimestampToISOString = (timestamp?) => {
    return moment.unix(timestamp).toISOString();
  };

  /**
   * @summary Check whether provide object id is valid or invalid
   *
   * @param objectId - mongoDB id to be checked
   * @return {Boolean} - Return true if valid otherwise false
   */

  public static isValidObjectId = (objectId) => {
    try {
      const { ObjectId } = mongoose.Types;
      // This will return true for any 12 or 24 chars string
      let isValid = ObjectId.isValid(objectId);

      /**
       * Valid ids do not change when casted to an ObjectId but a string that gets a false valid
       * will change when casted to an objectId
       */
      if (isValid) {
        const castedObjectId = new ObjectId(objectId);
        isValid = String(objectId) === String(castedObjectId);
      }

      return isValid;
    } catch (error) {
      logger.error(error, "Invalid ObjectID");
      return false;
    }
  };
}
