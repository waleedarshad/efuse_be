import { isEmpty } from "lodash";
import zxcvbn from "zxcvbn";

import { IPasswordError } from "../interfaces";

export class AuthHelper {
  public validatePassword(password: string | undefined): IPasswordError | null {
    const error: IPasswordError = { password: "Password is required" };

    // Make sure password is present
    if (!password) {
      return error;
    }

    // Make sure password is no less than 8 characters
    if (password.length < 8) {
      error.password = "Password too short. Please make sure to add atleast 8 characters long password";
      error.passwordSuggestions = [
        "Add another word or two. Uncommon words are better.",
        "Straight rows of keys are easy to guess",
        "Use a longer keyboard pattern with more turns"
      ];

      return error;
    }

    const { feedback, score } = zxcvbn(password);

    if (score < 2) {
      error.password = "You have entered a weak password.";

      // When package doesn't return any feedback
      if (isEmpty(feedback)) {
        return error;
      }

      const { warning, suggestions } = feedback;

      // Set warning received from lib
      if (warning) {
        error.password = warning;
      }

      // Set suggestions received from lib
      if (suggestions.length > 0) {
        error.passwordSuggestions = suggestions;
      }

      return error;
    }

    // if it reaches here then password is valid
    return null;
  }
}
