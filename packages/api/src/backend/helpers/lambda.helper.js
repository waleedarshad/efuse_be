const { Logger } = require("@efuse/logger");
const aws = require("aws-sdk");
const logger = Logger.create();

const { AWS } = require("../config/aws-instance");

const invokeLambda = (serviceName, functionName, payload) => {
  const environment = process.env.NODE_ENV || "development";
  const lambda = new AWS.Lambda();
  const params = {
    FunctionName: `${serviceName}-${environment}-${functionName}`,
    InvocationType: "Event",
    Payload: JSON.stringify({
      ...payload
    })
  };
  logger.info("Invoking lambda", params);
  lambda.invoke(params, (err, data) => {
    if (err) logger.info(err, err.stack);
    else logger.info(data);
  });
};

const invokeLambdaWithResponseCallback = (serviceName, functionName, payload, callback) => {
  const environment = process.env.NODE_ENV || "development";
  const lambda = new AWS.Lambda();
  const params = {
    FunctionName: `${serviceName}-${environment}-${functionName}`,
    InvocationType: "RequestResponse",
    Payload: JSON.stringify({
      ...payload
    })
  };
  lambda.invoke(params, callback);
};

module.exports.invokeLambda = invokeLambda;
module.exports.invokeLambdaWithResponseCallback = invokeLambdaWithResponseCallback;
