export const maxAndMinFieldFilterBuilder = (minValue, maxValue) => {
  if (minValue && maxValue) {
    return { $gte: minValue, $lte: maxValue };
  } else if (minValue) {
    return { $gte: minValue };
  } else if (maxValue) {
    return { $lte: maxValue };
  }
  return null;
};
