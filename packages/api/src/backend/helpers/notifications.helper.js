const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const NotificationDispatcherLib = require("../../lib/notifications/dispatcher");
const { sendNotificationEmailAsync } = require("../../lib/notifications/email");
const NotificationUtil = require("../../lib/notifications/utils");
const { sendEmail } = require("../../lib/email");

const { UserSettingsService } = require("../../lib/user-settings/user-settings");
const { UserService } = require("../../lib/users/user.service");

const userSettings = new UserSettingsService();
const userService = new UserService();

const fullName = (user) => user.name;

const differentUsers = (senderId, receiverId) => String(senderId) !== String(receiverId);

const mentionText = (sender, mentions, type) => {
  const length = mentions.length - 1;
  const memberText = length === 1 ? "other" : "others";
  const text = length > 0 ? `you and ${length} ${memberText}` : "you";
  return `<b>${fullName(sender)}</b>  mentioned ${text} in their ${type}`;
};

const dispatchNotification = async (
  content,
  sender,
  receiver,
  notifyable,
  notifyableType,
  permission,
  appendTimeline = false,
  feedId = ""
) => {
  const notification = {
    content,
    sender,
    receiver,
    notifyable,
    notifyableType
  };
  await NotificationDispatcherLib.dispatchSingleNotificationAsync(notification, permission, appendTimeline, feedId);
};

const getCategory = (notificationReason) => {
  switch (notificationReason) {
    case "newMessage":
      return "chatNotifications";
    case "newUserFollow":
      return "followNotifications";
    default:
      return "postNotifications";
  }
};

const templateConstructor = (functionName, sender, type, feedId) => {
  let subject;
  let body;
  let linkText;
  let link;
  switch (functionName) {
    case "newHype":
      subject = `@${sender.username} hyped your ${type}`;
      body = ` hyped up your ${type}`;
      break;
    case "newUserFollow":
      subject = `@${sender.username} started following you`;
      body = " started following you";
      break;
    case "newComment": {
      const isPostComment = type === "comment";
      const bodyText = isPostComment ? "commented on your " : "replied to your ";
      const endText = isPostComment ? "post" : "comment";
      subject = `@${sender.username} ${bodyText}${endText}`;
      body = ` ${bodyText}`;
      linkText = endText;
      link = `https://efuse.gg/home?feedId=${feedId}`;
      break;
    }
    case "newShare":
      subject = `@${sender.username} shared your post`;
      body = " shared your ";
      linkText = "post";
      link = `https://efuse.gg/home?feedId=${feedId}`;
      break;
    default:
      // SendGrid is not enabled for this notification
      return false;
  }
  return {
    subject,
    preHeader: subject,
    body,
    linkText,
    link,
    profile: `@${sender.username}`,
    profileLink: `https://efuse.gg/u/${sender.username}`
  };
};

const sendNotifyEmail = async (receiverId, senderId, notificationReason, feedId = null, type = "") => {
  const emailEnabled = await NotificationUtil.emailNotificationsEnabled();
  if (!emailEnabled) {
    return;
  }
  const userNotificationSettings = await userSettings.get(receiverId, { notifications: 1 });
  const category = getCategory(notificationReason);
  if (userNotificationSettings.notifications.email[category][notificationReason]) {
    const sendUser = await userService.findOne({ _id: senderId });
    const emailData = templateConstructor(notificationReason, sendUser, type, feedId);
    if (emailData) {
      await sendNotificationEmailAsync(receiverId, emailData);
    }
  }
};

const applyNotification = async (sender, notifyable) => {
  const senderId = sender.id;
  const notification = {
    content: `<b>${fullName(sender)}</b> applied for an opportunity.`,
    sender: senderId,
    notifyable,
    notifyableType: "opportunities"
  };
  await NotificationDispatcherLib.applicantNotificationAsync(notification);
};

// todo: investigate
// @ts-ignore
const sendNewOrganizationEmail = async (recipientEmail, organization, sender) => {
  try {
    const orgCreater = await userService.findOne({ _id: sender });

    if (!orgCreater) {
      throw new Error("Unable to find registered creator.");
    }

    const emailData = {
      subject: "A new organization has just signed up at eFuse",
      preHeader: "A new organization has just signed up at eFuse",
      body: `<p style="padding:10px 20px; font-size:16px; color:#a9b5c4; line-height:22px;">
      <a href='https://efuse.gg/u/${orgCreater.username}' style='color:#fff;'>${orgCreater.username}</a> just created a new organization:
      <a href='https://efuse.gg/organizations/show?id=${organization._id}' style='color:#fff;'>
        ${organization.name}</a>
      </p>`
    };

    return await sendEmail(recipientEmail, "d-d92d1cb614c943d4824d3e9f1995eaaa", emailData);
  } catch (error) {
    logger.error({ error });
  }
};

// todo: investigate
// @ts-ignore
const sendNewRecruitedEmail = async (recipientEmail, recruitedId) => {
  const recruitUser = await userService.findOne({ _id: recruitedId });

  // todo: investigate
  // @ts-ignore
  const USERNAME = recruitUser.username;
  try {
    const emailData = {
      subject: "New Pipeline Registration",
      preHeader: "A new Pipeline registration has just signed up on eFuse",
      body: `<p style="padding:10px 20px; font-size:16px; color:#a9b5c4; line-height:22px;">
      <a href='https://efuse.gg/u/${USERNAME}' style='color:#fff;'>${USERNAME}</a> just registered for The Pipeline.`
    };
    return await sendEmail(recipientEmail, "d-d92d1cb614c943d4824d3e9f1995eaaa", emailData);
  } catch (error) {
    logger.error({ error });
  }
};

const followNotification = async (sender, receiverId, notifyable) => {
  const senderId = sender.id;
  if (differentUsers(senderId, receiverId)) {
    await sendNotifyEmail(receiverId, senderId, "newUserFollow");

    await dispatchNotification(
      `<b>${fullName(sender)}</b> started following you.`,
      senderId,
      receiverId,
      notifyable._id,
      "friends",
      "followNotifications.newUserFollow"
    );
  }
};

const hypeNotification = async (sender, receiver, notifyable, type, feedId) => {
  try {
    const senderId = sender.id || sender._id;
    const receiverId = receiver || notifyable.user._id;

    if (differentUsers(senderId, receiverId)) {
      await sendNotifyEmail(receiverId, senderId, "newHype", undefined, type);

      await dispatchNotification(
        `<b>${fullName(sender)}</b> hyped your ${type} up.`,
        senderId,
        receiverId,
        notifyable._id,
        "hypes",
        "postNotifications.newHype",
        true,
        feedId
      );
    }
  } catch (error) {
    logger.error(error);
  }
};

const commentNotification = async (sender, feed, notifyable, homeFeed) => {
  const senderId = sender.id;
  const receiverId = feed.user._id;
  if (differentUsers(senderId, receiverId)) {
    /*
      This breaks the comment, Adding a check so that email notification
      is dispatched only if homeFeed object is present
    */
    if (homeFeed) {
      await sendNotifyEmail(receiverId, senderId, "newComment", homeFeed._id, "comment");
    }
    await dispatchNotification(
      `<b>${fullName(sender)}</b> commented on your post.`,
      senderId,
      receiverId,
      notifyable.comment._id,
      "comments",
      "postNotifications.newComment",
      true,
      feed._id
    );
  }
};

const hypedPostCommentNotification = async (sender, feed, notifyable) => {
  const senderId = sender.id;
  const notification = {
    content: `<b>${fullName(sender)}</b> has commented on a post you've hyped!`,
    sender: senderId,
    notifyable: notifyable.comment._id,
    notifyableType: "comments"
  };
  await NotificationDispatcherLib.hypedPostCommentNotificationAsync(
    notification,
    "hypedPostCommentNotification",
    feed._id
  );
};

const commentReplyNotification = async (sender, comment, notifyable, parentFeedId) => {
  const senderId = sender.id;
  // const receiverId = comment.user._id;
  // TODO Hype Email notifications will be temporarily disabled due to inability to give a reliable link
  // await sendNotifyEmail(receiverId, senderId, "newComment", parentFeedId, "commentReply");

  const notification = {
    content: `<b>${fullName(sender)}</b> replied to your comment.`,
    sender: senderId,
    notifyable: notifyable.comment._id,
    notifyableType: "comments"
  };
  await NotificationDispatcherLib.commentReplyNotificationAsync(
    notification,
    "postNotifications.newComment",
    notifyable.comment.parent,
    comment._id
  );
};

const shareNotification = async (sender, receiverId, notifyable, feedId) => {
  const senderId = sender.id;
  if (differentUsers(senderId, receiverId)) {
    await sendNotifyEmail(receiverId, senderId, "newShare", feedId);

    await dispatchNotification(
      `<b>${fullName(sender)}</b> shared your post.`,
      senderId,
      receiverId,
      notifyable,
      "homeFeeds",
      "postNotifications.newShare"
    );
  }
};

const organizationInvitationNotification = async (sender, notifyable, organization) => {
  const senderId = sender._id;
  const receiverId = notifyable.invitee;
  if (differentUsers(senderId, receiverId)) {
    await dispatchNotification(
      `<b>${fullName(sender)}</b> invited you to join ${organization.name}`,
      senderId,
      receiverId,
      notifyable._id,
      "organizationInvitations",
      "organizationInvitationNotification"
    );
  }
};

const sendTournamentStaffNotification = async (sender, receiver, tournament) => {
  const senderId = sender._id;
  const receiverId = receiver._id;

  if (differentUsers(senderId, receiverId)) {
    await dispatchNotification(
      `You have been added as a staff member for the <b>${tournament.tournamentName}</b> tournament by ${fullName(
        sender
      )}`,
      senderId,
      receiverId,
      tournament._id,
      "erenatournaments",
      null
    );
  }
};

module.exports.applyNotification = applyNotification;
module.exports.commentNotification = commentNotification;
module.exports.commentReplyNotification = commentReplyNotification;
module.exports.differentUsers = differentUsers;
module.exports.dispatchNotification = dispatchNotification;
module.exports.followNotification = followNotification;
module.exports.getCategory = getCategory;
module.exports.hypedPostCommentNotification = hypedPostCommentNotification;
module.exports.hypeNotification = hypeNotification;
module.exports.mentionText = mentionText;
module.exports.organizationInvitationNotification = organizationInvitationNotification;
module.exports.sendNewOrganizationEmail = sendNewOrganizationEmail;
module.exports.sendNewRecruitedEmail = sendNewRecruitedEmail;
module.exports.sendNotifyEmail = sendNotifyEmail;
module.exports.sendTournamentStaffNotification = sendTournamentStaffNotification;
module.exports.shareNotification = shareNotification;
module.exports.templateConstructor = templateConstructor;
