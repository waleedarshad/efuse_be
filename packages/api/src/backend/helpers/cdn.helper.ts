import { URL } from "url";
import { Document, Schema } from "mongoose";
import { Service, Trace } from "../../lib/decorators";

const CDN_MAPPER = [
  { originalHost: "efuse.s3.amazonaws.com", cdnHost: "scontent.efcdn.io" },
  { originalHost: "s3.us-east-1.amazonaws.com", cdnHost: "cdn.efuse.gg" },
  {
    originalHost: "staging-efuse.s3.amazonaws.com",
    cdnHost: "staging-cdn.efuse.gg"
  }
];

@Service("cdn.helper")
export class CdnHelper {
  @Trace({ service: "cdn.helper", name: "cdn" })
  public static cdn(schema: Schema<Document>, fields: string[] = ["file"]): void {
    // Add fields to store original URL

    for (const field of fields) {
      schema.add({ [`${field}OriginalUrl`]: String });
    }

    // Store Original URL in separate field
    // Replace S3 Url with CDN
    function setCdn(next) {
      replaceOriginalURLWithFileURL(this);
      next();
    }

    function setCdnForInsertMany(next, docs) {
      for (const doc of docs) {
        replaceOriginalURLWithFileURL(doc);
      }
      next();
    }

    function replaceOriginalURLWithFileURL(obj) {
      const doc = obj;

      for (const targetedField of fields) {
        const originalUrlFieldKey = `${targetedField}OriginalUrl`;

        if (doc[targetedField] && doc[targetedField].url !== undefined) {
          const originalUrl = new URL(doc[targetedField].url);

          // find corresponding cdn host
          const mapperObject = CDN_MAPPER.find((mapper) => mapper.originalHost === originalUrl.hostname);

          // Store original url & replace with CDN
          if (mapperObject) {
            doc[originalUrlFieldKey] = originalUrl.href;
            originalUrl.hostname = mapperObject.cdnHost;

            // Udating URL
            doc[targetedField].url = originalUrl.href;
          }
        }
      }
    }

    schema.pre("save", setCdn);
    schema.pre("update", setCdn);
    schema.pre("insertMany", setCdnForInsertMany);
  }
}
