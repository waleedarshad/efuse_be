import { maxAndMinFieldFilterBuilder } from "./query.helper";

describe("query.helper", () => {
  describe("maxAndMinFieldFilterBuilder", () => {
    it("returns the correct filter when both min and max values are available", () => {
      const expected = { $gte: 5, $lte: 10 };

      const actual = maxAndMinFieldFilterBuilder(5, 10);

      expect(actual).toEqual(expected);
    });

    it("returns the correct filter when only the min value is available", () => {
      const expected = { $gte: 5 };

      const actual = maxAndMinFieldFilterBuilder(5, null);

      expect(actual).toEqual(expected);
    });

    it("returns the correct filter when only the max value is available", () => {
      const expected = { $lte: 10 };

      const actual = maxAndMinFieldFilterBuilder(null, 10);

      expect(actual).toEqual(expected);
    });
  });
});
