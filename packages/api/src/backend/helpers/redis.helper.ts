import Redis from "ioredis";
import { Logger, ILogger } from "@efuse/logger";

import { REDIS_ENDPOINT, REDIS_PORT } from "../../config";
import { ExpireAsync, GetAsync, SetAsync } from "../types";

export class RedisHelper {
  private _client: Redis.Redis;
  private _logger: ILogger;
  private _expireAsync: ExpireAsync;
  private _getAsync: GetAsync;
  private _setAsync: SetAsync;

  private static _instance: RedisHelper;

  private constructor() {
    this._logger = Logger.create({ name: "redis.helper" });

    if (process.env.JEST_WORKER_ID !== undefined) {
      this._logger.info("[backend/helpers/redis.helper] Detected running in jest.  Not connecting to Redis.");
    }

    if (!REDIS_ENDPOINT) {
      this._logger.fatal("FATAL:  Redis must be configured.");
      process.exit(-1);
    }

    this._client = RedisHelper.newClient();

    this._expireAsync = this._client.expire.bind(this._client);
    this._getAsync = this._client.get.bind(this._client);
    this._setAsync = this.client.set.bind(this._client);

    this._client.on("reconnecting", (event) => {
      this._logger.info("Redis connection re-connecting");
    });

    this._client.on("connect", () => {
      this._logger.info("Redis connection established");
    });

    this._client.on("error", (event) => {
      this._logger.fatal(
        { event },
        "A redis error occured. Make sure redis is up and running. Refer https://github.com/eFuse-Inc/eFuse_BE/blob/master/readme.md#install-and-run-redis-locally"
      );
      process.exit(1);
    });
  }

  public static instance(): RedisHelper {
    if (!this._instance) {
      this._instance = new RedisHelper();
    }

    return this._instance;
  }

  public static newClient = (): Redis.Redis => {
    return new Redis({
      host: REDIS_ENDPOINT,
      keepAlive: 5,
      port: REDIS_PORT,
      reconnectOnError(_err) {
        return true;
      },
      showFriendlyErrorStack: true
    });
  };

  public get client(): Redis.Redis {
    return this._client;
  }

  public get expireAsync(): ExpireAsync {
    return this._expireAsync;
  }

  public get setAsync(): SetAsync {
    return this._setAsync;
  }

  public get getAsync(): GetAsync {
    return this._getAsync;
  }
}
