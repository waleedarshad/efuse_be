import { ErenaHelper } from "./erena-v2.helper";

describe("erena-v2.helper", () => {
  describe("formatBracketsResponse", () => {
    it("returns the correct formatted brackets object", () => {
      const expected = {
        _id: "b1",
        rounds: [
          {
            _id: "r11",
            matches: [
              {
                _id: "m11",
                teams: [
                  {
                    _id: "t11"
                  }
                ]
              }
            ]
          }
        ]
      };

      const bracket = {
        _id: "b1",
        rounds: [
          {
            _id: "r1",
            round: {
              _id: "r11",
              matches: [
                {
                  _id: "m1",
                  match: {
                    _id: "m11",
                    teams: [
                      {
                        _id: "t1",
                        team: { _id: "t11" }
                      }
                    ]
                  }
                }
              ]
            }
          }
        ]
      };

      const actual = ErenaHelper.formatBracketResponse(bracket);

      expect(actual).toEqual(expected);
    });
  });
});
