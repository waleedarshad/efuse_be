import { Types } from "mongoose";
import { isEmpty } from "lodash";
import { ILogger, Logger } from "@efuse/logger";

import { aggregateFeedPost, buildAssociatedFeed } from "../../lib/feeds";
import { IPostFeedItem } from "../interfaces/feed/post-feed-item";
import { IHomeFeed } from "../interfaces/home-feed";
import { IMedia } from "../interfaces/media";
import { IFeedMedia } from "../interfaces/feed/feed-item";
import { IUser } from "../interfaces/user";
import { IFeedUser } from "../interfaces/feed/feed-user";
import { UserBadgeService } from "../../lib/badge/user-badge.service";

import { Friend } from "../models/friend";
import { OrganizationFollower } from "../models/organization-follower";
import { IFeedOrganization } from "../interfaces/feed/feed-organization";
import { ILoungeFeed } from "../interfaces/lounge-feed";
import { IPopulatedUserBadge, IMention } from "../interfaces";

const logger: ILogger = Logger.create({ name: "feed.helper" });

/**
 * @summary Formatted Media
 *
 * @param {Object} media - old media
 * @return {object} feedMedia user
 */
const getMedia = (mediaObjects: IMedia[]): IFeedMedia[] => {
  const feedMedia: IFeedMedia[] = [];
  mediaObjects.forEach((media) => {
    if (media.file) {
      feedMedia.push({
        id: media._id,
        filename: media.file.filename || "",
        contentType: media.file.contentType || "",
        url: media.file.url || ""
      });
    }
  });

  return feedMedia;
};

/**
 * @summary Feed User
 *
 * @param {IUser} user - user object to convert into new Format
 *
 * @returns {Promise<IFeedUser>} feed user user object
 */
const getUser = async (user: IUser): Promise<IFeedUser> => {
  const userBadgeService = new UserBadgeService();

  let badges: IPopulatedUserBadge[] = [];

  // Wrapping in try/catch so that badges doesn't break feed
  try {
    badges = await userBadgeService.findAllByUserId(user._id);
  } catch (error) {
    logger.error(error, "Error while fetching user badges");
    badges = [];
  }

  return {
    name: user.name,
    username: user.username,
    profileImage: user.profilePicture?.url || "",
    verified: user?.verified || false,
    streak: user.streaks?.daily?.streakDuration,
    online: (user.showOnline && user?.online) || false,
    id: <string>(<unknown>user._id),
    badges
  };
};

/**
 * @summary is organization followed
 *
 * @param {Object} userId - UserId
 * @param {Object} currentUserId - currentUserId
 * @return {object} return true if current user is follower
 */
const isOrganizationFollowed = async (organizationId, userId): Promise<boolean | undefined> => {
  const organizationFollower = await OrganizationFollower.findOne({
    followee: organizationId,
    follower: userId
  });
  return !!organizationFollower;
};

/**
 * @summary is current user follower
 *
 * @param {Object} userId - UserId
 * @param {Object} currentUserId - currentUserId
 * @return {object} return true if current user is follower
 */
export const isUserFollowed = async (userId, currentUserId): Promise<boolean> => {
  const friend = await Friend.findOne({
    follower: currentUserId,
    followee: userId
  });

  return !!friend;
};

/**
 * @summary return organization in new format
 * @param feedPost
 * @return formatted organization
 */
export const getOrganization = (org): IFeedOrganization => {
  const organization: IFeedOrganization = {
    profileImage: org?.profileImage?.url || "",
    slug: org?.slug,
    name: org?.name,
    verified: org?.verified?.status,
    id: <string>(<unknown>org?._id)
  };
  return organization;
};

export const getFeedOrganization = (feed): IFeedOrganization | undefined => {
  if (feed?.timelineableType === "organizations") {
    return getOrganization(feed?.associatedTimelineable);
  }
  return;
};

export const isFollowed = async (feedPosts, objectId, currentUserId): Promise<boolean | undefined> => {
  if (objectId && currentUserId) {
    const followed = await (feedPosts.timelineableType === "organizations"
      ? isOrganizationFollowed(objectId, currentUserId)
      : isUserFollowed(objectId, currentUserId));
    return followed;
  }
  return false;
};

export const getFeedKind = (timelineableType: string): string => {
  switch (timelineableType) {
    case "organizations":
      return "orgPost";
    case "users":
      return "userPost";
    default:
      return "post";
  }
};

/**
 * @summary Format feed post
 *
 * @param {Object} feed - Feed data
 * @param {ObjectId} currentUserId - Id of the current user
 * @param {ObjectId} objectId - Organization id or UserId
 * @return {object} feed
 */
export const formatHomeFeedPosts = async (
  feed: IHomeFeed,
  currentUserId?: Types.ObjectId | string
): Promise<IPostFeedItem | null> => {
  // todo: investigate
  // @ts-ignore
  const feedPosts = await aggregateFeedPost(feed, currentUserId);

  if (!isEmpty(feedPosts)) {
    const author = feedPosts.associatedFeed?.author;
    const mentions = feedPosts.associatedFeed?.mentions;
    const { attachments } = feedPosts;

    const formattedFeed: IPostFeedItem = {
      parentFeedId: feedPosts._id,
      kind: getFeedKind(feedPosts.timelineableType),
      text: feedPosts.associatedFeed.content,
      author: author && (await getUser(author)),
      mentions,
      media: attachments && getMedia(attachments),
      openGraph: feedPosts.associatedFeed?.opengraphTags,
      feedId: feedPosts.associatedFeed?._id,
      createdAt: feedPosts.associatedFeed?.createdAt,
      views: feedPosts.associatedFeed?.metadata?.views,
      rankScore: feedPosts.associatedUser?.stats?.hypeScoreRanking,
      comments: feedPosts.associatedFeed?.comments,
      hypes: feedPosts.associatedFeed?.hypes,
      currentUserHypes: feedPosts.associatedFeed?.userHypes,
      organization: getFeedOrganization(feedPosts),
      currentUserFollows: await isFollowed(feedPosts, feed.timelineable, currentUserId),
      associatedGame: feedPosts.associatedGame,
      kindId: feedPosts.timelineable,
      associatedYoutubeVideo: feedPosts.associatedYoutubeVideo
    };
    return formattedFeed;
  }
  return null;
};

/**
 * @summary Format lounge feed post
 *
 * @param {Object} feed - Feed data
 * @param {ObjectId} currentUserId - Id of the current user
 * @param {ObjectId} objectId - Organization id or UserId
 * @return {object} feed
 */
export const formatLoungeFeedPosts = async (
  feed: ILoungeFeed,
  currentUserId?: Types.ObjectId | string
): Promise<IPostFeedItem | null> => {
  // todo: investigate
  // @ts-ignore
  const feedPosts = await buildAssociatedFeed(feed, currentUserId);

  if (!isEmpty(feedPosts)) {
    const user = feedPosts.associatedFeed?.associatedUser;
    const mentions = feedPosts.associatedFeed?.mentions;
    const { attachments } = feedPosts;
    const formattedFeed: IPostFeedItem = {
      parentFeedId: feedPosts._id,
      kind: getFeedKind(feedPosts.timelineableType),
      text: feedPosts.associatedFeed?.content,
      author: user && (await getUser(user)),
      mentions,
      media: attachments && getMedia(attachments),
      openGraph: feedPosts.associatedFeed?.opengraphTags,
      feedId: feedPosts.associatedFeed?._id,
      createdAt: feedPosts.associatedFeed?.createdAt,
      views: feedPosts.associatedFeed?.metadata?.views,
      rankScore: feedPosts.associatedUser?.stats?.hypeScoreRanking,
      comments: feedPosts.associatedFeed?.comments,
      hypes: feedPosts.associatedFeed?.hypes,
      currentUserHypes: feedPosts.associatedFeed?.userHypes,
      organization: getFeedOrganization(feedPosts),
      currentUserFollows: await isFollowed(feedPosts, feed.timelineable, currentUserId),
      associatedGame: feedPosts.associatedGame,
      associatedYoutubeVideo: feedPosts.associatedYoutubeVideo
    };
    return formattedFeed;
  }
  return null;
};

export const sanitizeContent = (text: string | undefined, mentions: IMention[]): string | undefined => {
  if (!text) {
    return undefined;
  }

  let content = text;
  if (mentions && mentions.length > 0 && content) {
    mentions.forEach((mention, index) => {
      const toBeMatched = buildMentionTag(mention);
      content = content.replace(toBeMatched, `${mention.display.substr(1)}`);
    });
  }

  return content;
};
export const buildMentionTag = (mention: IMention) => {
  return mention.type
    ? `@[${mention.display.split("@")[1]}](${mention.id}-${mention.type})`
    : `@[${mention.display.split("@")[1]}](${mention.id})`;
};
