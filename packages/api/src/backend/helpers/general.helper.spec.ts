import mongoose from "mongoose";
import { GeneralHelper } from "./general.helper";

describe("general.helper", () => {
  const file = [{ originalname: "name", contentType: "cont", location: "abc" }];
  const obj = { title: "title", subTitle: "sub", url: "xyz" };

  describe("convertUnixTimestampToISOString", () => {
    it("returns the correct ISO string when valid timestamp is passed in", () => {
      const expected = "2021-04-14T15:30:05.000Z";

      const actual = GeneralHelper.convertUnixTimestampToISOString(1618414205);

      expect(actual).toEqual(expected);
    });

    it("returns null if no valid timestamp", () => {
      const expected = null;

      const actual = GeneralHelper.convertUnixTimestampToISOString();

      expect(actual).toEqual(expected);
    });
  });

  describe("saveFile", () => {
    it("returns the correct object when no file params is passed in", () => {
      const expected = { ...obj };

      const actual = GeneralHelper.saveFile(undefined, "image", obj);

      expect(actual).toEqual(expected);
    });

    it("returns the correct object when params are passed in", () => {
      const expected = {
        image: {
          contentType: "cont",
          filename: "name",
          url: "abc"
        },
        ...obj
      };

      const actual = GeneralHelper.saveFile(file, "image", obj);

      expect(actual).toEqual(expected);
    });
  });

  describe("buildFileObject", () => {
    it("returns the correct object when no file params is passed in", () => {
      const expected = {};

      const actual = GeneralHelper.buildFileObject(undefined);

      expect(actual).toEqual(expected);
    });

    it("returns the correct object when file param is passed in", () => {
      const expected = {
        contentType: "cont",
        filename: "name",
        url: "abc"
      };

      const actual = GeneralHelper.buildFileObject(file);

      expect(actual).toEqual(expected);
    });
  });

  describe("convertToObjectIds", () => {
    it("returns the correct object when no filter param is passed in", () => {
      const expected = {};

      const actual = GeneralHelper.convertToObjectIds("organizations", { query: {} });

      expect(actual).toEqual(expected);
    });

    it("returns the correct object when filter param is passed in", () => {
      const expected = { organization: mongoose.Types.ObjectId("5ee9edfe81ca3851badd5288") };

      const req = {
        query: {
          filters: '{"organization": "5ee9edfe81ca3851badd5288" }'
        }
      };

      const actual = GeneralHelper.convertToObjectIds("opportunities", req);

      expect(actual).toEqual(expected);
    });
  });

  describe("jsonParser", () => {
    it("returns the correct object when filter param is passed in", () => {
      const expected = { role: "student" };

      const actual = GeneralHelper.jsonParser('{"role": "student" }');

      expect(actual).toEqual(expected);
    });
  });

  describe("generateToken", () => {
    it("returns a 32 byte crypto token", () => {
      const mock = jest.fn().mockReturnValue("ad457805fcc8507d287ddf3d144fd74b7ec343f894957244bc1fa11d76b23f1a");
      GeneralHelper.generateToken = mock;
      const actual = GeneralHelper.generateToken();

      expect(actual).toEqual("ad457805fcc8507d287ddf3d144fd74b7ec343f894957244bc1fa11d76b23f1a");
    });
  });

  describe("validateHtml", () => {
    it("returns sanitized HTML", () => {
      const expected = "<p>Never say never</p>";

      const actual = GeneralHelper.validateHtml("<p class='bold'>Never say never</p>");

      expect(actual).toEqual(expected);
    });
  });

  describe("constructLearningArticleUrl", () => {
    it("returns constructed url for the learning article provided category and article slug", () => {
      const expected = "/news/efuse/chlorine";

      const actual = GeneralHelper.constructLearningArticleUrl({ slug: "efuse" }, { slug: "chlorine" });

      expect(actual).toEqual(expected);
    });
  });

  describe("randomString", () => {
    it("returns a random string of provided characters and length", () => {
      const actual = GeneralHelper.randomString(5, "abcde");

      expect(actual).toBeTruthy();
    });
  });

  describe("changeExtension", () => {
    it("returns changed extension of the file", () => {
      const expected = "folder/file.pdf";

      const actual = GeneralHelper.changeExtension("folder/file.txt", ".pdf");

      expect(actual).toEqual(expected);
    });
  });

  describe("isValidObjectId", () => {
    it("returns true when valid object id is passed in", () => {
      const expected = true;

      const actual = GeneralHelper.isValidObjectId("5ee9edfe81ca3851badd5288");

      expect(actual).toEqual(expected);
    });

    it("returns false when invalid object id is passed in", () => {
      const expected = false;

      const actual = GeneralHelper.isValidObjectId("5ee9edfe81ca3851badd52");

      expect(actual).toEqual(expected);
    });
  });
});
