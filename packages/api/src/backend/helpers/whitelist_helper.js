const ALLOWED_USER_FIELDS =
  "_id name roles profilePicture username verified twitchUserName twitch online showOnline showOnlineMobile goldSubscriber goldSubscriberSince stats publicTraitsMotivations bio pathway discordUserUniqueId";

const ALLOWED_USER_PATCH_FIELDS = [
  "address",
  "bio",
  "dateOfBirth",
  "day",
  "email",
  "gender",
  "headerImage",
  "month",
  "name",
  "prevEmail",
  "profilePicture",
  "publicTraitsMotivations",
  "username",
  "year",
  "zipCode"
];

const APPLICANT_FIELDS = `${ALLOWED_USER_FIELDS} educationExperience applicant address`;

const associatedUserFields = () => whitelistAggregateFields();

const fieldsForUserAutopopulate = () => whitelistAutopopulation();

const fieldsForUserAutopopulateInApplicants = () => whitelistAutopopulation(APPLICANT_FIELDS);

const associatedApplicantUserFields = () => whitelistAggregateFields(APPLICANT_FIELDS);

const allowedUserFields = ALLOWED_USER_FIELDS;

const allowedApplicantFields = APPLICANT_FIELDS;

const whitelistAggregateFields = (fields = ALLOWED_USER_FIELDS) => {
  const buildFields = {};
  fields.split(" ").forEach((field) => (buildFields[field] = 1));
  return {
    $project: buildFields
  };
};

const whitelistAutopopulation = (fields = ALLOWED_USER_FIELDS) => {
  return {
    maxDepth: 1,
    select: fields
  };
};

const socialAccountLinkFields = {
  bio: 1,
  traits: 1,
  xboxVerified: 1,
  xboxGamertag: 1,
  snapchatVerified: 1,
  snapchat: 1,
  twitterVerified: 1,
  twitterUsername: 1,
  battlenetVerified: 1,
  linkedinVerified: 1,
  steamVerified: 1,
  twitchVerified: 1,
  discordVerified: 1,
  discordUserName: 1,
  googleVerified: 1,
  google: 1,
  facebookVerified: 1,
  facebookLink: 1,
  youtube: 1,
  tikTok: 1,
  instagramUsername: 1,
  instagramLink: 1,
  pathway: 1
};

const twitchFields =
  "_id username email twitchVerified twitchID twitchUserName twitchDisplayName twitchProfileImage twitchAccessToken twitchRefreshToken twitchAccessTokenExpiresAt twitchScope twitchTokenType twitch twitchLink twitchFollowersCount";

const userPayload = (user) => ({
  id: user.id || user._id,
  username: user.username,
  email: user.email,
  firstRun: user.firstRun,
  profilePicture: user.profilePicture,
  streamChatToken: user.streamChatToken,
  twitch: user.twitch,
  twitchUserName: user.twitchUserName,
  bio: user.bio,
  roles: user.roles,
  traits: user.traits,
  motivations: user.motivations,
  locale: user.locale,
  organizations: user.organizations,
  createdAt: user.createdAt,
  goldSubscriber: user.goldSubscriber,
  goldSubscriberSince: user.goldSubscriberSince,
  stats: user.stats,
  publicTraitsMotivations: user.publicTraitsMotivations,
  verified: user.verified,
  pathway: user.pathway,
  dateOfBirth: user.dateOfBirth,
  name: user.name
});

module.exports.ALLOWED_USER_PATCH_FIELDS = ALLOWED_USER_PATCH_FIELDS;
module.exports.allowedApplicantFields = allowedApplicantFields;
module.exports.allowedUserFields = allowedUserFields;
module.exports.associatedApplicantUserFields = associatedApplicantUserFields;
module.exports.associatedUserFields = associatedUserFields;
module.exports.fieldsForUserAutopopulate = fieldsForUserAutopopulate;
module.exports.fieldsForUserAutopopulateInApplicants = fieldsForUserAutopopulateInApplicants;
module.exports.socialAccountLinkFields = socialAccountLinkFields;
module.exports.twitchFields = twitchFields;
module.exports.userPayload = userPayload;
module.exports.whitelistAggregateFields = whitelistAggregateFields;
module.exports.whitelistAutopopulation = whitelistAutopopulation;
