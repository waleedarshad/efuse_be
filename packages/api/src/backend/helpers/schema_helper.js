const { Schema } = require("mongoose");

const fileSchema = {
  filename: {
    type: String,
    default: "mindblown_white.png"
  },
  contentType: {
    type: String,
    default: "image/png"
  },
  url: {
    type: String,
    default: "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
  }
};

const fileSchemaDefaultsBriefcase = {
  filename: {
    type: String,
    default: "briefcase.png"
  },
  contentType: {
    type: String,
    default: "image/png"
  },
  url: {
    type: String,
    default: "https://efuse.gg/static/images/briefcase.png"
  }
};

const fileSchemaDefaultsNoImage = {
  filename: {
    type: String,
    default: "no_image.jpg"
  },
  contentType: {
    type: String,
    default: "image/jpg"
  },
  url: {
    type: String,
    default: "https://efuse.gg/static/images/no_image.jpg"
  }
};

const createdAtSchema = { type: Date, default: Date.now };
const updatedAtSchema = { type: Date, default: null };

const mentionSchema = [
  {
    childIndex: { type: Number },
    display: { type: String },
    id: { type: Schema.Types.ObjectId, ref: "users" },
    index: { type: Number },
    plainTextIndex: { type: Number },
    type: { type: String }
  }
];

module.exports.createdAtSchema = createdAtSchema;
module.exports.fileSchema = fileSchema;
module.exports.fileSchemaDefaultsBriefcase = fileSchemaDefaultsBriefcase;
module.exports.fileSchemaDefaultsNoImage = fileSchemaDefaultsNoImage;
module.exports.mentionSchema = mentionSchema;
module.exports.updatedAtSchema = updatedAtSchema;
