require("reflect-metadata");

const { Logger } = require("@efuse/logger");
const bodyParser = require("body-parser");

// todo: investigate
// @ts-ignore
require("body-parser-xml")(bodyParser);
const express = require("express");
const passport = require("passport");
const { efuseJwt } = require("./config/passport");
const helmet = require("helmet");
const Sentry = require("@sentry/node");
const useragent = require("express-useragent");
const expressip = require("express-ip");
const http = require("http");
const expressLogger = require("pino-http")({
  redact: ["password", "userParams.password", "user.password", "req.headers.authorization"]
});
const cookieParser = require("cookie-parser");
const createError = require("http-errors");

const { passportHeaderApiKeyStrategy } = require("./config/passport-api-key");
const { VerifyToken } = require("../lib/verify-token.service");

const verifyToken = new VerifyToken();
const logger = Logger.create();

const { ApolloServer } = require("apollo-server-express");

process.on("uncaughtException", (err) => {
  logger.fatal({ err }, `Caught exception: ${err}`);
});

process.on("unhandledRejection", (err) => {
  logger.fatal({ err }, `Caught rejection: ${err}`);
});

const { NODE_ENV } = process.env;
const SSM_ENV_PATH = `/${NODE_ENV || "<MISSING_ENV>"}/env/`;

let app;
let server;

const configure = async () => {
  const { BULLET_TRAIN_ENVIRONMENT, SENTRY_DSN } = require("@efuse/key-store");

  logger.info("Initializing GraphQL schemas");
  const { registerSchemas } = require("../graphql");

  logger.info("Initializing Express application");
  app = express();
  server = http.createServer(app);

  // todo: investigate
  // @ts-ignore
  const io = require("socket.io")(server, { pingInterval: 15000 });
  const redisAdapter = require("socket.io-redis");

  const { FeatureFlagService } = require("../lib/feature-flags/feature-flag.service");
  const { FeatureFlagAsyncService } = require("../lib/feature-flags/feature-flag-async.service");
  const AsyncFeatureFlags = new FeatureFlagAsyncService();
  const { RedisHelper } = require("./helpers/redis.helper");

  logger.info("Registering new models");
  // Registering model
  require("./models/LearningArticleCategory");

  // todo: investigate
  // @ts-ignore
  io.adapter(redisAdapter({ pubClient: RedisHelper.newClient(), subClient: RedisHelper.newClient() }));

  // init sentry
  logger.info("Initializing Sentry");
  Sentry.init({ dsn: SENTRY_DSN, environment: process.env.NODE_ENV });

  // enabled Cors
  require("./middlewares/enable-cors")(app);

  app.use(helmet());
  // todo: investigate
  // @ts-ignore
  app.use(bodyParser.json({ limit: "1mb", extended: true }));
  app.use(bodyParser.urlencoded({ limit: "1mb", extended: true }));

  /**
   * bodyParser.xml should be above bodyParser.text as it produces Error: Non-whitespace before first tag
   *
   * Bug: https://bit.ly/3bfTVtT
   * Ref: https://stackoverflow.com/a/46871847
   */
  // todo: investigate
  // @ts-ignore
  app.use(bodyParser.xml());
  app.use(bodyParser.text());

  app.use(cookieParser());
  app.use(expressLogger);

  /**
   * Since we're behind load balancers that we own this will set a few args
   * properly.
   *
   * @see https://expressjs.com/en/guide/behind-proxies.html
   */
  app.set("trust proxy", true);

  // Make sure we show the correct client IP instead of load balancer/proxy IP
  app.use((req, res, next) => {
    // todo: investigate
    // @ts-ignore
    res.log = req.log = req.log.child({
      "remote-addr": req.headers["x-real-ip"] || req.headers["x-forwarded-for"] || req.connection.remoteAddress
    });

    next();
  });

  app.use((req, res, next) => {
    // todo: investigate
    // @ts-ignore
    res.log = req.log = req.log.child({ "remote-user": req.user ? req.user.id : "-" });

    next();
  });

  app.use(useragent.express());
  app.use(expressip().getIpInfoMiddleware);

  /**
   * Configure passport
   */
  logger.info("Initializing Passport");
  app.use(passport.initialize());

  efuseJwt(passport);
  require("./config/passport-steam")(passport);
  require("./config/passport-linkedin")(passport);
  require("./config/passport-bnet")(passport);
  require("./config/passport-snapchat")(passport);
  require("./config/passport-google")(passport);
  require("./config/passport-twitch")(passport);
  require("./config/passport-state-space")(passport);
  require("./config/passport-riot")(passport);

  const { passportAuth0Strategy } = require("./config/passport-auth0");
  passportAuth0Strategy(passport);
  passportHeaderApiKeyStrategy(passport);

  logger.info("Initializing socket authentication");
  require("./middlewares/authenticate-socket")(io);

  app.use((req, res, next) => {
    // todo: investigate
    // @ts-ignore
    res.io = io;

    if (BULLET_TRAIN_ENVIRONMENT) {
      req.bulletTrain = FeatureFlagService;
    }

    next();
  });

  /**
   * Register routes
   */
  logger.info("registering routes...");
  require("./routes/api")(app);

  const schema = await registerSchemas();

  const apolloServer = new ApolloServer({
    // todo: investigate
    // @ts-ignore
    schema,
    context: async ({ req }) => {
      const token = req.headers.authorization || null;
      // set the user within the context if an authorization header exists
      if (token && token !== "undefined") {
        let user = await verifyToken.validate(token.replace("Bearer ", ""));
        return { user, userId: user._id };
      }
      return;
    }
  });

  apolloServer.applyMiddleware({ app, path: "/api/graphql" });

  // When route doesn't exist
  app.use((req, res, next) => {
    const { params, query, url } = req;

    const message = `The url requested: '${url}', is invalid.`;

    logger.info({ message, url, params, query });

    next(createError(404, message));
  });

  /**
   * Generic error handler
   */
  app.use((err, req, res, next) => {
    // headers are already sent
    if (res.headersSent) {
      return next(err);
    }

    const response = {
      code: err.status || 500,
      message: err.message,
      success: err.success || false,
      ...err.properties
    };

    logger.error(err.message || "an abnormal error occurred");

    return res.status(err.status || 500).json(response);
  });

  // sentry
  app.use(Sentry.Handlers.requestHandler());
  app.use(Sentry.Handlers.errorHandler());

  /**
   * Make sure we refresh our global feature flags on server startup
   */
  AsyncFeatureFlags.refreshGlobalFeatureFlags();

  return { app, server };
};

module.exports.configure = configure;
