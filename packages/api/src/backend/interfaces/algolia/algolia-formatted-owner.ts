import { Types } from "mongoose";

export interface IAlgoliaFormattedOwner {
  _id: Types.ObjectId | string;
  username: string;
  name: string;
  verified: boolean;
}
