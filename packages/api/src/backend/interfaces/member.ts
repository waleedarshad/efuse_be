import { Document, Types } from "mongoose";

export interface IMember extends Document {
  title?: string;
  organization: Types.ObjectId;
  user: Types.ObjectId;
  isBanned?: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
  deleted: boolean;
}
