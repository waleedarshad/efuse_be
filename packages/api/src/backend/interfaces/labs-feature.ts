import { Document } from "mongoose";

export interface ILabsFeature extends Document {
  createdAt: Date;
  description?: string;
  enabled: boolean;
  featureFlag: string;
  label: string;
  referralsRequired?: number;
  updatedAt?: Date;
  visible: boolean;
}
