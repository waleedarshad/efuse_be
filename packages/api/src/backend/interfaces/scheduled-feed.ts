import { Document, Types } from "mongoose";

import { IMention } from "./mention";

export interface IScheduledFeed extends Document {
  _id: Types.ObjectId | string;
  user: Types.ObjectId;
  content: string;
  mentions: IMention[];
  shared: boolean;
  sharedFeed: Types.ObjectId;
  sharedTimeline: Types.ObjectId;
  timelineable: Types.ObjectId;
  timelineableType: string;
  sharedPostUser: Types.ObjectId;
  sharedTimelineable: Types.ObjectId;
  sharedTimelineableType: string;
  verified: boolean;
  crossPosts: string[];
  media: Types.ObjectId;
  mediaObjects: Types.ObjectId[];
  twitchClip: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
  scheduledAt: Date;
  processed: boolean;
  platform: string;
  giphy: Types.ObjectId;
  status: string;
  errorMessage?: string;
  jobSetAt?: Date;
  jobCompletedAt?: Date;
  associatedGame?: Types.ObjectId | string;
  youtubeVideo?: Types.ObjectId | string;
}

export interface IScheduledFeedItem extends IScheduledFeed {
  kind: string;
  kindId?: Types.ObjectId | string;
  parentFeedId: string;
}
