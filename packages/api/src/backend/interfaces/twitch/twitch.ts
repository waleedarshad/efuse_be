import { Document } from "mongoose";

import { ITwitchStream } from "./twitch-stream";
import { ITwitchWebhookSubscription } from "./twitch-webhook-subscription";

export interface ITwitch extends Document {
  viewCount: number;
  displayName: string;
  profileImageUrl: string;
  description: string;
  type: string;
  broadcasterType: string;
  webhookSubscription: ITwitchWebhookSubscription;
  stream: ITwitchStream;
  sendStartStreamMessage: boolean;
  createdAt: Date;
  updatedAt: Date;
}
