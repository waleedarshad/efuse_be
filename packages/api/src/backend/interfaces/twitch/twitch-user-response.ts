export interface ITwitchUserResponse {
  broadcaster_type: string;
  description: string;
  display_name: string;
  id: string;
  login: string;
  offline_image_url: string;
  profile_image_url: string;
  type: string;
  view_count: number;
  email: string;
  created_at: string;
}

export interface ITwitchUserQueryParams {
  id?: string;
  login?: string;
}
