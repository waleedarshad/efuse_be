import { ITwitchPagination } from "./twitch-pagination";

export interface ITwitchClipResponse {
  id: string;
  url: string;
  embed_url: string;
  broadcaster_id: string;
  broadcaster_name: string;
  creator_id: string;
  creator_name: string;
  video_id: string;
  game_id: string;
  language: string;
  title: string;
  view_count: number;
  created_at: string;
  thumbnail_url: string;
  duration: number;
}

export interface ITwitchClipPaginationResponse extends ITwitchPagination {
  data: ITwitchClipResponse[];
}

export interface ITwitchClipOptionalQueryParams {
  after?: string;
  before?: string;
  ended_at?: string;
  first?: number;
  started_at?: string;
}

export interface ITwitchClipBroadcasterQueryParams extends ITwitchClipOptionalQueryParams {
  broadcaster_id: string;
}

export interface ITwitchClipGameQueryParams extends ITwitchClipOptionalQueryParams {
  game_id: string;
}

export interface ITwitchClipIdQueryParams extends ITwitchClipOptionalQueryParams {
  id: string;
}
