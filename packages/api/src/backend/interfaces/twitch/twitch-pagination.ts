export interface ITwitchPagination {
  pagination: { cursor: string };
}
