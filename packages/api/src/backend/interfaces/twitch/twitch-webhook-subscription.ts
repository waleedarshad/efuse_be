export interface ITwitchWebhookSubscription {
  isActive: boolean;
  callback: string;
  expiresAt: Date;
}
