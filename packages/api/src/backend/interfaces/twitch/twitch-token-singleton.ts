import { ObjectId } from "../../../backend/types";

export interface ITwitchTokenSingleton {
  userId?: ObjectId;
  token: string;
  refreshToken: string;
}
