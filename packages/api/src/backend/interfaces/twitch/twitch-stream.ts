import { Types } from "mongoose";

export interface ITwitchStream {
  isLive: boolean;
  title: string;
  game: Types.ObjectId;
  viewerCount: number;
  startedAt: Date;
  endedAt: Date;
  lastLiveAt: Date;
}
