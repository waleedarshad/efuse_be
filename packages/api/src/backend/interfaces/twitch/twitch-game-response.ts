import { ITwitchPagination } from "./twitch-pagination";

export interface ITwitchGameResponse {
  box_art_url: string;
  id: string;
  name: string;
}

export interface ITwitchGameNameQueryParam {
  name: string;
}

export interface ITwitchGameIdQueryParam {
  id: string;
}

export interface ITwitchGameQueryParams {
  name: string;
  id: string;
}

export interface ITwitchGamePaginationResponse extends ITwitchPagination {
  data: ITwitchGameResponse[];
}
