import { Optional } from "@efuse/types";
import { Document } from "mongoose";
import { ITwitchStream, ITwitchWebhookSubscription } from "./twitch";

export interface ITwitchAccountInfo extends Document {
  accountEmail: Optional<string>;
  accountId: Optional<string>;
  accountType: Optional<string>;
  broadcasterType: Optional<string>;
  description: Optional<string>;
  displayName: Optional<string>;
  followers: Optional<number>;
  kind: string;
  link: Optional<string>;
  name: Optional<string>;
  owner: string;
  ownerKind: string;
  picture: Optional<string>;
  profileBannerImage: Optional<string>;
  profileImage: Optional<string>;
  sendStartStreamMessage: Optional<boolean>;
  stream: Optional<ITwitchStream>;
  username: Optional<string>;
  verified: Optional<boolean>;
  verifiedByTwitch: Optional<boolean>;
  viewCount: Optional<number>;
  webhookSubscription: Optional<ITwitchWebhookSubscription>;
}
