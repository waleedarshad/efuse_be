import { Document, Types } from "mongoose";
import { IFeedAggregate } from "./feed";

export interface ILoungeFeed extends Document {
  _id: Types.ObjectId;
  user: Types.ObjectId;
  feed: Types.ObjectId;
  timelineable: Types.ObjectId;
  timelineableType: string;
  userVerified: boolean;
  published: boolean;
  gold: boolean;
  createdAt?: Date;
  updatedAt?: Date;
  original?: boolean;
}

export interface ILoungeAggregate extends ILoungeFeed {
  associatedFeed: IFeedAggregate;
}
