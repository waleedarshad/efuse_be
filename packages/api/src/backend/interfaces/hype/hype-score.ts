export interface HypeScoreCalculation {
  hypeScore: number;
  totalFeedComments: number;
  totalFeedCommentHypes: number;
  totalHypes: number;
  postViews: number;
}
