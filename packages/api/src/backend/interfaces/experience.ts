import { Document } from "mongoose";

export interface IExperience extends Document {
  name: string;
  subViews: string[];
}
