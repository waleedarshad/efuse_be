import { Document, Types } from "mongoose";

export interface ILearningArticleGame extends Document {
  game: Types.ObjectId | string;
  learningArticle: Types.ObjectId | string;
}
