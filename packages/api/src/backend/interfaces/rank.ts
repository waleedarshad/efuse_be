import { Document, Types } from "mongoose";

export interface IRank extends Document {
  document: Types.ObjectId;
  documentType: string;
  rankValue: number;
  createdAt?: Date;
  updatedAt?: Date;
}
