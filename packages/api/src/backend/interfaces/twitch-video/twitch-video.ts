import { Document, Types } from "mongoose";

export interface ITwitchVideo extends Document {
  id: string;
  user: Types.ObjectId | string;
  video_id: string;
  user_id: string;
  user_name: string;
  title: string;
  description: string;
  created_at: Date;
  published_at: Date;
  url: string;
  thumbnail_url: string;
  viewable: string;
  view_count: string;
  language: string;
  type: string;
  duration: string;
  jobRanAt: Date;
  createdAt: Date;
}
