import { LeagueJoinStatus } from "@efuse/entities";
import { Document } from "mongoose";
import { ILeague } from "./league";

export interface ILeagueWithJoinStatus extends Document {
  league: ILeague;
  joinStatus: LeagueJoinStatus;
}
