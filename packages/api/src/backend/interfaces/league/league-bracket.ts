import { LeagueBracketType } from "@efuse/entities";
import { Document, Types } from "mongoose";

export interface ILeagueBracket extends Document {
  name: string;
  type: LeagueBracketType;
  pool: Types.ObjectId;
}
