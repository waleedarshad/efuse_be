import { ObjectId } from "../../types";

/**
 * This will be generated on the fly
 */
export interface ILeagueScoreboard {
  team: ObjectId;
  wins: number;
  losses: number;
}
