import { Types } from "mongoose";
import { ILeagueScoreboard } from "./league-scoreboard";

/**
 * This will be generated on the fly
 */
export interface ILeagueScoreboardWithPool {
  pool: Types.ObjectId;
  scoreboard: ILeagueScoreboard[];
}
