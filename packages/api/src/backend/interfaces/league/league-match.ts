import { LeagueMatchState } from "@efuse/entities";
import { Document, Types } from "mongoose";

export interface ILeagueMatch extends Document {
  round: Types.ObjectId;
  teams: Types.ObjectId[];
  winner: Types.ObjectId;
  state: LeagueMatchState;
  startTime: Date;
  endTime: Date;
}
