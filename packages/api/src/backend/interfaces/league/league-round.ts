import { Document, Types } from "mongoose";

export interface ILeagueRound extends Document {
  name: string;
  bracket: Types.ObjectId;
  value: number;
  startDate: Date;
}
