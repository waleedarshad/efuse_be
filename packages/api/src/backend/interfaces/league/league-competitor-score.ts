import { LeagueScoreCompetitorType, LeagueScoreCreatorType } from "@efuse/entities";
import { Document, Types } from "mongoose";

export interface ILeagueCompetitorScore extends Document {
  creator: Types.ObjectId;
  creatorType: LeagueScoreCreatorType;
  value: number;
  image: string;
  competitor: Types.ObjectId;
  competitorType: LeagueScoreCompetitorType;
  gameScore: Types.ObjectId;
}
