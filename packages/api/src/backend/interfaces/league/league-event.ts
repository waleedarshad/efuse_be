import { LeagueBracketType, LeagueEventState, LeagueEventTimingMode } from "@efuse/entities";
import { Document, Types } from "mongoose";

export interface ILeagueEvent extends Document {
  bracketType: LeagueBracketType;
  description: string;
  details: string;
  gamesPerMatch: number;
  league: Types.ObjectId;
  maxTeams: number;
  name: string;
  rules: string;
  startDate: Date;
  state: LeagueEventState;
  teams: Types.ObjectId[];
  timingMode: LeagueEventTimingMode;
}
