import { Document, Types } from "mongoose";
import { LeagueEntryType, LeagueState, OwnerType } from "@efuse/entities";

export interface ILeague extends Document {
  name: string;
  imageUrl: string;
  description: string;
  owner: Types.ObjectId;
  ownerType: OwnerType;
  createdAt: Date;
  updatedAt: Date;
  entryType: LeagueEntryType;
  game: Types.ObjectId;
  teams: Types.ObjectId[];
  state: LeagueState;
  rules: string;
}
