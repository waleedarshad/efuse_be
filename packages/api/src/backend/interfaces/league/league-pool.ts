import { Document, Types } from "mongoose";

export interface ILeaguePool extends Document {
  name: string;
  teams: Types.ObjectId[];
  event: Types.ObjectId;
}
