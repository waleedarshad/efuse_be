import { LeagueScoreCreatorType, LeagueGameScoreType } from "@efuse/entities";
import { Document, Types } from "mongoose";

export interface ILeagueGameScore extends Document {
  creator: Types.ObjectId;
  creatorType: LeagueScoreCreatorType;
  image: string;
  match: Types.ObjectId;
  type: LeagueGameScoreType;
  gameNumber: number;
}
