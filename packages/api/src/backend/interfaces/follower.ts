import { ObjectId } from "../types";

export interface IFollower {
  id: ObjectId;
  name: string;
}
