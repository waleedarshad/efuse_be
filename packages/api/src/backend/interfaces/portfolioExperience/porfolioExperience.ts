import { Types, Document } from "mongoose";

export interface IPortfolioExperience extends Document {
  _id: Types.ObjectId;
  user: Types.ObjectId;
  company: string;
  jobTitle: string;
  startDate: Date;
  endDate: Date;
  present: boolean;
  projects: [
    {
      title: string;
      roles: [string];
      description: string;
    }
  ];
}
