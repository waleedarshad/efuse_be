export enum NotifyableType {
  MESSAGES = "messages",
  FRIENDS = "friends",
  LIKES = "likes",
  COMMENTS = "comments",
  FEEDS = "feeds",
  HOME_FEEDS = "homeFeeds",
  OPPORTUNITIES = "opportunities",
  ORGANIZATION_INVITATIONS = "organizationInvitations",
  ERENA_TOURNAMENTS = "erenatournaments",
  HYPES = "hypes"
}

export interface IBrazeWebhookBody {
  content: string;
  sender: string;
  receiver: string;
  notifyable: string;
  notifyableType: NotifyableType;
  timeline?: string;
}
