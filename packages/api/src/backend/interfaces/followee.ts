import { ObjectId } from "../types";

export interface IFollowee {
  id: ObjectId;
}
