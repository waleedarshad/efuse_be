import { Types, Document } from "mongoose";

export interface IUserStats extends Document {
  user: Types.ObjectId;
  hypeScore: number;
  hypeScoreUpdatedAt?: Date;
  hypeScoreRanking?: number;
  hypeScorePercentile?: number;
  postViews: number;
  totalEngagements: number;
  totalHypes?: number;
}
