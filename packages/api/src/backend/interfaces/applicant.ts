import { Document, Types } from "mongoose";

import { ObjectId } from "../types";
import { File } from "./file";
import { IUser } from "./user";

interface ICandidateQuestion {
  index: number;
  question: string;
  media: File;
}

export interface IApplicant extends Document {
  user: Types.ObjectId | string | IUser;
  _id?: ObjectId;
  profilePicture?: File;

  /** @deprecated In favor of entity & entityType field  */
  opportunity: Types.ObjectId | string;

  candidateQuestions: ICandidateQuestion[];
  status: string;
  createdAt?: Date;
  updatedAt?: Date;
  entity: Types.ObjectId | string;
  entityType: string;
  favorite?: boolean;
}
