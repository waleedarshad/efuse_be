import { Document, Types } from "mongoose";

export interface IStarredRecruit extends Document {
  recruitmentProfile: Types.ObjectId;
  recruiterUser: Types.ObjectId;
  ownerType: string;
  owner: Types.ObjectId;
  updatedAt?: Date;
  createdAt?: Date;
}
