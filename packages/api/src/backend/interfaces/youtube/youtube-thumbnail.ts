export interface IYoutubeThumbnail {
  url: string;
  width: number;
  height: number;
}
