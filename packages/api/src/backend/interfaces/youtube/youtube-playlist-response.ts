import { IYoutubeThumbnail } from "./youtube-thumbnail";

export interface IYoutubePlaylistItem {
  kind: string;
  etag: string;
  id: string;
  snippet: {
    publishedAt: Date;
    channelId: string;
    title: string;
    description: string;
    thumbnails: {
      default: IYoutubeThumbnail;
      medium: IYoutubeThumbnail;
      high: IYoutubeThumbnail;
      standard: IYoutubeThumbnail;
      maxres: IYoutubeThumbnail;
    };
    channelTitle: string;
    playlistId: string;
    position: number;
    resourceId: {
      kind: string;
      videoId: string;
    };
    videoOwnerChannelTitle: string;
    videoOwnerChannelId: string;
  };
  contentDetails: {
    videoId: string;
    videoPublishedAt: Date;
  };
}

export interface IYoutubePlaylistResponse {
  kind: string;
  etag: string;
  nextPageToken?: string;
  prevPageToken?: string;
  pageInfo: {
    totalResults: number;
    resultsPerPage: number;
  };
  items: IYoutubePlaylistItem[];
}
