import { Document } from "mongoose";
import { ILivestreamData } from "./livestream-data";

export interface ILivestream extends Document {
  type: string;
  data: ILivestreamData;
}
