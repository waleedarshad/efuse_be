import { ILivestreamGame } from "./livestream-game";
import { IUser } from "../user";

export interface ILivestreamData {
  _id: IUser;
  user: string;
  platform: string;
  startedAt: Date;
  endedAt: Date;
  streamTitle: string;
  viewers: number;
  views: number;
  thumbnailUrl: string;
  twitchUserAvatar: string;
  game: ILivestreamGame;
}
