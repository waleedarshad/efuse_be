export interface ILivestreamGame {
  id: number;
  name: string;
  box_art_url: string;
}
