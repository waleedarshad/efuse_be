import { IGame } from "../game";
import { IFeedGame } from "./feed-game";

export interface IPopulatedFeedGame extends Omit<IFeedGame, "game"> {
  game: IGame;
}
