import { Document, Types } from "mongoose";

export interface IFeedGame extends Document {
  game: Types.ObjectId | string;
  feed: Types.ObjectId | string;
  createdAt?: Date;
  updatedAt?: Date;
}
