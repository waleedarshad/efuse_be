import { IFeedItem } from "./feed-item";
import { IPostFeedItem } from "./post-feed-item";

// Feed is a list of FeedItem
export interface IFeedList {
  docs: (IFeedItem | IPostFeedItem)[];
}
