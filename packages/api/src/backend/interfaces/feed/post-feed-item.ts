import { Types } from "mongoose";

import { IGame } from "..";
import { ILink } from "../link";
import { IMention } from "../mention";
import { IFeedItem, IFeedMedia } from "./feed-item";
import { IFeedOrganization } from "./feed-organization";
import { IFeedUser } from "./feed-user";
import { YoutubeVideo } from "../../../backend/models/youtube-video/youtube-video.model";
import { Giphy } from "../../models/giphy/giphy.model";
import { ObjectId } from "../../types";

export interface IPostFeedItem extends IFeedItem {
  /**
   * The id of the parent feed (HomeFeed or LoungeFeed)
   */
  parentFeedId: string;
  kind: string; // post type default post
  text: string; // Body of the post
  author?: IFeedUser;
  organization?: IFeedOrganization;
  mentions?: IMention[];
  media?: IFeedMedia[];
  openGraph?: ILink;
  currentUserHypes?: number;
  currentUserFollows?: boolean;
  feedRank?: number;
  associatedGame?: IGame;
  kindId?: Types.ObjectId | string;
  original?: boolean;
  associatedYoutubeVideo: YoutubeVideo;
  giphy?: ObjectId | Giphy;
  twitchClip?: string | Types.ObjectId;
}
