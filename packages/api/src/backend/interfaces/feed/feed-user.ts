import { IPopulatedUserBadge } from "../badge";

export interface IFeedUser {
  name: string;
  username: string;
  profileImage: string;
  verified: boolean;
  streak: number;
  online: boolean;
  id: string;
  badges: IPopulatedUserBadge[];
}
