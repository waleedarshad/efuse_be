import { Types } from "mongoose";

export interface IFeedMedia {
  id: string;
  filename: string;
  contentType: string;
  url: string;
}

export interface IFeedItem {
  kind: string;
  feedId: Types.ObjectId;
  createdAt?: Date;
  views?: number;
  rankScore?: number;
  comments?: number;
  hypes?: number;
}
