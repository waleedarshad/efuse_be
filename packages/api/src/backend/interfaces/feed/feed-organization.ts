export interface IFeedOrganization {
  id: string;
  profileImage: string;
  slug: string;
  name: string;
  verified: boolean;
}
