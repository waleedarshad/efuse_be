import { IUser } from "../user";

export interface IMagicLoginResponse {
  token: string;
  refreshToken: string;
  user: IUser;
  message: string;
}
