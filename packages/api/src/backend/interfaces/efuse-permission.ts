import { Document } from "mongoose";
import { PermissionActionsEnum } from "../../lib/permissions/enums/permission-actions.enum";

export interface IEfusePermission extends Document {
  subject: string;
  object: string;
  action: PermissionActionsEnum;
  tenant: string;
}
