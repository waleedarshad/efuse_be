export interface IPasswordError {
  password: string;
  passwordSuggestions?: string[];
}
