import { Document, Types, LeanDocument } from "mongoose";
import { DocumentType } from "@typegoose/typegoose";

import { Giphy } from "../models/giphy/giphy.model";
import { Hype } from "../models/hype/hype.model";

import { File } from "./file";
import { IMedia } from "./media";
import { IMention } from "./mention";

// todo: investigate & fix
// @ts-ignore
export interface IComment extends Document {
  content?: string;
  user: Types.ObjectId | string;
  commentable: Types.ObjectId | string;
  commentableType: string;
  mentions?: IMention[];
  likes: number;
  giphy?: Types.ObjectId | string;
  parent?: Types.ObjectId | string | null;
  deleted: boolean;
  platform: string;
  replyCount: number;
  hypesCount: number;
  totalEngagement: number;
  thread: {
    docs?: IComment[];
    totalDocs?: number;
    limit?: number;
    page?: number;
    totalPages?: number;
    nextPage?: number;
    prevPage?: number;
    pagingCounter?: boolean;
    hasPrevPage?: boolean;
    hasNextPage?: boolean;
  };
  like?: Hype | LeanDocument<DocumentType<Hype>>;
  associatedGiphy: Giphy;
  associatedMedia: IMedia;
}

export interface ICommentBody
  extends Omit<IComment, "user" | "commentable" | "commentableType" | "likes" | "deleted" | "giphy"> {
  feedId: Types.ObjectId | string;
  file: File;
  giphy: Giphy;
}
