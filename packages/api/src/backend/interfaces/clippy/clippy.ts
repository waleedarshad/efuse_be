import { Document, Types } from "mongoose";

export interface IClippy extends Document {
  user: Types.ObjectId;
  url: string;
  shoutout: string;
}
