export interface IIpInfo {
  ip: string;
  range: number[];
  country: string;
  region: string;
  eu: string;
  timezone: string;
  city: string;
  ll: number[];
  metro: number[];
  area: number[];
  error: string;
}
