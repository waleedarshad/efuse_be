import { Document, Types } from "mongoose";

interface IPasswordHistory {
  password: string;
  createdAt: Date;
  updatedAt: Date;
}

interface IAuthToken {
  accessToken: string;
  refreshToken: string;
}

export interface ISecure extends Document {
  password: string;
  user: Types.ObjectId;
  history: IPasswordHistory[];
  snapchat: IAuthToken;
  google: IAuthToken;
  bnet: IAuthToken;
  linkedin: IAuthToken;
  twitch: IAuthToken;
  createdAt?: Date;
  updatedAt?: Date;
}
