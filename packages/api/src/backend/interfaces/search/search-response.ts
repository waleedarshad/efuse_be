import { ISearch } from "./search";

export interface ISearchResponseObject {
  count: number;
  docs: ISearch[];
}

export interface ISearchResponse {
  users: ISearchResponseObject;
}
