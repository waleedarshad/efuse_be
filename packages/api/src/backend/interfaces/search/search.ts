import { Types } from "mongoose";

export interface ISearch {
  _id: Types.ObjectId | string;
  f_id: Types.ObjectId | string;
  name: string;
  profilepicture: string;
  profilePicture: { url: string };
  score: number;
  type: string;
  url: string;
  username: string;
  verified: boolean;
}
