export interface IOAuthRequestTokenResponse {
  oauth_callback_confirmed: string;
  oauth_token_secret: string;
  oauth_token: string;
}
