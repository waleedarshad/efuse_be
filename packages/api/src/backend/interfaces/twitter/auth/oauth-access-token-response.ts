export interface IOAuthAccessTokenResponse {
  oauth_token_secret: string;
  oauth_token: string;
  screen_name: string;
  user_id: string;
}
