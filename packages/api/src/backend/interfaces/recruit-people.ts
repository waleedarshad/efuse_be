import { Document, Types } from "mongoose";

export interface IRecruitPeople extends Document {
  recruitmentProfile: Types.ObjectId;
  name: string;
  type: string;
  phone?: string;
  email?: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export interface IRecruitPeopleRequestBody {
  name: string;
  type: string;
  phone?: string;
  email?: string;
}

export interface IRecruitPeopleUpdateBody {
  name?: string;
  type?: string;
  phone?: string;
  email?: string;
}
