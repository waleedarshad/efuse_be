import { Document } from "mongoose";

export interface StatInfo extends Document {
  rank: string;
  percentile: {
    type: number;
  };
  displayName: string;
  displayCategory: string;
  category: string;
  metadata: unknown;
  value: number;
  displayValue: string;
  displayType: string;
}

export interface IStatsInfo {
  displayValue: string;
  percentile: number;
}

export interface IOverwatchAvailableSegment extends Document {
  type: string;
  attributes: {
    realm: string;
  };
  metadata: {
    name: {
      type: string;
    };
  };
  expiryDate: {
    type: string;
  };
}

export interface IOverwatchSegmentResponse extends Document {
  type: string;
  attributes: { realm: string };
  metadata: { name: string };
  expiryDate: string;
  stats: {
    timePlayed: StatInfo;
    wins: StatInfo;
    matchesPlayed: StatInfo;
    timeSpentOnFire: StatInfo;
    cards: StatInfo;
    wlPercentage: StatInfo;
    medals: StatInfo;
    goldMedals: StatInfo;
    silverMedals: StatInfo;
    bronzeMedals: StatInfo;
    multiKills: StatInfo;
    soloKills: StatInfo;
    objectiveKills: StatInfo;
    environmentalKills: StatInfo;
    finalBlows: StatInfo;
    damageDone: StatInfo;
    healingDone: StatInfo;
    eliminations: StatInfo;
    deaths: StatInfo;
    kd: StatInfo;
    kg: StatInfo;
    objectiveTime: StatInfo;
    defensiveAssists: StatInfo;
    offensiveAssists: StatInfo;
    mostEliminations: StatInfo;
    mostFinalBlows: StatInfo;
    mostDamageDone: StatInfo;
    mostHealingDone: StatInfo;
    mostDefensiveAssists: StatInfo;
    mostOffensiveAssists: StatInfo;
    mostObjectiveKills: StatInfo;
    mostObjectiveTime: StatInfo;
    mostSoloKills: StatInfo;
    mostTimeSpentOnFire: StatInfo;
  };
}
