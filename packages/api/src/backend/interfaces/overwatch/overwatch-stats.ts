import { Document } from "mongoose";
import { IOverwatchAvailableSegment, IOverwatchSegmentResponse } from "./overwatch-segment-data";

export interface IOverwatchStatsResponse extends Document {
  platformInfo: {
    platformSlug: string;
    platformUserId: string;
    platformUserIdentifier: string;
    avatarUrl: string;
    additionalParameters: string;
    platformUserHandle: string;
  };
  userInfo: {
    userId: number;
    isPremium: boolean;
    isVerified: boolean;
    isInfluencer: boolean;
    countryCode: string;
    customAvatarUrl: string;
    customHeroUrl: string;
    socialAccounts: string;
    pageviews: string;
    isSuspicious: string;
  };
  metadata: unknown;
  segments: IOverwatchSegmentResponse[];
  availableSegments: IOverwatchAvailableSegment[];
  expiryDate: string;
}
