import { Document, Types } from "mongoose";
import { InviteCodeType, InviteResourceType } from "@efuse/entities";
import { IUser } from "./user";
import { IOrganization } from "./organization";

export interface IInviteCode extends Document {
  code: string;
  type: InviteCodeType;
  user: Types.ObjectId | IUser;
  organization: Types.ObjectId | IOrganization;
  resource: Types.ObjectId;
  resourceType: InviteResourceType;
  active: boolean;
}
