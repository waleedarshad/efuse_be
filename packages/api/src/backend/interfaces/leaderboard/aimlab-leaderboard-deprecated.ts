import { AimlabProfileDto } from "../../models/aimlab";
import { ILeaderboardCommon } from "./leaderboard-common";

/**
 * This interface will be deprecated once the new pipeline features are complete.
 */
export interface IAimlabLeaderboardDeprecated extends ILeaderboardCommon {
  stats: AimlabProfileDto;
}
