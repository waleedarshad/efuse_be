import { Document, Types } from "mongoose";

/**
 * The base for the pipeline leaderboards. Applys to Aimlab, League of legends and Valorant
 */
export interface ILeaderboardBase extends Document {
  rank: number;
  userId: Types.ObjectId;
  recruitmentProfileId: Types.ObjectId;
}
