import { Types } from "mongoose";
import { ILeaderboardBase } from "./leaderboard-base";

/**
 * Will replace aimlab leaderboard deprecated
 *
 * Represents a user on the eFuse aimlab pipeline leaderboard
 */
export interface IAimlabLeaderboard extends ILeaderboardBase {
  aimlabStatsId: Types.ObjectId;
}
