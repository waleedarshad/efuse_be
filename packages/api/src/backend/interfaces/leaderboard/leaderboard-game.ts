import { PaginateResult } from "mongoose";
import { IGame } from "../game";
import { ILeaderboardBase } from "./leaderboard-base";

export interface ILeaderboardGame {
  game: IGame;
  leaderboard: PaginateResult<ILeaderboardBase>;
}
