import { Document } from "mongoose";
import { IUser } from "../user";

export interface ILeaderboardCommon extends Document {
  city: string;
  graduationClass: string;
  state: string;
  country: string;
  user: IUser;
}
