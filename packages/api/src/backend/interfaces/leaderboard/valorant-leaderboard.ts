import { Types } from "mongoose";
import { ILeaderboardBase } from "./leaderboard-base";

/**
 * Will replace valorant leaderboard deprecated
 *
 * Represents a user on the eFuse valorant pipeline leaderboard
 */
export interface IValorantLeaderboard extends ILeaderboardBase {
  valorantStatsId: Types.ObjectId;
}
