import { OverwatchStats } from "../../../backend/models/overwatch/overwatch.model";
import { ILeaderboardCommon } from "./leaderboard-common";

export interface IOverwatchLeaderboard extends ILeaderboardCommon {
  overwatchStats: OverwatchStats;
}
