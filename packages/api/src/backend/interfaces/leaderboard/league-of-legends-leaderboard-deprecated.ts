import { ILeagueOfLegendsStats } from "../leagueOfLegends";
import { ILeaderboardCommon } from "./leaderboard-common";

/**
 * This interface will be deprecated once the new pipeline features are complete.
 */
export interface ILeagueOfLegendsLeaderboardDeprecated extends ILeaderboardCommon {
  leagueOfLegendsStats: ILeagueOfLegendsStats;
}
