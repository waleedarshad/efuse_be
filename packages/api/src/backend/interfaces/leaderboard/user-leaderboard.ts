import { Types } from "mongoose";

export interface ILeaderboard {
  placement: number;
  placementPercent: number;
  user: Types.ObjectId;
  streak: Types.ObjectId;
  isActive: boolean;
  boardType: string;
  stats: Types.ObjectId;
  valueAtTheTimeOfCreation: number;

  // This will be deleted once we migrate data
  streakDuration: number;
}

export interface IUserLeaderboard {
  leaderboard: ILeaderboard;
  activeLeaderboard: ILeaderboard;
}
