import { Types } from "mongoose";
import { ILeaderboardBase } from "./leaderboard-base";

/**
 * Will replace league of legends leaderboard deprecated
 *
 * Represents a user on the eFuse league of legends pipeline leaderboard
 */
export interface ILeagueOfLegendsLeaderboard extends ILeaderboardBase {
  leagueOfLegendsStatsId: Types.ObjectId;
}
