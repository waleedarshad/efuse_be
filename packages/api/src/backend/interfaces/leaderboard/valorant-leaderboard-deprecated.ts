import { IValorantStats } from "../valorant/valorant-stats";
import { ILeaderboardCommon } from "./leaderboard-common";

/**
 * This interface will be deprecated once the new pipeline features are complete.
 */
export interface IValorantLeaderboardDeprecated extends ILeaderboardCommon {
  stats: IValorantStats;
}
