import { Types } from "mongoose";

export interface IUserOverwatchStats {
  nickname: string;
  platform: string;
  stats?: Types.ObjectId | string | undefined;
}
