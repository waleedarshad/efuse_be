import { Document, Types } from "mongoose";
import { ERenaScoringKinds, ERenaTournamentOwnerKinds, ERenaTournamentStatus, ERenaType } from "./erena-enums";

export interface IERenaTournament extends Document {
  slug: string;
  tournamentId: string;
  tournamentName: string;
  hideLeaderboard: boolean;
  totalTeams: number;
  totalRounds: number;
  logoURL: string;
  playersPerTeam: number;
  currentRound: number;
  isFinal: boolean;
  startDatetime: Date;
  endDatetime: Date;
  brandName: string;
  brandLogoUrl: string;
  logoUrl: string;
  landingPageUrl: string;
  extensionLinkoutUrl: string;
  twitchChannel: string;
  youtubeChannel: string;
  rulesMarkdown: string;
  status: ERenaTournamentStatus | ERenaTournamentStatus[];
  type: ERenaType;
  imageUrl1: string;
  imageUrl2: string;
  imageUrl3: string;
  imageUrl4: string;
  imageUrl5: string;
  imageUrl6: string;
  imageLink1: string;
  imageLink2: string;
  imageLink3: string;
  imageLink4: string;
  imageLink5: string;
  imageLink6: string;
  websiteUrl: string;
  organization: string;
  tournamentDescription: string;
  primaryColor: string;
  secondaryColor: string;
  backgroundImageUrl: string;
  extensionPointer: boolean;
  extensionNamespace: string;
  gameSelection: string;
  challongeLeaderboard: string;
  owner: Types.ObjectId;
  ownerType: ERenaTournamentOwnerKinds;
  bracketType: ERenaScoringKinds;
  opportunity: Types.ObjectId;
  createdAt?: Date;
  updatedAt?: Date;
  game?: Types.ObjectId | string;
  requirements: Array<Types.ObjectId | string>;

  // Using it to resolveType for union types in graphql
  kind?: string;

  /**
   * @deprecated
   */
  globalStats: {
    teamsRemaining: number;
    playersRemaining: number;
    totalKills: number;
    totalValue: number;
    statName: string;
  };
  /**
   * @deprecated
   */
  airtableId: string;
  /**
   * @deprecated
   */
  airtableBaseId: string;
  /**
   * @deprecated
   */
  statName: string;
  /**
   * @deprecated
   */
  lastAirtableModificationTime: string;
}
