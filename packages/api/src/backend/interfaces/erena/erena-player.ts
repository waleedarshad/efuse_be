import { Document, Types } from "mongoose";
import { IUser } from "../user";

export interface IERenaPlayer extends Document {
  name: string;
  isActive: boolean;
  team: Types.ObjectId;
  tournament: Types.ObjectId;
  type: string;
  user: Types.ObjectId | string | IUser;
  createdAt?: Date;
  updatedAt?: Date;

  /**
   * @deprecated
   */
  lastAirtableModificationTime?: string;
  /**
   * @deprecated
   */
  stats?: {
    kills: number;
    value: number;
    name: string;
  };
  /**
   * @deprecated
   */
  airtableId?: string;
  /**
   * @deprecated
   */
  airtableBaseId?: string;
  /**
   * @deprecated
   * Not stored in DB. Used during leaderboard generation
   */
  rank?: number;

  matchId?: string;
}

/**
 * @deprecated
 */
export interface IUpdatePlayerStatsParams {
  total_kills: number;
  status: string;
  name: string;
  userId: Types.ObjectId | string;
}
