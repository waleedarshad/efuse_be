import { Document, Types } from "mongoose";
import { ERenaRoles } from "./erena-enums";

export interface IERenaStaff extends Document {
  user: Types.ObjectId | string;
  tournament: Types.ObjectId;
  role: ERenaRoles;
  createdAt?: Date;
  updatedAt?: Date;
}
