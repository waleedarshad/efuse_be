import { Document, Types } from "mongoose";
import { ERenaScoringKinds, ERenaScoreOwnerKinds } from "./erena-enums";

export interface IERenaScore extends Document {
  erenaKind: ERenaScoringKinds;
  ownerKind: ERenaScoreOwnerKinds;
  ownerId: Types.ObjectId;
  match: Types.ObjectId;
  tournament: Types.ObjectId;
  score: number;
}
