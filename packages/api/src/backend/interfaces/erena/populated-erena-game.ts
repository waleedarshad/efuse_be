import { IGame } from "../game";
import { IERenaGame } from "./erena-game";

export interface IPopulatedERenaGame extends Omit<IERenaGame, "game"> {
  game: IGame;
}
