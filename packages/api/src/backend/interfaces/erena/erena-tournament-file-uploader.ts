export interface IERenaTournamentFileUploader {
  image?: Express.Multer.File[];
  brandLogoUrl?: Express.Multer.File[];
  backgroundImageUrl?: Express.Multer.File[];
}
