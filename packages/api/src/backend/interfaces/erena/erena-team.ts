import { Document, Types } from "mongoose";

export interface IERenaTeam extends Document {
  name: string;
  isActive: boolean;
  tournament: Types.ObjectId;
  type: string;
  createdAt?: Date;
  updatedAt?: Date;

  /**
   * @deprecated not stored in database
   */
  rank?: number;
  /**
   * @deprecated This is only used for Twitch Plugin and is not stored in the database
   */
  teamName?: string;
  /**
   * @deprecated
   */
  stats?: {
    kills: number;
    value: number;
    name: string;
  };
  /**
   * @deprecated
   */
  airtableBaseId?: string;
  /**
   * @deprecated
   */
  airtableId?: string;
  /**
   * @deprecated
   */
  lastAirtableModificationTime?: string;

  matchId?: string;
}
