import { IERenaPlayer } from "./erena-player";
import { IERenaScore } from "./erena-score";
import { IERenaTeam } from "./erena-team";

export interface IERenaTeamPopulated extends Omit<IERenaTeam, "players"> {
  players?: IERenaPlayer[] | [];
  teamScores?: IERenaScore[];
}
