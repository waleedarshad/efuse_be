import { IERenaTeam } from "./erena-team";
import { IERenaLeaderboardPlayer } from "./erena-leaderboard-player";

export interface IERenaLeaderboardTeam extends IERenaTeam {
  score?: number;
  players?: IERenaLeaderboardPlayer[];
  summedPlayerScore?: number;
  rank?: number;
}
