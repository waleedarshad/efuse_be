import { Document, Types } from "mongoose";
import { File } from "../file";

import { IERenaTeamPopulated } from "./erena-team-populated";

export interface IERenaMatch extends Document {
  matchDate: Date;
  winner: Types.ObjectId | IERenaTeamPopulated | string | undefined;
  matchNumber: number;
  tournament: Types.ObjectId | string;
  teams: Array<{ team: Types.ObjectId | IERenaTeamPopulated | string }>;
  createdAt?: Date;
  updatedAt?: Date;
  teamOneImage?: File;
  teamTwoImage?: File;

  /**
   * @deprecated
   */
  teamOneScore?: number;

  /**
   * @deprecated
   */
  teamTwoScore?: number;

  /**
   * @deprecated
   * Not in database.
   */
  addTeam?: string;

  /**
   * @deprecated
   * Not in database.
   */
  addTeamIndex?: number;
}

export interface IERenaMatchCreateParams {
  matchDate: Date;
  team1: Types.ObjectId | string;
  team2: Types.ObjectId | string;
}

export interface IERenaMatchMultiCreateParams {
  matchNumber: number;
  tournament: Types.ObjectId | string;
  teams: unknown[];
}
