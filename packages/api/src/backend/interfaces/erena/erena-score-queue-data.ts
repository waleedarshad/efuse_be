import { Types } from "mongoose";

/**
 * @deprecated
 */
export interface IERenaScoreQueueData {
  name: string;
  status: boolean;
  tournamentIdOrSlug: string;
  teamId: Types.ObjectId | string;
}
