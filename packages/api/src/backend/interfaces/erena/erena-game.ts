import { Document, Types } from "mongoose";

export interface IERenaGame extends Document {
  game: Types.ObjectId | string;
  tournament: Types.ObjectId | string;
  updatedAt?: Date;
  createdAt?: Date;
}
