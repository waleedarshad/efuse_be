import { IERenaPlayer } from "./erena-player";
import { IERenaTeam } from "./erena-team";

/**
 * @deprecated This was only used for returning a player with a team after updating player
 */
export interface IERenaPlayerPopulated extends Omit<IERenaPlayer, "team"> {
  team: IERenaTeam;
}
