import { IERenaPlayer } from "./erena-player";

export interface IERenaLeaderboardPlayer extends Omit<IERenaPlayer, "rank"> {
  score: number;
  rank: number;
}
