import { Document, Types } from "mongoose";

export interface IERenaFeaturedEvent extends Document {
  tournament: Types.ObjectId;
  active: boolean;
}
