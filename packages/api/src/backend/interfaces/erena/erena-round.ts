import { Document, Types } from "mongoose";
import { IERenaMatch } from "./erena-match";

export interface IERenaRound extends Document {
  roundDate: Date;
  roundTitle: string;
  roundNumber: number;
  tournament: Types.ObjectId;
  matches: Array<{ match: Types.ObjectId | IERenaMatch }> | IERenaMatch[];
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IERenaRoundCreateParams extends Omit<IERenaRound, "tournament" | "matches"> {
  matches: string[];
}
