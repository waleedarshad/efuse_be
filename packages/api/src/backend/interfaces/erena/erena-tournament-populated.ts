import { IOpportunity } from "../opportunity";
import { IERenaBracket } from "./erena-bracket";
import { IERenaTeamPopulated } from "./erena-team-populated";
import { IERenaTournament } from "./erena-tournament";

export interface IERenaTournamentPopulated extends Omit<IERenaTournament, "opportunity"> {
  opportunity?: IOpportunity;
  teamStats?: IERenaTeamPopulated[];
  bracket?: IERenaBracket;
}
