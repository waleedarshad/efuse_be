/**
 * @desc The type of scoring used for an eRena event.
 */
export enum ERenaScoringKinds {
  bracket = "bracket",
  pointRace = "pointRace"
}

/**
 * @desc The type of owner for an eRena score.
 */
export enum ERenaScoreOwnerKinds {
  player = "player",
  team = "team"
}

/**
 * @desc eRena tournament status
 */
export enum ERenaTournamentStatus {
  scheduled = "Scheduled",
  active = "Active",
  finished = "Finished",
  deleted = "Deleted"
}

/**
 * @desc The type of eRena event. Used to determine if a tournament is a white label event (Keempark) or an eFuse event.
 */
export enum ERenaType {
  whiteLabel = "WHITE_LABEL",
  efuse = "EFUSE"
}

/**
 * Tournament owner kinds. Directly references collection names
 */
export enum ERenaTournamentOwnerKinds {
  user = "users",
  org = "organizations"
}

/**
 * Default namespaces for eRena
 */
export enum ERenaNamespaces {
  efuse = "efuse"
}

export enum ERenaRoles {
  owner = "owner",
  administrator = "administrator"
}
