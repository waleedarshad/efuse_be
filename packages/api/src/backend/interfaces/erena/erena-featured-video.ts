import { Document } from "mongoose";

export interface IERenaFeaturedVideo extends Document {
  title: string;
  description: string;
  youtubeLink: string;
  order: number;
}
