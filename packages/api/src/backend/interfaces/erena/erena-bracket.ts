import { Document, Types } from "mongoose";
import { IERenaRound } from "./erena-round";

export interface IERenaBracket extends Document {
  totalTeams: number;
  totalRounds: number;
  totalMatches: number;
  tournament: Types.ObjectId;
  rounds: Array<{ round: Types.ObjectId | IERenaRound }> | IERenaRound[];
  createdAt?: Date;
  updatedAt?: Date;
}
