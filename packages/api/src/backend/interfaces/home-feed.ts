import { Document, Types } from "mongoose";
import { IFeedAggregate } from "./feed";
import { IUser } from "./user";

export interface IHomeFeed extends Document {
  _id: Types.ObjectId | string;
  user: Types.ObjectId | string;
  feed: Types.ObjectId | string;
  timelineable: Types.ObjectId | string;
  timelineableType: string;
  original: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IHomeFeedAggregate extends IHomeFeed {
  associatedFeed: IFeedAggregate;
  associatedUser: IUser;
}
