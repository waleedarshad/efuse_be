import { Document, Types } from "mongoose";
import { Giphy } from "../../backend/models/giphy/giphy.model";

import { IMention } from "./mention";
import { File } from "./file";
import { IPostFeedItem } from "./feed/post-feed-item";
import { ObjectId } from "../../backend/types";
export interface IMetadata {
  views: number;
  hashtags: string[];
  comprehendAnalysis: {
    languages: [{ languageCode: string; score: string }];
    sentiments: {
      sentiment: string;
      sentimentScore: {
        positive: string;
        negative: string;
        neutral: string;
        mixed: string;
      };
    };
    entities: [
      {
        score: string;
        type: string;
        text: string;
        beginOffset: number;
        endOffset: number;
      }
    ];
  };
  boosted: boolean;
  feedRank: number;
  sharedLinks: string[];
}

// Extending IFeed property to support formatted feed property
export interface IFeed extends Document, IPostFeedItem {
  _id: Types.ObjectId;
  content?: string;
  comments: number;
  likes: number;
  user: Types.ObjectId;
  mentions?: IMention[];
  sharedFeed?: Types.ObjectId;
  shared: boolean;
  sharedTimeline?: Types.ObjectId;
  sharedTimelineable?: Types.ObjectId;
  sharedTimelineableType?: string;
  reports?: [{ reportedBy: Types.ObjectId; description: string; createdAt: Date }];
  platform?: string;
  metadata?: IMetadata;
  updatedAt?: Date;
  createdAt?: Date;
  scheduledFeed?: Types.ObjectId;
  boosted: boolean;
  youtubeVideo?: Types.ObjectId | string;
}

export interface IFeedAggregate extends IFeed {
  metadata: IMetadata;
}

export interface IFeedInput {
  text?: string;
  twitchClip?: string;
  file?: File;
  files?: File[];
  giphy?: ObjectId | Giphy;
  kindId: string;
  kind: string;
  verified: boolean;
  platform: string;
  mentions: IMention[];
  scheduledFeedId?: string;
  shared?: boolean;
  sharedFeed?: string;
  sharedTimeline?: string;
  sharedTimelineable?: string;
  sharedTimelineableType?: string;
  sharedPostUser?: string;
  crossPosts?: string[];
  associatedGame?: string;
  youtubeVideo?: string;
  scheduledAt: Date;
}
