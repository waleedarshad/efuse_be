import { OwnerKind } from "@efuse/entities";

export interface IExternalAuthRequestInfo {
  requestingUserId: string;
  ownerKind: OwnerKind;
  ownerId: string;
}
