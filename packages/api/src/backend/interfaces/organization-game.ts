import { Document, Types } from "mongoose";

export interface IOrganizationGame extends Document {
  game: Types.ObjectId | string;
  organization: Types.ObjectId | string;
}
