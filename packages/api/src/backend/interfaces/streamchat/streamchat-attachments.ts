export interface IStreamchatAttachments {
  asset_url: string;
  author_name: string;
  image_url: string;
  og_scrape_url: string;
  text: string;
  thumb_url: string;
  title_link: string;
  title: string;
  type: string;
}
