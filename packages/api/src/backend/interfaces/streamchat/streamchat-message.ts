import { IStreamchatAttachments } from "./streamchat-attachments";
import { IStreamchatReactions } from "./streamchat-reactions";
import { IStreamchatUser } from "./streamchat-user";

export interface IStreamchatMessage {
  attachments: IStreamchatAttachments[];
  cid: string;
  created_at: Date;
  html: string;
  id: string;
  latest_reactions: IStreamchatReactions[];
  mentioned_users: IStreamchatUser[];
  own_reactions: IStreamchatReactions[];
  pin_expires: Date | null;
  pinned_at: Date | null;
  pinned_by: Date | null;
  pinned: boolean;
  reaction_counts: Record<string, number>;
  reaction_scores: Record<string, number>;
  reply_count: number;
  shadowed: boolean;
  silent: boolean;
  text: string;
  type: string;
  updated_at: Date;
  user: IStreamchatUser | null;
}
