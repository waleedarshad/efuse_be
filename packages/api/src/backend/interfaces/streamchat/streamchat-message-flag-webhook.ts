import { IStreamchatMessage } from "./streamchat-message";
import { IStreamchatUser } from "./streamchat-user";

export interface IStreamchatMessageFlagWebhook {
  channel_id: string;
  channel_type: string;
  cid: string;
  createdAt: Date;
  members: IStreamchatUser[];
  message: IStreamchatMessage;
  total_flags: number;
  type: string;
  user: IStreamchatUser;
}
