import { IStreamchatUser } from "./streamchat-user";

export interface IStreamchatChannelMember {
  banned: boolean;
  created_at: Date;
  role: "member" | "owner";
  shadow_banned: boolean;
  updated_at: Date;
  user_id: string;
  user: IStreamchatUser;
}
