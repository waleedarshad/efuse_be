export interface IStreamchatChannelConfig {
  automod_behavior: string;
  automod_thresholds: Record<string, string>;
  automod: string;
  blocklist_behavior: string;
  blocklist: string;
  commands: string[];
  connect_events: boolean;
  created_at: Date;
  custom_events: boolean;
  max_message_length: number;
  message_retention: string;
  mutes: boolean;
  name: string;
  push_notifications: boolean;
  reactions: boolean;
  read_events: boolean;
  replies: boolean;
  search: boolean;
  typing_events: boolean;
  updated_at: Date;
  uploads: boolean;
  url_enrichment: boolean;
}
