export interface IStreamchatUser {
  banned: boolean;
  created_at: Date;
  id: string;
  image: string;
  last_active: Date;
  name: string;
  online: boolean;
  role: string;
  updated_at: Date;
}
