import { IStreamchatChannelConfig } from "./streamchat-channel-config.interface";
import { IStreamchatChannelMember } from "./streamchat-channel-member.interface";
import { IStreamchatUser } from "./streamchat-user";

export interface IStreamchatChannel {
  auto_translation_language: string;
  cid: string;
  config: IStreamchatChannelConfig;
  created_at: Date;
  created_by: IStreamchatUser;
  disabled: boolean;
  frozen: boolean;
  id: string;
  last_message_at: Date;
  member_count: number;
  members: IStreamchatChannelMember[];
  name: string;
  type: string;
  updated_at: Date;
}
