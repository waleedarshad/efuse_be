import { IStreamchatChannel } from "./streamchat-channel.interface";
import { IStreamchatMessage } from "./streamchat-message";
import { IStreamchatUser } from "./streamchat-user";

export interface IStreamchatPresendMessageWebhookResponse {
  channel: IStreamchatChannel;
  message: IStreamchatMessage;
  user: IStreamchatUser;
}
