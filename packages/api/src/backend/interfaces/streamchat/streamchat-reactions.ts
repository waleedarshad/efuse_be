import { IStreamchatUser } from "./streamchat-user";

export interface IStreamchatReactions {
  createdAt: Date;
  message_id: string;
  score: number;
  type: string;
  updatedAt: Date;
  user_id: string;
  user: IStreamchatUser;
}
