export interface IQueryOpts {
  populate?: string[];
  select?: string;
  lean?: boolean;
}
