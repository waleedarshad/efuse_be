import { Document, Types } from "mongoose";

export interface ITranscodeMedia extends Document {
  jobId: string;
  originalMedia: Types.ObjectId;
  transcodedMedia: Types.ObjectId[];
  hlsMedia: Types.ObjectId;
  processed: boolean;
  status: string;
  error: { code?: number; message: string };
  createdAt?: Date;
  updatedAt?: Date;
}
