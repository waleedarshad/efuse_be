/**
 * @desc The kind of entry being performed
 */
export enum TokenLedgerKind {
  credit = "credit",
  debit = "debit"
}

/**
 * @desc The source of the ledger entry
 */
export enum TokenLedgerAction {
  newPost = "newPost",
  comment = "comment",
  hype = "hype",
  boostPost = "boostPost"
}
