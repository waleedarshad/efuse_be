export interface ITokenActionValues {
  newPost: number;
  comment: number;
  hype: number;
  boostPost: number;
}
