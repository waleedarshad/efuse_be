import { Document, Types } from "mongoose";
import { TokenLedgerAction, TokenLedgerKind } from "./token-ledger-enums";

export interface ITokenLedgerEntry extends Document {
  action: TokenLedgerAction;
  balance: number;
  ledgerKind: TokenLedgerKind;
  user: Types.ObjectId | string;
  value: number;
}
