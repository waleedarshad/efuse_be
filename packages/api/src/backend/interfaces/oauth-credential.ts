import { Document } from "mongoose";

export interface IOAuthCredential extends Document {
  accessToken?: string;
  accessTokenExpiration?: Date;
  accessTokenSecret?: string;
  createdAt?: Date;
  idToken?: string;
  owner: string;
  ownerKind: string;
  refreshToken?: string;
  scope?: string[];
  service: string;
  tokenType?: string;
  updatedAt?: Date;
}
