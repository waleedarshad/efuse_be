import { Document, Types } from "mongoose";
import { FavoriteOwnerType, FavoriteRelatedModel } from "@efuse/entities";

export interface IFavorite extends Document {
  creator: Types.ObjectId;
  relatedDoc: Types.ObjectId;
  relatedModel: FavoriteRelatedModel;
  updatedAt?: Date;
  createdAt?: Date;
  owner: Types.ObjectId;
  ownerType: FavoriteOwnerType;
}
