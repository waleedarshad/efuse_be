import { Document } from "mongoose";

import { File } from "./file";

export interface IGame extends Document {
  description: string;
  gameImage: File;
  kind: string;
  slug: string;
  title: string;
  createdAt?: Date;
  updatedAt?: Date;
}
