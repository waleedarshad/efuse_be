import { Types } from "mongoose";

import { File } from "../file";

export interface ILearningArticleCategory extends Document {
  _id: Types.ObjectId;
  name: string;
  slug: string;
  image: File;
  isActive: string;
  user: Types.ObjectId;
}
