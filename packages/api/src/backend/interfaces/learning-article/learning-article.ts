import { Document, Types } from "mongoose";

import { File } from "../file";
import { ILearningArticleCategory } from "./learning-article-category";
import { IGame } from "../game";
import { ObjectId } from "../../types";

export interface ILearningArticle extends Document {
  _id: ObjectId;
  slug: string;
  title: string;
  body: string;
  status: string;
  author: Types.ObjectId | string;
  authorType: string;
  category: ILearningArticleCategory | Types.ObjectId | string;
  summary: string;
  image: File;
  video: File;
  isActive: boolean;
  promoted: boolean;
  traits: string[];
  motivations: string[];
  publishDate: Date;
  createdAt?: Date;
  updatedAt?: Date;
  associatedGames?: IGame[];
  url: string;
  promoted_url?: string;
}
