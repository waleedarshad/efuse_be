import { IOrganization } from "../organization";
import { IUser } from "../user";
import { ILearningArticle } from "./learning-article";
import { ILearningArticleCategory } from "./learning-article-category";

export interface IPopulatedLearningArticle extends Omit<ILearningArticle, "author" | "category"> {
  author: IUser | IOrganization | null;
  category: ILearningArticleCategory | null;
}
