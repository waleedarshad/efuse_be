import { Document, Types } from "mongoose";
import { File } from "./file";

export interface IMedia extends Document {
  file: File;
  user: Types.ObjectId | string;
  mediaable: Types.ObjectId | string;
  mediaableType: string;
  fileOriginalUrl: string;
  profile: string;
  size: number;
  updatedAt?: Date;
  createdAt?: Date;
}

export interface IMediaUpdate extends Document {
  file?: File;
  user?: Types.ObjectId;
  mediaable?: Types.ObjectId;
  mediaableType?: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export interface IResponse {
  n: number;
  nModified: number;
  ok: number;
}

export interface IAssociatedMedia {
  media: IMedia | undefined;
  medias: IMedia[] | [];
}
