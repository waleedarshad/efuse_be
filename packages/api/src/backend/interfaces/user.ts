import { Document, Types } from "mongoose";

import { File } from "./file";
import { IStreak } from "./streak";
import { IUserStats } from "./user-stats";
import { IUserLeagueOfLegendsInfo } from "./user-league-of-legends-info";
import { IUserOverwatchStats } from "./user-overwatch-stats";
import { GamePlatform } from "../../backend/models/game-platform.model";
import { IPortfolioExperience } from "./portfolioExperience/porfolioExperience";
import { ILeaderboard } from "./leaderboard/user-leaderboard";
import { IPopulatedUserBadge } from "./badge";
import { IGame } from "./game";
import { ObjectId } from "../../backend/types";
interface Platform {
  username: string;
  platform: string;
}

export interface AffiliatedGame {
  affiliatedGame?: Types.ObjectId;
  stats?: [{ stat: string; value: string }];
  platforms?: string;
}

export interface DiscordHooks {
  _id?: Types.ObjectId;
  name: string;
  url: string;
}

export interface IComponentLayout {
  name?: string;
}

export interface ISkill {
  value: number;
  order: number;
}

export interface IUser extends Document {
  _id: Types.ObjectId;
  name: string;
  username: string;
  email: string;
  password?: string;
  dateOfBirth?: string;
  address?: string;
  zipCode?: string;
  gender?: string;
  traits: string[];
  motivations: string[];
  title?: string;
  organizations?: Types.ObjectId[];
  mainOrganization?: Types.ObjectId;
  interest?: string;
  phoneNumber?: string;
  school?: string;
  educationLevel?: string;
  graduationYear?: string;
  graduationMonth?: string;
  gpa?: string;
  major?: string;
  act?: string;
  sat?: string;
  gpaScale?: string;
  bio?: string;
  firstRun?: string;
  profilePicture?: File;
  headerImage?: File;
  resume?: File;
  roles: string[];
  locale?: string;
  status?: string;
  lastLoginAt?: Date;
  showOnline?: boolean;
  showOnlineMobile?: boolean;
  deleteAccountRequest?: boolean;
  deleteAccountRequestDate?: Date;
  verifyAccountRequest?: boolean;
  verifyAccountRequestDate?: Date;
  embeddedLink?: string;
  online?: boolean;
  resetPasswordToken?: string;
  resetPasswordExpires?: Date;
  emailVerificationToken?: string;
  emailVerificationExpires?: Date;
  emailVerificationConfirmedAt?: Date;
  verified?: boolean;
  blockedUsers?: Types.ObjectId[];
  og?: { number?: number; show?: boolean };
  userSignupDevice?: string;
  referral?: string;
  userAgent?: string;
  referralUrl?: string;
  slug?: string;
  migratedToCognito?: boolean;
  signupUrl?: string;
  componentLayout?: IComponentLayout[];
  lastUsernameUpdatedAt?: Date;
  passwordMigrated?: boolean;
  passwordMigratedAt?: Date;
  signupSocial?: string;
  weeklyNewsLetter?: boolean;
  portfolioProgress?: number;
  portfolioProgressUpdatedAt?: Date;
  goldSubscriber?: boolean;
  goldSubscriberSince?: Date;
  stats?: Types.ObjectId | IUserStats;
  publicTraitsMotivations?: boolean;
  pathway?: Types.ObjectId;
  deleted?: boolean;
  associatedGames?: IGame[];

  // Using it to resolveType for union types in graphql
  kind?: string;
  promoted?: boolean;
  promoted_url?: string;

  // Facebook
  facebookVerified?: boolean;
  facebookID?: string;
  facebookAccessToken?: string;
  facebookTokenType?: string;
  facebookAccessTokenExpiresAt?: Date;
  facebookPages?: [{ pageId?: string; pageName?: string; accessToken?: string }];
  facebookLink?: string;

  // Google
  /**
   * @deprecated use `GoogleAccountInfo`
   */
  googleVerified?: boolean;
  /**
   * @deprecated use `GoogleAccountInfo`
   */
  google?: { id: string; name: string; profile: string; picture: string; gender: string; locale: string };

  // Discord
  discordServer?: string;
  discordVerified?: boolean;
  discordUserId?: string;
  discordUserName?: string;
  discordAccessToken?: string;
  discordTokenType?: string;
  discordRefreshToken?: string;
  discordTokenScope?: string;
  discordTokenExpiresAt?: Date;
  discordWebhooks?: DiscordHooks[];
  discordUserUniqueId?: string;

  // Twitch
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchVerified?: boolean;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchID?: string;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchUserName?: string;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchDisplayName?: string;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchProfileImage?: string;

  /**
   * @deprecated use OAuthCrendential
   */
  twitchAccessToken?: string;
  /**
   * @deprecated use OAuthCrendential
   */
  twitchRefreshToken?: string;
  /**
   * @deprecated use OAuthCrendential
   */
  twitchAccessTokenExpiresAt?: Date;
  /**
   * @deprecated use OAuthCrendential
   */
  twitchScope?: string[];
  /**
   * @deprecated use OAuthCrendential
   */
  twitchTokenType?: string;

  /**
   * @deprecated use TwitchAccountInfo
   */
  twitch?: Types.ObjectId | string;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchLink?: string;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchFollowersCount?: number;
  /**
   * @deprecated use TwitchAccountInfo
   */
  twitchEmail?: string;

  // gamePlatforms
  gamePlatforms?: GamePlatform[];

  // experiences
  portfolioExperiences?: IPortfolioExperience[];

  // leaderboard
  leaderboard?: ILeaderboard;
  activeLeaderboard?: ILeaderboard;

  // badges
  badges?: IPopulatedUserBadge[];

  // Steam
  steamId?: string;
  steamUsername?: string;
  steamVerified?: boolean;
  steam?: Types.ObjectId;

  // Xbox
  xboxGamertag?: string;
  xbox?: Types.ObjectId;
  xboxVerified?: boolean;
  xboxUserId?: string;

  // Linkedin
  linkedin?: {
    id?: string;
    displayName: string;
    name: { givenName: string; familyName: string };
    photos: string[];
  };
  linkedinVerified?: boolean;
  linkedinUsername?: string;
  linkedinLink?: string;

  // Instagram
  instagramLink?: string;
  instagramUsername?: string;

  // TikTok
  tiktokLink?: string;
  tikTok?: {
    tiktokUsername?: string;
    tiktokFollowers?: number;
    tiktokVerified?: boolean;
    createdAt?: Date;
  };

  // Twitter
  /**
   * @deprecated use TwitterAccountInfo
   */
  twitterLink?: string;
  /**
   * @deprecated use TwitterAccountInfo
   */
  twitterId?: string;
  /**
   * @deprecated use TwitterAccountInfo
   */
  twitterUsername?: string;
  /**
   * @deprecated use TwitterAccountInfo
   */
  twitterVerified?: boolean;
  /**
   * @deprecated use OAuthCredential
   */
  twitterOauthToken?: string;
  /**
   * @deprecated use OAuthCredential
   */
  twitterOauthTokenSecret?: string;
  /**
   * @deprecated use TwitterAccountInfo
   */
  twitterfollowersCount?: number;

  // Battlenet
  battlenetVerified?: boolean;
  battlenetId?: string;

  // Snapchat
  snapchat?: {
    id?: string;
    displayName?: string;
    bitmoji?: {
      avatarId?: string;
      avatarUrl?: string;
    };
  };
  snapchatVerified?: boolean;

  // Youtube
  youtubeChannelId?: string;
  youtube?: {
    youtubeChannelId?: string;
    youtubeFollowers?: number;
    youtubeVerified?: boolean;
    youtubeUsername?: string;
    createdAt?: Date;
  };

  youtubeChannel?: ObjectId;

  // StreamChat
  streamChatToken?: string;

  // Social Experience
  socialExperience?: [
    {
      platform?: string;
      headline?: string;
      username?: string;
      startDate?: Date;
      endDate?: Date;
      present?: boolean;
      description?: string;
    }
  ];

  // Portfolio events
  portfolioEvents?: [
    {
      _id?: ObjectId;
      title?: string;
      subTitle?: string;
      eventDate?: Date;
      description?: string;
    }
  ];

  // Portfolio Honors
  portfolioHonors?: [
    {
      _id?: ObjectId;
      title?: string;
      subTitle?: string;
      description?: string;
    }
  ];

  // Business Experience
  businessExperience?: [
    {
      _id?: Types.ObjectId | string;
      title?: string;
      subTitle?: string;
      startDate?: Date;
      endDate?: Date;
      present?: boolean;
      image?: File;
      description?: string;
    }
  ];

  // Education Experience
  educationExperience?: [
    {
      _id?: Types.ObjectId | string;
      level?: number;
      subTitle?: string;
      school?: string;
      degree?: string;
      fieldofstudy?: string;
      grade?: string;
      startDate?: Date;
      endDate?: Date;
      present?: boolean;
      image?: File;
      description?: string;
    }
  ];

  // eSports Experience
  esportsExperience?: [
    {
      title?: string;
      subTitle?: string;
      startDate?: Date;
      endDate?: Date;
      present?: boolean;
      description?: string;
      image?: File;
    }
  ];

  // Business Skills
  businessSkills?: ISkill[];

  // Gaming Skills
  gamingSkills?: ISkill[];

  // League of Legends
  leagueOfLegends?: IUserLeagueOfLegendsInfo;

  // overwatch stats
  overwatch?: IUserOverwatchStats;

  // Rocket League
  rocketleague?: {
    platforms: Platform[];
    stats: Array<{
      trnScore: number;
      goalShotRatio: number;
      wins: number;
      goals: number;
      saves: number;
      mvps: number;
      assists: number;
      mvpWinsRatio: number;
      platform: string;
      shots: number;
      ranks: [
        {
          season: string;
          ranks: [
            {
              title: string;
              subTitle: string;
              ratingScore: number;
              ratingPercent: string;
              games: number;
              winStreak: number;
            }
          ];
        }
      ];
    }>;
  };

  // PUBG
  pubg?: {
    platforms: [{ username: string; platform: string; accountId: string }];
    stats: Types.ObjectId[];
  };

  // Custom fields
  streaks: {
    daily: IStreak;
    dailyComment: IStreak;
  };

  // Legacy Games implementation
  /** @deprecated */
  affiliatedGames?: AffiliatedGame[];

  // Legacy notifications fields
  /** @deprecated Migrated to UserSettings */
  chatNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  followsNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  likeNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  repliesNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  commentsNotification?: boolean;
  sharesNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  mentionsNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  postOrganizationNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  opportunityOrganizationNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  applyNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  organizationInvitationNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  newsOrganizationNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  hypedPostCommentNotification?: boolean;
  /** @deprecated Migrated to UserSettings */
  chatNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  followsNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  likeNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  repliesNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  commentsNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  sharesNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  mentionsNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  postOrganizationNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  opportunityOrganizationNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  applyNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  organizationInvitationNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  newsOrganizationNotificationMobile?: boolean;
  /** @deprecated Migrated to UserSettings */
  hypedPostCommentNotificationMobile?: boolean;
}
