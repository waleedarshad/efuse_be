import { Document, Types } from "mongoose";
import { IMention } from "./mention";

export interface IAutomatedFeed extends Document {
  user: Types.ObjectId;
  content: string;
  mentions: IMention[];
  timelineable: Types.ObjectId;
  timelineableType: string;
  verified: boolean;
  crossPosts: string[];
  media: Types.ObjectId;
  mediaObjects: Types.ObjectId[];
  automatedType: string;
  twitchClip: Types.ObjectId;
  lastSentAt?: Date;
  createdAt?: Date;
  updatedAt?: Date;
  platform: string;
  associatedGame?: Types.ObjectId | string;
  youtubeVideo?: Types.ObjectId | string;
}
