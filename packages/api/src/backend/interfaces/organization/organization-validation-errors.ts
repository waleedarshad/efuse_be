export interface IOrganizationValidationError {
  name?: string;
  scale?: string;
  organizationType?: string;
  status?: string;
  description?: string;
  phoneNumber?: string;
}
