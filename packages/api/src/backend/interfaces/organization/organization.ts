import { Document, Types } from "mongoose";

import { File } from "../file";
import { IGame } from "../game";
import { IMember } from "../member";

interface Verified {
  status?: boolean;
  verifiedBy?: Types.ObjectId;
  verifiedAt?: Date;
}

interface VideoCarouselItem {
  url?: string;
  title?: string;
  uploaded?: boolean;
  video?: File;
}

enum PublishStatus {
  draft = "draft",
  visible = "visible",
  hidden = "hidden"
}

interface PlayerCard {
  title?: string;
  subTitle?: string;
  url?: string;
  image?: File;
}

interface PromoVideo {
  title?: string;
  description?: string;
  embeddedUrl?: string;
  isVideo?: boolean;
  videoFile?: File;
}

interface Honor {
  title?: string;
  subTitle?: string;
  description?: string;
  image?: File;
}

interface Event {
  title?: string;
  subTitle?: string;
  eventDate?: Date;
  description?: string;
  image?: File;
}

export interface IOrganization extends Document {
  name: string;
  description?: string;
  scale?: string;
  website?: string;
  status: string;
  organizationType: string;
  email?: string;
  phoneNumber?: string;
  toBeVerified?: boolean;
  location?: string;
  verified?: Verified;
  profileImage?: File;
  headerImage?: File;

  // owner of the organization
  user?: Types.ObjectId;
  captains?: Types.ObjectId[];
  about?: string;
  events?: Event[];
  honors?: Honor[];
  promoVideo?: PromoVideo;
  playerCards?: PlayerCard[];
  discordServer?: string;
  publishStatus?: PublishStatus | string;
  slug?: string;
  videoCarousel?: VideoCarouselItem[];
  followersCount?: number;
  promoted?: boolean;
  role?: string;
  membersCount?: number;
  shortName: string;
  associatedGames?: IGame[];
  deleted?: boolean;
  createdAt?: Date;
  updatedAt?: Date;

  // Using it to resolveType for union types in graphql
  kind?: string;
  promoted_url?: string;
}

export interface EditableOrganization {
  organization: IOrganization;
  canEdit: boolean;
}

export interface UpdateCaptainResponse {
  captains?: Types.ObjectId[];
  flashType: string;
  message: string;
}

export interface BanMemberResponse {
  bannedMember: IMember | null;
  flashType: string;
  message: string;
}

export interface UnBanMemberResponse {
  unBannedMember: IMember | null;
  flashType: string;
  message: string;
}

export interface KickMemberResponse {
  members: IMember[];
  message: string;
  flashType: string;
}
