import { Document, Types } from "mongoose";

export interface IOrganizationFollower extends Document {
  follower: Types.ObjectId | string;
  followee: Types.ObjectId | string;
  createdAt?: Date;
  updatedAt?: Date;
}
