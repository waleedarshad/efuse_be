import { IOrganization } from "./organization";
import { IUser } from "../user";

export interface IPopulatedOrganizationUser extends Omit<IOrganization, "user"> {
  user: IUser | null;
}
