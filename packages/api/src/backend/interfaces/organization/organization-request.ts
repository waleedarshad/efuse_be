import { Document, Types } from "mongoose";

export interface IOrganizationRequest extends Document {
  organization: Types.ObjectId | string;
  user: Types.ObjectId | string;
  status: string;
  createdAt?: Date;
  updatedAt?: Date;
}
