import { Document, Types } from "mongoose";

export interface IOrganizationInvitation extends Document {
  inviter: Types.ObjectId;
  invitee: Types.ObjectId;
  inviteeEmail: string;
  organization: Types.ObjectId;
  status: string;
}
