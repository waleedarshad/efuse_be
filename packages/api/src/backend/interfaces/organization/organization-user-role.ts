import { ObjectId } from "../../types";
import { IOrganizationRole } from "./organization-role";

// this object is not in database, this is generally created when it is called through graphql
export interface IOrganizationUserRole {
  user: ObjectId;
  organizationRoles: IOrganizationRole[];
}
