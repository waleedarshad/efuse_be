import { IOrganization } from "./organization";
import { OrganizationUserRoleEnum } from "../../../lib/permissions/enums";

// this object is not in database, this is generally created when it is called through graphql
export interface IOrganizationRole {
  role: OrganizationUserRoleEnum;
  organization: IOrganization;
}
