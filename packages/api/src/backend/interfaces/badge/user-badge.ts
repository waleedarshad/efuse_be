import { Document, Types } from "mongoose";

export interface IUserBadge extends Document {
  badge: Types.ObjectId | string;
  user: Types.ObjectId | string;
  createdAt?: Date;
  updatedAt?: Date;
}
