import { DocumentType } from "@typegoose/typegoose";
import { Badge } from "../../models/badge.model";
import { IUserBadge } from "./user-badge";

export interface IPopulatedUserBadge extends Omit<IUserBadge, "badge"> {
  badge: DocumentType<Badge>;
}
