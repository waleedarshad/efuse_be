export interface ITranscodeMediaDetail {
  timestamp: number;
  accountId: string;
  queue: string;
  jobId: string;
  status: string;
  userMetadata: unknown;
  outputGroupDetails: Array<{
    outputDetails: Array<{
      outputFilePaths: string[];
      durationInMs: number;
      videoDetails: { widthInPx: number; heightInPx: number };
    }>;
    type: string;
  }>;
  errorCode: number;
  errorMessage: string;
}

export interface ITranscodeMediaStatus {
  id: string;
  version: string;
  "detail-type": string;
  source: string;
  account: string;
  time: Date;
  region: string;
  resources: string[];
  detail: ITranscodeMediaDetail;
}
