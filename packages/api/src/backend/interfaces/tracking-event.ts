import { Document, Types } from "mongoose";

export interface ITrackingEvent extends Document {
  name: string;
  properties: unknown;
  trackedResource: {
    param: string;
    value: string;
  };
  identity: { userId?: Types.ObjectId | string; anonymousId?: string };
  context: {
    userAgent?: string;
    ip?: string;
    location?: { city?: string; country: string; region: string };
    timezone?: string;
    os?: string;
    active?: boolean;
    referrer?: {
      type?: string;
      name?: string;
      url?: string;
      link?: string;
    };
  };
  updatedAt?: Date;
  createdAt?: Date;
}
