import { Document, Types } from "mongoose";

export interface IUserGame extends Document {
  game: Types.ObjectId | string;
  user: Types.ObjectId | string;
}
