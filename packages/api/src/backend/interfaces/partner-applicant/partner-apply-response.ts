import { IPartnerApplicant } from "./partner-applicant";

export interface IPartnerApplyResponse {
  canApply: boolean;
  reasons: string[];
  partnerApplicant?: IPartnerApplicant;
}
