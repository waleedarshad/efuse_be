import { Document, Types } from "mongoose";

export interface IPartnerApplicant extends Document {
  user: Types.ObjectId | string;
  status: string;
}
