export type { IPartnerApplicant } from "./partner-applicant";
export type { IPartnerApplyResponse } from "./partner-apply-response";
