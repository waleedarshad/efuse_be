export interface File {
  filename?: string;
  contentType?: string;
  url?: string;
}

export interface IFileRequestParam {
  profilePicture: Express.Multer.File[];
  headerImage: Express.Multer.File[];
  resume: Express.Multer.File[];
}
