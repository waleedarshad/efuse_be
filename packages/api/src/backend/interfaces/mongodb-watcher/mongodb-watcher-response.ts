export interface IMongodbWatcherResponse<T> {
  _id: { _data: string };
  operationType: string;
  clusterTime: string;
  fullDocument: T;
  ns: { db: string; coll: string };
  documentKey: { _id: string };
  updateDescription: {
    updatedFields: Record<string, unknown>;
    removedFields: Array<Record<string, unknown>> | Array<string>;
  };
}
