import { Document, Types } from "mongoose";
import { NoteOwnerType, NoteRelatedModel } from "@efuse/entities";

export interface INote extends Document {
  author: Types.ObjectId;
  content: string;
  relatedDoc: Types.ObjectId;
  relatedModel: NoteRelatedModel;
  updatedAt?: Date;
  createdAt?: Date;
  owner: Types.ObjectId;
  ownerType: NoteOwnerType;
}
