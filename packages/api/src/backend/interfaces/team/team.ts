import { Document, Types } from "mongoose";
import { OwnerType } from "@efuse/entities";
import { File } from "../file";

export interface ITeam extends Document {
  description: string;
  game: Types.ObjectId;
  image: File;
  name: string;
  owner: Types.ObjectId;
  ownerType: OwnerType;
}
