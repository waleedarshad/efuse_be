import { Document, Types } from "mongoose";
import { TeamMemberStatus } from "@efuse/entities";

export interface ITeamMember extends Document {
  team: Types.ObjectId;
  user: Types.ObjectId;
  role: string;
  status: TeamMemberStatus;
}
