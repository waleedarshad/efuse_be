export type { 
  IValidationResponse, 
  IValidateEmailDeprecatedResponse, 
  IEmailValidationErrors, 
  IInputValidationResponse 
} from "./validation";
