export interface IValidationResponse {
  valid: boolean;
  error: string;
}

export interface IValidateEmailDeprecatedResponse {
  errors: {
    email?: string;
  };
  isValid: boolean;
  invalidFormat: boolean;
}

export interface IEmailValidationErrors {
  email?: string;
}

export interface IInputValidationResponse<ErrorsType> {
  errors: ErrorsType;
  isValid: boolean;
}

export interface IUsernameError {
  username?: string;
}

export interface IValidateUsernameDeprecatedResponse {
  errors: IUsernameError;
  isValid: boolean;
  invalidUsername: boolean;
}
