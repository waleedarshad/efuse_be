export interface ISwaggerRequestBody {
  description: string;
  content: {
    "application/json": {
      schema: { $ref: string };
    };
  };
  required: boolean;
}
