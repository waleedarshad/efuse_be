export interface ISwaggerPaginationResponse {
  [key: string]: {
    description: string;
    content: {
      "application/json": {
        schema: {
          allOf: [{ $ref: string }, { type: string; properties: { docs: { type: string; items: { $ref: string } } } }];
        };
      };
    };
  };
}
