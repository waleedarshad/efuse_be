export interface ISitemapUrl {
  url: string;
  changefreq: string;
  priority: number;
}
