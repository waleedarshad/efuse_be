export interface IBulkWriteResponse {
  upserted: number;
  updated: number;
  modified: number;
  deleted: number;
  matched: number;
}
