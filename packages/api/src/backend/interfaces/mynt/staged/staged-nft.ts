import { Document } from "mongoose";
import { IMyntTraits } from "../mynt-traits";
import { IStagedNFTTransaction } from "./staged-nft-transaction";

export interface IStagedNFT extends Document {
  assetFile: string;
  name: string;
  externalLink: string;
  description: string;
  traits: IMyntTraits;
  unlockableContent: string;
  eFuseUserId: string;
  isListed: boolean;
  sellPrice?: string;
  transactionHistory?: IStagedNFTTransaction[];
}
