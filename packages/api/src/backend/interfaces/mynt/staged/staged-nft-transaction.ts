import { Document } from "mongoose";

export interface IStagedNFTTransaction extends Document {
  purchaserId: string;
  sellerId: string;
  nftId: string;
  orderId: string;
  payerId: string;
  purchasePrice: string;
}
