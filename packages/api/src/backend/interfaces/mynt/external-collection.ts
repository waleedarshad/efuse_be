import { Document } from "mongoose";
import { IExternalTraitStats } from "./external-trait-stats";

export interface IExternalCollection extends Document {
  name: string;
  slug: string;
  editors: string[];
  hidden: boolean;
  featured: boolean;
  createdDate: Date;
  description: string;
  imageUrl: string;
  largeImageUrl: string;
  featuredImageUrl: string;
  stats: object;
  displayData: object;
  payoutAddress?: string;
  traitStats: IExternalTraitStats;
  externalLink?: string;
  wikiLink?: string;
}
