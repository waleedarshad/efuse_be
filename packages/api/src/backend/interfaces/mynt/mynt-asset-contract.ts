import { Document } from "mongoose";
import { AssetContractType, WyvernSchemaName } from "./mynt-enums";

export interface IMyntAssetContract extends Document {
  name: string;
  address: string;
  type: AssetContractType;
  schemaName: WyvernSchemaName;
  sellerFeeBasisPoints: number;
  buyerFeeBasisPoints: number;
  description: string;
  tokenSymbol: string;
  imageUrl: string;
  stats?: object;
  traitStats?: object[];
  externalLink?: string;
  wikiLink?: string;
}
