import { AssetEventType, AuctionType } from "./mynt-enums";
import { IMyntTransaction } from "./mynt-transaction";

export interface IMyntAssetEvent {
  eventType: AssetEventType;
  eventTimestamp: Date;
  auctionType: AuctionType;
  totalPrice: string;
  transaction: IMyntTransaction | null;
  paymentToken: object | null;
}
