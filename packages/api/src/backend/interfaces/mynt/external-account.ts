export interface IExternalAccount {
  address: string;
  config: string;
  profileImgUrl: string;
  user: IExternalUser | null;
}
export interface IExternalUser {
  username: string;
}
