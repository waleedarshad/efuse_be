import { Document } from "mongoose";
import BigNumber from "bignumber.js";
import { IExternalAccount } from "./external-account";
import { IExternalCollection } from "./external-collection";
import { IMyntAssetContract } from "./mynt-asset-contract";
import { IMyntAssetEvent } from "./mynt-asset-event";
import { IMyntTraits } from "./mynt-traits";
import { TokenStandardVersion, WyvernSchemaName } from "./mynt-enums";

export interface IMyntAsset extends Document {
  openSeaId: string | null;
  tokenId: string | null;
  tokenAddress: string;
  schemaName?: WyvernSchemaName;
  version?: TokenStandardVersion;
  name?: string;
  decimals?: number;
  assetContract: IMyntAssetContract;
  assetCollection: IExternalCollection;
  description: string;
  owner: IExternalAccount;
  isPresale: boolean;
  imageUrl: string;
  imagePreviewUrl: string;
  imageUrlOriginal: string;
  imageUrlThumbnail: string;
  animationUrl: string;
  animationOriginalUrl: string;
  openSeaLink: string;
  externalLink: string;
  traits: IMyntTraits;
  numSales: number;
  lastSale: IMyntAssetEvent | null;
  backgroundColor: string | null;
  transferFee: BigNumber | string | null;
  creator: IExternalAccount;
}
