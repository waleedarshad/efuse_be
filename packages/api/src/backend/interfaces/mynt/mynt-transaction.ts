import { IExternalAccount } from "./external-account";

export interface IMyntTransaction {
  fromAccount: IExternalAccount;
  toAccount: IExternalAccount;
  createdDate: Date;
  modifiedDate: Date;
  transactionHash: string;
  transactionIndex: string;
  blockNumber: string;
  blockHash: string;
  timestamp: Date;
}
