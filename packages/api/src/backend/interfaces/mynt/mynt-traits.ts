export interface IMyntTraits {
  rarity: string;
  game: string;
  owner: string;
  creator: string;
  mediaType: string;
}
