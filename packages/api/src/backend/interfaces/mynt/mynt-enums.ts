export enum AssetContractType {
  Fungible = "fungible",
  SemiFungible = "semi-fungible",
  NonFungible = "non-fungible",
  Unknown = "unknown"
}

export enum WyvernSchemaName {
  ERC20 = "ERC20",
  ERC721 = "ERC721",
  ERC1155 = "ERC1155",
  LegacyEnjin = "Enjin",
  ENSShortNameAuction = "ENSShortNameAuction"
}

export enum TokenStandardVersion {
  Unsupported = "unsupported",
  Locked = "locked",
  Enjin = "1155-1.0",
  ERC721v1 = "1.0",
  ERC721v2 = "2.0",
  ERC721v3 = "3.0"
}

/**
 * Defines set of possible auctions types
 */
export enum AuctionType {
  Dutch = "dutch",
  English = "english",
  MinPrice = "min_price"
}
/**
 * Defines the possible types of asset events that can take place
 */
export enum AssetEventType {
  AuctionCreated = "created",
  AuctionSuccessful = "successful",
  AuctionCancelled = "cancelled",
  OfferEntered = "offer_entered",
  BidEntered = "bid_entered",
  BidWithdraw = "bid_withdraw",
  AssetTransfer = "transfer",
  AssetApprove = "approve",
  CompositionCreated = "composition_created",
  Custom = "custom",
  Payout = "payout"
}
