export interface NumericalTraitStats {
  min: number;
  max: number;
}

export interface StringTraitStats {
  [key: string]: number;
}

export interface IExternalTraitStats {
  [traitName: string]: NumericalTraitStats | StringTraitStats;
}
