import { Types } from "mongoose";
import { IUser } from "./user";

export interface IGraphQLContext {
  user: IUser;
  userId: Types.ObjectId;
}

export interface PaginationArgs {
  limit: number;
  page: number;
}
