/**
 * Represents a player's ability in a valorant match round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantAbilityDTO {
  grenadeEffects: string;
  ability1Effects: string;
  ability2Effects: string;
  ultimateEffects: string;
}
