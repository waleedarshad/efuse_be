import { Document } from "mongoose";
/**
 * Represents a player as part of an entry on the valorant leaderboard
 *
 * @link: https://developer.riotgames.com/apis#val-ranked-v1/GET_getLeaderboard
 */
export interface IValorantLeaderboardPlayerDTO extends Document {
  /**
   * This field may be omitted if the player has been anonymized.
   */
  puuid: string;
  /**
   * This field may be omitted if the player has been anonymized.
   */
  gameName: string;
  /**
   * This field may be omitted if the player has been anonymized.
   */
  tagLine: string;
  leaderboardRank: number;
  rankedRating: number;
  numberOfWins: number;
  competitiveTier: number;

  // these fields are not part of the data from Valorant
  // but we want to store them in our collection for quick reference
  actId: string;
  region: string;
}
