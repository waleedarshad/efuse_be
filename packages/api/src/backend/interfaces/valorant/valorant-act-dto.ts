export interface IValorantActDTO {
  name: string;
  id: string;
  isActive: boolean;
  parentId: string;
  type: string;
}
