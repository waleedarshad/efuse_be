/**
 * Represents a the ability casts by a player in a valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantAbilityCastsDTO {
  grenadeCasts: number;
  ability1Casts: number;
  ability2Casts: number;
  ultimateCasts: number;
}
