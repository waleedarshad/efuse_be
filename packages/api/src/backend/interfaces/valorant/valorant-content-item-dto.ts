export interface IValorantContentItemDTO {
  name: string;
  id: string;
  assetName: string;
}
