/**
 * Represents a team in a Valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantMatchTeamDTO {
  /**
   * This is an arbitrary string. Red and Blue in bomb modes. The puuid of the player in deathmatch.
   */
  teamId: string;
  won: boolean;
  roundsPlayed: number;
  roundsWon: number;
  /**
   * Team points scored. Number of kills in deathmatch.
   */
  numPoints: number;
}
