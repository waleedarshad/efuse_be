import { Document } from "mongoose";

/**
 * IValorantApiVersion is used to keep track of the last version of data
 * we pulled from https://dash.valorant-api.com/
 */
export interface IValorantApiVersion extends Document {
  branch: string;
  version: string;
  buildVersion: string;
  buildDate: Date;
}
