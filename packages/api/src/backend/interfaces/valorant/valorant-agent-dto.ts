import { Document } from "mongoose";

/**
 * Represents a summoner from the unofficial Valorant API
 *
 * @link: https://dash.valorant-api.com/endpoints/agents
 */
export interface IValorantAgentDTO extends Document {
  uuid: string;
  displayName: string;
  description: string;
  developerName: string;
  displayIcon: string;
  displayIconSmall: string;
  bustPortrait: string;
  fullPortrait: string;
  assetPath: string;
}
