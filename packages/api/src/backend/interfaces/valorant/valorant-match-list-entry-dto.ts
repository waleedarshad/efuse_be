import { Document } from "mongoose";

/**
 * Represents a single entry in a user's valorant match list
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatchlist
 */
export interface IValorantMatchListEntryDTO extends Document {
  matchId: string;
  gameStartTimeMillis: number;
  teamId: string;
}
