/**
 * Represents a player's damage in a valorant match round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantDamageDTO {
  receiver: string;
  damage: number;
  legshots: number;
  bodyshots: number;
  headshots: number;
}
