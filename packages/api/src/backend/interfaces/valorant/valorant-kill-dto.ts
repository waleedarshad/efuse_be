import { IValorantFinishingDamageDTO } from "./valorant-finishing-damage-dto";
import { IValorantLocationDTO } from "./valorant-location-dto";
import { IValorantPlayerLocationDTO } from "./valorant-player-location-dto";

/**
 * Represents a player's kill in a valorant match round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantKillDTO {
  timeSinceGameStartMillis: number;
  timeSinceRoundStartMillis: number;
  killer: string;
  victim: string;
  victimLocation: IValorantLocationDTO;
  assistants: string[];
  playerLocations: IValorantPlayerLocationDTO[];
  finishingDamage: IValorantFinishingDamageDTO;
}
