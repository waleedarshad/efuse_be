import { IValorantAbilityDTO } from "./valorant-ability-dto";
import { IValorantDamageDTO } from "./valorant-damage-dto";
import { IValorantEconomyDTO } from "./valorant-economy-dto";
import { IValorantKillDTO } from "./valorant-kill-dto";

/**
 * Represents a player's stats in a valorant match round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantPlayerRoundStatsDTO {
  puuid: string;
  kills: IValorantKillDTO[];
  damage: IValorantDamageDTO[];
  score: number;
  economy: IValorantEconomyDTO;
  ability: IValorantAbilityDTO;
}
