import { IValorantLocationDTO } from "./valorant-location-dto";

/**
 * Represents a player's location at the end of a valorant round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantPlayerLocationDTO {
  puuid: string;
  viewRadians: number;
  location: IValorantLocationDTO;
}
