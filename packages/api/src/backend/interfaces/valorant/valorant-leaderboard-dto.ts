import { IValorantLeaderboardPlayerDTO } from "./valorant-leaderboard-player-dto";

/**
 * Represents a valorant leaderboard
 *
 * @link: https://developer.riotgames.com/apis#val-ranked-v1/GET_getLeaderboard
 */
export interface IValorantLeaderboardDTO {
  /**
   * The act id for the given leaderboard. Act ids can be found using the val-content API.
   */
  actId: string;
  /**
   * The total number of players in the leaderboard.
   */
  totalPlayers: number;
  immortalStartingPage: number;
  topTierRRThreshold: number;
  /**
   * The shard for the given leaderboard.
   */
  shard: string;
  players: IValorantLeaderboardPlayerDTO[];
}
