import { Document, Types } from "mongoose";
import { IAgentPlayerStats } from "./valorant-agent-player-stats";

/**
 * Represents the player stats provided to the front end and stored in
 * the "valorantPlayerStats" collection
 */
export interface IValorantStats extends Document {
  user: Types.ObjectId;
  puuid: string;
  gameName: string;
  tagLine: string;
  region: string;

  competitiveTier: number;
  competitiveTierName: string;
  competitiveTierImage: string;
  rankedRating: number;
  leaderboardRank: number;

  wins: number;
  losses: number;
  agentMatchStats: IAgentPlayerStats[];
  lastMatchTime: number;
  damage: number;
  kills: number;
  deaths: number;
  assists: number;
  episodeId: string;
  episodeName: string;
  actId: string;
  actName: string;
}
