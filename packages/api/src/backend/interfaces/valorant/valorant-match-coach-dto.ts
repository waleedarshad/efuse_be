/**
 * Represents a coach in a valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantMatchCoachDTO {
  puuid: string;
  teamId: string;
}
