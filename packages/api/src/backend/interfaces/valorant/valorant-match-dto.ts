import { Document } from "mongoose";
import { IValorantMatchCoachDTO } from "./valorant-match-coach-dto";
import { IValorantMatchInfoDTO } from "./valorant-match-info-dto";
import { IValorantMatchPlayerDTO } from "./valorant-match-player-dto";
import { IValorantMatchRoundResultDTO } from "./valorant-match-round-result-dto";
import { IValorantMatchTeamDTO } from "./valorant-match-team-dto";

/**
 * Represents a valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantMatchDTO extends Document {
  matchInfo: IValorantMatchInfoDTO;
  players: IValorantMatchPlayerDTO[];
  coaches: IValorantMatchCoachDTO[];
  teams: IValorantMatchTeamDTO[];
  roundResults: IValorantMatchRoundResultDTO[];
}
