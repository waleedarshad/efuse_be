import { IValorantMatchPlayerStatsDTO } from "./valorant-match-player-stats-dto";

/**
 * Represents a player in a valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantMatchPlayerDTO {
  puuid: string;
  gameName: string;
  tagLine: string;
  teamId: string;
  partyId: string;
  characterId: string;
  stats: IValorantMatchPlayerStatsDTO;
  competitiveTier: number;
}
