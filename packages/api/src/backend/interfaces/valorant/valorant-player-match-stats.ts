import { Document } from "mongoose";

/**
 * Represents a player's aggregated stats in a match
 */
export interface IValorantPlayerMatchStats extends Document {
  actId: string;
  agent: string;
  assists: number;
  competitiveTier: number;
  damage: number;
  deaths: number;
  gameMode: string;
  kills: number;
  matchId: string;
  puuid: string;
  queueId: string;
  timePlayed: number;
  timeStarted: number;
  won: boolean;
}
