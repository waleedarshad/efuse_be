/**
 * Represents a player's finishing damage for a kill in a valorant match round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantFinishingDamageDTO {
  damageType: string;
  damageItem: string;
  isSecondaryFireMode: boolean;
}
