/**
 * Represents a location valorant match round result
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantLocationDTO {
  x: number;
  y: number;
}
