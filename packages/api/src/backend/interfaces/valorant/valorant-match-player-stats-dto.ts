import { IValorantAbilityCastsDTO } from "./valorant-ability-casts-dto";

/**
 * Represents a player's stats in a valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantMatchPlayerStatsDTO {
  score: number;
  roundsPlayed: number;
  kills: number;
  deaths: number;
  assists: number;
  playtimeMillis: number;
  abilityCasts: IValorantAbilityCastsDTO;
}
