/**
 * Represents recent valorant matches from a queue
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getRecent
 */
export interface IValorantRecentMatchesDTO {
  currentTime: number;
  matchIds: string[];
}
