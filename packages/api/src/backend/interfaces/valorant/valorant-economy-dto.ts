/**
 * Represents a player's economy in a valorant match round
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantEconomyDTO {
  loadoutValue: number;
  weapon: string;
  armor: string;
  remaining: number;
  spent: number;
}
