import { Document } from "mongoose";
import { IValorantActDTO } from "./valorant-act-dto";
import { IValorantContentItemDTO } from "./valorant-content-item-dto";

/**
 * Represents the Valorant Content
 */
export interface IValorantContentDTO extends Document {
  version: string;
  characters: IValorantContentItemDTO[];
  playerCards: IValorantContentItemDTO[];
  playerTitles: IValorantContentItemDTO[];
  acts: IValorantActDTO[];
  region: string;
}
