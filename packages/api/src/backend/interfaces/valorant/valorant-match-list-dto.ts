import { Document } from "mongoose";
import { IValorantMatchListEntryDTO } from "./valorant-match-list-entry-dto";

/**
 * Represents a user's valorant match list
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatchlist
 */
export interface IValorantMatchListDTO extends Document {
  puuid: string;
  history: IValorantMatchListEntryDTO[];
}
