import { IValorantLocationDTO } from "./valorant-location-dto";
import { IValorantPlayerLocationDTO } from "./valorant-player-location-dto";
import { IValorantPlayerRoundStatsDTO } from "./valorant-player-round-stats-dto";

/**
 * Represents a the results of a Valorant match
 *
 * @link: https://developer.riotgames.com/apis#val-match-v1/GET_getMatch
 */
export interface IValorantMatchRoundResultDTO {
  roundNum: number;
  roundResult: string;
  roundCeremony: string;
  winningTeam: string;
  bombPlanter: string;
  bombDefuser: string;
  plantRoundTime: number;
  playPlayerLocations: IValorantPlayerLocationDTO[];
  plantLocation: IValorantLocationDTO;
  plantSite: string;
  defuseRoundTime: number;
  defuseLocation: IValorantLocationDTO;
  playerStats: IValorantPlayerRoundStatsDTO[];
  roundResultCode: string;
}
