import { IValorantAgentDTO } from "./valorant-agent-dto";

export interface IAgentPlayerStats {
  agent: string;
  wins: number;
  losses: number;
  kills: number;
  deaths: number;
  assists: number;
  damage: number;
  timePlayed: number;
  agentData: IValorantAgentDTO;
}
