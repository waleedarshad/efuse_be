export interface IValorantActiveShardDTO {
  puuid: string;
  game: string;
  activeShard: string;
}
