import { ITrackCron } from "./track-cron";
import { IOrganization } from "../organization";
import { IUser } from "../user";

export interface IPopulatedUserOrgTrackCron extends Omit<ITrackCron, "entity"> {
  entity: IUser | IOrganization;
}
