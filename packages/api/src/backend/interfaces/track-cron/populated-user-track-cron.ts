import { ITrackCron } from ".";
import { IUser } from "../user";

export interface IPopulatedUserTrackCron extends Omit<ITrackCron, "entity"> {
  entity: IUser;
}
