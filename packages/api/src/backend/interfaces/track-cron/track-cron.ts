import { Document, Types } from "mongoose";

export interface ITrackCron extends Document {
  kind: string;
  entity: Types.ObjectId | string;
  entityKind: string;
  method: string;
  lastRanAt: Date;
  createdAt?: Date;
  updatedAt?: Date;
  lastProcessedAt: Date;
  lastStatus: string;
  errorMessage?: string;
  lastPaginationCursor?: string;
}
