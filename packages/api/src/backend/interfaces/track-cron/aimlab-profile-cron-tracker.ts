import { ITrackCron } from "./track-cron";
import { IUser } from "../user";

export interface IAimlabProfileCronTracker extends Omit<ITrackCron, "entity"> {
  entity: IUser;
}
