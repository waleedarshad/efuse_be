import { Optional } from "@efuse/types";
import { Document } from "mongoose";

export interface IGoogleAccountInfo extends Document {
  kind: string;
  name: Optional<string>;
  owner: string;
  ownerKind: string;
  picture: Optional<string>;
  verified: Optional<boolean>;
}
