import { Document, Types } from "mongoose";

import { File } from "./file";

export interface IStreak extends Document {
  user: Types.ObjectId;
  streakName: string;
  streakIcon: string;
  mostRecentActiveStreakDay: string;
  isActive: boolean;
  streakDuration: number;
  createdAt?: Date;
  updatedAt?: Date;
}

export interface IValidStreakInfo {
  description: string;
  checkFunc: () => boolean;
}

export interface IValidStreak {
  dailyUsage: IValidStreakInfo;
  dailyComment: IValidStreakInfo;
}

export interface IStreakPopulatedUser extends Document {
  username: string;
  headerImage: File;
  profilePicture: File;
  status: string;
  deleted: boolean;
}

export interface IStreakPopulated extends Omit<IStreak, "user"> {
  user: Types.ObjectId | IStreakPopulatedUser;
}

export interface IUserStreak {
  daily: IStreak;
  dailyComment: IStreak;
}
