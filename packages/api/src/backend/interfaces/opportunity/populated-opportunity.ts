import { IApplicant } from "../applicant";
import { File } from "../file";
import { IGame } from "../game";
import { IOrganization } from "../organization";
import { IUser } from "../user";
import { IOpportunity } from "./opportunity";
import { IOpportunityRequirementsResponse } from "./opportunity-requirements-response";

export interface IPopulatedOpportunity extends IOpportunity {
  applicant: IApplicant | null;
  applicantAvatars: File[] | never[];
  associatedOrganization: IOrganization | null;
  associatedUser: IUser | null;
  applicantRequirements: IOpportunityRequirementsResponse;
  associatedGame: IGame | null;
}
