import { Document } from "mongoose";

export interface IOpportunityRequirements extends Document {
  name: string;
  description: string;
  isActive: boolean;
  target: string;
  property: string;
  custom: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
