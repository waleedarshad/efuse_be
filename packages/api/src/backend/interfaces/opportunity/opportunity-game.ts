import { Types, Document } from "mongoose";

export interface IOpportunityGame extends Document {
  game: Types.ObjectId | string;
  opportunity: Types.ObjectId | string;
  createdAt?: Date;
  updatedAt?: Date;
}
