import { IOrganization } from "../organization";
import { IUser } from "../user";
import { IOpportunity } from "./opportunity";

export interface IPopulateOpportunityOwner extends Omit<IOpportunity, "user" | "organization"> {
  user: IUser | null;
  organization: IOrganization | null;
}
