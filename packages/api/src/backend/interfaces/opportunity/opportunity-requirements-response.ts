import { IOpportunityRequirements } from "./opportunity-requirements";

export interface IOpportunityRequirementsResponse {
  canApply: boolean;
  requirementsMet: IOpportunityRequirements[];
  requirementsNotMet: IOpportunityRequirements[];
}
