import { IGame } from "../game";
import { IOpportunityGame } from "./opportunity-game";

export interface IPopulatedOpportunityGame extends Omit<IOpportunityGame, "game"> {
  game: IGame;
}
