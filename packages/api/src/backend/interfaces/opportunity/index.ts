export type { IOpportunity } from "./opportunity";
export type { IOpportunityAssociatedObjects } from "./opportunity-associated-objects";
export type { IPopulatedOpportunity } from "./populated-opportunity";
export type { IOpportunityRequirements } from "./opportunity-requirements";
export type { IOpportunityRequirementsResponse } from "./opportunity-requirements-response";
export type { IOpportunityGame } from "./opportunity-game";
export type { IPopulatedOpportunityGame } from "./populated-opportunity-game";
export type { IPopulateOpportunityOwner } from "./populated-opportunity-owner";
