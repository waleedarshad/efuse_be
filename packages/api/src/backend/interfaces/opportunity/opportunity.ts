import { Document, Types } from "mongoose";

import { IApplicant } from "../applicant";
import { File } from "../file";

export interface IOpportunity extends Document {
  opportunityType: string;
  subType: string;
  title: string;
  onBehalfOfOrganization: boolean;
  organization: Types.ObjectId | string;
  self: string;
  location: string;
  "applicationDueDate?"?: boolean;
  dueDate: Date;
  "gameAffiliated?"?: boolean;
  game: Types.ObjectId;
  "minimumAgeRequired?"?: boolean;
  minimumAge: number;
  "moneyIncluded?"?: boolean;
  link: string;
  description: string;
  verified: {
    status: boolean;
    verifiedBy: Types.ObjectId;
    verifiedAt: Date;
  };
  image: File;
  user: Types.ObjectId | string;
  gpaRequired: boolean;
  gpa: number;
  satRequired: boolean;
  sat: number;
  actRequired: boolean;
  act: number;
  streamRequired: boolean;
  goldRequired: boolean;
  status: string;
  candidateQuestions: [{ index: number; question: string; inputType: string }];
  shortName: string;
  publishStatus: string;
  experience_level: string;
  contract: string[];
  sector: string[];
  company_name: string;
  external: boolean;
  external_url: string;
  external_source: string;
  external_id: string;
  randomSeed: number;
  applicationCost: number;
  metadata: { views: number; boosted: boolean; rank: number };
  // For updating nested value
  "metadata.views"?: number;
  "metadata.boosted"?: boolean;
  "metadata.rank"?: number;
  promoted: boolean;
  traits: string;
  motivations: string;
  startDate: Date;
  moneyIncluded: number;
  openings: number;
  requirements: Types.ObjectId[];
  createdAt?: Date;
  updatedAt?: Date;
  applicationResponseDate?: Date;

  // Using it to resolveType for union types in graphql
  kind?: string;
  promoted_url?: string;
  applicants?: IApplicant[];
}
