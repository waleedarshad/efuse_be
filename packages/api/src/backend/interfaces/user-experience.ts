import { Types, Document } from "mongoose";

interface SubViews {
  name: string;
  views: string;
  skips: string;
}

export interface IUserExperience extends Document {
  user: string | Types.ObjectId;
  experience: string | Types.ObjectId;
  metrics: {
    globalViews: number;
    globalSkips: number;
    subViews: SubViews[];
  };
}
