export interface TwitterAPIMediaUploadResponse {
  media_id: number;
  media_id_string: string;
  media_key: string;
  size: number;
  expires_after_secs: number;
  image: {
    image_type: string;
    w: number;
    h: number;
  };
  video: {
    video_type: string;
  };
  processing_info?: {
    state: string;
    check_after_secs: number;
    error?: {
      code: number;
      name: string;
      message: string;
    };
  };
}

export interface TwitterServiceCreateTweetResponse {
  url: string;
  id: number;
}
