import { Document, Types } from "mongoose";

export interface IFriend extends Document {
  follower: Types.ObjectId;

  // todo: investigate
  // @ts-ignore
  followee: Types.ObjectId;
}
