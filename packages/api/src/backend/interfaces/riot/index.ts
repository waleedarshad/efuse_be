export type { IRiotLeagueOfLegendsProfile } from "./riot-league-of-legends-profile";
export type { IRiotProfile } from "./riot-profile";
export type { IRiotUserInfo } from "./riot-user-info";
export type { IRiotValorantProfile } from "./riot-valorant-profile";
