export interface IRiotAccountDTO {
  puuid: string;
  gameName: string;
  tagLine: string;
}
