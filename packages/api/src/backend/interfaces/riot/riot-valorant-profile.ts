import { IValorantProfile } from "@efuse/passport-riot";

export type IRiotValorantProfile = Omit<IValorantProfile, "error">;
