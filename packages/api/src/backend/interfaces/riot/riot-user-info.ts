import { IUserInfo } from "@efuse/passport-riot";

export type IRiotUserInfo = Omit<IUserInfo, "error">;
