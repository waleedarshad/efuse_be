import { ILOLProfile } from "@efuse/passport-riot";

export type IRiotLeagueOfLegendsProfile = Omit<ILOLProfile, "error">;
