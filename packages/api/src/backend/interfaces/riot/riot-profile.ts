import { Document, Types } from "mongoose";

import { IRiotUserInfo } from "./riot-user-info";
import { IRiotValorantProfile } from "./riot-valorant-profile";
import { IRiotLeagueOfLegendsProfile } from "./riot-league-of-legends-profile";

export interface IRiotProfile extends Document {
  owner: Types.ObjectId | string;
  ownerKind: string;
  leagueOfLegendsProfile: IRiotLeagueOfLegendsProfile;
  userInfo: IRiotUserInfo;
  valorantProfile: IRiotValorantProfile;
  updatedAt?: Date;
  createdAt?: Date;
}
