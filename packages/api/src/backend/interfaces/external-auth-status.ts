export interface IExternalAuthStatus {
  google: boolean;
  twitch: boolean;
  twitter: boolean;
  statespace: boolean;
  riot: boolean;
  valorant: boolean;
}
