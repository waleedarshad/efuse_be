import { Optional } from "@efuse/types";
import { Document } from "mongoose";

export interface ITwitterAccountInfo extends Document {
  description: Optional<string>;
  followers: Optional<number>;
  kind: string;
  name: Optional<string>;
  owner: string;
  ownerKind: string;
  picture: Optional<string>;
  profileBannerImage: Optional<string>;
  profileImage: Optional<string>;
  username: Optional<string>;
  verified: Optional<boolean>;
  verifiedByTwitter: Optional<boolean>;
}
