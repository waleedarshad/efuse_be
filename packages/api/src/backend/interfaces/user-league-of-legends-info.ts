import { Types } from "mongoose";

export interface IUserLeagueOfLegendsInfo {
  riotNickname?: string;
  riotRegion?: string;
  verified?: boolean;
  riotId?: string;
  verificationToken?: string;
  stats?: Types.ObjectId;
}
