import { Document, Types } from "mongoose";

export interface ITwitchFriend extends Document {
  user: Types.ObjectId | string;
  from_id: string;
  from_name: string;
  to_id: string;
  to_name: string;
  followed_at: string;
  jobRanAt: Date;
  createdAt: Date;
}
