import { Document } from "mongoose";

export interface ILeagueOfLegendsChampionData extends Document {
  /**
   * League of Legends champion ID
   */
  key: string;
  /**
   * League of Legends champion name
   */
  name: string;
  /**
   * League of Legends version corresponding to this data
   */
  version: string;
  /**
   * League of Legends CDN image link for square icon
   */
  sourceIconImage: string;
  /**
   * League of Legends CDN image link for "loading image"
   * (vertical rectangle)
   */
  sourceLoadingImage: string;
  /**
   * League of Legends CDN image link for full size splash art
   */
  sourceSplashImage: string;
  /**
   * eFuse CDN image link for square icon
   */
  iconImage: string;
  /**
   * eFuse CDN image link for "loading image"
   * (vertical rectangle)
   */
  loadingImage: string;
  /**
   * eFuse CDN image link for full size splash art
   */
  splashImage: string;
}
