/**
 * Represents a summoner's mini series stats from the official League of Legends API
 *
 * @link: https://developer.riotgames.com/apis#league-v4/GET_getLeagueEntriesForSummoner
 */
export interface IMiniSeriesDTO {
  losses: number;
  progress: string;
  target: number;
  wins: number;
}
