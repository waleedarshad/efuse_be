import { Document } from "mongoose";
import { IChampionMasteryDTO } from "./champion-mastery-dto";
import { ILeagueEntryDTO } from "./league-entry-dto";

export interface ILeagueOfLegendsStats extends Document {
  // TODO: [spavel] add user id here and get rid of 'userInfo'
  userInfo?: {
    platformSlug: string;
  };
  leagueInfo: ILeagueEntryDTO[];
  championInfo: IChampionMasteryDTO[];
  leaderboardRankTierScore: number;
  leaderboardRankTier: string;
}
