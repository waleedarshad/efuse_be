import { IMiniSeriesDTO } from "./mini-series-dto";

/**
 * Represents a summoner's league stats from the official League of Legends API
 *
 * @link: https://developer.riotgames.com/apis#league-v4/GET_getLeagueEntriesForSummoner
 */
export interface ILeagueEntryDTO {
  leagueId: string;
  summonerId: string;
  summonerName: string;
  queueType: string;
  tier: string;
  tierImage: string;
  rank: string;
  leaguePoints: number;
  wins: number;
  losses: number;
  hotStreak: boolean;
  veteran: boolean;
  freshBlood: boolean;
  inactive: boolean;
  miniSeries: IMiniSeriesDTO;
}
