/**
 * Represents a summoner from the official League of Legends API
 *
 * @link: https://developer.riotgames.com/apis#summoner-v4/GET_getBySummonerName
 */
export interface ISummonerDTO {
  accountId: string;
  profileIconId: number;
  revisionDate: number;
  name: string;
  id: string;
  puuid: string;
  summonerLevel: number;
}
