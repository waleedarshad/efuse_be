import { Document } from "mongoose";

export interface ILeagueOfLegendsVersionData extends Document {
  version: string;
}
