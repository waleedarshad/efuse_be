export type { IChampionMasteryDTO } from "./champion-mastery-dto";
export type { ILeagueEntryDTO } from "./league-entry-dto";
export type { ILeagueOfLegendsStats } from "./league-of-legends-stats";
export type { IMiniSeriesDTO } from "./mini-series-dto";
export type { ISummonerDTO } from "./summoner-dto";
