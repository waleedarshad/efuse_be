export interface IFlagsmithCondition {
  operator: string;
  property: string;
  value: string;
}
