import { IFlagsmithIdentity } from "./flagsmith-identity";

export interface IFlagsmithTrait {
  identity: IFlagsmithIdentity;
  trait_value: string;
  trait_key: string;
}
