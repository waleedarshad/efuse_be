export interface IFlagsmithIdentity {
  environment?: number;
  id?: number;
  identifier: string;
}
