export interface IFlagsmithFeature {
  id: number;
  name: string;
  created_date: string;
  description: string | null;
  initial_value: string | null;
  default_enabled: boolean;
  type: string;
}
