import { IFlagsmithSegment } from "./flagsmith-segment";

export interface IFlagSmithWebhookFeature {
  created_at: string;
  default_enabled: boolean;
  description: string;
  id: number;
  initial_value?: string | undefined;
  name: string;
  project: {
    id: number;
    name: string;
  };
  type: string;
}

export interface IFlagSmithWebhookFeatureState {
  enabled: boolean;
  environment: {
    id: number;
    name: string;
  };
  feature: IFlagSmithWebhookFeature;
  feature_segment: {
    segment: IFlagsmithSegment;
  };
  feature_state_value: string;
  identity?: string | undefined;
  identity_identifier?: string | undefined;
}

export interface IFlagSmithWebhookData {
  changed_by: string;
  new_state: IFlagSmithWebhookFeatureState;
  previous_state: IFlagSmithWebhookFeatureState;
  timestamp: string;
}

export interface IFlagSmithWebhookBody {
  data: IFlagSmithWebhookData;
  event_type: string;
}
