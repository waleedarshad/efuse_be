import { IFlagsmithFlag } from "./flagsmith-flag";
import { IFlagsmithTrait } from "./flagsmith-trait";

export interface IFlagsmithIdentityWithTraits {
  flags: IFlagsmithFlag[];
  identifier?: string;
  traits: IFlagsmithTrait[];
}
