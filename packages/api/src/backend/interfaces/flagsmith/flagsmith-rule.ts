import { IFlagsmithCondition } from "./flagsmith-condition";

export interface IFlagsmithRule {
  conditions: IFlagsmithCondition[];
  rules: unknown[];
  type: string;
}
