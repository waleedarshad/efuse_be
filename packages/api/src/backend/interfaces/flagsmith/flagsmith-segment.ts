import { IFlagsmithRule } from "./flagsmith-rule";

export interface IFlagsmithSegment {
  description: string;
  id: number;
  name: string;
  project: number;
  rules: IFlagsmithRule[];
}
