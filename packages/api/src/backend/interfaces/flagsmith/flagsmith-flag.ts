import { IFlagsmithFeature } from "./flagsmith-feature";

export interface IFlagsmithFlag {
  id: number;
  feature: IFlagsmithFeature;
  feature_state_value: string | null;
  enabled: boolean;
  environment: number | null;
  identity: number | null;
  feature_segment: number | null;
}
