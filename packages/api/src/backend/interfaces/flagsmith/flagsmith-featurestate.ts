export interface IFlagsmithFeaturestate {
  count: string;
  next: string | null;
  previous: string | null;
  results: IFlagsmithFeaturestateResult[];
}

export interface IFlagsmithFeaturestateResult {
  id: string;
  feature_state_value: string | null;
  multivariate_feature_state_values: any[];
  identity: {
    id: number;
    identifier: string;
  };
  enabled: boolean;
  feature: number;
  environment: number;
  feature_segment: string | null;
}
