import { Document, Types } from "mongoose";
import { IIpInfo } from "./ip-info";
import { IUserAgent } from "./user-agent";

export interface ISession extends Document {
  id?: Types.ObjectId | string | undefined;
  _id?: Types.ObjectId | string | undefined;
  refreshToken: string;
  active: boolean;
  revokedAt: Date;
  user: Types.ObjectId | string;
  used: boolean;
  usedAt: Date;
  expiry: Date;
  userAgent: IUserAgent;
  location: IIpInfo;
  createdAt?: Date;
  updatedAt?: Date;
}
