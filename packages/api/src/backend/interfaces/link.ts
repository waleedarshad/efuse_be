import { Document } from "mongoose";

export interface ILink extends Document {
  shortName: string;
  url: string;
  opengraph: {
    title: string;
    description: string;
    domain?: string;
    img: string;
  };
  createdAt?: Date;
  updatedAt?: Date;
}
