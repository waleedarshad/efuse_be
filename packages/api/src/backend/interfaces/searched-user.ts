import { Document } from "mongoose";
import { File } from "./file";

export interface ISearchedUser extends Document {
  username: string;
  type: string;
  name: string;
  profilePicture: File;
  verified: boolean;
}
