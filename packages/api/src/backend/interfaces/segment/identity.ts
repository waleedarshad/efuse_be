export interface IIdentityUser {
  userId: string | number;
  anonymousId?: string | number;
}

export interface IIdentityAnonymous {
  anonymousId: string | number;
  userId?: string | number;
}
