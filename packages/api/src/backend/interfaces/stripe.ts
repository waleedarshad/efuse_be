export interface IStripeError {
  statusCode?: number | string;
  raw?: { message?: string };
}
