import { Document, Types } from "mongoose";

export interface ITwitchClip extends Document {
  clip_id: string;
  url: string;
  embed_url: string;
  broadcaster_id: string;
  broadcaster_name: string;
  creator_id: string;
  creator_name: string;
  video_id: string;
  game_id: string;
  language: string;
  title: string;
  view_count: string;
  created_at: Date;
  thumbnail_url: string;
  user: Types.ObjectId;
  storedInDb: Date;
  jobRanAt: Date;
  hosted: boolean;
  hostedFile: {
    url: string;
    contentType: string;
    filename: string;
    originalUrl: string;
  };
  hostedAt: Date;
  hostingResult: string;
  hostingError: string;
}
