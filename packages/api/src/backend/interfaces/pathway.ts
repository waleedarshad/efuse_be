import { Document } from "mongoose";

export interface IPathway extends Document {
  name: string;
  description?: string;
  active: boolean;
  createdAt?: Date;
  updatedAt?: Date;
}
