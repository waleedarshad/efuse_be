import { Types } from "mongoose";

export interface IMention {
  childIndex: number;
  display: string;
  id: Types.ObjectId | string;
  index: number;
  plainTextIndex: number;
  type: string;
}
