export interface IAuth0RefreshTokenBody {
  client_id: string;
  client_secret: string;
  grant_type: string;
  scope: string;
  refresh_token: string;
}
