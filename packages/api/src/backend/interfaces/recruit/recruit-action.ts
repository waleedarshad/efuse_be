import { Document, Types } from "mongoose";

export enum ACTION {
  NO_ACTION = "No Action",
  CONTACTED = "Contacted",
  RECRUITING = "Recruiting",
  TRY_OUT = "Try Out",
  OFFERED = "Offered",
  COMMITTED = "Committed"
}

export interface IRecruitAction extends Document {
  recruitmentProfile: Types.ObjectId;
  recruiterUser: Types.ObjectId;
  ownerType: string;
  owner: Types.ObjectId;
  action: string;
  updatedAt: Date;
  createdAt: Date;
}
