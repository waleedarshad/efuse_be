const SteamStrategy = require("passport-steam").Strategy;

const { BACKEND_DOMAIN, STEAM_API_KEY } = require("@efuse/key-store");
const SteamController = require("../controllers/steam.controller");

module.exports = (passport) =>
  passport.use(
    new SteamStrategy(
      {
        returnURL: `${BACKEND_DOMAIN}/api/auth/steam/callback`,
        realm: BACKEND_DOMAIN,
        apiKey: STEAM_API_KEY
      },
      SteamController.getSteamProfile
    )
  );
