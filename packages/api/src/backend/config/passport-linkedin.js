const LinkedinStrategy = require("passport-linkedin-oauth2").Strategy;
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { BACKEND_DOMAIN, LINKEDIN_CLIENT_ID, LINKEDIN_CLIENT_SECRET } = require("@efuse/key-store");
const { LinkedinAuthController } = require("../controllers/external-account-auth/linkedin-auth.controller");

const linkedinAuthController = new LinkedinAuthController();

module.exports = (passport) => {
  if (!LINKEDIN_CLIENT_ID || !LINKEDIN_CLIENT_SECRET) {
    logger.info("Missing LinkedinClientKey/Secret.  Not enabling LinkedIn Passport");
  } else {
    passport.use(
      new LinkedinStrategy(
        {
          clientID: LINKEDIN_CLIENT_ID,
          clientSecret: LINKEDIN_CLIENT_SECRET,
          callbackURL: `${BACKEND_DOMAIN}/api/auth/linkedin/callback`,
          scope: ["r_liteprofile"],
          passReqToCallback: true
        },
        linkedinAuthController.verifyProfile
      )
    );
  }
};
