import UserExperienceController from "../controllers/legacy-user-experience.controller";
import RegistrationsController from "../controllers/registrations.controller";

export const ACTIONS_CONFIG = {
  getUserExperience: UserExperienceController.index,
  validateEmail: RegistrationsController.validateEmail
};
