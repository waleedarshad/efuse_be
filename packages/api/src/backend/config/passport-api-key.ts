import { USER_BAN_API_KEY } from "@efuse/key-store";
import { PassportStatic } from "passport";
import { HeaderAPIKeyStrategy } from "passport-headerapikey";

export const passportHeaderApiKeyStrategy = (passport: PassportStatic): void => {
  passport.use(
    new HeaderAPIKeyStrategy(
      { header: "Authorization", prefix: "X-Api-Key " },
      false,
      (apiKey: string, verified: (err: Error | null, user?: Object | undefined, info?: Object | undefined) => void) => {
        if (apiKey === USER_BAN_API_KEY) {
          return verified(null, true);
        }

        return verified(null, false);
      }
    )
  );
};
