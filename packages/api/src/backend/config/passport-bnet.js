const BnetStrategy = require("passport-bnet").Strategy;
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { BNET_CLIENT_ID, BNET_CLIENT_SECRET, BACKEND_DOMAIN } = require("@efuse/key-store");
const { BnetAuthController } = require("../controllers/external-account-auth/bnet-auth.controller");

const bnetAuthController = new BnetAuthController();

module.exports = (passport) => {
  if (BNET_CLIENT_ID && BNET_CLIENT_SECRET) {
    return passport.use(
      new BnetStrategy(
        {
          clientID: BNET_CLIENT_ID,
          clientSecret: BNET_CLIENT_SECRET,
          callbackURL: `${BACKEND_DOMAIN}/api/auth/bnet/callback`,
          region: "us",
          passReqToCallback: true
        },
        bnetAuthController.verifyProfile
      )
    );
  }
  logger.info("Missing Bnet Client Id/Secret.  Not enabling Bnet Passport");
};
