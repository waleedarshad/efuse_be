import { Logger } from "@efuse/logger";

const logger = Logger.create();

if (!process.env.NODE_ENV) {
  logger.fatal("You must set the NODE_ENV environment variable to determine BE environment");
  logger.fatal("e.g. NODE_ENV=production or NODE_ENV=staging");

  process.exit(2);
}

// REDIS_ENDPOINT is used for local development and not pulled in via SSM
if (!process.env.REDIS_ENDPOINT) {
  if (process.env.ECS_CLUSTER) {
    logger.fatal("REDIS_ENDPOINT Environment not set. Looks like running in ECS. Exiting.");

    process.exit(9);
  } else {
    logger.warn("REDIS_ENDPOINT Environment Variable not set. Defaulting to localhost");
    process.env.REDIS_ENDPOINT = "localhost";
  }
}
