const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const RiotStrategy = require("@efuse/passport-riot").Strategy;
const { BACKEND_DOMAIN, RIOT_CLIENT_ID, RIOT_CLIENT_SECRET } = require("@efuse/key-store");

const { RiotAuthController } = require("../controllers/external-account-auth/riot-auth.controller");

const riotAuthController = new RiotAuthController();

module.exports = (passport) => {
  if (RIOT_CLIENT_ID && RIOT_CLIENT_SECRET) {
    const strategy = new RiotStrategy(
      // todo: investigate
      // @ts-ignore
      {
        clientID: RIOT_CLIENT_ID,
        clientSecret: RIOT_CLIENT_SECRET,
        callbackURL: `${BACKEND_DOMAIN}/api/auth/riot/callback`,
        scope: "cpid offline_access openid",
        passReqToCallback: true
      },
      riotAuthController.verifyProfile
    );

    // // Forces the user to reconfirm their consent, even if no new scopes are requested
    strategy.authorizationParams = (options) => {
      return { prompt: "consent" };
    };

    return passport.use(strategy);
  }
  logger.info("Missing Riot Client Id/Secret. Not enabling Riot Passport");
};
