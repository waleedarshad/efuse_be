const { Logger } = require("@efuse/logger");
const GoogleStrategy = require("passport-google-oauth20").Strategy;

const { BACKEND_DOMAIN, YOUTUBE_GOOGLE_ID, YOUTUBE_GOOGLE_SECRET } = require("@efuse/key-store");

const { GoogleAuthController } = require("../controllers/external-account-auth/google-auth.controller");

const googleAuthController = new GoogleAuthController();
const logger = Logger.create({ name: "backend/config/passport-google.js" });

module.exports = (passport) => {
  if (YOUTUBE_GOOGLE_ID && YOUTUBE_GOOGLE_SECRET) {
    passport.use(
      new GoogleStrategy(
        {
          clientID: YOUTUBE_GOOGLE_ID,
          clientSecret: YOUTUBE_GOOGLE_SECRET,
          callbackURL: `${BACKEND_DOMAIN}/api/auth/youtube/callback`,
          // todo: investigate
          // @ts-ignore
          accessType: "offline",
          prompt: "consent",
          scope: ["openid", "profile", "https://www.googleapis.com/auth/youtube.readonly"],
          passReqToCallback: true
        },
        googleAuthController.verifyProfile
      )
    );
  } else {
    logger.info("Missing Youtube Id/Secret. Not enabling Youtube Passport");
  }
};
