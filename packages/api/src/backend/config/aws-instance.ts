import { AWS_ACCESS_KEY_ID, AWS_S3_REGION, AWS_SECRET_ACCESS_KEY } from "@efuse/key-store";
import AWS from "aws-sdk";

AWS.config.update({
  accessKeyId: AWS_ACCESS_KEY_ID,
  region: AWS_S3_REGION,
  secretAccessKey: AWS_SECRET_ACCESS_KEY
});

export { AWS };
