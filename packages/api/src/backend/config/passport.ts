import { PassportStatic } from "passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";
import { JwtPayload } from "jsonwebtoken";
import * as Sentry from "@sentry/node";

import { Session } from "../models/session";
import { User } from "../models/User";
import { IUser } from "../interfaces/user";
import { UserFields } from "../../lib/users/user-fields";

import { JWT_KEY, JWT_MAX_AGE } from "../../config";
import { UserStatus } from "@efuse/entities";

const opts: StrategyOptions = {
  algorithms: ["HS256"],
  audience: "eFuse-app",
  issuer: "api.efuse.gg",
  jsonWebTokenOptions: {
    maxAge: JWT_MAX_AGE.toString()
  },
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: JWT_KEY
};

export const efuseJwt = (passport: PassportStatic) => {
  passport.use(
    "efuseJwt",
    new Strategy(opts, async (jwt_payload: JwtPayload, done) => {
      try {
        const { sub, jti } = jwt_payload;

        if (!sub || !jti) {
          return done(null, false);
        }

        const session = await Session.findOne({
          _id: jti,
          active: true
        }).lean();

        if (!session) {
          return done(null, false);
        }

        const user: IUser = await User.findById(sub).select(UserFields.JWT_USER_FIELDS).cache(10);

        if (!user || user.status === UserStatus.BLOCKED) {
          return done(null, false);
        }

        Sentry.configureScope((scope) => {
          scope.setUser({ email: user.email });
        });

        return done(null, user);
      } catch (error) {
        return done(error, false);
      }
    })
  );
};
