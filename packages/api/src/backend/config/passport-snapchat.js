const SnapchatStrategy = require("passport-snapchat").Strategy;
const { Logger } = require("@efuse/logger");
const logger = Logger.create();

const { BACKEND_DOMAIN, SNAPCHAT_CLIENT_ID, SNAPCHAT_OAUTH_SECRET } = require("@efuse/key-store");
const { SnapchatAuthController } = require("../controllers/external-account-auth/snapchat-auth.controller");

const snapchatAuthController = new SnapchatAuthController();

module.exports = (passport) => {
  if (SNAPCHAT_CLIENT_ID && SNAPCHAT_OAUTH_SECRET) {
    return passport.use(
      new SnapchatStrategy(
        {
          clientID: SNAPCHAT_CLIENT_ID,
          clientSecret: SNAPCHAT_OAUTH_SECRET,
          callbackURL: `${BACKEND_DOMAIN}/api/auth/snapchat/callback`,
          profileFields: ["id", "displayName", "bitmoji"],
          scope: ["user.external_id", "user.display_name", "user.bitmoji.avatar"],
          passReqToCallback: true
        },
        // todo: investigate
        // @ts-ignore
        snapchatAuthController.verifyProfile
      )
    );
  }
  logger.info("Missing Snapchat Client Id/Secret. Not enabling Snapchat Passport");
};
