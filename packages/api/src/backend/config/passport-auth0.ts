import { UserStatus } from "@efuse/entities";
import {
  AUTH0_EFUSE_PROFILE_CLAIM,
  AUTH0_INTERNAL_AUDIENCE,
  AUTH0_INTERNAL_DOMAIN,
  AUTH0_INTERNAL_ISSUER_URL
} from "../../config";
import { PassportStatic } from "passport";
import { ExtractJwt, Strategy, StrategyOptions } from "passport-jwt";
import jwks from "jwks-rsa";
import * as Sentry from "@sentry/node";

import { User } from "../models/User";
import { IUser } from "../interfaces/user";

interface IAuth0JwtPayload {
  iss: string;
  sub?: string;
  aud: string | string[];
  iat: number;
  exp: number;
  azp: string;
  scope: string;
}

const audience: string[] = AUTH0_INTERNAL_AUDIENCE.split(",");

const opts: StrategyOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKeyProvider: jwks.passportJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${AUTH0_INTERNAL_DOMAIN}/.well-known/jwks.json`
  }),
  algorithms: ["RS256"],
  jsonWebTokenOptions: { audience },
  issuer: AUTH0_INTERNAL_ISSUER_URL
};

export const passportAuth0Strategy = (passport: PassportStatic): void => {
  passport.use(
    "efuseAuth0Jwt",
    new Strategy(opts, async (jwt_payload: IAuth0JwtPayload, done) => {
      try {
        const { userId } = jwt_payload[AUTH0_EFUSE_PROFILE_CLAIM];

        if (!userId) {
          return done(null, false);
        }

        const user: IUser | null = await User.findById(userId).cache(10).select("+email +status");

        if (!user) {
          return done(null, false);
        }

        if (user.status === UserStatus.BLOCKED) {
          return done(null, false);
        }

        Sentry.configureScope((scope) => {
          scope.setUser({ email: user.email });
        });

        return done(null, user);
      } catch (error) {
        return done(error, false);
      }
    })
  );
};
