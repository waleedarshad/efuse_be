const TwitchStrategy = require("passport-twitch.js").Strategy;
const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const { BACKEND_DOMAIN, TWITCH_CLIENT_ID, TWITCH_SECRET } = require("@efuse/key-store");

const { TwitchAuthController } = require("../controllers/external-account-auth/twitch-auth.controller");
const twitchAuthController = new TwitchAuthController();

module.exports = (passport) => {
  if (TWITCH_SECRET && TWITCH_CLIENT_ID) {
    return passport.use(
      new TwitchStrategy(
        {
          clientID: TWITCH_CLIENT_ID,
          clientSecret: TWITCH_SECRET,
          callbackURL: `${BACKEND_DOMAIN}/api/auth/twitch/callback`,
          scope: "user:read:email",
          passReqToCallback: true
        },
        twitchAuthController.verifyProfile
      )
    );
  } else {
    logger.error("Missing Twitch Client ID/Secret. Twitch passport strategy not enabled");
  }
};
