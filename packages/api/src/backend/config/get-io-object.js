const { RedisHelper } = require("../helpers/redis.helper");

const ioObject = require("socket.io-emitter")(RedisHelper.newClient());

module.exports.io = ioObject;
