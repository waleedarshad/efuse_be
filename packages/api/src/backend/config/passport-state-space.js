const { Logger } = require("@efuse/logger");
const logger = Logger.create();
const StateSpaceStrategy = require("@efuse/passport-state-space").Strategy;
const { STATE_SPACE_CLIENT_ID, STATE_SPACE_CLIENT_SECRET, BACKEND_DOMAIN } = require("@efuse/key-store");

const { StateSpaceAuthController } = require("../controllers/external-account-auth/state-space-auth.controller");

const stateSpaceAuthController = new StateSpaceAuthController();

module.exports = (passport) => {
  if (STATE_SPACE_CLIENT_ID && STATE_SPACE_CLIENT_SECRET) {
    const strategy = new StateSpaceStrategy(
      {
        clientID: STATE_SPACE_CLIENT_ID,
        clientSecret: STATE_SPACE_CLIENT_SECRET,
        callbackURL: `${BACKEND_DOMAIN}/api/auth/statespace/callback`,
        scope: "offline_access openid user user.profile",
        passReqToCallback: true
      },
      // todo: investigate
      // @ts-ignore
      stateSpaceAuthController.verifyProfile
    );

    // forces the user to reconfirm their consent, even if no new scopes are requested
    strategy.authorizationParams = (options) => {
      return { prompt: "consent" };
    };

    return passport.use(strategy);
  }
  logger.info("Missing State Space Client Id/Secret. Not enabling State Space Passport");
};
