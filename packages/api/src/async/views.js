const Config = require("./config");

const Views = require("../lib/views");

const incrementFeedViewsQueue = Config.initQueue("incrementFeedViewsQueue");
const incrementOpportunityViewsQueue = Config.initQueue("incrementOpportunityViews");

const incrementFeedViews = (feedIds) => incrementFeedViewsQueue.add({ feedIds });
const incrementOpportunityViews = async (opportunityIds) => incrementOpportunityViewsQueue.add({ opportunityIds });

const initProcessors = () => {
  // Feed views processor
  incrementFeedViewsQueue.process((job) => Views.incrementFeedViews(job.data.feedIds));
  // Opportunity views processor
  incrementOpportunityViewsQueue.process((job) => Views.incrementOpportunityViews(job.data.opportunityIds));
};

module.exports.incrementFeedViews = incrementFeedViews;
module.exports.incrementOpportunityViews = incrementOpportunityViews;
module.exports.initProcessors = initProcessors;
module.exports.incrementFeedViewsQueue = incrementFeedViewsQueue;
module.exports.incrementOpportunityViewsQueue = incrementOpportunityViewsQueue;
