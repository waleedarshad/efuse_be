// @ts-ignore
const tracer = require("dd-trace").init({
  analytics: true,
  logInjection: true,
  env: process.env.NODE_ENV,
  service: "async"
});

const { REDIS_ENDPOINT_ASYNC, REDIS_PORT } = require("../config");
const { Logger } = require("@efuse/logger");
const { HTTP_HEADERS } = require("dd-trace/ext/formats");
const Queue = require("bull");
const Redis = require("ioredis");

const { setQueues } = require("bull-board");

// This is done because each queue attaches an event emitter on the redis.  See this PR for more context:
// https://github.com/eFuse-Inc/eFuse_BE/pull/3225
// More of a discussion in the bull queue github here:
// https://github.com/OptimalBits/bull/issues/503
const EventEmitter = require("events");
EventEmitter.defaultMaxListeners = 256;

const parentLogger = Logger.create();
const { EFuseAnalyticsUtil } = require("../lib/analytics.util");

const client = new Redis(REDIS_PORT, REDIS_ENDPOINT_ASYNC);
const subscriber = new Redis(REDIS_PORT, REDIS_ENDPOINT_ASYNC);

client.on("error", () => {
  console.error(
    "A redis error occured. Make sure redis is up and running. Refer https://github.com/eFuse-Inc/eFuse_BE/blob/master/readme.md#install-and-run-redis-locally"
  );
  process.exit(1);
});

subscriber.on("error", () => {
  console.error(
    "A redis error occured. Make sure redis is up and running. Refer https://github.com/eFuse-Inc/eFuse_BE/blob/master/readme.md#install-and-run-redis-locally"
  );
  process.exit(1);
});

// createClient is used in the queue config to reuse redis connections
const createClient = (type) => {
  switch (type) {
    case "client":
      return client;
    case "subscriber":
      return subscriber;
    default:
      return new Redis(REDIS_PORT, REDIS_ENDPOINT_ASYNC);
  }
};

const defaultConfig = {
  // Documentation here: https://github.com/OptimalBits/bull/blob/develop/REFERENCE.md#queue
  defaultJobOptions: {
    removeOnComplete: 1000, // Only keep 1k completed job logs so we don't use all redis memory
    removeOnFail: 1000 // same, but for failed jobs
  },
  settings: {
    /*
      Based on this thread, increasing guardInterval which should help redis consuming a ton of cpu/network bandwidth
      By setting it to 1000 seconds (1000 * 1000ms) with 33 works it should only call this once very ~33 seconds
      https://github.com/OptimalBits/bull/issues/1222
     */
    guardInterval: 1000 * 1000
  },
  createClient // reuse redis connections
};

const allQueues = {};

const getAllQueues = () => {
  return allQueues;
};

const getQueue = (name) => {
  return allQueues[name];
};

/**
 * initQueue creates a new queue based on the passed name.
 * If a queue already exists with that name it will return that queue instead.
 *
 * @param {string} name The name of the queue, this is used to identify existing queues to prevent multiple instances
 * @param {*} opts
 */
const initQueue = (name, opts = null) => {
  const logger = parentLogger.child({ func: name, async: true });
  const config = {};
  // Apply our default config and any config passed in
  Object.assign(config, defaultConfig, opts);

  // Let's make sure that the queue hasn't already been created.
  // We don't need multiple as it will create 3 new redis connections for each new queue
  let queue = getQueue(name);
  if (queue) {
    return queue;
  }

  // existing queue not found, create a new one
  queue = new Queue(name, config);
  allQueues[name] = queue;
  setQueues([queue]);

  // Let's patch our add to send in the tracing context of the sender
  // so we can have end-to-end tracing across our bull queues

  queue._add = queue.add;
  queue.add = (data, addOpts = {}) => {
    // Get the current span for tracing purposes.
    const currentSpan = tracer.scope().active();

    // Let's encapsulate the data being passed in to be a child object
    // so that we can inject some tracing parameters
    const payload = { data };

    // If we have a span currently active, then let's inject the tracing information
    // into the object.  This will be picked up during the processing of the request.
    if (currentSpan) {
      const carrier = {};
      tracer.inject(currentSpan, HTTP_HEADERS, carrier);

      if (carrier) {
        payload.tracing = carrier;
      }
    }

    return queue._add(payload, addOpts);
  };

  // Add metrics for all successful jobs
  queue.on("completed", () => {
    // logger.info({ job, result, name }, "job completed");
    EFuseAnalyticsUtil.incrementMetric("async.job.complete", 1, [`jobname:${name}`]);
  });

  // Add metrics for all failed jobs
  queue.on("failed", (job, error) => {
    logger.error({ job, error, name }, "job failed");
    EFuseAnalyticsUtil.incrementMetric("async.job.failed", 1, [`jobname:${name}`]);
  });

  // Add metrics for all stalled jobs
  queue.on("stalled", (job) => {
    logger.info({ job, name }, "job stalled");
    EFuseAnalyticsUtil.incrementMetric("async.job.stalled", 1, [`jobname:${name}`]);
  });

  queue.on("error", (error) => {
    logger.error({ error, name }, "job error");
    EFuseAnalyticsUtil.incrementMetric("async.job.failed", 1, [`jobname:${name}`]);
  });

  queue._process = queue.process;

  queue.process = (func) => {
    const wrappedFunc = async (job) => {
      const { tracing } = job.data;

      // We overloaded the original data passed in to inject the tracing information
      // so that we can trace across process/server boundaries.
      // Added check to make sure we only overload when we have job.data.data (fakhir)
      if (job.data && job.data.data) {
        job.data = job.data.data;
      }

      job.logger = logger;
      let span = null;
      if (tracing) {
        // Extract our tracing parameters and create a new tracing span
        // so that the tracing of the async function is tied to the original request
        // that fired this async request.
        const spanContext = tracer.extract(HTTP_HEADERS, tracing);

        // Now that we have a spanContext from the original caller, create a new span
        // and signify that we are a child of the span in original request.
        span = tracer.startSpan("async.func", {
          childOf: spanContext,
          tags: { "resource.name": name, resource: name }
        });
        // span.setTag("async.func", name);
      } else {
        span = tracer.startSpan("async.func", {
          tags: { "resource.name": name, resource: name, jobId: job.id }
        });
      }
      // Now that we have a span, activate it and call the original async function.
      try {
        const result = await tracer.scope().activate(span, () => {
          // Wrap the function in an explicit tracing span
          return func(job);
        });
        span.finish();

        return result;
      } catch (error) {
        logger.error(error, "Async exception");
      }
    };

    return queue._process(wrappedFunc);
  };

  return queue;
};

module.exports.allQueues = allQueues;
module.exports.createClient = createClient;
module.exports.defaultConfig = defaultConfig;
module.exports.getAllQueues = getAllQueues;
module.exports.getQueue = getQueue;
module.exports.initQueue = initQueue;
