const Config = require("./config");
const BlacklistLib = require("../lib/blacklist/blacklist");

const cleanupFeedQueue = Config.initQueue("cleanupFeed");

const initProcessors = async () => {
  cleanupFeedQueue.process((job) => {
    const { feedId, content } = job.data;
    return BlacklistLib.cleanupFeed(feedId, content);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.cleanupFeedQueue = cleanupFeedQueue;
