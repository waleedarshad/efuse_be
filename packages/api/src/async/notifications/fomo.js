const Config = require("../config");

const Fomo = require("../../lib/fomo/index");

const fomoNotifyFollowersAsyncQueue = Config.initQueue("fomoNotifyFollowers");

const fomoNotifyFollowersAsync = (follower, followee) => {
  fomoNotifyFollowersAsyncQueue.add({ follower, followee });
};

const initProcessors = () => {
  fomoNotifyFollowersAsyncQueue.process((job) => Fomo.fomoNotifyFollowers(job.data.follower, job.data.followee));
};

module.exports.initProcessors = initProcessors;
module.exports.fomoNotifyFollowersAsync = fomoNotifyFollowersAsync;
module.exports.fomoNotifyFollowersAsyncQueue = fomoNotifyFollowersAsyncQueue;
