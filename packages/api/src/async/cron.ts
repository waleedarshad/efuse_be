import { ILogger, Logger } from "@efuse/logger";
import { StatsEnum } from "@efuse/entities";

import { MongodbService } from "../lib/mongodb/mongodb.service";
import { Environments } from "../lib/environments";
import Config from "./config";

import { StatsService } from "../lib/stats.service";
import { createStreaksLeaderboard, createViewsLeaderboard, createEngagementsLeaderboard } from "../lib/leaderboards";
import { GDPRAuditService } from "../lib/gdpr/gdpr-audit.service";
import { FeedRankService } from "../lib/lounge/feed-rank.service";
import { getTwitterFollowersCountCron } from "../lib/users/twitter";
import { HitmarkerService } from "../lib/hitmarker/hitmarker.service";
import { LeagueOfLegendsCronService } from "../lib/leagueOfLegends/league-of-legends-cron.service";
import { NotificationsSummaryService } from "../lib/notifications/notifications-summary.service";
import { OpportunityRankService } from "../lib/opportunities/opportunity-rank.service";
import { OverwatchService } from "../lib/overwatch/overwatch.service";
import { resubExpiringTwitchWebhooks } from "../lib/twitch";
import { retryCrossPostToDiscord } from "../lib/creatortools/discordHandlers";
import { retryCrossPostToFacebookPage } from "../lib/creatortools/facebookHandlers";
import { retryTwitterCrossPosts } from "../lib/creatortools/twitterHandlers";
import { ScheduledFeedService } from "../lib/creatortools/scheduled-feed.service";
import { SitemapService } from "../lib/sitemap/sitemap.service";
import { StreaksService } from "../lib/streaks/streaks.service";
import { syncPubgStatsCron } from "../lib/game_stats/pubg";
import { syncSteamProfileCron, syncSteamFriendsCron, syncSteamGamesCron } from "../lib/steamservice";
import { TwitchClipAsyncService } from "../lib/twitch/twitch-clip-async.service";
import { TwitchFollowerAsyncService } from "../lib/twitch/twitch-follower-async.service";
import { TwitchFriendAsyncService } from "../lib/twitch/twitch-friend-async.service";
import { OpengraphTagsService } from "../lib/opengraph-tags/opengraph-tags.service";
import { UserBadgeJobService } from "../lib/badge/user-badge-job.service";
import { YoutubeService } from "../lib/youtube/youtube.service";
import { TwitchCronsEnum } from "../lib/twitch/twitch.enum";
import { MyntAssetAsyncService } from "../lib/mynt/mynt-asset-async.service";
import { AimlabAsyncService, AimlabCronType } from "../lib/aimlab";
import { TwitchVideoAsyncService } from "../lib/twitch/twitch-video-async.service";
import { TwitchAccountInfoAsyncService } from "../lib/twitch/twitch-account-info-async.service";
import { PipelineLeaderboardCronService } from "../lib/leaderboards/pipeline-leaderboard-cron.service";
import { ValorantCronService } from "../lib/valorant/valorant-cron.service";
import { ErenaCronService } from "../lib/erena/erena-cron.service";
import { OpportunityAsyncService } from "../lib/opportunities/opportunity-async.service";
import { GameService } from "../lib/game/game.service";
import { GameEnum } from "../lib/game/game.enum";
import { YoutubeCronsEnum } from "../lib/youtube/youtube.enum";
import { YoutubeVideoAsyncService } from "../lib/youtube/youtube-video-async.service";

const logger: ILogger = Logger.create();

process.on("uncaughtException", (err) => {
  logger.error({ err }, "Caught unhandled exception");
});

process.on("unhandledRejection", (err) => {
  logger.error({ err }, "Caught unhandled rejection");
});

class Cron {
  private $config: any[] = [];
  private $service: any;

  public allQueues = (): any[] => {
    return this.$config.map((config) => Config.initQueue(config.func.name));
  };

  public initialize = () => {
    MongodbService.initialize()
      .then(() => {
        this.$service = require("../lib/twitchservice");

        this.configure();
      })
      .then(() => {
        this.$config.forEach((config) => {
          this.createCronJob(
            config.name,
            config.func,
            config.repeatSeconds,
            config.cronExpression,
            config.environments,
            config.queueSettings
          ).catch((error) => {
            logger.error({ config, error }, "an error ocurred in config:");
          });
        });
      })
      .catch((error) => logger.error(error, "an error occurred initializing cron"));
  };

  private configure = (): void => {
    this.$config.push(
      // Creator Tools
      { name: "getTwitterFollowersCountCron", func: getTwitterFollowersCountCron, cronExpression: "0 0 * * *" }, // Run every day at 12:00 AM
      { name: "ProcessScheduledPostsCron", func: ScheduledFeedService.ProcessScheduledPostsCron, repeatSeconds: 60 }, // Run every minute
      { name: "resubExpiringTwitchWebhooks", func: resubExpiringTwitchWebhooks, repeatSeconds: 300 },
      { name: "retryCrossPostToDiscord", func: retryCrossPostToDiscord, repeatSeconds: 120 },
      { name: "retryCrossPostToFacebookPage", func: retryCrossPostToFacebookPage, repeatSeconds: 120 },
      { name: "retryTwitterCrossPosts", func: retryTwitterCrossPosts, repeatSeconds: 120 },
      // Stats
      // { name: "collectBullStats", func: collectBullStats, repeatSeconds: 10 },
      { name: StatsEnum.COLLECT_STATS_CRON, func: StatsService.collectStatsCron, repeatSeconds: 60 },
      // Game Stats
      {
        name: "syncLOLStats",
        func: LeagueOfLegendsCronService.SyncAllLeagueOfLegendsStatsCron,
        repeatSeconds: 7200, // run every 2 hours
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "checkUsersLOLInfo",
        func: LeagueOfLegendsCronService.CheckUsersLOLInfoCron,
        repeatSeconds: 3600, // run every hour
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "fetchLOLDataAndImages",
        func: LeagueOfLegendsCronService.FetchLeagueOfLegendsDataCron,
        cronExpression: "0 3 * * *", // Run every day at 3:00 AM
        queueSettings: {
          lockDuration: 60000
        },
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: AimlabCronType.UPDATE,
        func: AimlabAsyncService.refreshStatsCron,
        repeatSeconds: 60, // run every minute
        environments: Environments.PRODUCTION_ONLY
      },
      { name: "SyncOverwatchStats", func: OverwatchService.SyncAllUserOverwatchStatsCron, repeatSeconds: 3600 }, // Run after 60 minutes
      { name: "syncPubgStatsCron", func: syncPubgStatsCron, cronExpression: "0 */6 * * *" }, // Run every 6 hours
      // Hitmarker
      {
        name: "pruneHitmarkerJobs",
        func: HitmarkerService.pruneHitmarkerJobs,
        repeatSeconds: 28800,
        environments: Environments.PRODUCTION_AND_STAGING
      }, // Run every 8 hours
      {
        name: "syncHitmarkerJobs",
        func: HitmarkerService.syncHitmarkerJobs,
        repeatSeconds: 28800,
        environments: Environments.PRODUCTION_AND_STAGING
      }, // Run every 8 hours
      // Link Tracking
      { name: "UpdateOGTagsCron", func: OpengraphTagsService.UpdateOGTagsCron, cronExpression: "15 0 * * *" }, // Run every day at 12:15 AM
      // Twitchservice
      { name: TwitchCronsEnum.TWITCH_CLIPS, func: TwitchClipAsyncService.SyncTwitchClipsCron, repeatSeconds: 300 }, // Run every 5 minutes
      {
        name: TwitchCronsEnum.TWITCH_FOLLOWERS,
        func: TwitchFollowerAsyncService.SyncTwitchFollowersCron,
        repeatSeconds: 300 // Run every 5 minutes
      },
      {
        name: TwitchCronsEnum.TWITCH_FRIENDS,
        func: TwitchFriendAsyncService.SyncTwitchFriendsCron,
        repeatSeconds: 300 // Run every 5 minutes
      },
      {
        name: TwitchCronsEnum.TWITCH_VIDEOS,
        func: TwitchVideoAsyncService.SyncTwitchVideosCron,
        repeatSeconds: 300 // Run every 5 minutes
      },
      {
        name: TwitchCronsEnum.TWITCH_INFO,
        func: TwitchAccountInfoAsyncService.SyncTwitchInfoCron,
        repeatSeconds: 300 // Run every 5 minutes
      },
      { name: "updateLiveStream", func: this.$service.updateLiveStream, repeatSeconds: 60 }, // Run every minute
      // Steamservice
      { name: "syncSteamProfileCron", func: syncSteamProfileCron, cronExpression: "0 6 * * *" }, // Run every day at 06:00AM
      { name: "syncSteamFriendsCron", func: syncSteamFriendsCron, cronExpression: "0 7 * * *" }, // Run every day at 07:00AM
      { name: "syncSteamGamesCron", func: syncSteamGamesCron, cronExpression: "0 8 * * *" }, // Run every day at 08:00AM
      // Streak
      { name: "endStreakCron", func: StreaksService.endStreakCron, repeatSeconds: 3600 }, // Run every hour
      // Sitemap
      { name: "syncSitemapCron", func: SitemapService.SyncSitemapCron, repeatSeconds: 3600 }, // Run every hour
      // Weekly Summary notification
      { name: "WeeklySummaryCron", func: NotificationsSummaryService.WeeklySummaryCron, cronExpression: "0 18 * * 1" }, // Run every monday at 06:00pm UTC which is 2pm EST
      // Leaderboard
      { name: "createStreaksLeaderboard", func: createStreaksLeaderboard, cronExpression: "0 1 * * *" }, // Run every day at 1:00am
      { name: "createViewsLeaderboard", func: createViewsLeaderboard, cronExpression: "15 0 * * *" }, // Run every day at 12:15am
      { name: "createEngagementsLeaderboard", func: createEngagementsLeaderboard, cronExpression: "20 0 * * *" }, // Run every day at 12:20am
      // Lounge Feed
      { name: "refreshFeedRankCron", func: FeedRankService.RefreshFeedRankCron, cronExpression: "*/5 * * * *" }, // Run every 5 minutes
      // GDPR
      {
        name: "DeleteSoftDeletedRecords",
        func: GDPRAuditService.DeleteSoftDeletedRecords,
        cronExpression: "10 0 * * *"
      }, // Run every data at 12:10am
      // Opportunity Rank
      {
        name: "RefreshOpportunityRankCron",
        func: OpportunityRankService.RefreshOpportunityRankCron,
        repeatSeconds: 3600
      }, // Run every hour,

      // Youtube channel stats
      {
        name: YoutubeCronsEnum.YOUTUBE_STATS,
        func: YoutubeService.SyncAllYoutubeStatsCron,
        cronExpression: "0 0 * * 0"
      }, // Run every week on sunday

      // Youtube videos Cron
      {
        name: YoutubeCronsEnum.YOUTUBE_VIDEOS,
        func: YoutubeVideoAsyncService.SyncYoutubeVideosCron,
        repeatSeconds: 300
      }, // Run every 5 minutes

      // User Badges
      {
        name: "CreateAffiliateBadgeCron",
        func: UserBadgeJobService.CreateAffiliateBadgeCron,
        cronExpression: "30 0 * * *" // Run every day at 12:30AM
      },
      {
        name: "RemoveAffiliateBadgeCron",
        func: UserBadgeJobService.RemoveAffiliateBadgeCron,
        cronExpression: "45 0 * * *"
      }, // Run every day at 12:45AM

      // Mynt
      {
        name: "SyncOMyntAssetsCron",
        func: MyntAssetAsyncService.SyncMyntAssetsCron,
        cronExpression: "*/30 * * * *" // Run every 30 minutes
      },
      // Generate Aimlab Leaderboard
      {
        name: "GenerateAndSaveAimlabLeaderboardCron",
        func: PipelineLeaderboardCronService.GenerateAndSaveAimlabLeaderboardCron,
        repeatSeconds: 900 // Every 15 minutes
      },
      // Generate League of Legends Leaderboard
      {
        name: "GenerateAndSaveLeagueOfLegendsLeaderboardCron",
        func: PipelineLeaderboardCronService.GenerateAndSaveLeagueOfLegendsLeaderboardCron,
        repeatSeconds: 900 // Every 15 minutes
      },
      // Update Aimlab Leaderboard History
      {
        name: "UpdateAimlabLeaderboardHistoryCron",
        func: PipelineLeaderboardCronService.UpdateAimlabLeaderboardHistoryCron,
        cronExpression: "40 0 * * *" // Every day at 12:40am
      },
      // Update League of Legends Leaderboard History
      {
        name: "UpdateLeagueOfLegendsLeaderboardHistoryCron",
        func: PipelineLeaderboardCronService.UpdateLeagueOfLegendsLeaderboardHistoryCron,
        cronExpression: "42 0 * * *", // Every day at 12:42am
        environments: Environments.PRODUCTION_ONLY
      },
      // Generate Valorant Leaderboard
      {
        name: "GenerateAndSaveValorantLeaderboardCron",
        func: PipelineLeaderboardCronService.GenerateAndSaveValorantLeaderboardCron,
        environments: Environments.PRODUCTION_ONLY,
        repeatSeconds: 900 // Every 15 minutes
      },
      {
        name: "FetchValorantDataCron",
        func: ValorantCronService.FetchValorantDataCron,
        cronExpression: "0 4 * * *", // Run every day at 4:00 AM
        queueSettings: {
          lockDuration: 60000
        },
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "FetchValorantContentDataCron",
        func: ValorantCronService.FetchValorantContentDataCron,
        cronExpression: "30 4 * * *", // Run every day at 4:30 AM
        queueSettings: {
          lockDuration: 60000
        },
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "ProcessValorantLeaderboards",
        func: ValorantCronService.ProcessValorantLeaderboardsCron,
        repeatSeconds: 3600, // Run every hour
        queueSettings: {
          lockDuration: 60000
        },
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "FetchAndSaveUserMatchlistCron",
        func: ValorantCronService.FetchAndSaveUserMatchlistCron,
        repeatSeconds: 7200, // Run every 2 hours
        queueSettings: {
          lockDuration: 60000
        },
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "RefreshAllValorantStatsCron",
        func: ValorantCronService.RefreshAllValorantStatsCron,
        repeatSeconds: 3600, // Run every hour
        queueSettings: {
          lockDuration: 60000
        },
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: "UpdateValorantGameNamesCron",
        func: ValorantCronService.UpdateValorantGameNamesCron,
        repeatSeconds: 7200, // Run every 2 hours
        environments: Environments.PRODUCTION_ONLY
      },
      // Update Valorant Leaderboard History
      {
        name: "UpdateValorantLeaderboardHistoryCron",
        func: PipelineLeaderboardCronService.UpdateValorantLeaderboardHistoryCron,
        cronExpression: "44 0 * * *", // Every day at 12:44am
        environments: Environments.PRODUCTION_ONLY
      },
      // Valorant recent match crons are disabled until Riots API limits are increased

      // Update North America valorant matches
      // {
      //   name: "FetchAndSaveValorantRecentMatchesNACron",
      //   func: ValorantCronService.FetchAndSaveValorantRecentMatchesNACron,
      //   repeatSeconds: 300, // Every 5 minutes
      //   environments: Environments.PRODUCTION_ONLY
      // },
      // // Update Asia-Pacific valorant matches
      // {
      //   name: "FetchAndSaveValorantRecentMatchesAPCron",
      //   func: ValorantCronService.FetchAndSaveValorantRecentMatchesAPCron,
      //   repeatSeconds: 300, // Every 5 minutes
      //   environments: Environments.PRODUCTION_ONLY
      // },
      // // Update BR valorant matches
      // {
      //   name: "FetchAndSaveValorantRecentMatchesBRCron",
      //   func: ValorantCronService.FetchAndSaveValorantRecentMatchesBRCron,
      //   repeatSeconds: 300, // Every 5 minutes
      //   environments: Environments.PRODUCTION_ONLY
      // },
      // // Update EU valorant matches
      // {
      //   name: "FetchAndSaveValorantRecentMatchesEUCron",
      //   func: ValorantCronService.FetchAndSaveValorantRecentMatchesEUCron,
      //   repeatSeconds: 300, // Every 5 minutes
      //   environments: Environments.PRODUCTION_ONLY
      // },
      // // Update Korea valorant matches
      // {
      //   name: "FetchAndSaveValorantRecentMatchesKRCron",
      //   func: ValorantCronService.FetchAndSaveValorantRecentMatchesKRCron,
      //   repeatSeconds: 300, // Every 5 minutes
      //   environments: Environments.PRODUCTION_ONLY
      // },
      // // Update LATAM valorant matches
      // {
      //   name: "FetchAndSaveValorantRecentMatchesLatamCron",
      //   func: ValorantCronService.FetchAndSaveValorantRecentMatchesLatamCron,
      //   repeatSeconds: 300, // Every 5 minutes
      //   environments: Environments.PRODUCTION_ONLY
      // },

      // Close Opportunities on due date
      {
        name: "CloseOpportunitiesByDueDateCron",
        func: OpportunityAsyncService.CloseOpportunitiesByDueDateCron,
        cronExpression: "0 4 * * *", // Run every day at 4AM (12:00AM EST)
        environments: Environments.PRODUCTION_ONLY
      },
      {
        name: GameEnum.SYNC_WITH_ALGOLIA_CRON,
        func: GameService.SyncGamesWithAlgoliaCron,
        cronExpression: "0 1 * * *"
      }, // Run every day at 1:00 AM
      // Update eRena events state
      {
        name: "UpdateErenaEventsStateCron",
        func: ErenaCronService.UpdateErenaEventStatusCron,
        cronExpression: "*/5 * * * *", // Run every 5 minutes
        environments: Environments.PRODUCTION_AND_STAGING
      }
    );
  };

  private createCronJob = async (
    name,
    func,
    seconds,
    cronExpression = null,
    environments = [],
    queueSettings = {}
  ): Promise<any> => {
    const cronMsg = cronExpression || `${seconds} seconds`;

    const { NODE_ENV } = process.env;

    if (environments.length > 0 && !environments.includes(<never>NODE_ENV)) {
      logger.warn(`Skipping cron ${name} for environment: ${NODE_ENV}`);
      return;
    }

    logger.info(`Registering new cron for ${name} to run every ${cronMsg}`);

    const queueName = `${name}-${seconds || cronExpression}`;

    const queueOptions = { settings: queueSettings };

    const queue = Config.initQueue(queueName, queueOptions);

    // Remove any pre-existing crons for function in the event the cron definition
    // has changed.  Otherwise multiple crons will be run.

    const jobs = await queue.getRepeatableJobs();

    jobs.forEach((job) => {
      logger.info(`Removing pre-existing cron job: ${job.key}`);
      queue.removeRepeatableByKey(job.key).catch((error) => {
        logger.error(error);
      });
    });

    // Set the passed function to be the processor
    queue.process(func).catch((error) => {
      logger.error(error);
    });

    // Add the cron
    const repeat = cronExpression ? { cron: cronExpression } : { every: seconds * 1000 };
    const myJob = await queue.add({}, { repeat });

    return myJob;
  };
}

export const cron: Cron = new Cron();

// if we are running directly
try {
  if (require.main === module) {
    cron.initialize();
  }
} catch (error) {
  logger.error(error, "a require error occurred initializing cron");
}
