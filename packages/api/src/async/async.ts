// eslint-disable-next-line @typescript-eslint/no-var-requires
require("dd-trace").init({
  analytics: true,
  logInjection: true,
  env: process.env.NODE_ENV,
  service: "async"
});

import { ILogger, Logger } from "@efuse/logger";

import { MongodbService } from "../lib/mongodb/mongodb.service";

import blacklist from "./blacklist";
import comments from "./core/comments";
import comprehend from "../lib/feeds/comprehend";
import discord from "./creator-tools/discord";
import emailNotifications from "../lib/notifications/email";
import facebook from "./creator-tools/facebook";
import feed from "../lib/feeds";
import fomo from "./notifications/fomo";
import leaderboard from "../lib/leaderboards";
import lounge from "./core/lounge";
import notifications from "../lib/notifications/dispatcher";
import profileProgress from "../lib/recruitment/profile-progress";
import pubg from "./stats/pubg";
import steamservice from "./steam-service";
import streamchat from "../lib/messaging/streamchat";
import stripe from "./core/stripe";
import subscription from "../lib/subscriptions";
import twitchClip from "./twitch-service/twitch-clip";
import twitchCreatortool from "./creator-tools/twitch";
import twitter from "./creator-tools/twitter";
import twitterCore from "./core/twitter";
import usersLib from "../lib/users";
import views from "./views";

import { AimlabAsyncService } from "../lib/aimlab";
import { AlgoliaService } from "../lib/algolia/algolia.service";
import { ApplicantService } from "../lib/applicants/applicant.service";
import { BrazeService } from "../lib/braze/braze.service";
import { ERenaBracketService } from "../lib/erena/erena-bracket.service";
import { FeedRankService } from "../lib/lounge/feed-rank.service";
import { FeedService } from "../lib/feeds/feed.service";
import { HypeAsyncService } from "../lib/hype/hype-async.service";
import { LeagueOfLegendsQueueService } from "../lib/leagueOfLegends/league-of-legends-api-queue.service";
import { LearningArticleService } from "../lib/learningArticles/learning-article.service";
import { MediaService } from "../lib/media/media.service";
import { MemberBusinessRulesService } from "../lib/member/member-business-rules.service";
import { NotificationsSummaryService } from "../lib/notifications/notifications-summary.service";
import { OpengraphTagsService } from "../lib/opengraph-tags/opengraph-tags.service";
import { OpportunityAsyncService } from "../lib/opportunities/opportunity-async.service";
import { OpportunityRankService } from "../lib/opportunities/opportunity-rank.service";
import { OpportunitySearchService } from "../lib/opportunities/opportunity-search.service";
import { OrganizationSearchService } from "../lib/organizations/organization-search.service";
import { PipelineLeaderboardQueueService } from "../lib/leaderboards/pipeline-leaderboard-queue.service";
import { ScheduledFeedService } from "../lib/creatortools/scheduled-feed.service";
import { StreaksService } from "../lib/streaks/streaks.service";
import { StreamChatService } from "../lib/streamchat/streamchat.service";
import { TrackingEventService } from "../lib/tracking-event/tracking-event.service";
import { TwitchAccountInfoAsyncService } from "../lib/twitch/twitch-account-info-async.service";
import { TwitchAsyncService } from "../lib/twitch/twitch-async.service";
import { TwitchClipAsyncService } from "../lib/twitch/twitch-clip-async.service";
import { TwitchFollowerAsyncService } from "../lib/twitch/twitch-follower-async.service";
import { TwitchFriendAsyncService } from "../lib/twitch/twitch-friend-async.service";
import { TwitchVideoAsyncService } from "../lib/twitch/twitch-video-async.service";
import { UserSearchService } from "../lib/users/user-search.service";
import { ValorantAsyncService } from "../lib/valorant/valorant-async.service";
import { YoutubeAsyncService } from "../lib/youtube/youtube-async.service";
import { YoutubeVideoAsyncService } from "../lib/youtube/youtube-video-async.service";
import { FeatureFlagAsyncService } from "../lib/feature-flags/feature-flag-async.service";
import { HomeFeedAsyncService } from "../lib/home-feed/home-feed-async.service";
import { SessionService } from "../lib/session/session.service";
import { PromotedService } from "../lib/ads/promoted.service";

const logger: ILogger = Logger.create({ name: "async" });

process.on("uncaughtException", (err) => {
  logger.error({ err }, "Caught unhandled exception");
});

process.on("unhandledRejection", (err) => {
  logger.error({ err }, "Caught unhandled rejection");
});

class Async {
  private twitchservice: any;

  public initialize = () =>
    MongodbService.initialize()
      .then(() => blacklist.initProcessors())
      .then(() => comprehend.initProcessors())
      .then(() => discord.initProcessors())
      .then(() => lounge.initProcessors())
      .then(() => facebook.initProcessors())
      .then(() => feed.initProcessors())
      .then(() => pubg.initProcessors())
      .then(() => twitter.initProcessors())
      .then(() => twitterCore.initProcessors())
      .then(() => steamservice.initProcessors())
      .then(() => stripe.initProcessors())
      .then(() => twitchCreatortool.initProcessors())
      .then(() => {
        this.twitchservice = require("./twitch-service");

        return this.twitchservice.initProcessors();
      })
      .then(() => twitchClip.initProcessors())
      .then(() => comments.initProcessors())
      .then(() => fomo.initProcessors())
      .then(() => leaderboard.initAsyncProcessors())
      .then(() => notifications.initProcessors())
      .then(() => profileProgress.initAsyncProcessors())
      .then(() => StreaksService.initAsyncProcessors())
      .then(() => streamchat.initAsyncProcessors())
      .then(() => subscription.initAsyncProcessors())
      .then(() => usersLib.initAsyncProcessors())
      .then(() => views.initProcessors())
      .then(() => AimlabAsyncService.initAsyncProcessors())
      .then(() => AlgoliaService.InitAsyncProcessors())
      .then(() => ApplicantService.InitAsyncProcessors())
      .then(() => BrazeService.InitAsyncProcessors())
      .then(() => ERenaBracketService.InitAsyncProcessors())
      .then(() => FeedRankService.InitAsyncProcessors())
      .then(() => FeedService.InitAsyncProcessors())
      .then(() => HypeAsyncService.InitAsyncProcessors())
      .then(() => LeagueOfLegendsQueueService.InitAsyncProcessors())
      .then(() => LearningArticleService.InitAsyncProcessors())
      .then(() => MediaService.InitAsyncProcessors())
      .then(() => MemberBusinessRulesService.InitAsyncProcessors())
      .then(() => NotificationsSummaryService.InitAsyncProcessors())
      .then(() => OpengraphTagsService.InitAsyncProcessors())
      .then(() => OpportunityAsyncService.InitAsyncProcessors())
      .then(() => OpportunityRankService.InitAsyncProcessors())
      .then(() => OpportunitySearchService.InitAsyncProcessors())
      .then(() => OrganizationSearchService.InitAsyncProcessors())
      .then(() => PipelineLeaderboardQueueService.InitAsyncProcessors())
      .then(() => ScheduledFeedService.InitAsyncProcessors())
      .then(() => StreamChatService.InitAsyncProcessors())
      .then(() => TrackingEventService.InitAsyncProcessors())
      .then(() => TwitchAccountInfoAsyncService.InitAsyncProcessors())
      .then(() => TwitchAsyncService.InitAsyncProcessors())
      .then(() => TwitchClipAsyncService.InitAsyncProcessors())
      .then(() => TwitchFollowerAsyncService.InitAsyncProcessors())
      .then(() => TwitchFriendAsyncService.InitAsyncProcessors())
      .then(() => TwitchVideoAsyncService.InitAsyncProcessors())
      .then(() => UserSearchService.InitAsyncProcessors())
      .then(() => ValorantAsyncService.InitAsyncProcessors())
      .then(() => YoutubeAsyncService.InitAsyncProcessors())
      .then(() => YoutubeVideoAsyncService.InitAsyncProcessors())
      .then(() => emailNotifications.initAsyncProcessors())
      .then(() => FeatureFlagAsyncService.InitAsyncProcessors())
      .then(() => HomeFeedAsyncService.InitAsyncProcessors())
      .then(() => SessionService.InitAsyncProcessors())
      .then(() => PromotedService.InitAsyncProcessors())
      .catch((error) => logger.error(error, "an error occurred registering async processors"));
}

const async = new Async();

// If we are run direct
try {
  if (require.main === module) {
    async.initialize().catch((error) => {
      logger.error(error, "an error occurred initializing async");
    });
  }
} catch (error) {
  logger.error(error, "a require error occurred initializing async");
}

export const initialize = async.initialize;
