const Config = require("../config");
const { LoungeFeedService } = require("../../lib/lounge/lounge-feed.service");

const cleanupUsersLoungeFeedQueue = Config.initQueue("cleanupUsersLoungeFeed");
const updateLoungeFeedQueue = Config.initQueue("updateLoungeFeed");

const initProcessors = async () => {
  const loungeFeedService = new LoungeFeedService();
  updateLoungeFeedQueue.process((job) => {
    const { userId, feedId, timelineable, timelineableType, verified } = job.data;
    return loungeFeedService.updateLoungeFeed(userId, feedId, timelineable, timelineableType, verified);
  });

  cleanupUsersLoungeFeedQueue.process((job) => {
    return loungeFeedService.cleanupUsersLoungeFeed(job.data.userId);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.cleanupUsersLoungeFeedQueue = cleanupUsersLoungeFeedQueue;
module.exports.updateLoungeFeedQueue = updateLoungeFeedQueue;
