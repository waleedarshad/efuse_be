const Config = require("../config");
const CommentsAsync = require("../../lib/users/comments");

const addCommentToFeedQueue = Config.initQueue("addCommentToFeed");
const initToggleCommentLikeQueue = Config.initQueue("initToggleCommentLike");
const removeCommentFromFeedQueue = Config.initQueue("removeCommentFromFeed");
const toggleCommentLikeQueue = Config.initQueue("toggleCommentLike");
const updateCommentOnFeedQueue = Config.initQueue("updateCommentOnFeed");

const initProcessors = () => {
  initToggleCommentLikeQueue.process((job) => {
    const { feedId, userId, commentId, isLiked, value } = job.data;
    const result = CommentsAsync.initToggleCommentLike(feedId, userId, commentId, isLiked, value);

    return result;
  });

  toggleCommentLikeQueue.process((job) => {
    const { commentId, isLiked, value, homeFeedId, user } = job.data;
    const result = CommentsAsync.toggleCommentLike(commentId, isLiked, value, homeFeedId, user);

    return result;
  });

  addCommentToFeedQueue.process((job) => {
    const { feedId, userId, comment } = job.data;
    return CommentsAsync.addCommentToFeed(feedId, userId, comment);
  });

  removeCommentFromFeedQueue.process((job) => {
    const { feedId, userId, commentId } = job.data;
    return CommentsAsync.removeCommentFromFeed(feedId, userId, commentId);
  });

  updateCommentOnFeedQueue.process((job) => {
    const { feedId, userId, commentId, commentData } = job.data;
    return CommentsAsync.updateCommentOnFeed(feedId, userId, commentId, commentData);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.addCommentToFeedQueue = addCommentToFeedQueue;
module.exports.initToggleCommentLikeQueue = initToggleCommentLikeQueue;
module.exports.removeCommentFromFeedQueue = removeCommentFromFeedQueue;
module.exports.toggleCommentLikeQueue = toggleCommentLikeQueue;
module.exports.updateCommentOnFeedQueue = updateCommentOnFeedQueue;
