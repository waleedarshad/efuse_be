const Config = require("../config");
const TwitterLib = require("../../lib/users/twitter");

const getTwitterFollowersQueue = Config.initQueue("getTwitterFollowers");

const initProcessors = async () => {
  getTwitterFollowersQueue.process((job) => {
    const { twitterUsername } = job.data;
    return TwitterLib.getTwitterFollowers(twitterUsername);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.getTwitterFollowersQueue = getTwitterFollowersQueue;
