const Config = require("../config");
const StripeLib = require("../../lib/stripe");

const createStripeCustomerQueue = Config.initQueue("createStripeCustomer");

const initProcessors = async () => {
  createStripeCustomerQueue.process((job) => {
    const { email, name, userId } = job.data;
    return StripeLib.createStripeCustomer(email, name, userId);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.createStripeCustomerQueue = createStripeCustomerQueue;
