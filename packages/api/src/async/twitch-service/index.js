const Config = require("../config");
const TwitchServiceLib = require("../../lib/twitchservice");

const fetchAndUpdateStreamInfoQueue = Config.initQueue("fetchAndUpdateStreamInfo");
const storeStreamDataQueue = Config.initQueue("storeStreamData");

const initProcessors = async () => {
  storeStreamDataQueue.process((job) => {
    const { streams } = job.data;
    return TwitchServiceLib.storeStreamData(streams);
  });

  fetchAndUpdateStreamInfoQueue.process((job) => {
    const { users } = job.data;
    return TwitchServiceLib.fetchAndUpdateStreamInfo(users);
  });
};

module.exports.fetchAndUpdateStreamInfoQueue = fetchAndUpdateStreamInfoQueue;
module.exports.initProcessors = initProcessors;
module.exports.storeStreamDataQueue = storeStreamDataQueue;
