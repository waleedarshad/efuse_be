const Config = require("../config");
const { TwitchClipMediaService } = require("../../lib/twitch/twitch-clip-media.service");

const uploadTwitchClipQueue = Config.initQueue("uploadTwitchClip");

const initProcessors = async () => {
  const twitchClipMediaService = new TwitchClipMediaService();
  uploadTwitchClipQueue.process((job) => {
    const { feedId, twitchClipId } = job.data;
    return twitchClipMediaService.uploadTwitchClip(feedId, twitchClipId);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.uploadTwitchClipQueue = uploadTwitchClipQueue;
