const Config = require("../config");
const DiscordHandlers = require("../../lib/creatortools/discordHandlers");

const crossPostToDiscordQueue = Config.initQueue("crossPostToDiscord");

const initProcessors = async () => {
  crossPostToDiscordQueue.process((job) => {
    const { userId, content, mediaId, mediaIds } = job.data;
    return DiscordHandlers.crossPostToDiscord(userId, content, mediaId, mediaIds);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.crossPostToDiscordQueue = crossPostToDiscordQueue;
