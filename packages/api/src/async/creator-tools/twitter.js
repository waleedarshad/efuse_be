const Config = require("../config");
const TwitterHandlers = require("../../lib/creatortools/twitterHandlers");

const crossPostToTwitterQueue = Config.initQueue("crossPostToTwitter");

const initProcessors = async () => {
  crossPostToTwitterQueue.process((job) => {
    const { userId, content, mediaId } = job.data;
    return TwitterHandlers.crossPostToTwitter(userId, content, mediaId);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.crossPostToTwitterQueue = crossPostToTwitterQueue;
