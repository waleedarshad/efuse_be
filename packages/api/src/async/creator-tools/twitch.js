const Config = require("../config");
const TwitchHandlerLib = require("../../lib/creatortools/twitchHandler");

const subscribeToStreamChangeWebhookQueue = Config.initQueue("subscribeToStreamChangeWebhook");

const initProcessors = async () => {
  subscribeToStreamChangeWebhookQueue.process((job) =>
    TwitchHandlerLib.subscribeToStreamChangeWebhook(job.data.owner, job.data.ownerKind)
  );
};

module.exports.initProcessors = initProcessors;
module.exports.subscribeToStreamChangeWebhookQueue = subscribeToStreamChangeWebhookQueue;
