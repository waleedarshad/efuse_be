const Config = require("../config");
const FacebookHandlers = require("../../lib/creatortools/facebookHandlers");

const crossPostToFacebookPageQueue = Config.initQueue("crossPostToFacebookPage");

const initProcessors = async () => {
  crossPostToFacebookPageQueue.process((job) => {
    const { userId, content, mediaId, mediaIds } = job.data;
    return FacebookHandlers.crossPostToFacebookPage(userId, content, mediaId, mediaIds);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.crossPostToFacebookPageQueue = crossPostToFacebookPageQueue;
