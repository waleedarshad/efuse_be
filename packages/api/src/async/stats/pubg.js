const Config = require("../config");
const PubgLib = require("../../lib/game_stats/pubg");

// PUBG API rate limit is 10 requests per minute
const PUBG_API_LIMITS = {
  max: 10,
  duration: 60000
};

const syncPubgStatsQueue = Config.initQueue("syncPubgStats", {
  limiter: PUBG_API_LIMITS
});

const initProcessors = async () => {
  syncPubgStatsQueue.process((job) => {
    const { user } = job.data;
    return PubgLib.syncPubgStats(user);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.syncPubgStatsQueue = syncPubgStatsQueue;
