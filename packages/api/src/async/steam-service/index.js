const Config = require("../config");
const SteamServiceLib = require("../../lib/steamservice");

const syncSteamFriendsQueue = Config.initQueue("syncSteamFriends");
const syncSteamGamesQueue = Config.initQueue("syncSteamGames");
const syncSteamProfileQueue = Config.initQueue("syncSteamProfile");

const initProcessors = async () => {
  syncSteamProfileQueue.process((job) => {
    const { userId } = job.data;
    return SteamServiceLib.syncSteamProfile(userId);
  });

  syncSteamFriendsQueue.process((job) => {
    const { userId } = job.data;
    return SteamServiceLib.syncSteamFriends(userId);
  });

  syncSteamGamesQueue.process((job) => {
    const { userId } = job.data;
    return SteamServiceLib.syncSteamGames(userId);
  });
};

module.exports.initProcessors = initProcessors;
module.exports.syncSteamFriendsQueue = syncSteamFriendsQueue;
module.exports.syncSteamGamesQueue = syncSteamGamesQueue;
module.exports.syncSteamProfileQueue = syncSteamProfileQueue;
