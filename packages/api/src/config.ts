import * as envVars from "@efuse/key-store";

export const BULLET_TRAIN_ENVIRONMENT = envVars.BULLET_TRAIN_ENVIRONMENT || "test";
export const REDIS_ENDPOINT_ASYNC = envVars.REDIS_ENDPOINT_ASYNC || "localhost";
export const REDIS_ENDPOINT = envVars.REDIS_ENDPOINT || "localhost";
export const REDIS_PORT = parseInt(envVars.REDIS_PORT) || 6379;
export const JWT_KEY = envVars.JWT_KEY || "supersekrit";
export const JWT_MAX_AGE = envVars.JWT_MAX_AGE || 3600;
export const AUTH0_EFUSE_PROFILE_CLAIM = envVars.AUTH0_EFUSE_PROFILE_CLAIM || "";
export const AUTH0_INTERNAL_AUDIENCE = envVars.AUTH0_INTERNAL_AUDIENCE || "";
export const AUTH0_INTERNAL_DOMAIN = envVars.AUTH0_INTERNAL_DOMAIN || "";
export const AUTH0_INTERNAL_ISSUER_URL = envVars.AUTH0_INTERNAL_ISSUER_URL || "";
export const AWS_S3_BUCKET = envVars.AWS_S3_BUCKET || "nonexistent_bucket";
export const AWS_S3_REGION = envVars.AWS_S3_REGION || "";
export const AUTH0_CLIENT_ID = envVars.AUTH0_CLIENT_ID || "";
export const AUTH0_CLIENT_SECRET = envVars.AUTH0_CLIENT_SECRET || "";
export const AUTH0_SCOPES = envVars.AUTH0_SCOPES || "";
export const MONGO_DB_URL = envVars.MONGO_DB_URL || "mongodb://localhost:27017/test";
export const MONGO_DB_POOL_SIZE = envVars.MONGO_DB_POOL_SIZE || 10;
