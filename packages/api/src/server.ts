const START_TIME = new Date().getTime();

// Import the tracer so guarantee it is initialized before anything else
import "./tracer";

import { Logger } from "@efuse/logger";
import yargs from "yargs";

const logger = Logger.create();

logger.info("check-environment initialization");
import "./backend/config/check-environment";

import { configure } from "./backend/app";
import { initialize } from "./async/async";

const TAG = "[server]";

import { MongodbService } from "./lib/mongodb/mongodb.service";

MongodbService.initialize()
  .then(() => {
    logger.info("Starting backend/app initialization");
    return configure();
  })
  .then((appInitializationPayload) => {
    // Setting up defaults for async, cron for local development
    const argv = yargs.option("async", { default: true }).parseSync();

    return { appInitializationPayload, argv };
  })
  .then(({ appInitializationPayload, argv }) => {
    if (argv.async) {
      logger.info("Starting async processors.  To disable run:  npm run server -- --no-async");

      initialize().catch((error) => {
        logger.error(error, "unable to initialize async");
      });
    } else {
      logger.info("Not starting async processing");
    }

    return appInitializationPayload;
  })
  .then((appInitializationPayload) => {
    const { server } = appInitializationPayload;

    const port = process.env.PORT || 5000;

    server.listen(port, () => {
      const NOW = new Date().getTime();
      const STARTUP_TIME = (NOW - START_TIME) / 1000;
      logger.warn({ startup_time: STARTUP_TIME }, `💣\u00A0\u00A0Server running on port: ${port}`);
    });

    return true;
  })
  .catch((error) => logger.error(error, `${TAG} an error occurred configuring express application`));
