import request from "supertest";
import { Express } from "express";
import mongoose from "mongoose";
import { registerUser } from "./utils/auth";
import { configure } from "../src/backend/app";

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
});

describe("/api/twitch/:user_id/clips", () => {
  describe("GET", () => {
    it("should get clips for the provided user", async () => {
      const { token, user } = await registerUser(app);

      await mongoose.connection.db.collection("twitchclips").insertOne({ user: mongoose.Types.ObjectId(user.id) });

      await request(app)
        .get(`/api/twitch/${user.id}/clips`)
        .set("Authorization", token)
        .expect(({ body }) => expect(body.clips.docs.length).toBeGreaterThan(0));
    });
  });
});
