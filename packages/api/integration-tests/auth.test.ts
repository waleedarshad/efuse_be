import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";

jest.setTimeout(60000);

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
});

describe("api/auth/register", () => {
  describe("POST", () => {
    it("should return OK with tokens", async () => {
      const newUser = {
        email: "thor@avengers.com",
        name: "Thor",
        dateOfBirth: new Date(1983, 7, 11),
        username: "GodOfThunder",
        password: "pointbreak1"
      };

      await request(app)
        .post("/api/auth/register")
        .send({
          user: newUser,
          platform: "web"
        })
        .expect(200)
        .expect("Content-Type", /json/)
        .expect(({ body }) =>
          expect(body).toEqual(
            expect.objectContaining({
              refreshToken: expect.any(String),
              token: expect.any(String),
              user: expect.anything(),
              message: "Logged in successfully!"
            })
          )
        );
    });
  });
});

describe("api/auth/login", () => {
  describe("POST", () => {
    it("should return OK with tokens", async () => {
      await request(app)
        .post("/api/auth/login")
        .send({ email: "thor@avengers.com", password: "pointbreak1", platform: "web" })
        .expect(200)
        .expect(({ body }) =>
          expect(body).toEqual(
            expect.objectContaining({
              refreshToken: expect.any(String),
              token: expect.any(String),
              user: expect.anything(),
              message: "Logged in successfully!"
            })
          )
        );
    });
  });
});
