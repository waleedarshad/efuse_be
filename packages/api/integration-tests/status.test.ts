import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";

jest.setTimeout(60000);

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
}, 60000);

describe("api/__status__", () => {
  describe("GET", () => {
    it("should return OK", async () => {
      await request(app)
        .get("/api/__status__")
        .expect(200)
        .expect("Content-Type", /text\/html/)
        .expect("OK");
    });
  });
});
