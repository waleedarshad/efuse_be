import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";
import { mongoose } from "@typegoose/typegoose";
import { registerUser } from "./utils/auth";
import { Types } from "mongoose";
import { ObjectId } from "../src/backend/types";
import {
  LeagueBracketType,
  LeagueEntryType,
  LeagueEventState,
  LeagueEventTimingMode,
  OwnerType
} from "@efuse/entities";

describe("LeaguePool Resolver Tests", () => {
  let app: Express;
  let token: string;
  let user: any;
  let poolId: ObjectId;
  let teams: ObjectId[];
  let context: { userId: ObjectId };
  let eventId: ObjectId;
  let gameId: ObjectId;
  let orgId: ObjectId;
  let leagueId: ObjectId;

  let eventNoOwnerId: ObjectId;
  let userNotOwnerEventId: ObjectId;
  let poolNoEventId: ObjectId;

  beforeAll(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;
    context = { userId: user.id };

    const game = await mongoose.connection.db.collection("games").insertOne({
      description: "Warzone description.",
      gameImage: {
        url: "efuse.gg"
      },
      kind: "game",
      slug: "warzone-test-1",
      title: "Call of Duty: Warzone"
    });

    gameId = game.insertedId.toString();

    const org = await mongoose.connection.db.collection("organizations").insertOne({
      name: "Owner of League",
      user: context.userId,
      owner: context.userId,
      shortName: "organization",
      slug: "organization-slug"
    });

    orgId = org.insertedId.toString();

    const team1 = await mongoose.connection.db.collection("teams").insertOne({
      description: "team 1 is cool",
      game: gameId,
      name: "team 1",
      owner: context.userId,
      ownerType: OwnerType.ORGANIZATIONS
    });

    const team2 = await mongoose.connection.db.collection("teams").insertOne({
      description: "team 2 is dope",
      game: gameId,
      name: "team 2",
      owner: context.userId,
      ownerType: OwnerType.ORGANIZATIONS
    });

    teams = [team1.insertedId.toString(), team2.insertedId.toString()];

    const league = await mongoose.connection.db.collection("leagues").insertOne({
      name: "The Best League",
      imageUrl: "image",
      description: "a test",
      owner: orgId,
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: gameId,
      teams: teams,
      rules: "no rules"
    });

    leagueId = league.insertedId.toString();

    const event = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      description: "test event description",
      details: "test event details",
      gamesPerMatch: 1,
      league: leagueId,
      maxTeams: 8,
      name: "test event",
      rules: "some rules",
      startDate: new Date(),
      state: LeagueEventState.PENDING_TEAMS,
      teams: teams,
      timingMode: LeagueEventTimingMode.WEEKLY
    });

    eventId = Types.ObjectId(event.insertedId.toString());

    const pool = await mongoose.connection.db.collection("leaguepools").insertOne({
      name: "Test Pool",
      teams: teams,
      event: eventId
    });

    poolId = pool.insertedId.toString();

    const leagueNoOwner = await mongoose.connection.db.collection("leagues").insertOne({
      name: "League No Owner",
      imageUrl: "image",
      description: "no owner test",
      owner: undefined,
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: gameId,
      teams: teams,
      rules: "no rules"
    });

    const leagueNoOwnerId = leagueNoOwner.insertedId.toString();

    const eventNoOwner = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      description: "event no owner description",
      details: "event no owner details",
      gamesPerMatch: 1,
      league: leagueNoOwnerId,
      maxTeams: 8,
      name: "event no owner",
      rules: "some rules",
      startDate: new Date(),
      state: LeagueEventState.PENDING_TEAMS,
      teams: teams,
      timingMode: LeagueEventTimingMode.WEEKLY
    });

    eventNoOwnerId = Types.ObjectId(eventNoOwner.insertedId.toString());

    const userNotOwnerLeagueResponse = await mongoose.connection.db.collection("leagues").insertOne({
      name: "user not owner",
      imageUrl: "image",
      description: "user not owner",
      owner: Types.ObjectId(), // random id for owner
      ownerType: OwnerType.USERS,
      entryType: LeagueEntryType.OPEN,
      game: gameId,
      teams: teams,
      rules: "no rules"
    });

    const userNotOwnerLeagueId = userNotOwnerLeagueResponse.insertedId.toString();

    const userNotOwnerEventResponse = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      description: "event no owner description",
      details: "event no owner details",
      gamesPerMatch: 1,
      league: userNotOwnerLeagueId,
      maxTeams: 8,
      name: "event no owner",
      rules: "some rules",
      startDate: new Date(),
      state: LeagueEventState.PENDING_TEAMS,
      teams: teams,
      timingMode: LeagueEventTimingMode.WEEKLY
    });

    userNotOwnerEventId = Types.ObjectId(userNotOwnerEventResponse.insertedId.toString());

    const poolNoEvent = await mongoose.connection.db.collection("leaguepools").insertOne({
      name: "Event doesn't exist",
      teams: teams,
      event: Types.ObjectId()
    });

    poolNoEventId = poolNoEvent.insertedId.toString();
  }, 600000);

  describe("Queries", () => {
    it("getLeaguePoolById returns a pool found by its id", async () => {
      const postData = {
        operationName: "getLeaguePoolById",
        variables: { poolId: poolId },
        query: `query getLeaguePoolById($poolId: String!) {
          getLeaguePoolById(poolId: $poolId) {
            _id
            name
            event {
              name
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) =>
          expect(res.body).toEqual({
            data: { getLeaguePoolById: { _id: poolId, name: "Test Pool", event: { name: "test event" } } }
          })
        );
    });

    it("getLeaguePoolById returns null when searching for pool that doesn't exist", async () => {
      const fakePoolId = Types.ObjectId(); // random id that doesn't exist in db

      const postData = {
        operationName: "getLeaguePoolById",
        variables: { poolId: fakePoolId },
        query: `query getLeaguePoolById($poolId: String!) {
          getLeaguePoolById(poolId: $poolId) {
            _id
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) =>
          expect(res.body).toEqual({
            data: { getLeaguePoolById: null }
          })
        );
    });

    it("getPaginatedPools returns a pool found by its id when page and limit are set", async () => {
      const paginatedArgs = {
        eventId: eventId,
        page: 1,
        limit: 1
      };

      const postData = {
        operationName: "getPaginatedPools",
        variables: paginatedArgs,
        query: `query getPaginatedPools($eventId: String!, $page: Int, $limit: Int) {
          getPaginatedPools(eventId: $eventId, page: $page, limit: $limit) {
            docs {
              _id
            }
            page
            limit
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              getPaginatedPools: {
                docs: [
                  {
                    _id: poolId
                  }
                ],
                page: paginatedArgs.page,
                limit: paginatedArgs.limit
              }
            }
          })
        );
    });

    it("getPaginatedPools returns a pool found by its id when page and limit are default", async () => {
      const paginatedArgs = {
        eventId: eventId
      };

      const postData = {
        operationName: "getPaginatedPools",
        variables: paginatedArgs,
        query: `query getPaginatedPools($eventId: String!) {
          getPaginatedPools(eventId: $eventId) {
            docs {
              _id
            }
            page
            limit
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              getPaginatedPools: {
                docs: [
                  {
                    _id: poolId
                  }
                ],
                page: 1,
                limit: 1
              }
            }
          })
        );
    });

    it("getPaginatedPools returns second page of pools", async () => {
      const pool2 = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "Test Pool 2",
        teams: teams,
        event: eventId
      });

      const pool2Id = pool2.insertedId.toString();

      const paginatedArgs = {
        eventId: eventId,
        page: 2,
        limit: 1
      };

      const postData = {
        operationName: "getPaginatedPools",
        variables: paginatedArgs,
        query: `query getPaginatedPools($eventId: String!, $page: Int, $limit: Int) {
          getPaginatedPools(eventId: $eventId, page: $page, limit: $limit) {
            docs {
              _id
            }
            page
            limit
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              getPaginatedPools: {
                docs: [
                  {
                    _id: pool2Id
                  }
                ],
                page: paginatedArgs.page,
                limit: paginatedArgs.limit
              }
            }
          })
        );
    });
  });

  describe("Mutations", () => {
    it("createLeaguePool creates a pool", async () => {
      const poolArgs = {
        eventId: eventId,
        name: "a created pool",
        teams: teams
      };

      const postData = {
        operationName: "createLeaguePool",
        variables: { eventId: poolArgs.eventId, name: poolArgs.name, teams: poolArgs.teams, context: context },
        query: `mutation createLeaguePool($eventId: String!, $name: String, $teams: [String]) {
          createLeaguePool(eventId: $eventId, name: $name, teams: $teams) {
            name
            event {
              name
            }
            teams {
              name
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              createLeaguePool: {
                name: poolArgs.name,
                event: { name: "test event" },
                teams: [{ name: "team 1" }, { name: "team 2" }]
              }
            }
          })
        );
    });

    it("createLeaguePool returns a failure when no league owner exists", async () => {
      const poolArgs = {
        eventId: eventNoOwnerId,
        name: "a created pool",
        teams: teams
      };

      const postData = {
        operationName: "createLeaguePool",
        variables: { eventId: poolArgs.eventId, name: poolArgs.name, teams: poolArgs.teams, context: context },
        query: `mutation createLeaguePool($eventId: String!, $name: String, $teams: [String]) {
          createLeaguePool(eventId: $eventId, name: $name, teams: $teams) {
            name
            event {
              name
            }
            teams {
              name
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "Missing owner",
              statusCode: 400
            })
          )
        );
    });

    it("createLeaguePool returns a 403 error when user is not the league owner", async () => {
      const poolArgs = {
        eventId: userNotOwnerEventId,
        name: "a created pool",
        teams: teams
      };

      const postData = {
        operationName: "createLeaguePool",
        variables: { eventId: poolArgs.eventId, name: poolArgs.name, teams: poolArgs.teams, context: context },
        query: `mutation createLeaguePool($eventId: String!, $name: String, $teams: [String]) {
          createLeaguePool(eventId: $eventId, name: $name, teams: $teams) {
            name
            event {
              name
            }
            teams {
              name
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "User does not have permission to perform this action.",
              statusCode: 403
            })
          )
        );
    });

    it("generateLeagueTeamPools returns 422 when no event exists", async () => {
      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: poolNoEventId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "Event does not exist",
              statusCode: 422
            })
          )
        );
    });

    it("generateLeagueTeamPools returns pools when they already exist", async () => {
      const postData = {
        operationName: "generateLeagueTeamPools",
        variables: { eventId: eventId, context: context },
        query: `mutation generateLeagueTeamPools($eventId: String!) {
          generateLeagueTeamPools(eventId: $eventId) {
            name
            event {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              generateLeagueTeamPools: [
                {
                  event: {
                    _id: eventId.toString()
                  },
                  name: "Test Pool"
                },
                {
                  event: {
                    _id: eventId.toString()
                  },
                  name: "Test Pool 2"
                },
                {
                  event: {
                    _id: eventId.toString()
                  },
                  name: "a created pool"
                }
              ]
            }
          })
        );
    });

    it("generateLeagueTeamPools returns no pools when event has no teams", async () => {
      const noTeamsEvent = await mongoose.connection.db.collection("leagueevents").insertOne({
        bracketType: LeagueBracketType.ROUND_ROBIN,
        description: "event no owner description",
        details: "event no owner details",
        gamesPerMatch: 1,
        league: leagueId,
        maxTeams: 8,
        name: "event no owner",
        rules: "some rules",
        startDate: new Date(),
        state: LeagueEventState.PENDING_TEAMS,
        teams: [],
        timingMode: LeagueEventTimingMode.WEEKLY
      });

      const noTeamsEventId = noTeamsEvent.insertedId.toString();

      const postData = {
        operationName: "generateLeagueTeamPools",
        variables: { eventId: noTeamsEventId, context: context },
        query: `mutation generateLeagueTeamPools($eventId: String!) {
          generateLeagueTeamPools(eventId: $eventId) {
            name
            event {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              generateLeagueTeamPools: []
            }
          })
        );
    });

    it("generateLeagueTeamPools returns 406 when there is an uneven number of teams", async () => {
      const oneTeamEvent = await mongoose.connection.db.collection("leagueevents").insertOne({
        bracketType: LeagueBracketType.ROUND_ROBIN,
        description: "event no owner description",
        details: "event no owner details",
        gamesPerMatch: 1,
        league: leagueId,
        maxTeams: 8,
        name: "event no owner",
        rules: "some rules",
        startDate: new Date(),
        state: LeagueEventState.PENDING_TEAMS,
        teams: [Types.ObjectId()],
        timingMode: LeagueEventTimingMode.WEEKLY
      });

      const oneTeamEventId = oneTeamEvent.insertedId.toString();

      const postData = {
        operationName: "generateLeagueTeamPools",
        variables: { eventId: oneTeamEventId, context: context },
        query: `mutation generateLeagueTeamPools($eventId: String!) {
          generateLeagueTeamPools(eventId: $eventId) {
            name
            event {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "There must be an even number of teams",
              statusCode: 406
            })
          )
        );
    });

    it("updateLeaguePool returns a 403 error when user is not the event owner", async () => {
      const poolToUpdate = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "Random Event",
        teams: teams,
        event: userNotOwnerEventId
      });

      const poolToUpdateId = poolToUpdate.insertedId.toString();

      const poolBody = {
        name: "updated name",
        teams: []
      };

      const postData = {
        operationName: "updateLeaguePool",
        variables: { poolId: poolToUpdateId, poolBody: poolBody, context: context },
        query: `mutation updateLeaguePool($poolId: String!, $poolBody: UpdateLeaguePoolBody!) {
          updateLeaguePool(poolId: $poolId, poolBody: $poolBody) {
            name
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "User does not have permission to perform this action.",
              statusCode: 403
            })
          )
        );
    });

    it("updateLeaguePool updates name and teams of pool", async () => {
      const poolToUpdate = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "Random Event",
        teams: teams,
        event: eventId
      });

      const poolToUpdateId = poolToUpdate.insertedId.toString();

      const poolBody = {
        name: "updated name",
        teams: []
      };

      const postData = {
        operationName: "updateLeaguePool",
        variables: { poolId: poolToUpdateId, poolBody: poolBody, context: context },
        query: `mutation updateLeaguePool($poolId: String!, $poolBody: UpdateLeaguePoolBody!) {
          updateLeaguePool(poolId: $poolId, poolBody: $poolBody) {
            name
            teams {
              name
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              updateLeaguePool: {
                name: "updated name",
                teams: []
              }
            }
          })
        );
    });

    it("moveTeamsToLeaguePool returns a 403 when fromPool doesn't have permissions", async () => {
      const fromPoolTeams = [Types.ObjectId(), Types.ObjectId()];

      const fromPool = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "From Pool",
        teams: fromPoolTeams,
        event: userNotOwnerEventId
      });

      const fromPoolId = fromPool.insertedId.toString();

      const postData = {
        operationName: "moveTeamsToLeaguePool",
        variables: { fromPoolId: fromPoolId, toPoolId: poolId, teams: fromPoolTeams, context: context },
        query: `mutation moveTeamsToLeaguePool($fromPoolId: String, $toPoolId: String, $teams: [String]) {
          moveTeamsToLeaguePool(fromPoolId: $fromPoolId, toPoolId: $toPoolId, teams: $teams) {
            name
            teams {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "User does not have permission to perform this action.",
              statusCode: 403
            })
          )
        );
    });

    it("moveTeamsToLeaguePool returns a 403 when toPool doesn't have permissions", async () => {
      const toPool = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "To Pool",
        teams: [],
        event: userNotOwnerEventId
      });

      const toPoolId = toPool.insertedId.toString();

      const postData = {
        operationName: "moveTeamsToLeaguePool",
        variables: { fromPoolId: poolId, toPoolId: toPoolId, teams: teams, context: context },
        query: `mutation moveTeamsToLeaguePool($fromPoolId: String, $toPoolId: String, $teams: [String]) {
          moveTeamsToLeaguePool(fromPoolId: $fromPoolId, toPoolId: $toPoolId, teams: $teams) {
            name
            teams {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "User does not have permission to perform this action.",
              statusCode: 403
            })
          )
        );
    });

    it("moveTeamsToLeaguePool update pools and check that both have correct teams", async () => {
      const toPool = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "To Pool",
        teams: [],
        event: eventId
      });

      const toPoolId = toPool.insertedId.toString();

      const postData = {
        operationName: "moveTeamsToLeaguePool",
        variables: { fromPoolId: poolId, toPoolId: toPoolId, teams: teams, context: context },
        query: `mutation moveTeamsToLeaguePool($fromPoolId: String, $toPoolId: String, $teams: [String]) {
          moveTeamsToLeaguePool(fromPoolId: $fromPoolId, toPoolId: $toPoolId, teams: $teams) {
            name
            teams {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              moveTeamsToLeaguePool: [
                {
                  name: "Test Pool",
                  teams: []
                },
                {
                  name: "To Pool",
                  teams: [{ _id: teams[0].toString() }, { _id: teams[1].toString() }]
                }
              ]
            }
          })
        );
    });

    it("clearAllTeamsInLeaguePool returns a 403 when user is not event owner", async () => {
      const postData = {
        operationName: "clearAllTeamsInLeaguePools",
        variables: { eventId: userNotOwnerEventId, context: context },
        query: `mutation clearAllTeamsInLeaguePools($eventId: String) {
          clearAllTeamsInLeaguePools(eventId: $eventId) {
            name
            teams {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "User does not have permission to perform this action.",
              statusCode: 403
            })
          )
        );
    });

    it("clearAllTeamsInLeaguePool clears all teams", async () => {
      const postData = {
        operationName: "clearAllTeamsInLeaguePools",
        variables: { eventId: eventId, context: context },
        query: `mutation clearAllTeamsInLeaguePools($eventId: String) {
          clearAllTeamsInLeaguePools(eventId: $eventId) {
            name
            teams {
              _id
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: {
              clearAllTeamsInLeaguePools: [
                {
                  name: "Test Pool",
                  teams: []
                },
                {
                  name: "Test Pool 2",
                  teams: []
                },
                {
                  name: "a created pool",
                  teams: []
                },
                {
                  name: "updated name",
                  teams: []
                },
                {
                  name: "To Pool",
                  teams: []
                }
              ]
            }
          })
        );
    });

    it("deleteLeaguePool deletes a pool", async () => {
      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: poolId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body).toEqual({
            data: { deleteLeaguePool: true }
          })
        );
    });

    it("deleteLeaguePool throws error for invalid pool id", async () => {
      const invalidPoolId = Types.ObjectId();

      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: invalidPoolId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "Invalid id",
              statusCode: 400
            })
          )
        );
    });

    it("deleteLeaguePool throws error when no league owner exists", async () => {
      const poolToDelete = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "Invalid Event",
        teams: teams,
        event: eventNoOwnerId
      });

      const poolToDeleteId = poolToDelete.insertedId.toString();

      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: poolToDeleteId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "Missing owner",
              statusCode: 400
            })
          )
        );
    });

    it("deleteLeaguePool returns a 403 error when user is not the league owner", async () => {
      const poolToDelete = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "Invalid Event",
        teams: teams,
        event: userNotOwnerEventId
      });

      const poolToDeleteId = poolToDelete.insertedId.toString();

      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: poolToDeleteId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "User does not have permission to perform this action.",
              statusCode: 403
            })
          )
        );
    });

    it("deleteLeaguePool returns 422 when event does not exist", async () => {
      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: poolNoEventId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "Event does not exist",
              statusCode: 422
            })
          )
        );
    });

    it("deleteLeaguePool returns 422 when league does not exist", async () => {
      const eventNoLeague = await mongoose.connection.db.collection("leagueevents").insertOne({
        bracketType: LeagueBracketType.ROUND_ROBIN,
        description: "event no owner description",
        details: "event no owner details",
        gamesPerMatch: 1,
        league: Types.ObjectId(),
        maxTeams: 8,
        name: "event no owner",
        rules: "some rules",
        startDate: new Date(),
        state: LeagueEventState.PENDING_TEAMS,
        teams: teams,
        timingMode: LeagueEventTimingMode.WEEKLY
      });

      const eventNoLeagueId = Types.ObjectId(eventNoLeague.insertedId.toString());

      const poolToDelete = await mongoose.connection.db.collection("leaguepools").insertOne({
        name: "Random Event",
        teams: teams,
        event: eventNoLeagueId
      });

      const poolToDeleteId = poolToDelete.insertedId.toString();

      const postData = {
        operationName: "deleteLeaguePool",
        variables: { poolId: poolToDeleteId },
        query: `mutation deleteLeaguePool($poolId: String!) {
          deleteLeaguePool(poolId: $poolId)
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect((res) =>
          expect(res.body.errors[0].extensions.exception).toEqual(
            expect.objectContaining({
              message: "League does not exist",
              statusCode: 422
            })
          )
        );
    });
  });
});
