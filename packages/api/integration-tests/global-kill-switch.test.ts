import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";
import { registerUser } from "./utils/auth";
import { GlobalKillSwitch } from "../src/backend/middlewares/global-kill-switch.middleware";
import { FeatureFlagService } from "../src/lib/feature-flags/feature-flag.service";

enum KillSwitchEnums {
  DISABLE_LOGIN = "killswitch:global_disable_login",
  DISABLE_REGISTRATIONS = "killswitch:global_disable_registrations",
  DISABLE_LOUNGE_FEEDS = "killswitch:global_disable_lounge_feeds",
  DISABLE_CREATE_FEEDS = "killswitch:global_disable_create_feeds"
}

describe("GlobalKillSwitch", () => {
  let app: Express;
  let flags: string[] = [];

  const loginKillSwitchSpy = jest.spyOn(GlobalKillSwitch, "loginKillSwitch");
  const registrationKillSwitchSpy = jest.spyOn(GlobalKillSwitch, "registrationKillSwitch");
  const loungeFeedKillSwitchSpy = jest.spyOn(GlobalKillSwitch, "loungeFeedKillSwitch");
  const createFeedKillSwitchSpy = jest.spyOn(GlobalKillSwitch, "createFeedKillSwitch");

  jest.spyOn(FeatureFlagService, "hasFeature").mockImplementation((_key, _userId = ""): Promise<boolean> => {
    if (flags.includes(_key)) {
      return Promise.resolve(true);
    }
    return Promise.resolve(false);
  });

  jest.setTimeout(60000);
  beforeAll(async () => {
    const result = await configure();
    app = result.app;
  }, 60000);

  beforeEach(() => {
    loginKillSwitchSpy.mockClear();
    registrationKillSwitchSpy.mockClear();
    loungeFeedKillSwitchSpy.mockClear();
    createFeedKillSwitchSpy.mockClear();
  });

  describe("/auth/login", () => {
    let user;

    const payload = {
      email: "tony.stark@avengers.com",
      password: "iamironman1",
      platform: "web"
    };

    beforeEach(async () => {
      flags = [];
      user = await registerUser(app);
    }, 60000);

    it("Should return 200 and call loginKillSwitch middleware once", async () => {
      await request(app)
        .post("/api/auth/login")
        .send(payload)
        .expect(200)
        .expect(() => expect(loginKillSwitchSpy).toHaveBeenCalledTimes(1));
    });

    it("Should return 403 and proper error when loginKillSwitch feature is toggled on", async () => {
      flags.push(KillSwitchEnums.DISABLE_LOGIN);

      await request(app)
        .post("/api/auth/login")
        .send(payload)
        .expect(403)
        .expect({ error: "Requested resource is not available. Please try again later" });
    });
  });

  describe("/auth/register", () => {
    const payload = {
      user: {
        email: "newTestdata@gmail.com",
        name: "New Test User",
        dateOfBirth: new Date(1965, 3, 4),
        username: "newtestuser123",
        password: "iamironman1"
      },
      platform: "web"
    };

    beforeEach(() => {
      flags = [];
    });

    it("Should return 200 call registrationKillSwitch middleware once", async () => {
      await request(app)
        .post("/api/auth/register")
        .send(payload)
        .expect(200)
        .expect(() => expect(registrationKillSwitchSpy).toHaveBeenCalledTimes(1));
    });

    it("Should return 403 and proper error when registrationKillSwitch feature is toggled on", async () => {
      flags.push(KillSwitchEnums.DISABLE_REGISTRATIONS);

      await request(app)
        .post("/api/auth/register")
        .send(payload)
        .expect(403)
        .expect({ error: "Requested resource is not available. Please try again later" });
    });
  });

  describe("/api/feeds/verified", () => {
    let access;
    beforeEach(async () => {
      flags = [];
      access = await registerUser(app);
    });

    it("Should return 200 call loungeFeedKillSwitch middleware once", async () => {
      await request(app)
        .get("/api/feeds/verified")
        .set("Authorization", access.token)
        .expect(200)
        .expect(() => expect(loungeFeedKillSwitchSpy).toHaveBeenCalledTimes(1));
    });

    it("Should return 403 and proper error when loungeFeedKillSwitch feature is toggled on", async () => {
      flags.push(KillSwitchEnums.DISABLE_LOUNGE_FEEDS);

      await request(app)
        .get("/api/feeds/verified")
        .set("Authorization", access.token)
        .expect(403)
        .expect({ error: "Requested resource is not available. Please try again later" });
    });
  });

  describe("/api/feeds/lounge/featured", () => {
    let access;
    beforeEach(async () => {
      flags = [];
      access = await registerUser(app);
    });

    it("Should return 200 call loungeFeedKillSwitch middleware once", async () => {
      await request(app)
        .get("/api/feeds/lounge/featured")
        .set("Authorization", access.token)
        .expect(200)
        .expect(() => expect(loungeFeedKillSwitchSpy).toHaveBeenCalledTimes(1));
    });

    it("Should return 403 and proper error when loungeFeedKillSwitch feature is toggled on", async () => {
      flags.push(KillSwitchEnums.DISABLE_LOUNGE_FEEDS);

      await request(app)
        .get("/api/feeds/lounge/featured")
        .set("Authorization", access.token)
        .expect(403)
        .expect({ error: "Requested resource is not available. Please try again later" });
    });
  });

  describe("/api/feeds/discover/latest", () => {
    let access;
    beforeEach(async () => {
      flags = [];
      access = await registerUser(app);
    });

    it("Should return 200 call loungeFeedKillSwitch middleware once", async () => {
      await request(app)
        .get("/api/feeds/discover/latest")
        .set("Authorization", access.token)
        .expect(200)
        .expect(() => expect(loungeFeedKillSwitchSpy).toHaveBeenCalledTimes(1));
    });

    it("Should return 403 and proper error when loungeFeedKillSwitch feature is toggled on", async () => {
      flags.push(KillSwitchEnums.DISABLE_LOUNGE_FEEDS);

      await request(app)
        .get("/api/feeds/discover/latest")
        .set("Authorization", access.token)
        .expect(403)
        .expect({ error: "Requested resource is not available. Please try again later" });
    });
  });
});
