import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";

import { mongoose } from "@typegoose/typegoose";
import { registerUser } from "./utils/auth";

jest.setTimeout(60000);

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
});

describe("Hypes query", () => {
  let token;
  let user;
  let feed;
  let homeFeed;
  let hype;
  let comment;
  let commentHype;

  beforeAll(async () => {
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    feed = await mongoose.connection.db.collection("feeds").insertOne({
      metadata: {
        views: 296,
        toxicity: [],
        hashtags: ["#post"]
      },
      comments: 0,
      likes: 1,
      shared: false,
      deleted: false,
      content: "Extracting post #post",
      user: user.id,
      mentions: [],
      platform: "web",
      reports: []
    });

    homeFeed = await mongoose.connection.db.collection("homeFeeds").insertOne({
      user: user.id,
      timelineableType: "users",
      original: false,
      deleted: false,
      feed: feed.insertedId.toString(),
      timelineable: user.id
    });

    hype = await mongoose.connection.db.collection("hypes").insertOne({
      user: user.id,
      objId: feed.insertedId,
      objType: "feeds",
      platform: "web",
      hypeCount: 11
    });

    hype = await mongoose.connection.db.collection("hypes").findOne({
      _id: hype.insertedId
    });

    comment = await mongoose.connection.db.collection("comments").insertOne({
      content: "blah blah",
      user: user.id,
      commentable: feed.insertedId,
      commentableType: "feeds",
      likes: 0,
      platform: "web"
    });

    commentHype = await mongoose.connection.db.collection("hypes").insertOne({
      user: user.id,
      objId: comment.insertedId,
      objType: "comments",
      platform: "web",
      hypeCount: 1
    });

    commentHype = await mongoose.connection.db.collection("hypes").findOne({
      _id: commentHype.insertedId
    });
  }, 600000);

  describe("Hype paginated queries", () => {
    it("returns the paginated feed hypes user by feedId", async () => {
      const hypeVariables = {
        feedId: feed.insertedId.toString(),
        page: 1,
        limit: 10
      };

      const postData = {
        operationName: "getFeedHypedUsers",
        variables: hypeVariables,
        query: `query getFeedHypedUsers($limit: Int!, $page: Int!, $feedId: String!){
          getFeedHypedUsers(limit: $limit, page: $page, feedId: $feedId) {
            docs {
              id
              name
              username
              currentUserFollows
              isFollowedByUser
            }
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) => {
          expect(res.body).toEqual({
            data: {
              getFeedHypedUsers: {
                docs: [
                  {
                    id: user.id,
                    name: user.name,
                    username: user.username,
                    currentUserFollows: false,
                    isFollowedByUser: false
                  }
                ]
              }
            }
          });
        });
    });

    it("returns the paginated feed hypes by feedId", async () => {
      const hypeVariables = {
        feedId: feed.insertedId.toString(),
        page: 1,
        limit: 10
      };

      const postData = {
        operationName: "getFeedHypes",
        variables: hypeVariables,
        query: `query getFeedHypes($limit: Int!, $page: Int!, $feedId: String!){
          getFeedHypes(limit: $limit, page: $page, feedId: $feedId) {
            docs {
              id
              user {
                id
              }
              objId
              objType
              platform
              hypeCount
            }
            page
            limit
            totalDocs

          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) => {
          expect(res.body).toEqual({
            data: {
              getFeedHypes: {
                docs: [
                  {
                    id: hype._id.toString(),
                    user: {
                      id: hype.user
                    },
                    objId: hype.objId.toString(),
                    objType: hype.objType,
                    platform: hype.platform,
                    hypeCount: hype.hypeCount
                  }
                ],
                page: 1,
                limit: 10,
                totalDocs: 1
              }
            }
          });
        });
    });

    it("returns the paginated comment hyped user by commentId", async () => {
      const hypeCommentVariables = {
        commentId: comment.insertedId,
        page: 1,
        limit: 10
      };

      const postData = {
        operationName: "getCommentHypedUsers",
        variables: hypeCommentVariables,
        query: `query getCommentHypedUsers($limit: Int!, $page: Int!, $commentId: String!){
          getCommentHypedUsers(limit: $limit, page: $page, commentId: $commentId) {
            docs {
              id
              name
              username
              currentUserFollows
              isFollowedByUser
            }
            page
            limit
            totalDocs
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) => {
          expect(res.body).toEqual({
            data: {
              getCommentHypedUsers: {
                docs: [
                  {
                    id: user.id,
                    name: user.name,
                    username: user.username,
                    currentUserFollows: false,
                    isFollowedByUser: false
                  }
                ],
                page: 1,
                limit: 10,
                totalDocs: 1
              }
            }
          });
        });
    });

    it("returns the paginated comment hypes by commentId", async () => {
      const hypeCommentVariables = {
        commentId: comment.insertedId,
        page: 1,
        limit: 10
      };

      const postData = {
        operationName: "getCommentHypes",
        variables: hypeCommentVariables,
        query: `query getCommentHypes($limit: Int!, $page: Int!, $commentId: String!){
          getCommentHypes(limit: $limit, page: $page, commentId: $commentId) {
            docs {
              id
              user {
                id
              }
              objId
              objType
              platform
              hypeCount
            }
            page
            limit
            totalDocs

          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) => {
          expect(res.body).toEqual({
            data: {
              getCommentHypes: {
                docs: [
                  {
                    id: commentHype._id.toString(),
                    user: {
                      id: commentHype.user
                    },
                    objId: commentHype.objId.toString(),
                    objType: commentHype.objType,
                    platform: commentHype.platform,
                    hypeCount: commentHype.hypeCount
                  }
                ],
                page: 1,
                limit: 10,
                totalDocs: 1
              }
            }
          });
        });
    });
  });

  describe("Hype paginated Mutations", () => {
    it("should hype comment", async () => {
      const hypeCommentVariables = {
        commentId: comment.insertedId,
        hypeCount: 1,
        platform: "web"
      };

      const postData = {
        operationName: "HypeComment",
        variables: hypeCommentVariables,
        query: `mutation HypeComment($hypeCount: Int!, $platform: String!, $commentId: String!){
          HypeComment(hypeCount: $hypeCount, platform: $platform, commentId: $commentId) {
            newHype
          }
        }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(postData)
        .expect(200)
        .expect((res) => {
          expect(res.body).toEqual({
            data: {
              HypeComment: {
                newHype: true
              }
            }
          });
        });
    });

    it("should unhype comment", async () => {
      const unhypeCommentVariables = {
        commentId: comment.insertedId
      };

      const unhypeData = {
        operationName: "UnHypeComment",
        variables: unhypeCommentVariables,
        query: `mutation UnHypeComment($commentId: String!){
            UnHypeComment(commentId: $commentId) {
              _id
            }
          }`
      };

      await request(app)
        .post("/api/graphql")
        .set("Authorization", token)
        .send(unhypeData)
        .expect(200)
        .expect((res) => {
          expect(res.body.data.UnHypeComment._id).toEqual(expect.any(String));
        });
    });
  });
});
