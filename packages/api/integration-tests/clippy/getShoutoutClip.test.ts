import request from "supertest";
import { Express } from "express";
import mongoose from "mongoose";
import nock from "nock";
import { registerUser } from "../utils/auth";
import { configure } from "../../src/backend/app";

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
});

describe("getShoutoutClip", () => {
  it("should get shoutout clips from twitch", async () => {
    const testChannelName = "somechannel";

    const clippyTwitchClip = {
      id: "",
      url: "",
      embed_url: "",
      broadcaster_id: "",
      broadcaster_name: "",
      creator_id: "",
      creator_name: "",
      video_id: "",
      game_id: "",
      language: "",
      title: "",
      view_count: 0,
      created_at: "",
      thumbnail_url: "thumbnail-preview",
      duration: 0
    };

    nock("https://api.twitch.tv")
      .get(`/helix/users?login=${testChannelName}`)
      .reply(200, { data: [{ id: "bcasterid" }] })
      .get("/helix/clips?broadcaster_id=bcasterid&first=1")
      .twice() // this same request is being made twice everytime we get clips?
      .reply(200, { data: [clippyTwitchClip] });

    const { token, user } = await registerUser(app);

    const twitchToken = "twitchtoken";

    const clippy = await mongoose.connection.db
      .collection("clippy")
      .insertOne({ user: mongoose.Types.ObjectId(user.id), token: twitchToken });

    const command = await mongoose.connection.db.collection("clippy.commands").insertOne({ clippy: clippy.insertedId });

    const role = await mongoose.connection.db
      .collection("roles")
      .insertOne({ value: "clippyFree", name: "testClippy" });

    await mongoose.connection.db
      .collection("entityroles")
      .insertOne({ entity: mongoose.Types.ObjectId(user.id), entityKind: "users", role: role.insertedId });

    await mongoose.connection.db
      .collection("oauthcredential")
      .insertOne({ owner: user.id, ownerKind: "users", service: "twitch" });

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send({
        operationName: "GetShoutoutClip",
        query: `
        query GetShoutoutClip($token: String!, $channelName: String!, $commandId: String!) {
          getShoutoutClip(token: $token, channelName: $channelName, commandId: $commandId) {
            id
            url
            embed_url
            broadcaster_id
            broadcaster_name
            creator_id
            creator_name
            video_id
            game_id
            language
            title
            view_count
            created_at
            thumbnail_url
            duration
            mp4_url
            game {
              name
              box_art_url
              id
            }
          }
        }

        `,
        variables: {
          token: twitchToken,
          commandId: command.insertedId,
          channelName: testChannelName
        }
      })
      .expect({
        data: {
          getShoutoutClip: {
            id: "",
            url: "",
            embed_url: "",
            broadcaster_id: "",
            broadcaster_name: "",
            creator_id: "",
            creator_name: "",
            video_id: "",
            game_id: "",
            language: "",
            title: "",
            view_count: 0,
            created_at: "",
            thumbnail_url: "thumbnail-preview",
            duration: 0,
            mp4_url: "thumbnail.mp4",
            game: null
          }
        }
      });
  });
});
