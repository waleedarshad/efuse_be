import request from "supertest";
import { Express } from "express";
import { Types } from "mongoose";
import { configure } from "../src/backend/app";
import { registerUser } from "./utils/auth";
import { mongoose } from "@typegoose/typegoose";
import { LeagueBracketType } from "@efuse/entities";
import { LeagueRoundSort } from "../src/graphql/enums";

describe("League Round API Resolver", () => {
  let app: Express;
  let token;
  let user;
  let bracket;
  let firstRound;
  let secondRound;
  let thirdRound;

  beforeAll(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    const currentUserId = Types.ObjectId(user.id);

    bracket = await mongoose.connection.db.collection("leaguebrackets").insertOne({
      name: "Bracket 1",
      pool: new Types.ObjectId(),
      type: LeagueBracketType.ROUND_ROBIN
    });

    firstRound = await mongoose.connection.db.collection("leaguerounds").insertOne({
      name: "First Round",
      bracket: bracket.insertedId,
      value: 1
    });
    secondRound = await mongoose.connection.db.collection("leaguerounds").insertOne({
      name: "Second Round",
      bracket: bracket.insertedId,
      value: 2
    });
    thirdRound = await mongoose.connection.db.collection("leaguerounds").insertOne({
      name: "Third Round",
      bracket: bracket.insertedId,
      value: 3
    });
  }, 60000);

  it("getLeagueRoundById will return a 422 round is not found", async () => {
    const roundId = new Types.ObjectId();

    const postData = {
      operationName: "getLeagueRoundById",
      variables: { roundId: roundId },
      query: `query getLeagueRoundById($roundId: String!) {
          getLeagueRoundById(roundId: $roundId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "Round does not exist",
            statusCode: 422
          })
        )
      );
  });

  it("getLeagueRoundById will return a round that is found", async () => {
    const postData = {
      operationName: "getLeagueRoundById",
      variables: { roundId: firstRound.insertedId },
      query: `query getLeagueRoundById($roundId: String!) {
            getLeagueRoundById(roundId: $roundId) {
                _id
              }
            }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getLeagueRoundById: { _id: firstRound.insertedId.toString() } }
        })
      );
  });

  it("getLeagueRoundsByBracket will return an empty array if bracket is not found", async () => {
    const notFoundBracket = new Types.ObjectId();

    const postData = {
      operationName: "getLeagueRoundsByBracket",
      variables: { bracketId: notFoundBracket },
      query: `query getLeagueRoundsByBracket($bracketId: String!) {
          getLeagueRoundsByBracket(bracketId: $bracketId) {
              _id
          }
        }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getLeagueRoundsByBracket: [] }
        })
      );
  });

  it("getLeagueRoundsByBracket will return an array with rounds", async () => {
    const postData = {
      operationName: "getLeagueRoundsByBracket",
      variables: { bracketId: bracket.insertedId },
      query: `query getLeagueRoundsByBracket($bracketId: String!) {
          getLeagueRoundsByBracket(bracketId: $bracketId) {
              name
              _id
          }
        }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueRoundsByBracket: [
              { name: "First Round", _id: firstRound.insertedId.toString() },
              { name: "Second Round", _id: secondRound.insertedId.toString() },
              { name: "Third Round", _id: thirdRound.insertedId.toString() }
            ]
          }
        })
      );
  });

  it("getPaginatedLeagueRounds should return the number of rounds the limit is set to and returns ascending order", async () => {
    const postData = {
      operationName: "getPaginatedLeagueRounds",
      variables: {
        bracketId: bracket.insertedId,
        page: 1,
        limit: 2,
        sortOn: LeagueRoundSort.NAME,
        sortDirection: "asc"
      },
      query: `query getPaginatedLeagueRounds($bracketId: String!, $page: Int!, $limit: Int!, $sortOn: LeagueRoundSort!, $sortDirection: SortDirection!) {
        getPaginatedLeagueRounds(bracketId: $bracketId, page: $page, limit: $limit, sortOn: $sortOn, sortDirection: $sortDirection) {
              docs {
                name
              }
          }
        }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getPaginatedLeagueRounds: { docs: [{ name: "First Round" }, { name: "Second Round" }] } }
        })
      );
  });

  it("getPaginatedLeagueRounds should return the number of rounds in descending order", async () => {
    const postData = {
      operationName: "getPaginatedLeagueRounds",
      variables: {
        bracketId: bracket.insertedId,
        page: 1,
        limit: 2,
        sortOn: LeagueRoundSort.NAME,
        sortDirection: "desc"
      },
      query: `query getPaginatedLeagueRounds($bracketId: String!, $page: Int!, $limit: Int!, $sortOn: LeagueRoundSort!, $sortDirection: SortDirection!) {
        getPaginatedLeagueRounds(bracketId: $bracketId, page: $page, limit: $limit, sortOn: $sortOn, sortDirection: $sortDirection) {
              docs {
                name
              }
          }
        }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getPaginatedLeagueRounds: { docs: [{ name: "Third Round" }, { name: "Second Round" }] } }
        })
      );
  });

  it("getPaginatedLeagueRounds should return the number of rounds based on sort on of value asc", async () => {
    const postData = {
      operationName: "getPaginatedLeagueRounds",
      variables: {
        bracketId: bracket.insertedId,
        page: 1,
        limit: 2,
        sortOn: LeagueRoundSort.VALUE,
        sortDirection: "asc"
      },
      query: `query getPaginatedLeagueRounds($bracketId: String!, $page: Int!, $limit: Int!, $sortOn: LeagueRoundSort!, $sortDirection: SortDirection!) {
        getPaginatedLeagueRounds(bracketId: $bracketId, page: $page, limit: $limit, sortOn: $sortOn, sortDirection: $sortDirection) {
              docs {
                value
              }
          }
        }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getPaginatedLeagueRounds: { docs: [{ value: 1 }, { value: 2 }] } }
        })
      );
  });

  it("getPaginatedLeagueRounds should return the number of rounds based on sort on of value desc", async () => {
    const postData = {
      operationName: "getPaginatedLeagueRounds",
      variables: {
        bracketId: bracket.insertedId,
        page: 1,
        limit: 2,
        sortOn: LeagueRoundSort.VALUE,
        sortDirection: "desc"
      },
      query: `query getPaginatedLeagueRounds($bracketId: String!, $page: Int!, $limit: Int!, $sortOn: LeagueRoundSort!, $sortDirection: SortDirection!) {
        getPaginatedLeagueRounds(bracketId: $bracketId, page: $page, limit: $limit, sortOn: $sortOn, sortDirection: $sortDirection) {
              docs {
                value
              }
          }
        }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getPaginatedLeagueRounds: { docs: [{ value: 3 }, { value: 2 }] } }
        })
      );
  });
});
