import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";

jest.setTimeout(60000);

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
}, 60000);
describe("api/__gitsha__", () => {
  describe("GET", () => {
    it("should return OK", async () => {
      process.env.GIT_SHA = "gitsha";
      await request(app)
        .get("/api/__gitsha__")
        .expect(200)
        .expect("Content-Type", /text\/html/)
        .expect("gitsha");
    });
  });
});
