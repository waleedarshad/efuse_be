import request from "supertest";
import { Express } from "express";
import { configure } from "../src/backend/app";
import { Types } from "mongoose";
import { registerUser } from "./utils/auth";
import {
  LeagueBracketType,
  LeagueEntryType,
  LeagueEventState,
  LeagueEventTimingMode,
  OwnerType
} from "@efuse/entities";
import { mongoose } from "@typegoose/typegoose";

jest.setTimeout(120000);

describe("League Event API Resolver", () => {
  let app: Express;
  let token;
  let user;
  let org;
  let league;
  let leagueEvent;
  let leagueNoAccess;
  let leagueEventNoAccess;
  let game;

  beforeAll(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    const currentUserId = Types.ObjectId(user.id);

    game = await mongoose.connection.db.collection("games").insertOne({
      description: "Warzone description.",
      gameImage: {
        url: "efuse.gg"
      },
      kind: "game",
      slug: "warzone",
      title: "Call of Duty: Warzone"
    });

    org = await mongoose.connection.db.collection("organizations").insertOne({
      name: "Not Owner Of League",
      user: currentUserId,
      owner: currentUserId,
      shortName: "user-permission",
      slug: "user-permission-slug"
    });

    league = await mongoose.connection.db.collection("leagues").insertOne({
      name: "The Best League",
      imageUrl: "image",
      description: "a test",
      owner: org.insertedId.toString(),
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "no rules"
    });

    leagueEvent = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      league: league.insertedId.toString(),
      maxTeams: 4,
      name: "League Event No Access",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    });

    leagueNoAccess = await mongoose.connection.db.collection("leagues").insertOne({
      name: "You Cant Access",
      imageUrl: "image",
      description: "a test",
      owner: new Types.ObjectId(),
      ownerType: OwnerType.USERS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "no rules"
    });

    leagueEventNoAccess = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      league: leagueNoAccess.insertedId.toString(),
      maxTeams: 4,
      name: "League Event No Access",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    });
  }, 120000);

  it("createLeagueEvent returns 422 if league assigned does not exist", async () => {
    const leagueEventMissingLeague = {
      bracketType: LeagueBracketType.ROUND_ROBIN,
      leagueId: new Types.ObjectId(),
      maxTeams: 4,
      name: "League Event Having Missing League",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 0,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    };

    const postData = {
      operationName: "createLeagueEvent",
      variables: { leagueEvent: leagueEventMissingLeague },
      query: `mutation createLeagueEvent($leagueEvent: CreateLeagueEventArg!) {
        createLeagueEvent(leagueEvent: $leagueEvent) {
          gamesPerMatch
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "League does not exist",
            statusCode: 422
          })
        )
      );
  });

  it("createLeagueEvent returns 400 error if games per match does not exist on league event", async () => {
    const leagueEventMissingLeague = {
      bracketType: LeagueBracketType.ROUND_ROBIN,
      leagueId: league.insertedId.toString(),
      maxTeams: 4,
      name: "League Event has no games per match",
      description: "this is description",
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    };

    const postData = {
      operationName: "createLeagueEvent",
      variables: { leagueEvent: leagueEventMissingLeague },
      query: `mutation createLeagueEvent($leagueEvent: CreateLeagueEventArg!) {
        createLeagueEvent(leagueEvent: $leagueEvent) {
          gamesPerMatch
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(400)
      .expect((res) => expect(res.body.errors[0].message).toContain("League Event has no games per match"));
  });

  it("createLeagueEvent set gamesPerMatch to 1 by default if none or 0 is provided", async () => {
    const leagueEventNoGamesPerMatch = {
      bracketType: LeagueBracketType.ROUND_ROBIN,
      leagueId: league.insertedId.toString(),
      maxTeams: 4,
      name: "League Event To Create",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 0,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    };

    const postData = {
      operationName: "createLeagueEvent",
      variables: { leagueEvent: leagueEventNoGamesPerMatch },
      query: `mutation createLeagueEvent($leagueEvent: CreateLeagueEventArg!) {
        createLeagueEvent(leagueEvent: $leagueEvent) {
          gamesPerMatch
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { createLeagueEvent: { gamesPerMatch: 1 } }
        })
      );
  });

  it("createLeagueEvent creates the league event", async () => {
    const leagueEvent = {
      bracketType: LeagueBracketType.ROUND_ROBIN,
      leagueId: league.insertedId.toString(),
      maxTeams: 4,
      name: "League Event To Create",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    };

    const postData = {
      operationName: "createLeagueEvent",
      variables: { leagueEvent: leagueEvent },
      query: `mutation createLeagueEvent($leagueEvent: CreateLeagueEventArg!) {
        createLeagueEvent(leagueEvent: $leagueEvent) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { createLeagueEvent: { name: leagueEvent.name } }
        })
      );
  });

  it("createLeagueEvent creates the league event as single elimination", async () => {
    const leagueEvent = {
      bracketType: LeagueBracketType.SINGLE_ELIM,
      leagueId: league.insertedId.toString(),
      maxTeams: 4,
      name: "Single Elim Event",
      numberOfRounds: 1,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    };

    const postData = {
      operationName: "createLeagueEvent",
      variables: { leagueEvent: leagueEvent },
      query: `mutation createLeagueEvent($leagueEvent: CreateLeagueEventArg!) {
        createLeagueEvent(leagueEvent: $leagueEvent) {
          bracketType
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { createLeagueEvent: { bracketType: leagueEvent.bracketType } }
        })
      );
  });

  it("updateLeagueEvent returns 422 event does not exist when trying to be updated", async () => {
    const leagueThatDoesNotExist = new Types.ObjectId();

    const postData = {
      operationName: "updateLeagueEvent",
      variables: { leagueEvent: {}, eventId: leagueThatDoesNotExist.toString() },
      query: `mutation updateLeagueEvent($leagueEvent: UpdateLeagueEventArg!, $eventId: String!) {
        updateLeagueEvent(leagueEvent: $leagueEvent, eventId: $eventId) {
          _id
          name
          rules
          description
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "Event does not exist",
            statusCode: 422
          })
        )
      );
  });

  it("updateLeagueEvent returns 403 if user does not have permission to update an event", async () => {
    const leagueEventId = leagueEventNoAccess.insertedId;

    const postData = {
      operationName: "updateLeagueEvent",
      variables: { leagueEvent: {}, eventId: leagueEventId },
      query: `mutation updateLeagueEvent($leagueEvent: UpdateLeagueEventArg!, $eventId: String!) {
        updateLeagueEvent(leagueEvent: $leagueEvent, eventId: $eventId) {
          _id
          name
          rules
          description
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "User does not have permission to perform this action.",
            statusCode: 403
          })
        )
      );
  });

  it("updateLeagueEvent throws 400 if maxteams is less then number of teams", async () => {
    const leagueMaxTeam = await mongoose.connection.db.collection("leagues").insertOne({
      name: "League For Max Teams",
      imageUrl: "image",
      description: "a test",
      owner: org.insertedId.toString(),
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: ["Team 1", "Team 2", "Team 3"],
      rules: "no rules"
    });

    const leagueEventMaxTeam = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      league: leagueMaxTeam.insertedId.toString(),
      maxTeams: 2,
      name: "League Event For Max Teams",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    });

    const updateLeagueEvent = {
      description: "Updated description",
      gamesPerMatch: 1,
      teams: ["Team 1", "Team 2", "Team 3"],
      maxTeams: 2,
      name: "Changed name",
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "Details Updated",
      rules: "You have rules now"
    };

    const postData = {
      operationName: "updateLeagueEvent",
      variables: { leagueEvent: updateLeagueEvent, eventId: leagueEventMaxTeam.insertedId },
      query: `mutation updateLeagueEvent($leagueEvent: UpdateLeagueEventArg!, $eventId: String!) {
        updateLeagueEvent(leagueEvent: $leagueEvent, eventId: $eventId) {
          _id
          name
          rules
          description
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "Number of teams must be less than or equal to max teams set for event",
            statusCode: 400
          })
        )
      );
  });

  it("updateLeagueEvent updates the league event", async () => {
    const leagueEventId = leagueEvent.insertedId;

    const updateLeagueEvent = {
      description: "Updated description",
      gamesPerMatch: 1,
      maxTeams: 4,
      name: "Changed name",
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "Details Updated",
      rules: "You have rules now"
    };

    const postData = {
      operationName: "updateLeagueEvent",
      variables: { leagueEvent: updateLeagueEvent, eventId: leagueEventId },
      query: `mutation updateLeagueEvent($leagueEvent: UpdateLeagueEventArg!, $eventId: String!) {
        updateLeagueEvent(leagueEvent: $leagueEvent, eventId: $eventId) {
          _id
          name
          rules
          description
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            updateLeagueEvent: {
              _id: leagueEventId.toString(),
              description: updateLeagueEvent.description,
              name: updateLeagueEvent.name,
              rules: updateLeagueEvent.rules
            }
          }
        })
      );
  });

  it("generateRoundsAndMatches returns 403 user does not have permission", async () => {
    const leagueEventId = leagueEventNoAccess.insertedId;

    const postData = {
      operationName: "generateRoundsAndMatches",
      variables: { eventId: leagueEventId },
      query: `mutation generateRoundsAndMatches($eventId: String!) {
        generateRoundsAndMatches(eventId: $eventId) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "User does not have permission to perform this action.",
            statusCode: 403
          })
        )
      );
  });

  it("generateRoundsAndMatches returns 422 league event does not exist", async () => {
    const leagueEventId = new Types.ObjectId();

    const postData = {
      operationName: "generateRoundsAndMatches",
      variables: { eventId: leagueEventId },
      query: `mutation generateRoundsAndMatches($eventId: String!) {
        generateRoundsAndMatches(eventId: $eventId) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "Event does not exist",
            statusCode: 422
          })
        )
      );
  });

  it("generateRoundsAndMatches returns a single elim league event after generation of round and match ups", async () => {
    const singleElimLeague = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.SINGLE_ELIM,
      league: league.insertedId.toString(),
      maxTeams: 4,
      name: "Single Elim Event",
      numberOfRounds: 1,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    });

    await mongoose.connection.db.collection("leaguepools").insertOne({
      teams: ["team 1", "team 2", "team 3", "team 4"],
      name: "pool one",
      event: singleElimLeague.insertedId.toString()
    });

    const postData = {
      operationName: "generateRoundsAndMatches",
      variables: { eventId: singleElimLeague.insertedId },
      query: `mutation generateRoundsAndMatches($eventId: String!) {
        generateRoundsAndMatches(eventId: $eventId) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            generateRoundsAndMatches: {
              name: "Single Elim Event"
            }
          }
        })
      );
  });

  it("generateRoundsAndMatches returns a league event after generation of rounds and matches", async () => {
    const leagueEventId = leagueEvent.insertedId;

    await mongoose.connection.db.collection("leaguepools").insertOne({
      teams: ["team 1", "team 2"],
      name: "pool one",
      event: leagueEvent.insertedId.toString()
    });

    const postData = {
      operationName: "generateRoundsAndMatches",
      variables: { eventId: leagueEventId },
      query: `mutation generateRoundsAndMatches($eventId: String!) {
        generateRoundsAndMatches(eventId: $eventId) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            generateRoundsAndMatches: {
              name: "Changed name"
            }
          }
        })
      );
  });

  it("deleteLeagueEvent returns 403 user does not have permissions to delete", async () => {
    const leagueEventId = leagueEventNoAccess.insertedId;

    const postData = {
      operationName: "deleteLeagueEvent",
      variables: { eventId: leagueEventId },
      query: `mutation deleteLeagueEvent($eventId: String!) {
        deleteLeagueEvent(eventId: $eventId)
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "User does not have permission to perform this action.",
            statusCode: 403
          })
        )
      );
  });

  it("deleteLeagueEvent throws error if league is not found", async () => {
    const notFoundLeagueEventId = new Types.ObjectId();

    const postData = {
      operationName: "deleteLeagueEvent",
      variables: { eventId: notFoundLeagueEventId },
      query: `mutation deleteLeagueEvent($eventId: String!) {
        deleteLeagueEvent(eventId: $eventId)
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "League event not found.",
            statusCode: 404
          })
        )
      );
  });

  it("deleteLeagueEvent should delete the league event", async () => {
    const postData = {
      operationName: "deleteLeagueEvent",
      variables: { eventId: leagueEvent.insertedId },
      query: `mutation deleteLeagueEvent($eventId: String!) {
        deleteLeagueEvent(eventId: $eventId)
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { deleteLeagueEvent: true }
        })
      );
  });
});
