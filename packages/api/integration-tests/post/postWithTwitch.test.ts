import request from "supertest";
import { Express } from "express";
import mongoose from "mongoose";
import { registerUser } from "../utils/auth";
import { configure } from "../../src/backend/app";

let app: Express;
let user: { id: string }, token: string, twitchClips: any, testPost: any;

beforeAll(async () => {
  const result = await configure();
  app = result.app;

  const response = await registerUser(app);
  token = response.token;
  user = response.user;

  await mongoose.connection.db.collection("constants").insertOne({
    feature: "TOKEN_ACTION_VALUES",
    variables: {
      newPost: 5
    }
  });

  twitchClips = await mongoose.connection.db.collection("twitchclips").insertMany([
    {
      title: "anything",
      user: mongoose.Types.ObjectId(user.id)
    },
    {
      title: "something",
      user: mongoose.Types.ObjectId(user.id)
    }
  ]);
});

describe("post with Twitch clip", () => {
  it("is successfully created", async () => {
    const response = await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send({
        operationName: "CreatePost",
        query: `
      mutation CreatePost($body: PostInput!) {
        createPost(body: $body) {
          text
          feedId
        }
      }
      `,
        variables: {
          body: {
            text: "What Have They Got In There, King Kong?",
            kindId: user.id,
            kind: "users",
            twitchClip: twitchClips.insertedIds[0]
          }
        }
      })
      .expect(200)
      .expect(({ body }) => {
        expect(body).toEqual(
          expect.objectContaining({
            data: {
              createPost: {
                text: "What Have They Got In There, King Kong?",
                feedId: expect.anything()
              }
            }
          })
        );
      });

    testPost = response.body.data.createPost;
  });

  it("is successfully updated", async () => {
    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send({
        operationName: "UpdatePost",
        query: `
      mutation UpdatePost($body: UpdatePostInput!, $updatePostId: ID) {
        updatePost(body: $body, id: $updatePostId) {
          text
        }
      }
      `,
        variables: {
          body: {
            text: "...Your Scientists Were So Preoccupied With Whether Or Not They Could, They Didn’t Stop To Think If They Should.",
            twitchClip: twitchClips.insertedIds[1]
          },
          updatePostId: testPost.feedId
        }
      })
      .expect(200)
      .expect({
        data: {
          updatePost: {
            text: "...Your Scientists Were So Preoccupied With Whether Or Not They Could, They Didn’t Stop To Think If They Should."
          }
        }
      });
  });
});
