import request from "supertest";
import { Express } from "express";
import { registerUser } from "./utils/auth";
import { configure } from "../src/backend/app";

jest.setTimeout(60000);

let app: Express;

beforeAll(async () => {
  const result = await configure();
  app = result.app;
});

describe("api/users/current_user", () => {
  describe("GET", () => {
    it("should return OK with user data", async () => {
      const { token } = await registerUser(app);

      await request(app)
        .get("/api/users/current_user")
        .set("Authorization", token)
        .expect(200)
        .expect((res) =>
          expect(res.body).toEqual(expect.objectContaining({ user: expect.objectContaining({ username: "IronMan" }) }))
        );
    });
  });
});
