import path from "path";
import { IDockerComposeOptions } from "docker-compose";

const options: IDockerComposeOptions = {
  cwd: path.join(__dirname),
  log: true,
  composeOptions: [["--project-name", "efuse-api-integration-test"]]
};

export default options;
