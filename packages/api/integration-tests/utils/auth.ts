import request from "supertest";
import { Express } from "express";

const defaultUser = {
  email: "tony.stark@avengers.com",
  name: "Tony Stark",
  dateOfBirth: new Date(1965, 3, 4),
  username: "IronMan",
  password: "iamironman1"
};

/**
 * Registers a user to be used for authenticated requests through the API.
 * If the user can't be registered a login is attempted.
 * A default user is registered if none is provided.
 * @param app The Exrpress app to be registered with.
 * @param user Optional user information to register with.
 * @returns The response body from the registration request.
 */
export const registerUser = async (
  app: Express,
  user?: { email: string; name: string; dateOfBirth: Date; username: string; password: string }
) => {
  const userToRegister = user || defaultUser;

  const payload = { platform: "web", user: userToRegister };
  const response = await request(app).post("/api/auth/register").send(payload);

  if (response.statusCode === 200) return response.body;

  const { email, password } = userToRegister;
  return authenticateUser(app, { email, password });
};

/**
 * Authenticates a registered user.
 * A user must be registered prior to authenticating.
 * If no user is provided an attempt to log in the default user is made.
 * @param app
 * @param credentials
 * @returns The response body from the login request.
 */
export const authenticateUser = async (app: Express, credentials?: { email: string; password: string }) => {
  const defaultCredentials = { email: defaultUser.email, password: defaultUser.password };
  const payload = { platform: "web", ...(credentials || defaultCredentials) };
  const response = await request(app).post("/api/auth/login").send(payload);

  if (response.statusCode !== 200)
    throw new Error(`Login failed for user ${payload.email}. Make sure the user is registered.`);

  return response.body;
};
