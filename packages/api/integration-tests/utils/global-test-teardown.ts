import compose from "docker-compose";
import options from "./compose-options";

const down = async () => {
  await compose.down(options);
};

const globalTeardown = async () => {
  // TODO: figure out how to clean up all open handles so jest can exit cleanly.
  // for now opting to force jest to exit.
  // killing the redis server causes the client connections to shut down app.
  // TODO: reenable once there is a solution to clean up client connections.
  // await down();
};

export default globalTeardown;
