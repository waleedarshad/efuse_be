import mongoose from "mongoose";
import cachegoose from "cachegoose";
import Redis from "ioredis";
import nock from "nock";
import { pino as mockPino } from "@efuse/test-helpers";

jest.setTimeout(60000);

jest.mock("pino", () => mockPino);
jest.mock("dd-trace", () => ({
  scope: jest.fn(),
  init: jest.fn().mockReturnValue({
    scope: jest.fn().mockReturnValue({ active: jest.fn() }),
    trace: jest.fn((_name, _options, fn) => fn())
  })
}));

jest.mock("hot-shots", () => ({
  StatsD: jest.fn().mockReturnValue({ childClient: jest.fn(), increment: jest.fn() })
}));

process.env.BULLET_TRAIN_ENVIRONMENT = "jest";
process.env.JWT_KEY = "jest";
process.env.TWITCH_CLIENT_ID = "twitchclientid";
process.env.JWT_MAX_AGE = "7d";

const preventExternalNetworkRequests = () => {
  nock.disableNetConnect();
  nock.enableNetConnect("127.0.0.1");
};

const connectDB = async () => {
  cachegoose(mongoose);
  await mongoose.connect(process.env.MONGO_URI as string, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true
  });
  await mongoose.connection.db.dropDatabase();
};

const disconnectDB = async () => {
  await mongoose.connection.close();
};

const seedRedis = async () => {
  const redisClient = new Redis({
    host: process.env.REDIS_ENDPOINT,
    port: parseInt(process.env.REDIS_PORT!),
    lazyConnect: true
  });
  await redisClient.connect();
  await redisClient.flushall();
  await redisClient.set(
    "feature_flags:flags",
    JSON.stringify([
      {
        feature: {
          name: "allow_login"
        },
        enabled: true
      }
    ])
  );
  redisClient.disconnect();
};

beforeAll(async () => {
  preventExternalNetworkRequests();
  await connectDB();
  await seedRedis();
}, 60000);

afterAll(async () => {
  await disconnectDB();
});
