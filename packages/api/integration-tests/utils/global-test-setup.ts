import compose from "docker-compose";
import options from "./compose-options";

const setup = async () => {
  // TODO: get random open ports
  const mongoPort = 59627;
  const redisPort = 59628;
  process.env.MONGODB_PORT = mongoPort.toString();
  process.env.MONGO_URI = `mongodb://localhost:${mongoPort}/test`;
  process.env.REDIS_PORT = redisPort.toString();
  process.env.REDIS_ENDPOINT = "localhost";
  process.env.REDIS_ENDPOINT_ASYNC = "localhost";
};

export = async () => {
  try {
    await setup();
    await compose.upAll(options);
    await compose.ps(options);
    console.log("Test environment started successfully!");
  } catch (err) {
    console.error({ err });
    process.exit(1);
  }
};
