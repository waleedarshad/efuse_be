import request from "supertest";
import { Express } from "express";
import { configure } from "../../src/backend/app";
import { mongoose } from "@typegoose/typegoose";
import { registerUser } from "../utils/auth";
import {
  LeagueBracketType,
  LeagueEventState,
  LeagueEventTimingMode,
  LeagueMatchState,
  OwnerType
} from "@efuse/entities";
import { Types } from "mongoose";

describe("League Match API Resolver", () => {
  let app: Express;
  let user;
  let org;
  let league;
  let event;
  let round;
  let bracket;
  let pool;
  let token;
  let team;
  let matches;

  jest.setTimeout(60000);

  beforeAll(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    const currentUserId = Types.ObjectId(user.id);

    org = await mongoose.connection.db.collection("organizations").insertOne({
      name: "League Owner",
      user: currentUserId,
      owner: currentUserId,
      shortName: "org",
      slug: "org"
    });

    league = await mongoose.connection.db.collection("leagues").insertOne({
      name: "The Best League",
      imageUrl: "image",
      description: "a test",
      owner: org.insertedId.toString(),
      ownerType: OwnerType.ORGANIZATIONS,
      teams: [],
      rules: "no rules"
    });

    event = await mongoose.connection.db.collection("leagueevents").insertOne({
      bracketType: LeagueBracketType.ROUND_ROBIN,
      league: league.insertedId.toString(),
      maxTeams: 4,
      name: "League Event No Access",
      numberOfRounds: 2,
      description: "this is description",
      gamesPerMatch: 1,
      numberOfPools: 1,
      startDate: new Date(2021, 10, 29),
      state: LeagueEventState.ACTIVE,
      timingMode: LeagueEventTimingMode.WEEKLY,
      details: "details",
      rules: "the rules"
    });

    pool = await mongoose.connection.db.collection("leaguepools").insertOne({
      name: "pool",
      event: event.insertedId.toString()
    });

    bracket = await mongoose.connection.db.collection("leaguebrackets").insertOne({
      name: "bracket",
      pool: pool.insertedId.toString()
    });

    round = await mongoose.connection.db.collection("leaguerounds").insertOne({
      name: "myRound",
      bracket: bracket.insertedId.toString()
    });

    team = await mongoose.connection.db.collection("teams").insertOne({
      name: "myTeam"
    });

    matches = await mongoose.connection.db.collection("leaguematches").insertMany([
      {
        round: round.insertedId,
        teams: [team.insertedId],
        winner: team.insertedId,
        state: LeagueMatchState.PENDING,
        startTime: new Date(2021, 10, 29),
        endTime: new Date(2021, 10, 30)
      },
      {
        round: round.insertedId,
        teams: [team.insertedId],
        winner: team.insertedId,
        state: LeagueMatchState.PENDING,
        startTime: new Date(2021, 10, 31),
        endTime: new Date(2021, 11, 1)
      },
      {
        round: round.insertedId,
        teams: [team.insertedId],
        winner: team.insertedId,
        state: LeagueMatchState.PENDING,
        startTime: new Date(2021, 11, 2),
        endTime: new Date(2021, 11, 3)
      }
    ]);
  }, 600000);

  describe("Mutations", () => {
    describe("when the user is allowed to modify or create matches", () => {
      it("creates a league match", async () => {
        const myRound = await mongoose.connection.db.collection("leaguerounds").insertOne({
          name: "my new round",
          bracket: bracket.insertedId.toString()
        });

        const leagueMatchBody = {
          round: myRound.insertedId.toString(),
          teams: [team.insertedId.toString()],
          winner: team.insertedId.toString(),
          state: LeagueMatchState.PENDING,
          startTime: new Date(2021, 10, 29),
          endTime: new Date(2021, 10, 30)
        };

        const expectedData = { createLeagueMatch: { winner: { name: "myTeam" }, round: { name: "my new round" } } };

        const postData = {
          operationName: "createLeagueMatch",
          variables: { leagueMatchBody },
          query: `mutation createLeagueMatch($leagueMatchBody: LeagueMatchBody!) {
            createLeagueMatch(leagueMatchBody: $leagueMatchBody) {
              round {
                name
              }
              winner {
                name
              }
            }
          }`
        };

        await request(app)
          .post("/api/graphql")
          .set("Authorization", token)
          .send(postData)
          .expect((res) => {
            expect(res.body).toEqual({ data: expectedData });
          })
          .expect(200);
      });

      it("deletes a league match", async () => {
        const matchToDelete = await mongoose.connection.db.collection("leaguematches").insertOne({
          round: round.insertedId,
          teams: [team.insertedId],
          winner: team.insertedId,
          state: LeagueMatchState.PENDING,
          startTime: new Date(2021, 10, 29),
          endTime: new Date(2021, 10, 30)
        });

        const postData = {
          operationName: "deleteLeagueMatch",
          variables: { matchId: matchToDelete.insertedId },
          query: `mutation deleteLeagueMatch($matchId: String!) {
            deleteLeagueMatch(matchId: $matchId) {
              _id
            }
          }`
        };

        await request(app).post("/api/graphql").set("Authorization", token).send(postData).expect(200);
      });

      it("updates a league match", async () => {
        const leagueMatchBody = {
          state: LeagueMatchState.ACTIVE
        };

        const expectedData = { updateLeagueMatch: { state: LeagueMatchState.ACTIVE } };

        const postData = {
          operationName: "updateLeagueMatch",
          variables: { matchId: matches.ops[2]._id, leagueMatchBody },
          query: `mutation updateLeagueMatch($matchId: String!, $leagueMatchBody: LeagueMatchBody!) {
            updateLeagueMatch(matchId: $matchId, leagueMatchBody: $leagueMatchBody) {
              state
            }
          }`
        };

        await request(app)
          .post("/api/graphql")
          .set("Authorization", token)
          .send(postData)
          .expect((res) => {
            expect(res.body).toEqual({ data: expectedData });
          })
          .expect(200);
      });
    });

    describe("when the user is not allowed to change matches in the league", () => {
      let forbiddenToken;

      beforeAll(async () => {
        const forbiddenUser = {
          email: "notAllowed@forbidden.com",
          name: "cantModify",
          dateOfBirth: new Date("1990-01-01"),
          username: "cantModify",
          password: "passW.asdlfkj"
        };

        const registeredUser = await registerUser(app, forbiddenUser);
        forbiddenToken = registeredUser.token;
      });

      it("fails to create a match", async () => {
        const leagueMatchBody = {
          round: round.insertedId.toString(),
          teams: [team.insertedId.toString()],
          winner: team.insertedId.toString(),
          state: LeagueMatchState.PENDING,
          startTime: new Date(2021, 10, 29),
          endTime: new Date(2021, 10, 30)
        };

        const postData = {
          operationName: "createLeagueMatch",
          variables: { leagueMatchBody },
          query: `mutation createLeagueMatch($leagueMatchBody: LeagueMatchBody!) {
            createLeagueMatch(leagueMatchBody: $leagueMatchBody) {
              _id
            }
          }`
        };

        await request(app)
          .post("/api/graphql")
          .set("Authorization", forbiddenToken)
          .send(postData)
          .expect((res) => {
            expect(res.body.errors.length).toBeGreaterThan(0);
          });
      });

      it("fails to update a match", async () => {
        const leagueMatchBody = {
          state: LeagueMatchState.ACTIVE
        };

        const postData = {
          operationName: "updateLeagueMatch",
          variables: { matchId: matches.ops[2]._id, leagueMatchBody },
          query: `mutation updateLeagueMatch($matchId: String!, $leagueMatchBody: LeagueMatchBody!) {
            updateLeagueMatch(matchId: $matchId, leagueMatchBody: $leagueMatchBody) {
              _id
            }
          }`
        };

        await request(app)
          .post("/api/graphql")
          .set("Authorization", forbiddenToken)
          .send(postData)
          .expect((res) => {
            expect(res.body.errors.length).toBeGreaterThan(0);
          });
      });

      it("fails to delete a match", async () => {
        const postData = {
          operationName: "deleteLeagueMatch",
          variables: { matchId: matches.ops[0]._id },
          query: `mutation deleteLeagueMatch($matchId: String!) {
            deleteLeagueMatch(matchId: $matchId) {
              _id
            }
          }`
        };

        await request(app)
          .post("/api/graphql")
          .set("Authorization", forbiddenToken)
          .send(postData)
          .expect((res) => {
            expect(res.body.errors.length).toBeGreaterThan(0);
          });
      });
    });
  });

  describe("Queries", () => {
    it("getMatchById returns correct data", async () => {
      const expectedData = {
        getLeagueMatchById: {
          winner: { name: "myTeam" },
          round: { name: "myRound" },
          teams: [
            {
              name: "myTeam"
            }
          ],
          state: LeagueMatchState.PENDING,
          startTime: new Date(2021, 10, 29).toISOString(),
          endTime: new Date(2021, 10, 30).toISOString()
        }
      };

      const postData = {
        operationName: "getLeagueMatchById",
        variables: { matchId: matches.ops[0]._id },
        query: `query getLeagueMatchById($matchId: String!) {
        getLeagueMatchById(matchId: $matchId) {
          round {
            name
          }
          winner {
            name
          }
          teams {
            name
          }
          startTime
          endTime
          state
        }
      }`
      };

      await request(app)
        .post("/api/graphql")
        .send(postData)
        .expect((res) => {
          expect(res.body).toEqual({ data: expectedData });
        })
        .expect(200);
    });

    it("getPaginatedLeagueMatches returns correct data", async () => {
      const expectedData = {
        getPaginatedLeagueMatches: {
          totalDocs: 3,
          page: 1,
          limit: 2,
          hasNextPage: true,
          docs: [
            {
              winner: { name: "myTeam" },
              round: { name: "myRound" },
              teams: [
                {
                  name: "myTeam"
                }
              ],
              state: LeagueMatchState.PENDING,
              startTime: new Date(2021, 10, 29).toISOString(),
              endTime: new Date(2021, 10, 30).toISOString()
            },
            {
              winner: { name: "myTeam" },
              round: { name: "myRound" },
              teams: [
                {
                  name: "myTeam"
                }
              ],
              state: LeagueMatchState.PENDING,
              startTime: new Date(2021, 10, 31).toISOString(),
              endTime: new Date(2021, 11, 1).toISOString()
            }
          ]
        }
      };

      const postData = {
        operationName: "getPaginatedLeagueMatches",
        variables: { roundId: round.insertedId, page: 1, limit: 2 },
        query: `query getPaginatedLeagueMatches($roundId: String!, $page: Int, $limit: Int) {
        getPaginatedLeagueMatches(roundId: $roundId, page: $page, limit: $limit) {
          totalDocs
          page
          limit
          hasNextPage
          docs {
            round {
              name
            }
            winner {
              name
            }
            teams {
              name
            }
            startTime
            endTime
            state
          }
        }
      }`
      };

      await request(app)
        .post("/api/graphql")
        .send(postData)
        .expect((res) => {
          expect(res.body).toEqual({ data: expectedData });
        })
        .expect(200);
    });
  });
});
