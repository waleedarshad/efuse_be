import request from "supertest";
import { configure } from "../../src/backend/app";
import { Express } from "express";
import { registerUser } from "../utils/auth";
import { Types } from "mongoose";
import { mongoose } from "@typegoose/typegoose";
import { LeagueGameScoreType, LeagueMatchState } from "@efuse/entities";

jest.setTimeout(60000);

describe("League Game Score API Resolver", () => {
  let app: Express;
  let token;
  let user;
  let gameScoreOne;
  let gameScoreTwo;
  let gameScoreThree;
  let match;

  beforeEach(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    match = await mongoose.connection.db.collection("leaguematches").insertOne({
      round: new Types.ObjectId(),
      teams: [],
      winner: new Types.ObjectId(),
      state: LeagueMatchState.PENDING,
      startTime: new Date(2021, 10, 29),
      endTime: new Date(2021, 10, 30)
    });

    gameScoreOne = await mongoose.connection.db.collection("leaguegamescores").insertOne({
      creator: new Types.ObjectId(),
      creatorType: "teams",
      image: "image/string",
      match: match.insertedId,
      type: LeagueGameScoreType.SUBMITTED,
      gameNumber: 1
    });

    gameScoreTwo = await mongoose.connection.db.collection("leaguegamescores").insertOne({
      creator: new Types.ObjectId(),
      creatorType: "teams",
      image: "image/string",
      match: match.insertedId,
      type: LeagueGameScoreType.FINALIZED,
      gameNumber: 1
    });

    gameScoreThree = await mongoose.connection.db.collection("leaguegamescores").insertOne({
      creator: new Types.ObjectId(),
      creatorType: "teams",
      image: "image/string",
      match: match.insertedId,
      type: LeagueGameScoreType.FINALIZED,
      gameNumber: 1
    });
  }, 60000);

  it("getLeagueGameScoreById should return a 422 if league game score is not found", async () => {
    const gameScoreId = new Types.ObjectId();

    const postData = {
      operationName: "getLeagueGameScoreById",
      variables: { gameScoreId: gameScoreId },
      query: `query getLeagueGameScoreById($gameScoreId: String!) {
        getLeagueGameScoreById(gameScoreId: $gameScoreId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "League game score is not found",
            statusCode: 422
          })
        )
      );
  });

  it("getLeagueGameScoreById should return a league game score", async () => {
    const postData = {
      operationName: "getLeagueGameScoreById",
      variables: { gameScoreId: gameScoreOne.insertedId },
      query: `query getLeagueGameScoreById($gameScoreId: String!) {
        getLeagueGameScoreById(gameScoreId: $gameScoreId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueGameScoreById: { _id: gameScoreOne.insertedId.toString() }
          }
        })
      );
  });

  it("getLeagueGameScores should return array of league game scores", async () => {
    const postData = {
      operationName: "getLeagueGameScores",
      variables: { matchId: match.insertedId },
      query: `query getLeagueGameScores($matchId: String!) {
        getLeagueGameScores(matchId: $matchId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueGameScores: [
              { _id: gameScoreOne.insertedId.toString() },
              { _id: gameScoreTwo.insertedId.toString() },
              { _id: gameScoreThree.insertedId.toString() }
            ]
          }
        })
      );
  });

  it("getLeagueGameScores should return an array of only game scores found by filter type", async () => {
    const postData = {
      operationName: "getLeagueGameScores",
      variables: { matchId: match.insertedId, type: LeagueGameScoreType.FINALIZED },
      query: `query getLeagueGameScores($matchId: String!, $type: LeagueGameScoreType!) {
        getLeagueGameScores(matchId: $matchId, type: $type) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueGameScores: [
              { _id: gameScoreTwo.insertedId.toString() },
              { _id: gameScoreThree.insertedId.toString() }
            ]
          }
        })
      );
  });

  it("getPaginatedLeagueGameScores returns game scores found by its id when page and limit are set", async () => {
    const paginatedArgs = {
      matchId: match.insertedId,
      type: LeagueGameScoreType.FINALIZED,
      page: 1,
      limit: 1,
      sortDirection: "asc"
    };

    const postData = {
      operationName: "getPaginatedLeagueGameScores",
      variables: paginatedArgs,
      query: `query getPaginatedLeagueGameScores($matchId: String!, $type: LeagueGameScoreType!, $page: Int!, $limit: Int!, $sortDirection: SortDirection!) {
        getPaginatedLeagueGameScores(matchId: $matchId, type: $type, page: $page, limit: $limit, sortDirection: $sortDirection) {
          docs {
            _id
          }
          page
          limit
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getPaginatedLeagueGameScores: {
              docs: [
                {
                  _id: gameScoreTwo.insertedId.toString()
                }
              ],
              page: paginatedArgs.page,
              limit: paginatedArgs.limit
            }
          }
        })
      );
  });
});
