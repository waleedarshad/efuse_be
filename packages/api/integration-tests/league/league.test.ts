import request from "supertest";
import { Express } from "express";
import { configure } from "../../src/backend/app";
import { mongoose } from "@typegoose/typegoose";
import { registerUser } from "../utils/auth";
import { LeagueEntryType, OwnerType } from "@efuse/entities";
import { Types } from "mongoose";
import { LeaguePermissionsEnum, PermissionActionsEnum } from "../../src/lib/permissions/enums";

describe("League API Resolver", () => {
  let app: Express;
  let token;
  let user;
  let org;
  let league;
  let leagueNoAccess;
  let game;

  jest.setTimeout(60000);

  beforeAll(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    const currentUserId = Types.ObjectId(user.id);

    game = await mongoose.connection.db.collection("games").insertOne({
      description: "Warzone description.",
      gameImage: {
        url: "efuse.gg"
      },
      kind: "game",
      slug: "warzone",
      title: "Call of Duty: Warzone"
    });

    org = await mongoose.connection.db.collection("organizations").insertOne({
      name: "Not Owner Of League",
      user: currentUserId,
      owner: currentUserId,
      shortName: "user-permission",
      slug: "user-permission-slug"
    });

    league = await mongoose.connection.db.collection("leagues").insertOne({
      name: "The Best League",
      imageUrl: "image",
      description: "a test",
      owner: org.insertedId.toString(),
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "no rules"
    });

    leagueNoAccess = await mongoose.connection.db.collection("leagues").insertOne({
      name: "League I Cant Access",
      imageUrl: "image",
      description: "a test",
      owner: new Types.ObjectId(),
      ownerType: OwnerType.USERS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "no rules"
    });
  }, 600000);

  it("createLeague returns 422 if league has organization assigned that does not exist", async () => {
    //required because createLeague takes in a league of CreateLeagueArgs
    const passedLeague = {
      name: "Calebs League",
      imageUrl: "image",
      description: "description",
      owner: new Types.ObjectId(),
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: "123445566",
      teams: [],
      rules: "no rules"
    };

    const postData = {
      operationName: "createLeague",
      variables: { league: passedLeague },
      query: `mutation createLeague($league: CreateLeagueArgs!) {
        createLeague(league: $league) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception.data).toEqual(
          expect.objectContaining({
            message: "Unable to find requested organization",
            statusCode: 422
          })
        )
      );
  });

  it("createLeague returns 403 if user does not have permission to create league", async () => {
    //required because createLeague takes in a league of CreateLeagueArgs
    const passedLeague = {
      name: "Calebs League",
      imageUrl: "image",
      description: "description",
      owner: org.insertedId.toString(),
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "no rules"
    };

    const postData = {
      operationName: "createLeague",
      variables: { league: passedLeague },
      query: `mutation createLeague($league: CreateLeagueArgs!) {
        createLeague(league: $league) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception.data).toEqual(
          expect.objectContaining({
            message: "User does not have permission to perform this action.",
            statusCode: 403
          })
        )
      );
  });

  it("createLeague creates a league", async () => {
    //required because createLeague takes in a league of CreateLeagueArgs
    const passedLeague = {
      name: "Calebs League",
      imageUrl: "image",
      description: "description",
      owner: org.insertedId.toString(),
      ownerType: OwnerType.ORGANIZATIONS,
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "no rules"
    };

    await mongoose.connection.db.collection("efuse_permission").insertOne({
      subject: org.insertedId.toString(),
      object: LeaguePermissionsEnum.CREATE_LEAGUE,
      action: PermissionActionsEnum.ACCESS
    });

    const postData = {
      operationName: "createLeague",
      variables: { league: passedLeague },
      query: `mutation createLeague($league: CreateLeagueArgs!) {
        createLeague(league: $league) {
          name
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { createLeague: { name: passedLeague.name } }
        })
      );
  });

  it("getLeagueById returns the league found by its id", async () => {
    const leagueId = league.insertedId;

    const postData = {
      operationName: "getLeagueById",
      variables: { leagueId: leagueId },
      query: `query getLeagueById($leagueId: String!) {
        getLeagueById(leagueId: $leagueId) {
          _id
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getLeagueById: { _id: leagueId.toString() } }
        })
      );
  });

  it("updateLeague returns 403 if user does not have permission to update a league", async () => {
    const leagueId = leagueNoAccess.insertedId;

    const postData = {
      operationName: "updateLeague",
      variables: { leagueId: leagueId, updates: {} },
      query: `mutation updateLeague($leagueId: String!, $updates: UpdateLeagueArgs!) {
        updateLeague(leagueId: $leagueId, updates: $updates) {
          _id
          name
          description
          rules
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception.data).toEqual(
          expect.objectContaining({
            message: "User does not have permission to perform this action.",
            statusCode: 403
          })
        )
      );
  });

  it("updateLeague updates the league and returns the updated league", async () => {
    const updatedLeague = {
      name: "You Are An Updated League",
      imageUrl: "updated/image/url",
      description: "updated description",
      entryType: LeagueEntryType.OPEN,
      game: game.insertedId.toString(),
      teams: [],
      rules: "you have rules now"
    };

    const leagueId = league.insertedId;

    const postData = {
      operationName: "updateLeague",
      variables: { leagueId: leagueId, updates: updatedLeague },
      query: `mutation updateLeague($leagueId: String!, $updates: UpdateLeagueArgs!) {
        updateLeague(leagueId: $leagueId, updates: $updates) {
          _id
          name
          description
          rules
        }
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            updateLeague: {
              _id: league.insertedId.toString(),
              name: updatedLeague.name,
              description: updatedLeague.description,
              rules: updatedLeague.rules
            }
          }
        })
      );
  });

  it("deleteLeague returns 403 if user does not have permission to delete a league", async () => {
    const leagueId = leagueNoAccess.insertedId;

    const postData = {
      operationName: "deleteLeague",
      variables: { leagueId: leagueId },
      query: `mutation deleteLeague($leagueId: String!) {
        deleteLeague(leagueId: $leagueId)
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception.data).toEqual(
          expect.objectContaining({
            message: "User does not have permission to perform this action.",
            statusCode: 403
          })
        )
      );
  });

  it("deleteLeague throws error if league is not found", async () => {
    const leagueId = new Types.ObjectId();

    const postData = {
      operationName: "deleteLeague",
      variables: { leagueId: leagueId },
      query: `mutation deleteLeague($leagueId: String!) {
        deleteLeague(leagueId: $leagueId)
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception.data).toEqual(
          expect.objectContaining({
            message: "League not found.",
            statusCode: 404
          })
        )
      );
  });

  it("deleteLeague deletes the league and returns true", async () => {
    const postData = {
      operationName: "deleteLeague",
      variables: { leagueId: league.insertedId },
      query: `mutation deleteLeague($leagueId: String!) {
        deleteLeague(leagueId: $leagueId)
      }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { deleteLeague: true }
        })
      );
  });
});
