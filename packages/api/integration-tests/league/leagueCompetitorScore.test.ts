import request from "supertest";
import { Express } from "express";
import { Types } from "mongoose";
import { configure } from "../../src/backend/app";
import { registerUser } from "../utils/auth";
import { mongoose } from "@typegoose/typegoose";

jest.setTimeout(60000);
describe("League Competitor Score API Resolver", () => {
  let app: Express;
  let token;
  let user;
  let compScore;
  let compScoreTwo;
  let gameScore;

  beforeAll(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    gameScore = await mongoose.connection.db.collection("leaguegamescores").insertOne({
      creator: new Types.ObjectId(),
      creatorType: "teams",
      match: new Types.ObjectId(),
      gameNumber: 1,
      type: "submitted"
    });

    compScore = await mongoose.connection.db.collection("leaguecompetitorscores").insertOne({
      value: 2,
      competitor: new Types.ObjectId(),
      competitorType: "team",
      creator: new Types.ObjectId(),
      creatorType: "teams",
      gameScore: gameScore.insertedId
    });

    compScoreTwo = await mongoose.connection.db.collection("leaguecompetitorscores").insertOne({
      value: 1,
      competitor: new Types.ObjectId(),
      competitorType: "team",
      creator: new Types.ObjectId(),
      creatorType: "teams",
      gameScore: gameScore.insertedId
    });
  }, 60000);

  it("getLeagueCompetitorScoreById will return a 422 score is not found", async () => {
    const scoreId = new Types.ObjectId();

    const postData = {
      operationName: "getLeagueCompetitorScoreById",
      variables: { scoreId: scoreId },
      query: `query getLeagueCompetitorScoreById($scoreId: String!) {
        getLeagueCompetitorScoreById(scoreId: $scoreId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "Score does not exist",
            statusCode: 422
          })
        )
      );
  });

  it("getLeagueCompetitorScoreById will return a score that is found", async () => {
    const scoreId = compScore.insertedId;

    const postData = {
      operationName: "getLeagueCompetitorScoreById",
      variables: { scoreId: scoreId },
      query: `query getLeagueCompetitorScoreById($scoreId: String!) {
        getLeagueCompetitorScoreById(scoreId: $scoreId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: { getLeagueCompetitorScoreById: { _id: compScore.insertedId.toString() } }
        })
      );
  });

  it("getLeagueCompetitorScores will return an empty array if no scores are found", async () => {
    const gameScoreId = new Types.ObjectId();

    const postData = {
      operationName: "getLeagueCompetitorScores",
      variables: { gameScoreId: gameScoreId },
      query: `query getLeagueCompetitorScores($gameScoreId: String!) {
        getLeagueCompetitorScores(gameScoreId: $gameScoreId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueCompetitorScores: []
          }
        })
      );
  });

  it("getLeagueCompetitorScores will return an array of scores that are found", async () => {
    const gameScoreId = gameScore.insertedId;

    const postData = {
      operationName: "getLeagueCompetitorScores",
      variables: { gameScoreId: gameScoreId },
      query: `query getLeagueCompetitorScores($gameScoreId: String!) {
        getLeagueCompetitorScores(gameScoreId: $gameScoreId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueCompetitorScores: [
              { _id: compScore.insertedId.toString() },
              { _id: compScoreTwo.insertedId.toString() }
            ]
          }
        })
      );
  });
});
