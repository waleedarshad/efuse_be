import request from "supertest";
import { configure } from "../../src/backend/app";
import { Express } from "express";
import { registerUser } from "../utils/auth";
import { Types } from "mongoose";
import { mongoose } from "@typegoose/typegoose";

jest.setTimeout(60000);

describe("League Bracket API Resolver", () => {
  let app: Express;
  let token;
  let user;
  let bracket;

  beforeEach(async () => {
    const result = await configure();
    app = result.app;
    const registeredUser = await registerUser(app);

    token = registeredUser.token;
    user = registeredUser.user;

    bracket = await mongoose.connection.db.collection("leaguebrackets").insertOne({
      name: "bracket",
      pool: new Types.ObjectId()
    });
  }, 60000);

  it("getLeagueBracketById should return a 422 if league bracket is not found", async () => {
    const bracketId = new Types.ObjectId();

    const postData = {
      operationName: "getLeagueBracketById",
      variables: { bracketId: bracketId },
      query: `query getLeagueBracketById($bracketId: String!) {
        getLeagueBracketById(bracketId: $bracketId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body.errors[0].extensions.exception).toEqual(
          expect.objectContaining({
            message: "League bracket is not found",
            statusCode: 422
          })
        )
      );
  });

  it("getLeagueBracketById returns a league bracket that is found", async () => {
    const bracketId = bracket.insertedId;

    const postData = {
      operationName: "getLeagueBracketById",
      variables: { bracketId: bracketId },
      query: `query getLeagueBracketById($bracketId: String!) {
        getLeagueBracketById(bracketId: $bracketId) {
              _id
            }
          }`
    };

    await request(app)
      .post("/api/graphql")
      .set("Authorization", token)
      .send(postData)
      .expect(200)
      .expect((res) =>
        expect(res.body).toEqual({
          data: {
            getLeagueBracketById: { _id: bracket.insertedId.toString() }
          }
        })
      );
  });
});
