/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  name: "@efuse/api/integration",
  displayName: "@efuse/api/integration",
  globalSetup: "./utils/global-test-setup.ts",
  globalTeardown: "./utils/global-test-teardown.ts",
  setupFilesAfterEnv: ["./utils/setup-tests.ts"]
};
