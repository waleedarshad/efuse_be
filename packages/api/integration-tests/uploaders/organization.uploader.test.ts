import request from "supertest";
import { Express } from "express";
import { registerUser } from "../utils/auth";
import { configure } from "../../src/backend/app";
import { mongoose } from "@typegoose/typegoose";

let app: Express;

const uploadMock = jest.fn().mockReturnValue({
  on: jest.fn().mockReturnValue("uploading"),
  send: jest.fn().mockImplementation((fn) =>
    fn({
      bucket: "testbucket",
      location: "testlocation",
      etag: "testEtag"
    })
  )
});

jest.mock("aws-sdk", () => {
  const instance = {
    upload: () => uploadMock(),
    promise: jest.fn()
  };
  return {
    S3: jest.fn(() => instance),
    Comprehend: jest.fn(),
    config: {
      update: jest.fn()
    }
  };
});

jest.setTimeout(60000);

describe("organization.uploader", () => {
  let access;
  beforeEach(async () => {
    const result = await configure();
    app = result.app;
    access = await registerUser(app);
  });

  describe("createOrganization", () => {
    const organizationData = {
      associatedGames: [
        "6144cf23d16cec0010910405",
        "6086a8d141f9ebf64ea0641b",
        "602eed49c605d61bdbce08cd",
        "6037cdf5e40f18d63fa4ba9e",
        "6086aa2541f9ebf64ea0641d"
      ],
      name: "gamezone",
      organizationType: "Community",
      shortName: "gamezone",
      status: "Open"
    };

    const postData = {
      operationName: "CreateOrganizationMutation",
      variables: { organization: organizationData },
      query: `mutation CreateOrganizationMutation($organization: CreateOrganizationArgs!) {
        createOrganization(organization: $organization) {
          _id
          name
          shortName
        }
      }`
    };

    it("should return OK with user data & should not call upload middleware atleast once", async () => {
      await request(app)
        .post("/api/graphql")
        .set("Authorization", access.token)
        .send(postData)
        .expect(200)
        .expect(() => expect(uploadMock).not.toHaveBeenCalled());
    });
  });

  describe("updateOrganization", () => {
    let organizationId;
    let updateOrganizationData;

    beforeAll(async () => {
      const organizationData = {
        associatedGames: [
          "6144cf23d16cec0010910405",
          "6086a8d141f9ebf64ea0641b",
          "602eed49c605d61bdbce08cd",
          "6037cdf5e40f18d63fa4ba9e",
          "6086aa2541f9ebf64ea0641d"
        ],
        name: "tonygamezone",
        organizationType: "Community",
        shortName: "tgamezone",
        status: "Open",
        user: access.user.id,
        slug: "tgame-zone"
      };

      const createdOrganization = await mongoose.connection.db.collection("organizations").insertOne(organizationData);

      organizationId = createdOrganization.insertedId;

      const organization = await request(app)
        .get(`/api/organizations/edit/${organizationId}`)
        .set("Authorization", access.token)
        .expect(200);

      updateOrganizationData = organization.body.organization;
    });

    beforeEach(() => {
      jest.restoreAllMocks();
    });

    it("should should call upload middleware atleast once when file is attached", async () => {
      await request(app)
        .put(`/api/organizations/update/${organizationId}`)
        .set("Authorization", access.token)
        .field("name", "updated-gamezone")
        .field("scale", "1-99")
        .field("organizationType", updateOrganizationData.organizationType)
        .field("status", updateOrganizationData.status)
        .field("description", "This is some description")
        .attach("profileImage", "packages/api/integration-tests/uploaders/no_image.jpeg")
        .attach("headerImage", "packages/api/integration-tests/uploaders/valorant-card.jpeg")
        .expect(() => {
          expect(uploadMock).toHaveBeenCalled();
          expect(uploadMock).toHaveBeenCalledTimes(1);
        });
    });

    it("should call upload middleware when file is not attached", async () => {
      await request(app)
        .put(`/api/organizations/update/${organizationId}`)
        .set("Authorization", access.token)
        .field("name", "updated-gamezone")
        .field("scale", "1-99")
        .field("organizationType", updateOrganizationData.organizationType)
        .field("status", updateOrganizationData.status)
        .field("description", "This is some description")
        .expect(() => expect(uploadMock).not.toHaveBeenCalled);
    });
  });
});
