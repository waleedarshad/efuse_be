/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  name: "@efuse/api/unit",
  displayName: "@efuse/api/unit",
  roots: ["<rootDir>/src"],
  setupFilesAfterEnv: ["./unit-tests/setup-tests.ts"]
};
