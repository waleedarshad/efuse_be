import { pino as mockPino } from "@efuse/test-helpers";

jest.mock("../src/async/config.js");
jest.mock("pino", () => mockPino);
jest.mock("dd-trace", () => ({
  scope: jest.fn(),
  init: jest.fn().mockReturnValue({
    scope: jest.fn().mockReturnValue({ active: jest.fn() }),
    trace: jest.fn((_name, _options, fn) => fn())
  })
}));

jest.mock("../src/lib/decorators/trace", () => ({
  Trace: () => {
    return (_target, _propertyKey, descriptor) => {
      return descriptor;
    };
  }
}));
