import { ILogger, Logger } from "@efuse/logger";

export const logger: ILogger = Logger.create({ name: "@efuse/decorators" });
export const noop = function fn(): void {};
