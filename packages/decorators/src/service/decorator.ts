import { ILogger, Logger } from "@efuse/logger";
import { StatsD } from "hot-shots";

import { ServiceConfig } from "./config";
import { EFuseAnalyticsUtil } from "../utils/analytics.utils";

const KIND = "controller";

export function Service(options?: ServiceConfig): any;
export function Service(name?: string, options?: ServiceConfig): any;
export function Service(nameOrConfig?: string | ServiceConfig): any {
  const options = (typeof nameOrConfig === "object" ? nameOrConfig : { name: KIND }) || {};
  const name = typeof nameOrConfig === "string" ? nameOrConfig : options.name;

  return <T extends { new (...args: any[]): {} }>(constructor: T): T | void => {
    return class extends constructor {
      protected $logger = Logger.create({ name });
      protected $name = `be-${name}`;
      protected $stats = EFuseAnalyticsUtil.Stats.childClient({ globalTags: { service: name, kind: KIND } });
      public get logger(): ILogger {
        return this.$logger;
      }

      public get name(): string {
        return this.$name;
      }

      public get stats(): StatsD {
        return this.$stats;
      }
    };
  };
}
