import { Nullable } from "@efuse/types";
import { Span } from "dd-trace";
import * as Formats from "dd-trace/ext/formats";

import { tracer } from "./tracer";
import { PrivateDatadogContext } from "./types";

export class TraceUtils {
  /**
   * If your logger cannot be automatically injected, you must manually inject
   * the log message. This function will mutate the metadata parameter to include
   * IDs tieing it back to the APM span.
   *
   * @see https://docs.datadoghq.com/tracing/advanced/connect_logs_and_traces/?tab=nodejs
   *
   * @param metadata  The log metadata to augment
   * @param span      An optional span object to add the tags to.
   *                  If none provided, the current span will be used.
   */
  public static addLogMetadata = (metadata: object, span?: Span): void => {
    const currentSpan = span || TraceUtils.getCurrentSpan();

    if (!currentSpan) {
      return;
    }

    tracer.inject(currentSpan.context(), Formats.LOG, metadata);
  };

  /**
   * Add tags to a span to have more context about how and why it was running.
   * If added to the root span, tags are searchable and filterable.
   *
   * @param tags  An object with the tags to add to the span
   * @param span  An optional span object to add the tags to.
   *              If none provided, the current span will be used.
   */
  public static addTags = (tags: object, span?: Nullable<Span>): void => {
    const currentSpan = span || TraceUtils.getCurrentSpan();

    if (!currentSpan) {
      return;
    }

    currentSpan.addTags(tags);
  };

  public static getCurrentSpan = (): Nullable<Span> => tracer.scope().active();

  /**
   * The root span is an undocumented internal property that DataDog adds to
   * `context.req`. The root span is required in order to add searchable tags.
   *
   * Unfortunately, there is no API to access the root span directly.
   * See: node_modules/dd-trace/src/plugins/util/web.js
   *
   * @param context A Koa context object
   */
  public static getRootSpanFromRequestContext = (context: PrivateDatadogContext): Nullable<Span> => {
    return context?.req?._datadog?.span ?? null;
  };

  /**
   * Datadog will mark spans as errors if they throw an error.
   * This function will allow other spans to show up as errors as well.
   *
   * @param error   An Error object.
   * @param span    An optional span object to add the tags to.
   *                If none provided, the current span will be used.
   */
  public static markAsError = (error: Error, span?: Span): void => {
    TraceUtils.addTags(
      {
        errorMessage: error.message,
        "error.type": error.name,
        "error.msg": error.message,
        "error.stack": error.stack
      },
      span
    );
  };
}
