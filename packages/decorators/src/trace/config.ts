import { Tags } from "./types";

export interface TraceConfig {
  className?: string;
  methodName?: string;
  name?: string;
  resourceName?: string;
  root?: boolean;
  searchable?: boolean; // cause the span to show up in trace search and analytics
  serviceName?: string;
  spanName?: string;
  tags?: Tags;
}
