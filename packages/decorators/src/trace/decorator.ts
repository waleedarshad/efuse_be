import { TraceConfig } from "./config";
import { ETraceHelper } from "./helper";
import { Constructor } from "./types";

/**
 * This decorator will cause the methods of a class, or an individual method,
 * to be traced by the APM.
 *
 * Going to rely on inferrence do its thing for this function.
 *
 * @param config  Optional configuration for the span that will be created for
 *                this trace.
 */

function Trace(config?: TraceConfig): any {
  function traceDecorator(target: Constructor): void;
  function traceDecorator<T>(
    target: Record<string, unknown>,
    propertyKey: string | symbol,
    descriptor: TypedPropertyDescriptor<T>
  ): void;

  function traceDecorator(a: Constructor | Record<string, any>, b?: any, c?: any): void {
    if (typeof a === "function") {
      // need to cast as there is no safe runtime way to check if a function is a constructor
      ETraceHelper.traceClass(config)(<Constructor>a);
    } else {
      ETraceHelper.traceMethod(config)(a, b, c);
    }
  }

  return traceDecorator;
}

export { Trace };
