import { SpanOptions } from "dd-trace";
import * as DDTags from "dd-trace/ext/tags";

import { TraceConfig } from "./config";
import { TraceUtils } from "./trace.utils";
import { tracer, tracerOptions } from "./tracer";
import { Constructor } from "./types";

export class ETraceHelper {
  public static makeServiceName = (serviceName: string): string => `${tracerOptions.service}-${serviceName}`;

  public static traceClass = (config?: TraceConfig) => {
    return function <T extends Constructor>(constructor: T): void {
      const keys = Reflect.ownKeys(constructor.prototype);

      keys.forEach((key) => {
        if (key === "constructor") {
          return;
        }

        const descriptor = Object.getOwnPropertyDescriptor(constructor.prototype, key);

        if (typeof key === "string" && typeof descriptor?.value === "function") {
          Object.defineProperty(
            constructor.prototype,
            key,
            ETraceHelper.traceMethod(config)(constructor, key, descriptor)
          );
        }
      });
    };
  };

  public static traceFunction =
    (config: TraceConfig) =>
    <F extends (...args: any[]) => any, P extends Parameters<F>, R extends ReturnType<F>>(target: F): F => {
      return tracer.isMock
        ? target
        : (function wrapperFn(this: any, ...args: P): R {
            const {
              className,
              methodName = target.name,
              spanName = "DEFAULT_SPAN_NAME",
              searchable: useAnalytics,
              tags
            } = config;

            const serviceName = config.serviceName
              ? this.makeServiceName(config.serviceName)
              : this.makeServiceName(spanName);

            const childOf = config.root ? undefined : tracer.scope().active() || undefined;
            const resource = config.resourceName ? config.resourceName : className;

            const resourceName = resource ? `${className}.${methodName}` : methodName;

            const spanOptions: SpanOptions = {
              childOf,
              tags: {
                [DDTags.SERVICE_NAME]: serviceName,
                [DDTags.RESOURCE_NAME]: resourceName,
                ...tags
              }
            };

            const span = tracer.startSpan(spanName, spanOptions);

            if (!span) {
              return target.call(this, ...args);
            }

            if (useAnalytics) {
              span.setTag(DDTags.ANALYTICS, true);
            }

            /**
             * The callback fn needs to be wrapped in an arrow fn as the
             * activate fn clobbers `this`.
             */

            return tracer.scope().activate(span, () => {
              const output = target.call(this, ...args);

              if (output && typeof output.then === "function") {
                output
                  .catch((error: Error) => {
                    TraceUtils.markAsError(error, span);
                  })
                  .finally(() => {
                    span.finish();
                  });
              } else {
                span.finish();
              }

              return output;
            });
          } as F);
    };

  public static traceMethod = (config?: TraceConfig) => {
    return function <R extends any, A extends any[], F extends (...args: A) => R>(
      target: any,
      _propertyKey: string,
      descriptor: PropertyDescriptor
    ): TypedPropertyDescriptor<F> {
      const wrappedFn = descriptor.value;

      if (wrappedFn) {
        const className = target.name || target.constructor.name; // target.name is needed if the target is the constructor itself
        const methodName = wrappedFn.name;

        descriptor.value = ETraceHelper.traceFunction({ ...config, className, methodName })(wrappedFn);
      }

      return descriptor;
    };
  };
}
