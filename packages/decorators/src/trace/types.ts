import { Span, Tracer } from "dd-trace";
import * as DDTags from "dd-trace/ext/tags";

export interface Constructor {
  new (...args: any[]): any;
}

export type Tag = typeof DDTags[keyof typeof DDTags];
export type Tags = { [tag in Tag]?: any } & { [key: string]: any };
export type MockTracer = Tracer & { isMock?: boolean };
export type PrivateDatadogContext = { req: object & { _datadog?: { span?: Span } } };
