import { Tracer } from "dd-trace";

import { logger, noop } from "../constants";
import { MockTracer } from "./types";

const callableHandlers = {
  apply<T extends (...args: any) => any, A extends Parameters<T>>(_target: T, _thisArg: any, _args: A): ReturnType<T> {
    const newMock = new Proxy(noop, callableHandlers);

    return newMock as unknown as ReturnType<T>;
  },

  get<T, P extends keyof T>(_target: T, _prop: P, _receiver: any): T[P] {
    const newMock = new Proxy(noop, callableHandlers);
    return newMock as unknown as T[P];
  }
};

const callableMock = new Proxy(noop, callableHandlers);

export const mockTracer = new Proxy({} as MockTracer, {
  get<K extends keyof MockTracer>(_target: Tracer, key: K) {
    logger.debug(
      `Attempted to access the DataDog tracer before the init function was called. Attempted to access property "${key}".`
    );

    if (key === "isMock") {
      return true;
    }

    if (key === "wrap") {
      return (_: any, f: any) => f;
    }

    return callableMock;
  }
});
