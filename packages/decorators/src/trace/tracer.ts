import { Tracer, TracerOptions } from "dd-trace";

import { logger } from "../constants";
import { mockTracer } from "./mock-tracer";

export let tracerOptions: TracerOptions = {};
export let tracer: Tracer & { isMock?: boolean } = mockTracer;

/**
 * This is a wrapper around the datadog init function. This function should be
 * imported and called before any other code, otherwise the APM may not be
 * able to trace it properly or at all.
 *
 * @param options The `TracerOptions` to be passed the tracer init function
 */
export const init = (options: TracerOptions): void => {
  tracerOptions = options;

  tracer = require("dd-trace");
  tracer.init(tracerOptions);
  tracer.isMock = false;

  if (tracerOptions.enabled) {
    logger.info("DataDog APM Trace Running, with options: ", tracerOptions);
  } else {
    logger.info("DataDog APM Trace Disabled");
  }
};
