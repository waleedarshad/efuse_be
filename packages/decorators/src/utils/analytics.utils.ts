import { NODE_ENV } from "@efuse/key-store";
import { StatsD } from "hot-shots";
import tracer, { Tracer } from "dd-trace";

export class EFuseAnalyticsUtil {
  private static efStats: StatsD;
  private static efTracer: Tracer;

  public static get Stats(): StatsD {
    if (!EFuseAnalyticsUtil.efStats) {
      EFuseAnalyticsUtil.efStats = new StatsD({
        prefix: "efuse.",
        globalTags: { env: NODE_ENV }
      });
    }

    return this.efStats;
  }

  public static get Tracer(): Tracer {
    if (process.env.JEST_WORKER_ID === undefined && !EFuseAnalyticsUtil.efTracer) {
      EFuseAnalyticsUtil.efTracer = tracer.init({
        analytics: true,
        logInjection: true,
        env: process.env.NODE_ENV,
        service: "be",
        runtimeMetrics: true
      });
    }

    return this.efTracer;
  }

  public static incrementGauge = (eventName: string, value: number, tags = []): void => {
    if (EFuseAnalyticsUtil.Stats) {
      EFuseAnalyticsUtil.Stats.gauge(eventName, value, tags);
    }
  };

  public static incrementMetric = (eventName: string, value = 1, tags = []): void => {
    if (EFuseAnalyticsUtil.Stats) {
      EFuseAnalyticsUtil.Stats.increment(eventName, value, tags);
    }
  };
}
