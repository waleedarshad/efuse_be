import { ILogger, Logger } from "@efuse/logger";
import { StatsD } from "hot-shots";

import { ControllerConfig } from "./config";
import { EFuseAnalyticsUtil } from "../utils/analytics.utils";

const KIND = "controller";

export function Controller(config?: ControllerConfig): any;
export function Controller(name?: string, config?: ControllerConfig): any;
export function Controller(nameOrConfig?: string | ControllerConfig): any {
  const options = (typeof nameOrConfig === "object" ? nameOrConfig : { name: KIND }) || {};
  const name = typeof nameOrConfig === "string" ? nameOrConfig : options.name;

  return <T extends { new (...args: any[]): {} }>(constructor: T): T | void => {
    return class extends constructor {
      protected $logger: ILogger = Logger.create({ name });
      protected $name = `be-${name}`;
      protected $stats: StatsD = EFuseAnalyticsUtil.Stats.childClient({ globalTags: { service: name, kind: KIND } });

      public get logger(): ILogger {
        return this.$logger;
      }

      public get name(): string {
        return this.$name;
      }

      public get stats(): StatsD {
        return this.$stats;
      }
    };
  };
}
