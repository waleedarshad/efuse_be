#!/usr/bin/env node

const { build } = require("esbuild");
const { esbuildDecorators } = require("@anatine/esbuild-decorators");
const { nodeExternalsPlugin } = require("esbuild-node-externals");
const globby = require("globby");

const ROOT = "packages/decorators";

const ENTRY_POINTS = globby.sync([
  `${ROOT}/src/**/*`,
  `!${ROOT}/src/**/*.spec.ts`,
  `!${ROOT}/**/__mocks__/**`,
  `!${ROOT}/**/__tests__/**`
]);

build({
  bundle: false,
  color: true,
  entryPoints: [`${ROOT}/index.ts`, ...ENTRY_POINTS],
  external: [],
  format: "cjs",
  outdir: `${ROOT}/dist`,
  platform: "node",
  plugins: [
    nodeExternalsPlugin({ packagePath: [`${ROOT}/package.json`] }),
    esbuildDecorators({ cwd: process.cwd(), force: false, tsconfig: `${ROOT}/tsconfig.build.json`, tsx: false })
  ],
  target: ["es2020"],
  tsconfig: `${ROOT}/tsconfig.build.json`
}).catch((error) => {
  console.error(error);
});
