const base = require("../../jest.base.js");

module.exports = {
  ...base,
  name: "@efuse/key-store",
  displayName: "@efuse/key-store",
  setupFilesAfterEnv: ["jest-extended", "jest-chain", require.resolve("./setup-tests.ts")]
};
