import { PermissionsService } from "../permissions.service";

export abstract class PolicyManager {
  public abstract builder: any;

  protected $permissionsService: PermissionsService;

  constructor() {
    this.$permissionsService = new PermissionsService();
  }
}
