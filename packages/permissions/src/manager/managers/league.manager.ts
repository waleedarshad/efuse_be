import { Trace } from "@efuse/decorators";
import { Types } from "mongoose";
import { IPolicy } from "../../interfaces";
import { LeaguePolicyBuilder } from "../builders";
import { PolicyManager } from "../policy-manager";

// TODO: The methods here should use our existing ObjectId type, but that is going to require moving
// that type to a different package and that isn't the most straightforward with our current state
// of packages being in flux. Definitely don't want to be setting up circular dependancies.

export class LeaguePolicyManager extends PolicyManager {
  public builder = LeaguePolicyBuilder;

  /**
   * Adds a permission for one organization to join the host's leagues.
   * @param joiningOrgId The ID of the organization that is joining the host organization's leagues.
   * @param hostOrgId The ID of the organization that is hosting the leagues.
   * @returns If the policy was stored.
   */
  @Trace()
  public async addJoinedOrgPolicy(
    joiningOrgId: Types.ObjectId | string,
    hostOrgId: Types.ObjectId | string
  ): Promise<boolean> {
    const policy = this.builder.joinOrgPolicy(joiningOrgId, hostOrgId);

    return await this.$permissionsService.addPolicy(policy);
  }

  /**
   * Gets an array of policies where the organization has permission to join the host organization's leagues.
   * @param orgId The ID of the organization to find joined organizations (leagues) for.
   * @returns An array of policies representing the permissions.
   */
  @Trace()
  public async findJoinedOrgsPolicies(orgId: Types.ObjectId | string): Promise<IPolicy[]> {
    const policy = this.builder.joinedOrgsPolicy(orgId);

    return await this.$permissionsService.findPolicies(policy);
  }

  /**
   * Determines whether an organization has permission to join the host organization's leagues.
   * @param joiningOrgId The ID of the organization that may have joined the host organization's leagues.
   * @param hostOrgId The ID of the organization that is hosting the leagues.
   * @returns Whether or not the organization has joined the host organization's leagues.
   */
  @Trace()
  public async hasJoinedOrgPolicy(
    joiningOrgId: Types.ObjectId | string,
    hostOrgId: Types.ObjectId | string
  ): Promise<boolean> {
    const policy = this.builder.joinOrgPolicy(joiningOrgId, hostOrgId);

    return await this.$permissionsService.policyExists(policy);
  }
}
