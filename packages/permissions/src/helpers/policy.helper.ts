import { Failure } from "@efuse/contracts";
import { IGroupPolicy } from "../interfaces/group-policy";
import { IPolicy } from "../interfaces/policy";

/**
 * Convert policy interface object to array that can be passed into underlying casbin plugin
 * @date 7/29/2021
 *
 * @param {IPolicy} policy
 * @returns {string[]}
 */
export function convertPolicyToArray(policy: IPolicy): string[] {
  return [policy.subject ?? "", policy.tenant ?? "", policy.object ?? "", policy.action ?? ""];
}

/**
 * Convert policy interface objects to array that can be passed into underlying casbin plugin
 * @date 7/29/2021
 *
 * @param {IPolicy[]} policies
 * @returns {string[][]}
 */
export function convertPoliciesToArray(policies: IPolicy[]): string[][] {
  return policies.map((policy) => convertPolicyToArray(policy));
}

/**
 * Convert group policy interface object to array that can be passed into underlying casbin plugin
 * @date 7/29/2021
 *
 * @param {IGroupPolicy} groupPolicy
 * @returns {string[]}
 */
export function convertGroupPolicyToArray(groupPolicy: IGroupPolicy): string[] {
  return [groupPolicy.subject ?? "", groupPolicy.tenant ?? "", groupPolicy.policySubject ?? ""];
}

/**
 * Convert group policy interface objects to array that can be passed into underlying casbin plugin
 * @date 7/29/2021
 *
 * @param {Policy[]} policies
 * @returns {string[][]}
 */
export function convertGroupPoliciesToArray(groupPolicies: IGroupPolicy[]): string[][] {
  return groupPolicies.map((groupPolicy) => convertGroupPolicyToArray(groupPolicy));
}

/**
 * Convert raw policy array from Casbin to an IPolicy object
 * @date 8/23/2021
 *
 * @export
 * @param {string[]} rawPolicy
 * @returns {IPolicy}
 */
export function convertArrayToPolicy(rawPolicy: string[]): IPolicy {
  if (rawPolicy.length !== 4) {
    throw Failure.InternalServerError("Invalid raw policy");
  }

  return { subject: rawPolicy[0], tenant: rawPolicy[1], object: rawPolicy[2], action: rawPolicy[3] };
}

/**
 * Convert raw array of policies from Casbin to array of IPolicy objects
 * @date 7/30/2021
 *
 * @export
 * @param {string[][]} rawPolicies
 * @returns {IPolicy[]}
 */
export function convertArrayToPolicies(rawPolicies: string[][]): IPolicy[] {
  return rawPolicies.map((rawPolicy) => convertArrayToPolicy(rawPolicy));
}

/**
 * Convert raw grouping policy array from Casbin to an IGroupPolicy object
 * @date 8/23/2021
 *
 * @export
 * @param {string[]} rawPolicy
 * @returns {IGroupPolicy}
 */
export function convertArrayToGroupPolicy(rawPolicy: string[]): IGroupPolicy {
  if (rawPolicy.length < 3) {
    throw Failure.InternalServerError("Invalid raw policy");
  }

  return { subject: rawPolicy[0], policySubject: rawPolicy[1], tenant: rawPolicy[2] };
}

/**
 * Convert raw array of grouping policies from Casbin to array of IGroupPolicy objects
 * @date 7/30/2021
 *
 * @export
 * @param {string[][]} rawPolicies
 * @returns {IGroupPolicy[]}
 */
export function convertArrayToGroupPolicies(rawPolicies: string[][]): IGroupPolicy[] {
  return rawPolicies.map((rawPolicy) => convertArrayToGroupPolicy(rawPolicy));
}
