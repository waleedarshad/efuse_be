export interface IGroupPolicy {
  /**
   * The entity performing an action.
   * @type {string}
   */
  subject?: string;

  /**
   * The policy name to assign to the subject
   * @type {string}
   */
  policySubject?: string;

  /**
   * Tenant to restrict access.
   * I.E. Organization ID. or empty string for no tenant.
   *
   * @type {?string}
   */
  tenant?: string;
}
