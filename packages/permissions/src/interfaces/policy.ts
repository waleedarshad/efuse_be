export interface IPolicy {
  /**
   * The subject of the policy.
   * This can be a user id, org id, or a group name for creating group policies
   * @type {string}
   */
  subject?: string;

  /**
   * The resource being accessed
   * @type {string}
   */
  object?: string;

  /**
   * Access method
   * Could be read, write, all, etc..
   *
   * @type {string}
   */
  action?: string;

  /**
   * Tenant to restrict access.
   * I.E. Organization ID. or empty string for no tenant.
   *
   * @type {?string}
   */
  tenant?: string;
}
