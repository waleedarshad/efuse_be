export enum PermissionActionsEnum {
  ACCESS = "access",
  ALL = "all",
  READ = "read",
  WRITE = "write"
}
