import { MONGO_DB_URL } from "@efuse/key-store";
import { newSyncedEnforcer, newModelFromString, Model, SyncedEnforcer } from "casbin";
import { MongooseAdapter } from "casbin-mongoose-adapter";

enum State {
  uninitialized,
  initializing,
  initialized
}

export class PermissionEnforcer {
  private static Model: Model | undefined;
  private static Adapter: MongooseAdapter | undefined;
  private static PermissionEnforcer: SyncedEnforcer | undefined;
  private static State: State = State.uninitialized;
  private static EnforcerPromise: Promise<boolean>;

  public static async Instance(): Promise<SyncedEnforcer> {
    if (PermissionEnforcer.State === State.uninitialized) {
      // When ran asynchronously the delay in initializing causes multiple copies to be spawned so
      // we're marking it as initializing right away to give that time to complete before other
      // instances attempt to do the same thing.
      PermissionEnforcer.State = State.initializing;

      PermissionEnforcer.Model = newModelFromString(`
        [request_definition]
        r = sub, dom, obj, act

        [policy_definition]
        p = sub, dom, obj, act

        [role_definition]
        g = _, _, _

        [policy_effect]
        e = some(where (p.eft == allow))

        [matchers]
        m = g(r.sub, p.sub, r.dom) && r.dom == p.dom && r.obj == p.obj && r.act == p.act
      `);

      PermissionEnforcer.EnforcerPromise = new Promise(async (resolve, reject) => {
        // A mongoose synced adapter is required if we want to use removePolicies
        PermissionEnforcer.Adapter = await MongooseAdapter.newSyncedAdapter(MONGO_DB_URL);

        PermissionEnforcer.PermissionEnforcer = await newSyncedEnforcer(
          PermissionEnforcer.Model,
          PermissionEnforcer.Adapter
        );

        PermissionEnforcer.State = State.initialized;
        PermissionEnforcer.PermissionEnforcer.enableAutoSave(true);

        resolve(true);

        // TODO: Reject on error.
      });
    }

    await PermissionEnforcer.EnforcerPromise;

    // This can only be reached if the PermissionEnforcer is not null but TypeScript can't verify
    // that so we're manually asserting it to be non-null. Should probably add some error checking
    // around this just in case at some point.
    await PermissionEnforcer.PermissionEnforcer!.loadPolicy();

    return PermissionEnforcer.PermissionEnforcer!;
  }
}
