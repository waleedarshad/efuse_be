import { Failure } from "@efuse/contracts";
import { Service, Trace } from "@efuse/decorators";
import {
  convertArrayToGroupPolicies,
  convertArrayToPolicies,
  convertGroupPoliciesToArray,
  convertGroupPolicyToArray,
  convertPoliciesToArray,
  convertPolicyToArray
} from "./helpers/policy.helper";
import { IGroupPolicy } from "./interfaces/group-policy";
import { IPolicy } from "./interfaces/policy";
import { PermissionEnforcer } from "./permission-enforcer";

/**
 * The Permission service uses the underlying permission enforcer to manage permissions for eFuse.
 *
 * @link https://casbin.org/docs/en/how-it-works
 * @date 7/29/2021
 *
 * @export
 * @class PermissionsService
 * @typedef {PermissionsService}
 */
@Service("permissions.service")
export class PermissionsService {
  /**
   * Add policy to permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IPolicy} policy the policy to create
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async addPolicy(policy: IPolicy): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.addPolicy(...convertPolicyToArray(policy));

    return result;
  }

  /**
   * Add multiple policies at the same time to permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IPolicy[]} policies
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async addPolicies(policies: IPolicy[]): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.addPolicies(convertPoliciesToArray(policies));

    return result;
  }

  /**
   * Update existing policy in permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IPolicy} originalPolicy
   * @param {IPolicy} newPolicy
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async updatePolicy(originalPolicy: IPolicy, newPolicy: IPolicy): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.updatePolicy(convertPolicyToArray(originalPolicy), convertPolicyToArray(newPolicy));

    return result;
  }

  /**
   * Delete existing policy from permission system
   * @date 7/29/2021 - 3:14:56 PM
   *
   * @public
   * @async
   * @param {IPolicy} policy
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async deletePolicy(policy: IPolicy): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.removePolicy(...convertPolicyToArray(policy));

    return result;
  }

  /**
   * Delete multiple policies from permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IPolicy[]} policies
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async deletePolicies(policies: IPolicy[]): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.removePolicies(convertPoliciesToArray(policies));

    return result;
  }

  /**
   * Add group policy to permissions system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IGroupPolicy} groupPolicy
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async addGroupPolicy(groupPolicy: IGroupPolicy): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.addGroupingPolicy(...convertGroupPolicyToArray(groupPolicy));

    return result;
  }

  /**
   * Add multiple group policies to permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IGroupPolicy[]} groupPolicies
   * @returns {Promise<boolean>}
   */
  public async addGroupPolicies(groupPolicies: IGroupPolicy[]): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();
    const result = await enforcer.addGroupingPolicies(convertGroupPoliciesToArray(groupPolicies));

    return result;
  }

  /**
   * Delete existing group policy from permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IGroupPolicy} groupPolicy
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async deleteGroupPolicy(groupPolicy: IGroupPolicy): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.removeGroupingPolicy(...convertGroupPolicyToArray(groupPolicy));

    return result;
  }

  /**
   * Delete multiple group policies from permission system
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IGroupPolicy[]} groupPolicies
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async deleteGroupPolicies(groupPolicies: IGroupPolicy[]): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();

    const result = await enforcer.removeGroupingPolicies(convertGroupPoliciesToArray(groupPolicies));

    return result;
  }

  /**
   * Check if policy already exists in the permission system
   *
   * Note: this should not be used to verify permissions. Use `hasPermission` for that.
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IPolicy} policy
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async policyExists(policy: IPolicy): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();
    const result = await enforcer.hasPolicy(...convertPolicyToArray(policy));

    return result;
  }

  /**
   * Search the system for policies based on subject, object, action, and/or tenant
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IPolicy} policy
   * @returns {Promise<IPolicy[]>}
   */
  public async findPolicies(policy: IPolicy): Promise<IPolicy[]>;
  /**
   * Search the system for policies based on subject, object, action, and/or tenant
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {?string} subject
   * @param {?string} object
   * @param {?string} action
   * @param {?string} tenant
   * @returns {Promise<IPolicy[]>}
   */
  public async findPolicies(subject?: string, object?: string, action?: string, tenant?: string): Promise<IPolicy[]>;
  @Trace()
  public async findPolicies(
    subject?: string | IPolicy,
    object?: string,
    action?: string,
    tenant?: string
  ): Promise<IPolicy[]> {
    // eslint-disable-next-line prefer-rest-params
    if (!this.areAnyArgsSet(arguments)) {
      throw Failure.BadRequest("No policy properties were provided.");
    }

    const policy =
      // eslint-disable-next-line prefer-rest-params
      typeof arguments[0] === "string"
        ? <IPolicy>{
            subject: subject,
            object: object,
            action: action,
            tenant: tenant
          }
        : // Forced typecasting here to inform TypeScript that subject refers to an object at this point.
          <IPolicy>subject ?? {};
    const enforcer = await PermissionEnforcer.Instance();
    const results = await enforcer.getFilteredPolicy(
      0,
      policy.subject ?? "",
      policy.tenant ?? "",
      policy.object ?? "",
      policy.action ?? ""
    );

    return convertArrayToPolicies(results);
  }

  /**
   * Search the system for group policies based on subject, policySubject, and/or tenant
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {IGroupPolicy} policy
   * @returns {Promise<IGroupPolicy[]>}
   */
  public async findGroupPolicies(policy: IGroupPolicy): Promise<IGroupPolicy[]>;
  /**
   * Search the system for group policies based on subject, policySubject, and/or tenant
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {?string} subject
   * @param {?string} policySubject
   * @param {?string} tenant
   * @returns {Promise<IGroupPolicy[]>}
   */
  public async findGroupPolicies(subject?: string, policySubject?: string, tenant?: string): Promise<IGroupPolicy[]>;
  @Trace()
  public async findGroupPolicies(
    subject?: string | IGroupPolicy,
    policySubject?: string,
    tenant?: string
  ): Promise<IGroupPolicy[]> {
    // eslint-disable-next-line prefer-rest-params
    if (!this.areAnyArgsSet(arguments)) {
      throw Failure.BadRequest("No policy properties were provided.");
    }

    const policy =
      // eslint-disable-next-line prefer-rest-params
      typeof arguments[0] === "string"
        ? <IGroupPolicy>{
            subject: subject,
            policySubject: policySubject,
            tenant: tenant
          }
        : // Forced typecasting here to inform TypeScript that subject refers to an object at this point.
          <IGroupPolicy>subject ?? {};
    const enforcer = await PermissionEnforcer.Instance();
    const results = await enforcer.getFilteredGroupingPolicy(
      0,
      policy.subject ?? "",
      policy.policySubject ?? "",
      policy.tenant ?? ""
    );

    return convertArrayToGroupPolicies(results);
  }

  /**
   * Verifies the subject is allowed to perform the action on the object supplied
   * Optionally specify tenant to only check for a specific tenant
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {string} subject
   * @param {string} object
   * @param {string} action
   * @param {?string} tenant
   * @returns {Promise<boolean>}
   */
  @Trace()
  public async hasPermission(subject: string, object: string, action: string, tenant?: string): Promise<boolean> {
    const enforcer = await PermissionEnforcer.Instance();
    // policies are subject, tenant, object, action
    const userHasDirectAccess =
      (<string[][]>await enforcer.getFilteredPolicy(0, subject, tenant ?? "", object ?? "", action ?? "")).length > 0;

    // retrieve all the groups for the subject (optionally specifying tenant
    // group policies are subject, policySubject, tenant
    const userGroups = await enforcer.getFilteredGroupingPolicy(0, subject, "", tenant ?? "");
    // check if those groups have the permission
    const permissionsPromises = <Promise<boolean>[]>userGroups?.map((group) => {
      return enforcer.enforce(group[1], group[2], object, action);
    });

    const userHasGroupAccess = (await Promise.all(permissionsPromises)).some((val) => val === true);

    return userHasDirectAccess || userHasGroupAccess;
  }

  /**
   * Retrieves all the subjects that are part of the supplied group
   * Optionally specify tenant to restrict results to a specific tenant.
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {string} group
   * @param {?string} tenant
   * @returns {Promise<string[]>} string array containing the subject values
   */
  @Trace()
  public async fetchSubjectsInGroup(group: string, tenant?: string): Promise<string[]> {
    const enforcer = await PermissionEnforcer.Instance();

    let results: string[][] = [];
    if (tenant) {
      results = await enforcer.getFilteredGroupingPolicy(1, group, tenant);
    } else {
      results = await enforcer.getFilteredGroupingPolicy(1, group);
    }
    const groupSubjects = <string[]>results.map((grouping) => grouping[0]);

    return groupSubjects;
  }

  /**
   * Retrieves all the subjects that have a group role for a tenant
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {string} tenant
   * @returns {Promise<string[]>} string array containing the subject values
   */
  @Trace()
  public async fetchSubjectWithGroupRoleForTenant(tenant: string): Promise<string[]> {
    const enforcer = await PermissionEnforcer.Instance();

    // we are using index 2 as that is ALWAYS the tenant index
    const results = await enforcer.getFilteredGroupingPolicy(2, tenant);
    const groupMembers = <string[]>results?.map((policy) => policy[0]);

    return groupMembers;
  }

  /**
   * Retrieve all permission for a subject
   * Optionally specify tenant to restrict results to a specific tenant.
   *
   * Note: this code is subject to change based on this issue: https://github.com/casbin/node-casbin/issues/304
   *
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {string} subject
   * @param {?string} tenant
   * @returns {Promise<IPolicy[]>}
   */
  @Trace()
  public async fetchPermissionsForSubject(subject: string): Promise<IPolicy[]> {
    const enforcer = await PermissionEnforcer.Instance();

    const userPermissions = <string[][]>await enforcer.getPermissionsForUser(subject);
    const userGroups = await enforcer.getFilteredGroupingPolicy(0, subject);
    const userGroupPermissions: string[][] = [];

    const permissionsPromises = <Promise<string[][]>[]>userGroups?.map((group) => {
      return <Promise<string[][]>>enforcer.getPermissionsForUser(group[1]);
    });
    (await Promise.all(permissionsPromises)).forEach((permission) => {
      userGroupPermissions.push(...permission);
    });

    userPermissions.push(...userGroupPermissions);

    return convertArrayToPolicies(userPermissions);
  }

  /**
   * Retrieve all groups for the specified subject
   * @date 7/29/2021
   *
   * @public
   * @async
   * @param {string} subject
   * @param {?string} tenant
   * @returns {Promise<IGroupPolicy[]>}
   */
  @Trace()
  public async fetchGroupsForSubject(subject: string): Promise<IGroupPolicy[]> {
    const enforcer = await PermissionEnforcer.Instance();

    const userGroups = await enforcer.getFilteredGroupingPolicy(0, subject);

    return convertArrayToGroupPolicies(userGroups);
  }

  /**
   * Checks to see if at least one of the arguments passed is set. If the arguments iterable object is
   * empty it returns false. This expects the actual language defined arguments object but will work
   * with other Array-like objects.
   *
   * TODO: This is copied from general.helper. It needs to be refactored into a separate package to
   * avoid circular dependancies.
   * @param {IArguments} args
   * @returns {boolean}
   */
  @Trace()
  private areAnyArgsSet(args) {
    if (!args) {
      // This can only occur if someone calls this on something other than the arguments object, which
      // will always be defined within a function.
      throw Failure.BadRequest("The arguments object was not set.");
    }

    // Will return false if the array is empty as well.
    return Array.from(args).some((value) => {
      return value != null;
    });
  }
}
