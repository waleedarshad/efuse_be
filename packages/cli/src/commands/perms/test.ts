import { flags } from "@oclif/command";
// import { OrganizationACLService, MongodbService } from "@efuse/api";
// import { OrganizationUserRoleEnum, PermissionsService } from "@efuse/permissions";

import { EFuseCommand } from "../../command";

export default class Test extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "orgId", description: "Optional orgId", require: false, default: "NOT_A_REAL_ORG" },
    { name: "object", description: "Optional object", require: false, default: "TEST_PERMISSION" },
    { name: "action", description: "Optional action", require: false, default: "TEST_ACTION" }
  ];
  public static description = "Command to get test permissions (for orgs).";
  public static examples = [`$ efusecli perms:test`, `$ efusecli perms:test`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  // private organizationACLService: OrganizationACLService = new OrganizationACLService();
  // private permissionService: PermissionsService = new PermissionsService();

  public async run(): Promise<void> {
    // const flags = this.parsedFlags as OutputFlags<typeof Test.flags>;
    // const args = this.parsedArgs! as OutputArgs<any>;

    // MongodbService.initialize()
    //   .then(async () => {
    //     // await this.getOrgPermissions(flags, args);
    //     // await this.createOrgPermissions(flags, args);
    //     // await this.getOrgPermissions(flags, args);
    //     // await this.deleteOrgPermissions(flags, args);
    //     // await this.getOrgPermissions(flags, args);
    //
    //     process.exit(0);
    //   })
    //   .catch((error) => {
    //     this.logger.error(error);
    //   });
    process.exit(0);
  }

  // private async createOrgPermissions(flags: OutputFlags<typeof Test.flags>, args: OutputArgs<any>): Promise<void> {
  //   const orgAddResult = await this.organizationACLService.addOrgPermission(args.orgId, args.object, args.action, [
  //     OrganizationUserRoleEnum.CAPTAIN,
  //     OrganizationUserRoleEnum.MEMBER
  //   ]);
  //
  //   this.logger.debug({ orgAddResult }, "createdOrgPermissions: orgAddResult");
  // }
  //
  // private async deleteOrgPermissions(flags: OutputFlags<typeof Test.flags>, args: OutputArgs<any>): Promise<void> {
  //   const orgDeleteResult = await this.organizationACLService.removeOrgPermission(args.orgId, args.object, args.action);
  //
  //   this.logger.debug({ orgDeleteResult }, "deleteOrgPermissions: orgDeleteResult");
  // }
  //
  // private async getOrgPermissions(flags: OutputFlags<typeof Test.flags>, args: OutputArgs<any>): Promise<void> {
  //   const permissions = await this.organizationACLService.orgHasPermission(args.orgId, args.object, args.action);
  //
  //   this.logger.debug({ permissions }, "getOrgPermissions: permissions");
  //
  //   const policies = await this.permissionService.findPolicies({
  //     tenant: args.orgId,
  //     object: args.object,
  //     action: args.action
  //   });
  //
  //   this.logger.debug({ policies }, "getOrgPermissions: policies");
  // }
}
