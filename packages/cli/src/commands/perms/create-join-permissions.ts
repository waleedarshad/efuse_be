// import { OutputArgs, OutputFlags } from "@oclif/parser";
import { MongodbService, LeagueService, TeamService } from "@efuse/api";
import { EFuseCommand } from "../../command";
import { OwnerType } from "@efuse/entities";
// import { Types } from "mongoose";

const HOST_ORG_ID = "BLEH"; // "60b7d19158a8d6001f640110";

export default class CreateJoinPermissions extends EFuseCommand {
  // public static args = [...EFuseCommand.args];
  public static description =
    "Command to create join permission for existing league in the new 'temporary' eFuse system.";
  public static examples = [`./ef perms:create-join-permissions`];
  // public static flags = {
  //   ...EFuseCommand.flags,
  //   help: flags.help({ char: "h" })
  // };

  public async run(): Promise<void> {
    // const flags = this.parsedFlags as OutputFlags<typeof TranslateOrgMembers.flags>;
    // const args = this.parsedArgs! as OutputArgs<any>;
    //

    MongodbService.initialize()
      .then(async () => {
        await this.processOldPermissions();
        process.exit(0);
      })
      .catch((error) => {
        this.logger.error(error);
      });
  }

  private async processOldPermissions(): Promise<void> {
    // const eFusePermissionsService: EFusePermissionsService = new EFusePermissionsService();
    const leagueService: LeagueService = new LeagueService();
    const teamService: TeamService = new TeamService();

    const leagues = await leagueService.find(
      {
        ownerType: OwnerType.ORGANIZATIONS,
        owner: HOST_ORG_ID
      },
      { teams: true },
      { lean: true }
    );

    const teams = (<string[]>[]).concat(
      ...leagues.map((league) => {
        return league.teams.map((team) => team.toString());
      })
    );

    const teamsWithOwners = await teamService.find({ _id: { $in: teams } }, { _id: 1, name: 1, owner: 1 }, { lean: 1 });

    const organizationIds = teamsWithOwners.map((team) => team.owner.toString());
    this.logger.info(organizationIds);

    const uniqueOrgIds = [...new Set(organizationIds)];
    this.logger.info(uniqueOrgIds);

    const orgJoinPermissionPromises = uniqueOrgIds.map((org) => {
      return leagueService.giveOrgLeagueJoinPermissions(org, HOST_ORG_ID);
    });

    await Promise.all(orgJoinPermissionPromises);
  }
}
