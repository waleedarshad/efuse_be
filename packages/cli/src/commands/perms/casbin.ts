import { MONGO_DB_URL } from "@efuse/key-store";
import { newSyncedEnforcer, SyncedEnforcer, newModelFromString, Model } from "casbin";
import { MongoAdapter } from "casbin-mongodb-adapter";

import { EFuseCommand } from "../../command";

export default class TestCasbin extends EFuseCommand {
  static description = "Test Casbin Functionality";

  private _model: Model | undefined;
  private _adapter: MongoAdapter | undefined;
  private _permissionEnforcer: SyncedEnforcer | undefined;

  async run() {
    await this.initializeEnforcer();

    await this.createOrgAndUserPolicies();

    //get all policies
    const policies = await this._permissionEnforcer?.getPolicy();
    this.logger.info(<string[][]>policies);

    await this.getOrgHasAccessToRecruit();

    await this.getUserHasAccessToRecruit();

    await this.getOrgCaptains();

    await this.getAllOrgMembers();
  }

  private async getOrgHasAccessToRecruit(): Promise<void> {
    const results = <boolean>await this._permissionEnforcer?.hasPolicy("my_org_id", "recruit", "access");
    this.logger.info(`my_org_id has access: ${results}`);

    const results2 = <boolean>await this._permissionEnforcer?.hasPolicy("my_org_id_2", "recruit", "access");
    this.logger.info(`my_org_id_2 has access: ${results2}`);
  }

  private async getUserHasAccessToRecruit(): Promise<void> {
    // get all orgs my_user_id is a captain of
    const results = await this._permissionEnforcer?.getFilteredPolicy(0, "my_user_id", "", "org_captain");
    if (!results) {
      return;
    }
    this.logger.info(<string[][]>results);

    // iterate over orgs that the user is a captain of, and check if the org has access
    const promises = results.map(async (policy) => {
      const orgId = policy[1];

      const orgHasAccess = await this._permissionEnforcer?.hasPolicy(orgId, "recruit", "access");
      this.logger.info(`${orgId} recruit access: ${orgHasAccess}`);

      return orgHasAccess;
    });
    const userHasAccess = (await Promise.all(promises)).some((value) => value);

    this.logger.info(`User has access: ${userHasAccess}`);
  }

  private async getOrgCaptains(): Promise<void> {
    const results = await this._permissionEnforcer?.getFilteredPolicy(1, "my_org_id", "org_captain");
    const orgCaptains = <string[]>results?.map((policy) => policy[0]);

    this.logger.info(`Org Captains: ${orgCaptains}`);
  }

  private async getAllOrgMembers(): Promise<void> {
    // this will find all policies where the object is "my_org_id_3"
    // this implies a user is the subject and any "action" is fine
    const results = await this._permissionEnforcer?.getFilteredPolicy(1, "my_org_id_3");
    const orgMembers = <string[]>results?.map((policy) => policy[0]);

    this.logger.info(`Org Members: ${orgMembers}`);
  }

  // TODO: Does not match our existing casbin implementation. See permission-enforcer.ts.
  private async initializeEnforcer(): Promise<void> {
    this._model = newModelFromString(`
    [request_definition]
    r = sub, obj, act

    [policy_definition]
    p = sub, obj, act

    [role_definition]
    g = _, _

    [policy_effect]
    e = some(where (p.eft == allow))

    [matchers]
    m = g(r.sub, p.sub) && r.obj == p.obj && r.act == p.act
    `);

    this._adapter = await MongoAdapter.newAdapter({
      uri: MONGO_DB_URL,
      collection: "casbin_test",
      database: "efuse"
    });

    this._permissionEnforcer = await newSyncedEnforcer(this._model, this._adapter);
    await this._permissionEnforcer.loadPolicy();
  }

  private async createOrgAndUserPolicies(): Promise<void> {
    // add recruitment access permission to org
    await this._permissionEnforcer?.addPolicy("my_org_id", "recruit", "access");

    // add org captain policies for my_user_id
    await this._permissionEnforcer?.addPolicy("my_user_id", "my_org_id", "org_captain");
    await this._permissionEnforcer?.addPolicy("my_user_id", "my_org_id_2", "org_captain");
    await this._permissionEnforcer?.addPolicy("my_user_id", "my_org_id_3", "member");

    // add org membership policies for my_other_user_id
    await this._permissionEnforcer?.addPolicy("my_other_user_id", "my_org_id", "org_captain");
    await this._permissionEnforcer?.addPolicy("my_other_user_id", "my_org_id_3", "org_captain");

    // add org membership policies for my_other_other_user_id
    await this._permissionEnforcer?.addPolicy("my_other_other_user_id", "my_org_id_3", "SOME_OTHER_ROLE");
  }
}
