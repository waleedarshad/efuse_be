import { flags } from "@oclif/command";
// import { OutputArgs, OutputFlags } from "@oclif/parser";
// import { OrganizationsService, OrganizationACLService, MongodbService, IOrganization, MemberService } from "@efuse/api";
// import { OrganizationUserRoleEnum } from "@efuse/permissions";
import { EFuseCommand } from "../../command";
// import { Types } from "mongoose";

export default class TranslateOrgMembers extends EFuseCommand {
  public static args = [...EFuseCommand.args];
  public static description =
    "Command to get create permissions in the casbin system for all org members, including captains and owners.";
  public static examples = [`$ efusecli perms:translate-org-members`, `$ efusecli perms:translate-org-members`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  // private organizationACLService: OrganizationACLService = new OrganizationACLService();
  // private organizationService: OrganizationsService = new OrganizationsService();
  // private memberService: MemberService = new MemberService();

  public async run(): Promise<void> {
    // const flags = this.parsedFlags as OutputFlags<typeof TranslateOrgMembers.flags>;
    // const args = this.parsedArgs! as OutputArgs<any>;
    //
    // MongodbService.initialize()
    //   .then(async () => {
    //     await this.processOrgMembers(flags, args);
    //     process.exit(0);
    //   })
    //   .catch((error) => {
    //     this.logger.error(error);
    //   });
    process.exit(0);
  }

  // private async processOrgMembers(
  //   flags: OutputFlags<typeof TranslateOrgMembers.flags>,
  //   args: OutputArgs<any>
  // ): Promise<void> {
  //   const limit = 10;
  //   let page = 1;
  //   let limitReached = false;
  //   // retrieve paginated list of orgs
  //   // iterate over results
  //   // pull owner, captains, and users
  //   // create perms for each of them
  //   while (!limitReached) {
  //     this.logger.info(`Processing page: ${page}`);
  //
  //     const orgs = await this.organizationService.findPaginated({}, { page, limit });
  //     this.logger.info(`Retrieved ${orgs.docs.length} orgs.`);
  //
  //     const processPromises = orgs.docs.map((org) => this.processOrganization(org));
  //
  //     await Promise.all(processPromises);
  //
  //     page += 1;
  //     limitReached = orgs.docs.length < limit;
  //
  //     this.logger.info(`Finished processing page: ${page}`);
  //   }
  // }
  //
  // private async processOrganization(org: IOrganization): Promise<void> {
  //   const owner = (<Types.ObjectId>org.user)?.toString();
  //   const captains = org.captains?.map((captain: Types.ObjectId) => captain.toString());
  //   const members = (await this.memberService.find({ organization: org._id }, { user: true }, { lean: true })).map(
  //     (member) => member.user.toString()
  //   );
  //
  //   // add owner permission
  //   if (owner) {
  //     const ownerResult = await this.organizationACLService.addUserOrgRole(
  //       org._id.toString(),
  //       owner,
  //       OrganizationUserRoleEnum.OWNER
  //     );
  //     this.logger.info(
  //       `${ownerResult ? "Successfully" : "Unsuccessfully"} set user (${owner}) to owner of org ${org._id}`
  //     );
  //   }
  //
  //   // add captains permission
  //   if (captains) {
  //     const captainPermissionPromises = captains.map((captain) =>
  //       this.organizationACLService.addUserOrgRole(org._id.toString(), captain, OrganizationUserRoleEnum.CAPTAIN)
  //     );
  //     const captainsResult = (await Promise.all(captainPermissionPromises)).every((result) => result);
  //     this.logger.info(
  //       `${captainsResult ? "Successfully" : "Unsuccessfully"} set users (${
  //         captains.length
  //       }) to captains of org ${org._id.toString()}`
  //     );
  //   }
  //
  //   // add members permissions
  //   if (members) {
  //     const memberPermissionPromises = members.map((member) =>
  //       this.organizationACLService.addUserOrgRole(org._id.toString(), member, OrganizationUserRoleEnum.MEMBER)
  //     );
  //     const memberResult = (await Promise.all(memberPermissionPromises)).every((result) => result);
  //     this.logger.info(
  //       `${memberResult ? "Successfully" : "Unsuccessfully"} set users (${
  //         members.length
  //       }) to captains of org ${org._id.toString()}`
  //     );
  //   }
  //
  //   this.logger.info({ owner, captains, members });
  //   return;
  // }
}
