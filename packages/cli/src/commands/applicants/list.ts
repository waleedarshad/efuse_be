import { Applicant, ApplicantEntityTypeEnum, User } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";
import { LeanDocument, Types } from "mongoose";

import { DataFormatService } from "../../services";
import { EFuseCommand } from "../../command";
import { USER_FIELDS } from "../../constants";

export default class ApplicantsList extends EFuseCommand {
  public static args = [...EFuseCommand.args, { name: "opportunityId", require: true }];
  public static description = "Command to list applicants for an opportunity.";
  public static examples = [`$ efusecli applicants:list 1234`, `$ efusecli learning_articles:list 1234 -f json`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof ApplicantsList.flags>;
    const args = this.parsedArgs!;
    this.listApplicantUsersForOpportunity(flags, args).catch((error) =>
      this.logger.error(error, "Error resolving listApplicantUsersForOpportunity promise")
    );
  }

  private async listApplicantUsersForOpportunity(flags, args): Promise<void> {
    try {
      const userIds: LeanDocument<{ user: Types.ObjectId }>[] = await Applicant.find(
        { entity: args.opportunityId, entityType: ApplicantEntityTypeEnum.OPPORTUNITY },
        { _id: 0 }
      )
        .select("user")
        .lean();

      const userIdList: Types.ObjectId[] = userIds.map((u) => u.user);

      const selection: string = this.formatService.determineSelectionBasedOnFormat(flags.format, USER_FIELDS);
      const users = await User.find({ _id: { $in: userIdList } })
        .select(selection)
        .lean();

      this.formatService.outputDataBasedOnFormat(flags.format, users, USER_FIELDS);
      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
