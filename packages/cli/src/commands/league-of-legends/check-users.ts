import { LeagueOfLegendsCronService } from "@efuse/api";

import { EFuseCommand } from "../../command";

export default class CheckUserLOLInfo extends EFuseCommand {
  static description = "Check users basic LOL info and fix issues.";

  async run(): Promise<void> {
    await this.checkUsersLeagueInfoAndCorrectIssues();
  }

  private async checkUsersLeagueInfoAndCorrectIssues(): Promise<void> {
    try {
      this.logger.info("Begin queuing Users for LOL Check");
      await LeagueOfLegendsCronService.CheckUsersLOLInfoCron();
      this.logger.info("Finished queuing Users for LOL Check");
    } catch (error) {
      throw this.logger.error(error);
    }
  }
}
