import {
  AlgoliaService,
  IPopulatedOrganizationUser,
  Organization,
  OrganizationPublishStatusEnum,
  OrganizationSearchService,
  UserFields,
  MongodbService
} from "@efuse/api";
import { DOMAIN } from "@efuse/key-store";
import { QueryCursor } from "mongoose";
import moment from "moment";

import { EFuseCommand } from "../../command";

export default class UserSyncWithAlgolia extends EFuseCommand {
  static description = "Clears the existing Organization indice & sync all organizations with Algolia";

  private $algoliaService: AlgoliaService = new AlgoliaService();
  private $organizationSearchService: OrganizationSearchService = new OrganizationSearchService();

  async run(): Promise<void> {
    MongodbService.initialize()
      .then(async () => {
        try {
          this.logger.info("Fetching all visible organization");

          const organizationCursor: QueryCursor<IPopulatedOrganizationUser> = (<unknown>Organization.find({
            publishStatus: OrganizationPublishStatusEnum.VISIBLE as string
          })
            .populate({ path: "user", select: UserFields.ALGOLIA_FIELDS_TO_BE_SELECTED })
            .lean()
            .cursor()) as QueryCursor<IPopulatedOrganizationUser>;

          const objects: Record<string, unknown>[] = [];

          for (
            let organization = await organizationCursor.next();
            organization !== null;
            organization = await organizationCursor.next()
          ) {
            this.logger.info(
              { _id: organization._id, url: `${DOMAIN}/org/${organization.shortName}` },
              "Build organization object to sync with algolia"
            );

            const object: Record<string, unknown> = {
              ...organization,
              user: organization.user ? this.$algoliaService.buildUserObject(organization.user) : null
            };

            if (organization.createdAt) {
              object.createdAtUnix = moment(organization.createdAt).unix();
            }

            if (organization.updatedAt) {
              object.updatedAtUnix = moment(organization.updatedAt).unix();
            }

            objects.push(object);
          }

          this.logger.info(`Clearing existing records at indice ${this.$organizationSearchService.$index}`);

          await this.$algoliaService.clearObjects(this.$organizationSearchService.$index);

          this.logger.info("Cleared records. Now syncing records!");

          await this.$algoliaService.syncObjects(this.$organizationSearchService.$index, objects);

          this.logger.info("All records are synced with algolia");

          process.exit(0);
        } catch (error) {
          this.logger.error("Error occurred while attempting to read from database.", { exit: false });
          this.logger.error(error);

          process.exit(0);
        }
      })
      .catch((error) => this.logger.error(error, "Error connecting to MongoDB"));
  }
}
