import { IUser, User } from "@efuse/api";

import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";
import { LeanDocument } from "mongoose";

import { DataFormatService } from "../../services";
import { EFuseCommand } from "../../command";
import { USER_FIELDS } from "../../constants";

export default class UsersList extends EFuseCommand {
  public static description = "eFuse command to list users or give more specific list of users based on flags";
  public static examples = ["ef users:list -i 1234", "ef users:list -e jc@gmail.com", "ef users:list -n tigerking420"];
  public static flags = {
    ...EFuseCommand.flags,
    email: flags.string({
      char: "e",
      description: "Lookup users by their email"
    }),
    help: flags.help({ char: "h" }),
    id: flags.string({
      char: "i",
      description: "Lookup users by their user id"
    }),
    username: flags.string({
      char: "n",
      description: "Lookup users by their username"
    })
  };

  private formatService: DataFormatService = new DataFormatService(this);

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof UsersList.flags>;
    this.listUsersByFlags(flags).catch((error) => this.logger.error(error, "Error resolving listUsersByFlags promise"));
  }

  private createUserQuery(email: string | undefined, id: string | undefined, name: string | undefined): {} {
    let query: {} = {};
    const adjustedQuery: (
      | { _id: string | undefined }
      | { email: string | undefined }
      | { username: string | undefined }
    )[] = [];

    if (id) {
      adjustedQuery.push({ _id: id });
    }
    if (email) {
      adjustedQuery.push({ email: email });
    }
    if (name) {
      adjustedQuery.push({ username: name });
    }

    if (adjustedQuery.length > 0) {
      query = {
        $or: adjustedQuery
      };
    }

    return query;
  }

  private async listUsersByFlags(flags): Promise<void> {
    const query: {} = this.createUserQuery(flags.email, flags.id, flags.username);
    const selection: string = this.formatService.determineSelectionBasedOnFormat(flags.format, USER_FIELDS);

    try {
      const users: LeanDocument<IUser>[] = await User.find(query).select(selection).lean();

      if (!users) {
        this.logger.error("Bad request. Couldn't find users");
      }

      this.formatService.outputDataBasedOnFormat(flags.format, users, USER_FIELDS);

      process.exit(0);
    } catch (e: any) {
      this.logger.error("Error connecting to database", { exit: false });
      this.logger.error(e);
    }
  }
}
