import { AimlabLeaderboard, LeagueOfLegendsLeaderboard, ValorantLeaderboard } from "@efuse/api";
import { OutputFlags } from "@oclif/parser";
import { flags } from "@oclif/command";

import { EFuseCommand } from "../../command";
import { DataFormatService } from "../../services";

enum Leaderboard {
  AIMLAB = "aimlab",
  LEAGUE_OF_LEGENDS = "lol",
  VALORANT = "valorant"
}

export default class UsersLeaderboardRank extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "userId", description: "Which user retrieve leaderboard info for", require: true },
    {
      name: "leaderboard",
      description: "Which leaderboard to get info from",
      require: true,
      options: [Leaderboard.AIMLAB, Leaderboard.LEAGUE_OF_LEGENDS, Leaderboard.VALORANT],
      default: Leaderboard.VALORANT
    }
  ];
  public static description = "eFuse command to get a users leaderboard info";
  public static examples = [
    "ef users:leaderboard --user 2345 --valorant",
    "ef users:leaderboard --user 2345 --valorant"
  ];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof UsersLeaderboardRank.flags>;
    const args = this.parsedArgs!;
    this.getUsersLeaderboardInfo(flags, args).catch((error) =>
      this.logger.error(error, "Error resolving getUsersLeaderboardInfo promise")
    );
  }

  private async getUsersLeaderboardInfo(flags, args): Promise<void> {
    try {
      let userLeaderboard;
      switch (args.leaderboard) {
        case Leaderboard.AIMLAB:
          userLeaderboard = await AimlabLeaderboard.findOne({ userId: args.userId }).lean();
          break;
        case Leaderboard.LEAGUE_OF_LEGENDS:
          userLeaderboard = await LeagueOfLegendsLeaderboard.findOne({ userId: args.userId }).lean();
          break;
        case Leaderboard.VALORANT:
        default:
          userLeaderboard = await ValorantLeaderboard.findOne({ userId: args.userId }).lean();
          break;
      }

      if (!userLeaderboard) {
        this.logger.error("Bad request. Couldn't find any leaderboard info");
      }

      this.formatService.outputDataBasedOnFormat(flags.format, [userLeaderboard]);

      process.exit(0);
    } catch (e: any) {
      this.logger.error("Error connecting to database", { exit: false });
      this.logger.error(e);
    }
  }
}
