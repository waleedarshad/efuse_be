import { AlgoliaService, User, UserFields, UserSearchService, SchemaHelper, MongodbService } from "@efuse/api";
import { UserStatus } from "@efuse/entities";
import { DOMAIN } from "@efuse/key-store";
import moment from "moment";

import { EFuseCommand } from "../../command";

export default class UserSyncWithAlgolia extends EFuseCommand {
  static description = "Clears the existing User indice & sync all users with Algolia";

  private $algoliaService: AlgoliaService = new AlgoliaService();
  private $userSearchService: UserSearchService = new UserSearchService();

  async run(): Promise<void> {
    MongodbService.initialize()
      .then(async () => {
        try {
          this.logger.info("Fetching all active users with a profile image uploaded");

          const userCursor = User.find(
            {
              status: UserStatus.ACTIVE,
              "profilePicture.url": { $ne: SchemaHelper.fileSchema.url.default }
            },
            {},
            { autopopulate: false }
          )
            .select(UserFields.ALGOLIA_FIELDS_TO_BE_SELECTED)
            .lean()
            .cursor();

          const objects: Record<string, unknown>[] = [];

          for (let user = await userCursor.next(); user !== null; user = await userCursor.next()) {
            this.logger.info(
              { _id: user._id, username: user.username, profileURL: `${DOMAIN}/u/${user.username}` },
              "Build user object to sync with algolia"
            );

            const userToBeSynced = this.$algoliaService.buildUserObject(user);

            if (user.createdAt) {
              userToBeSynced.createdAtUnix = moment(user.createdAt).unix();
            }

            if (user.updatedAt) {
              userToBeSynced.updatedAtUnix = moment(user.updatedAt).unix();
            }

            objects.push(userToBeSynced);
          }

          this.logger.info(`Clearing existing records at indice ${this.$userSearchService.$index}`);

          await this.$algoliaService.clearObjects(this.$userSearchService.$index);

          this.logger.info("Cleared records. Now syncing records!");

          await this.$algoliaService.syncObjects(this.$userSearchService.$index, objects);

          this.logger.info("All records are synced with algolia");

          process.exit(0);
        } catch (error) {
          this.logger.error("Error occurred while attempting to read from database.", { exit: false });
          this.logger.error(error);

          process.exit(0);
        }
      })
      .catch((error) => this.logger.error(error, "Error connecting to MongoDB"));
  }
}
