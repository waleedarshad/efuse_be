import { PipelineLeaderboardService } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { EFuseCommand } from "../../../command";
import { DataFormatService } from "../../../services";

export default class LeagueOfLegendsList extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "page", description: "What page to look at on list", require: true, default: 1 },
    { name: "limit", description: "Number of items per page", default: 25 }
  ];
  public static description = "Command to get paginated League Of Legends leaderboard list.";
  public static examples = [
    `$ efusecli leaderboards:league_of_legends:list 1`,
    `$ efusecli leaderboards:league_of_legends:list 1 5`
  ];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);
  private pipelineLeaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof LeagueOfLegendsList.flags>;
    const args = this.parsedArgs!;
    this.listPaginatedLeagueOfLegendsLeaderboard(flags, args).catch((error) =>
      this.logger.error(error, "Error resolving listPaginatedLeagueOfLegendsLeaderboard promise")
    );
  }

  private async listPaginatedLeagueOfLegendsLeaderboard(flags, args): Promise<void> {
    const page = args.page;
    const limit = args.limit;

    if (!flags.silent) {
      if (isNaN(page)) {
        this.logger.error("Page argument must be a number!");
      }

      if (isNaN(limit)) {
        this.logger.error("Limit argument must be a number!");
      }
    }

    try {
      const leaderboard = await this.pipelineLeaderboardService.getPaginatedLOLLeaderboard(page, limit);

      this.formatService.outputDataBasedOnFormat(flags.format, leaderboard.docs);
      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
