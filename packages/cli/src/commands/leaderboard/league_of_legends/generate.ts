import { LeagueOfLegendsLeaderboard, PipelineLeaderboardService } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { EFuseCommand } from "../../../command";
import { DataFormatService } from "../../../services";

export default class LeagueOfLegendsLeaderboardGenerate extends EFuseCommand {
  public static description = "Command to generate and update League of Legends pipeline leaderboard";
  public static examples = ["ef league_of_legends:leaderboard:generate"];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private dataFormatService: DataFormatService = new DataFormatService(this);
  private pipelineLeaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof LeagueOfLegendsLeaderboardGenerate.flags>;
    this.generateAndSaveLeagueOfLegendsPipelineLeaderboard(flags).catch((error) =>
      this.logger.error(error, "Error resolving generateAndSaveLeagueOfLegendsPipelineLeaderboard promise")
    );
  }

  private async generateAndSaveLeagueOfLegendsPipelineLeaderboard(flags): Promise<void> {
    await PipelineLeaderboardService.GenerateAndSaveLeagueOfLegendsLeaderboard();

    const foundLeaderboard = await LeagueOfLegendsLeaderboard.find({}).lean();

    this.dataFormatService.outputDataBasedOnFormat(flags.format, foundLeaderboard);

    process.exit(0);
  }
}
