import { AimlabLeaderboard, PipelineLeaderboardService } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { DataFormatService } from "../../../services";
import { EFuseCommand } from "../../../command";

export default class AimlabLeaderboardGenerate extends EFuseCommand {
  public static description = "Command to generate and update aimlab pipeline leaderboard";
  public static examples = ["ef aimlab:leaderboard:generate"];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private dataFormatService: DataFormatService = new DataFormatService(this);
  private pipelineLeaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof AimlabLeaderboardGenerate.flags>;
    this.generateAndSaveAimlabPipelineLeaderboard(flags).catch((error) =>
      this.logger.error(error, "Error resolving generateAndSaveAimlabPipelineLeaderboard promise")
    );
  }

  private async generateAndSaveAimlabPipelineLeaderboard(flags): Promise<void> {
    await PipelineLeaderboardService.GenerateAndSaveAimlabLeaderboard();

    const foundLeaderboard = await AimlabLeaderboard.find({}).lean();

    this.dataFormatService.outputDataBasedOnFormat(flags.format, foundLeaderboard);

    process.exit(0);
  }
}
