import { PipelineLeaderboardService } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { DataFormatService } from "../../../services";
import { EFuseCommand } from "../../../command";

export default class AimlabList extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "page", description: "What page to look at on list", require: true, default: 1 },
    { name: "limit", description: "Number of items per page", default: 25 }
  ];
  public static description = "Command to get paginated aimlab leaderboard list.";
  public static examples = [`$ efusecli leaderboards:aimlab:list 1`, `$ efusecli leaderboards:aimlab:list 1 5`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);
  private pipelineLeaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof AimlabList.flags>;
    const args = this.parsedArgs!;
    this.listPaginatedAimlabLeaderboard(flags, args).catch((error) =>
      this.logger.error(error, "Error resolving listPaginatedAimlabLeaderboard promise")
    );
  }

  private async listPaginatedAimlabLeaderboard(flags, args): Promise<void> {
    const page = args.page;
    const limit = args.limit;

    if (!flags.silent) {
      if (isNaN(page)) {
        this.logger.error("Page argument must be a number!");
      }

      if (isNaN(limit)) {
        this.logger.error("Limit argument must be a number!");
      }
    }

    try {
      const leaderboard = await this.pipelineLeaderboardService.getPaginatedAimlabLeaderboard(page, limit);

      this.formatService.outputDataBasedOnFormat(flags.format, leaderboard.docs);
      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
