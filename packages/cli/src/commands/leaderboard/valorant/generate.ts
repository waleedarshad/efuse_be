import { PipelineLeaderboardService, ValorantLeaderboard } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { EFuseCommand } from "../../../command";
import { DataFormatService } from "../../../services";

export default class ValorantLeaderboardGenerate extends EFuseCommand {
  public static description = "Command to generate and update valorant pipeline leaderboard";
  public static examples = ["ef valorant:leaderboard:generate"];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private dataFormatService: DataFormatService = new DataFormatService(this);
  private pipelineLeaderboardService: PipelineLeaderboardService = new PipelineLeaderboardService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof ValorantLeaderboardGenerate.flags>;
    this.generateAndSaveValorantPipelineLeaderboard(flags).catch((error) =>
      this.logger.error(error, "Error resolving generateAndSaveValorantPipelineLeaderboard promise")
    );
  }

  private async generateAndSaveValorantPipelineLeaderboard(flags): Promise<void> {
    await PipelineLeaderboardService.GenerateAndSaveValorantLeaderboard();
    const foundLeaderboard = await ValorantLeaderboard.find({}).lean();

    this.dataFormatService.outputDataBasedOnFormat(flags.format, foundLeaderboard);

    process.exit(0);
  }
}
