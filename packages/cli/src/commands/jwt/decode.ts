import { flags } from "@oclif/command";
import jwt_decode, { JwtHeader, JwtPayload } from "jwt-decode";

import { EFuseCommand } from "../../command";

export default class JwtDecode extends EFuseCommand {
  static description = "Decodes a Base64 encoded JWT.";

  static flags = {
    header: flags.boolean({ description: "Decode the JWT header." })
  };

  static args = [{ name: "token" }];

  async run(): Promise<void> {
    const { args, flags } = this.parse(JwtDecode);
    const token = args.token;
    const decodeHeader = flags.header;

    if (!token) {
      this.logger.error("A token must be provided.");
      process.exit(1);
    }

    try {
      const decodedToken = decodeHeader
        ? jwt_decode<JwtHeader>(token, { header: true })
        : jwt_decode<JwtPayload>(token);

      if (!decodedToken) {
        this.logger.error("The token failed to decode. Check your format and try again.");
        process.exit(1);
      }

      // TODO: Pretty print the JSON here. Maybe as a flag so that we can still chain output?
      this.echo(JSON.stringify(decodedToken));
      process.exit(0);
    } catch (e: any) {
      this.logger.error(e);
      process.exit(1);
    }
  }
}
