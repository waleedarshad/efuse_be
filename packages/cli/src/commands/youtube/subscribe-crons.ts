import { CronKindEnum, TrackCron, YoutubeChannelModel, YoutubeCronsEnum } from "@efuse/api";

import { EFuseCommand } from "../../command";

export default class SubscribeCrons extends EFuseCommand {
  static description = "Subscribe entities to youtube crons";
  private $youtubeChannel = YoutubeChannelModel;

  async run(): Promise<void> {
    try {
      this.logger.info("Fetching all youtube channels");

      const youtubeChannelCursor = this.$youtubeChannel
        .find({ owner: { $exists: true } }, { owner: 1, ownerKind: 1 })
        .cursor();

      let totalChannels = 0;

      const bulkOperations: Record<string, unknown>[] = [];

      const crons = Object.values(YoutubeCronsEnum);

      for (
        let channel = await youtubeChannelCursor.next();
        channel !== null;
        channel = await youtubeChannelCursor.next()
      ) {
        totalChannels += 1;

        this.logger.info(
          `Preparing entity ${String(channel.owner)} | entityKind: ${channel.ownerKind} to subscribe to cron`
        );

        crons.forEach((cron) => {
          const query = {
            entity: channel.owner,
            kind: CronKindEnum.YOUTUBE,
            entityKind: channel.ownerKind,
            method: cron
          };

          bulkOperations.push({
            updateOne: {
              filter: query,
              update: { ...query, updatedAt: Date.now() },
              upsert: true, // create new object if it is not already there
              setDefaultsOnInsert: true, // this would set the schema defaults
              ordered: false // this would make sure all queries are processed, otherwise mongoose stops processing on first error
            }
          });
        });
      }

      const response = await TrackCron.bulkWrite(bulkOperations);

      this.logger.info(
        { upserted: response.upsertedCount, updated: response.upsertedCount },
        `Subscribed ${totalChannels} youtube channels to cron`
      );

      process.exit();
    } catch (error) {
      this.logger.error(error, "Error while fetching & updating users");
      process.exit();
    }
  }
}
