import { Friend, User } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";
import { LeanDocument, Types } from "mongoose";

import { DataFormatService } from "../../services";
import { EFuseCommand } from "../../command";
import { USER_FIELDS } from "../../constants";

export default class FollowersList extends EFuseCommand {
  public static args = [...EFuseCommand.args, { name: "userId", require: true }];
  public static description = "Retrieve a list of all followers for the specified user.";
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" }),
    service: flags.string({
      char: "s",
      description: "The service to retrieve followers for. Currently only eFuse followers supported.",
      options: ["efuse", "twitch"],
      default: "efuse"
    })
  };

  private formatService: DataFormatService = new DataFormatService(this);

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof FollowersList.flags>;
    const args = this.parsedArgs!;
    this.determineAndListAppropriateFollowers(flags, args).catch((error) =>
      this.logger.error(error, "Error resolving determineAndListAppropriateFollowers promise")
    );
  }

  private async listEfuseFollowers(flags, args): Promise<void> {
    try {
      const followers: LeanDocument<{ follower: Types.ObjectId }>[] = await Friend.find(
        { followee: args.userId },
        { _id: 0 }
      )
        .select("follower")
        .lean();
      const followerIds: Types.ObjectId[] = followers.map((f) => f.follower);

      const selection: string = this.formatService.determineSelectionBasedOnFormat(flags.format, USER_FIELDS);
      const users = await User.find({ _id: { $in: followerIds } })
        .select(selection)
        .lean();
      this.formatService.outputDataBasedOnFormat(flags.format, users, USER_FIELDS);
    } catch (err: any) {
      this.logger.error("Error retrieving Efuse followers", { exit: false });
      this.logger.error(err);
    }
  }

  private async determineAndListAppropriateFollowers(flags, args): Promise<void> {
    if (!args.userId) {
      this.logger.error("A user ID is required.");
    }

    if (!flags.silent) {
      this.logger.info(`Retrieving followers for user ID: ${args.userId}.`);
    }

    try {
      switch (flags.service.toLowerCase()) {
        case "twitch":
          await this.listTwitchFollowers(flags, args);
          break;
        case "efuse":
        default:
          await this.listEfuseFollowers(flags, args);
          break;
      }
      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error retrieving followers", { exit: false });
      this.logger.error(err);
    }
  }

  private async listTwitchFollowers(flags, args): Promise<void> {
    try {
      this.logger.error("Not implemented yet!");
    } catch (err: any) {
      this.logger.error("Error retrieving followers", { exit: false });
      this.logger.error(err);
    }
  }
}
