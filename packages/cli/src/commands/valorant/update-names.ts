import { ValorantCronService } from "@efuse/api";

import { EFuseCommand } from "../../command";

export default class UpdateNames extends EFuseCommand {
  public static description =
    "eFuse command to queue all Valorant Players and check and update game names and taglines";

  public async run(): Promise<void> {
    await ValorantCronService.UpdateValorantGameNamesCron();
  }
}
