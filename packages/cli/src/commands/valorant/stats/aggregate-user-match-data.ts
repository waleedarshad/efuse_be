import { IValorantMatchDTO, IValorantMatchPlayerDTO } from "@efuse/api";

import fs from "fs";

import { EFuseCommand } from "../../../command";
import * as MATCH_DATA from "./match-data.json";

interface PlayerMatchData {
  actId: string;
  agent: string;
  assists: number;
  competitiveTier: number;
  damage: number;
  deaths: number;
  kills: number;
  matchId: string;
  puuid: string;
  timePlayed: number;
  won: boolean;
}
export default class ValorantAggregateUserMatchData extends EFuseCommand {
  static description = "Aggregate hard coded match data. (Note: This is meant as an example and test command.)";

  async run(): Promise<void> {
    const matchData = <IValorantMatchDTO>(<unknown>MATCH_DATA);

    const allPlayerMatchData: PlayerMatchData[] = [];
    matchData.players.forEach((player: IValorantMatchPlayerDTO) => {
      const team = matchData.teams.find((t) => t.teamId === player.teamId);
      const won = team?.won ? true : false; // use ternary because team?.won can be undefined

      // iterate over every match's round results to tally up the damage
      let damage = 0;
      for (const roundResult of matchData.roundResults) {
        // find the right player:
        const roundResultPlayerStats = roundResult.playerStats.find((stats) => stats.puuid === player.puuid);
        if (roundResultPlayerStats) {
          const damageValues = roundResultPlayerStats.damage.map((value) => value.damage);

          const roundDamage = damageValues.reduce((partialSum, value) => partialSum + value, 0);
          damage += roundDamage;
        }
      }

      const playerMatchData: PlayerMatchData = {
        actId: matchData.matchInfo.seasonId,
        agent: player.characterId,
        assists: player.stats.assists,
        competitiveTier: player.competitiveTier,
        damage,
        deaths: player.stats.deaths,
        kills: player.stats.kills,
        matchId: matchData.matchInfo.matchId,
        puuid: player.puuid,
        timePlayed: matchData.matchInfo.gameLengthMillis,
        won
      };

      allPlayerMatchData.push(playerMatchData);
    });

    fs.writeFile("aggregated_stats.json", JSON.stringify(allPlayerMatchData), (error) => {
      if (error) {
        this.logger.error(error);
      } else {
        this.logger.info(
          "Successfully wrote aggregated_stats.json to current working directory (Probably project root)."
        );
      }
    });

    // this.exit(0);
  }
}
