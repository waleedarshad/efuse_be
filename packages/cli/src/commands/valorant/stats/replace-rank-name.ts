import { IValorantStats, ValorantStats, ValorantStatsModel } from "@efuse/api";

import { EFuseCommand } from "../../../command";

export default class ValorantStatsReplaceRankName extends EFuseCommand {
  private readonly model: ValorantStatsModel<IValorantStats> = ValorantStats;

  static description = "Replace the value of a rank name across all Valorant stats with a new one.";

  static args = [
    { name: "oldName", description: "The Valorant rank name to replace." },
    { name: "newName", description: "The new Valorant rank name to use." }
  ];

  async run(): Promise<void> {
    const { args } = this.parse(ValorantStatsReplaceRankName);
    const { oldName, newName } = args;

    if (!oldName) {
      this.logger.error("Bad Request. Original category not found");
      this.exit(1);
    }

    if (!newName) {
      this.logger.error("Bad Request. Original category not found");
      this.exit(1);
    }

    const result = await this.model.updateMany({ competitiveTierName: oldName }, { competitiveTierName: newName });
    this.logger.info(result);
    this.exit(0);
  }
}
