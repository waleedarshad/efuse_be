import { ValorantRegions, ValorantService, ValorantStatsService } from "@efuse/api";

import { EFuseCommand } from "../../../command";

/**
 * This is a command that was written fairly quickly. It could use some improvements such as
 * not using valorant stats to fetch basic info
 */
export default class ValorantStatsRefresh extends EFuseCommand {
  private valorantService = new ValorantService();
  private valorantStatsService = new ValorantStatsService();

  static args = [
    { name: "userId", description: "The players userId", require: true },
    {
      name: "region",
      description: "The valorant region to check for information",
      require: true,
      default: ValorantRegions.NA,
      options: Object.values(ValorantRegions)
    }
  ];

  async run(): Promise<void> {
    const { args } = this.parse(ValorantStatsRefresh);
    const { userId, region } = args;

    const foundValorantStats = await this.valorantStatsService.findOne(
      { user: userId },
      { gameName: true, tagLine: true, puuid: true },
      { lean: true }
    );

    if (!foundValorantStats) {
      this.logger.warn("Could not find valorant info for user");
      this.exit(1);
    }

    const stats = await this.valorantService.retrieveAndSaveValorantStatsForPlayer(
      userId,
      region,
      foundValorantStats.gameName,
      foundValorantStats.tagLine,
      foundValorantStats.puuid
    );

    this.logger.info(stats);
  }
}
