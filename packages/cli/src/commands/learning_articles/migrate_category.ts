import { ILearningArticleCategory, LearningArticle, LearningArticleCategory } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { EFuseCommand } from "../../command";

export default class NewsArticleCategoryMigration extends EFuseCommand {
  public static description = "Command to list learning article categories using slugs";
  public static examples = [`$ efusecli learning_articles:migrate_category featured-podcast esport-business -d`];
  public static args = [{ name: "oldCategory" }, { name: "newCategory" }];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" }),
    deactivate: flags.boolean({
      char: "d",
      description: "deactivates old category",
      default: false
    })
  };

  public async run(): Promise<void> {
    const { args } = this.parse(NewsArticleCategoryMigration);
    const flags = this.parsedFlags as OutputFlags<typeof NewsArticleCategoryMigration.flags>;

    this.migrateArticleCategory(args, flags).catch((error) =>
      this.logger.error(error, "Error resolving migrateArticleCategory promise")
    );
  }

  private async migrateArticleCategory(args, flags): Promise<void> {
    try {
      const oldCategory = <ILearningArticleCategory>await LearningArticleCategory.findOne({ slug: args.oldCategory });

      if (!oldCategory) {
        this.logger.error("Bad Request. Original category not found");
      }

      const newCategory = <ILearningArticleCategory>await LearningArticleCategory.findOne({ slug: args.newCategory });

      if (!newCategory) {
        this.logger.error("Bad Request. Replacement category not found.");
      }

      if (oldCategory._id && newCategory._id) {
        const updatedArticles = await LearningArticle.updateMany(
          { category: oldCategory._id },
          { $set: { category: newCategory._id } }
        );

        if (!updatedArticles) {
          this.logger.error("Unable to update learning category.");
        }

        this.logger.info("Successfully updated all articles with new slug: ", args.newCategory);

        if (flags.deactivate) {
          const inactivatedCategory = <ILearningArticleCategory>(
            (<unknown>(
              await LearningArticleCategory.updateOne({ slug: args.oldCategory }, { $set: { isActive: false } })
            ))
          );

          if (!inactivatedCategory) {
            this.logger.error("Unable to make category inactive");
          }
        }

        this.logger.info("Successfully inactivated old category with slug: ", args.oldCategory);
      } else {
        this.logger.error("One or both categories do not have an id.");
      }

      process.exit(0);
    } catch (error: any) {
      console.error(error);
    }
  }
}
