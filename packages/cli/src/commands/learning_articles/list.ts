import { ILearningArticle, LearningArticle } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";
import { LeanDocument } from "mongoose";

import { DataFormatService } from "../../services";
import { EFuseCommand } from "../../command";

export default class LearningArticlesList extends EFuseCommand {
  public static description = "Command to list learning articles";
  public static examples = [`$ efusecli learning_articles:list`, `$ efusecli learning_articles:list -f json`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof LearningArticlesList.flags>;
    this.listLearningArticles(flags).catch((error) =>
      this.logger.error(error, "Error resolving listLearningArticles promise")
    );
  }

  private async listLearningArticles(flags): Promise<void> {
    try {
      const learningArticles: LeanDocument<ILearningArticle>[] = await LearningArticle.find().lean();

      if (!learningArticles) {
        this.logger.error("Bad request");
      }

      this.formatService.outputDataBasedOnFormat(flags.format, learningArticles);
      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
