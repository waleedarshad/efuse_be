import {
  AlgoliaService,
  LearningArticleService,
  LearningArticleGameService,
  ILearningArticle,
  LearningArticle,
  LearningArticleStatusEnum,
  IPopulatedLearningArticle,
  MongodbService
} from "@efuse/api";
import { QueryCursor } from "mongoose";
import { DOMAIN } from "@efuse/key-store";
import moment from "moment";

import { EFuseCommand } from "../../command";

export default class LearningArticleSyncWithAlgolia extends EFuseCommand {
  static description = "Clears the existing LearningArticle indice & sync all LearningArticles with Algolia";

  private $algoliaService: AlgoliaService = new AlgoliaService();
  private $learningArticleService: LearningArticleService = new LearningArticleService();
  private $learningArticleGameService: LearningArticleGameService = new LearningArticleGameService();

  async run() {
    MongodbService.initialize()
      .then(async () => {
        try {
          await this.manualSyncAll();
          // await this.syncViaService();
          process.exit(0);
        } catch (error) {
          this.logger.error(error, "Error while fetching & syncing learning articles with algolia");

          process.exit();
        }
      })
      .catch((error) => this.logger.error(error, "Error connecting to MongoDB"));
  }

  /**
   * Used to test syncing of a single article
   * @private
   */
  private async syncViaService(): Promise<void> {
    const learningArticlesCursor = (<unknown>(
      LearningArticle.find({ status: LearningArticleStatusEnum.PUBLISHED, slug: "i-have-a-game" }).lean().cursor()
    )) as QueryCursor<ILearningArticle>;

    for (
      let learningArticle = await learningArticlesCursor.next();
      learningArticle !== null;
      learningArticle = await learningArticlesCursor.next()
    ) {
      await this.$learningArticleService.syncWithAlgolia(learningArticle);
      this.logger.info(
        { _id: learningArticle._id, name: learningArticle.title, slug: learningArticle.slug },
        "Synced learning article."
      );
    }
  }

  /**
   * Clears all synced articles and resyncs them all
   * @private
   */
  private async manualSyncAll(): Promise<void> {
    this.logger.info("Fetching published learning articles");

    const learningArticlesCursor = (<unknown>(
      LearningArticle.find({ status: LearningArticleStatusEnum.PUBLISHED })
        .populate("author")
        .populate("category")
        .lean()
        .cursor()
    )) as QueryCursor<IPopulatedLearningArticle>;

    const objects: Record<string, unknown>[] = [];

    for (
      let learningArticle = await learningArticlesCursor.next();
      learningArticle !== null;
      learningArticle = await learningArticlesCursor.next()
    ) {
      this.logger.info(
        {
          _id: learningArticle._id,
          url: `${DOMAIN}/news/${learningArticle.category?.slug}/${learningArticle.slug}`
        },
        "Build learning articles object to sync with algolia"
      );

      if (!learningArticle.author || !learningArticle.category) {
        this.logger.warn("Skipping record as no associated author or category not found");

        continue;
      }

      const formattedAuthor = this.$algoliaService.formatOwner(learningArticle.author, learningArticle.authorType);

      const associatedGames = await this.$learningArticleGameService.getAssociatedGames(learningArticle._id);

      const articleToBeSynced: Record<string, unknown> = {
        ...learningArticle,
        author: formattedAuthor || {},
        publishDateUnix: moment(learningArticle.publishDate).unix(),
        associatedGames
      };

      if (learningArticle.createdAt) {
        articleToBeSynced.createdAtUnix = moment(learningArticle.createdAt).unix();
      }

      if (learningArticle.updatedAt) {
        articleToBeSynced.updatedAtUnix = moment(learningArticle.updatedAt).unix();
      }

      objects.push(articleToBeSynced);
    }

    this.logger.info(`Clearing existing records at indice ${this.$learningArticleService.$index}`);

    await this.$algoliaService.clearObjects(this.$learningArticleService.$index);

    this.logger.info("Cleared records. Now syncing records!");

    await this.$algoliaService.syncObjects(this.$learningArticleService.$index, objects);

    this.logger.info("All records are synced with algolia");
  }
}
