import { Config } from "@efuse/api";
import { Queue, Job } from "bull";

import { EFuseCommand } from "../../command";

const API_LIMITS = {
  max: 3,
  duration: 1000
};

const QUEUE_NAME = "TEST_QUEUE";

export default class QueueTest extends EFuseCommand {
  public static args = [{ name: "arg1" }];
  public static description = "Execute Queue Testing";

  private myQueue: Queue = this.fetchQueue();

  public async run() {
    const { args } = this.parse(QueueTest);

    const jobPromises = this.generateTasks(args.arg1);
    const addedJobs = await Promise.all(jobPromises);

    const finishedPromises = addedJobs.map((promise) => promise.finished());
    const results = await Promise.all(finishedPromises);

    this.logger.info(results);
  }

  private generateTasks(arg: string): Promise<any>[] {
    const promises: Promise<Job<any>>[] = [];
    for (let index = 0; index < 12; index++) {
      const job = this.myQueue.add({ message: `${arg}: Job #${index + 1}` }, { priority: 1 });

      promises.push(job);
    }

    return promises;
  }

  private fetchQueue(): Queue {
    // attempt to get existing queue before creating a new one
    const valQueue: Queue = <Queue>Config.getQueue(QUEUE_NAME);

    if (valQueue) {
      this.myQueue = valQueue;
    } else {
      // create a new queue
      this.myQueue = <Queue>Config.initQueue(QUEUE_NAME, {
        limiter: API_LIMITS,
        defaultJobOptions: { removeOnComplete: true }
      });

      this.myQueue.process((job: Job<QueueParams>) => {
        const time = new Date().toISOString().substr(11, 11);
        this.logger.info(`${time}: ${job.data.message}`);
        return job.data.message;
      });
    }

    return this.myQueue;
  }
}

interface QueueParams {
  message: string;
}
