import { ErenaCronService } from "@efuse/api";

import { EFuseCommand } from "../../command";

export default class UpdateEventStatus extends EFuseCommand {
  static description =
    "Updates the status of any erena events that are scheduled and have started or are active and have ended.";

  async run(): Promise<void> {
    try {
      await ErenaCronService.UpdateErenaEventStatusCron();
    } catch (error) {
      this.logger.error(error);
    }
  }
}
