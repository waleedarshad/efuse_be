import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { EFuseCommand } from "../../command";
import { LeagueService, MongodbService } from "@efuse/api";

export default class AddOrgToLeagues extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "joiningOrgId", description: "The org that is joining the leagues.", require: true },
    { name: "hostOrgId", description: "The org that is hosting the leagues.", require: true }
  ];
  public static description = "Command to give an organization permission to join the host organization's leagues.";
  public static examples = [`$ efusecli leagues:addOrgToLeagues joiningOrgId hostOrgId`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private leaguesService: LeagueService = new LeagueService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof AddOrgToLeagues.flags>;
    const args = this.parsedArgs!;

    MongodbService.initialize()
      .then(async () => {
        await this.addOrgToLeagues(flags, args);
      })
      .catch((error) => {
        this.logger.error(error);
      });
  }

  private async addOrgToLeagues(flags, args): Promise<void> {
    const joiningOrgId: string = args.joiningOrgId;
    const hostOrgId: string = args.hostOrgId;

    try {
      const result = await this.leaguesService.giveOrgLeagueJoinPermissions(joiningOrgId, hostOrgId);

      if (result) {
        this.logger.info("Successfully added org league permissions.");
      } else {
        this.logger.info("Could not add org league permissions.");
      }

      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
