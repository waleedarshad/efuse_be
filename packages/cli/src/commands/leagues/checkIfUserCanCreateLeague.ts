import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { EFuseCommand } from "../../command";
import { MongodbService, OrganizationsService } from "@efuse/api";

export default class CheckIfUserCanCreateLeague extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "userId", description: "The ID of the user to test.", require: true }
  ];
  public static description = "Command to check if a user can create leagues.";
  public static examples = [`$ efusecli leagues:checkIfUserCanCreateLeague userId`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private organizationsService: OrganizationsService = new OrganizationsService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof CheckIfUserCanCreateLeague.flags>;
    const args = this.parsedArgs!;

    MongodbService.initialize()
      .then(async () => {
        await this.canUserCreateLeague(flags, args);
      })
      .catch((error) => {
        this.logger.error(error);
      });
  }

  private async canUserCreateLeague(flags, args): Promise<void> {
    const userId: string = args.userId;

    try {
      const result = await this.organizationsService.canUserCreateLeagues(userId);

      if (result) {
        this.logger.info("User can create leagues!");
      } else {
        this.logger.info("User cannot create leagues.");
      }

      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
