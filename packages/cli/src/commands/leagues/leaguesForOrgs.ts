import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";

import { DataFormatService } from "../../services";
import { EFuseCommand } from "../../command";
import { OrganizationsService, LeagueService, TeamService, MongodbService } from "@efuse/api";

export default class LeaguesForOrgs extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "orgId", description: "What org to fetch leagues for", require: true }
  ];
  public static description = "Command to get leagues with teams for organization.";
  public static examples = [`$ efusecli leagues:leaguesForOrgs myOrgId`, `$ efusecli leagues:leaguesForOrgs myOrgId`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);
  private organizationService: OrganizationsService = new OrganizationsService();
  private leaguesService: LeagueService = new LeagueService();
  private teamsService: TeamService = new TeamService();

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof LeaguesForOrgs.flags>;
    const args = this.parsedArgs!;

    MongodbService.initialize()
      .then(async () => {
        await this.listLeaguesForOrgs(flags, args);
      })
      .catch((error) => {
        this.logger.error(error);
      });
  }

  private async listLeaguesForOrgs(flags, args): Promise<void> {
    const orgId: string = args.orgId;

    try {
      const org = await this.organizationService.findOne(
        { _id: orgId },
        { _id: 1, name: 1, shortName: 1 },
        { lean: true }
      );

      if (!org) {
        this.logger.error("Failed to find organization");
        process.exit(1);
      }
      const leagues = await this.leaguesService.find(
        { owner: org._id },
        { name: 1, state: 1, teams: 1 },
        { lean: true }
      );

      const teamResults: any[] = [];
      const fetchPromises = leagues.map(async (league) => {
        const teams = await this.teamsService.find(
          { _id: { $in: league.teams } },
          { name: 1, owner: 1, createdAt: 1 },
          { lean: 1 }
        );

        const teamsPromises = teams.map(async (team) => {
          const teamResult: any = team;
          const teamOrg = await this.organizationService.findOne(
            { _id: team.owner },
            { name: 1, shortName: 1 },
            { lean: true }
          );

          teamResult.organization = teamOrg?.name;
          teamResult.orgShortName = teamOrg?.shortName;
          teamResult.orgId = teamOrg?._id;
          teamResult.league = league.name;
          teamResult.leagueId = league._id;

          teamResults.push(teamResult);
        });

        await Promise.all(teamsPromises);
      });
      await Promise.all(fetchPromises);

      this.formatService.outputDataBasedOnFormat(flags.format, teamResults);

      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
