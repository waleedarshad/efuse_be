import { PipelineLeaderboardQueueService } from "@efuse/api";
import { flags } from "@oclif/command";

import { EFuseCommand } from "../../command";

export default class LeaderboardsQueue extends EFuseCommand {
  public static description = "Command to queue all pipeline leaderboards for generation";
  public static examples = ["ef leaderboards:queue"];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private leaderboardQueueService: PipelineLeaderboardQueueService = new PipelineLeaderboardQueueService();

  public async run(): Promise<void> {
    this.queueAllPipelineLeaderboards().catch((error) =>
      this.logger.error(error, "Error resolving queueAllPipelineLeaderboards promise")
    );
  }

  private async queueAimlabForGeneration(): Promise<void> {
    try {
      this.logger.info("Queuing Aimlab Leaderboard Generation");
      await this.leaderboardQueueService.generateAndSaveAimlabLeaderboardQueued();

      return void 0;
    } catch (error) {
      this.logger.error("Queuing for Aimlab leaderboard generation failed", error);
    }
  }

  private async queueAllPipelineLeaderboards(): Promise<void> {
    try {
      const aimlabLeaderboard = this.queueAimlabForGeneration();
      const leagueLeaderboard = this.queueLeagueOfLegendsForGeneration();
      const valorantLeaderboard = this.queueValorantForGeneration();

      await Promise.allSettled([aimlabLeaderboard, leagueLeaderboard, valorantLeaderboard]);

      this.logger.info("All leaderboards successfully queued, generated and saved!");
      process.exit(0);
    } catch (error) {
      this.logger.error("Queuing leaderboards for generation failed", error);
      process.exit(2);
    }
  }

  private async queueLeagueOfLegendsForGeneration(): Promise<void> {
    try {
      this.logger.info("Queuing League of Legends Leaderboard Generation");
      await this.leaderboardQueueService.generateAndSaveLeagueOfLegendsLeaderboardQueued();

      return void 0;
    } catch (error) {
      this.logger.error("Queuing for League of Legends leaderboard generation failed", error);
    }
  }

  private async queueValorantForGeneration(): Promise<void> {
    try {
      this.logger.info("Queuing Valorant Leaderboard Generation");
      await this.leaderboardQueueService.generateAndSaveValorantLeaderboardQueued();

      return void 0;
    } catch (error) {
      this.logger.error("Queuing for Valorant leaderboard generation failed", error);
    }
  }
}
