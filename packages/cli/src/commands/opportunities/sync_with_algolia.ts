import {
  AlgoliaService,
  IPopulateOpportunityOwner,
  Opportunity,
  OpportunitySearchService,
  OpportunityStatusEnum,
  UserFields,
  MongodbService
} from "@efuse/api";
import { DOMAIN } from "@efuse/key-store";
import { OwnerKind } from "@efuse/entities";
import { QueryCursor } from "mongoose";
import moment from "moment";

import { EFuseCommand } from "../../command";

export default class OpportunitySyncWithAlgolia extends EFuseCommand {
  static description = "Clears the existing Opportunity indice & sync all opportunities with Algolia";

  private $algoliaService: AlgoliaService = new AlgoliaService();
  private $opportunitySearchService: OpportunitySearchService = new OpportunitySearchService();

  async run(): Promise<void> {
    MongodbService.initialize()
      .then(async () => {
        try {
          this.logger.info("Fetching all visible opportunities");

          const opportunityCursor = (<unknown>(
            Opportunity.find({ status: OpportunityStatusEnum.VISIBLE }, {}, { autopopulate: false })
              .populate({ path: "user", select: UserFields.ALGOLIA_FIELDS_TO_BE_SELECTED })
              .populate("organization")
              .lean()
              .cursor()
          )) as QueryCursor<IPopulateOpportunityOwner>;

          const objects: Record<string, unknown>[] = [];

          for (
            let opportunity = await opportunityCursor.next();
            opportunity !== null;
            opportunity = await opportunityCursor.next()
          ) {
            this.logger.info(
              { _id: opportunity._id, url: `${DOMAIN}/o/${opportunity.shortName}` },
              "Build opportunity object to sync with algolia"
            );

            // Fetch owner
            const ownerType = opportunity.organization ? OwnerKind.ORGANIZATION : OwnerKind.USER;
            const owner = ownerType === OwnerKind.ORGANIZATION ? opportunity.organization : opportunity.user;

            const formattedOwner = this.$algoliaService.formatOwner(owner, ownerType);

            // Build the opportunity object
            const opportunityToBeSynced: Record<string, unknown> = <Record<string, unknown>>{
              ...opportunity,
              user: opportunity.user ? this.$algoliaService.buildUserObject(opportunity.user) : null,
              organization: opportunity.organization,
              author: formattedOwner || {}
            };

            // Converting Dates to unix for algolia sorting
            if (opportunity.dueDate) {
              opportunityToBeSynced.dueDateUnix = moment(opportunity.dueDate).unix();
            }

            if (opportunity.startDate) {
              opportunityToBeSynced.startDateUnix = moment(opportunity.startDate).unix();
            }

            if (opportunity.createdAt) {
              opportunityToBeSynced.createdAtUnix = moment(opportunity.createdAt).unix();
            }

            if (opportunity.updatedAt) {
              opportunityToBeSynced.updatedAtUnix = moment(opportunity.updatedAt).unix();
            }

            objects.push(opportunityToBeSynced);
          }

          this.logger.info(`Clearing existing records at indice ${this.$opportunitySearchService.$index}`);

          await this.$algoliaService.clearObjects(this.$opportunitySearchService.$index);

          this.logger.info("Cleared records. Now syncing records!");

          await this.$algoliaService.syncObjects(this.$opportunitySearchService.$index, objects);

          this.logger.info("All records are synced with algolia");

          process.exit(0);
        } catch (error) {
          this.logger.error("Error occurred while attempting to read from database.", { exit: false });
          this.logger.error(error);

          process.exit(0);
        }
      })
      .catch((error) => this.logger.error(error, "Error connecting to MongoDB"));
  }
}
