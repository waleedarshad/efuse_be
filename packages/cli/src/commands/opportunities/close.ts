import { OpportunityAsyncService, OpportunityService } from "@efuse/api";

import { EFuseCommand } from "../../command";

interface CloseArgs {
  opportunityId: string;
}

export default class OpportunitiesClose extends EFuseCommand {
  public static args = [
    ...EFuseCommand.args,
    { name: "opportunityId", description: "Id of opportunity to close", defualt: null }
  ];
  public static description =
    "Command to close an individual opportunity or to find and close opportunities based on due date";
  public static examples = [`./ef opportunities:close`, `./ef opportunities:close 5dca9b1b5ec4ccbeb19f618d`];

  public async run(): Promise<void> {
    const args = <CloseArgs>this.parsedArgs!;
    if (args && args.opportunityId) {
      await this.closeOpportunity(args.opportunityId);
    } else {
      await this.closeOpportunitiesByDueDate();
    }
  }

  private async closeOpportunity(opportunityId: string): Promise<void> {
    if (!opportunityId) {
      this.logger.error("Opportunity ID must be provided!");
      this.exit(0);
    }

    const oppService = new OpportunityService();
    await oppService.closeOpportunity(opportunityId);

    this.logger.info(`Successfully closed opportunity with ID ${opportunityId}`);
  }

  private async closeOpportunitiesByDueDate(): Promise<void> {
    try {
      const asyncService = new OpportunityAsyncService();
      this.logger.info(`Beginning to find and close opportunities by due date.`);

      await asyncService.findAndQueueOpportunitiesForClosure();

      this.logger.info(`Successfully found and closed opportunities by due date.`);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to close opportunities.", { exit: false });
      this.logger.error(err);
    }
  }
}
