import { IOpportunity, MONGO_PREFERENCE_TAGS, Opportunity } from "@efuse/api";
import { flags } from "@oclif/command";
import { OutputFlags } from "@oclif/parser";
import { cli } from "cli-ux";
import { LeanDocument } from "mongoose";

import { EFuseCommand } from "../../command";
import { DataFormatService } from "../../services";

export default class OpportunitiesList extends EFuseCommand {
  public static description = "Command to list opportunities";
  public static examples = [`$ efusecli opportunities:list`, `$ efusecli opportunities:list -f json`];
  public static flags = {
    ...EFuseCommand.flags,
    help: flags.help({ char: "h" })
  };

  private formatService: DataFormatService = new DataFormatService(this);

  public async run(): Promise<void> {
    const flags = this.parsedFlags as OutputFlags<typeof OpportunitiesList.flags>;
    this.listOpportunities(flags).catch((error) =>
      this.logger.error(error, "Error resolving listOpportunities promise")
    );
  }

  private async listOpportunities(flags): Promise<void> {
    const selection: string = this.formatService.determineSelectionBasedOnFormat(flags.format);

    try {
      cli.action.start("This may take a minute"); // start the spinner
      const opportunities: LeanDocument<IOpportunity>[] = await Opportunity.find({ external: true })
        .select(selection)
        .read("secondary", MONGO_PREFERENCE_TAGS)
        .lean();
      cli.action.stop(); // stop the spinner

      if (!opportunities) {
        this.logger.error("Bad request");
      }

      this.formatService.outputDataBasedOnFormat(flags.format, opportunities);
      process.exit(0);
    } catch (err: any) {
      this.logger.error("Error occurred while attempting to read from database.", { exit: false });
      this.logger.error(err);
    }
  }
}
