/* Important Notes:
 * 1. The logging functions defined in the Command abstract class are being deprecated to ensure
 *    that we have uniform usage of the custom logger. They were originally being overridden but
 *    that caused some confusion with naming and made the downstream code more obtuse than necessary
 *    with potentially multiple functions being used doing the same thing.
 * 2. The debug function is not being overridden because it works a little differently, using an
 *    external library that only runs when DEBUG=* is set on the CLI prior to the call. If we desire
 *    to hook into this in the future for our own debug function we should override that one too.
 * */

import Command, { flags } from "@oclif/command";
import { IConfig } from "@oclif/config";
import { PrettyPrintableError } from "@oclif/errors";
import * as Parser from "@oclif/parser";

import { Logger } from "./utils/logger";

/**
 * Base class that wraps up some standard functionality shared between all of our eFuse commands.
 */
export abstract class EFuseCommand extends Command {
  static args: Parser.args.Input = [];
  static flags: flags.Input<any> = {
    format: flags.string({
      description: "The data format to use for the output.",
      options: ["csv", "json", "human"],
      default: "csv"
    }),
    silent: flags.boolean({ description: "Silences stdout/stderr output." }),
    verbose: flags.boolean({ char: "v", description: "Shows additional information." })
  };

  /* Workaround for oclif being built for JavaScript first. It expects you to assign to the static
   * object which is fine in JavaScript but a no-no in TypeScript. When accessing this from the derived
   * class, do so like this:
   *  const { flags } = this.parsedFlags as OutputFlags<typeof Derived.flags>;
   *  const { args } = this.parsedArgs!;
   *
   * See here for more info:
   *    https://github.com/oclif/oclif/issues/225#issuecomment-574484114 */
  protected parsedArgs?: Parser.OutputArgs<any>;
  protected parsedFlags?: Parser.OutputFlags<typeof EFuseCommand.flags>;
  protected logger: Logger;

  constructor(argv: string[], config: IConfig) {
    super(argv, config);

    this.logger = new Logger();
  }

  /**
   * Logs the message to the stdout output without decorating it (bypassing the logger).
   * Use this function to write raw data to the CLI as command output.
   * @param message
   * @param args
   */
  echo(message?: string, ...args: any[]): void {
    super.log(message, ...args);
  }

  /**
   * Logs the message to the stdout output.
   * @deprecated Use logger.info() for logging informational messages or echo() for outputting raw data.
   * @param {string} message
   * @param args
   */

  log(message?: string, ...args: any[]): void {
    throw new Error(
      "Decommissioned. Use logger.info() for logging informational messages or echo() for outputting raw data."
    );
  }

  /**
   * Logs the input to the stderr output as a warning.
   * @deprecated Use logger.warn() instead.
   * @param input
   */

  warn(input: string | Error): void {
    throw new Error("Decommissioned. Use logger.warn() instead.");
  }

  /**
   * Logs the input to the stderr output.
   * @deprecated Use logger.error() instead.
   * @param input
   * @param options
   */
  error(input: string | Error, options: { code?: string; exit: false } & PrettyPrintableError): void;
  /**
   * Logs the input to the stderr output.
   * @deprecated Use logger.error() instead.
   * @param input
   * @param options
   */
  error(input: string | Error, options?: { code?: string; exit?: number } & PrettyPrintableError): never;
  /**
   * Logs the input to the stderr output.
   * @deprecated Use logger.error() instead.
   * @param input
   * @param options
   */

  error(
    input: string | Error,
    options: { code?: string; exit?: number | false } & PrettyPrintableError = {}
  ): void | never {
    throw new Error("Decommissioned. Use logger.error() instead.");
  }

  /**
   * Initializes the command properties and ancillary functionality.
   * @returns
   */
  async init(): Promise<any> {
    const { args, flags } = this.parse(this.constructor as Parser.Input<typeof EFuseCommand.flags>);

    this.parsedArgs = args;
    this.parsedFlags = flags;

    if (this.parsedFlags.silent) {
      this.logger.silence();
    }

    return super.init();
  }

  /**
   * Catches any errors.
   * @param err
   * @returns
   */
  async catch(err: any): Promise<any> {
    return super.catch(err);
  }

  /**
   * Called automatically regardless of whether or not an error occurred.
   * @param err
   * @returns
   */
  async finally(err: Error | undefined): Promise<any> {
    return super.finally(err);
  }
}
