import fs from "fs";
import path from "path";

export function findWorkspaceRoot(initial: string) {
  if (!initial) {
    initial = process.cwd();
  }

  let previous: string | null = null;
  let current: string = path.normalize(initial);

  do {
    const manifest = readPackageJSON(current);
    const workspaces = extractWorkspaces(manifest);

    if (workspaces) {
      return current;
    }

    previous = current;
    current = path.dirname(current);
  } while (current !== previous);

  return null;
}

function extractWorkspaces(manifest) {
  const workspaces = (manifest || {}).workspaces;

  return (workspaces && workspaces.packages) || (Array.isArray(workspaces) ? workspaces : null);
}

function readPackageJSON(dir) {
  const file = path.join(dir, "package.json");

  if (fs.existsSync(file)) {
    return JSON.parse(fs.readFileSync(file, "utf8"));
  }

  return null;
}
