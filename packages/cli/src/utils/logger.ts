/* This has two purposes. The first is to act as a wrapper so we can swap logging services in the
 * future if necessary. The second is to extend the pino logger with the capability to toggle all
 * logging to be silent so that it works with a --silent flag. Unfortunately due to the way Pino is
 * built, being a JavaScript-first library, extending it is impossible and the workaround for this
 * used in the existing BE logging class does nothing to allow us to add this new functionality.
 * */

import pino, { LogFn } from "pino";

/**
 * This class wraps up the logging functionality for the eFuse CLI.
 */
export class Logger {
  protected logger: pino.Logger;
  private silenced: boolean;

  constructor(logger?: pino.Logger, silent?: boolean) {
    this.logger =
      logger ??
      pino({
        name: "efusecli",
        prettyPrint: { colorize: true }
      });
    this.silenced = silent ?? false;
  }

  /**
   * Silences the output.
   * @returns
   */
  silence(): Logger {
    this.silenced = true;

    return this;
  }

  /**
   * Un-silences the output.
   * @returns
   */
  louden(): Logger {
    this.silenced = false;

    return this;
  }

  /**
   * Output a debug message.
   * @param msg
   * @param args
   */
  debug(msg: string, ...args: any[]): Logger;
  debug<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  debug<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.debug.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * Output an error message.
   * @param msg
   * @param args
   */
  error(msg: string, ...args: any[]): Logger;
  error<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  error<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.error.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * Output a message indicating a fatal error.
   * @param msg
   * @param args
   */
  fatal(msg: string, ...args: any[]): Logger;
  fatal<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  fatal<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.fatal.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * Output a log message.
   * @param msg
   * @param args
   */
  info(msg: string, ...args: any[]): Logger;
  info<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  info<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.info.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * Output nothing, a noop method.
   * @param msg
   * @param args
   */
  silent(msg: string, ...args: any[]): Logger;
  silent<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  silent<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.silent.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * Output a trace message.
   * @param msg
   * @param args
   */
  trace(msg: string, ...args: any[]): Logger;
  trace<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  trace<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.trace.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * Output a warning message.
   * @param msg
   * @param args
   */
  warn(msg: string, ...args: any[]): Logger;
  warn<T extends object>(obj: T, msg?: string, ...args: any[]): Logger;
  warn<T extends object>(obj: T | string, msg?: string, ...args: [any]) {
    this.disambiguateLogMethod(this.logger.warn.bind(this.logger), obj, msg, ...args);

    return this;
  }

  /**
   * This method is a generic way to handle determing which method signature to call, properly
   * passing the args, and silencing the output of all commands if the logger service is silenced.
   * @private
   * @param fn
   * @param obj
   * @param msg
   * @param args
   */
  private disambiguateLogMethod<T extends object>(fn: LogFn, obj: T | string, msg?: string, ...args: any[]): void {
    const logFn = this.silenced ? this.logger.silent : fn;

    if (typeof obj === "object") {
      logFn(obj, msg, args);
    } else {
      logFn(obj, [msg, ...args]);
    }
  }
}
