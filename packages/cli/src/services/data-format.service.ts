import { cli } from "cli-ux";
import { Parser } from "json2csv";

import { EFuseCommand } from "../command";

enum FormatOptions {
  CSV = "csv",
  HUMAN = "human",
  JSON = "json"
}

export class DataFormatService {
  private command: EFuseCommand;

  constructor(command: EFuseCommand) {
    this.command = command;
  }

  /**
   * Returns a string to be used in the selection function on a mongoose call
   * Note: not necessary if trying to retrieve entire object and not specific fields of it.
   *
   * EX: User.find().selection(formatService.determineFormatForSelection(format, fields))
   *
   * @param format The flag the user inputs - csv, human, or json
   * @param fieldsToRetrieve An optional array of strings that say what fields should be retrieved for the call. Only used for CSV or Human Readable, ignored for json. Make sure the fields match with variables in the data you expect
   *
   * Ex: fieldsToRetrieve = [_id] and user._id
   */
  public determineSelectionBasedOnFormat(format: string | undefined, fieldsToRetrieve?: string[]): string {
    const defaultFormat: string = fieldsToRetrieve && fieldsToRetrieve.length > 0 ? fieldsToRetrieve.join(" ") : "";
    if (!format) {
      return defaultFormat;
    }

    switch (format.toLowerCase()) {
      case FormatOptions.CSV:
        return defaultFormat;
      case FormatOptions.HUMAN:
        return defaultFormat;
      case FormatOptions.JSON:
        return "";
      default:
        return defaultFormat;
    }
  }

  /**
   * Outputs the data as csv, human or json and adjusts the output based on format and fields
   *
   * EX: User.find().selection(formatService.determineFormatForSelection(format, fields))
   *
   * @param format the flag the user inputs - csv, human, or json
   * @param data the data that is meant to be displayed in console. Note: must be passed in as array
   * @param fieldsToShow  An optional array of strings that say what fields should be output. Only used for CSV or Human Readable, ignored for json. Make sure the fieldsToShow match with variables in the data you expect
   *
   * Ex: fields = [_id] and user._id
   */
  public outputDataBasedOnFormat(format: string | undefined, data: any[], fieldsToShow?: string[]): void {
    const dataKeys: string[] = data && data.length > 0 ? Object.keys(data[0]) : [];
    const fields: string[] = fieldsToShow && fieldsToShow.length > 0 ? fieldsToShow : dataKeys;

    if (!format) {
      this.createCsvAndLog(data, fields);

      return void 0;
    }

    switch (format.toLowerCase()) {
      case FormatOptions.CSV:
        this.createCsvAndLog(data, fields);
        break;
      case FormatOptions.HUMAN:
        this.createCliTable(data, fields);
        break;
      case FormatOptions.JSON:
        this.command.echo(JSON.stringify(data));
        break;
      default:
        this.createCsvAndLog(data, fields);
        break;
    }
  }

  /**
   * Displays the data based on fields in a more human readable way.
   *
   * @param data the data that is meant to be displayed in console
   * @param fields an array of variable keys. make sure the fields match with variables in the data you expect
   *
   * Ex: fields = [_id] and user._id
   *
   */
  private createCliTable(data: any[], fields: string[]): void {
    const adjustedFields: any = {};
    fields.forEach((field: string) => {
      adjustedFields[field] = "";
    });

    this.command.echo(""); // spacing
    cli.table(data, adjustedFields);
    this.command.echo(""); // spacing
  }

  /**
   * Parses and displays the data based on fields.
   *
   * @param data the data that is meant to be displayed in console
   * @param fields an array of variable keys. make sure the fields match with variables in the data you expect
   *
   * Ex: fields = [_id] and user._id
   *
   */
  private createCsvAndLog(data: any[], fields: string[]): void {
    const parser = new Parser({ fields });
    const csv = parser.parse(data);
    this.command.echo(csv);
  }
}
