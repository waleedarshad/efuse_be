# @efuse/cli

eFuse CLI

[![oclif](https://img.shields.io/badge/cli-oclif-brightgreen.svg)](https://oclif.io)
[![Version](https://img.shields.io/npm/v/efusecli.svg)](https://npmjs.org/package/efusecli)
[![Downloads/week](https://img.shields.io/npm/dw/efusecli.svg)](https://npmjs.org/package/efusecli)
[![License](https://img.shields.io/npm/l/efusecli.svg)](https://github.com/eFuse-Inc/eFuse_BE/blob/master/package.json)

<!-- toc -->

- [Usage](#usage)
- [Commands](#commands)

* [efusecli](#efusecli)
<!-- tocstop -->

# Usage

<!-- usage -->

```sh-session
$ npm install -g efusecli
$ efusecli COMMAND
running command...
$ efusecli (-v|--version|version)
efusecli/0.0.0 darwin-x64 node-v15.14.0
$ efusecli --help [COMMAND]
USAGE
  $ efusecli COMMAND
...
```

<!-- usagestop -->

# Commands

<!-- commands -->

- [`efusecli followers:list [USER_ID]`](#efusecli-followerslist-file)
- [`efusecli help [COMMAND]`](#efusecli-help-command)

* [`efusecli opportunities:list [FILE]`](#efusecli-opportunities-list-file)
* [`efusecli users:list [COMMAND]`](#efusecli-users-list-command)

## `efusecli applicants:list [COMMAND]`

Command to list applicants of an opportunity

```
USAGE
  $ efusecli applicants:list [COMMAND]

ARGUMENTS
  opportunityId the id of a opportunity to find their applicants

OPTIONS
  -h, --help                        show CLI help
  -v, --verbose                     Shows additional information.
  --console-log-format=normal|pino  [default: normal] Determines the format to use for messages logged to the console.
  --format=csv|json|human           [default: csv] The data format to use for the output.
  --silent                          Silences stdout/stderr output.

```

_See code: [src/commands/applicants/list.ts](https://github.com/eFuse-Inc/eFuse_BE/blob/v0.0.0/src/commands/applicants/list.ts)_

## `efusecli followers:list [USER_ID]`

display help for efusecli

```
USAGE
  $ efusecli followers:list [USER_ID]

ARGUMENTS
  userId the id of a user to find their followers

OPTIONS
  -h, --help                        show CLI help
  -s, --service=efuse|twitch        [default: efuse] The service to retrieve followers for. Currently only eFuse followers supported.
  -v, --verbose                     Shows additional information.
  --console-log-format=normal|pino  [default: normal] Determines the format to use for messages logged to the console.
  --format=csv|json|human           [default: csv] The data format to use for the output.
  --silent                          Silences stdout/stderr output.
```

_See code: [src/commands/followers/list.ts](https://github.com/eFuse-Inc/eFuse_BE/blob/v0.0.0/src/commands/followers/list.ts)_

## `efusecli learning_articles:list [COMMAND]`

Command to list learning articles

```
USAGE
  $ efusecli learning_articles:list [COMMAND]

OPTIONS
  -h, --help                        show CLI help
  -v, --verbose                     Shows additional information.
  --console-log-format=normal|pino  [default: normal] Determines the format to use for messages logged to the console.
  --format=csv|json|human           [default: csv] The data format to use for the output.
  --silent                          Silences stdout/stderr output.

```

_See code: [src/commands/learning_articles/list.ts](https://github.com/eFuse-Inc/eFuse_BE/blob/v0.0.0/src/commands/learning_articles/list.ts)_

## `efusecli help [COMMAND]`

display help for efusecli

```
USAGE
  $ efusecli help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
  -f, --force
  -h, --help       show CLI help
  -n, --name=name  name to print
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.2/src/commands/help.ts)_

## `efusecli opportunities:list [FILE]`

Command to interact with the opportunities database

```
USAGE
  $ efusecli opportunities:list [FILE]

OPTIONS
  -h, --help                   show CLI help
  -v, --verbose                     Shows additional information.
  --console-log-format=normal|pino  [default: normal] Determines the format to use for messages logged to the console.
  --format=csv|json|human           [default: csv] The data format to use for the output.
  --silent                          Silences stdout/stderr output.

EXAMPLE
  $ efusecli opportunities:list
  $ efusecli opportunities:list -f json
```

_See code: [src/commands/opportunities/list.ts](https://github.com/eFuse-Inc/eFuse_BE/blob/v0.0.0/src/commands/opportunities/list.ts)_

## `efusecli users:list [COMMAND]`

eFuse command to list users or give more specific list of users based on flags

```
USAGE
  $ efusecli users:list [COMMAND]

OPTIONS
  -e, --email=email            Lookup users by their email
  -h, --help                   show CLI help
  -i, --id=id                  Lookup users by their user id
  -n, --username=username      Lookup users by their username
  -v, --verbose                     Shows additional information.
  --console-log-format=normal|pino  [default: normal] Determines the format to use for messages logged to the console.
  --format=csv|json|human           [default: csv] The data format to use for the output.
  --silent                          Silences stdout/stderr output.
```

_See code: [src/commands/users/list.ts](https://github.com/eFuse-Inc/eFuse_BE/blob/v0.0.0/src/commands/users/list.ts)_

## `efusecli valorant:leaderboard:generate [COMMAND]`

Command to generate and update valorant pipeline leaderboard

```
USAGE
  $ efusecli valorant:leaderboard:generate [COMMAND]


OPTIONS
  -h, --help                        show CLI help
  -v, --verbose                     Shows additional information.
  --console-log-format=normal|pino  [default: normal] Determines the format to use for messages logged to the console.
  --format=csv|json|human           [default: csv] The data format to use for the output.
  --silent                          Silences stdout/stderr output.

```

_See code: [src/commands/valorant/leaderboard/generate.ts](https://github.com/eFuse-Inc/eFuse_BE/blob/v0.0.0/src/commands/leaderboard/generate.ts)_

<!-- commandsstop -->
