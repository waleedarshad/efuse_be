#!/usr/bin/env node

const command = require("@oclif/command");
const flush = require("@oclif/command/flush");
const errorHandler = require("@oclif/errors/handle");

command
  .run()
  .then(flush)
  // @ts-ignore
  .catch(errorHandler);
