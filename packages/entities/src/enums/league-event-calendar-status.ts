export enum LeagueEventCalendarStatus {
  CURRENT = "CURRENT",
  PAST = "PAST",
  UPCOMING = "UPCOMING"
}
