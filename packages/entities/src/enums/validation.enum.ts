export enum EmailValidationErrorEnum {
  REQUIRED = "Email is required",
  INVALID = "Please enter a valid email address",
  TAKEN = "Email has already been taken"
}

export enum UsernameValidationErrorEnum {
  REQUIRED = "Username is required",
  ONLY_ALPHA_NUMERIC = "Only alphanumeric characters are allowed in username",
  ATLEAST_4_CHARACTERS = "Username must be at least 4 characters",
  TAKEN = "Username has already been taken",
  SEXUAL_OR_OFFENSIVE = "Sexual or offensive words are not allowed",
  INVALID_CHARACTERS = "Username has invalid characters. Allowed: a-Z, 0-9, _"
}
