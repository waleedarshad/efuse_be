export enum FavoriteRelatedModel {
  APPLICANTS = "applicants",
  RECRUITMENT_PROFILES = "recruitmentprofiles"
}
