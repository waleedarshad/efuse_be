export enum LeagueScoreCompetitorType {
  TEAM = "team",
  TEAM_MEMBER = "teamMember"
}
