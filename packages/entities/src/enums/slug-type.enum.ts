export enum SlugTypeEnum {
  OPPORTUNITY = "opportunities",
  LEARNING_ARTICLE = "learningarticles",
  ORGANIZATION = "organizations",
  ERENA_TOURNAMENT = "erenatournaments"
}
