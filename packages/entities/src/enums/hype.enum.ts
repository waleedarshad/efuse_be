export enum HypeEnum {
  FEED = "feeds",
  COMMENT = "comments"
}

export enum HypePaginationLimitEnum {
  DEFAULT = 10,
  MAX = 25
}
