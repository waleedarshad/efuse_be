export enum PublishStatus {
  CLOSED = "closed",
  OPEN = "open",
  PUBLISHED = "Published",
  VISIBLE = "visible"
}
