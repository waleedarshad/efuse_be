export enum LeagueJoinStatus {
  JOINED = "joined",
  PENDING = "pending",
  JOINED_AND_PENDING = "joinedAndPending"
}
