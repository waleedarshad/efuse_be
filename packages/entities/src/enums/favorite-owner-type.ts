export enum FavoriteOwnerType {
  ORGANIZATIONS = "organizations",
  USERS = "users"
}
