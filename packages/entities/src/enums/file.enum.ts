export enum MindBlownImageEnum {
  filename = "mindblown_white.png",
  contentType = "image/png",
  url = "https://cdn.efuse.gg/uploads/static/global/mindblown_white.png"
}

export enum BriefcaseImageEnum {
  filename = "briefcase.png",
  contentType = "image/png",
  url = "https://efuse.gg/static/images/briefcase.png"
}

export enum NoImageEnum {
  filename = "no_image.jpg",
  contentType = "image/jpg",
  url = "https://efuse.gg/static/images/no_image.jpg"
}
