// TODO: Should this just be named entry type so that it can be used for tournaments as well?

// These match tournament types from traditional sports
//  * Closed tournaments are like Open tournaments except restrictions other than point qualifications
//  are required. For instance, they may only allow players from a certain region.
//  * Invitational tournaments are by invitation only.
//  * Open tournaments are open for anyone who qualifies to join.
export enum LeagueEntryType {
  CLOSED = "closed",
  INVITATIONAL = "invitational",
  OPEN = "open"
}
