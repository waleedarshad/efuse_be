export enum OAuthServiceKind {
  GOOGLE = "google",
  TWITCH = "twitch",
  TWITTER = "twitter",
  STATESPACE = "statespace",
  RIOT = "riot",
  VALORANT = "valorant"
}
