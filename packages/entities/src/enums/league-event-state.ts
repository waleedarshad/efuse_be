export enum LeagueEventState {
  ACTIVE = "active",
  CANCELLED = "cancelled",
  FINISHED = "finished",
  PENDING = "pending",
  PENDING_POOLS = "pendingPools",
  PENDING_TEAMS = "pendingTeams",
  PENDING_BRACKET = "pendingBracket"
}
