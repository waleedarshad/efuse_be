export enum ChangeStreamOperationTypeEnum {
  DELETE = "delete",
  INSERT = "insert",
  RENAME = "rename",
  REPLACE = "replace",
  UPDATE = "update"
}
