export enum LeagueMatchState {
  ACTIVE = "active",
  BYE = "bye",
  CANCELLED = "cancelled",
  FINISHED = "finished",
  PENDING = "pending"
}
