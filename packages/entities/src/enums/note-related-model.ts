export enum NoteRelatedModel {
  APPLICANTS = "applicants",
  RECRUITMENT_PROFILES = "recruitmentprofiles"
}
