export enum PlatformEnum {
  BATTLENET = "battlenet",
  PSN = "psn",
  XBL = "xbl",
  STEAM = "steam"
}
