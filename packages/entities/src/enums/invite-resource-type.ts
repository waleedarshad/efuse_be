export enum InviteResourceType {
  LEAGUE = "LEAGUE",
  LEAGUE_EVENT = "LEAGUE_EVENT",
  TEAM = "TEAM"
}
