export enum OpportunityType {
  EVENT = "Event",
  JOB = "Job",
  OTHER = "Other",
  SCHOLARSHIP = "Scholarship",
  TEAM_OPENING = "Team Opening"
}
