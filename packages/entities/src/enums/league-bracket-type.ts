export enum LeagueBracketType {
  POINT_RACE = "pointRace",
  SINGLE_ELIM = "singleElim",
  DOUBLE_ELIM = "doubleElim",
  ROUND_ROBIN = "roundRobin",
  SWISS = "swiss"
}
