export enum OrganizationValidationErrorsEnum {
  NAME = "Name is required",
  SCALE = "Scale is required",
  ORGANIZATION_TYPE = "Organization type is required",
  STATUS = "Status is required",
  DESCRIPTION = "Description is required",
  PHONE_NUMBER = "Please add phone number"
}
