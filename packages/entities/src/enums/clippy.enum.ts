export enum ClippyCommandKindEnum {
  SHOUTOUT = "shoutout",
  MY_CLIPS = "myClips",
  EFUSE_SHOWCASE = "eFuseShowcase",
  SOCIAL_SHARING = "socialSharing",
  PROMOTIONAL_CLIPS = "promotionalClips",
  STOP = "stop",
  STOP_ALL = "stopAll",
  WATCH = "watch",
  MUTE = "mute",
  REPLAY = "replay",
  UNMUTE = "unmute",
  RESET = "reset"
}

export enum ClippySortOrderEnum {
  TOP = "top",
  RANDOM = "random"
}

export enum ClippyDateRangeEnum {
  DAY = "day",
  WEEK = "week",
  MONTH = "month",
  YEAR = "year",
  ALL = "all"
}

export enum ClippyAllowedRolesEnum {
  BROADCASTER = "broadcaster",
  MODERATOR = "mod",
  SUBSCRIBER = "subscriber",
  VIP = "vip",
  EVERYONE = "everyone"
}

export enum ClippyDefaultCommandEnum {
  SHOUTOUT = "so",
  MUTE = "mute",
  WATCH = "watch",
  STOP = "stop",
  STOP_ALL = "stopAll",
  REPLAY = "replay",
  EFUSE_SHOWCASE = "eFuse",
  MY_CLIPS = "clippy",
  UNMUTE = "unmute",
  RESET = "reset"
}

export enum ClippySegmentEventEnum {
  USER_INITIALIZED = "CLIPPY_USER_INITIALIZED",
  CLIP_COUNT_RETRIEVED = "CLIPPY_CLIP_COUNT_RETRIEVED"
}
