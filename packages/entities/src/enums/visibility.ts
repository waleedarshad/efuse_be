export enum Visibility {
  DRAFT = "draft",
  HIDDEN = "hidden",
  VISIBLE = "visible"
}
