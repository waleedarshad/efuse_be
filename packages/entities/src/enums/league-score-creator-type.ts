export enum LeagueScoreCreatorType {
  ORGANIZATIONS = "organizations",
  USERS = "users",
  TEAMS = "teams"
}
