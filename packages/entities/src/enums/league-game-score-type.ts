export enum LeagueGameScoreType {
  CONFLICT = "conflict",
  FINALIZED = "finalized",
  SUBMITTED = "submitted"
}
