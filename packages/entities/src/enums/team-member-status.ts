export enum TeamMemberStatus {
  ACTIVE = "active",
  INACTIVE = "inactive",
  PENDING = "pending"
}
