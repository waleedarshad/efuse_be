export enum MediaProfileEnum {
  SD = "sd",
  HD = "hd",
  UHD = "uhd",
  HLS = "hls"
}
