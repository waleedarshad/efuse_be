export enum NoteOwnerType {
  ORGANIZATIONS = "organizations",
  USERS = "users"
}
