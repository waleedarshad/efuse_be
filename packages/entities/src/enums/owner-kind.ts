export enum OwnerKind {
  ORGANIZATION = "organizations",
  USER = "users"
}
