export enum StreakName {
  DAILY_COMMENT = "dailyComment",
  DAILY_USAGE = "dailyUsage"
}
