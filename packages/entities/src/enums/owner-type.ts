export enum OwnerType {
  ORGANIZATIONS = "organizations",
  USERS = "users"
}
