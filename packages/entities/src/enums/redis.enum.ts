export enum RedisRecommendationsEnum {
  USERS = "recommendations:users",
  ORGANIZATIONS = "recommendations:organizations",
  OPPORTUNITIES = "recommendations:opportunities",
  LEARNING = "recommendations:learning"
}
