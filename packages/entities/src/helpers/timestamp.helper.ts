import { Schema } from "mongoose";

export function timestamps(schema: Schema<any>): void {
  const { typeKey } = (<any>schema).options;

  schema.add({ createdAt: { [typeKey]: Date, default: Date.now, index: true } });
  schema.add({ updatedAt: { [typeKey]: Date, index: true } });

  function setTimestamp(this: any, next: Function): void {
    this.updatedAt = Date.now();
    next();
  }

  schema.pre("save", setTimestamp);
  schema.pre("update", setTimestamp);
  schema.pre("updateOne", setTimestamp);
  schema.pre("findOneAndUpdate", setTimestamp);
}
