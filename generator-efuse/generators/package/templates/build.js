#!/usr/bin/env node

const { build } = require("esbuild");
const { esbuildDecorators } = require("@anatine/esbuild-decorators");
const { nodeExternalsPlugin } = require("esbuild-node-externals");
const globby = require("globby");

const ROOT = "packages/<%= name %>";
const ENTRY_POINTS = globby.sync([
  `${ROOT}/src/**/*`,
  `!${ROOT}/src/**/*.spec.ts`,
  `!${ROOT}/**/__mocks__/**`,
  `!${ROOT}/**/__tests__/**`
]);

build({
  bundle: false,
  entryPoints: [`${ROOT}/index.ts`, ...ENTRY_POINTS],
  external: [],
  format: "cjs",
  outdir: `${ROOT}/dist`,
  platform: "node",
  plugins: [
    nodeExternalsPlugin({ packagePath: ["./package.json", `${ROOT}/package.json`] }),
    esbuildDecorators({ cwd: process.cwd(), force: false, tsconfig: `${ROOT}/tsconfig.json`, tsx: false })
  ],
  target: ["ES2020"],
  tsconfig: `${ROOT}/tsconfig.json`
}).catch((error) => {
  console.error(error);
});
