module.exports = {
  displayName: "@efuse/<%= name %>",
  globals: { "esbuild-jest": { tsconfig: "./tsconfig.spec.json" } },
  haste: { hasteImplModulePath: require.resolve(`${process.cwd()}/scripts/jest/no-haste.js`) },
  moduleFileExtensions: ["js", "json", "ts"],
  name: "<%= name %>",
  setupFiles: [require.resolve(`${process.cwd()}/setup-test-environment.ts`)],
  setupFilesAfterEnv: ["jest-extended", "jest-chain", require.resolve("./setup-tests.ts")],
  testEnvironment: "node",
  testPathIgnorePatterns: ["/bin/", "/dist/", "/node_modules/"],
  testRegex: "/__tests__/[^/]*([^d]\\.ts)$",
  transform: { "^.+\\.tsx?$": "esbuild-jest" },
  transformIgnorePatterns: ["node_modules/(?!(@efuse)/)"]
};
