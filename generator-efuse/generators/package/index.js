const Generator = require("yeoman-generator");
const path = require("path");
const extend = require("deep-extend");

module.exports = class extends Generator {
  constructor(args, opts) {
    super(args, opts);

    // This makes `name` a required argument.
    this.argument("name", { type: String, required: true });
  }

  writing() {
    // Does nothing currently. Yeoman has this extremely stupid "feature" where it runs every method
    // added to the Generator prototype. More info here:
    //    https://yeoman.io/authoring/
  }

  /**
   * Copies the template files to the new package directory.
   */
  copyTemplate() {
    this.fs.copyTpl(this.templatePath("*"), this.destinationPath(path.join("packages", this.options.name)), {
      name: this.options.name
    });
  }

  /**
   * Add build scripts to the package.json file.
   */
  updatePackage() {
    const file = "package.json";
    const pkg = this.fs.readJSON(this.destinationPath(file), {});

    // TODO:
    //  1. This assumes there's already a package there. If we want to be able to reuse this for
    //     other projects we should ensure it handles the case of this being the first package added.
    //  2. This should ideally sort scripts alphabetically so users don't have to manually do it.
    extend(pkg, {
      scripts: {
        // Script for building just the new package.
        [`build.${this.options.name}`]: `./packages/${this.options.name}/build.js && tsc --build ./packages/${this.options.name}`,
        // Script for building all packages.
        "build.packages": `${pkg.scripts["build.packages"]} & yarn build.${this.options.name}`
      }
    });

    this.fs.writeJSON(this.destinationPath(file), pkg);
  }

  /**
   * Updates the tconfig.json file to include a reference to the new package.
   */
  updateTsconfig() {
    const file = "tsconfig.json";
    const tsconfig = this.fs.readJSON(this.destinationPath(file), {});

    tsconfig.references.push({
      path: `./packages/${this.options.name}/tsconfig.build.json`
    });

    tsconfig.references = tsconfig.references.sort((a, b) => a.path.localeCompare(b.path));

    // TODO: This currently outputs the JSON in a fully unfolded state, which is kind of ugly. We
    //       should see if it is possible to prettify this and match the old, clean style.
    this.fs.writeJSON(this.destinationPath(file), tsconfig);
  }
};
