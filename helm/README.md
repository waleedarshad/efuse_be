# BE Helm chart

This chart is used to deploy BE in a sane, and repeatable way.

```
helm upgrade --install be . --set image.tag=<SHA> --set ingress.enabled=true --set fqdn=<FQDN> --set beService=be
helm upgrade --install async . --set image.tag=<SHA> --set beService=async
helm upgrade --install cron . --set image.tag=<SHA> --set beService=cron
helm upgrade --install cron-async . --set image.tag=<SHA> --set beService=async
helm upgrade --install mongodb-watcher . --set image.tag=<SHA> --set beService=mongodb-watcher
```

## Defaults
Rather than create a generically useful chart for all possible scenarios, this chart makes several assumptions.

By default, only the core BE service is deployed. No ingress is enabled unless the operator specifically enables it. An ingress requires defining an FQDN, which will be registered in DNS. The operator may elect to have the DNS record proxied in CloudFlare.

Requests:
* cpu: 500 millicores (0.5 vCPU)
* memory: 1 gibibytes

Limits:
* cpu: 2000 millicores (2 vCPU)
* memory: 2 gibibytes

## eFuse settings
This chart adds several new options to the standard array of Helm values
* **albCertARN**: the ARN of an ACM certificate to attach to the ingress
* **beService**: which BE service to deploy: be, async, cron, or mongodb-watcher
* **certManager**: if defined, the name of the Cluster Issuer from which to request a certificate. Default: empty.
* **ddEnv**: the DataDog environment to define
* **ddService**: the DataDog service to define (default: value of beService)
* **efusePreview**: whether this is a preview deploy (default: false)
* **externalDNS**: whether to annotate the ingress with for external-dns registration. Default: false.
* **cloudflareProxied**: whether to use Cloudflare Proxy for the DNS.  Default: true.
* **fqdn**: the FQDN to use for the ingress and TLS cert. Mandatory, no default defined.
* **nodeEnv**: the value of the NODE_ENV environment variable. Default: staging.
* **redisUrlKey**: the name of a key in a secret named `redis-urls` that contains a Redis URL to inject as an environment variable. Default: "be".

## Examples
Install a production instance of BE, with async, cron, and mongodb-watcher, and enable the Horizontal Pod Autoscaler:
```
helm upgrade --install be . --set image.tag=<SHA> --set beService=be --set ingress.enabled=true --set fqdn=api.efuse.gg --set redisKeyUrl=be --set autoscaling.enabled=true
helm upgrade --install async . --set image.tag=<SHA> --set beService=async --set redisUrlKey=be --set autoscaling.enabled=true
helm upgrade --install cron . --set image.tag=<SHA> --set beService=cron --set redisUrlKey=cron --set autoscaling.enabled=true
helm upgrade --install cron-async . --set image.tag=<SHA> --set beService=async --set redisUrlKey=cron --set autoscaling.enabled=true
helm upgrade --install mongodb-watcher . --set image.tag=<SHA> --set beService=mongodb-watcher --set redisUrlKey=cron --set autoscaling.enabled=true
```

Install a BE PR at a preview URL, with a lower CPU and memory limit:
```
helm upgrade --install be-pr-12345 . --set image.tag=<SHA> --set beService=be --set ingress.enabled=true --set fqdn=be-pr-12345.efuse.wtf --set externalDNS=true --set resources.limits.memory=1.5Gi --set resources.limits.cpu=750m
```
