#!/bin/bash
# This script is a wrapper around Helm. It requires a few pieces of information
# to work correctly.

set -euo pipefail

function usage {
  echo "Usage: $(basename $0) RELEASE CLUSTER SHA [OPTIONS]" 2>&1
  echo '        RELEASE : name of this release'
  echo '        CLUSTER : k8s cluster to which to deploy'
  echo '        SHA     : image SHA to deploy'
  echo '        OPTIONS : raw Helm options to pass'
}

SED=""
if [[ "$OSTYPE" == "darwin"* ]]; then
  # BSD sed requires an extension, even if it's zero-length.
  # Add this so we can run deploy from Mac laptops, as needed
  SED="''"
fi

if [[ ${#} -eq 0 ]]; then
   usage
   exit 0;
fi

if [ $# -lt 3 ]; then
  echo "At least 3 arguments are required."
  echo
  usage
  exit 1;
fi

RELEASE=$1; shift;
CLUSTER=$1; shift;
SHA=$1; shift;
OPTIONS="$@"

# these should never be empty, but let's be extra careful.
if [ -z "${RELEASE}" ]; then
    echo "Error: Release not defined."
    exit 1;
fi
if [ -z "${CLUSTER}" ]; then
  echo "Error: Cluster not defined."
  exit 1;
fi
if [ -z "${SHA}" ]; then
  echo "Error: git SHA not defined."
  exit 1;
fi

# ensure that we reference the Helm chart regardless of what directory
# we are in when we invoke this script.
PROJECT_DIR=$(git rev-parse --show-toplevel)
HELM_DIR=${PROJECT_DIR}/helm

# change the appVersion in Chart.yaml to the image SHA
sed -i ${SED} s"/appVersion: \"1.0.0\"/appVersion: \"${SHA}\"/" $HELM_DIR/Chart.yaml
helm upgrade --install "${RELEASE}" ${HELM_DIR} \
    --kube-context ${CLUSTER} \
    --set image.tag=${SHA} \
    ${OPTIONS}
if [ "${GITHUB_ACTIONS}" != "true" ]; then
  # we put this back to 1.0.0 if we're running this on a laptop.
  # No need to accidentally commit this change.
  sed -i ${SED} s"/appVersion: \"${SHA}\"/appVersion: \"1.0.0\"/" $HELM_DIR/Chart.yaml
fi

