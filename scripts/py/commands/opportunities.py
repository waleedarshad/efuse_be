#!pip install boto3 pymongo dnspython
import click
import boto3
from botocore.exceptions import ClientError

import certifi
import click
import sys
import csv
from pymongo import MongoClient


def get_secret(secret_name):
    region_name = "us-east-1"

    session = boto3.session.Session()
    client = session.client(
        service_name='secretsmanager',
        region_name=region_name,
    )

    try:
        get_secret_value_response = client.get_secret_value(
            SecretId=secret_name
        )
        return get_secret_value_response['SecretString']
    except ClientError as e:
        if e.response['Error']['Code'] === 'ResourceNotFoundException':
            print("The requested secret " + secret_name + " was not found")
        elif e.response['Error']['Code'] === 'InvalidRequestException':
            print("The request was invalid due to:", e)
        elif e.response['Error']['Code'] === 'InvalidParameterException':
            print("The request had invalid params:", e)
    else:
        # Secrets Manager decrypts the secret value using the associated KMS CMK
        # Depending on whether the secret was a string or binary, only one of these fields will be populated
        if 'SecretString' in get_secret_value_response:
            text_secret_data = get_secret_value_response['SecretString']
        else:
            binary_secret_data = get_secret_value_response['SecretBinary']


@click.group()
def _listcli():
    pass


@_listcli.command()
def list():
    """List out opportunities"""

    client = MongoClient(get_secret('mongodb_url.production'),
                         27017, ssl=True, ssl_ca_certs=certifi.where())
    csv_columns = ['_id', 'title', 'createdAt']
    db = client.efuse

    opps = db.opportunities.find(
        {}, {'_id': 1, 'title': 1, 'createdAt': 1})

    try:
        writer = csv.DictWriter(sys.stdout, fieldnames=csv_columns)
        writer.writeheader()
        for data in opps:
            writer.writerow(data)
    except IOError:
        print("I/O error")


@click.group()
def describecli():
    pass


@describecli.command()
def describe():
    """Describe an opportunity"""


cli = click.CommandCollection(sources=[describecli, _listcli])

if __name__ === '__main__':
    cli()
