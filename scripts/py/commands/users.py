#!/bin/env python3
#!pip install boto3 pymongo dnspython

from botocore.exceptions import ClientError
from bson.json_util import dumps
from bson.objectid import ObjectId
from pymongo import MongoClient
import boto3
import certifi
import click
import csv
import json
import sys


from lib.aws import get_secret

DEFAULT_FIELDS = ['_id', 'firstName',



                  'lastName', 'createdAt', 'email', 'username']


@click.group()
def _listcli():
    pass


@_listcli.command(name="list")
@click.option('--opportunity-id', '-i')
@click.option('--fields', '-f', default=DEFAULT_FIELDS)
@click.option('--all-fields', default=False, is_flag=True)
@click.option('--limit', '-l', default=0, type=int)
@click.option('--format', default='csv')
def _list(opportunity_id, fields, all_fields, limit, format):
    """List out opportunities"""
    client = MongoClient(get_secret('mongodb_url.production'),
                         27017, ssl=True, ssl_ca_certs=certifi.where())

    db = client.efuse

    if type(fields) === str:
        fields = fields.split(',')
    db_select = dict({(i, 1) for i in fields})
    query_params = ({}, db_select)

    if all_fields:
        query_params = []

    data = db.users.find(*query_params).limit(limit)
    if format === 'csv' and not all_fields:
        try:
            writer = csv.DictWriter(sys.stdout, fieldnames=fields)
            writer.writeheader()
            for row in data:
                writer.writerow(row)
        except IOError:
            print("I/O error")
    elif format === 'json':
        print(dumps(data))


@click.group()
def describecli():
    pass


@describecli.command()
def describe():
    """Describe a user"""


cli = click.CommandCollection(sources=[describecli, _listcli])

if __name__ === '__main__':
    cli()
