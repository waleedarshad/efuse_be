import requests
from lxml import html


def get_user_twitch_lxml_tree(twitch_username):
    '''Given a twitch username, scrape socialbade to get twitch stats.'''
    url = f'https://socialblade.com/twitch/user/{twitch_username}'
    user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'
    headers = {'User-Agent': user_agent}
    page = requests.get(url, headers=headers)

    tree = html.fromstring(page.content)
    return tree


def twitch_30day_channel_views(twitch_username):
    '''Given a twitch username, return 30day total channel views.'''
    tree = get_user_twitch_lxml_tree(twitch_username)
    return tree.xpath('//*[@id="socialblade-user-content"]/div[5]/div[2]/p[1]')[0].text.strip()


def twitch_total_followers(twitch_username):
    '''Given a twitch username, return 30day total channel views.'''
    tree = get_user_twitch_lxml_tree(twitch_username)
    return tree.xpath('//*[@id="socialblade-user-content"]/div[13]/div[1]/div[2]/div[2]')[0].text.strip()
