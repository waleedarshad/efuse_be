const unitTestConfig = require("./unit.jest.config");
const integrationTestConfig = require("./int.jest.config");
/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  testEnvironment: "node",
  projects: [...unitTestConfig.projects, ...integrationTestConfig.projects]
};
