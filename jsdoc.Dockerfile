FROM node:14.15.1-slim as builder
WORKDIR /app
COPY . .

RUN yarn jsdoc -c jsdoc.conf --readme readme.md

FROM node:14.15.1-slim as runner
WORKDIR /app
COPY --from=builder /app/docs ./docs
EXPOSE 80
CMD yarn http-server ./docs -p 80
