# eFuse

### NOTE: If on Apple M1 chip

Make sure Rosetta2 is installed:

```
softwareupdate --install-rosetta
```

## Install Node and Npm

```
# On OSX
brew install nvm
```

Once you are using NVM, simply run the `nvm use` command in the root of the project directory to install the correct node version (v15) and the corresponding npm version (v7).

### Manual Steps

If for some reason `nvm use` doesn't work, you can manually install node by following these steps:

#### Install Node 15

```
nvm install v15
> Now using node v15.14.0 (npm v7.5.1)
```

#### (Optional) Make v15 the default version

```
nvm alias default v15.14.0
> default -> v15.14.0
```

#### (Optional) Set up nvm to auto switch

Each of our projects are configured with a .nvmrc file that declares the nvm version for that project. Simply run nvm use when entering a project directory to use the version. Alternatively add the following code if you are using zsh for it to automatically use the right version:

```
# place this after nvm initialization in your .zshrc or .profile!
autoload -U add-zsh-hook
load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"
  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")
    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}
add-zsh-hook chpwd load-nvmrc
load-nvmrc
```

### Verify Install

```
node -v
> v15.14.0

npm -v
> 7.5.1
```

### Install and Run Redis Locally

```
brew install redis
brew services start redis
```

### Clean the Directory and Install NPM Packages

This custom command executes a few commands under the hood. Check out the `package.json` for details. This command can also be used when pulling down new branches.

```
./setup
```

### Run yarn command

Running below command will install all the dependencies of the project

```
yarn install
```

### Run doppler setup command

Running below command gives an enhanced experience & asks you to load existing config. If we select No then it will ask you to select project & the config to load credentials from. For more information please visit our internal doppler docs: https://efuse.atlassian.net/wiki/spaces/EFUSE/pages/2064678913/Secrets+Management+using+Doppler

```
doppler setup
doppler configure set config=dev
```

### Build all the local eFuse written packages

```
yarn build.all
```

### Run the server

```
yarn serve.dev
```

This will launch the front server on http://localhost:5000

## Testing

### Requirements

Docker and Docker Compose must be installed to run the tests.

Follow these links for instructions to install [Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/).

### Run the tests

All tests

```
yarn test
```

Unit only

```
yarn test.unit
```

Integration only

```
yarn test.int
```

## Feature Flagging

Please see the following for adding feature flags to the BE:
https://efuse.atlassian.net/wiki/spaces/EFUSE/pages/187105285/Feature%2BFlags%2BBulletTrain

## Adding new environment variables to BE

https://efuse.atlassian.net/wiki/spaces/EFUSE/pages/2064678913/Secrets+Management+using+Doppler#Adding-a-new-environment-variable-using-GUI:

## Finding circular references

```
yarn dlx madge --circular --extensions js,ts ./packages/api/src/
```
