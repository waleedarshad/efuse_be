FROM node:15.14.0-stretch

ARG GIT_SHA
ARG DOPPLER_TOKEN

ENV GIT_SHA=${GIT_SHA}
ENV DD_VERSION=${GIT_SHA}
ENV NODE_OPTIONS="--max-old-space-size=4096"
ENV PORT=80
ENV PYTHON=/usr/bin/python

RUN apt-get update && apt-get install --no-install-recommends -y \
    build-essential=12.3 \
    gcc=4:6.3.0-4 \
    libc6-dev=2.24-11+deb9u4 \
    make=4.1-9.1 \
    python3=3.5.3-1 \
    rsync=3.1.2-1+deb9u2 \
    wget=1.18-5+deb9u3 \
    apt-transport-https ca-certificates curl gnupg && \
    curl -sLf --retry 3 --tlsv1.2 --proto "=https" 'https://packages.doppler.com/public/cli/gpg.DE2A7741A397C129.key' | apt-key add - && \
    echo "deb https://packages.doppler.com/public/cli/deb/debian any-version main" | tee /etc/apt/sources.list.d/doppler-cli.list && \
    apt-get update && \
    apt-get -y install doppler \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app
COPY . .

RUN ./setup
RUN yarn install --immutable --immutable-cache
RUN yarn build.all

RUN doppler secrets download doppler.encrypted.json -t $DOPPLER_TOKEN

EXPOSE 80
CMD ["doppler", "run", "--fallback=doppler.encrypted.json", "--", "yarn", "node", "./packages/api/dist/src/server.js", "--no-async", "--no-redis"]
